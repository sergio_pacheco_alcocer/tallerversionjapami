﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Autoriza_Ejercido.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Solicitud_Pagos.Datos;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;
using JAPAMI.Parametros_Almacen_Cuentas.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Autoriza_Ejercido : System.Web.UI.Page
{
#region "Page_Load"
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Page_Load
    /// DESCRIPCION : Carga la configuración inicial de los controles de la página.
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Valida que exista algun usuario logueado al sistema.
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
                //Cmb_Anio_Contable.SelectedValue = nuevo;
                //Llenar_Grid_Cierres_generales(nuevo);
                Acciones();
                Txt_Lector_Codigo_Barras.Focus();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
#endregion
    #region "Metodos"
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade 
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Llenar_Grid_Solicitudes_Por_Ejercer();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///                para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                           si es una alta, modificacion
    ///                           
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = true;
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Configuracion_Acceso("Frm_Ope_Con_Autoriza_Ejercido.aspx");
                    break;
            }
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Grid_Solicitudes_Por_Ejercer
    /// DESCRIPCION : Llena el grid Solicitudes de pago que se encuentren en estatus de por pagar
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 22/Diciembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llenar_Grid_Solicitudes_Por_Ejercer()
    {

        String No_Autoriza_Ramo33 = "";
        String No_Autoriza_Ramo33_2 = "";
        try
        {
            Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
            DataTable Dt_Usuario = new DataTable();
            Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
            if (Dt_Usuario.Rows.Count > 0)
            {
                No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
            }
            Grid_Solicitud_Pagos.DataSource = null;   // Se iguala el DataTable con el Grid
            Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;
            Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Autoriza_Solicitud = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();
            DataTable Dt_Resultado = new DataTable();
            Rs_Autoriza_Solicitud.P_Tipo_Recurso = "RECURSO ASIGNADO";
            Dt_Resultado = Rs_Autoriza_Solicitud.Consulta_Solicitudes_SinEjercer();

            Grid_Solicitud_Pagos.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
            Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;


            if (Dt_Resultado.Rows.Count > 0)
            {
                Grid_Solicitud_Pagos.Columns[2].Visible = true;
                Grid_Solicitud_Pagos.Columns[4].Visible = true;
                Grid_Solicitud_Pagos.DataSource = Dt_Resultado;   // Se iguala el DataTable con el Grid
                Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;
                Grid_Solicitud_Pagos.Columns[4].Visible = false;
                Grid_Solicitud_Pagos.Columns[2].Visible = false;
            }
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitudes de Pago", "alert('En este momento no se tienen pagos pendientes por autorizar');", true);
            //}
        }
        catch (Exception ex)
        {
            throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
        }
    }
    // ****************************************************************************************
    //'NOMBRE DE LA FUNCION:Accion
    //'DESCRIPCION : realiza la modificacion del estatus  
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 07/Noviembre/2011 12:12 pm
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Acciones()
    {
        int Actualiza_Presupuesto;
        int Registra_Movimiento;
        String Reserva;
        String Monto;
        String Tipo;
        String Estatus;
        DataTable Dt_solicitud;
        DataTable Dt_Partidas = new DataTable();
        String Accion = String.Empty;
        String No_Solicitud = String.Empty;
        String Comentario = String.Empty;
        String No_Autoriza_Ramo33 = "";
        String No_Autoriza_Ramo33_2 = "";
        DataTable Dt_Consulta_Estatus = new DataTable();
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();    //Objeto de acceso a los metodos.
        Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
        if (Request.QueryString["Accion"] != null)
        {
            Accion = HttpUtility.UrlDecode(Request.QueryString["Accion"].ToString());
            if (Request.QueryString["id"] != null)
            {
                No_Solicitud = HttpUtility.UrlDecode(Request.QueryString["id"].ToString());
            }
            if (Request.QueryString["x"] != null)
            {
                Comentario = HttpUtility.UrlDecode(Request.QueryString["x"].ToString());
            }
            //Response.Clear()zz
            switch (Accion)
            {
                case "Rechazar_Ejercido":
                    Cancela_Solicitud_Pago(No_Solicitud, Comentario, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(),null);
                    break;
                case "Codigo_Barras":
                    if (No_Solicitud.Length < 10)
                    {
                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                        Rs_Solicitud.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                    }
                    else
                    {
                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                        Rs_Solicitud.P_No_Solicitud_Pago = No_Solicitud;
                    }
                    Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
                    DataTable Dt_Usuario = new DataTable();
                    Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
                    if (Dt_Usuario.Rows.Count > 0)
                    {
                        No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                        No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                    }
                    if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                    {
                        Rs_Solicitud.P_Tipo_Recurso = "RAMO 33";
                    }
                    else
                    {
                        Rs_Solicitud.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    }
                    Dt_Consulta_Estatus = Rs_Solicitud.Consulta_Solicitudes_SinAutorizar();
                    if (Dt_Consulta_Estatus.Rows.Count > 0)
                    {
                        foreach (DataRow Registro in Dt_Consulta_Estatus.Rows)
                        {
                            if (Registro["Estatus"].ToString() == "PORPAGAR")
                            {
                                Dt_solicitud = Rs_Solicitud_Pago.Consultar_Solicitud_Pago();
                                if (Dt_solicitud.Rows.Count > 0)
                                {
                                    Tipo = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                    Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                                    if (Tipo != "00001" && Tipo != "00003")
                                    {
                                        if (Estatus == "PORPAGAR")
                                        {
                                            Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                                            Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                                            Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Rs_Solicitud_Pago.P_No_Solicitud_Pago);
                                            Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Reserva, "EJERCIDO", "DEVENGADO", Dt_Partidas);
                                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "EJERCIDO", "DEVENGADO", Convert.ToDouble(Monto), "", "", "", "");
                                        }
                                        if (Estatus == "PORPAGAR")
                                        {
                                            Rs_Solicitud_Pago.P_No_Solicitud_Pago = Rs_Solicitud_Pago.P_No_Solicitud_Pago;
                                            Rs_Solicitud_Pago.P_Estatus = "PRE-EJERCIDO";
                                            Rs_Solicitud_Pago.P_Comentario = Comentario;
                                            Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                            Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                            Rs_Solicitud_Pago.P_Usuario_Autorizo_Ejercido = Cls_Sessiones.Nombre_Empleado.ToString();
                                            Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                        }
                                    }
                                    else
                                    {
                                        if (Estatus == "PORPAGAR")
                                        {
                                            Rs_Solicitud_Pago.P_No_Solicitud_Pago = Rs_Solicitud_Pago.P_No_Solicitud_Pago;
                                            Rs_Solicitud_Pago.P_Estatus = "PORCOMPROBAR";
                                            Rs_Solicitud_Pago.P_Comentario = Comentario;
                                            Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                            Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                            Rs_Solicitud_Pago.P_Usuario_Autorizo_Ejercido = Cls_Sessiones.Nombre_Empleado.ToString();
                                            Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Autorizar_Solicitud
    ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado 
    ///PROPIEDADES          :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 02/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Autorizar_Solicitud(object sender, EventArgs e)
    {
        Int32 Indice = 0;
        int Actualiza_Presupuesto;
        int Registra_Movimiento;
        String Reserva;
        String Monto;
        String Tipo;
        String Autorizados = "Ninguno";
        String Estatus="";
        Boolean Afectado = true;
        String Servicios_Generales = "";
        DataTable Dt_solicitud;
        DataTable Dt_Partidas = new DataTable();
        String No_Solicitud = String.Empty;
        String No_Autoriza_Ramo33 = "";
        String No_Autoriza_Ramo33_2 = "";
        Boolean Autorizado;
        DataTable Dt_Consulta_Estatus = new DataTable();
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();    //Objeto de acceso a los metodos.
        Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        String Tipo_Comprobacion = "";
        try
        {
            foreach (GridViewRow Renglon_Grid in Grid_Solicitud_Pagos.Rows)
            {
                Indice++;
                Grid_Solicitud_Pagos.SelectedIndex = Indice;
                Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                if (Autorizado)
                {
                    Autorizados = "SI";
                    No_Solicitud = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).CssClass;
                    Rs_Solicitud.P_No_Solicitud_Pago = No_Solicitud;
                    Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario_2 = new Cls_Cat_Con_Parametros_Negocio();
                    DataTable Dt_Usuario_2 = new DataTable();
                    Dt_Usuario_2 = Rs_Validar_Usuario_2.Consulta_Datos_Parametros();
                    if (Dt_Usuario_2.Rows.Count > 0)
                    {
                        No_Autoriza_Ramo33 = Dt_Usuario_2.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                        No_Autoriza_Ramo33_2 = Dt_Usuario_2.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                    }
                    if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                    {
                        Rs_Solicitud.P_Tipo_Recurso = "RAMO 33";
                    }
                    else
                    {
                        Rs_Solicitud.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    }
                    Dt_Consulta_Estatus = Rs_Solicitud.Consulta_Solicitudes_SinAutorizar();
                    if (Dt_Consulta_Estatus.Rows.Count > 0)
                    {
                        foreach (DataRow Registro in Dt_Consulta_Estatus.Rows)
                        {
                            if (Registro["Estatus"].ToString() == "PORPAGAR")
                            {
                                SqlConnection Cn = new SqlConnection();
                                SqlCommand Cmmd = new SqlCommand();
                                SqlTransaction Trans = null;

                                // crear transaccion para crear el convenio 
                                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                                Cn.Open();
                                Trans = Cn.BeginTransaction();
                                Cmmd.Connection = Cn;
                                Cmmd.Transaction = Trans;
                                try
                                {
                                    Tipo_Comprobacion = "";
                                    Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                    Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                    Dt_solicitud = Rs_Solicitud_Pago.Consultar_Solicitud_Pago();
                                    if (Dt_solicitud.Rows.Count > 0)
                                    {
                                        //revisar si la solicitud es de servicios masivos generales 
                                        if (!String.IsNullOrEmpty(Dt_solicitud.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString().Trim()))
                                        {
                                            Servicios_Generales = "SI";
                                        }
                                        else
                                        {
                                            Servicios_Generales = "NO";
                                        }
                                        Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                        Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                                        Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                                        if (Tipo_Comprobacion != "SI")
                                        {
                                           // Respuesta = Autoriza_Contabilidad_Solicitud_pago(No_Solicitud, Cmmd);//, Comentario, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString());
                                            if (Estatus == "PORPAGAR")
                                            {
                                                DataTable Dt_Tipo = new DataTable();
                                                Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                                Dt_Tipo=Rs_Consulta.Consulta_Tipo_Solicitud_Pagos();
                                                if (Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().ToUpper() != "FINIQUITO" || Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().Substring(0, 4).ToUpper() != "FINI")
                                                {
                                                    Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                                                    Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                                                    if (Servicios_Generales == "SI")
                                                    {
                                                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Servicios_Generales(No_Solicitud, Cmmd);
                                                    }
                                                    else
                                                    {
                                                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(No_Solicitud, Cmmd);
                                                    }
                                                    Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("EJERCIDO", "DEVENGADO", Dt_Partidas, Cmmd);
                                                    if (Actualiza_Presupuesto > 0)
                                                    {
                                                        Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "EJERCIDO", "DEVENGADO", Convert.ToDouble(Monto), "", "", "", "", Cmmd);
                                                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                                        Rs_Solicitud_Pago.P_Estatus = "PRE-EJERCIDO";
                                                        Rs_Solicitud_Pago.P_Comentario = "";
                                                        Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                                        Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                                        Rs_Solicitud_Pago.P_Usuario_Autorizo_Ejercido = Cls_Sessiones.Nombre_Empleado.ToString();
                                                        Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                                        Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                                        Trans.Commit();
                                                    }
                                                    else
                                                    {
                                                        Trans.Rollback();
                                                        Lbl_Mensaje_Error.Text = "Error:";
                                                        Lbl_Mensaje_Error.Text = "No hay suficiencia Presupuestal para realizar la autorizacion de la solicitud No." + No_Solicitud;
                                                        Img_Error.Visible = true;
                                                        Lbl_Mensaje_Error.Visible = true;
                                                        Afectado = false;
                                                    }
                                                }
                                                else
                                                {
                                                    Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                                                    Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                                                    Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Finiquito(No_Solicitud, Cmmd);
                                                    Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("EJERCIDO", "DEVENGADO", Dt_Partidas, Cmmd);
                                                    if (Actualiza_Presupuesto > 0)
                                                    {
                                                        Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "EJERCIDO", "DEVENGADO", Convert.ToDouble(Monto), "", "", "", "", Cmmd);
                                                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                                        Rs_Solicitud_Pago.P_Estatus = "PRE-EJERCIDO";
                                                        Rs_Solicitud_Pago.P_Comentario = "";
                                                        Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                                        Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                                        Rs_Solicitud_Pago.P_Usuario_Autorizo_Ejercido = Cls_Sessiones.Nombre_Empleado.ToString();
                                                        Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                                        Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                                        Trans.Commit();
                                                    }
                                                    else
                                                    {
                                                        Trans.Rollback();
                                                        Lbl_Mensaje_Error.Text = "Error:";
                                                        Lbl_Mensaje_Error.Text = "No hay suficiencia Presupuestal para realizar la autorizacion de la solicitud No." + No_Solicitud;
                                                        Img_Error.Visible = true;
                                                        Lbl_Mensaje_Error.Visible = true;
                                                        Afectado = false;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (Estatus == "PORPAGAR")
                                            {
                                                Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                                Rs_Solicitud_Pago.P_Estatus = "PORCOMPROBAR";
                                                Rs_Solicitud_Pago.P_Comentario = "";
                                                Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                                Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                                Rs_Solicitud_Pago.P_Usuario_Autorizo_Ejercido = Cls_Sessiones.Nombre_Empleado.ToString();
                                                Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                                Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                                Trans.Commit();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Trans.Commit();
                                    }
                                }
                                catch (SqlException Ex)
                                {
                                    Trans.Rollback();
                                    Lbl_Mensaje_Error.Text = "Error:";
                                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Visible = true;
                                }
                                catch (Exception Ex)
                                {
                                    Lbl_Mensaje_Error.Text = "Error:";
                                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Visible = true;
                                }
                                finally
                                {
                                    Cn.Close();
                                }
                            }
                            if (Afectado == false)
                            {
                                break;
                            }
                        }
                    }
                }
                if (Afectado == false)
                {
                    break;
                }
            }
            if (Autorizados != "SI")
            {
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = "No Selecciono ninguna Solicitud para ser Autorizado";
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Visible = true;
            }
            else
            {
                if (Afectado != false)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "window.setTimeout('window.location.reload(true);',1);", true);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
        }
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Txt_Lector_Codigo_Barras_TextChanged
    ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado 
    ///PROPIEDADES          :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 02/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Txt_Lector_Codigo_Barras_TextChanged(object sender, EventArgs e)
    {
        int Actualiza_Presupuesto;
        int Registra_Movimiento;
        String Reserva;
        String Monto;
        String Tipo;
        String Estatus;
        String Servicios_Generales = "";
        Boolean Afectado = true;
        DataTable Dt_solicitud;
        DataTable Dt_Partidas = new DataTable();
        String Accion = String.Empty;
        String No_Solicitud = String.Empty;
        String Comentario = String.Empty;
        String No_Autoriza_Ramo33 = "";
        String No_Autoriza_Ramo33_2 = "";
        DataTable Dt_Consulta_Estatus = new DataTable();
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();    //Objeto de acceso a los metodos.
        Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        String Tipo_Comprobacion = "";
        try
        {
            if (!String.IsNullOrEmpty(Txt_Lector_Codigo_Barras.Text))
            {
                No_Solicitud = Txt_Lector_Codigo_Barras.Text;
                if (No_Solicitud.Length < 10)
                {
                    Rs_Solicitud_Pago.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                    Rs_Solicitud.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                    No_Solicitud = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                }
                else
                {
                    Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                    Rs_Solicitud.P_No_Solicitud_Pago = No_Solicitud;
                }
                Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
                DataTable Dt_Usuario = new DataTable();
                Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
                if (Dt_Usuario.Rows.Count > 0)
                {
                    No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                    No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                }
                if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                {
                    Rs_Solicitud.P_Tipo_Recurso = "RAMO 33";
                }
                else
                {
                    Rs_Solicitud.P_Tipo_Recurso = "RECURSO ASIGNADO";
                }
                Dt_Consulta_Estatus = Rs_Solicitud.Consulta_Solicitudes_SinAutorizar();
                if (Dt_Consulta_Estatus.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Consulta_Estatus.Rows)
                    {
                        if (Registro["Estatus"].ToString() == "PORPAGAR")
                        {
                            SqlConnection Cn = new SqlConnection();
                            SqlCommand Cmmd = new SqlCommand();
                            SqlTransaction Trans = null;

                            // crear transaccion para crear el convenio 
                            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                            Cn.Open();
                            Trans = Cn.BeginTransaction();
                            Cmmd.Connection = Cn;
                            Cmmd.Transaction = Trans;
                            try
                            {
                                Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                Dt_solicitud = Rs_Solicitud_Pago.Consultar_Solicitud_Pago();
                                if (Dt_solicitud.Rows.Count > 0)
                                {
                                    //revisar si la solicitud es de servicios masivos generales 
                                    if (!String.IsNullOrEmpty(Dt_solicitud.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString().Trim()))
                                    {
                                        Servicios_Generales = "SI";
                                    }
                                    else
                                    {
                                        Servicios_Generales = "NO";
                                    }
                                    Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                    Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                                    Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                                    if (Tipo_Comprobacion != "SI")
                                    {
                                        // Respuesta = Autoriza_Contabilidad_Solicitud_pago(No_Solicitud, Cmmd);//, Comentario, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString());
                                        if (Estatus == "PORPAGAR")
                                        {
                                            DataTable Dt_Tipo = new DataTable();
                                            Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                            Dt_Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos();
                                            if (Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().ToUpper() != "FINIQUITO" || Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().Substring(0, 4).ToUpper() != "FINI")
                                            {
                                                Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                                                Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                                                if (Servicios_Generales == "SI")
                                                {
                                                    Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Servicios_Generales(No_Solicitud, Cmmd);
                                                }
                                                else
                                                {
                                                    Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(No_Solicitud, Cmmd);
                                                }
                                                Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("EJERCIDO", "DEVENGADO", Dt_Partidas, Cmmd);
                                                if (Actualiza_Presupuesto > 0)
                                                {
                                                    Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "EJERCIDO", "DEVENGADO", Convert.ToDouble(Monto), "", "", "", "", Cmmd);
                                                    Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                                    Rs_Solicitud_Pago.P_Estatus = "PRE-EJERCIDO";
                                                    Rs_Solicitud_Pago.P_Comentario = "";
                                                    Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                                    Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                                    Rs_Solicitud_Pago.P_Usuario_Autorizo_Ejercido = Cls_Sessiones.Nombre_Empleado.ToString();
                                                    Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                                    Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                                    Trans.Commit();
                                                }
                                                else
                                                {
                                                    Trans.Rollback();
                                                    Lbl_Mensaje_Error.Text = "Error:";
                                                    Lbl_Mensaje_Error.Text = "No hay suficiencia Presupuestal para realizar la autorizacion de la solicitud No." + No_Solicitud;
                                                    Img_Error.Visible = true;
                                                    Lbl_Mensaje_Error.Visible = true;
                                                    Afectado = false;
                                                }
                                            }
                                            else
                                            {
                                                Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                                                Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                                                Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Finiquito(No_Solicitud, Cmmd);
                                                Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("EJERCIDO", "DEVENGADO", Dt_Partidas, Cmmd);
                                                if (Actualiza_Presupuesto > 0)
                                                {
                                                    Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "EJERCIDO", "DEVENGADO", Convert.ToDouble(Monto), "", "", "", "", Cmmd);
                                                    Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                                    Rs_Solicitud_Pago.P_Estatus = "PRE-EJERCIDO";
                                                    Rs_Solicitud_Pago.P_Comentario = "";
                                                    Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                                    Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                                    Rs_Solicitud_Pago.P_Usuario_Autorizo_Ejercido = Cls_Sessiones.Nombre_Empleado.ToString();
                                                    Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                                    Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                                    Trans.Commit();
                                                }
                                                else
                                                {
                                                    Trans.Rollback();
                                                    Lbl_Mensaje_Error.Text = "Error:";
                                                    Lbl_Mensaje_Error.Text = "No hay suficiencia Presupuestal para realizar la autorizacion de la solicitud No." + No_Solicitud;
                                                    Img_Error.Visible = true;
                                                    Lbl_Mensaje_Error.Visible = true;
                                                    Afectado = false;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Estatus == "PORPAGAR")
                                        {
                                            Rs_Solicitud_Pago.P_No_Solicitud_Pago = Rs_Solicitud_Pago.P_No_Solicitud_Pago;
                                            Rs_Solicitud_Pago.P_Estatus = "PORCOMPROBAR";
                                            Rs_Solicitud_Pago.P_Comentario = Comentario;
                                            Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                            Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                            Rs_Solicitud_Pago.P_Usuario_Autorizo_Ejercido = Cls_Sessiones.Nombre_Empleado.ToString();
                                            Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                            Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                            Trans.Commit();
                                        }
                                    }
                                }
                                else
                                {
                                    Trans.Commit();
                                }
                            }
                            catch (SqlException Ex)
                            {
                                Trans.Rollback();
                                Lbl_Mensaje_Error.Text = "Error:";
                                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                            }
                            catch (Exception Ex)
                            {
                                Lbl_Mensaje_Error.Text = "Error:";
                                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                            }
                            finally
                            {
                                Cn.Close();
                            }
                        }
                    }
                }
                if (Afectado != false)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "window.setTimeout('window.location.reload(true);',1);", true);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 24/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Cancela_Solicitud_Pago(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand P_Cmmd)
    {
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Honorarios = new DataTable();
        DataTable Dt_Arrendamientos = new DataTable();
        DataTable Dt_Hon_Asimilables = new DataTable();
        DataRow Registro_Honorarios;
        Double Monto_total = 0;
        DataRow Registro_Arrendamiento;
        DataRow Registro_Hon_Asimila;
        Decimal Total_Iva_por_Acreditar = 0;
        Decimal Total_ISR_Honorarios = 0;
        Decimal Total_Cedular_Honorarios = 0;
        Decimal Total_ISR_Arrendamientos = 0;
        Decimal Total_Cedular_Arrendamientos = 0;
        Decimal Total_Hono_asimi = 0;
        String Cuenta_ISR = "0";
        String Cuenta_Cedular = "0";
        String Beneficiario_ID = "";
        String CTA_IVA_PENDIENTE = "";
        String Tipo_Beneficiario = "";
        Decimal Iva_Por_Partida = 0;
        int partida = 0;
        DataRow row;
        int Partida = 0;
        String Resultado = "SI"; 
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        String Tipo_Comprobacion = "";
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            //Agregar las partidas de los impuestos de isr y de retencion de iva si tiene honorarios la solicitud de pago
            //se crean las columnas de los datatable de honorarios y de arrendamientos
            if (Dt_Honorarios.Rows.Count == 0)
            {
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Decimal));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Decimal));
            }
            if (Dt_Arrendamientos.Rows.Count == 0)
            {
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Decimal));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Decimal));
            }
            if (Dt_Hon_Asimilables.Rows.Count == 0)
            {
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Decimal));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Decimal));
            }
            //se consultan los detalles de la solicitud de pago
            Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
            Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
            if (Dt_Datos_Solicitud.Rows.Count > 0)
            {
                foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                {
                    if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                    {
                        Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                    }
                }
                //se recorre el dataset de los detalles para dividir los detalles por tipo de operacion 
                foreach (DataRow fila in Dt_Datos_Solicitud.Rows)
                {
                    if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "HONORARIOS")
                    {
                        Registro_Honorarios = Dt_Honorarios.NewRow();
                        Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                        Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                        Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                        Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                        Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                        Dt_Honorarios.Rows.Add(Registro_Honorarios); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Honorarios.AcceptChanges();
                    }
                    if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "ARRENDAMIENTO")
                    {
                        Registro_Arrendamiento = Dt_Arrendamientos.NewRow();
                        Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                        Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                        Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                        Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                        Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                        Dt_Arrendamientos.Rows.Add(Registro_Arrendamiento); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Arrendamientos.AcceptChanges();
                    }
                    if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "HON. ASIMILABLE")
                    {
                        Registro_Hon_Asimila = Dt_Hon_Asimilables.NewRow();
                        Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                        Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                        Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                        Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                        Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                        Dt_Hon_Asimilables.Rows.Add(Registro_Hon_Asimila); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Hon_Asimilables.AcceptChanges();
                    }
                }
                Total_ISR_Honorarios = 0;
                Total_Cedular_Honorarios = 0;
                if (Dt_Honorarios.Rows.Count > 0)
                {
                    foreach (DataRow fila in Dt_Honorarios.Rows)
                    {
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                        {
                            Total_ISR_Honorarios = Total_ISR_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                        }
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                        {
                            Total_Cedular_Honorarios = Total_Cedular_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                        }
                    }
                }
                Total_ISR_Arrendamientos = 0;
                Total_Cedular_Arrendamientos = 0;
                if (Dt_Arrendamientos.Rows.Count > 0)
                {
                    foreach (DataRow fila in Dt_Arrendamientos.Rows)
                    {
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                        {
                            Total_ISR_Arrendamientos = Total_ISR_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                        }
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                        {
                            Total_Cedular_Arrendamientos = Total_Cedular_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                        }
                    }
                }
                Total_Hono_asimi = 0;
                if (Dt_Hon_Asimilables.Rows.Count > 0)
                {
                    foreach (DataRow fila in Dt_Hon_Asimilables.Rows)
                    {
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                        {
                            Total_Hono_asimi = Total_Hono_asimi + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                        }
                    }
                }
            }
        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
        Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
        String Tipo_Poliza_ID = "";
        foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
        {
            if (!String.IsNullOrEmpty(Registro["Monto_Partida"].ToString()))
            {
                if (!String.IsNullOrEmpty(Registro["IVA_MONTO"].ToString()))
                {
                    Iva_Por_Partida = Convert.ToDecimal(Registro["IVA_MONTO"].ToString());
                }
                else
                {
                    Iva_Por_Partida = 0;
                }
                Partida = Partida + 1;
                Txt_Monto_Solicitud.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                if (!String.IsNullOrEmpty(Registro["Empleado_Cuenta"].ToString()))
                {
                    Txt_Cuenta_Contable_ID_Empleado.Value = Registro["Empleado_Cuenta"].ToString();
                    Beneficiario_ID = Registro[Cat_Empleados.Campo_Empleado_ID].ToString();
                    Tipo_Beneficiario = "EMPLEADO";
                }
                else
                {
                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Acreedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "A")
                    {
                        Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Acreedor"].ToString();
                        Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                        Tipo_Beneficiario = "PROVEEDOR";
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Contratista"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "C")
                        {
                            Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Contratista"].ToString();
                            Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                            Tipo_Beneficiario = "PROVEEDOR";
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Deudor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "D")
                            {
                                Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Deudor"].ToString();
                                Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                Tipo_Beneficiario = "PROVEEDOR";
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Judicial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "J")
                                {
                                    Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Judicial"].ToString();
                                    Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                    Tipo_Beneficiario = "PROVEEDOR";
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Nomina"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "N")
                                    {
                                        Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Nomina"].ToString();
                                        Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                        Tipo_Beneficiario = "PROVEEDOR";
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Predial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "Z")
                                        {
                                            Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Predial"].ToString();
                                            Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                            Tipo_Beneficiario = "PROVEEDOR";
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Proveedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                                            {
                                                Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Proveedor"].ToString();
                                                Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                                Tipo_Beneficiario = "PROVEEDOR";
                                            }
                                            else
                                            {
                                                if (Registro["Tipo_Solicitud_Pago_ID"].ToString() == "00008" && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                                                {
                                                    Rs_Cuentas_Proveedor.P_Cuenta = "211200001";
                                                    Rs_Cuentas_Proveedor.P_Cmmd = Cmmd;
                                                    Dt_C_Proveedor = Rs_Cuentas_Proveedor.Consulta_Existencia_Cuenta_Contable();
                                                    if (Dt_C_Proveedor.Rows.Count > 0)
                                                    {
                                                        Txt_Cuenta_Contable_ID_Proveedor.Value = Dt_C_Proveedor.Rows[0]["Cuenta_Contable_ID"].ToString();
                                                        Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                                        Tipo_Beneficiario = "PROVEEDOR";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (!String.IsNullOrEmpty(Registro["Tipo_Solicitud_Pago_ID"].ToString()))
                {
                    Tipo_Poliza_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                }
                Txt_Monto_Solicitud.Value = Registro["Monto"].ToString();
                Txt_Concepto_Solicitud.Value = Registro["Concepto"].ToString();
                Txt_Cuenta_Contable_reserva.Value = Registro["Cuenta_Contable_Reserva"].ToString();
                Txt_No_Reserva.Value = Registro["NO_RESERVA"].ToString();
                Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Tipo_Poliza_ID;
                Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                if (Tipo_Comprobacion != "SI")
                {
                            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                            if (Dt_Partidas_Polizas.Rows.Count == 0)
                            {
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                                Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                            }
                            row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_reserva.Value;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Registro["Monto_Partida"].ToString()) + Convert.ToDecimal(Registro["Monto_ISR"].ToString()) + Convert.ToDecimal(Registro["Monto_Cedular"].ToString()) - Iva_Por_Partida;
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                            Monto_total = Monto_total + (Convert.ToDouble(Registro["Monto_Partida"].ToString()) + Convert.ToDouble(Registro["Monto_ISR"].ToString()) + Convert.ToDouble(Registro["Monto_Cedular"].ToString()) - Convert.ToDouble(Iva_Por_Partida));
                }
            }
        }
        if (Tipo_Comprobacion != "SI")
        {
            //se consultan los detalles de la solicitud de pago
            Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
            Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
            if (Dt_Datos_Solicitud.Rows.Count > 0)
            {
                Total_ISR_Honorarios = 0;
                Total_Cedular_Honorarios = 0;
                if (Dt_Honorarios.Rows.Count > 0)
                {
                    foreach (DataRow fila in Dt_Honorarios.Rows)
                    {
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                        {
                            Total_ISR_Honorarios = Total_ISR_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                        }
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                        {
                            Total_Cedular_Honorarios = Total_Cedular_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                        }
                    }
                }
                Total_ISR_Arrendamientos = 0;
                Total_Cedular_Arrendamientos = 0;
                if (Dt_Arrendamientos.Rows.Count > 0)
                {
                    foreach (DataRow fila in Dt_Arrendamientos.Rows)
                    {
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                        {
                            Total_ISR_Arrendamientos = Total_ISR_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                        }
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                        {
                            Total_Cedular_Arrendamientos = Total_Cedular_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                        }
                    }
                }
                row = Dt_Partidas_Polizas.NewRow();
                partida = partida + 1;
                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Proveedor.Value.ToString()))
                {
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Proveedor.Value;
                }
                else
                {
                    if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Empleado.Value.ToString()))
                    {
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Empleado.Value;
                    }
                }
                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + No_Solicitud_Pago;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Txt_Monto_Solicitud.Value.ToString()) - (Total_Cedular_Arrendamientos + Total_Cedular_Honorarios + Total_ISR_Arrendamientos + Total_ISR_Honorarios);
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row["Beneficiario_ID"] = Beneficiario_ID;
                row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                // se recorre el datatable de honorarios para realizar la sumatoria de los impuestos en las variables correspondientes 
                //y asi obtener el total de los impuestos de cada tipo
                Rs_Parametros.P_Cmmd = Cmmd;
                Dt_Cuentas = Rs_Parametros.Consulta_Datos_Parametros();
                if (Dt_Honorarios.Rows.Count > 0)
                {
                    if (Total_ISR_Honorarios > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas.Rows.Count > 0){
                            Cuenta_ISR = Dt_Cuentas.Rows[0]["CTA_ISR"].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(Cuenta_ISR))
                        {
                            partida = partida + 1;
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_ISR_Honorarios;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                    if (Total_Cedular_Honorarios > 0)
                    {
                        // CUENTA IMPUESTO CEDULAR 1% HONORARIOS RET consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas.Rows.Count > 0){
                            Cuenta_Cedular = Dt_Cuentas.Rows[0]["CTA_Cedular"].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(Cuenta_Cedular))
                        {
                            partida = partida + 1;
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cedular;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Cedular_Honorarios;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        
                    }
                }
                // se recorre el datatable de honorarios para realizar la sumatoria de los impuestos en las variables correspondientes 
                //y asi obtener el total de los impuestos de cada tipo
                if (Dt_Arrendamientos.Rows.Count > 0)
                {
                    Cuenta_Cedular = "";
                    Cuenta_ISR = "";
                    if (Total_ISR_Arrendamientos > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas.Rows.Count > 0){
                            Cuenta_ISR = Dt_Cuentas.Rows[0]["ISR_Arrendamiento_ID"].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(Cuenta_ISR))
                        {
                        partida = partida + 1;
                        row = Dt_Partidas_Polizas.NewRow();
                        //Agrega el cargo del registro de la póliza
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + No_Solicitud_Pago;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_ISR_Arrendamientos;
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                        row["Beneficiario_ID"] = Beneficiario_ID;
                        row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                        Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Partidas_Polizas.AcceptChanges();
                        }
                        
                    }
                    if (Total_Cedular_Arrendamientos > 0)
                    {
                        // CUENTA IMPUESTO CEDULAR 1% HONORARIOS RET consultamos el id que tiene la cuenta en el sistema
                         if (Dt_Cuentas.Rows.Count > 0){
                            Cuenta_Cedular = Dt_Cuentas.Rows[0]["Cedular_Arrendamiento_ID"].ToString().Trim();
                        }
                         if (!String.IsNullOrEmpty(Cuenta_Cedular))
                         {
                             partida = partida + 1;
                             row = Dt_Partidas_Polizas.NewRow();
                             //Agrega el cargo del registro de la póliza
                             row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                             row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cedular;
                             row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + No_Solicitud_Pago;
                             row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Cedular_Arrendamientos;
                             row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                             row["Beneficiario_ID"] = Beneficiario_ID;
                             row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                             Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                             Dt_Partidas_Polizas.AcceptChanges();
                         }
                    }
                }

                if (Dt_Hon_Asimilables.Rows.Count > 0)
                {
                    if (Total_Hono_asimi > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas.Rows.Count > 0)
                        {
                            Cuenta_ISR = Dt_Cuentas.Rows[0]["Honorarios_Asimilables_ID"].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(Cuenta_ISR))
                        {
                            partida = partida + 1;
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Hono_asimi;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
                //Se agrega la partida de iva pendiente de acreditar
                Rs_Parametros_Iva.P_Cmmd = Cmmd;
                Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                if (Dt_Cuentas_Iva.Rows.Count > 0)
                {
                    if (Total_Iva_por_Acreditar > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_PENDIENTE = Dt_Cuentas_Iva.Rows[0]["CTA_IVA_PENDIENTE"].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_PENDIENTE))
                        {
                            partida = partida + 1;
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_PENDIENTE;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Solicitud.Value;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
            }
            //Agrega los valores a pasar a la capa de negocios para ser dados de alta
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(partida);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Monto_total + Convert.ToDouble(Total_Iva_por_Acreditar);// Convert.ToDouble(Txt_Monto_Solicitud.Value);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto = "Rechazo-" + No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentarios_Ejercido = Comentario;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva_Anterior = Convert.ToDouble(Txt_No_Reserva.Value);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Resultado=Rs_Modificar_Ope_Con_Solicitud_Pagos.Modificar_Solicitud_Pago(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
        }
        else
        {
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Convert.ToDouble(Txt_Monto_Solicitud.Value);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentarios_Ejercido = Comentario;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva_Anterior = Convert.ToDouble(Txt_No_Reserva.Value);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.Modificar_Solicitud_Pago_Sin_Poliza(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
            Resultado = "SI";
        }
        return Resultado;
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago_Servicios_Generales
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 24/Septiembre/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Cancela_Solicitud_Pago_Servicios_Generales(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand P_Cmmd)
    {
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Honorarios = new DataTable();
        DataTable Dt_Arrendamientos = new DataTable();
        DataTable Dt_Hon_Asimilables = new DataTable();
        Double Monto_total = 0;
        Decimal Total_Iva_por_Acreditar = 0;
        String Beneficiario_ID = "";
        String CTA_IVA_PENDIENTE = "";
        String Tipo_Beneficiario = "";
        DataRow row;
        int Partida = 0;
        String Resultado = "SI"; 
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        String Tipo_Comprobacion = "";
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
        //se consultan los detalles de la solicitud de pago
        Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
        Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
        Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
        if (Dt_Datos_Solicitud.Rows.Count > 0)
        {
            foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
            {
                if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                {
                    Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                }
            }
        }
        //Agrega los campos que va a contener el DataTable de los detalles de la póliza
        if (Dt_Partidas_Polizas.Rows.Count == 0)
        {
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
        }
        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
        Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago_Servicios_Generales();
        String Tipo_Poliza_ID = "";
        foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
        {
            if (!String.IsNullOrEmpty(Registro["Monto_Partida"].ToString()))
            {
                Partida = Partida + 1;
                Txt_Monto_Solicitud.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                if (!String.IsNullOrEmpty(Registro["Empleado_Cuenta"].ToString()))
                {
                    Txt_Cuenta_Contable_ID_Empleado.Value = Registro["Empleado_Cuenta"].ToString();
                    Beneficiario_ID = Registro[Cat_Empleados.Campo_Empleado_ID].ToString();
                    Tipo_Beneficiario = "EMPLEADO";
                }
                else
                {
                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Proveedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                    {
                        Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Proveedor"].ToString();
                        Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                        Tipo_Beneficiario = "PROVEEDOR";
                    }
                }
                if (!String.IsNullOrEmpty(Registro["Tipo_Solicitud_Pago_ID"].ToString()))
                {
                    Tipo_Poliza_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                }
                Txt_Monto_Solicitud.Value = Registro["Monto"].ToString();
                Txt_Concepto_Solicitud.Value = Registro["Concepto"].ToString();
                Txt_Cuenta_Contable_reserva.Value = Registro["Cuenta_Contable_Reserva"].ToString();
                Txt_No_Reserva.Value = Registro["NO_RESERVA"].ToString();
                Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Tipo_Poliza_ID;
                Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                if (Tipo_Comprobacion != "SI")
                {
                            row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_reserva.Value;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Registro["Monto_Partida"].ToString());
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                            Monto_total = Monto_total + (Convert.ToDouble(Registro["Monto_Partida"].ToString()));// + Convert.ToDouble(Registro["Monto_ISR"].ToString()) + Convert.ToDouble(Registro["Monto_Cedular"].ToString()) - Convert.ToDouble(Iva_Por_Partida));
                }
            }
        }
        if (Tipo_Comprobacion != "SI")
        {
            //se consultan los detalles de la solicitud de pago
            Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
            Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
            if (Dt_Datos_Solicitud.Rows.Count > 0)
            {
                row = Dt_Partidas_Polizas.NewRow();
                Partida = Partida + 1;
                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Proveedor.Value.ToString()))
                {
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Proveedor.Value;
                }
                else
                {
                    if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Empleado.Value.ToString()))
                    {
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Empleado.Value;
                    }
                }
                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + No_Solicitud_Pago;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Txt_Monto_Solicitud.Value.ToString());// - (Total_Cedular_Arrendamientos + Total_Cedular_Honorarios + Total_ISR_Arrendamientos + Total_ISR_Honorarios);
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row["Beneficiario_ID"] = Beneficiario_ID;
                row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                //Se agrega la partida de iva pendiente de acreditar
                Rs_Parametros_Iva.P_Cmmd = Cmmd;
                Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                if (Dt_Cuentas_Iva.Rows.Count > 0)
                {
                    if (Total_Iva_por_Acreditar > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_PENDIENTE = Dt_Cuentas_Iva.Rows[0]["CTA_IVA_PENDIENTE"].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_PENDIENTE))
                        {
                            Partida = Partida + 1;
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_PENDIENTE;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Solicitud.Value;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
            }
            //Agrega los valores a pasar a la capa de negocios para ser dados de alta
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(Partida);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Monto_total + Convert.ToDouble(Total_Iva_por_Acreditar);// Convert.ToDouble(Txt_Monto_Solicitud.Value);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
            //Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto = "Rechazo-" + No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentarios_Ejercido = Comentario;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva_Anterior = Convert.ToDouble(Txt_No_Reserva.Value);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Servicios_Generales = "SI";
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Resultado=Rs_Modificar_Ope_Con_Solicitud_Pagos.Modificar_Solicitud_Pago(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
        }
        else
        {
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Convert.ToDouble(Txt_Monto_Solicitud.Value);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentarios_Ejercido = Comentario;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva_Anterior = Convert.ToDouble(Txt_No_Reserva.Value);
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.Modificar_Solicitud_Pago_Sin_Poliza(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
            Resultado = "SI";
        }
        return Resultado;
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago_Finiquito
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Cancela_Solicitud_Pago_Finiquito(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand P_Cmmd)
    {
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Honorarios = new DataTable();
        DataTable Dt_Arrendamientos = new DataTable();
        DataTable Dt_Hon_Asimilables = new DataTable();
        DataTable Dt_Deducciones = new DataTable();
        DataTable Dt_Datos_Deduccones = new DataTable();
        DataRow Registro_Deduccion;
        DataTable Dt_Cuenta_Empleado = new DataTable();
        DataTable Dt_Deducciones_Persepciones = new DataTable();
        Decimal Total_Partidas = 0;
        String Beneficiario_ID = "";
        String Tipo_Beneficiario = ""; 
        Decimal Total_Debe = 0;
        Decimal Total_Monto = 0;
        Decimal Total_Haber = 0;
        int partida = 0;
        DataRow row;
        String Resultado = "SI";
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            if (Dt_Deducciones.Rows.Count == 0)
            {
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Partida_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Fte_Financiamiento_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Area_Funcional_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Debe, typeof(System.Double));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Haber, typeof(System.Double));
            }
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Detalles_Solicitud();
            if (Dt_Datos_Polizas.Rows.Count > 0)
            {
                //Se consultan las deducciones y persepciones de la solicitud de pago
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                Dt_Datos_Deduccones = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud_Finiquito();
                if (Dt_Datos_Deduccones.Rows.Count > 0)
                {
                    foreach (DataRow Fila_Deduccion in Dt_Datos_Deduccones.Rows)
                    {
                        Registro_Deduccion = Dt_Deducciones.NewRow();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Partida_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Partida_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Fte_Financiamiento_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Fte_Financiamiento_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Proyecto_Programa_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Proyecto_Programa_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Area_Funcional_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Area_Funcional_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString().Trim();
                        Dt_Deducciones.Rows.Add(Registro_Deduccion); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Deducciones.AcceptChanges();
                    }
                }
            }
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
            if (Dt_Datos_Polizas.Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Dt_Datos_Polizas.Rows[0]["Monto"].ToString()))
                {
                    Total_Partidas = Convert.ToDecimal(Dt_Datos_Polizas.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                    Txt_Monto_Solicitud.Value = Dt_Datos_Polizas.Rows[0]["Monto"].ToString();
                    Dt_Cuenta_Empleado = Rs_Parametros_Iva.Obtener_Parametros_Poliza();//se optiene la cuenta del empleado
                    if (Dt_Cuenta_Empleado.Rows.Count > 0)
                    {
                        Txt_Cuenta_Contable_ID_Empleado.Value = Dt_Cuenta_Empleado.Rows[0][Cat_Nom_Parametros_Poliza.Campo_Cuenta_Servicios_Profecionales_Pagar_A_CP].ToString();
                    }
                    else
                    {
                        Txt_Cuenta_Contable_ID_Empleado.Value = "";
                    }
                    Beneficiario_ID = Dt_Datos_Polizas.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString();
                    Tipo_Beneficiario = "EMPLEADO";
                    Txt_Concepto_Solicitud.Value = Dt_Datos_Polizas.Rows[0]["Concepto"].ToString();
                    Txt_No_Reserva.Value = Dt_Datos_Polizas.Rows[0]["NO_RESERVA"].ToString();
                }
            }
            if (Txt_Cuenta_Contable_ID_Empleado.Value != "")
            {
                //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                if (Dt_Partidas_Polizas.Rows.Count == 0)
                {
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
                    Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                }
                ///Se consultan los detalles de las deducciones y persepciones del finiquito
                Dt_Deducciones_Persepciones = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consultar_Datos_Solicitud_Finiquito();
                if (Dt_Deducciones_Persepciones.Rows.Count > 0)
                {
                    foreach (DataRow Fila_D_P in Dt_Deducciones_Persepciones.Rows)
                    {
                        if (Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString()) != 0)
                        {
                            partida = partida + 1;
                            Total_Debe = Total_Debe + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString());
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID].ToString();
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Solicitud.Value;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString());
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        else
                        {
                            partida = partida + 1;
                            Total_Haber = Total_Haber + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString());
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID].ToString();
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Solicitud.Value;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString());
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
                    Total_Monto = Total_Debe - Total_Haber;
                    row = Dt_Partidas_Polizas.NewRow();
                    partida = partida + 1;
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Empleado.Value.ToString()))
                    {
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Empleado.Value;
                    }
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Solicitud.Value;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Monto;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row["Beneficiario_ID"] = Beneficiario_ID;
                    row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(partida);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Convert.ToDouble(Txt_Monto_Solicitud.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto = "CANCELACION-" + No_Solicitud_Pago + "-" + Txt_Concepto_Solicitud.Value;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentarios_Ejercido = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva_Anterior = Convert.ToDouble(Txt_No_Reserva.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Resultado = Rs_Modificar_Ope_Con_Solicitud_Pagos.Modificar_Solicitud_Finiquito(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
            }
            else
            {
                Resultado = "NO,PE";
            }
                return Resultado;
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO  : 21-Febrero-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    private void Abrir_Ventana(String Nombre_Archivo)
    {
        String Pagina = "../../Reporte/"; //"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
        try
        {
            Pagina = Pagina + Nombre_Archivo;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
        }
    }
    #endregion
    #region generar caratula
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String Numero_Solicitud)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        try
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            Ds_Reporte = new DataSet();
            Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
            Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            Lbl_Mensaje_Error.Visible = true;
        }

    }
    #endregion
    #region (Control Acceso Pagina)
    ///*******************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 30/Septiembre/2011
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    ///*******************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            //Botones.Add(Btn_Nuevo);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Accion"]))
                {
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 30/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }

    #endregion
    protected void Btn_Cancelar_Click(object sender, EventArgs e)
    {
        try
        {
            Inicializa_Controles();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Solicitud_Click
    ///DESCRIPCIÓN: manda llamar el metodo de la impresion de la caratula 
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO  : 21-mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Solicitud_Click(object sender, EventArgs e)
    {
        String No_Solicitud = ((LinkButton)sender).Text;
        Imprimir(No_Solicitud);
    }
    protected void Btn_Comentar_Click(object sender, EventArgs e)
    {
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Tipo_Solicitud = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Solcitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
        DataTable Dt_Datos_Polizas = new DataTable();
        DataTable Dt_Tipo_Solicitud = new DataTable();
        Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
        ReportDocument Reporte = new ReportDocument();
        String Servicios_Generales = "";
        DataTable Dt_solicitud = new DataTable();
        String Tipo_Solicitud = "";
        String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
        String Nombre_Archivo = "Rechazado";// +Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
        String Usuario = ""; 
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        String Resultado = "";
        // crear transaccion para crear el convenio 
        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
        Cn.Open();
        Trans = Cn.BeginTransaction();
        Cmmd.Connection = Cn;
        Cmmd.Transaction = Trans;
        try
        {
            DataRow Row;
            DataTable Dt_Reporte = new DataTable();

            if (Txt_Comentario.Text != "")
            {
                Rs_Solcitud_Pago.P_No_Solicitud_Pago = Txt_No_Solicitud_Autorizar.Value;
                Rs_Solcitud_Pago.P_Cmmd = Cmmd;
                Dt_solicitud = Rs_Solcitud_Pago.Consulta_Datos_Solicitud_Pago();
                //revisar si la solicitud es de servicios masivos generales 
                if (!String.IsNullOrEmpty(Dt_solicitud.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString().Trim()))
                {
                    Servicios_Generales = "SI";
                }
                else
                {
                    Servicios_Generales = "NO";
                }
                DataTable Dt_Tipo = new DataTable();
                Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                Rs_Consulta.P_Cmmd = Cmmd;
                Dt_Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos();
                if (Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().ToUpper() != "FINIQUITO" || Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().Substring(0, 4).ToUpper() != "FINI")
                {
                    if (Servicios_Generales == "SI")
                    {
                        Resultado = Cancela_Solicitud_Pago_Servicios_Generales(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(), Cmmd);
                    }
                    else
                    {
                        Resultado = Cancela_Solicitud_Pago(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(), Cmmd);
                    }
                   
                }
                else
                {
                    Resultado = Cancela_Solicitud_Pago_Finiquito(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(), Cmmd);
                
                }
                if (Resultado == "SI")
                {
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud_Autorizar.Value;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                    Trans.Commit();
                    //  se crea la tabla para el reporte de la cancelacion
                    Dt_Reporte.Columns.Add("NO_SOLICITUD", typeof(System.String));
                    Dt_Reporte.Columns.Add("NO_RESERVA", typeof(System.String));
                    Dt_Reporte.Columns.Add("PROVEEDOR", typeof(System.String));
                    Dt_Reporte.Columns.Add("MONTO", typeof(System.Double));
                    Dt_Reporte.Columns.Add("FECHA_CREO", typeof(System.DateTime));
                    Dt_Reporte.Columns.Add("FECHA_RECHAZO", typeof(System.DateTime));
                    Dt_Reporte.Columns.Add("TIPO_SOLICITUD_PAGO_ID", typeof(System.String));
                    Dt_Reporte.Columns.Add("CONCEPTO_SOLICITUD", typeof(System.String));
                    Dt_Reporte.Columns.Add("COMENTARIO", typeof(System.String));
                    Dt_Reporte.Columns.Add("USUARIO_CREO", typeof(System.String));
                    Dt_Reporte.Columns.Add("USUARIO_RECHAZO", typeof(System.String));
                    Dt_Reporte.TableName = "Dt_Cancelacion";


                    foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
                    {
                        Row = Dt_Reporte.NewRow();

                        Row["NO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString());
                        Row["NO_RESERVA"] = (Registro["Reserva"].ToString());
                        if (!String.IsNullOrEmpty(Registro["Proveedor"].ToString()))
                        {
                            Row["PROVEEDOR"] = (Registro["Empleado"].ToString());
                        }
                        else
                        {
                            Row["PROVEEDOR"] = (Registro["Proveedor"].ToString());
                        }
                        Row["MONTO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                        Row["FECHA_CREO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo].ToString());
                        Row["FECHA_RECHAZO"] = "" + DateTime.Now;

                        //  para el tipo de solicitud
                        Rs_Tipo_Solicitud.P_Tipo_Solicitud = (Registro[Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString());
                        Dt_Tipo_Solicitud = Rs_Tipo_Solicitud.Consulta_Tipo_Solicitud();

                        foreach (DataRow Tipo in Dt_Tipo_Solicitud.Rows)
                        {
                            Tipo_Solicitud = (Tipo[Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion].ToString());
                        }
                        Row["TIPO_SOLICITUD_PAGO_ID"] = Tipo_Solicitud;
                        Row["CONCEPTO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString());
                        Row["COMENTARIO"] = Txt_Comentario.Text;
                        Usuario = (Registro[Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo].ToString());
                        Usuario = Usuario.Replace(".", "");
                        Row["USUARIO_CREO"] = Usuario;
                        Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                        Usuario = Usuario.Replace(".", "");
                        Row["USUARIO_RECHAZO"] = Usuario;
                        Dt_Reporte.Rows.Add(Row);
                        Dt_Reporte.AcceptChanges();
                    }
                    Ds_Reporte.Clear();
                    Ds_Reporte.Tables.Clear();
                    Ds_Reporte.Tables.Add(Dt_Reporte.Copy());
                    Reporte.Load(Ruta_Archivo + "Rpt_Con_Cancelacion_Recepcion.rpt");
                    Reporte.SetDataSource(Ds_Reporte);
                    DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                    Nombre_Archivo += ".pdf";
                    Ruta_Archivo = @Server.MapPath("../../Reporte/");
                    m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                    ExportOptions Opciones_Exportacion = new ExportOptions();
                    Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);

                    Abrir_Ventana(Nombre_Archivo);
                    Inicializa_Controles();
                }
                else
                {
                    Trans.Rollback();
                    
                    Lbl_Mensaje_Error.Text = "Error:";
                    Lbl_Mensaje_Error.Text = "No tienes suficiencia presupuestal para realizar la cancelacion";
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";

            }
        }
        catch (SqlException Ex)
        {
            Trans.Rollback();
            Lbl_Mensaje_Error.Text = "Error:";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
            Img_Error.Visible = true;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = "Error:";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
            Img_Error.Visible = true;
        }
        finally
        {
            Cn.Close();
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Buscar_No_Solicitud_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Resultado = new DataTable();
        String No_Autoriza_Ramo33 = "";
        String No_Autoriza_Ramo33_2 = "";
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
            DataTable Dt_Usuario = new DataTable();
            Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
            if (Dt_Usuario.Rows.Count > 0)
            {
                No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
            }
            if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
            {
                Rs_Consultar_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
                Rs_Consultar_Solicitud_Pagos.P_Comentario = "EJERCIDO";
                if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                {
                    Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                }
                else
                {
                    Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                }
                Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
            }
            else
            {
                Rs_Consultar_Solicitud_Pagos.P_Estatus = "PORPAGAR";
                if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                {
                    Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                }
                else
                {
                    Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                }
                Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes();
            }
            if (Dt_Resultado.Rows.Count <= 0)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la busqueda <br />";
                Txt_Busqueda_No_Solicitud.Focus();
            }
            else
            {
                Grid_Solicitud_Pagos.Columns[4].Visible = true;
                Grid_Solicitud_Pagos.Columns[2].Visible = true;
                Grid_Solicitud_Pagos.DataSource = Dt_Resultado;
                Grid_Solicitud_Pagos.DataBind();
                Grid_Solicitud_Pagos.Columns[4].Visible = false;
                Grid_Solicitud_Pagos.Columns[2].Visible = false;
                Txt_Busqueda_No_Solicitud.Text = "";
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Txt_Buscar_No_Solicitud_TextChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Resultado = new DataTable();
        String No_Autoriza_Ramo33 = "";
        String No_Autoriza_Ramo33_2 = "";
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
            DataTable Dt_Usuario = new DataTable();
            Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
            if (Dt_Usuario.Rows.Count > 0)
            {
                No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
            }
            if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
            {
                Rs_Consultar_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
                Rs_Consultar_Solicitud_Pagos.P_Comentario = "EJERCIDO";
                if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                {
                    Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                }
                else
                {
                    Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                }
                Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
            }
            else
            {
                Rs_Consultar_Solicitud_Pagos.P_Estatus = "PORPAGAR";
                if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                {
                    Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                }
                else
                {
                    Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                }
                Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes();
            }
            if (Dt_Resultado.Rows.Count <= 0)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la busqueda <br />";
                Txt_Busqueda_No_Solicitud.Focus();
            }
            else
            {
                Grid_Solicitud_Pagos.Columns[4].Visible = true;
                Grid_Solicitud_Pagos.Columns[2].Visible = true;
                Grid_Solicitud_Pagos.DataSource = Dt_Resultado;
                Grid_Solicitud_Pagos.DataBind();
                Grid_Solicitud_Pagos.Columns[4].Visible = false;
                Grid_Solicitud_Pagos.Columns[2].Visible = false;
                Txt_Busqueda_No_Solicitud.Text = "";
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    #region"Grid"
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Solicitud_Pagos_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 22/Diciembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Solicitud_Pagos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
        DataTable Dt_Detalles = new DataTable();
        DataTable Dt_Documentos = new DataTable();

        try
        {
            if (Grid_Solicitud_Pagos.SelectedIndex > (-1))
            {
                Solicitud_Negocio.P_No_Solicitud_Pago = Grid_Solicitud_Pagos.SelectedRow.Cells[2].Text.Trim();
                Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
                Dt_Documentos = Solicitud_Negocio.Consulta_Documentos();


                if (Dt_Detalles != null)
                {
                    if (Dt_Detalles.Rows.Count > 0)
                    {
                        Txt_No_Pago_Det.Text = Dt_Detalles.Rows[0]["NO_SOLICITUD_PAGO"].ToString().Trim();
                        Txt_No_Reserva_Det.Text = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString().Trim();
                        Txt_Concepto_Reserva_Det.Text = Dt_Detalles.Rows[0]["CONCEPTO_RESERVA"].ToString().Trim();
                        Txt_Beneficiario_Det.Text = Dt_Detalles.Rows[0]["BENEFICIARIO"].ToString().Trim();
                        Txt_Fecha_Solicitud_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_SOLICITUD"]);
                        Txt_Monto_Solicitud_Det.Text = String.Format("{0:c}", Dt_Detalles.Rows[0]["MONTO"]);
                        Txt_Fecha_Autoriza_Director_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZO_RECHAZO_JEFE"]);
                        Txt_Fecha_Recepcion_Doc_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECEPCION_DOCUMENTOS"]);
                        Txt_Recibio_Documentacion_Fisica.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_DOC_FISICA"].ToString().Trim();
                        Txt_Fecha_Recibio_Documentacion_Fisica.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECEPCION_DOCUMENTOS"]);
                        Txt_Aut_Documentacion_Fisica.Text = Dt_Detalles.Rows[0]["USUARIO_AUTORIZO_DOCUMENTOS"].ToString().Trim();
                        Txt_Fecha_Recepcion_Doc_Contabilidad.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECIBIO_CONTABILIDAD"]);
                        Txt_Recibio_Documentacion_Contabilidad.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_CONTABILIDAD"].ToString().Trim();
                        Txt_Aut_Contabilidad.Text = Dt_Detalles.Rows[0]["USUARIO_AUTORIZO_CONTABILIDAD"].ToString().Trim();
                        Txt_Fecha_Aut_Contabilidad.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZO_RECHAZO_CONTABI"]);
                        Txt_No_poliza_Det.Text = Dt_Detalles.Rows[0]["No_Poliza"].ToString().Trim();
                        Txt_Recibio_Documentacion_Ejercido.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_EJERCIDO"].ToString().Trim();
                        Txt_Fecha_Recepcion_Doc_Ejercido.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECIBIO_EJERCIDO"]);
                        Grid_Solicitud_Pagos.SelectedIndex = -1;

                        Grid_Documentos.Columns[3].Visible = true;
                        Grid_Documentos.Columns[4].Visible = true;
                        Grid_Documentos.DataSource = Dt_Documentos;
                        Grid_Documentos.DataBind();
                        Grid_Documentos.Columns[3].Visible = false;
                        Grid_Documentos.Columns[4].Visible = false;
                        Mpe_Detalles.Show();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
        }
    }
    protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        HyperLink Hyp_Lnk_Ruta;
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                if (!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) && e.Row.Cells[3].Text.Trim() != "&nbsp;")
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[3].Text.Trim();
                    Hyp_Lnk_Ruta.Enabled = true;
                }
                else
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "";
                    Hyp_Lnk_Ruta.Enabled = false;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Solicitud_Pagos_RowDataBound
    /// DESCRIPCION : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 09/enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Solicitud_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            CheckBox chek = (CheckBox)e.Row.FindControl("Chk_Autorizado");
            CheckBox chek2 = (CheckBox)e.Row.FindControl("Chk_Rechazado");
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[9].Text != "PORPAGAR")
                {
                    chek.Enabled = false;
                    chek2.Enabled = false;
                }
                else
                {
                    chek.Enabled = true;
                    chek2.Enabled = true;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #endregion
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Poliza_Click
    /// DESCRIPCION : obtiene los datos de la poliza que se imprimira
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 09/Noviembre/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Poliza_Click(object sender, EventArgs e)
    {
        String mes_ano = ((LinkButton)sender).CommandArgument;
        String No_poliza = ((LinkButton)sender).Text;
        String Tipo_Poliza_ID=((LinkButton)sender).ToolTip;
        String No_Solicitud = ((LinkButton)sender).CssClass;
        Imprimir(No_poliza, Tipo_Poliza_ID, mes_ano, null, false, No_Solicitud);
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Imprimir
    /// DESCRIPCION : obtiene los datos de la poliza que se imprimira
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 09/Noviembre/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Imprimir(String NO_POLIZA, String TIPO_POLIZA, String mes_ano, DataTable Dt_Consulta, Boolean Estado, String Solicitud)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Ejercido = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();
        DataTable Dt_Beneficiario = new DataTable();
        try
        {
            if (Estado == true)
            {
                Ds_Reporte = new DataSet();
                if (Dt_Consulta.Rows.Count > 0)
                {
                    Dt_Consulta.TableName = "Dt_Cancelacion";
                    Ds_Reporte.Tables.Add(Dt_Consulta.Copy());
                    //Se llama al método que ejecuta la operación de generar el reporte.
                    Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Cancelacion_Recepcion.rpt", "Poliza_Cancelada" + NO_POLIZA, ".pdf");
                }
            }
            else
            {
                Cls_Ope_Con_Polizas_Negocio Poliza = new Cls_Ope_Con_Polizas_Negocio();
                Rs_Ejercido.P_No_Solicitud_Pago = Solicitud;
                Dt_Beneficiario=Rs_Ejercido.Consulta_Solicitud_Pago_Completa();
                Ds_Reporte = new DataSet();
                Poliza.P_No_Poliza = NO_POLIZA;
                Poliza.P_Tipo_Poliza_ID = TIPO_POLIZA;
                Poliza.P_Mes_Ano = mes_ano;
                Dt_Pagos = Poliza.Consulta_Detalle_Poliza();
                if (Dt_Pagos.Rows.Count > 0)
                {
                    foreach (DataRow registro in Dt_Pagos.Rows)
                    {
                        registro.BeginEdit();
                        if (!String.IsNullOrEmpty(Dt_Beneficiario.Rows[0]["PROVEEDOR_ID"].ToString()))
                        {
                            registro["BENEFICIARIO"] =" (" + Dt_Beneficiario.Rows[0]["PROVEEDOR_ID"].ToString() + ")" + Dt_Beneficiario.Rows[0]["COMPANIA"].ToString();
                             registro["CUENTA_DE_PAGO"] ="";
                             registro["CUENTA_A_PAGAR"] ="";
                        }
                        else
                        {
                            registro["BENEFICIARIO"] = Dt_Beneficiario.Rows[0]["COMPANIA"].ToString();
                            registro["CUENTA_DE_PAGO"] = "";
                            registro["CUENTA_A_PAGAR"] = "";
                        }
                        registro.EndEdit();
                        registro.AcceptChanges();
                        Dt_Pagos.AcceptChanges();
                        registro.AcceptChanges();
                        Dt_Pagos.AcceptChanges();
                    }
                    Dt_Pagos.TableName = "Dt_Datos_Poliza";
                    Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                    //Se llama al método que ejecuta la operación de generar el reporte.
                    Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Poliza.rpt", "Poliza" + NO_POLIZA, ".pdf");
                }
            }
        }
        //}
        catch (Exception Ex)
        {
            //Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            //Lbl_Mensaje_Error.Visible = true;
        }

    }
    #region Metodos Reportes
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    
    protected void Mostrar_Reporte(String Nombre_Reporte)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
}
