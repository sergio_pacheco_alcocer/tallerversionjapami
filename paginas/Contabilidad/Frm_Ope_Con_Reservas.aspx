﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Reservas.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Reservas" Title="Reservas" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript">
        function Grid_Anidado(Control, Fila)
        {
            var div = document.getElementById(Control); 
            var img = document.getElementById('img' + Control);
            
            if (div.style.display == "none") 
            {
                div.style.display = "inline";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                else {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                img.alt = "Contraer Registros";
            }
            else 
            {
                div.style.display = "none";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                else {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                img.alt = "Expandir Registros";
            }
        }
        function on(ctrl) {
            var disponible = parseFloat(ctrl.title.split(":")[1].replace(/,/gi, ''));
            var valor = parseFloat(ctrl.value.replace(/,/gi, ''));

            if (valor > disponible) {
                $('input[id$=Txt_Importe_Partida]').val('');
                alert('El importe no puede ser mayor al disponible!!');
            }
        }
        function onix(ctrl) {
            var disponible = parseFloat(ctrl.title.split(":")[1].replace(/,/gi , ''));
            var valor = parseFloat(ctrl.value.replace(/,/gi, ''));

            if (valor > disponible) {
                $('input[id$=Txt_Importe_Comprometido]').val('');
                alert('El importe no puede ser mayor al disponible!!');
            }
        }
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Polizas" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" >
        <ContentTemplate>        
            <asp:UpdateProgress ID="Uprg_Polizas" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                       <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                       <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                       </div>                       
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Compromisos" style="background-color:#ffffff; width:98%; height:100%;">
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Reservas</td>
                    </tr>
                    <tr>
                        <td align="left">&nbsp;
                            <asp:UpdatePanel ID="Upnl_Mensajes_Error" runat="server" UpdateMode="Always" RenderMode="Inline">
                                <ContentTemplate>                         
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"/>
                                </ContentTemplate>                                
                            </asp:UpdatePanel>                                      
                        </td>
                    </tr> 
                </table>

                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td>                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;"> 
                                            <asp:UpdatePanel ID="Upnl_Botones_Operacion" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
                                                <ContentTemplate> 
                                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                        CssClass="Img_Button" TabIndex="1"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                        onclick="Btn_Nuevo_Click" />
                                                    <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" 
                                                        CssClass="Img_Button" TabIndex="2"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                                                        onclick="Btn_Modificar_Click" OnClientClick="return confirm('&iquest;Esta seguro de Modificar la Reserva?');"   />
                                                    <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                        CssClass="Img_Button" TabIndex="4"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                        onclick="Btn_Salir_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>                                                
                                        </td>
                                        <td align="right" style="width:41%;">                                  
                                        </td>       
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>           
                        <div id="Div_Reserva_Datos" runat="server">
                        <asp:Panel ID="Pnl_Datos_Generales" runat="server" GroupingText="Datos de Reserva"
                        Width="98%">
                            <table  width="100%" border="0">
                                <tr><td colspan="7">&nbsp;</td> </tr>
                                <tr id='Tr_Busqueda' runat="server">
                                    <td  style=" width:20%"> &nbsp; &nbsp;
                                        <asp:Label ID="Lbl_Fecha_Inicio" runat="server" Text="Fecha Inicio"></asp:Label>
                                    </td>
                                    <td style=" width:20%">
                                        <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="70%" TabIndex="6" MaxLength="11" />
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_inicio"/>
                                         <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Inicio" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Inicio" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="Mev_Txt_Fecha_Poliza" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Inicio"
                                            ControlExtender="Mee_Txt_Fecha_Inicio" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Inicio Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>  
                                    </td>
                                    <td style="width:10%">
                                        <asp:Label ID="Lbl_Fecha_Final" runat="server" Text="Fecha Final"></asp:Label>
                                    </td>
                                    <td style="width:20%">
                                        <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="70%" TabIndex="6" MaxLength="11"  />
                                    <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Final" runat="server" 
                                        TargetControlID="Txt_Fecha_Final" WatermarkCssClass="watermarked" 
                                        WatermarkText="Dia/Mes/Año" Enabled="True" />
                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Final" runat="server" 
                                        TargetControlID="Txt_Fecha_Final" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Final"/>
                                     <asp:ImageButton ID="Btn_Fecha_Final" runat="server"
                                        ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                        Height="18px" CausesValidation="false"/>           
                                    <cc1:MaskedEditExtender 
                                        ID="Mee_Txt_Fecha_Final" 
                                        Mask="99/LLL/9999" 
                                        runat="server"
                                        MaskType="None" 
                                        UserDateFormat="DayMonthYear" 
                                        UserTimeFormat="None" Filtered="/"
                                        TargetControlID="Txt_Fecha_Final" 
                                        Enabled="True" 
                                        ClearMaskOnLostFocus="false"/>  
                                    <cc1:MaskedEditValidator 
                                        ID="MaskedEditValidator1" 
                                        runat="server" 
                                        ControlToValidate="Txt_Fecha_Final"
                                        ControlExtender="Mee_Txt_Fecha_Final" 
                                        EmptyValueMessage="Fecha Requerida"
                                        InvalidValueMessage="Fecha Final Invalida" 
                                        IsValidEmpty="false" 
                                        TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                        Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>  
                                    </td>
                                    <td style=" width:10%">
                                        <asp:Label ID="Lbl_Folio" runat="server" Text="No. Reserva"></asp:Label>
                                    </td>
                                    <td style=" width:15%">                                        
                                        <asp:TextBox ID="Txt_No_Folio" runat="server" Width="95%" AutoPostBack="true" OnTextChanged="Txt_No_Folio_OnTextChanged"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="Txt_No_Folio_TextBoxWatermarkExtender" runat="server"
                                        TargetControlID="Txt_No_Folio" WatermarkCssClass="watermarked" WatermarkText="<No. Reserva>"
                                        Enabled="True" />
                                        <cc1:FilteredTextBoxExtender  ID="Txt_No_Folio_FilteredTextBoxExtender" 
                                          runat="server" FilterType="Numbers" TargetControlID="Txt_No_Folio">
                                        </cc1:FilteredTextBoxExtender>
                                        
                                    </td>
                                    <td style="width:5%">
                                        <asp:ImageButton ID="Btn_Buscar_Reserva" runat="server" 
                                            ToolTip="Consultar" TabIndex="6" onclick="Btn_Buscar_Reserva_Click"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                            />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    &nbsp;
                                    &nbsp;
                                        <asp:Label ID="Lbl_Estatus" runat="server" Text="*Estatus"></asp:Label>
                                    </td>
                                    <td colspan = "2">
                                        <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="94%" >
                                        <asp:ListItem Value="GENERADA">GENERADA</asp:ListItem>
                                        <asp:ListItem Value="CANCELADA">CANCELADA</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td >*Tipo de Solicitud</td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="Cmb_Tipo_Solicitud_Pago" runat="server" TabIndex="6" Width="95%"
                                        AutoPostBack = "true" OnSelectedIndexChanged="Cmb_Tipo_Solicitud_Pago_SelectedIndexChanged"/> 
                                    </td>
                                </tr>
                                <tr id="Tr_Proveedor" runat="server">
                                    <td id="Td_Proveedor" runat="server">&nbsp;&nbsp;
                                        <asp:Label ID="Lbl_Proveedor" runat="server" Text="*Proveedor"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" MaxLength="100" TabIndex="11" Width="80%" AutoPostBack="true" Ontextchanged="Txt_Nombre_Proveedor_Solicitud_Pago_TextChanged"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" TargetControlID="Txt_Nombre_Proveedor_Solicitud_Pago"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                        <asp:ImageButton ID="Btn_Buscar_Proveedor_Solicitud_Pagos" 
                                            runat="server" ToolTip="Consultar"
                                            TabIndex="12" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                            onclick="Btn_Buscar_Proveedor_Solicitud_Pagos_Click"/>
                                    </td>
                                    <td colspan="4">
                                        <asp:DropDownList ID="Cmb_Proveedor_Solicitud_Pago" runat="server" TabIndex="13" Width="97%" />
                                    </td>
                                </tr>
                                <tr id="Tr_Deudor" runat="server">
                                    <td>&nbsp;&nbsp;
                                        <asp:Label ID="Lbl_Deudor" runat="server" Text="*Deudor"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="Txt_Deudor" runat="server" MaxLength="100" TabIndex="11" Width="80%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="Txt_Deudor"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                        <asp:ImageButton ID="Btn_Buscar_Deudor" 
                                            runat="server" ToolTip="Consultar"
                                            TabIndex="12" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                            onclick="Btn_Buscar_Deudor_Click"/>
                                    </td>
                                    <td colspan="4">
                                        <asp:DropDownList ID="Cmb_Deudores" runat="server" TabIndex="13" Width="97%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Deudor_OnSelectedIndexChanged" />
                                    </td>
                                </tr>
                                <tr id="Tr_Deudor_Anticipos" runat="server">
                                    <td>&nbsp;&nbsp;
                                        <asp:Label ID="Lbl_Anticipos" runat="server" Text="*Gastos"></asp:Label>
                                    </td>
                                    <td colspan="6">
                                        <asp:DropDownList ID="Cmb_anticipos" runat="server" TabIndex="13" Width="98%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Anticipos_OnSelectedIndexChanged" />
                                    </td>
                                </tr>
                                <tr id="Tr_Deudor_Anticipos_2" runat="server">
                                    <td>&nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Monto" runat="server" Text="*Monto"></asp:Label></td>
                                     <td colspan="2">
                                        <asp:TextBox ID="Txt_Gastos_Anticipos" runat="server" MaxLength="100" TabIndex="11" Width="94%" ToolTip="Importe" ReadOnly="true"></asp:TextBox>
                                    </td>
                                    <td>&nbsp;&nbsp;
                                        <asp:Label ID="Lbl_Factura_Solicitud" runat="server" Text="*No Documento"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_No_Factura_Solicitud" runat="server" MaxLength="20" Width="94%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Factura_Solicitud" runat="server"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" InvalidChars="'"
                                            TargetControlID="Txt_No_Factura_Solicitud" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr id="Tr_Deudor_Anticipos_3" runat="server">
                                    <td>
                                    &nbsp;&nbsp;
                                         <asp:Label ID="Lbl_Monto_Factura" runat="server" Text="*Total Factura"></asp:Label>
                                    </td>
                                    <td colspan="2" >
                                       <asp:TextBox ID="Txt_Monto_Factura" runat="server" MaxLength="20" Width="94%" ></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filtered_Txt_Monto_Factura" runat="server"
                                            FilterType="Custom, Numbers"  ValidChars="."
                                            TargetControlID="Txt_Monto_Factura">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>&nbsp;&nbsp;
                                        <asp:Label ID="Lbl_Fecha_Factura_Solicitud" runat="server" Text="*Fecha Factura"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Fecha_Factura_Solicitud" runat="server" Enabled="false" Width="75%"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Factura_Solicitud" runat="server" 
                                            TargetControlID="Txt_Fecha_Factura_Solicitud" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Img_Btn_Fecha_Factura_Solicitud"/>
                                        <asp:ImageButton ID="Img_Btn_Fecha_Factura_Solicitud" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>
                                            &nbsp;&nbsp;
                                        <asp:ImageButton ID="Btn_Agregar_Documento" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_add.png"
                                            CssClass="Img_Button" ToolTip="Agregar"  Height="15px" Width="15px"  AutoPostBack="true"  OnClick="Btn_Agregar_Documento_Click" />
                                    </td>
                                </tr>
                                <tr id="Tr_Grid_Facturas" runat="server">
                                    <td colspan ="7" align="center">
                                        <asp:Panel ID="Pnl_Facturas_Agregadas" runat="server" GroupingText="Facturas de Comrpobación" Width="98%">
                                            <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                                <tr align="center">
                                                    <td>
                                                        <div style="overflow:auto;width:98%;height:80px;vertical-align:top;">
                                                            <asp:GridView ID="Grid_Documentos" runat="server" Width="97%"
                                                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                                                 OnRowDataBound="Grid_Documentos_RowDataBound">
                                                                <Columns>
                                                                       <asp:BoundField DataField="Partida" HeaderText="Partida" ItemStyle-Font-Size="X-Small">
                                                                       <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                       <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                       </asp:BoundField>
                                                                       <asp:BoundField DataField="No_Documento" HeaderText="Documento" ItemStyle-Font-Size="X-Small">
                                                                         <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                                         <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                                       </asp:BoundField>
                                                                       <asp:BoundField DataField="Fecha_Documento" HeaderText="Fecha" DataFormatString="{0:dd/MMMM/yyyy}" ItemStyle-Font-Size="X-Small">
                                                                         <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                                         <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                                       </asp:BoundField>
                                                                       <asp:BoundField DataField="Monto" HeaderText="Monto" DataFormatString="{0:c}" ItemStyle-Font-Size="X-Small">
                                                                       <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                                                       <ItemStyle HorizontalAlign="Center" Width="20%" />
                                                                       </asp:BoundField>
                                                                       <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <center>
                                                                                <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                                                    CausesValidation="false" 
                                                                                    ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                                                    OnClientClick="return confirm('¿Está seguro de eliminar de la tabla la cuenta seleccionada?');" OnClick="Btn_Eliminar_Partida" />
                                                                            </center>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <SelectedRowStyle CssClass="GridSelected" />
                                                                <PagerStyle CssClass="GridHeader" />
                                                                <HeaderStyle CssClass="tblHead" />
                                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                            </asp:GridView> 
                                                       </div>                            
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="Tr_Empleado" runat="server">
                                    <td>&nbsp;&nbsp;
                                        <asp:Label ID="Lbl_Empleado" runat="server" Text="*Empleado"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="Txt_Nombre_Empleado" runat="server" MaxLength="100" TabIndex="11" Width="80%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Empleado" runat="server" TargetControlID="Txt_Nombre_Empleado"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                        <asp:ImageButton ID="Btn_Buscar_Empleado" 
                                            runat="server" ToolTip="Consultar"
                                            TabIndex="12" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                            onclick="Btn_Buscar_Empleado_Click" />
                                    </td>
                                    <td colspan="4">
                                        <asp:DropDownList ID="Cmb_Nombre_Empleado" runat="server" TabIndex="13" Width="97%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;
                                        <asp:Label ID="Lbl_Tipo_Reserva" runat="server" Text="*Tipo de reserva"></asp:Label>
                                    </td>
                                     <td colspan="2">
                                        <asp:DropDownList ID="Cmb_Tipo_Reserva" runat="server" Width="94%">
                                            <asp:ListItem>UNICA</asp:ListItem>
                                            <asp:ListItem>ABIERTA</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>                                    
                                    <td>
                                        <asp:Label ID="Lbl_Recursos" runat="server" Text="*Recursos"></asp:Label>
                                    </td>
                                     <td colspan="3">
                                        <asp:TextBox ID="Txt_Fuente_Ramo" runat="server" Width="94%" ReadOnly="true" Text="RECURSO ASIGNADO"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;
                                        <asp:Label ID="Lbl_Unidad_Responsable" runat="server" 
                                            Text="*Unidad Responsable"></asp:Label>
                                    </td>
                                    <td colspan="6">
                                        <asp:DropDownList ID="Cmb_Unidad_Responsable_Busqueda" runat="server" 
                                            AutoPostBack="true" 
                                            OnSelectedIndexChanged="Cmb_Unidad_Responsable_Busqueda_SelectedIndexChanged" 
                                            Width="98%">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                    <tr>
                                        <td>
                                            &nbsp; &nbsp;
                                            <asp:Label ID="Lbl_Concepto" runat="server" Text="*Concepto"></asp:Label>
                                        </td>
                                        <td colspan="6">
                                            <asp:TextBox ID="Txt_Conceptos" runat="server" TextMode="MultiLine" 
                                                Width="97.5%"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="Txt_Conceptos_TextBoxWatermarkExtender" 
                                                runat="server" Enabled="True" TargetControlID="Txt_Conceptos" 
                                                WatermarkCssClass="watermarked" 
                                                WatermarkText="Ingrese sus comentarios &lt;Maximo de 255 caracteres&gt;">
                                            </cc1:TextBoxWatermarkExtender>
                                            <cc1:FilteredTextBoxExtender ID="Txt_Conceptos_FilteredTextBoxExtender" 
                                                runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Conceptos" 
                                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ ">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr ID="Tr_Div_Partidas" runat="server" visible="false">
                                        <td colspan="7" style=" width:60%; text-align:center;">
                                            <div style=" max-height:150px; overflow:auto; width:100%; vertical-align:top;">
                                                <table style="width:97%;">
                                                    <tr>
                                                        <td style="width:100%;">
                                                            <asp:GridView ID="Grid_Partidas" runat="server" AllowSorting="true" 
                                                                AutoGenerateColumns="false" CssClass="GridView_Nested" ShowHeader="true"
                                                                DataKeyNames="Partida_ID" EmptyDataText="No se encontraron partidas con saldo" 
                                                                GridLines="None" OnRowCreated="Grid_Partidas_Asignadas_Detalle_RowCreated" 
                                                                OnSorting="Grid_Partidas_Sorting" style="white-space:normal" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="Btn_Seleccionar" runat="server" 
                                                                                CommandArgument='<%#Eval("PARTIDA_ID")%>' 
                                                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="3%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="PARTIDA_ID" />
                                                                    <asp:BoundField DataField="NOMBRE" HeaderText="Partida" SortExpression="NOMBRE">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="77%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="77%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DISPONIBLE" DataFormatString="{0:#,###,##0.00}" 
                                                                        HeaderText="Disponible" SortExpression="DISPONIBLE">
                                                                        <HeaderStyle HorizontalAlign="Right" Width="20%" />
                                                                        <ItemStyle HorizontalAlign="Right" Width="20%" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                <PagerStyle CssClass="GridHeader_Nested" />
                                                                <HeaderStyle CssClass="GridHeader_Nested" />
                                                                <AlternatingRowStyle CssClass="GridAltItem_Nested" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr ID="Tr_Partidas_Codigo" runat="server">
                                        <td colspan="7" style=" width:99%; ">
                                        <table width="99%"  id="Tr_Busqueda_Partida_Especifica" runat="server">
                                         <tr style="width:99%">
                                            <td style="width:30%">
                                                <asp:Label ID="Lbl_Busqueda_Partida" runat="server" Text="Busqueda Partida"></asp:Label>
                                            </td>
                                            <td colspan="6" style="width:70%; text-align:left">
                                                <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="100" TabIndex="11" Width="75%" AutoPostBack=true OnTextChanged="Txt_Busqueda_Partida_Click"></asp:TextBox>
                                                <asp:ImageButton ID="Btn_Busqueda_Partida" runat="server" 
                                                        ToolTip="Consultar" TabIndex="6" onclick="Btn_Busqueda_Partida_Click"
                                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                                        />
                                            </td>
                                        </tr>  
                                        </table>
                                            <center>
                                                <div  id="Div_Encabezados_Partidas_Disponibles" runat="server" >
                                                    <table width="99%"  border="0" cellspacing="0">
                                                        <tr>
                                                            <td Font-Size="XX-Small"  style=" width:5%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;"> &nbsp;F.F.</td>
                                                            <td Font-Size="XX-Small" style="width:5%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Prog.</td>
                                                            <td Font-Size="XX-Small" style="width:20%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Partida</td>
                                                            <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;Aprobado</td>
                                                            <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;Disponible</td>
                                                            <td Font-Size="XX-Small" style="width:11%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;Comprometido</td>
                                                            <td Font-Size="XX-Small" style="width:11%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;&nbsp;Devengado</td>
                                                            <td Font-Size="XX-Small" style="width:13%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;Ejercido</td>
                                                            <td Font-Size="XX-Small" style="width:7%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="center">Pagado</td>
                                                            <td Font-Size="XX-Small" style="width:12%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="center">Importe</td>    
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="Div_Partidas_Disponibles" runat="server" style=" max-height:180px; overflow:auto; width:100%; vertical-align:top;">
                                                    <table style="width:100%;">
                                                        <tr>
                                                            <td style="width:100%;">
                                                                <asp:GridView ID="Grid_Partida_Saldo" runat="server"
                                                                    AutoGenerateColumns="false" CssClass=""  Font-Size="XX-Small"  ShowHeader="false"
                                                                    EmptyDataText="No se encontraron partidas con saldo" GridLines="None" 
                                                                    OnRowDataBound="Grid_Partidas_Saldo_RowDataBound" style="white-space:normal" 
                                                                    Width="100%">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="DEPENDENCIA_ID" />
                                                                        <asp:BoundField DataField="FTE_FINANCIAMIENTO_ID" />
                                                                        <asp:BoundField DataField="CLAVE_FINANCIAMIENTO">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="4%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Center" Width="4%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="PROYECTO_PROGRAMA_ID" />
                                                                        <asp:BoundField DataField="CLAVE_PROGRAMA">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="0%" />
                                                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="0%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="PARTIDA_ID" />
                                                                          <asp:BoundField DataField="PROGRAMA">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Center" Width="5%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CLAVE_PARTIDA" >
                                                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                                            <ItemStyle Font-Size="XX-Small"  HorizontalAlign="Left" Width="20%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="APROBADO" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="8%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="8%" />
                                                                        </asp:BoundField>
                                                                         <asp:BoundField DataField="DISPONIBLE" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="10%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="10%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="COMPROMETIDO" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="11%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="11%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DEVENGADO" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="11%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="11%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="EJERCIDO" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="11%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="11%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="PAGADO" DataFormatString="{0:#,###,##0.00}" >
                                                                            <HeaderStyle HorizontalAlign="Right" Width="10%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="10%" />
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle HorizontalAlign="right" Width="8%" />
                                                                            <ItemStyle HorizontalAlign="right" Width="8%" />
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="Txt_Importe_Partida" runat="server" Font-Size="X-Small" 
                                                                                    CssClass="text_cantidades_grid" name="<%=Txt_Importe_Partida.ClientID %>" 
                                                                                    onkeyup="javascript:on(this);" Width="80%"></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Importe_Grid" runat="server" 
                                                                                    Enabled="True" FilterType="Custom, Numbers" 
                                                                                    TargetControlID="Txt_Importe_Partida" ValidChars="0123456789.,">
                                                                                </cc1:FilteredTextBoxExtender>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="CAPITULO_ID" HeaderText="CAPITULO_ID" />
                                                                    </Columns>
                                                                    <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                    <PagerStyle CssClass="GridHeader_Nested" />
                                                                    <HeaderStyle Height="0px"/>
                                                                    <AlternatingRowStyle CssClass="GridAltItem_Nested" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="Div_Cabecera_Modificar" runat="server">
                                                    <table width="99%"  border="0" cellspacing="0">
                                                        <tr>
                                                            <td  style="width:20%">
                                                                <asp:Label ID="Lbl_Modificacion" runat="server" Text="Tipo Modificacion"></asp:Label>
                                                            </td>
                                                            <td colspan="6">
                                                                <asp:DropDownList ID="Cmb_Tipo_Modificacion" runat="server" Width="40%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Tipo_Modificacion_SelectedIndexChanged">
                                                                    <asp:ListItem Value="1">AMPLIACION</asp:ListItem>
                                                                    <asp:ListItem Value="2">REDUCCION</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td Font-Size="XX-Small"  style=" width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;"> &nbsp;F.F.</td>
                                                            <td Font-Size="XX-Small" style="width:17%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Prog.</td>
                                                            <td Font-Size="XX-Small" style="width:35%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Partida</td>
                                                            <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;Disponible</td>
                                                            <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;Comprometido</td>
                                                            <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="center">Monto</td>    
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div  id="Div_Partidas_A_Modificar" runat="server" style=" max-height:180px; overflow:auto; width:100%; vertical-align:top;">
                                                    <table style="width:100%;">
                                                        <tr>
                                                            <td style="width:100%;">
                                                                <asp:GridView ID="Grid_Partidas_Reserva" runat="server"
                                                                    AutoGenerateColumns="false" CssClass=""  Font-Size="XX-Small"  ShowHeader="false"
                                                                    EmptyDataText="No se encontraron partidas con saldo" GridLines="None" 
                                                                    OnRowDataBound="Grid_Partidas_d_Reserva_RowDataBound" style="white-space:normal" 
                                                                    Width="100%">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="DEPENDENCIA_ID" />
                                                                        <asp:BoundField DataField="FTE_FINANCIAMIENTO_ID" />
                                                                        <asp:BoundField DataField="CLAVE_FINANCIAMIENTO">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" Width="8%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="PROYECTO_PROGRAMA_ID" />
                                                                        <asp:BoundField DataField="CLAVE_PROGRAMA">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="14%" />
                                                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="14%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="PARTIDA_ID" />
                                                                          <asp:BoundField DataField="PROGRAMA">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="12%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Center" Width="12%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CLAVE_PARTIDA" >
                                                                            <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                                                            <ItemStyle Font-Size="XX-Small"  HorizontalAlign="Left" Width="40%" />
                                                                        </asp:BoundField>
                                                                         <asp:BoundField DataField="DISPONIBLE" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="12%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="12%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="COMPROMETIDO" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="12%" />
                                                                            <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="12%" />
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle HorizontalAlign="right" Width="12%" />
                                                                            <ItemStyle HorizontalAlign="right" Width="12%" />
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="Txt_Importe_Comprometido" runat="server" Font-Size="X-Small" 
                                                                                    CssClass="text_cantidades_grid" name="<%=Txt_Importe_Comprometido.ClientID %>" 
                                                                                    onkeyup="javascript:onix(this);" Width="80%"></asp:TextBox>
                                                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Importe_Grid" runat="server" 
                                                                                    Enabled="True" FilterType="Custom, Numbers" 
                                                                                    TargetControlID="Txt_Importe_Comprometido" ValidChars="0123456789.,">
                                                                                </cc1:FilteredTextBoxExtender>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="CAPITULO_ID" HeaderText="CAPITULO_ID" />
                                                                        <asp:BoundField DataField="NO_RESERVA" HeaderText="NO_RESERVA" />
                                                                    </Columns>
                                                                    <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                    <PagerStyle CssClass="GridHeader_Nested" />
                                                                    <HeaderStyle Height="0px"/>
                                                                    <AlternatingRowStyle CssClass="GridAltItem_Nested" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                        </td>
                                        <td colspan="2" align="right">
                                                <asp:ImageButton ID="Btn_Agregar" runat="server" 
                                                    ImageUrl="~/paginas/imagenes/gridview/add_grid.png" OnClick="Btn_Agregar_Click" 
                                                    TabIndex="19" ToolTip="Agregar" />
                                        </td>
                                    </tr>                                
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="Pnl_Partidas_Asignadas" runat="server" GroupingText="Reservas" Width="99%">
                            <div style="width:100%; height:auto; max-height: 500px; overflow:auto; vertical-align:top;">
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width:100%;">
                                          <asp:GridView ID="Grid_Partida_Asignada" runat="server" style="white-space:normal;"
                                            AutoGenerateColumns="False" GridLines="None" 
                                            Width="100%"  EmptyDataText="No se encontraron reservas" 
                                            CssClass="GridView_1" HeaderStyle-CssClass="tblHead"
                                            DataKeyNames="DEPENDENCIA_ID"   OnRowDataBound="Grid_Partida_Asignada_RowDataBound"
                                            OnRowCreated="Grid_Partidas_Asignadas_Detalle_RowCreated"
                                            HeaderStyle-Font-Size="XX-Small">
                                            <Columns>
                                                <asp:TemplateField> 
                                                  <ItemTemplate> 
                                                        <a href="javascript:Grid_Anidado('div<%# Eval("DEPENDENCIA_ID") %>', 'alt');"> 
                                                             <img id="imgdiv<%# Eval("DEPENDENCIA_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/add_up.png" /> 
                                                        </a> 
                                                  </ItemTemplate> 
                                                  <AlternatingItemTemplate> 
                                                       <a href="javascript:Grid_Anidado('div<%# Eval("DEPENDENCIA_ID") %>', 'one');"> 
                                                            <img id="imgdiv<%# Eval("DEPENDENCIA_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/add_up.png" /> 
                                                       </a> 
                                                  </AlternatingItemTemplate> 
                                                  <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="Btn_Seleccionar_Reserva" runat="server" 
                                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png" 
                                                            onclick="Btn_Seleccionar_Reserva_Click" CommandArgument='<%#Eval("NO_RESERVA")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                                </asp:TemplateField>      
                                                <asp:BoundField DataField="DEPENDENCIA_ID" />
                                                <asp:BoundField DataField="FUENTE_FINANCIAMIENTO_ID" />
                                                <asp:BoundField DataField="PROGRAMA_ID" />
                                                <asp:BoundField DataField="EMPLEADO_ID" />
                                                <asp:BoundField DataField="PROVEEDOR_ID" />
                                                <asp:BoundField DataField="NO_RESERVA" HeaderText="No. Reserva" >
                                                    <HeaderStyle HorizontalAlign="Left"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="XX-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" >
                                                    <HeaderStyle HorizontalAlign="Left"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small"/>
                                                </asp:BoundField>
                                               <asp:BoundField DataField="FUENTE_FINANCIAMIENTO" HeaderText="Fuente Financiamiento">
                                                    <HeaderStyle HorizontalAlign="Left"/>
                                                    <ItemStyle HorizontalAlign="Left"  Width="6%" Font-Size="XX-Small" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PROGRAMA" HeaderText="Programa">
                                                    <HeaderStyle HorizontalAlign="Left"/>
                                                    <ItemStyle HorizontalAlign="Left"  Width="17%" Font-Size="XX-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="BENEFICIARIO" HeaderText="Beneficiario">
                                                    <HeaderStyle HorizontalAlign="Left"/>
                                                    <ItemStyle HorizontalAlign="Left"  Width="20%" Font-Size="XX-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto">
                                                    <HeaderStyle HorizontalAlign="Left"/>
                                                    <ItemStyle HorizontalAlign="Left"  Width="20%" Font-Size="XX-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TOTAL" HeaderText="Total" DataFormatString="{0:#,###,##0.00}">
                                                   <HeaderStyle HorizontalAlign="Center"/>
                                                    <ItemStyle HorizontalAlign="Right"  Width="10%" Font-Size="XX-Small"/>
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                   <ItemTemplate>
                                                     </td>
                                                     </tr> 
                                                     <tr>
                                                      <td colspan="100%">
                                                       <div id="div<%# Eval("DEPENDENCIA_ID") %>" style="display:block;position:relative;left:20px;" >                                                     
                                                           <asp:GridView ID="Grid_Partidas_Asignadas_Detalle" runat="server" style="white-space:normal;"
                                                               CssClass="GridView_Nested" HeaderStyle-CssClass="tblHead"
                                                               AutoGenerateColumns="false" GridLines="None" Width="98%"
                                                               OnRowCreated="Grid_Partidas_Asignadas_Detalle_RowCreated">
                                                               <Columns>
                                                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                                        ImageUrl="~/paginas/imagenes/paginas/Select_Grid_Inner.png">
                                                                        <ItemStyle Width="3%" />
                                                                    </asp:ButtonField> 
                                                                    <asp:BoundField DataField="DEPENDENCIA_ID"  />
                                                                    <asp:BoundField DataField="PARTIDA_ID"  />
                                                                    
                                                                    <asp:BoundField DataField="PARTIDA" HeaderText="Partida" >
                                                                        <HeaderStyle HorizontalAlign="Left" Font-Size="XX-Small"/>
                                                                        <ItemStyle HorizontalAlign="Left" Width="77%" Font-Size="XX-Small"/>
                                                                    </asp:BoundField>  
                                                                                                                                         
                                                                    <asp:BoundField DataField="IMPORTE" HeaderText="Importe" DataFormatString="{0:#,###,##0.00}">
                                                                        <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                        <ItemStyle HorizontalAlign="Right"  Width="10%" Font-Size="XX-Small" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                                                ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                                                onclick="Btn_Eliminar_Click" CommandArgument='<%#Eval("PARTIDA_ID")%>'
                                                                                OnClientClick="return confirm('&iquest;Esta seguro que desea elimina el registro?');"/>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="SALDO" HeaderText="saldo">
                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="5%" Font-Size="X-Small"/>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CAPITULO_ID" HeaderText="CAPITULO_ID" />
                                                               </Columns>
                                                               <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                               <PagerStyle CssClass="GridHeader_Nested" />
                                                               <HeaderStyle CssClass="GridHeader_Nested" />
                                                               <AlternatingRowStyle CssClass="GridAltItem_Nested" /> 
                                                           </asp:GridView>
                                                       </div>
                                                      </td>
                                                     </tr>
                                                   </ItemTemplate>
                                                </asp:TemplateField>
                                             </Columns>
                                             <SelectedRowStyle CssClass="GridSelected" />
                                                <PagerStyle CssClass="GridHeader" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                          </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                        </div>
              </div>
            </ContentTemplate>
      </asp:UpdatePanel>
</asp:Content>