﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Constantes;
using JAPAMI.Parametros.Ope_Con_Polizas;
using JAPAMI.Tipo_Polizas.Negocios;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Ayudante_JQuery;
using JAPAMI.Sessiones;

public partial class paginas_Contabilidad_Frm_Controlador_Polizas : System.Web.UI.Page
{
    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Controlador(this.Response, this.Request);
        }
        catch (Exception ex)
        {
            Response.Write("<h6>Error: " + ex.Message + "</h6>");
        }
    }
    #endregion

    #region (Metodos)
    public void Controlador(HttpResponse objResponse, HttpRequest objRequest)
    {
        //declaracion de variables
        String Tipo_Consulta = String.Empty; //variable para el tipo de consulta
        String Resultado = String.Empty; //variable para el resultado
        String Tipo_Poliza_ID = String.Empty; //variable para el tipo de la poliza
        String Fecha = String.Empty; //variable para la fecha
        String Busqueda = String.Empty; //variable para la busqueda
        String Datos_Poliza = String.Empty; //variable para los datos de la poliza

        try
        {
            //Obtener el tipo de la consulta
            Tipo_Consulta = Cls_Parametros_Ope_Con_Polizas.Parametro_Tipo_Consulta();

            //Seleccionar el tipo de la consulta
            switch (Tipo_Consulta)
            {
                case "Consulta_Tipos_Polizas":
                    Resultado = Construye_Tipos_Polizas_JSON_Combo();
                    break;

                case "Consulta_Fuentes_Financiamento":
                    Resultado = Construye_Fuentes_Financiamiento_JSON_Combo();
                    break;

                case "Consulta_Cuentas_Contables":
                    Resultado = Construye_Cuentas_Contables_JSON_Combo();
                    break;

                case "Consulta_Prefijo":
                    //Obtener el resto de los parametros
                    Tipo_Poliza_ID = Cls_Parametros_Ope_Con_Polizas.Parametro_Tipo_Poliza_ID();
                    Fecha = Cls_Parametros_Ope_Con_Polizas.Parametro_Fecha();

                    Resultado = Construye_Prefijo(Tipo_Poliza_ID, Fecha);
                    break;

                case "Autocompletar_Cuentas_Contables":
                    //Obtener el resto de los parametros
                    Busqueda = Cls_Parametros_Ope_Con_Polizas.Parametro_Autocompletar();

                    Resultado = Construye_Cuentas_Contables_JSON_Grid(Busqueda);
                    break;

                case "Datos_Usuario":
                    Resultado = Datos_Usuario_JSON();
                    break;

                case "Alta_Poliza":
                    //Obtener el resto de los parametros
                    Datos_Poliza = Cls_Parametros_Ope_Con_Polizas.Parametro_Datos_Poliza();

                    Resultado = Alta_Poliza(Datos_Poliza);
                    break;

                default:
                    break;
            }

            //Verificar si NO es la impresion a PDF
            if (Tipo_Consulta != "Impresion_Poliza")
            {
                Response.Clear();
                Response.ContentType = "application/json";
                Response.Write(Resultado);
                Response.Flush();
                Response.End();
                //HttpContext.Current.ApplicationInstance.CompleteRequest()
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.ToString(), ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Construye_Tipos_Polizas_JSON_Combo
    /// DESCRIPCION :           Construir la cadena JSON para el llenado del combo de tipos de polizas
    /// PARAMETROS  : 
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           08/Octubre/2012 12:30
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public String Construye_Tipos_Polizas_JSON_Combo()
    {
        //Declaracion de variables
        String Resultado = "[]"; //variable para el resultado
        Cls_Cat_Con_Tipo_Polizas_Negocio Polizas_Negocio = new Cls_Cat_Con_Tipo_Polizas_Negocio(); //variable para la capa de negocios
        DataTable Dt_Tipos_Polizas = new DataTable(); //tabla para la consulta de los tipos de polizas
        StringBuilder Constructor_Cadena = new StringBuilder(); //variable para el constructor de la cadena
        StringWriter Escritor_Cadena = new StringWriter(Constructor_Cadena); //Variable para el escritor de la cadena
        JsonTextWriter Escritor_JSON = new JsonTextWriter(Escritor_Cadena); //variable para el escritor JSON
        int Cont_Elementos = 0; //variable para el contador

        try
        {
            //Consultar los tipos de poliza
            Dt_Tipos_Polizas = Polizas_Negocio.Consulta_Tipos_Poliza(); //Consulta los tipos de polizas

            //Inicializar escritor
            Escritor_JSON.Formatting = Formatting.None;
            Escritor_JSON.WriteStartArray();

            //Colocar el primero elemento
            Escritor_JSON.WriteStartObject();
            Escritor_JSON.WritePropertyName(Cat_Con_Tipo_Polizas.Campo_Descripcion);
            Escritor_JSON.WriteValue(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"));
            Escritor_JSON.WritePropertyName(Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID);
            Escritor_JSON.WriteValue("0");
            Escritor_JSON.WritePropertyName("selected");
            Escritor_JSON.WriteValue(true);
            Escritor_JSON.WriteEndObject();

            //verificar si la consulta arrojo resultados
            if (Dt_Tipos_Polizas.Rows.Count > 0)
            {
                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Tipos_Polizas.Rows.Count; Cont_Elementos++)
                {
                    //Colocar los elementos
                    Escritor_JSON.WriteStartObject();
                    Escritor_JSON.WritePropertyName(Cat_Con_Tipo_Polizas.Campo_Descripcion);
                    Escritor_JSON.WriteValue(Dt_Tipos_Polizas.Rows[Cont_Elementos][Cat_Con_Tipo_Polizas.Campo_Descripcion].ToString().Trim());
                    Escritor_JSON.WritePropertyName(Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID);
                    Escritor_JSON.WriteValue(Dt_Tipos_Polizas.Rows[Cont_Elementos][Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID].ToString().Trim());
                    Escritor_JSON.WriteEndObject();
                }
            }

            //Finalizar el escritor
            Escritor_JSON.WriteEndArray();

            //Asignar resultado
            Resultado = Constructor_Cadena.ToString();

            //Cerrar los objetos
            Escritor_JSON.Close();
            Escritor_Cadena.Close();

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            return "['error':'" + ex.ToString().Trim() + "']";
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Construye_Fuentes_Financiamiento_JSON_Combo
    /// DESCRIPCION :           Construir la cadena JSON para el llenado del combo de tipos de las fuentes de financiamiento
    /// PARAMETROS  : 
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           08/Octubre/2012 16:50
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public String Construye_Fuentes_Financiamiento_JSON_Combo()
    {
        //Declaracion de variables
        String Resultado = "[]"; //variable para el resultado
        Cls_Cat_SAP_Fuente_Financiamiento_Negocio Fuente_Financiamiento_Negocio = new Cls_Cat_SAP_Fuente_Financiamiento_Negocio(); //variable para la capa de negocios
        DataTable Dt_Fuente_Financiamiento = new DataTable(); //Tabla para la fuente de financiamiento
        StringBuilder Constructor_Cadena = new StringBuilder(); //variable para el constructor de la cadena
        StringWriter Escritor_Cadena = new StringWriter(Constructor_Cadena); //Variable para el escritor de la cadena
        JsonTextWriter Escritor_JSON = new JsonTextWriter(Escritor_Cadena); //variable para el escritor JSON
        int Cont_Elementos = 0; //variable para el contador

        try
        {
            //Consultar las fuentes de financiamiento
            Dt_Fuente_Financiamiento = Fuente_Financiamiento_Negocio.Consulta_Fuentes_Financiamiento_Polizas();

            //Inicializar escritor
            Escritor_JSON.Formatting = Formatting.None;
            Escritor_JSON.WriteStartArray();

            //Verificar si la consulta arrojo resultados
            if (Dt_Fuente_Financiamiento.Rows.Count > 0)
            {
                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Fuente_Financiamiento.Rows.Count; Cont_Elementos++)
                {
                    //Colocar los elementos
                    Escritor_JSON.WriteStartObject();
                    Escritor_JSON.WritePropertyName("Nombre");
                    Escritor_JSON.WriteValue(Dt_Fuente_Financiamiento.Rows[Cont_Elementos]["Nombre"].ToString().Trim());
                    Escritor_JSON.WritePropertyName("FUENTE_FINANCIAMIENTO_ID");
                    Escritor_JSON.WriteValue(Dt_Fuente_Financiamiento.Rows[Cont_Elementos]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim());

                    //Verificar si es el primer elemento
                    if (Cont_Elementos == 0)
                    {
                        Escritor_JSON.WritePropertyName("selected");
                        Escritor_JSON.WriteValue(true);
                    }

                    Escritor_JSON.WriteEndObject();
                }
            }
            else
            {
                //Colocar el primero elemento
                Escritor_JSON.WriteStartObject();
                Escritor_JSON.WritePropertyName("Nombre");
                Escritor_JSON.WriteValue(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"));
                Escritor_JSON.WritePropertyName("FUENTE_FINANCIAMIENTO_ID");
                Escritor_JSON.WriteValue("0");
                Escritor_JSON.WritePropertyName("selected");
                Escritor_JSON.WriteValue(true);
                Escritor_JSON.WriteEndObject();
            }

            //Finalizar el escritor
            Escritor_JSON.WriteEndArray();

            //Asignar resultado
            Resultado = Constructor_Cadena.ToString();

            //Cerrar los objetos
            Escritor_JSON.Close();
            Escritor_Cadena.Close();

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            return "['error':'" + ex.ToString().Trim() + "']";
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Construye_Cuentas_Contables_JSON_Combo
    /// DESCRIPCION :           Construir la cadena JSON para el llenado del combo de las cuentas contables
    /// PARAMETROS  : 
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           08/Octubre/2012 19:00
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public String Construye_Cuentas_Contables_JSON_Combo()
    {
        //Declaracion de variables
        String Resultado = "[]"; //variable para el resultado
        Cls_Cat_Con_Cuentas_Contables_Negocio Cuentas_Contables_Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //variable para la capa de negocios
        DataTable Dt_Cuentas_Contables = new DataTable(); //tabla para las cuentas contables
        StringBuilder Constructor_Cadena = new StringBuilder(); //variable para el constructor de la cadena
        StringWriter Escritor_Cadena = new StringWriter(Constructor_Cadena); //Variable para el escritor de la cadena
        JsonTextWriter Escritor_JSON = new JsonTextWriter(Escritor_Cadena); //variable para el escritor JSON
        int Cont_Elementos = 0; //variable para el contador

        try
        {
            //Consultar las cuentas contables
            Cuentas_Contables_Negocio.P_Afectable = "SI";
            Dt_Cuentas_Contables = Cuentas_Contables_Negocio.Consulta_Cuentas_Contables();

            //Inicializar escritor
            Escritor_JSON.Formatting = Formatting.None;
            Escritor_JSON.WriteStartArray();

            //Colocar el primero elemento
            Escritor_JSON.WriteStartObject();
            Escritor_JSON.WritePropertyName("DESCRIPCION");
            Escritor_JSON.WriteValue(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"));
            Escritor_JSON.WritePropertyName("CUENTA_CONTABLE_ID");
            Escritor_JSON.WriteValue("0");
            Escritor_JSON.WritePropertyName("selected");
            Escritor_JSON.WriteValue(true);
            Escritor_JSON.WriteEndObject();

            //Verificar si la consulta arrojo resultados
            if (Dt_Cuentas_Contables.Rows.Count > 0)
            {
                //Ciclo para el baarido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Cuentas_Contables.Rows.Count; Cont_Elementos++)
                {
                    //Colocar los elementos
                    Escritor_JSON.WriteStartObject();
                    Escritor_JSON.WritePropertyName("DESCRIPCION");
                    Escritor_JSON.WriteValue(Dt_Cuentas_Contables.Rows[Cont_Elementos]["CUENTA"].ToString().Trim() + "-" + Dt_Cuentas_Contables.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim());
                    Escritor_JSON.WritePropertyName("CUENTA_CONTABLE_ID");
                    Escritor_JSON.WriteValue(Dt_Cuentas_Contables.Rows[Cont_Elementos]["CUENTA_CONTABLE_ID"].ToString().Trim());
                    Escritor_JSON.WriteEndObject();
                }
            }

            //Finalizar el escritor
            Escritor_JSON.WriteEndArray();

            //Asignar resultado
            Resultado = Constructor_Cadena.ToString();

            //Cerrar los objetos
            Escritor_JSON.Close();
            Escritor_Cadena.Close();

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            return "['error':'" + ex.ToString().Trim() + "']";
        }
    }


    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Construye_Prefijo
    /// DESCRIPCION : Construir el prefijo de acuerdo al tipo de la poliza
    /// PARAMETROS  : 1. Tipo_Poliza_ID: Cadena de texto que contiene el ID del tipo de poliza
    ///               2. Fecha: Cadena de texto que contiene la fecha de la poliza
    /// CREO        : Noe Mosqueda Valadez
    /// FECHA_CREO  : 11/Abril/2012 18:47
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public String Construye_Prefijo(String Tipo_Poliza_ID, String Fecha)
    {
        //Declaracion de variables
        String Prefijo = String.Empty; //variable para el prefijo
        Cls_Ope_Con_Polizas_Negocio Polizas_Negocio = new Cls_Ope_Con_Polizas_Negocio(); //variable para la capa de negocios
        DataTable Dt_Polizas_Tipo = new DataTable(); //Tabla para el resultado de la consulta
        String[] vec_prefijo; //vector para el prefijo
        String Mes_Anio = String.Empty; //variable para el mes y año de la poliza
        DateTime Fecha_dt; //variable para la fecha de la poliza

        try
        {
            //Convertir la fecha de la poliza
            Fecha_dt = Ayudante_JQuery.Convierte_Fecha_DatePicker_dt(Fecha);

            //Obtener el mes y el año
            Mes_Anio = String.Format("{0:MMyy}", Fecha_dt);

            //Asignar propiedades
            Polizas_Negocio.P_Tipo_Poliza_ID = Tipo_Poliza_ID;
            Polizas_Negocio.P_Mes_Ano = Mes_Anio;

            //Ejecutar consulta
            Dt_Polizas_Tipo = Polizas_Negocio.Consulta_Polizas_Tipo();

            //Seleccionar el tipo de poliza
            switch (Tipo_Poliza_ID)
            {
                case "00001": //Ingresos
                    //verificar si la consulta arrojo resultados
                    if (Dt_Polizas_Tipo.Rows.Count > 0)
                    {
                        //Crear el vector con el ultimo prefijo
                        vec_prefijo = Dt_Polizas_Tipo.Rows[0]["Prefijo"].ToString().Split('-');

                        //verificar si el vectpr tiene 2 elementos
                        if (vec_prefijo.Length == 2)
                        {
                            //COnstruir el prefijo
                            Prefijo = (Convert.ToInt32(vec_prefijo[0]) + 1).ToString().Trim() + "-" + (Convert.ToInt32(vec_prefijo[1]) + 1).ToString().Trim();
                        }
                    }
                    else
                    {
                        Prefijo = "301-1";
                    }
                    break;

                case "00002": //Egresos
                    //verificar si la consulta arrojo resultados
                    if (Dt_Polizas_Tipo.Rows.Count > 0)
                    {
                        //Crear el vector con el ultimo prefijo
                        vec_prefijo = Dt_Polizas_Tipo.Rows[0]["Prefijo"].ToString().Split('-');

                        //verificar si el vectpr tiene 2 elementos
                        if (vec_prefijo.Length == 2)
                        {
                            //COnstruir el prefijo
                            Prefijo = (Convert.ToInt32(vec_prefijo[0]) + 1).ToString().Trim() + "-" + (Convert.ToInt32(vec_prefijo[1]) + 1).ToString().Trim();
                        }
                    }
                    else
                    {
                        Prefijo = "1-1";
                    }
                    break;

                case "00003": //Diario
                    //verificar si la consulta arrojo resultados
                    if (Dt_Polizas_Tipo.Rows.Count > 0)
                    {
                        //Crear el vector con el ultimo prefijo
                        vec_prefijo = Dt_Polizas_Tipo.Rows[0]["Prefijo"].ToString().Split('-');

                        //verificar si el vectpr tiene 2 elementos
                        if (vec_prefijo.Length == 2)
                        {
                            //COnstruir el prefijo
                            Prefijo = (Convert.ToInt32(vec_prefijo[0]) + 1).ToString().Trim() + "-" + (Convert.ToInt32(vec_prefijo[1]) + 1).ToString().Trim();
                        }
                    }
                    else
                    {
                        Prefijo = "401-1";
                    }
                    break;

                default:
                    break;
            }

            //Entregar resultado
            return Prefijo;
        }
        catch (Exception ex)
        {
            return "['error':'" + ex.ToString().Trim() + "']";
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Construye_Cuentas_Contables_JSON_Grid
    /// DESCRIPCION : Construir la cadena JSON con el resultado de la busqueda
    /// PARAMETROS  : Busqueda: Cadena de texto que contiene la cadena de busqueda
    /// CREO        : Noe Mosqueda Valadez
    /// FECHA_CREO  : 12/Octubre/2012 16:00
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public String Construye_Cuentas_Contables_JSON_Grid(String Busqueda)
    {
        //Declaracion de variables
        String Resultado = "[{}]"; //variable para el resultado
        Cls_Cat_Con_Cuentas_Contables_Negocio Cuentas_Contables_Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable para la capa de negocios
        DataTable Dt_Cuentas_Contables = new DataTable(); //tabla para el resultado de la consulta

        try
        {
            //Consulta para las cuentas contables
            if (Cls_Util.EsNumerico(Busqueda) == true)
            {
                Cuentas_Contables_Negocio.P_Tipo_Busqueda = "Cuenta";
            }
            else
            {
                Cuentas_Contables_Negocio.P_Tipo_Busqueda = "Descripcion";
            }

            Cuentas_Contables_Negocio.P_Descripcion = Busqueda;
            Dt_Cuentas_Contables = Cuentas_Contables_Negocio.Consulta_Cuentas_Contables_Cuenta_Izquierda();

            //Verificar si la consulta arrojo resultados
            if (Dt_Cuentas_Contables.Rows.Count > 0)
            {
                Resultado = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Cuentas_Contables);
            }

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            return "['error':'" + ex.ToString().Trim() + "']";
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Datos_Usuario_JSON
    /// DESCRIPCION : Construir la cadena JSON con con los datos del usuario
    /// PARAMETROS  : 
    /// CREO        : Noe Mosqueda Valadez
    /// FECHA_CREO  : 30/Marzo/2013 12:06
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public String Datos_Usuario_JSON()
    {
        //Declaracion de variables
        String Resultado = "[{}]"; //Variable para el resultado

        try
        {
            //COlocar los datos en la variable
            //Resultado = "{'Nombre_Usuario':'" + Cls_Sessiones.Nombre_Empleado + "', 'No_Usuario':'" + Cls_Sessiones.No_Empleado + "'}";
            Resultado = "{\"Nombre_Usuario\":\"" + Cls_Sessiones.Nombre_Empleado + "\", \"No_Usuario\":\"" + Cls_Sessiones.No_Empleado + "\"}";

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            return "{'error':'" + ex.ToString().Trim() + "'}";
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Poliza
    /// DESCRIPCION : Dar de alta la poliza
    /// PARAMETROS  : Datos_Poliza: Cadena de texto que tiene los datos de cabecera y detalles
    /// CREO        : Noe Mosqueda Valadez
    /// FECHA_CREO  : 03/Abril/2013 12:06
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public String Alta_Poliza(String Datos_Poliza)
    {
        //Declaracion de variables
        String Resultado = "{}"; //Variable para el resultado
        Cls_Ope_Con_Polizas_Negocio Polizas_Negocio = new Cls_Ope_Con_Polizas_Negocio(); //variable para la capa de negocios
        DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles de las polizas
        String[] Vec_Cabecera_Poliza; //vector para los datos de la cabecera de la poliza
        DateTime Fecha_Poliza; //Variable para la fecha de la poliza
        String Concepto_Poliza = String.Empty; //variable para el concepto de la poliza
        String[] Vec_Detalles_Poliza; //variable para los detalles de la poliza
        String[] Vec_Renglon_Poliza; //variable para el renglon 
        int Cont_Elementos = 0; //variable para el contador
        DataRow Renglon; //Renglon para el llenado de la tabla
        Boolean Datos_Correctos = true; //variable que indica si los datos estan correctos

        try
        {
            //Obtener los datos de la poliza
            Vec_Cabecera_Poliza = Datos_Poliza.Split('|');

            //Verificar si la longitud es 8
            if (Vec_Cabecera_Poliza.Length == 8)
            {
                //Colocar los datos de la cabecera
                Polizas_Negocio.P_Tipo_Poliza_ID = Vec_Cabecera_Poliza[1].Trim();
                
                //Obtener la fecha de la poliza
                Fecha_Poliza = Ayudante_JQuery.Convierte_Fecha_DatePicker_dt(Vec_Cabecera_Poliza[2].Trim());

                Polizas_Negocio.P_Fecha_Poliza = Fecha_Poliza;
                Polizas_Negocio.P_Mes_Ano = String.Format("{0:MMyy}", Fecha_Poliza);

                //Obtener el concepto
                Concepto_Poliza = Vec_Cabecera_Poliza[3].Trim();
                Polizas_Negocio.P_Concepto = Concepto_Poliza;

                Polizas_Negocio.P_Total_Debe = Convert.ToDouble(Vec_Cabecera_Poliza[4].Trim());
                Polizas_Negocio.P_Total_Haber = Convert.ToDouble(Vec_Cabecera_Poliza[5].Trim());
                Polizas_Negocio.P_No_Partida = Convert.ToInt32(Vec_Cabecera_Poliza[6].Trim());
                Polizas_Negocio.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Polizas_Negocio.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
                Polizas_Negocio.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;

                //Extraer la informacion de los detalles de las polizas
                Vec_Detalles_Poliza = Vec_Cabecera_Poliza[7].Trim().Split('°');

                //Verificar si hay datos
                if (Vec_Detalles_Poliza.Length > 0)
                {
                    //Construir la tabla de los detalles
                    Dt_Detalles.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Detalles.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Detalles.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
                    Dt_Detalles.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                    Dt_Detalles.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                    Dt_Detalles.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                    Dt_Detalles.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                    Dt_Detalles.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
                    Dt_Detalles.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
                    Dt_Detalles.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Detalles.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
                    Dt_Detalles.Columns.Add("MOMENTO_FINAL", typeof(System.String));
                    Dt_Detalles.Columns.Add("Nombre_Cuenta", typeof(System.String));

                    //Ciclo para la construccion de la tabla
                    for (Cont_Elementos = 0; Cont_Elementos < Vec_Detalles_Poliza.Length; Cont_Elementos++)
                    {
                        //Instanciar el renglon
                        Renglon = Dt_Detalles.NewRow();

                        //Obtener el vector
                        Vec_Renglon_Poliza = Vec_Detalles_Poliza[Cont_Elementos].Trim().Split('¬');

                        //Verificar si tiene elementos
                        if (Vec_Renglon_Poliza.Length == 6)
                        {
                            //Llenar el renglon
                            Renglon[Ope_Con_Polizas_Detalles.Campo_Partida] = Convert.ToInt32(Vec_Renglon_Poliza[0]);
                            Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Vec_Renglon_Poliza[1].Trim();

                            //Verificar el concepto
                            if (Vec_Renglon_Poliza[2].Trim() == "CP")
                            {
                                Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Poliza;
                            }
                            else
                            {
                                Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto] = Vec_Renglon_Poliza[2].Trim();
                            }

                            Renglon[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Vec_Renglon_Poliza[3]);
                            Renglon[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Vec_Renglon_Poliza[4]);

                            //Colocar el renglon en la tabla
                            Dt_Detalles.Rows.Add(Renglon);
                        }
                        else
                        {
                            Resultado = "{'error':'Datos incompletos, renglon " + (Cont_Elementos + 1).ToString().Trim() + " '}";
                            Datos_Correctos = false;
                            break;
                        }
                    }

                    //Verificar si los datos estan correctos
                    if (Datos_Correctos == true)
                    {
                        Polizas_Negocio.P_Dt_Detalles_Polizas = Dt_Detalles;

                        //Dar de alta la poliza
                        Polizas_Negocio.Alta_Poliza();
                    }
                }
                else
                {
                    Resultado = "{'error':'Datos incompletos'}";
                }
            }
            else
            {
                Resultado = "{'error':'Datos incompletos'}";
            }

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            return "{'error':'" + ex.ToString().Trim() + "'}";
        }
    }

    public void Imprimir_Poliza(String No_Poliza, String Mes_Anio, String Tipo_Poliza_ID)
    {
        //Declaracion de variables
        Cls_Ope_Con_Polizas_Negocio Polizas_Negocio = new Cls_Ope_Con_Polizas_Negocio(); //variable para la capa de negocios
        ReportDocument Reporte = new ReportDocument(); //Reporte de Crystal
        Ds_Rpt_Con_Poliza_Nueva Ds_Rpt_Con_Poliza_Nueva_src = new Ds_Rpt_Con_Poliza_Nueva(); //Dataset archivo para el llenado dekl reporte
        DataRow Renglon; //Renglon para el llenado de las tablas
        int Cont_Elementos = 0; //Variable para el contador
        DataTable Dt_Pagos = new DataTable(); //tabla para la consulta de los detalles de las polizas
        String Ruta_Crystal = String.Empty; //variable para la ruta del reporte de crystal
        String Nombre_Archivo = "Poliza_" + No_Poliza + ".pdf";

        try
        {
            //Asignar propiedades
            Polizas_Negocio.P_Mes_Ano = Mes_Anio;
            Polizas_Negocio.P_Tipo_Poliza_ID = Tipo_Poliza_ID;
            Polizas_Negocio.P_No_Poliza = No_Poliza;

            //Ejecutar consulta
            Dt_Pagos = Polizas_Negocio.Consulta_Detalle_Poliza();

            //Verificar si la consulta arrojo resultados
            if (Dt_Pagos.Rows.Count > 0)
            {
                //Llenar la cabecera
                Renglon = Ds_Rpt_Con_Poliza_Nueva_src.Tables[0].NewRow();

                if (Dt_Pagos.Rows[0]["NO_POLIZA"] != DBNull.Value)
                {
                    Renglon["No_Poliza"] = Dt_Pagos.Rows[0]["NO_POLIZA"];
                }

                if (Dt_Pagos.Rows[0]["TIPO_POLIZA"] != DBNull.Value)
                {
                    Renglon["Tipo_Poliza"] = Dt_Pagos.Rows[0]["TIPO_POLIZA"];
                }

                //Fecha y hora
                Renglon["Fecha_Datos"] = DateTime.Now;
                Renglon["Hora_Datos"] = DateTime.Now;

                if (Dt_Pagos.Rows[0]["FECHA_POLIZA"] != DBNull.Value)
                {
                    Renglon["Fecha_Poliza"] = Dt_Pagos.Rows[0]["FECHA_POLIZA"];
                }

                if (Dt_Pagos.Rows[0]["PREFIJO"] != DBNull.Value)
                {
                    Renglon["Prefijo"] = Dt_Pagos.Rows[0]["PREFIJO"];
                }

                if (Dt_Pagos.Rows[0]["CONCEPTO"] != DBNull.Value)
                {
                    Renglon["Concepto"] = Dt_Pagos.Rows[0]["CONCEPTO"];
                }

                if (Dt_Pagos.Rows[0]["MES_ANO"] != DBNull.Value)
                {
                    Renglon["Mes_Ano"] = Dt_Pagos.Rows[0]["MES_ANO"];
                }

                //Colocar el renglon en la tabla
                Ds_Rpt_Con_Poliza_Nueva_src.Tables[0].Rows.Add(Renglon);

                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Pagos.Rows.Count; Cont_Elementos++)
                {
                    //Instanciar el renglon de los detalles
                    Renglon = Ds_Rpt_Con_Poliza_Nueva_src.Tables[1].NewRow();

                    //Colocar los datos en el renglon
                    if (Dt_Pagos.Rows[Cont_Elementos]["NO_POLIZA"] != DBNull.Value)
                    {
                        Renglon["No_Poliza"] = Dt_Pagos.Rows[Cont_Elementos]["NO_POLIZA"];
                    }

                    if (Dt_Pagos.Rows[Cont_Elementos]["PARTIDA"] != DBNull.Value)
                    {
                        Renglon["Partida"] = Dt_Pagos.Rows[Cont_Elementos]["PARTIDA"];
                    }

                    if (Dt_Pagos.Rows[Cont_Elementos]["CUENTA"] != DBNull.Value)
                    {
                        Renglon["Cuenta"] = Dt_Pagos.Rows[Cont_Elementos]["CUENTA"];
                    }

                    if (Dt_Pagos.Rows[Cont_Elementos]["DESCRIPCION"] != DBNull.Value)
                    {
                        Renglon["Descripcion"] = Dt_Pagos.Rows[Cont_Elementos]["DESCRIPCION"];
                    }

                    if (Dt_Pagos.Rows[Cont_Elementos]["CONCEPTO_PARTIDA"] != DBNull.Value)
                    {
                        Renglon["Concepto_Partida"] = Dt_Pagos.Rows[Cont_Elementos]["CONCEPTO_PARTIDA"];
                    }

                    if (Dt_Pagos.Rows[Cont_Elementos]["CODIGO_PROGRAMATICO"] != DBNull.Value)
                    {
                        Renglon["Codigo_Programatico"] = Dt_Pagos.Rows[Cont_Elementos]["CODIGO_PROGRAMATICO"];
                    }

                    if (Dt_Pagos.Rows[Cont_Elementos]["DEBE"] != DBNull.Value)
                    {
                        Renglon["Debe"] = Dt_Pagos.Rows[Cont_Elementos]["Debe"];
                    }

                    if (Dt_Pagos.Rows[Cont_Elementos]["HABER"] != DBNull.Value)
                    {
                        Renglon["Haber"] = Dt_Pagos.Rows[Cont_Elementos]["HABER"];
                    }

                    if (Dt_Pagos.Rows[Cont_Elementos]["BENEFICIARIO"] != DBNull.Value)
                    {
                        Renglon["Beneficiario"] = Dt_Pagos.Rows[Cont_Elementos]["BENEFICIARIO"];
                    }

                    if (Dt_Pagos.Rows[Cont_Elementos]["REFERENCIA"] != DBNull.Value)
                    {
                        Renglon["Referencia"] = Dt_Pagos.Rows[Cont_Elementos]["REFERENCIA"];
                    }

                    //Colocar el renglon en la tabla
                    Ds_Rpt_Con_Poliza_Nueva_src.Tables[1].Rows.Add(Renglon);
                }

                //Asignar la ruta del reporte de crystal
                Ruta_Crystal = HttpContext.Current.Server.MapPath("../Rpt/Contabilidad/Rpt_Con_Poliza_Nueva.rpt");

                //Generar el reporte
                Reporte.Load(Ruta_Crystal);
                Reporte.SetDataSource(Ds_Rpt_Con_Poliza_Nueva_src);

                //Mandar el reporte
                Reporte.ExportToHttpResponse(ExportFormatType.PortableDocFormat, HttpContext.Current.Response, true, Nombre_Archivo);
            }
        }
        catch (Exception ex)
        {
            Response.Write("<h6>Error: " + ex.Message + "</h6>");
        }
    }
    #endregion
}