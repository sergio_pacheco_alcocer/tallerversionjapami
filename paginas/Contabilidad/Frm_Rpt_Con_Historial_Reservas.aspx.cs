﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Sessiones;
using System.Data;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Catalogo_Compras_Proyectos_Programas.Negocio;
using JAPAMI.Sap_Partida_Generica.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using JAPAMI.Capitulos.Negocio;

public partial class paginas_Contabilidad_Frm_Rpt_Con_Historial_Reservas : System.Web.UI.Page
{
    #region Page_Load

    //objeto de clase historial reservas negocio
    private Cls_Rpt_Con_Historial_Reservas_Negocio Neg_Htr_Reservas;
    private DataTable Dt_Reservas;

    protected void Page_Load(object sender, EventArgs e)
    {
        try 
        {
            //Refresca la sesion del usuario logueado en el sistema
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout* 60) + 5));
            //valida que exista un usuario logueado en el sistema
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty))
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                
                //instanciar variables globales
                Neg_Htr_Reservas = new Cls_Rpt_Con_Historial_Reservas_Negocio();
                Dt_Reservas = new DataTable();
                Dt_Reservas = Neg_Htr_Reservas.Consulta_Historial_Reservas();

                Llenar_Combos();
                Estado_Inicial();
                Txt_Fecha_Inicio.Text = DateTime.Now.ToString("dd/MMM/YYYY").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/YYYY").ToUpper();

            }

        }catch(Exception Ex)
        {
            Mostrar_Mensaje_Error("Error al cargar la Pagina Historial Reservas",Ex.Message,true);
        }
    }

    
    #endregion

    #region Grid

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Historial_Reservas
    ///DESCRIPCIÓN          : Metodo para llenar el grid de historial reservas
    ///PARAMETROS           :
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 19/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Llenar_Grid_Historial_Reservas()
    {
        Cls_Rpt_Con_Historial_Reservas_Negocio Negocio = new Cls_Rpt_Con_Historial_Reservas_Negocio(); //conexion con la capa de negocios
        DataTable Dt_Reservas = new DataTable();

        try
        {
            ////filtro de dependencia id
            //if (Cmb_Dependencia_ID.SelectedIndex != 0)
            //    Negocio.P_Dependencia_Id = Cmb_Dependencia_ID.SelectedValue.ToString().Trim();

            ////filtro de financiamiento
            //if (Cmb_Financiamiento_Des.SelectedIndex != 0)
            //    Negocio.P_Financiamiento_Id = Cmb_Financiamiento_Des.SelectedValue.ToString().Trim();

            ////filtro de proyecto programas
            //if (Cmb_Proyectos_Programas_Des.SelectedIndex != 0)
            //    Negocio.P_Proyecto_Programas = Cmb_Proyectos_Programas_Des.SelectedValue.ToString().Trim();

            ////filtro de partida
            //if (Cmb_Partida_Generica_Des.SelectedIndex != 0)
            //    Negocio.P_Partida = Cmb_Partida_Generica_Des.SelectedValue.ToString().Trim();

            ////filtro de capitulo
            //if (Cmb_Capitulo_Des.SelectedIndex != 0)
            //    Negocio.P_Capitulo_Id = Cmb_Capitulo_Des.SelectedValue.ToString().Trim();


            Dt_Reservas = Negocio.Consulta_Historial_Reservas();
            if (Dt_Reservas.Rows.Count > 0)
            {
                Grid_Historial_Reservas.DataSource = Dt_Reservas;
                Grid_Historial_Reservas.DataBind();
            }
            else
            {
                Grid_Historial_Reservas.DataSource = new DataTable();
            }
        }
        catch (Exception Ex)
        {
            Mostrar_Mensaje_Error("Error al cargar la tabla de historial reservas ", Ex.Message, true);
        }
    }

    #endregion

    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Mostrar Mensaje de Error
    ///DESCRIPCIÓN          : Metodo para mostrar el error capturado
    ///PARAMETROS           : 1 El mensaje de donde sucedio el error
    ///                       2 El error cachado de la excepcion
    ///                       3 Mostrar la etiqueta en el aspx
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 18/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Mostrar_Mensaje_Error(String mensaje, String error, Boolean mostrar) {
        Lbl_Mensaje_Error.Text = mensaje;
        if (!String.IsNullOrEmpty(error))
            Lbl_Mensaje_Error.Text += " Causa: " + error;
        Lbl_Mensaje_Error.Visible = mostrar;
        Img_Error.Visible = mostrar;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Estado Inicial
    ///DESCRIPCIÓN          : Metodo para limpiar los campos o ponerlos en su posicion origal
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 18/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Estado_Inicial() {
        Mostrar_Mensaje_Error(null,null,false);

        Cmb_Dependencia_ID.SelectedIndex = -1;
        Cmb_Financiamiento_Des.SelectedIndex = -1;
        Cmb_Proyectos_Programas_Des.SelectedIndex = -1;
        Cmb_Partida_Generica_Des.SelectedIndex = -1;
        //Cmb_Partidas_Especificas_Des.SelectedIndex = -1;
        Cmb_Partidas_Especificas_Des.Enabled = false;
        Cmb_Capitulo_Des.SelectedIndex = -1;

        Txt_Fecha_Inicio.Enabled = false;
        Txt_Fecha_Final.Enabled = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar los combos
    ///DESCRIPCIÓN          : Metodo para llenar los combos  
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 18/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Llenar_Combos()
    {
        Llenar_Cmb_Dependencia_ID();
        Llenar_Cmb_Financiamiento_Des();
        Llenar_Cmb_Proyectos_Programa_Des();
        Llenar_Cmb_Partida_Generica_Des();
        Llenar_Cmb_Partidas_Especificas_Des("");
        Llenar_Cmb_Capitulo_Des();
        Llenar_Grid_Historial_Reservas();

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar el combo Dependencia
    ///DESCRIPCIÓN          : Metodo para llenar el combo dependencia
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 18/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Llenar_Cmb_Dependencia_ID() {

        //objeto de la clase Dependencias negocio
        Cls_Cat_Dependencias_Negocio Neg_Dependencias = new Cls_Cat_Dependencias_Negocio();
        DataTable Dt_Dependencia = new DataTable();

        try {
            
            Dt_Dependencia = Neg_Dependencias.Consulta_Dependencias();

            Cmb_Dependencia_ID.DataSource = Dt_Dependencia;
            Cmb_Dependencia_ID.DataTextField = "Nombre";
            Cmb_Dependencia_ID.DataValueField = "Dependencia_ID";
            Cmb_Dependencia_ID.DataBind();
            Cmb_Dependencia_ID.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            Cmb_Dependencia_ID.SelectedIndex = 0;

            ////int fila = 0;
            ////Cmb_Dependencia_ID.Items.Clear();
            ////Cmb_Dependencia_ID.Items.Insert(0, new ListItem("<SELECCIONE>", ""));

            ////if (Dt_Dependencia != null)
            ////{
            ////    foreach (DataRow row in Dt_Dependencia.Rows)
            ////    {
            ////        Cmb_Dependencia_ID.Items.Insert((fila + 1),
            ////            new ListItem(Dt_Dependencia.Rows[fila][1].ToString().Trim(),//texto en el index del combo
            ////                Dt_Dependencia.Rows[fila][0].ToString().Trim()));//valor del index del combo
            ////        fila++;
            ////    }
            ////}
            ////else
            ////    Cmb_Dependencia_ID.SelectedIndex = -1;

        
        }catch(Exception Ex){
            Mostrar_Mensaje_Error("Error al cargar el combo dependencia_id",Ex.Message,true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar el combo Financiamiento
    ///DESCRIPCIÓN          : Metodo para llenar el combo financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 18/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Llenar_Cmb_Financiamiento_Des()
    {
        //objeto de la clase fte financiamiento negocio
        Cls_Cat_SAP_Fuente_Financiamiento_Negocio Neg_Financiamiento = new Cls_Cat_SAP_Fuente_Financiamiento_Negocio();
        DataTable Dt_Financiamieto = new DataTable();

        try
        {
            Dt_Financiamieto = Neg_Financiamiento.Consulta_Fuente_Financiamiento();

            Cmb_Financiamiento_Des.DataSource = Dt_Financiamieto;
            Cmb_Financiamiento_Des.DataTextField = "Descripcion";
            Cmb_Financiamiento_Des.DataValueField = "Fuente_Financiamiento_id";
            Cmb_Financiamiento_Des.DataBind();

            Cmb_Financiamiento_Des.Items.Insert(0, new ListItem("<SELECCIONE>",""));
            Cmb_Financiamiento_Des.SelectedIndex = 0;

        }catch(Exception Ex){
            Mostrar_Mensaje_Error("Error al cargar combo Financiamiento ",Ex.Message,true);
        }


    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar el combo Proyecto Programas
    ///DESCRIPCIÓN          : Metodo para llenar el combo Proyecto Programas
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 18/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Llenar_Cmb_Proyectos_Programa_Des()
    {
        Cls_Cat_Com_Proyectos_Programas_Negocio Neg_Proyectos = new Cls_Cat_Com_Proyectos_Programas_Negocio();
        DataTable Dt_Proyectos = new DataTable();

        try {
            Dt_Proyectos = Neg_Proyectos.Consulta_Programas_Proyectos();

            Cmb_Proyectos_Programas_Des.DataSource = Dt_Proyectos;
            Cmb_Proyectos_Programas_Des.DataTextField = "Nombre";
            Cmb_Proyectos_Programas_Des.DataValueField = "Proyecto_Programa_Id";
            Cmb_Proyectos_Programas_Des.DataBind();

            Cmb_Proyectos_Programas_Des.Items.Insert(0, new ListItem("<SELECCIONE>",""));
            Cmb_Proyectos_Programas_Des.SelectedIndex = 0;

        }catch(Exception Ex){
                Mostrar_Mensaje_Error("Error al cargar el combo Proyectos Programas",Ex.Message,true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar el combo Partida Generica
    ///DESCRIPCIÓN          : Metodo para llenar el combo Partida Genericas
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 18/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Llenar_Cmb_Partida_Generica_Des()
    {
        Cls_Cat_Sap_Partida_Generica_Negocio Neg_Partidas_Genericas = new Cls_Cat_Sap_Partida_Generica_Negocio();
        DataTable Dt_Partida_Generica = new DataTable();

        Dt_Partida_Generica = Neg_Partidas_Genericas.Consultar_Partidas_Genericas();

        Cmb_Partida_Generica_Des.DataSource = Dt_Partida_Generica;
        Cmb_Partida_Generica_Des.DataTextField = "Descripcion";
        Cmb_Partida_Generica_Des.DataValueField = "Partida_Generica_Id";
        Cmb_Partida_Generica_Des.DataBind();

        Cmb_Partida_Generica_Des.Items.Insert(0, new ListItem("<SELECCIONE>",""));
        Cmb_Partida_Generica_Des.SelectedIndex = 0;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar el combo Partidas Especificas
    ///DESCRIPCIÓN          : Metodo para llenar el combo partidas Especificas
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 18/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Llenar_Cmb_Partidas_Especificas_Des(string id_Generica)
    {

        Cmb_Partidas_Especificas_Des.Items.Clear();
        Cls_Cat_SAP_Partidas_Especificas_Negocio neg_Partidas_especificas = new Cls_Cat_SAP_Partidas_Especificas_Negocio();
        DataTable Dt_Partidas_Especificas = new DataTable();

        if (!String.IsNullOrEmpty(id_Generica))
        {
            neg_Partidas_especificas.P_Partida_Generica_ID = id_Generica;
            Dt_Partidas_Especificas = neg_Partidas_especificas.Consulta_Partida_Especifica();

            Cmb_Partidas_Especificas_Des.DataSource = Dt_Partidas_Especificas;
            Cmb_Partidas_Especificas_Des.DataTextField = "Nombre";
            Cmb_Partidas_Especificas_Des.DataValueField = "Partida_Id";
            Cmb_Partidas_Especificas_Des.DataBind();
        }
            Cmb_Partidas_Especificas_Des.Items.Insert(0, new ListItem("<SELECCIONE>",""));
            Cmb_Partidas_Especificas_Des.SelectedIndex = 0;
        
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar el combo Capitulo
    ///DESCRIPCIÓN          : Metodo para llenar el combo Capitulo
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 18/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Llenar_Cmb_Capitulo_Des()
    {
        Cls_Cat_SAP_Capitulos_Negocio ne = new Cls_Cat_SAP_Capitulos_Negocio();
        DataTable Dt_Capitulo = new DataTable();

        Dt_Capitulo = ne.Consulta_Capitulos();

        Cmb_Capitulo_Des.DataSource = Dt_Capitulo;
        Cmb_Capitulo_Des.DataTextField = "Descripcion";
        Cmb_Capitulo_Des.DataValueField = "Capitulo_Id";
        Cmb_Capitulo_Des.DataBind();

        Cmb_Capitulo_Des.Items.Insert(0, new ListItem("<SELECCIONE>",""));
        Cmb_Capitulo_Des.SelectedIndex = 0;


    }

    

    #endregion

    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Partida_Generica_Des_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo cmb partida generica 
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 18/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Partida_Generica_Des_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Partida_Generica_Des.SelectedIndex != 0)
        {
            Cmb_Partidas_Especificas_Des.Enabled = true;
            Llenar_Cmb_Partidas_Especificas_Des(Cmb_Partida_Generica_Des.SelectedValue);
        }
        else
            Llenar_Cmb_Partidas_Especificas_Des(null);
        
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Historial_Reservas_Sorting
    ///DESCRIPCIÓN          : Evento de ordenar las columnas de los grids
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 19/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Historial_Reservas_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable Dt_Reservas = new DataTable();

        try
        {
            //Llenar_Grid_Rubros();
            Dt_Reservas = (DataTable)Grid_Historial_Reservas.DataSource;
            if (Dt_Reservas != null)
            {
                DataView Dv_Vista = new DataView(Dt_Reservas);
                String Orden = ViewState["SortDirection"].ToString();
                if (Orden.Equals("ASC"))
                {
                    Dv_Vista.Sort = e.SortExpression + " DESC";
                    ViewState["SortDirection"] = "DESC";
                }
                else
                {
                    Dv_Vista.Sort = e.SortExpression + " ASC";
                    ViewState["SortDirection"] = "ASC";
                }
                Grid_Historial_Reservas.DataSource = Dv_Vista;
                Grid_Historial_Reservas.DataBind();
            }
        }
        catch (Exception Ex)
        {
            Mostrar_Mensaje_Error("Error al ordenar la tabla historial reservas ",Ex.Message,true);
        }

    }

    #endregion

    
}
