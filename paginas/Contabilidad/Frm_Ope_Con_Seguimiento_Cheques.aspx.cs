﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Ope_Con_Seguimiento_De_Cheques.Negocios;
using JAPAMI.Constantes;

public partial class paginas_Contabilidad_Frm_Ope_Con_Seguimiento_Cheques : System.Web.UI.Page
{
    #region PAGE_LOAD
    protected void Page_Load(object sender, EventArgs e)
    {
        //Valores por primera vez
        if (!IsPostBack)
        {
            Habilitar_Controles("Inicio");
            //Establecer la fechas inicial y final
            DateTime _DateTime = DateTime.Now;
            int dias = _DateTime.Day;
            dias = dias * -1;
            dias++;
            _DateTime = _DateTime.AddDays(dias);
            _DateTime = _DateTime.AddMonths(-1);
            Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
            Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
            Llenar_Combo_Tipos_Beneficiarios();
            Llenar_Combo_Etapa();
            Llenar_Gird_Listado();
        }
    }
    #endregion

    #region METODOS
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Habilitar_Controles
    ///DESCRIPCIÓN: Habilita los controles segun corresponda
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 13/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Habilitar_Controles(String Modo) 
    { 
        try
        {
            switch (Modo)
            {
                case "Inicio":
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Div_Listado_Cheques.Visible = true;
                    Div_Contenido.Visible = false;
                    break;
                case "Cheque":
                    Btn_Salir.ToolTip = "Atras";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Div_Contenido.Visible = true;
                    Div_Listado_Cheques.Visible = false;
                    break;
                default: break;
            }
        }
        catch(Exception Ex)
        {
            Mostrar_Error("Error al Habilitar los controles: " + Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Error
    ///DESCRIPCIÓN: Muestra u oculta los mensajes de error
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 13/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Mostrar_Error(String Mensaje)
    {
        Img_Warning.Visible =  true;
        Lbl_Mensaje_Error.Text += Mensaje + "</br>";
        Lbl_Mensaje_Error.Visible  = true;
    }
    public void Mostrar_Error()
    {
        Img_Warning.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Mensaje_Error.Visible =  false;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Tipos_Beneficiarios
    ///DESCRIPCIÓN: Lllena el combo tipos_beneficiarios
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 13/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_Tipos_Beneficiarios() 
    {
        Cls_Ope_Con_Seguimiento_Cheques_Negocio Seguimieto_Cheques_Negocio = new Cls_Ope_Con_Seguimiento_Cheques_Negocio();
        DataTable Dt_Tipos_Beneficiarios = new DataTable();
        try 
        {
            Dt_Tipos_Beneficiarios = Seguimieto_Cheques_Negocio.Consultar_Tipos_Beneficiarios();
            if (Dt_Tipos_Beneficiarios != null && Dt_Tipos_Beneficiarios.Rows.Count > 0)
            {
                Cmb_Beneficiario.Items.Clear();
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Tipo_Beneficiario, Dt_Tipos_Beneficiarios);
            }
            else 
            {
                Cmb_Beneficiario.Items.Clear();
                Cmb_Beneficiario.Items.Add("<SELECCIONAR>");
                Cmb_Beneficiario.Items[0].Value = "0";
                Cmb_Beneficiario.Items[0].Selected = true;
            }
            //Combo Beneficiarios
            Cmb_Beneficiario.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
            Cmb_Beneficiario.SelectedIndex = 0;
        }
        catch(Exception Ex)
        {
            throw new Exception("Error al llenar la lista de tipos de beneficiarios : " + Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Buscar_Beneficiario
    ///DESCRIPCIÓN: Busca al beneficiario de a cuerdo al tipo seleccionadoo en el combo Tipo_Beneficiario
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 14/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Buscar_Beneficiario() 
    {
        Cls_Ope_Con_Seguimiento_Cheques_Negocio Seguimiento_Cheques_Negocio = new Cls_Ope_Con_Seguimiento_Cheques_Negocio();
        DataTable Dt_Beneficiarios = null;
        try 
        {
            Seguimiento_Cheques_Negocio.P_Beneficiario = Txt_Beneficiario.Text.ToString().Trim(); //Criterio de busqueda para buscar el beneficiario
            if(Cmb_Tipo_Beneficiario.SelectedIndex >0)
            Seguimiento_Cheques_Negocio.P_Tipo_Benefciario = Cmb_Tipo_Beneficiario.SelectedValue.Trim();//Criterio de busqueda para buscar el beneficiario
            
            Dt_Beneficiarios = Seguimiento_Cheques_Negocio.Consultar_Beneficiario(); //Consultamos los beneficiarios que coinciden con la descripcion introducida en txt_Beneficiario
            Cmb_Beneficiario.Items.Clear();
            if (Dt_Beneficiarios != null && Dt_Beneficiarios.Rows.Count > 0)
            {
                //Llenamos el combo de beneficiarios
                Cmb_Beneficiario.DataSource = Dt_Beneficiarios;
                Cmb_Beneficiario.DataTextField = Dt_Beneficiarios.Columns[0].ToString();
                Cmb_Beneficiario.DataValueField = Dt_Beneficiarios.Columns[0].ToString();
                Cmb_Beneficiario.DataBind();
                Cmb_Beneficiario.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
                Cmb_Beneficiario.SelectedIndex = 1;
                Txt_Beneficiario.Text = Cmb_Beneficiario.SelectedItem.Text;
            }
            else
            {
                Cmb_Beneficiario.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
                Cmb_Beneficiario.SelectedIndex = 0;
            }
        }
        catch(Exception Ex)
        {
            Mostrar_Error("Error al cargar los beneficiarios: " + Ex.Message);
        }
    }
      ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Etapa
    ///DESCRIPCIÓN: Llena las etapas por las que puede pasar el cheque
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 14/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_Etapa() 
    {
        try 
        {
            Cmb_Etapa.Items.Clear();
            Cmb_Etapa.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
            Cmb_Etapa.Items.Insert(1, new ListItem(HttpUtility.HtmlDecode("CAJA"), "CAJA"));
            Cmb_Etapa.Items.Insert(2, new ListItem(HttpUtility.HtmlDecode("CONTABILIDAD"), "CONTABILIDAD"));
            Cmb_Etapa.Items.Insert(3, new ListItem(HttpUtility.HtmlDecode("ADMINISTRATIVO"), "ADMINISTRATIVO"));
            Cmb_Etapa.Items.Insert(4, new ListItem(HttpUtility.HtmlDecode("TESORERIA"), "TESORERIA"));
            Cmb_Etapa.Items.Insert(5, new ListItem(HttpUtility.HtmlDecode("DIR. GENERAL"), "DIR. GENERAL"));
            Cmb_Etapa.Items.Insert(6, new ListItem(HttpUtility.HtmlDecode("POLTE. CONSEJO"), "POLTE. CONSEJO"));
           
            Cmb_Etapa.SelectedIndex = 0;
        }
        catch(Exception Ex)
        {
            throw new Exception("Error al cargar las etapas: " + Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Gird_Listado
    ///DESCRIPCIÓN: Reviza los filtros de busqueda y carga los datos a la gridview
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 17/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Gird_Listado() 
    {
        Cls_Ope_Con_Seguimiento_Cheques_Negocio Seguimiento_Cheques_Negocio = new Cls_Ope_Con_Seguimiento_Cheques_Negocio();
        DataTable Dt_Listado_Cheques = new DataTable();
        try 
        {
            //Verificar si tiene algo el filtro de No_Cheque
            if (!String.IsNullOrEmpty(Txt_No_Cheque.Text)) 
            {
                Seguimiento_Cheques_Negocio.P_No_Cheque = Txt_No_Cheque.Text.ToString().Trim();
            }
            //Verificar si tiene datos el filtro Beneficario
            if (!String.IsNullOrEmpty(Txt_Beneficiario.Text)) 
            {
                Seguimiento_Cheques_Negocio.P_Beneficiario = Txt_Beneficiario.Text.ToString().Trim();
            }
            //Obtener los fitlros de fecha
            Seguimiento_Cheques_Negocio.P_Fecha_Emición_Inicio = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Inicial.Text);
            Seguimiento_Cheques_Negocio.P_Fecha_Emición_Fin = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Final.Text);
            //Verificar si se selecciono una etapa del filtro etapa
            if (Cmb_Etapa.SelectedIndex > 0) 
            {
                Seguimiento_Cheques_Negocio.P_Etapa = Cmb_Etapa.SelectedItem.ToString().Trim();
            }
            //Verificar si se selecciono una opcion del filtro Tipo_Beneficiario
            if (Cmb_Tipo_Beneficiario.SelectedIndex > 0) 
            {
                Seguimiento_Cheques_Negocio.P_Tipo_Benefciario = Cmb_Tipo_Beneficiario.SelectedValue;
            }
            Dt_Listado_Cheques = Seguimiento_Cheques_Negocio.Consultar_Cheques_Listado();
            if (Dt_Listado_Cheques != null && Dt_Listado_Cheques.Rows.Count > 0)
            {
                Grid_Cheques.DataSource = Dt_Listado_Cheques;
                Grid_Cheques.DataBind();
            }
            else 
            {
                Grid_Cheques.DataSource = new DataTable();
                Grid_Cheques.DataBind();
            }
        }
        catch(Exception Ex)
        {
            Mostrar_Error("Error al carar el grid: " + Ex.Message);
        }
    }
    #endregion

    #region GRID
    #endregion

    #region EVENTOS
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.ToolTip.Equals("Inicio")) 
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else if (Btn_Salir.ToolTip.Equals("Atras")) 
        {
            Habilitar_Controles("Inicio");
            Mostrar_Error();
            //Establecer la fechas inicial y final
            DateTime _DateTime = DateTime.Now;
            int dias = _DateTime.Day;
            dias = dias * -1;
            dias++;
            _DateTime = _DateTime.AddDays(dias);
            _DateTime = _DateTime.AddMonths(-1);
            Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
            Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
            Llenar_Combo_Tipos_Beneficiarios();
            Llenar_Combo_Etapa();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN: Evento del boton buscar
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 17/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Llenar_Gird_Listado();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Cheque_Click
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Cheque_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Con_Seguimiento_Cheques_Negocio Seguimiento_Cheque_Negocio =  new Cls_Ope_Con_Seguimiento_Cheques_Negocio();
        try
        {
            //Obtenemos el No_Cheque
            Mostrar_Error();
            String No_Cheque = ((ImageButton)sender).CommandArgument;
            Habilitar_Controles("Cheque");
            Seguimiento_Cheque_Negocio.P_No_Cheque = No_Cheque;
            DataTable Dt_Detalles_Cheque = Seguimiento_Cheque_Negocio.Consultar_Detalle_Cheque();
            if (Dt_Detalles_Cheque != null && Dt_Detalles_Cheque.Rows.Count > 0)
            {
                Txt_No_Cheque_Contenido.Text = Dt_Detalles_Cheque.Rows[0][Ope_Con_Cheques.Campo_Folio].ToString();
                Txt_Beneficiario_Contenido.Text = Dt_Detalles_Cheque.Rows[0][Ope_Con_Cheques.Campo_Beneficiario].ToString();
                Txt_Estatus_Contenido.Text = Dt_Detalles_Cheque.Rows[0][Ope_Con_Cheques.Campo_Estatus].ToString();
                Txt_Importe_Contenido.Text = "$ " + Dt_Detalles_Cheque.Rows[0][Ope_Con_Cheques.Campo_Monto].ToString();
                Grid_Detalle_Cheques.DataSource = Dt_Detalles_Cheque;
                Grid_Detalle_Cheques.DataBind();
            }
            else 
            {
                Grid_Detalle_Cheques.DataSource = new DataTable();
                Grid_Detalle_Cheques.DataBind();
            }
        }
        catch(Exception Ex)
        {
            Mostrar_Error("Error al seleccionar el cheque: " + Ex.Message);
        }
        //Evento_Grid_Requisiciones_Seleccionar(No_Requisicion);

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Txt_Beneficiario_TextChanged
    ///DESCRIPCIÓN: Envento de la caja de texto a travez de la cual se buscara al beneficiario
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 14/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    //////*******************************************************************************
    protected void Txt_Beneficiario_TextChanged(object sender, EventArgs e)
    {
        String Tipo_Beneficiario = String.Empty;
        if (!String.IsNullOrEmpty(Txt_Beneficiario.Text.ToString()))
        {
            Mostrar_Error();
            Buscar_Beneficiario();
        }
        else 
        {
            Cmb_Beneficiario.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
            Cmb_Beneficiario.SelectedIndex = 0;
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Beneficiario_SelectedIndexChanged
    ///DESCRIPCIÓN: Envento del combo Cmb_Beneficiario
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 14/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    //////*******************************************************************************
    protected void Cmb_Beneficiario_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Beneficiario.SelectedIndex > 0) 
        {
            Txt_Beneficiario.Text = Cmb_Beneficiario.SelectedItem.ToString();
        }
    }
    #endregion
}
