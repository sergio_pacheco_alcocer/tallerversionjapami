﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Recepcion_Gasto_Comprobar.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Recepcion_Gasto_Comprobar" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../../jquery/jquery-1.5.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
<script language="javascript" type="text/javascript">
        function selec_todo2() {
            var y;
            y = $('#chkAll').is(':checked');
            var $chkBox = $("input:checkbox[id$=Chk_Autorizado]");
            if (y == true) {
                $chkBox.attr("checked", true);
            } else {
                $chkBox.attr("checked", false);
                //        x= $('.ser').attr('checked', false);
            }
        }
         // El popup para el rechazo de la solicitud
         function Abrir_Popup2(Control) {
             $find('Contenedor').show();
             document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = $(Control).parent().attr('class');
             document.getElementById("<%=Txt_Rechazo.ClientID%>").value = 1;
             $("#Tr_Autorizar").hide();
             $("#Tr_Rechazar").show();
         }
         
         function Cerrar_Modal_Popup() {
            $find('Contenedor').hide();
            Limpiar_Ctlr();
            return false;
        } 
        
        function Limpiar_Ctlr()
        {
            var $chkBox = $("input:checkbox[id$=Chk_Rechazar]");
            document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = "";
            document.getElementById("<%=Txt_Rechazo.ClientID%>").value = "";
            document.getElementById("<%=Txt_Comentario.ClientID%>").value = "";
            $chkBox.attr('checked', false);
        }
         
         function Validar_Cantidad_Caracteres(control) 
         {
            var limit = 250;
            var len = $(control).val().length; //cuenta los caracteres conforme se van introduciendo
            if (len > limit) {
                control.value = control.value.substring(250, limit); //elimina los caracteres de mas
            }
            if (len > 0) 
            {
                $("#Tr_Aceptar").show();
            } 
            else 
            {
                $("#Tr_Aceptar").hide(); 
            }
        }
        function MostrarProgress() {
            $('[id$=Up_Recepcion_Documentos]').show();
        }
        function OcultarProgress() {
            $('[id$=Up_Recepcion_Documentos]').delay(10000).hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Recepcion_Documentos" runat="server"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Up_Recepcion_Documentos" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Recepcion_Documentos" >
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Autorizacion y Asignacion de Solicitud de Gastos por Comprobar</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                 ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%">Busqueda
                            <asp:TextBox ID="Txt_Busqueda_No_Solicitud" runat="server" MaxLength="10" ToolTip="Buscar No. Solicitud"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_No_Solicitud" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Ingrese No. Solicitud>" TargetControlID="Txt_Busqueda_No_Solicitud" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_No_Solicitud" runat="server" 
                                TargetControlID="Txt_Busqueda_No_Solicitud" FilterType="Numbers" >
                            </cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="Btn_Buscar_No_Solicitud" runat="server" 
                                ToolTip="Consultar" 
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                onclick="Btn_Buscar_No_Solicitud_Click" />
                        </td> 
                    </tr>          
                </table>
                <table width="98%" class="estilo_fuente">
                    <tr>
                        <td>
                            <table width="99.9%" class="estilo_fuente">
                                <tr>
                                    <td style="width:30%">
                                    </td>
                                    <td align="right" >
                                        <asp:Label ID="Lbl_Seleccion_Masiva" runat="server" Text="AUTORIZAR TODOS"></asp:Label><input type="checkbox"  id="chkAll"  onclick="selec_todo2();" tabindex="2"/>
                                         &nbsp;
                                            <asp:Button ID="Btn_Autozizar" runat="server" ToolTip="Autorizar" Text='Autorizar' CssClass="Img_Button" TabIndex="3"  onclick="Autorizar_Solicitud" Width="15%" />
                                         &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;text-align:center;vertical-align:top;"> 
                            <center>
                                <div style="overflow:auto;height:500px;width:99.5%;vertical-align:top;border-style:outset;border-color:Silver;" >
                                    <asp:GridView ID="Grid_Solicitud_Deudores" runat="server" 
                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" DataKeyNames="No_Deuda"
                                        EmptyDataText="En este momento no se tienen pagos pendientes por autorizar"
                                        Width="98%" OnRowDataBound="Grid_Solicitud_RowDataBound">
                                        <Columns> 
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png"  >
                                                <ItemStyle Width="3%" />
                                                <HeaderStyle Width="3%" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="No_Deuda" HeaderText="Solicitud">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%"  Font-Size="X-Small"/>
                                                <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Deudor">
                                                <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Tipo Pago">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="Cmb_Tipo_Pago" runat="server" CssClass='<%# Eval("No_Deuda") %>' Font-Size="X-Small" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Tipo_Pago_OnSelectedIndexChanged" >
                                                    <asp:ListItem Value="1">Vale</asp:ListItem><asp:ListItem Value="2">Cheque</asp:ListItem></asp:DropDownList></ItemTemplate ><HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="XX-Small"/>
                                                <ItemStyle HorizontalAlign="Center" Width="10%" Font-Size="XX-Small"/>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Banco">
                                                <ItemTemplate>
                                                 <asp:DropDownList ID="Cmb_Banco_Transferencia" runat="server" Width="95%" Font-Size="X-Small"  AutoPostBack="true" OnSelectedIndexChanged="Cmb_Banco_Transferencia_OnSelectedIndexChanged">
                                                  </asp:DropDownList>
                                                </ItemTemplate >
                                                <HeaderStyle HorizontalAlign="Center" Width="13%" Font-Size="XX-Small"/>
                                                <ItemStyle HorizontalAlign="Center" Width="13%" Font-Size="XX-Small"/>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cuenta">
                                               <ItemTemplate>
                                                 <asp:DropDownList ID="Cmb_Cuenta" runat="server"  Font-Size="X-Small" Width="100%">
                                                  </asp:DropDownList>
                                                </ItemTemplate >
                                                <HeaderStyle HorizontalAlign="left" Width="40%" Font-Size="XX-Small"/>
                                                <ItemStyle HorizontalAlign="left" Width="40%" Font-Size="XX-Small"/>
                                            </asp:TemplateField>
                                            <asp:TemplateField  HeaderText= "Autorizar">
                                                <HeaderStyle HorizontalAlign="center" Width="5%" Font-Size="X-Small"/>
                                                <ItemStyle  HorizontalAlign="Center" Width="5%"  Font-Size="X-Small"/>
                                                <ItemTemplate>
                                                    <asp:CheckBox id="Chk_Autorizado" runat="server" CssClass='<%# Eval("No_Deuda") %>'/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField  HeaderText= "Rechazar">
                                                <HeaderStyle HorizontalAlign="center" Width="5%" Font-Size="X-Small"/>
                                                <ItemStyle  HorizontalAlign="Center" Width="5%"  Font-Size="X-Small"/>
                                                <ItemTemplate>
                                                    <asp:CheckBox id="Chk_Rechazar" runat="server" onclick="Abrir_Popup2(this);" CssClass='<%# Eval("No_Deuda") %>'/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus">
                                                <HeaderStyle HorizontalAlign="Left" Width="1%" Font-Size="X-Small"/>
                                                <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="1%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Tipo_Deudor" HeaderText="deudor">
                                                <HeaderStyle HorizontalAlign="Left" Width="1%" Font-Size="X-Small"/>
                                                <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="1%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEUDOR_id" HeaderText="DEUDOR_id">
                                                <HeaderStyle HorizontalAlign="Left" Width="1%" Font-Size="X-Small"/>
                                                <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="1%" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbl_Solicitud" runat="server" 
                                                        Text='<%# Bind("No_Deuda") %>' Visible="false"></asp:Label>
                                                        <asp:Literal ID="Ltr_Inicio" runat="server"  
                                                        Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='position:static'&gt;&lt;td colspan='9'; align='right';&gt;" />
                                                    <asp:GridView ID="Grid_Datos_Solicitud" runat="server" AllowPaging="False" 
                                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="98%">
                                                        <Columns>
                                                             <asp:BoundField DataField="Tipo_Movimiento" HeaderText="Tipo Movimiento">
                                                                <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                                                <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Concepto" HeaderText="Concepto">
                                                                <HeaderStyle HorizontalAlign="Left" Width="50%" Font-Size="X-Small"/>
                                                                <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="50%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Importe" HeaderText="Monto" DataFormatString="{0:c}">
                                                                <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus">
                                                                <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                        <FooterStyle CssClass="GridPager" />
                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                        <PagerStyle CssClass="GridPager" />
                                                        <RowStyle CssClass="GridItem" />
                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                    </asp:GridView>
                                                    <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </center>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:HiddenField ID="Txt_Monto_Solicitud" runat="server" />
                            <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Proveedor" runat="server" />
                            <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Empleado" runat="server" />
                            <asp:HiddenField ID="Txt_Cuenta_Contable_reserva" runat="server" />
                            <asp:HiddenField ID="Txt_No_Reserva" runat="server" />
                            <asp:HiddenField ID="Txt_Rechazo" runat="server" />
                            <cc1:ModalPopupExtender ID="Mpe_Busqueda" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Contenedor"
                                PopupControlID="Pnl_Busqueda_Contenedor" TargetControlID="Btn_Comodin_Open" PopupDragHandleControlID="Pnl_Busqueda_Cabecera" 
                                CancelControlID="Btn_Comodin_Close" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                <asp:Button Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Close" runat="server" Text="" />
                                <asp:Button  Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Open" runat="server" Text="" OnClientClick="javascript:return false;" />
                            <asp:HiddenField ID="Txt_No_Solicitud_Autorizar" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
        <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
                    style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
                    <asp:Panel ID="Pnl_Busqueda_Cabecera" runat="server" 
                        style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                        <table width="99%">
                            <tr style="display:none;" id="Tr_Rechazar">
                                <td style="color:Black;font-size:12;font-weight:bold;">
                                   <asp:Image ID="Img_Informatcion_Rechazo" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                                     RECHAZO DE SOLICITUD 
                                </td>
                                <td align="right" style="width:10%;">
                                   <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                        ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClientClick="javascript:return Cerrar_Modal_Popup();"/>  
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>                                                                          
                           <div style="color: #5D7B9D">
                             <table width="100%">
                                <tr>
                                    <td align="left" style="text-align: left;" >                                    
                                        <asp:UpdatePanel ID="Upnl_Comentario" runat="server">
                                            <ContentTemplate>
                                                <asp:UpdateProgress ID="Progress_Upnl_Comentario" runat="server" AssociatedUpdatePanelID="Upnl_Comentario" DisplayAfter="0">
                                                    <ProgressTemplate>
                                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div></ProgressTemplate></asp:UpdateProgress><table width="100%">
                                                   <tr>
                                                        <td colspan="2">
                                                            <table style="width:80%;">
                                                              <tr>
                                                                <td align="left" >
                                                                  <asp:ImageButton ID="Img_Error_Busqueda" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                                    Width="24px" Height="24px" style="display:none" />
                                                                    <asp:Label ID="Lbl_Error_Busqueda" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" style="display:none"/>
                                                                </td>            
                                                              </tr>         
                                                            </table>  
                                                        </td>
                                                    </tr>     
                                                    <tr>
                                                        <td colspan="2">
                                                            <hr />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:15%">
                                                            *Comentarios
                                                        </td>
                                                        <td >
                                                            <asp:TextBox ID="Txt_Comentario" runat="server" TextMode="MultiLine" Width="99.5%" MaxLength="200" Height="50px" />
                                                           <cc1:FilteredTextBoxExtender ID="Fte_Txt_Comentario" runat="server" FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters"
                                                                TargetControlID="Txt_Comentario" ValidChars="áéíóúÁÉÍÓÚ., " InvalidChars="&lt;,&gt;,&amp;,',!,"/>
                                                        </td>
                                                    </tr>
                                                   <tr>
                                                        <td colspan="2">
                                                            <hr />
                                                        </td>
                                                    </tr>                                    
                                                    <tr id="Tr_Aceptar" style="display:block;"  >
                                                        <td style="width:100%;text-align:left;" colspan="2">
                                                            <center>
                                                               <asp:Button ID="Btn_Comentar" runat="server" Text="Aceptar" CssClass="button"  
                                                                CausesValidation="false"  Width="200px" OnClick="Btn_Comentar_Click"/> 
                                                                <asp:Button ID="Bnt_Cancelar" runat="server" Text="Cancelar" CssClass="button"
                                                                CausesValidation="false"  Width="200px" OnClick="Btn_Cancelar_Click"/> 
                                                            </center>
                                                        </td>
                                                    </tr>
                                                  </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                             </table>
                           </div>
                    </asp:Panel>
</asp:Content>