<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Cat_Con_Proveedores.aspx.cs" Inherits="paginas_compras_Frm_Cat_Con_Proveedores"
    Title="Cat�logo de Proveedores" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../../jquery/jquery-1.5.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">

        function Activar(Control) {
            if ($(Control).parent().attr('class') == "Acreedor") {
                var a = $("input:checkbox[id$=Chk_Cuenta_Acreedor]");
                if ($(a[0]).is(':checked') == true) {
                    document.getElementById("<%=Txt_Cuenta_Acreedor.ClientID%>").disabled = false;
                    document.getElementById("<%=Txt_Cuenta_Acreedor.ClientID%>").value = "";
                } else {
                    document.getElementById("<%=Txt_Cuenta_Acreedor.ClientID%>").disabled = true;
                    document.getElementById("<%=Txt_Cuenta_Acreedor.ClientID%>").value = "";
                }
            }
            if ($(Control).parent().attr('class') == "Deudor") {
                var a = $("input:checkbox[id$=Chk_Cuenta_Deudor]");
                if ($(a[0]).is(':checked') == true) {
                    document.getElementById("<%=Txt_Cuenta_Deudor.ClientID%>").disabled = false;
                    document.getElementById("<%=Txt_Cuenta_Deudor.ClientID%>").value = "";
                } else {
                    document.getElementById("<%=Txt_Cuenta_Deudor.ClientID%>").disabled = true;
                    document.getElementById("<%=Txt_Cuenta_Deudor.ClientID%>").value = "";
                }
            }
            if ($(Control).parent().attr('class') == "Nomina") {
                var a = $("input:checkbox[id$=Chk_Cuenta_Nomina]");
                if ($(a[0]).is(':checked') == true) {
                    document.getElementById("<%=Txt_Cuenta_Nomina.ClientID%>").disabled = false;
                    document.getElementById("<%=Txt_Cuenta_Nomina.ClientID%>").value = "";
                } else {
                    document.getElementById("<%=Txt_Cuenta_Nomina.ClientID%>").disabled = true;
                    document.getElementById("<%=Txt_Cuenta_Nomina.ClientID%>").value = "";
                }
            }
            if ($(Control).parent().attr('class') == "Proveedor") {
                var a = $("input:checkbox[id$=Chk_Cuenta_Proveedor]");
                if ($(a[0]).is(':checked') == true) {
                    document.getElementById("<%=Txt_Cuenta_Proveedor.ClientID%>").disabled = false;
                    document.getElementById("<%=Txt_Cuenta_Proveedor.ClientID%>").value = "";
                } else {
                    document.getElementById("<%=Txt_Cuenta_Proveedor.ClientID%>").disabled = true;
                    document.getElementById("<%=Txt_Cuenta_Proveedor.ClientID%>").value = "";
                }
            }
            if ($(Control).parent().attr('class') == "Predial") {
                var a = $("input:checkbox[id$=Chk_Cuenta_Predial]");
                if ($(a[0]).is(':checked') == true) {
                    document.getElementById("<%=Txt_Cuenta_Predial.ClientID%>").disabled = false;
                    document.getElementById("<%=Txt_Cuenta_Predial.ClientID%>").value = "";
                } else {
                    document.getElementById("<%=Txt_Cuenta_Predial.ClientID%>").disabled = true;
                    document.getElementById("<%=Txt_Cuenta_Predial.ClientID%>").value = "";
                }
            }
            if ($(Control).parent().attr('class') == "Judicial") {
                var a = $("input:checkbox[id$=Chk_Cuenta_Judicial]");
                if ($(a[0]).is(':checked') == true) {
                    document.getElementById("<%=Txt_Cuenta_Judicial.ClientID%>").disabled = false;
                    document.getElementById("<%=Txt_Cuenta_Judicial.ClientID%>").value = "";
                } else {
                    document.getElementById("<%=Txt_Cuenta_Judicial.ClientID%>").disabled = true;
                    document.getElementById("<%=Txt_Cuenta_Judicial.ClientID%>").value = "";
                }
            }
            if ($(Control).parent().attr('class') == "Contratista") {
                var a = $("input:checkbox[id$=Chk_Cuenta_Contratista]");
                if ($(a[0]).is(':checked') == true) {
                    document.getElementById("<%=Txt_Cuenta_Contratista.ClientID%>").disabled = false;
                    document.getElementById("<%=Txt_Cuenta_Contratista.ClientID%>").value = "";
                } else {
                    document.getElementById("<%=Txt_Cuenta_Contratista.ClientID%>").disabled = true;
                    document.getElementById("<%=Txt_Cuenta_Contratista.ClientID%>").value = "";
                }
            }
            if ($(Control).parent().attr('class') == "Anticipo") {
                var a = $("input:checkbox[id$=Chk_Cuenta_Anticipo]");
                if ($(a[0]).is(':checked') == true) {
                    document.getElementById("<%=Txt_Cuenta_Anticipo.ClientID%>").disabled = false;
                    document.getElementById("<%=Txt_Cuenta_Anticipo.ClientID%>").value = "";
                } else {
                    document.getElementById("<%=Txt_Cuenta_Anticipo.ClientID%>").disabled = true;
                    document.getElementById("<%=Txt_Cuenta_Anticipo.ClientID%>").value = "";
                }
            }

        }
        function Activar_Banco(Control) {
            if ($(Control).parent().attr('class') == "Banco") {
                var a = $("input:checkbox[id$=Chk_Proveedor_Bancario]");
                if ($(a[0]).is(':checked') == true) {
                    document.getElementById("<%=Cmb_Proveedor_Bancario.ClientID%>").disabled = false;
                    document.getElementById("<%=Cmb_Proveedor_Bancario.ClientID%>").value = 0;
                } else {
                document.getElementById("<%=Cmb_Proveedor_Bancario.ClientID%>").disabled = true;
                document.getElementById("<%=Cmb_Proveedor_Bancario.ClientID%>").value = 0;
                }
            }
        }
        function Abrir_Modal_Popup() {
            $find('Busqueda_Cuentas_Contables').show();
            return false;
        }
        function Limpiar_Ctlr() {
            document.getElementById("<%=Txt_Busqueda_Cuenta_Contable.ClientID%>").value = "";
            return false;
        }
    
    </script>

    <style type="text/css">
        .style1
        {
            width: 20%;
            height: 26px;
        }
        .style2
        {
            width: 30%;
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="True">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                   <%-- <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>--%>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Contenido" style="width: 97%; height: 100%;">
                <table width="97%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td class="label_titulo">
                            Cat&aacute;logo Acreedores y Deudores
                        </td>
                    </tr>
                    <%--Fila de div de Mensaje de Error --%>
                    <tr>
                        <td>
                            <div id="Div_Contenedor_Msj_Error" style="width: 95%; font-size: 9px;" runat="server"
                                visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td align="left" style="font-size: 12px; color: Red; font-family: Tahoma; text-align: left;">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                        </td>
                                        <td style="font-size: 9px; width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" Visible="true" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" valign="middle">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                            CssClass="Img_Button" ToolTip="Nuevo" TabIndex="1" OnClick="Btn_Nuevo_Click" />
                                        <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                            CssClass="Img_Button" ToolTip="Modificar" TabIndex="2" OnClick="Btn_Modificar_Click" />
                                        <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                            ToolTip="Inicio" TabIndex="3" OnClick="Btn_Salir_Click" />
                                    </td>
                                    <td align="right">
                                        <asp:LinkButton ID="Btn_Busqueda_Avanzada" runat="server" OnClick="Btn_Busqueda_Avanzada_Click">B&uacute;squeda Avanzada</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="98%" class="estilo_fuente">
                                <tr>
                                    <td>
                                        <div id="Div_Busqueda_Avanzada" runat="server" style="overflow: auto; width: 99%;
                                            vertical-align: top; border-style: outset; border-color: Silver;">
                                            <table width="99%" class="estilo_fuente">
                                                <tr style="background-color: Silver; color: Black; font-size: 12; font-weight: bold;
                                                    border-style: outset;">
                                                    <td colspan="4" align="right">
                                                        <asp:ImageButton ID="Btn_Buscar" runat="server" ToolTip="Consultar" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                            OnClick="Btn_Buscar_Click" />
                                                        <asp:ImageButton ID="Btn_Limpiar_Busqueda_Avanzada" runat="server" ToolTip="Limpiar"
                                                            ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" OnClick="Btn_Limpiar_Busqueda_Avanzada_Click" />
                                                        <asp:ImageButton ID="Btn_Cerrar_Busqueda_Avanzada" runat="server" ToolTip="Cerrar"
                                                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Busqueda_Avanzada_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; width: 20%;">
                                                        Padr&oacute;n Acreedor/Deudor
                                                    </td>
                                                    <td style="text-align: left; width: 30%;">
                                                        <asp:TextBox ID="Txt_Busqueda_Padron_Proveedor" runat="server" Width="100%" TabIndex="3"
                                                            MaxLength="10"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" WatermarkCssClass="watermarked"
                                                            WatermarkText="<Ingrese el Padron de Proveedor>" TargetControlID="Txt_Busqueda_Padron_Proveedor" />
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="Txt_Busqueda_Padron_Proveedor"
                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,." Enabled="True" InvalidChars="<,>,&,',!,">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </td>
                                                    <td style="text-align: left; width: 20%;">
                                                        Nombre Comercial
                                                    </td>
                                                    <td style="text-align: left; width: 30%;">
                                                        <asp:TextBox ID="Txt_Busqueda_Nombre_Comercial" runat="server" TabIndex="4" Width="100%"
                                                            MaxLength="100"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Rol_ID" runat="server" WatermarkCssClass="watermarked"
                                                            WatermarkText="<Ingrese nombre>" TargetControlID="Txt_Busqueda_Nombre_Comercial" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        RFC
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Busqueda_RFC" runat="server" Width="100%" TabIndex="5" MaxLength="20"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkCssClass="watermarked"
                                                            WatermarkText="<Ingrese el RFC>" TargetControlID="Txt_Busqueda_RFC" />
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="Txt_Busqueda_RFC"
                                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="������������-"
                                                            Enabled="True" InvalidChars="'">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </td>
                                                    <td>
                                                        Raz&oacute;n Social
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Busqueda_Razon_Social" runat="server" Width="100%" TabIndex="6"
                                                            MaxLength="100"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" WatermarkCssClass="watermarked"
                                                            WatermarkText="<Ingrese Razon Social>" TargetControlID="Txt_Busqueda_Razon_Social" />
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" WatermarkCssClass="watermarked"
                                                            WatermarkText="<Ingrese el RFC>" TargetControlID="Txt_Busqueda_RFC" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Estatus
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="Cmb_Busqueda_Estatus" runat="server" Width="98%">
                                                            <asp:ListItem>--SELECCIONAR--</asp:ListItem>
                                                            <asp:ListItem Value="ACTIVO">ACTIVO</asp:ListItem>
                                                            <asp:ListItem Value="INACTIVO">INACTIVO</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="99%" class="estilo_fuente">
                                <tr>
                                    <td style="text-align: left;" class="style1">
                                        Padr&oacute;n Acreedor/Deudor
                                    </td>
                                    <td style="text-align: left;" class="style2">
                                        <asp:TextBox ID="Txt_Proveedor_ID" runat="server" Enabled="false" ReadOnly="true"
                                            Width="100%"></asp:TextBox>
                                    </td>
                                    <td style="text-align: left;" class="style1">
                                        Fecha de Registro
                                    </td>
                                    <td style="text-align: left;" class="style2">
                                        <asp:TextBox ID="Txt_Fecha_Registro" runat="server" Enabled="false" ReadOnly="true"
                                            Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        *Raz&oacute;n Social
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Razon_Social" runat="server" MaxLength="400" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Txt_Razon_Social_FilteredTextBoxExtender" runat="server"
                                            TargetControlID="Txt_Razon_Social" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                            ValidChars="������������. ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        *Nombre Comercial
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Nombre_Comercial" runat="server" MaxLength="400" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Txt_Nombre_Comercial_FilteredTextBoxExtender1" runat="server"
                                            TargetControlID="Txt_Nombre_Comercial" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                            ValidChars="������������. ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        *Representante Legal
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Representante_Legal" runat="server" MaxLength="250" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Txt_Representate_LegalFilteredTextBoxExtender1"
                                            runat="server" TargetControlID="Txt_Representante_Legal" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                            ValidChars="������������. ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        *Contacto
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Contacto" runat="server" MaxLength="100" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Txt_ContactoFilteredTextBoxExtender1" runat="server"
                                            TargetControlID="Txt_Contacto" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                            ValidChars="������������. ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        *RFC
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_RFC" runat="server" MaxLength="20" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Txt_RFCFilteredTextBoxExtender1" runat="server"
                                            TargetControlID="Txt_RFC" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                            ValidChars="������������. -">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        *Estatus
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="100%" TabIndex="10">
                                            <asp:ListItem Value="ACTIVO">ACTIVO</asp:ListItem>
                                            <asp:ListItem Value="INACTIVO">INACTIVO</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        CURP
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_CURP" runat="server" MaxLength="20" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filtered_Txt_CURP" runat="server" TargetControlID="Txt_CURP"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="������������. ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        *Tipo Acreedor/Deudor
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="Cmb_Tipo_Proveedor" runat="server" Width="100%" TabIndex="10">
                                            <asp:ListItem Value="0"><- SELECCIONE -></asp:ListItem>
                                            <asp:ListItem Value="CONTABILIDAD">CONTABILIDAD</asp:ListItem>
                                            <asp:ListItem Value="COMPRAS">COMPRAS</asp:ListItem>
                                            <asp:ListItem Value="TALLER">TALLER</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        *Persona
                                    </td>
                                    <td colspan="3">
                                        <asp:CheckBox ID="Chk_Fisica" Text="Fisica" runat="server" OnCheckedChanged="Chk_Fisica_CheckedChanged"
                                            AutoPostBack="true" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:CheckBox ID="Chk_Moral" Text="Moral" runat="server" OnCheckedChanged="Chk_Moral_CheckedChanged"
                                            AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        *Calle y N&uacute;mero
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Direccion" runat="server" MaxLength="250" Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        *Colonia
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Colonia" runat="server" MaxLength="50" Width="99%"></asp:TextBox>
                                    </td>
                                    <td>
                                        *Ciudad
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Ciudad" runat="server" MaxLength="50" Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        *Estado
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Estado" runat="server" MaxLength="30" Width="99%"></asp:TextBox>
                                    </td>
                                    <td>
                                        *CP
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Codigo_Postal" runat="server" MaxLength="5" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Txt_CP_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Codigo_Postal"
                                            FilterType="Numbers">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        *Tel&eacute;fono 1
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Telefono1" runat="server" MaxLength="20" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Telefono1" runat="server" TargetControlID="Txt_Telefono1"
                                            FilterType="Numbers">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        Tel&eacute;fono 2
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Telefono2" runat="server" MaxLength="20" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Telefono2" runat="server" TargetControlID="Txt_Telefono2"
                                            FilterType="Numbers">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Nextel
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Nextel" runat="server" MaxLength="20" Width="99%"></asp:TextBox>
                                    </td>
                                    <td>
                                        Fax
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Fax" runat="server" MaxLength="25" Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tipo Pago
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="Cmb_Tipo_Pago" runat="server" Width="100%">
                                            <asp:ListItem>--SELECCIONAR--</asp:ListItem>
                                            <asp:ListItem Value="CREDITO">Cr&eacute;dito</asp:ListItem>
                                            <asp:ListItem Value="CONTADO">Contado</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        Dias Cr&eacute;dito
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Dias_Credito" runat="server" MaxLength="3" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Txt_Dias_Credito_FilteredTextBoxExtender" runat="server"
                                            TargetControlID="Txt_Dias_Credito" FilterType="Numbers">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Forma de Pago
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="Cmb_Forma_Pago" runat="server" Width="100%" TabIndex="24">
                                            <asp:ListItem>--SELECCIONAR--</asp:ListItem>
                                            <asp:ListItem Value="TRANSFERENCIA">Transferencia</asp:ListItem>
                                            <asp:ListItem Value="CHEQUE">Cheque</asp:ListItem>
                                            <asp:ListItem Value="EFECTIVO">Efectivo</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        *Correo
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Correo" runat="server" MaxLength="30" Width="85%"></asp:TextBox>
                                        <asp:ImageButton ID="Ibtn_agregar_Correo" runat="server" Width="16px" Height="16px" 
                                            ImageUrl="~/paginas/imagenes/paginas/plus.gif" 
                                            onclick="Ibtn_agregar_Correo_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Lbl_Proveedor_Bancario" runat="server" Text="Proveedor Bancario"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                         <asp:CheckBox ID="Chk_Proveedor_Bancario" runat="server" Text=""  CssClass="Banco"  onclick="Activar_Banco(this);"/>
                                        <asp:DropDownList ID="Cmb_Proveedor_Bancario" runat="server" Width="96%">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td>
                                        *Password
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Password" runat="server" MaxLength="20" TextMode="Password"
                                            Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:GridView ID="Grid_Correos" runat="server" AutoGenerateColumns="False" CssClass="GridView_1"
                                            GridLines="None" DataKeyNames="correo" Style="white-space: normal" Width="100%">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="Btn_Eliminar_Correo" runat="server" CssClass="Img_Button" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                            ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" OnClick="Btn_Eliminar_Correo_Click"
                                                            ToolTip="Eliminar" Height="16px" Width="16px" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="0px" />
                                                    <ItemStyle Font-Size="X-Small" Width="5%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="correo" HeaderText="correo">
                                                    <HeaderStyle HorizontalAlign="Left" Width="95%" />
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="95%" />
                                                </asp:BoundField>
                                            </Columns>
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <PagerStyle CssClass="GridHeader" />
                                            <RowStyle CssClass="GridItem" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Comentarios
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Comentarios" runat="server" MaxLength="150" TextMode="MultiLine"
                                            Width="99%" Height="50px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &Uacute;ltima Actualizaci&oacute;n
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Ultima_Actualizacion" runat="server" MaxLength="100" enable="false"
                                            Width="99%"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="Chk_Actualizacion" runat="server" AutoPostBack="true" OnCheckedChanged="Chk_Actualizacion_CheckedChanged" />
                                    </td>
                                </tr>
                                <%-- <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Panel1" runat="server" GroupingText="Datos Banco" Width="98%" BackColor="White">
                                        <table width="99.5%"  border="0" cellspacing="0" class="estilo_fuente">              
                                            <tr>
                                                <td style="width:20%; text-align:left; cursor:default;">
                                                    Banco 
                                                </td>
                                                
                                                <td style="width:30%; text-align:left; cursor:default;">
                                                    <asp:TextBox ID="Txt_Banco_Proveedor" runat="server" Width="98%" ></asp:TextBox>
                                                     <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Empleado" runat="server" TargetControlID="Txt_Banco_Proveedor"
                                                        FilterType="Custom, UppercaseLetters, LowercaseLetters"/>
                                                    <asp:DropDownList ID="Cmb_Banco" runat="server" Width="98%"></asp:DropDownList>                       
                                               </td> 
                                                
                                                 <td style="width:20%; text-align:left; cursor:default;">
                                                    Banco Acredor/Deudor
                                                </td>
                                                <td style="width:30%; text-align:left; cursor:default;">
                                                    <asp:TextBox ID="Txt_Banco_ID" runat="server"  Width="98%" TabIndex="0" MaxLength="20"></asp:TextBox>
                                                     <cc1:FilteredTextBoxExtender ID="Filt_Txt_Banco_ID" runat="server" 
                                                    TargetControlID="Txt_Banco_ID" FilterType="Numbers" ValidChars="0123456789" />
                                                </td>
                                                
                                           </tr> 
                                        </table>
                                    
                                    <table width="99.5%"  border="0" cellspacing="0" class="estilo_fuente">  
                                       <tr>
                                            <td style="width:20%; text-align:left; cursor:default;">
                                                Clabe
                                            </td>
                                            <td style="width:30%; text-align:left; cursor:default;">
                                                <asp:TextBox ID="Txt_Clabe" runat="server" Width="98%" TabIndex="0" MaxLength="18"/>
                                                <cc1:FilteredTextBoxExtender ID="Filt_Clabe" runat="server" 
                                                    TargetControlID="Txt_Clabe" FilterType="Numbers" ValidChars="0123456789" />                        
                                            </td> 
                                            <td style="width:20%; text-align:left; cursor:default;">
                                                Numero Cuenta
                                            </td>
                                            <td style="width:30%; text-align:left; cursor:default;">
                                                <asp:TextBox ID="Txt_Cuenta_Banco" runat="server" Width="98%" TabIndex="0" MaxLength="10"/>
                                                <cc1:FilteredTextBoxExtender ID="Filt_Cuenta_Banco" runat="server" 
                                                    TargetControlID="Txt_Cuenta_Banco" FilterType="Numbers" ValidChars="0123456789" />                        
                                            </td> 
                                       </tr> 
                                       
                                    </table>
                                </asp:Panel>
                                </td>
                            </tr>--%>
                                <tr>
                                    <td colspan="4">
                                        <asp:Panel ID="Pnl_Datos_Cuenta" runat="server" GroupingText="Datos Cuentas" Width="98%"
                                            BackColor="White">
                                            <table width="99.5%" border="0" cellspacing="0" class="estilo_fuente">
                                                <tr>
                                                    <td style="text-align: left; width: 20%;">
                                                        <b></b>
                                                    </td>
                                                    <td colspan="3" style="text-align: left; width: 80%;">
                                                        B&uacute;squeda Avanzada de Cuentas
                                                        <asp:UpdatePanel ID="Udp_Busqueda_Cuenta_Modal" runat="server" UpdateMode="Conditional"
                                                            RenderMode="Inline">
                                                            <ContentTemplate>
                                                                <asp:ImageButton ID="Btn_Mostrar_Busqueda_Cuentas" runat="server" ToolTip="Busqueda Avanzada"
                                                                    TabIndex="1" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Height="17px"
                                                                    Width="17px" OnClientClick="javascript:return Abrir_Modal_Popup();" CausesValidation="false" />
                                                                <cc1:ModalPopupExtender ID="Mpe_Busqueda_Cuenta_Contable" runat="server" BackgroundCssClass="popUpStyle"
                                                                    BehaviorID="Busqueda_Cuentas_Contables" PopupControlID="Pnl_Busqueda_Cuentas_Contables"
                                                                    TargetControlID="Btn_Abrir" CancelControlID="Btn_Cerrar" DropShadow="True" DynamicServicePath=""
                                                                    Enabled="True" />
                                                                <asp:Button Style="background-color: transparent; border-style: none; width: .5px;"
                                                                    ID="Btn_Abrir" runat="server" Text="" />
                                                                <asp:Button Style="background-color: transparent; border-style: none; width: .5px;"
                                                                    ID="Btn_Cerrar" runat="server" Text="" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="Chk_Cuenta_Acreedor" runat="server" Text="C. Acreedor(A)" Onclick="Activar(this);"
                                                            CssClass="Acreedor" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Cuenta_Acreedor" runat="server" Enabled="false" MaxLength="9"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Cuenta_Acreedor" runat="server" TargetControlID="Txt_Cuenta_Acreedor"
                                                            FilterType="Numbers" />
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="Chk_Cuenta_Deudor" runat="server" Text="C. Deudor(D)" onclick="Activar(this);"
                                                            CssClass="Deudor" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Cuenta_Deudor" runat="server" Enabled="false" MaxLength="9"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Cuenta_Deudor" runat="server" TargetControlID="Txt_Cuenta_Deudor"
                                                            FilterType="Numbers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="Chk_Cuenta_Proveedor" runat="server" Text="C. Proveedor(P)" onclick="Activar(this);"
                                                            CssClass="Proveedor" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Cuenta_Proveedor" runat="server" Enabled="false" MaxLength="9"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Cuenta_Proveedor" runat="server" TargetControlID="Txt_Cuenta_Proveedor"
                                                            FilterType="Numbers" />
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="Chk_Cuenta_Nomina" runat="server" Text="C. Nomina(N)" onclick="Activar(this);"
                                                            CssClass="Nomina" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Cuenta_Nomina" runat="server" Enabled="false" MaxLength="9"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Cuenta_Nomina" runat="server" TargetControlID="Txt_Cuenta_Nomina"
                                                            FilterType="Numbers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="Chk_Cuenta_Contratista" runat="server" Text="C. Contratista(C)"
                                                            onclick="Activar(this);" CssClass="Contratista" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Cuenta_Contratista" runat="server" Enabled="false" MaxLength="9"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Cuenta_Contratista" runat="server" TargetControlID="Txt_Cuenta_Contratista"
                                                            FilterType="Numbers" />
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="Chk_Cuenta_Judicial" runat="server" Text="C. Judicial(J)" onclick="Activar(this);"
                                                            CssClass="Judicial" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Cuenta_Judicial" runat="server" Enabled="false" MaxLength="9"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Cuenta_Judicial" runat="server" TargetControlID="Txt_Cuenta_Judicial"
                                                            FilterType="Numbers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="Chk_Cuenta_Predial" runat="server" Text="C. Predial(Z)" onclick="Activar(this);"
                                                            CssClass="Predial" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Cuenta_Predial" runat="server" Enabled="false" MaxLength="9"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Cuenta_Predial" runat="server" TargetControlID="Txt_Cuenta_Predial"
                                                            FilterType="Numbers" />
                                                    </td>
                                                    <%--<td>
                                                <asp:CheckBox ID="Chk_Cuenta_Anticipo" runat="server" Text="C. Anticipo Deudor(T)" onclick="Activar(this);" CssClass="Anticipo"/>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_Cuenta_Anticipo" runat="server" Enabled="false" MaxLength="9"></asp:TextBox>                                                
                                                 <cc1:FilteredTextBoxExtender ID="Filt_Txt_Cuenta_Anticipo" runat="server" 
                                                    TargetControlID="Txt_Cuenta_Anticipo" FilterType="Numbers"/>
                                            </td>--%>
                                                    <td>
                                                        <asp:CheckBox ID="Chk_Cuenta_Anticipo" runat="server" Text="C. Anticipo Deudor(T)"
                                                            onclick="Activar(this);" CssClass="Anticipo" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Txt_Cuenta_Anticipo" runat="server" Enabled="false" MaxLength="9"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Cuenta_Anticipo" runat="server" TargetControlID="Txt_Cuenta_Anticipo"
                                                            FilterType="Numbers" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <cc1:TabContainer ID="Tab_Giros" runat="server" ActiveTabIndex="1" Width="99%">
                                            <cc1:TabPanel ID="Tab_Giro_Concepto" runat="server" HeaderText="TabPanel1" Style="vertical-align: top;">
                                                <HeaderTemplate>
                                                    Conceptos</HeaderTemplate>
                                                <ContentTemplate>
                                                    <table width="99%">
                                                        <tr>
                                                            <td align="center" style="text-align: right; width: 100%; vertical-align: top;">
                                                                <asp:GridView ID="Grid_Conceptos_Proveedor" runat="server" Style="white-space: normal"
                                                                    AutoGenerateColumns="False" Width="100%" CssClass="GridView_1" GridLines="None">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="CONCEPTO_ID" HeaderText="ID">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                                            <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="80%" />
                                                                            <ItemStyle HorizontalAlign="Left" Width="80%" Font-Size="X-Small" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="GridItem" />
                                                                    <PagerStyle CssClass="GridHeader" />
                                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                                    <HeaderStyle CssClass="GridHeader" />
                                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="Tab_Partidas" runat="server" HeaderText="TabPanel1" Style="vertical-align: top;">
                                                <HeaderTemplate>
                                                    Partidas</HeaderTemplate>
                                                <ContentTemplate>
                                                    <table width="99%">
                                                        <tr>
                                                            <td>
                                                                Concepto
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="Cmb_Conceptos" runat="server" AutoPostBack="True" Width="100%"
                                                                    OnSelectedIndexChanged="Cmb_Conceptos_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top; width: 15%">
                                                                Partidas Generales
                                                            </td>
                                                            <td style="vertical-align: top; width: 85%">
                                                                <asp:DropDownList ID="Cmb_Partidas_Generales" runat="server" Width="90%" />
                                                                &nbsp;<asp:ImageButton ID="Btn_Agregar_Partida" runat="server" CssClass="Img_Button"
                                                                    ImageUrl="~/paginas/imagenes/paginas/sias_add.png" TabIndex="1" ToolTip="Nuevo"
                                                                    OnClick="Btn_Agregar_Partida_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="text-align: right; width: 100%; vertical-align: top;" colspan="2">
                                                                <asp:GridView ID="Grid_Partidas_Generales" runat="server" AutoGenerateColumns="False"
                                                                    CssClass="GridView_1" GridLines="None" OnSelectedIndexChanged="Grid_Partidas_Generales_SelectedIndexChanged"
                                                                    Style="white-space: normal" Width="100%" EnableModelValidation="True">
                                                                    <Columns>
                                                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png"
                                                                            Text="Quitar">
                                                                            <ItemStyle Width="5%" />
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="PARTIDA_GENERICA_ID" HeaderText="ID">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="15%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="PARTIDA" HeaderText="Partida">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="80%" />
                                                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="80%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CONCEPTO_ID" HeaderText="ID" Visible="False">
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                                    <HeaderStyle CssClass="GridHeader" />
                                                                    <PagerStyle CssClass="GridHeader" />
                                                                    <RowStyle CssClass="GridItem" />
                                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="Tab_Historial_Actualizaciones" runat="server" Style="vertical-align: top;">
                                                <HeaderTemplate>
                                                    Historial Actualizaciones</HeaderTemplate>
                                                <ContentTemplate>
                                                    <table width="99%">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="Grid_Historial_Act" runat="server" Style="white-space: normal"
                                                                    AutoGenerateColumns="False" Width="100%" CssClass="GridView_1" GridLines="None">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="FECHA_ACTUALIZACION" HeaderText="Fecha Actualizaci�n">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="USUARIO_CREO" HeaderText="Usuario Actualizo">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="80%" />
                                                                            <ItemStyle HorizontalAlign="Left" Width="80%" Font-Size="X-Small" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="GridItem" />
                                                                    <PagerStyle CssClass="GridHeader" />
                                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                                    <HeaderStyle CssClass="GridHeader" />
                                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                        </cc1:TabContainer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="Div_Proveedores" runat="server" style="overflow: auto; height: 200px; width: 99%;
                                vertical-align: top; border-style: outset; border-color: Silver;">
                                <asp:GridView ID="Grid_Proveedores" runat="server" AutoGenerateColumns="False" CssClass="GridView_1"
                                    GridLines="None" AllowSorting="True" Width="100%" DataKeyNames="Proveedor_ID"
                                    OnSorting="Grid_Proveedores_Sorting" OnSelectedIndexChanged="Grid_Proveedores_SelectedIndexChanged"
                                    OnRowDataBound="Grid_Proveedores_RowDataBound">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="5%" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="Proveedor_ID" HeaderText="Padr&oacute;n Proveedor" Visible="True">
                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Nombre" HeaderText="Raz&oacute;n Social" Visible="True">
                                            <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                            <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Compania" HeaderText="Nombre Comercial" Visible="True">
                                            <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                            <ItemStyle HorizontalAlign="Left" Width="40%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="True">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Alerta" runat="server" ImageUrl="~/paginas/imagenes/gridview/circle_red.png"
                                                    Visible="false" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CUENTA"></asp:BoundField>
                                        <asp:BoundField DataField="CUENTA_CONTABLE_ID"></asp:BoundField>
                                        <asp:BoundField DataField="CUENTA_PROVEEDOR_ID"></asp:BoundField>
                                        <asp:BoundField DataField="CUENTA_CONTRATISTA_ID"></asp:BoundField>
                                        <asp:BoundField DataField="CUENTA_JUDICIAL_ID"></asp:BoundField>
                                        <asp:BoundField DataField="CUENTA_NOMINA_ID"></asp:BoundField>
                                        <asp:BoundField DataField="CUENTA_ACREEDOR_ID"></asp:BoundField>
                                        <asp:BoundField DataField="CUENTA_PREDIAL_ID"></asp:BoundField>
                                        <asp:BoundField DataField="CUENTA_DEUDOR_ID"></asp:BoundField>
                                        <asp:BoundField DataField="Cuenta_Anticipo_ID"></asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <script type="text/javascript" language="javascript">
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(fin_peticion);

                function fin_peticion() {
                    function Activar_Banco(Control) {
                        if ($(Control).parent().attr('class') == "Banco") {
                            var a = $("input:checkbox[id$=Chk_Proveedor_Bancario]");
                            if ($(a[0]).is(':checked') == true) {
                                document.getElementById("<%=Cmb_Proveedor_Bancario.ClientID%>").disabled = false;
                                document.getElementById("<%=Cmb_Proveedor_Bancario.ClientID%>").value = 0;
                            } else {
                                document.getElementById("<%=Cmb_Proveedor_Bancario.ClientID%>").disabled = true;
                                document.getElementById("<%=Cmb_Proveedor_Bancario.ClientID%>").value = 0;
                            }
                        }
                    }
                    function Activar(Control) {
                        if ($(Control).parent().attr('class') == "Acreedor") {
                            var a = $("input:checkbox[id$=Chk_Cuenta_Acreedor]");
                            if ($(a[0]).is(':checked') == true) {
                                document.getElementById("<%=Txt_Cuenta_Acreedor.ClientID%>").disabled = false;
                                document.getElementById("<%=Txt_Cuenta_Acreedor.ClientID%>").value = "";
                            } else {
                                document.getElementById("<%=Txt_Cuenta_Acreedor.ClientID%>").disabled = true;
                                document.getElementById("<%=Txt_Cuenta_Acreedor.ClientID%>").value = "";
                            }
                        }
                        if ($(Control).parent().attr('class') == "Deudor") {
                            var a = $("input:checkbox[id$=Chk_Cuenta_Deudor]");
                            if ($(a[0]).is(':checked') == true) {
                                document.getElementById("<%=Txt_Cuenta_Deudor.ClientID%>").disabled = false;
                                document.getElementById("<%=Txt_Cuenta_Deudor.ClientID%>").value = "";
                            } else {
                                document.getElementById("<%=Txt_Cuenta_Deudor.ClientID%>").disabled = true;
                                document.getElementById("<%=Txt_Cuenta_Deudor.ClientID%>").value = "";
                            }
                        }
                        if ($(Control).parent().attr('class') == "Nomina") {
                            var a = $("input:checkbox[id$=Chk_Cuenta_Nomina]");
                            if ($(a[0]).is(':checked') == true) {
                                document.getElementById("<%=Txt_Cuenta_Nomina.ClientID%>").disabled = false;
                                document.getElementById("<%=Txt_Cuenta_Nomina.ClientID%>").value = "";
                            } else {
                                document.getElementById("<%=Txt_Cuenta_Nomina.ClientID%>").disabled = true;
                                document.getElementById("<%=Txt_Cuenta_Nomina.ClientID%>").value = "";
                            }
                        }
                        if ($(Control).parent().attr('class') == "Proveedor") {
                            var a = $("input:checkbox[id$=Chk_Cuenta_Proveedor]");
                            if ($(a[0]).is(':checked') == true) {
                                document.getElementById("<%=Txt_Cuenta_Proveedor.ClientID%>").disabled = false;
                                document.getElementById("<%=Txt_Cuenta_Proveedor.ClientID%>").value = "";
                            } else {
                                document.getElementById("<%=Txt_Cuenta_Proveedor.ClientID%>").disabled = true;
                                document.getElementById("<%=Txt_Cuenta_Proveedor.ClientID%>").value = "";
                            }
                        }
                        if ($(Control).parent().attr('class') == "Predial") {
                            var a = $("input:checkbox[id$=Chk_Cuenta_Predial]");
                            if ($(a[0]).is(':checked') == true) {
                                document.getElementById("<%=Txt_Cuenta_Predial.ClientID%>").disabled = false;
                                document.getElementById("<%=Txt_Cuenta_Predial.ClientID%>").value = "";
                            } else {
                                document.getElementById("<%=Txt_Cuenta_Predial.ClientID%>").disabled = true;
                                document.getElementById("<%=Txt_Cuenta_Predial.ClientID%>").value = "";
                            }
                        }
                        if ($(Control).parent().attr('class') == "Judicial") {
                            var a = $("input:checkbox[id$=Chk_Cuenta_Judicial]");
                            if ($(a[0]).is(':checked') == true) {
                                document.getElementById("<%=Txt_Cuenta_Judicial.ClientID%>").disabled = false;
                                document.getElementById("<%=Txt_Cuenta_Judicial.ClientID%>").value = "";
                            } else {
                                document.getElementById("<%=Txt_Cuenta_Judicial.ClientID%>").disabled = true;
                                document.getElementById("<%=Txt_Cuenta_Judicial.ClientID%>").value = "";
                            }
                        }
                        if ($(Control).parent().attr('class') == "Contratista") {
                            var a = $("input:checkbox[id$=Chk_Cuenta_Contratista]");
                            if ($(a[0]).is(':checked') == true) {
                                document.getElementById("<%=Txt_Cuenta_Contratista.ClientID%>").disabled = false;
                                document.getElementById("<%=Txt_Cuenta_Contratista.ClientID%>").value = "";
                            } else {
                                document.getElementById("<%=Txt_Cuenta_Contratista.ClientID%>").disabled = true;
                                document.getElementById("<%=Txt_Cuenta_Contratista.ClientID%>").value = "";
                            }
                        }
                        if ($(Control).parent().attr('class') == "Anticipo") {
                            var a = $("input:checkbox[id$=Chk_Cuenta_Anticipo]");
                            if ($(a[0]).is(':checked') == true) {
                                document.getElementById("<%=Txt_Cuenta_Anticipo.ClientID%>").disabled = false;
                                document.getElementById("<%=Txt_Cuenta_Anticipo.ClientID%>").value = "";
                            } else {
                                document.getElementById("<%=Txt_Cuenta_Anticipo.ClientID%>").disabled = true;
                                document.getElementById("<%=Txt_Cuenta_Anticipo.ClientID%>").value = "";
                            }
                        }

                    }
                }
            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
    <%--//BUSCAR CUENTA CONTABLE--%>
    <asp:Panel ID="Pnl_Busqueda_Cuentas_Contables" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="650px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Cabecera_2" runat="server" Style="cursor: move; background-color: Silver;
            color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Cuentas Contables
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Modal" CausesValidation="false" runat="server" Style="cursor: pointer;"
                            ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png"
                            OnClick="Btn_Cerrar_Modal_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Cuentas_Contables" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Cuentas" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Cuentas_Contables"
                                    DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <table width="100%">
                                    <tr>
                                        <td colspan="4">
                                            <table style="width: 80%;">
                                                <tr>
                                                    <td align="left">
                                                        <asp:ImageButton ID="Img_Error_Busqueda_Cienta_Contable" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                            Width="24px" Height="24px" Visible="false" />
                                                        <asp:Label ID="Lbl_Error_Busqueda_Cuenta_Contable" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"
                                                            Visible="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_2" runat="server" OnClientClick="javascript:return Limpiar_Ctlr();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            *Tipo Cuenta
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="Cmb_Tipo_Cuenta" runat="server" Width="100%" TabIndex="24">
                                                <asp:ListItem>--SELECCIONAR--</asp:ListItem>
                                                <asp:ListItem Value="Acreedor">Acreedor</asp:ListItem>
                                                <asp:ListItem Value="Proveedor">Proveedor</asp:ListItem>
                                                <asp:ListItem Value="Contratista">Contratista</asp:ListItem>
                                                <asp:ListItem Value="Predial">Predial</asp:ListItem>
                                                <asp:ListItem Value="Deudor">Deudor</asp:ListItem>
                                                <asp:ListItem Value="Nomina">Nomina</asp:ListItem>
                                                <asp:ListItem Value="Judicial">Judicial</asp:ListItem>
                                                <asp:ListItem Value="Anticipo">Anticipo Deudor</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            Descripci&oacute;n
                                        </td>
                                        <td colspan="3" style="text-align: left; font-size: 11px;">
                                            <asp:TextBox ID="Txt_Busqueda_Cuenta_Contable" runat="server" Width="98%" TabIndex="11"
                                                MaxLength="100" />
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Cuenta" runat="server" TargetControlID="Txt_Busqueda_Cuenta_Contable"
                                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" InvalidChars="&lt;,&gt;,&amp;,',!,"
                                                ValidChars=" ������������.">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td colspan="2" style="width: 50%; text-align: left; font-size: 11px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%; text-align: left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Cuenta_Contable" runat="server" Text="Buscar Cuenta Contable"
                                                    CssClass="button" CausesValidation="false" Width="300px" TabIndex="15" OnClick="Btn_Busqueda_Cuentas_Popup_Click" />
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <div id="Div_Busqueda_Cuentas" runat="server" style="overflow: auto; max-height: 200px;
                                                width: 99%">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="Grid_Cuentas_Contables" runat="server" CssClass="GridView_1" AutoGenerateColumns="False"
                                                                GridLines="None" Width="99.9%" AllowSorting="True" HeaderStyle-CssClass="tblHead">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="Btn_Seleccionar_Fuente" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                                CommandArgument='<%# Eval("CUENTA") %>' OnClick="Btn_Seleccionar_Cuenta_Click" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Cuenta_Contable_ID" HeaderText="Cuenta_Contable_ID">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CUENTA" HeaderText="Cuenta" SortExpression="Cuenta">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripci&oacute;n" SortExpression="Descripcion">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="60%" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <SelectedRowStyle CssClass="GridSelected" />
                                                                <PagerStyle CssClass="GridHeader" />
                                                                <HeaderStyle CssClass="tblHead" />
                                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
