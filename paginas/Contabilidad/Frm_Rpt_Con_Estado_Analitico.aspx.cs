﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Text;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;

public partial class paginas_Contabilidad_Frm_Rpt_Con_Estado_Analitico : System.Web.UI.Page
{
    #region (Load/Init)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Valida que exista algun usuario logueado al sistema.
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Limpia_Controles();//Limpia los controles de la forma

                //Colocar el valor del campo oculto de la CONAC
                Txt_Campo_Oculto.Text = Consulta_Campo_Oculto_CONAC();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
    #region (Metodos)
    #region (Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 21-Febrero-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Cmb_Tipo_Reporte.SelectedIndex = 0;
            Cmb_Anio.SelectedIndex = 0;
            Cmb_Mes.SelectedIndex = 0;
            Chk_Movimientos_Saldo_No_Cero.Checked = false;
            Llenar_Combo_Anio();
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                           para mostrar los datos al usuario
    ///CREO       : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO  : 21-Febrero-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    private void Abrir_Ventana(String Nombre_Archivo)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
        try
        {
            Pagina = Pagina + Nombre_Archivo;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
        }
    }
    #endregion
    #region(Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Reporte
    /// DESCRIPCION : Validar que se se encuentre todos los datos para continuar con el reporte
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 18/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Reporte()
    {
        String Espacios_Blanco;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Lbl_Mensaje_Error.Text = "";
        Lbl_Mensaje_Error.Text += Espacios_Blanco + "Es necesario Introducir: <br>";
        
        if (Cmb_Tipo_Reporte.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Selecione algun tipo de reporte.<br>";
            Datos_Validos = false;
        }
        if (Cmb_Anio.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Selecione algun año.<br>";
            Datos_Validos = false;
        }
        if (Cmb_Mes.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Selecione algun mes.<br>";
            Datos_Validos = false;
        }
        
        return Datos_Validos;
    }
    #endregion
    #region (Consulta)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Estado_Analitico
    /// DESCRIPCION : Consulta todos los movimientos con sus detalles de las pólizas
    ///             realizadas en el rango de fechas proporcionadas por el usuario
    /// PARAMETROS  : Tipo_Reporte.- para saber si el reporte es excel o pdf
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 21/Febrero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Estado_Analitico(String Tipo_Reporte)
    {
        Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Estado_Cuentas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
        DataTable Dt_Tipo_Reporte = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Perido_Anterior = new DataTable();
        Ds_Rpt_Con_Estado_Analitico Ds_Reporte = new Ds_Rpt_Con_Estado_Analitico();
        Ds_Rpt_Con_Flujo_Efectivo Ds_Flujo = new Ds_Rpt_Con_Flujo_Efectivo();
        ReportDocument Reporte = new ReportDocument();
        String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
        String Nombre_Archivo = "Estado_Cuentas" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta_Situacion_Financiera = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataSet Ds_Reporte_Excel = new DataSet();
        String Nombre = "";
        Decimal mes = 0;
        try
        {
            //para saber el nombre que llevara el reporte inicio y se realiza la consulta
            Dt_Tipo_Reporte.Columns.Add("ANIO");
            Dt_Tipo_Reporte.Columns.Add("MES");
            Dt_Tipo_Reporte.TableName = "Dt_Tipo_Reporte";
            DataRow Dt_Row = Dt_Tipo_Reporte.NewRow();
            Dt_Row["ANIO"] = Cmb_Anio.SelectedValue;
            Dt_Row["MES"] = Cmb_Mes.SelectedItem.Text;
            if (Cmb_Tipo_Reporte.SelectedValue == "ESTADO ANALÍTICO DEL ACTIVO")
            {
                Dt_Tipo_Reporte.Rows.Add(Dt_Row);
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
                Rs_Consulta_Situacion_Financiera.P_Mes_Año_Ant = "'01" + Cmb_Mes.SelectedValue.Substring(2, 2)+"'";
                mes = Convert.ToDecimal(Cmb_Mes.SelectedValue.Substring(0, 2));
                for (Decimal i = 2; i <= mes;i++)
                {
                    Rs_Consulta_Situacion_Financiera.P_Mes_Año_Ant = Rs_Consulta_Situacion_Financiera.P_Mes_Año_Ant + " OR Cierre.MES_ANIO='" + String.Format("{0:00}", Convert.ToInt32(i))+Cmb_Mes.SelectedValue.Substring(2, 2) + "'";
                }
                Rs_Consulta_Situacion_Financiera.P_filtro = "1";
                Dt_Consulta = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera_Activo_Acumulado();
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);//  Dos Años antes del actual//+Cmb_Mes.SelectedValue.Substring(2,2);
                Rs_Consulta_Situacion_Financiera.P_filtro = "1";
                Dt_Perido_Anterior = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera_Activo();
                Dt_Consulta = Generar_Tabla_Final(Dt_Consulta, Dt_Perido_Anterior);
                Dt_Consulta.TableName = "Dt_Estado_Analitico";
                if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                {
                    Dt_Consulta.DefaultView.RowFilter = "(CARGO <> '0.00' OR ABONO <> '0.00') OR (SALDO_INICIAL<>'0.00' OR SALDO_FINAL<>'0.00')";
                    Dt_Consulta.DefaultView.Sort = "INDICE";
                }
                else
                {
                    Dt_Consulta.DefaultView.Sort = "INDICE";
                }
                Ds_Reporte_Excel.Clear();
                Ds_Reporte_Excel.Tables.Clear();
                Ds_Reporte_Excel.Tables.Add(Dt_Consulta.DefaultView.ToTable().Copy());

                Ds_Reporte.Clear();
                Ds_Reporte.Tables.Clear();
                Ds_Reporte.Tables.Add(Dt_Consulta.DefaultView.ToTable().Copy());
                Ds_Reporte.Tables.Add(Dt_Tipo_Reporte.Copy());
                Reporte.Load(Ruta_Archivo + "Rpt_Con_Estado_Analitico.rpt");
                Reporte.SetDataSource(Ds_Reporte);
                Nombre = "05_EAA_15_AP";
            }
            else
            {
                if (Cmb_Tipo_Reporte.SelectedValue == "ESTADO DE FLUJO DE EFECTIVO")
                {
                    //para la tabla de tipo de reporte
                    Dt_Tipo_Reporte.Columns.Add("AÑO");
                    Dt_Row["AÑO"] = Cmb_Anio.SelectedValue;
                    Dt_Tipo_Reporte.Rows.Add(Dt_Row);
                    Dt_Tipo_Reporte.TableName = "Dt_Periodo";
                    Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
                    Rs_Consulta_Situacion_Financiera.P_Mes_Año_Ant = Cmb_Mes.SelectedValue; //"'01" + Cmb_Mes.SelectedValue.Substring(2, 2) + "'";
                    Rs_Consulta_Situacion_Financiera.P_Filtro1 = "41";
                    Rs_Consulta_Situacion_Financiera.P_Filtro2 = "42";
                    Rs_Consulta_Situacion_Financiera.P_Filtro3 = "43";
                    Rs_Consulta_Situacion_Financiera.P_Filtro4 = "51";
                    Rs_Consulta_Situacion_Financiera.P_Filtro5 = "52";
                    Rs_Consulta_Situacion_Financiera.P_Filtro6 = "53";
                    Rs_Consulta_Situacion_Financiera.P_Filtro7 = "54";
                    mes = Convert.ToDecimal(Cmb_Mes.SelectedValue.Substring(0, 2));
                    Dt_Consulta = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera_Acumulado();
                    Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);// //+ Cmb_Mes.SelectedValue.Substring(2, 2).ToString();
                    Dt_Perido_Anterior = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                    Dt_Consulta = Generar_Tabla_Situacion_Financiera(Dt_Consulta, Dt_Perido_Anterior);
                    Rs_Consulta_Situacion_Financiera.P_Filtro1 = null;
                    Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
                    Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                    Rs_Consulta_Situacion_Financiera.P_Filtro4 = null;
                    Rs_Consulta_Situacion_Financiera.P_Filtro5 = null;
                    Rs_Consulta_Situacion_Financiera.P_Filtro6 = null;
                    Rs_Consulta_Situacion_Financiera.P_Filtro7 = null;
                    if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                    {
                        Dt_Consulta.DefaultView.RowFilter = "IMPORTE_ANT <> '0.00' AND IMPORTE <> '0.00'";
                    }
                    //para la nota de efe-01
                    DataTable Dt_Efe_01 = new DataTable();
                    DataTable Dt_Efe_02 = new DataTable();
                    Dt_Consulta.TableName = "Dt_Flujo_De_Efectivo";
                    Dt_Efe_01.TableName = "Dt_Efe_01";
                    Dt_Efe_02.TableName = "Dt_Efe_02";
                    Ds_Reporte_Excel.Clear();
                    Ds_Reporte_Excel.Tables.Clear();
                    if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                    {
                        Ds_Reporte_Excel.Tables.Add(Dt_Consulta.DefaultView.ToTable().Copy());
                    }
                    else
                    {
                        Ds_Reporte_Excel.Tables.Add(Dt_Consulta.Copy());
                    }
                    Ds_Reporte.Clear();
                    Ds_Reporte.Tables.Clear();
                    if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                    {
                        Ds_Reporte.Tables.Add(Dt_Consulta.DefaultView.ToTable().Copy());
                    }
                    else
                    {
                        Ds_Reporte.Tables.Add(Dt_Consulta.Copy());
                    }
                    Ds_Reporte.Tables.Add(Dt_Tipo_Reporte.Copy());
                    Ds_Reporte.Tables.Add(Dt_Efe_01.Copy());
                    Ds_Reporte.Tables.Add(Dt_Efe_02.Copy());
                    Reporte.Load(Ruta_Archivo + "Rpt_Con_Estado_Flujo_Efectivo.rpt");
                    Reporte.SetDataSource(Ds_Reporte);
                    Nombre = "04_EFE_15_AP";
                }
                else
                {
                    if (Cmb_Tipo_Reporte.SelectedValue == "ESTADO ANALÍTICO DE LA DEUDA Y OTROS PASIVOS")
                    {
                        Dt_Tipo_Reporte.Rows.Add(Dt_Row);
                        Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
                        Rs_Consulta_Situacion_Financiera.P_Mes_Año_Ant = "'01" + Cmb_Mes.SelectedValue.Substring(2, 2) + "'";
                        mes = Convert.ToDecimal(Cmb_Mes.SelectedValue.Substring(0, 2));
                        for (Decimal i = 2; i <= mes; i++)
                        {
                            Rs_Consulta_Situacion_Financiera.P_Mes_Año_Ant = Rs_Consulta_Situacion_Financiera.P_Mes_Año_Ant + " OR Cierre.MES_ANIO='" + String.Format("{0:00}", Convert.ToInt32(i)) + Cmb_Mes.SelectedValue.Substring(2, 2) + "'";
                        }
                        Dt_Consulta = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera_Activo_Acumulado();
                        Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);// //+ Cmb_Mes.SelectedValue.Substring(2, 2).ToString();
                        Dt_Perido_Anterior = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera_Activo();
                        Dt_Consulta = Generar_Tabla_Final_DyP(Dt_Consulta, Dt_Perido_Anterior);
                        Dt_Consulta.TableName = "Dt_Est_Analitico_DyP";
                        if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                        {
                            Dt_Consulta.DefaultView.RowFilter = "(CARGO <> '0.00' OR ABONO <> '0.00')OR (SALDO_INICIAL <> '0.00'OR SALDO_FINAL <> '0.00')";
                            Dt_Consulta.DefaultView.Sort = "INDICE ASC";
                        }
                        else
                        {
                            Dt_Consulta.DefaultView.Sort = "INDICE ASC";
                        }                        
                        Ds_Reporte_Excel.Clear();
                        Ds_Reporte_Excel.Tables.Clear();
                        Ds_Reporte_Excel.Tables.Add(Dt_Consulta.DefaultView.ToTable().Copy());

                        Ds_Reporte.Clear();
                        Ds_Reporte.Tables.Clear();
                        Ds_Reporte.Tables.Add(Dt_Consulta.DefaultView.ToTable().Copy());
                        Ds_Reporte.Tables.Add(Dt_Tipo_Reporte.Copy());
                        Reporte.Load(Ruta_Archivo + "Rpt_Con_Analitico_Deuda_Pasivos.rpt");
                        Reporte.SetDataSource(Ds_Reporte);
                        Nombre = "06_EADOP_15_AP";
                    }
                    else
                    {
                            if (Cmb_Tipo_Reporte.SelectedValue == "BASE DE DATOS CON MOVIMIENTOS CONTABLES")
                            {
                                Dt_Tipo_Reporte.Rows.Add(Dt_Row);
                                Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
                                Dt_Consulta = Rs_Consulta_Situacion_Financiera.Consulta_Movimientos_Contables();
                                Dt_Consulta = Generar_Tabla_Final_Movimientos_Contables(Dt_Consulta);
                                Dt_Consulta.TableName = "Dt_Movimientos_Contables";
                                Dt_Consulta.Columns.RemoveAt(8);
                                Dt_Consulta.AcceptChanges();

                                Ds_Reporte_Excel.Clear();
                                Ds_Reporte_Excel.Tables.Clear();
                                Ds_Reporte_Excel.Tables.Add(Dt_Consulta.Copy());

                                Ds_Reporte.Clear();
                                Ds_Reporte.Tables.Clear();
                                Ds_Reporte.Tables.Add(Dt_Consulta.Copy());
                                Ds_Reporte.Tables.Add(Dt_Tipo_Reporte.Copy());
                                Reporte.Load(Ruta_Archivo + "Rpt_Con_Movimientos_Contables.rpt");
                                Reporte.SetDataSource(Ds_Reporte);
                                Nombre = "05_BDMC_15_AP";
                            }                        
                    }
                }
            }
            if (Tipo_Reporte == "PDF")
            {
                DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                Nombre_Archivo += ".pdf";
                Ruta_Archivo = @Server.MapPath("../../Reporte/");
                m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                ExportOptions Opciones_Exportacion = new ExportOptions();
                Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);

                Abrir_Ventana(Nombre_Archivo);
            }

            else 
            {
                if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                {
                   
                }
                Generar_Rpt_Excel(Ds_Reporte_Excel, Nombre);
            }
        }
        catch (System.Threading.ThreadAbortException Ex)
        {}
        catch (Exception ex)
        {
            throw new Exception("Consulta_Diario_General " + ex.Message.ToString(), ex);
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consulta_Campo_Oculto_CONAC
    ///DESCRIPCIÓN: Consultar el parametro oculto de los reportes de la CONAC
    ///PARAMETROS: 
    ///CREO:        Noe Mosqueda Valadez
    ///FECHA_CREO:  23/Mayo/2013 10:55
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private string Consulta_Campo_Oculto_CONAC()
    {
        //Declaracion de variables
        Cls_Cat_Con_Parametros_Negocio Parametros_Negocio = new Cls_Cat_Con_Parametros_Negocio(); //variable para la capa de negocios
        DataTable Dt_Parametros = new DataTable(); //tabla para la consulta de los parametros
        string Resultado = string.Empty; //variable para el resultado

        try
        {
            //Consultar los parametros de contabilidad
            Dt_Parametros = Parametros_Negocio.Consulta_Datos_Parametros_2();

            //verificar si la consulta arrojo resultado
            if (Dt_Parametros.Rows.Count > 0)
            {
                //verificar si hay dato
                if (Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Campo_Escondido_CONAC] != DBNull.Value)
                {
                    if (string.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Campo_Escondido_CONAC].ToString().Trim()) == false)
                    {
                        Resultado = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Campo_Escondido_CONAC].ToString().Trim();
                    }
                }
            }

            //entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Anio_OnSelectedIndexChanged
    ///DESCRIPCIÓN: cargara el combo de meses con los que se encuentren cerrados
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  22/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Anio_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta_Meses = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Consulta = new DataTable();
        String Año = "";
        String Mes;
        DataTable Dt_Temp_Consulta = new DataTable();
        try
        {
            Año = Cmb_Anio.SelectedValue;
            Año = Año.Substring(2, 2);
            Rs_Consulta_Meses.P_Año = Año;
            Dt_Consulta = Rs_Consulta_Meses.Consulta_Meses_Cerrados();
            // se definen los campos del Dt_Temp_Consulta
            Dt_Temp_Consulta.Columns.Add("MES", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("MES_ANIO", typeof(System.String));
            DataRow row;
            foreach (DataRow Renglon in Dt_Consulta.Rows)
            {
                row = Dt_Temp_Consulta.NewRow();
                Mes = Renglon["Mes_Anio"].ToString().Substring(0, 2);
                switch (Mes)
                {
                    case "01":
                        Mes = "ENERO";
                        break;
                    case "02":
                        Mes = "FEBRERO";
                        break;
                    case "03":
                        Mes = "MARZO";
                        break;
                    case "04":
                        Mes = "ABRIL";
                        break;
                    case "05":
                        Mes = "MAYO";
                        break;
                    case "06":
                        Mes = "JUNIO";
                        break;
                    case "07":
                        Mes = "JULIO";
                        break;
                    case "08":
                        Mes = "AGOSTO";
                        break;
                    case "09":
                        Mes = "SEPTIEMBRE";
                        break;
                    case "10":
                        Mes = "OCTUBRE";
                        break;
                    case "11":
                        Mes = "NOVIEMBRE";
                        break;
                    default:
                        Mes = "DICIEMBRE";
                        break;
                }
                row["MES"] = Mes;
                row["MES_ANIO"] = Renglon["Mes_Anio"].ToString();
                Dt_Temp_Consulta.Rows.Add(row);
                Dt_Temp_Consulta.AcceptChanges();
            }

            Cmb_Mes.Items.Clear();
            Cmb_Mes.DataSource = Dt_Temp_Consulta;
            Cmb_Mes.DataValueField = "MES_ANIO";
            Cmb_Mes.DataTextField = "MES";
            Cmb_Mes.DataBind();
            Cmb_Mes.Items.Insert(0, "----- < SELECCIONE > -----");
            Cmb_Mes.SelectedIndex = 0;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Anio_OnSelectedIndexChanged
    ///DESCRIPCIÓN: cargara el combo de Anios con los que se encuentren cerrados
    ///PARAMETROS: 
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  22/Abril/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Llenar_Combo_Anio()
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta_Anios = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Dt_Consulta = Rs_Consulta_Anios.Consulta_Anios();
            if (Dt_Consulta.Rows.Count > 0)
            {
                Cmb_Anio.Items.Clear();
                Cmb_Anio.DataSource = Dt_Consulta;
                Cmb_Anio.DataValueField = "ANIO";
                Cmb_Anio.DataTextField = "ANIO";
                Cmb_Anio.DataBind();
                Cmb_Anio.Items.Insert(0, "----- < SELECCIONE > -----");
                Cmb_Anio.SelectedIndex = 0;
            }

            
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Situacion_Financiera
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  03/Marzo/2012
    ///MODIFICO:Sergio Manuel Gallardo Andrade  
    ///FECHA_MODIFICO: 11/marzo/2013
    ///CAUSA_MODIFICACIÓN: se necesita conocer el periodo anterior por lo que se agrego un dt
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Situacion_Financiera(DataTable Dt_Consulta, DataTable Dt_Perido_Anterior)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        #region Variables
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Temp_Consulta = new DataTable();
        String Indice = "";
        String Indice_2 = "";
        String Indice_Segundo_Nivel = "";
        String Auxiliar = "";
        String Auxiliar_Anterior = "";
        String Auxiliar_Cuenta = "5";
        String Tipo_Tercera_Posicion = "";
        DataRow Row_Nueva;
        //String Cuenta;
        int Contador_For = 0;
        Double Suma_Saldo = 0.0;
        Double Suma_Saldo_Anterior = 0.0;
        Double Suma_Cuenta_4 = 0.0;
        Double Suma_Cuenta_4_Anterior = 0.0;
        Double Suma_Cuenta_5 = 0.0;
        Double Suma_Cuenta_5_Anterior = 0.0;
        Double Suma_Origen = 0.0;
        Double Suma_Origen_Anterior = 0.0;
        Double Suma_Origen_31 = 0.0;
        Double Suma_Origen_31_Anterior = 0.0;
        Double Suma_Origen_12 = 0.0;
        Double Suma_Origen_12_Anterior = 0.0;
        Double Suma_Origen_45 = 0.0;
        Double Suma_Origen_45_Anterior = 0.0;
        Double Suma_Origen_2233 = 0.0;
        Double Suma_Origen_2233_Anterior = 0.0;
        Double Suma_Aplicacion_123_Anterior = 0.0;
        Double Suma_Origen_2234 = 0.0;
        Double Suma_Aplicacion_1235_Anterior = 0.0;
        Double Suma_Origen_32 = 0.0;
        Double Suma_Aplicacion = 0.0;
        Double Suma_Aplicacion_Anterior = 0.0;
        Double Suma_Aplicacion_121 = 0.0;
        Double Suma_Aplicacion_121_Anterior = 0.0;
        Double Suma_Aplicacion_123 = 0.0;
        Double Suma_Aplicacion_1235 = 0.0;
        Double Suma_Origen_2234_Anterior = 0.0;
        Double Suma_Aplicacion_124 = 0.0;
        Double Suma_Aplicacion_124_Anterior = 0.0;
        Double Suma_Aplicacion_46 = 0.0;
        Double Suma_Aplicacion_46_Anterior = 0.0;
        Double Suma_Aplicacion_2131 = 0.0;
        Double Suma_Aplicacion_2131_Anterior = 0.0;
        Double Suma_Origen_32_Anterior = 0.0;
        Double Suma_Aplicacion_2132 = 0.0;
        Double Suma_Aplicacion_2132_Anterior = 0.0;
        Double Suma_Aplicacion_23 = 0.0;
        Double Suma_Aplicacion_23_Anterior = 0.0;
        Double Suma_Neto_Operaciones = 0.0;
        Double Suma_Neto_Financiamiento_Anterior = 0.0;
        Double Suma_Neto_Inversion = 0.0;
        Double Suma_Neto_Financiamiento = 0.0;
        Double Suma_Cuenta_111_Anterior = 0.0;
        Double Suma_Neto_Operaciones_Anterior = 0.0;
        Double Suma_Cuenta_111 = 0.0;
        Double Suma_Neto_Inversion_Anterior = 0.0;
        DataRow Row_Final;
        String Ultimo = "";
        #endregion
        try
        {
            Dt_Nueva.Columns.Add("INDICE", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("IMPORTE", typeof(System.String));
            Dt_Nueva.Columns.Add("IMPORTE_ANT", typeof(System.String));
            Dt_Nueva.Columns.Add("NOTA", typeof(System.String));
            Dt_Nueva.TableName = "Dt_Flujo_De_Efectivo";
            Dt_Final.Columns.Add("INDICE", typeof(System.String));
            Dt_Final.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Final.Columns.Add("IMPORTE", typeof(System.String));
            Dt_Final.Columns.Add("IMPORTE_ANT", typeof(System.String));
            Dt_Final.Columns.Add("NOTA", typeof(System.String));
            Dt_Final.TableName = "Dt_Flujo_De_Efectivo";
            Rs_Consulta.P_Filtro1 = "41";
            Rs_Consulta.P_Filtro2 = "42";
            Rs_Consulta.P_Filtro3 = "430";
            Rs_Consulta.P_Filtro4 = "51";
            Rs_Consulta.P_Filtro5 = "52";
            Rs_Consulta.P_Filtro6 = "53";
            Rs_Consulta.P_Filtro7 = "54";
            Dt_Cuentas = Rs_Consulta.Consulta_Cuentas_Tercer_Nivel();
            Rs_Consulta.P_Filtro2 = null;
            Rs_Consulta.P_Filtro3 = null;
            Rs_Consulta.P_Filtro4 = null;
            Rs_Consulta.P_Filtro5 = null;
            Rs_Consulta.P_Filtro6 = null;
            Rs_Consulta.P_Filtro7 = null;
            for (Contador_For = Dt_Cuentas.Rows.Count - 1; Contador_For >= 0; Contador_For--)
            {
                Indice = Dt_Cuentas.Rows[Contador_For]["CUENTA"].ToString();
                Tipo_Tercera_Posicion = Indice;
                Indice_Segundo_Nivel = Indice;
                Indice_Segundo_Nivel = Indice_Segundo_Nivel.Substring(2, 1);
                Tipo_Tercera_Posicion = Tipo_Tercera_Posicion.Substring(0, 3);
                Indice = Indice.Substring(0, 4);
                Indice_2 = Indice.Substring(3, 1);
                ////para los que tengan en la tercera posicion diferente a CERO 0
                if (Convert.ToDouble(Indice_Segundo_Nivel) != 0)
                {
                    //se buscan los saldos
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        Auxiliar = (Registro["CUENTA"].ToString());
                        Auxiliar = Auxiliar.Substring(0, 3);

                        if (Tipo_Tercera_Posicion == Auxiliar)
                            Suma_Saldo += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                    }
                    //se buscan los saldos para el periodo anterior
                    foreach (DataRow Registro in Dt_Perido_Anterior.Rows)
                    {
                        Auxiliar_Anterior = (Registro["CUENTA"].ToString());
                        Auxiliar_Anterior = Auxiliar_Anterior.Substring(0, 3);

                        if (Tipo_Tercera_Posicion == Auxiliar_Anterior)
                            Suma_Saldo_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                    }
                    Ultimo = Indice.Substring(3, 1);
                    Indice = Indice.Substring(0, 1);
                    Auxiliar_Cuenta = Auxiliar_Cuenta.Substring(0, 1);
                    //para las sumas de las cuentas 4000 y 5000
                    if (Convert.ToDouble(Indice) == 4 && Ultimo == "0")
                    {
                        Suma_Cuenta_4 += Suma_Saldo; 
                        Suma_Cuenta_4_Anterior += Suma_Saldo_Anterior;
                    }
                    else
                    {
                        if (Convert.ToDouble(Indice) == 5 && Ultimo == "0")
                        {
                            Suma_Cuenta_5 += Suma_Saldo;
                            Suma_Cuenta_5_Anterior += Suma_Saldo_Anterior;
                        }
                    }
                    //se pasa la cuenta para poder compararla en el siguiente ciclo
                    Auxiliar_Cuenta = Indice;
                    //se ingresa los valores a la tabla
                    if (Indice_2 == "0")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Row_Nueva["INDICE"] = Dt_Cuentas.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        Row_Nueva["NOMBRE"] = Dt_Cuentas.Rows[Contador_For]["DESCRIPCION"].ToString();
                        Row_Nueva["IMPORTE"] = String.Format("{0:c}",Suma_Saldo).Replace("$","");
                        Row_Nueva["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Saldo_Anterior).Replace("$", "");
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Suma_Saldo = 0;
                    Suma_Saldo_Anterior = 0;
                }// fin del if 0
                else if (Convert.ToDouble(Indice) == 4300)
                {
                    if (Convert.ToDouble(Auxiliar_Cuenta) == 5)
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Row_Nueva["NOMBRE"] = "APLICACIÓN";
                        Row_Nueva["IMPORTE"] = String.Format("{0:c}", Suma_Cuenta_5).Replace("$", "");
                        Row_Nueva["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Cuenta_5_Anterior).Replace("$", "");
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    //se buscan los saldos de la cuenta 4300
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        Auxiliar = (Registro["CUENTA"].ToString());
                        Auxiliar = Auxiliar.Substring(0, 2);
                        String Auxilar_2 = Indice;
                        Auxilar_2 = Auxilar_2.Substring(0, 2);

                        if (Auxilar_2 == Auxiliar)
                            Suma_Saldo += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                    }
                    //para obtener el saldo anterior
                    foreach (DataRow Registro in Dt_Perido_Anterior.Rows)
                    {
                        Auxiliar = (Registro["CUENTA"].ToString());
                        Auxiliar = Auxiliar.Substring(0, 2);
                        String Auxilar_2 = Indice;
                        Auxilar_2 = Auxilar_2.Substring(0, 2);

                        if (Auxilar_2 == Auxiliar)
                            Suma_Saldo_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                    }
                    //para las sumas de las cuentas 4000 
                    if (Convert.ToDouble(Indice) == 4300)
                    {
                        Suma_Cuenta_4 += Suma_Saldo;
                        Suma_Cuenta_4_Anterior += Suma_Saldo_Anterior;
                    }
                    //se pasa la cuenta para poder compararla en el siguiente ciclo
                    Auxiliar_Cuenta = Indice;
                    //se ingresa los valores a la tabla
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["INDICE"] = Dt_Cuentas.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Cuentas.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["IMPORTE"] = String.Format("{0:c}", Suma_Saldo).Replace("$", "");
                    Row_Nueva["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Saldo_Anterior).Replace("$", "");
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Suma_Saldo = 0;
                    Suma_Saldo_Anterior = 0;
                }// fin del else if 4300
            }// fin del for
            //se llena el encabezado principal de la cuenta 4000
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "ORIGEN";
            Row_Nueva["IMPORTE"] = String.Format("{0:c}", Suma_Cuenta_4).Replace("$", "");
            Row_Nueva["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Cuenta_4_Anterior).Replace("$", "");
            Dt_Nueva.Rows.Add(Row_Nueva);
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "ACTIVIDADES DE OPERACIÓN";
            Dt_Nueva.Rows.Add(Row_Nueva);
            //se acomoda la tabla 
            for (Contador_For = Dt_Nueva.Rows.Count - 1; Contador_For >= 0; Contador_For--)
            {
                Row_Final = Dt_Final.NewRow();
                Indice = Dt_Nueva.Rows[Contador_For]["INDICE"].ToString();
                Row_Final["INDICE"] = Indice;
                Row_Final["NOMBRE"] = Dt_Nueva.Rows[Contador_For]["NOMBRE"].ToString();
                if (!String.IsNullOrEmpty(Dt_Nueva.Rows[Contador_For]["IMPORTE"].ToString()))
                {
                    Row_Final["IMPORTE"] = String.Format("{0:c}", Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["IMPORTE"].ToString().Replace("$", "").Replace(",", ""))).Replace("$", "");
                }
                if (!String.IsNullOrEmpty(Dt_Nueva.Rows[Contador_For]["IMPORTE_ANT"].ToString()))
                {
                    Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["IMPORTE_ANT"].ToString().Replace("$", "").Replace(",", ""))).Replace("$", "");
                }
                Dt_Final.Rows.Add(Row_Final);
            }
            Row_Final = Dt_Final.NewRow();
            Suma_Neto_Operaciones = Suma_Cuenta_4 - Suma_Cuenta_5;
            Suma_Neto_Operaciones_Anterior = Suma_Cuenta_4_Anterior - Suma_Cuenta_5_Anterior;
            Row_Final["NOMBRE"] = "FLUJO NETO DE EFECTIVO DE LAS ACTIVIDADES DE OPERACIÓN";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Cuenta_4 - Suma_Cuenta_5).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Cuenta_4_Anterior - Suma_Cuenta_5_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);
            #region Inversion
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "ACTIVIDADES DE INVERSIÓN";
            Dt_Final.Rows.Add(Row_Final);

            //para el origen
            #region Cuenta origen
            //para la cuenta 31
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "31";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_31 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo Anterior
            Rs_Consulta.P_Mes_Año = "12"+(Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_31_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //para la cuenta 2133
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "1233";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_12 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo Anterior
            Rs_Consulta.P_Mes_Año = "12" + (Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_12_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //para la cuenta 2133
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "45";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_45 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo Anteriror
            Rs_Consulta.P_Mes_Año = "12" + (Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_45_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            Suma_Origen = Suma_Origen_12 + Suma_Origen_31 + Suma_Origen_45;
            Suma_Origen_Anterior = Suma_Origen_12_Anterior + Suma_Origen_31_Anterior + Suma_Origen_45_Anterior;
            //cuenta origen
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "ORIGEN";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Origen_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 31
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "3100";//"3.1.0.0-A";
            Row_Final["NOMBRE"] = "Contribuciones de capital";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen_31).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Origen_31_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 1.2.3.3-A
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1233";//"1.2.3.3-A";
            Row_Final["NOMBRE"] = "Venta de activos fisicos";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen_12).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Origen_12_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 4.5.0.0
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "4500";//"4.5.0.0";
            Row_Final["NOMBRE"] = "Otros";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen_45).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Origen_45_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);
            #endregion
            //para la aplicacion
            #region Aplicacion

            //para la cuenta 1.2.1
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "121";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_121 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo Anterior
            Rs_Consulta.P_Mes_Año = "12"+(Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_121_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //para la cuenta 1.2.3
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "123";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Indice = Registro["CUENTA"].ToString();
                Indice = Indice.Substring(0, 4);
                if (Convert.ToDouble(Indice) != 1235)
                {
                    Suma_Aplicacion_123 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                }
                
            }
            //Periodo Anterior
            Rs_Consulta.P_Mes_Año = "12"+(Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Indice = Registro["CUENTA"].ToString();
                Indice = Indice.Substring(0, 4);
                if (Convert.ToDouble(Indice) != 1235)
                {
                    Suma_Aplicacion_123_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                }

            }
            //para la cuenta 1.2.3.5
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "1235";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_1235 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo Anterior
            Rs_Consulta.P_Mes_Año = "12"+(Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_1235_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //para la cuenta 1.2.4
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "124";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_124 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo Anterior
            Rs_Consulta.P_Mes_Año = "12"+(Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_124_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //para la cuenta 4.6
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "46";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_46 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Peridodo Anterior
            Rs_Consulta.P_Mes_Año = "12"+(Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Rs_Consulta.P_Filtro1 = "46";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_46_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //cuenta Aplicacion
            Suma_Aplicacion = Suma_Aplicacion_123 + Suma_Aplicacion_1235 + Suma_Aplicacion_124 + Suma_Aplicacion_46;
            Suma_Aplicacion_Anterior = Suma_Aplicacion_123_Anterior + Suma_Aplicacion_1235_Anterior + Suma_Aplicacion_124_Anterior + Suma_Aplicacion_46_Anterior;
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "APLICACIÓN";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 1.2.1
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1210";//"1.2.1.0-C";
            Row_Final["NOMBRE"] = "Inversiones financieras (aportaciones a fideicomisos)";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion_121).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_121_Anterior).Replace("$", "");
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 1.2.3.0-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1230";//"1.2.3.0-C";
            Row_Final["NOMBRE"] = "Bienes inmuebles";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion_123).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_123_Anterior).Replace("$", "");
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 1.2.3.5-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1235";//"1.2.3.5-C";
            Row_Final["NOMBRE"] = "Construcciones en proceso";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion_1235).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_1235_Anterior).Replace("$", "");
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 1.2.4-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1240";//"1.2.4.0-C";
            Row_Final["NOMBRE"] = "Bienes muebles";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion_124).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_124_Anterior).Replace("$", "");
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

             //cuenta 1.2.4-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1250";//"1.2.4.0-C";
            Row_Final["NOMBRE"] = "Intangibles";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion_124).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_124_Anterior).Replace("$", "");
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 4.6.0.0
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "4600";//"4.6.0.0";
            Row_Final["NOMBRE"] = "Otros";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion_46).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_46_Anterior).Replace("$", "");
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

            //TOTAL
            Suma_Neto_Inversion = Suma_Origen - Suma_Aplicacion;
            Suma_Neto_Inversion_Anterior = Suma_Origen_Anterior - Suma_Aplicacion_Anterior;
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "FLUJO NETO DE EFECTIVO DE LAS ACTIVIDADES DE INVERSIÓN";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen - Suma_Aplicacion).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Origen_45_Anterior - Suma_Aplicacion_46_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);
            #endregion

            #endregion
            #region Financiamiento
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "ACTIVIDADES DE FINANCIAMIENTO";
            Dt_Final.Rows.Add(Row_Final);

            #region Origen

            //para la cuenta 2.2.3.3
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "2233";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_2233 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            Rs_Consulta.P_Mes_Año = "12"+(Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_2233_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //para la cuenta 2.2.3.4
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "2234";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_2234 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo anterior
            Rs_Consulta.P_Mes_Año = "12"+(Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_2234_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //para la cuenta 3.2
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "32";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_32 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo Anterior
            Rs_Consulta.P_Mes_Año = "12" + (Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2, 2).ToString()) - 1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_32_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            Suma_Origen = Suma_Origen_2234 + Suma_Origen_2233 + Suma_Origen_32;
            Suma_Origen_Anterior = Suma_Origen_2234_Anterior + Suma_Origen_2233_Anterior + Suma_Origen_32_Anterior;
            //cuenta origen
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "ORIGEN";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Origen_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 2.2.3.3-A
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "2233";// "2.2.3.3-A";
            Row_Final["NOMBRE"] = "Endeudamiento neto interno";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen_2233).Replace("$", "");
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen_2233_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 2.2.3.4-A
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "2234";//"2.2.3.4-A";
            Row_Final["NOMBRE"] = "Endeudamiento neto externo";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen_2234).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Origen_2234_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 3.2.0.0
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "4700";//"3.2.0.0";
            Row_Final["NOMBRE"] = "Incremento de Patrimonio/Pasivos y Disminución de Activos";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen_32).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Origen_32_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            #endregion

            #region Aplicacion
            //para la cuenta 2.1.3.1-C
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "2131";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_2131 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo anterior
            Rs_Consulta.P_Mes_Año = "12" + (Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_2131_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //para la cuenta 2.1.3.2-C
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "2132";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_2132 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo Anterior
            Rs_Consulta.P_Mes_Año = "12"+(Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_2132_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //para la cuenta 2.3.0.0 -C
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "23";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_23 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo Anterior 
            Rs_Consulta.P_Mes_Año = "12" + (Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_23_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            Suma_Aplicacion = Suma_Aplicacion_2131 + Suma_Aplicacion_2132 + Suma_Aplicacion_23;
            Suma_Aplicacion_Anterior= Suma_Aplicacion_2131_Anterior+ Suma_Aplicacion_2132_Anterior+Suma_Aplicacion_23_Anterior;

            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "APLICACIÓN";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 2.1.3.1-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "2131";//"2.1.3.1-C";
            Row_Final["NOMBRE"] = "Servicios de la deuda interna";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion_2131).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_2131_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 2.1.3.2-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "2132";//"2.1.3.2-C";
            Row_Final["NOMBRE"] = "Servicios de la deuda externa";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion_2132).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_2132_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //cuenta 2.3.0.0-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "4800";//"2.3.0.0-C";
            Row_Final["NOMBRE"] = "Disminucion de otros pasivos";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Aplicacion_23).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Aplicacion_23_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //TOTAL
            Suma_Neto_Financiamiento = Suma_Origen - Suma_Aplicacion;
            Suma_Neto_Financiamiento_Anterior = Suma_Origen_Anterior + Suma_Aplicacion_Anterior;
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "FLUJO NETO DE EFECTIVO DE LAS ACTIVIDADES DE FINANCIAMIENTO";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Origen - Suma_Aplicacion).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Origen_Anterior - Suma_Aplicacion_Anterior).Replace("$", ""); 
            Dt_Final.Rows.Add(Row_Final);
            #endregion
            #endregion
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "INCREMENTO/DISMINUCIÓN NETA EN EL EFECTIVO Y EQUIVALENTES AL EFECTIVO";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Neto_Financiamiento + Suma_Neto_Inversion + Suma_Neto_Operaciones).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Neto_Financiamiento_Anterior + Suma_Neto_Inversion_Anterior + Suma_Neto_Operaciones_Anterior).Replace("$", "");
            Dt_Final.Rows.Add(Row_Final);

            //para la cuenta 1.1.1
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "111";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Cuenta_111 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            //Periodo Anterior
            Rs_Consulta.P_Mes_Año = "12"+(Convert.ToDouble(Cmb_Mes.SelectedValue.Substring(2,2).ToString())-1).ToString();
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Cuenta_111_Anterior += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = " EFECTIVO Y EQUIVALENTES AL EFECTIVO AL INICIO DEL PERIODO";
            Row_Final["IMPORTE"] = String.Format("{0:c}", Suma_Cuenta_111).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", Suma_Cuenta_111_Anterior).Replace("$", "");
            Row_Final["NOTA"] = "EFE-01";
            Dt_Final.Rows.Add(Row_Final);
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = " EFECTIVO Y EQUIVALENTES AL EFECTIVO AL FINAL DEL PERIODO";
            Row_Final["IMPORTE"] = String.Format("{0:c}", (Suma_Neto_Financiamiento + Suma_Neto_Inversion + Suma_Neto_Operaciones) + Suma_Cuenta_111).Replace("$", "");
            Row_Final["IMPORTE_ANT"] = String.Format("{0:c}", (Suma_Neto_Financiamiento_Anterior + Suma_Neto_Inversion_Anterior + Suma_Neto_Operaciones_Anterior) + Suma_Cuenta_111_Anterior).Replace("$", ""); ;
            Row_Final["NOTA"] = "EFE-01";
            Dt_Final.Rows.Add(Row_Final);
            return Dt_Final; 
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Periodo_Anterior
    ///DESCRIPCIÓN: Realiza el calculo del periodo anterior
    ///PARAMETROS:  String Mes_Numerico.- es el numero del mes
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  24/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected String Periodo_Anterior(String Mes_Numerico)
    {
        String Mes_Anterior = "";
        String Año_Anterior = "";
        Int32 Mes = 0;
        Int32 Año = 0;
        try
        {
            Mes_Anterior = Mes_Numerico.Substring(0, 2);
            if (Mes_Anterior == "01")
            {
                Año_Anterior = Mes_Numerico.Substring(2, 2);
                Año = Convert.ToInt32(Año_Anterior);
                Año--;
                Mes_Anterior = "12" + Año;
            }
            else
            {
                Mes = Convert.ToInt32(Mes_Anterior);
                Año_Anterior = Mes_Numerico.Substring(2, 2);
                Mes--;
                if(Mes <=9)
                    Mes_Anterior = "0" + Mes + "" + Año_Anterior;
                else
                    Mes_Anterior = "" + Mes + "" + Año_Anterior;
            }

            return Mes_Anterior;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Final_Movimientos_Contables
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte de los movimientos contables
    ///PARAMETROS:  Dt_Consulta= es la conslta que se adecuara para precentarce en el reporte
    ///CREO:        Hugo Enrique Ramirez Aguilera
    ///FECHA_CREO:  25/Abril/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Final_Movimientos_Contables(DataTable Dt_Consulta)
    {
        DataTable Dt_Nueva = new DataTable();
        DataRow Row_Nueva;
        try
        {
            Dt_Nueva.Columns.Add("FECHA", typeof(System.DateTime));
            Dt_Nueva.Columns.Add("NO_POLIZA", typeof(System.String));
            Dt_Nueva.Columns.Add("TIPO_POLIZA", typeof(System.String));
            Dt_Nueva.Columns.Add("CUENTA", typeof(System.String));
            Dt_Nueva.Columns.Add("DESCRIPCION", typeof(System.String));
            Dt_Nueva.Columns.Add("CONCEPTO", typeof(System.String));
            Dt_Nueva.Columns.Add("TIPO_MOVIMIENTO", typeof(System.String));
            Dt_Nueva.Columns.Add("IMPORTE", typeof(System.Double));
            
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.Double));


            if (Dt_Consulta is DataTable)
            {
                if (Dt_Consulta.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Row_Nueva["FECHA"] = Registro["fecha"].ToString();
                        Row_Nueva["NO_POLIZA"] = Registro["no_poliza"].ToString();
                        Row_Nueva["TIPO_POLIZA"] = Registro["descripcion"].ToString();
                        Row_Nueva["CUENTA"] = Registro["cuenta"].ToString();
                        Row_Nueva["DESCRIPCION"] = Registro["descripcion1"].ToString();
                        Row_Nueva["CONCEPTO"] = Registro["concepto"].ToString();

                        if (Convert.ToDouble(Registro["debe"].ToString()) > 0)
                        {
                            Row_Nueva["TIPO_MOVIMIENTO"] = "CARGO";
                            Row_Nueva["IMPORTE"] = Convert.ToDouble(Registro["DEBE"].ToString());
                        }
                        else
                        {
                            Row_Nueva["TIPO_MOVIMIENTO"] = "ABONO";
                            Row_Nueva["IMPORTE"] = Convert.ToDouble(Registro["haber"].ToString());
                        }


                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                }
            }

            return Dt_Nueva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }

    }
     ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Efe_02
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte de la nota efe-02
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramirez Aguilera
    ///FECHA_CREO:  06/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Efe_02()
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Estado_Anterior = new DataTable();
        DataTable Dt_Temp_Consulta= new DataTable();
        DataRow Row_Nueva;
        String Filtro = "";
        Double Suma_Cuenta_123 = 0.0;
        Double Suma_Cuenta_Final_123 = 0.0;
        Double Suma_Cuenta_124 = 0.0;
        Double Suma_Cuenta_Final_124 = 0.0;
        Int32 Contador_For = 0;

        try
        {
            Dt_Nueva.Columns.Add("CUENTA", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.Double));
            Dt_Nueva.Columns.Add("SUB", typeof(System.Double));
            Dt_Nueva.TableName = "Dt_Efe_02";

            // para la cuenta 123
            for (Contador_For = 0; Contador_For < 2; Contador_For++)
            {
                if (Contador_For == 0)
                    Filtro = "1235";

                else if (Contador_For == 1)
                    Filtro = "1236";

                Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString();
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                    Row_Nueva["FLUJO"] = Registro["SALDO_FINAL"].ToString();
                    //Row_Nueva["SUB"] = 0.0;
                    Dt_Nueva.Rows.Add(Row_Nueva);

                    //se pasan los valores para el total
                    Suma_Cuenta_123 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());

                }// fin del for

                //para el total de la cuenta principal
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Cuentas = Rs_Consulta.Consulta_Cuentas();
                Row_Nueva = Dt_Nueva.NewRow();
                foreach (DataRow Registro in Dt_Cuentas.Rows)
                {
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString().Substring(0,4);
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                }
                Row_Nueva["FLUJO"] = Suma_Cuenta_123;
                Dt_Nueva.Rows.Add(Row_Nueva);

                Suma_Cuenta_Final_123 += Suma_Cuenta_123;

                //inicializar variables para otra cuenta
                Suma_Cuenta_123 = 0.0;
                
            }// fin del for principal
            
            //para el total de la cuenta 1235, 1236
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "INMUEBLES";
            Row_Nueva["FLUJO"] = Suma_Cuenta_Final_123;
            //Row_Nueva["SUB"] = 0.0;
            Dt_Nueva.Rows.Add(Row_Nueva);


            //para las cuentas de 124
            for (Contador_For = 0; Contador_For < 5; Contador_For++)
            {
                if (Contador_For == 0)
                    Filtro = "1241";

                else if (Contador_For == 1)
                    Filtro = "1242";

                else if (Contador_For == 2)
                    Filtro = "1243";
                
                else if (Contador_For == 3)
                    Filtro = "1244";
                
                else if (Contador_For == 4)
                    Filtro = "1246";

                Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString();
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                    Row_Nueva["FLUJO"] = Registro["SALDO_FINAL"].ToString();
                    //Row_Nueva["SUB"] = 0.0;
                    Dt_Nueva.Rows.Add(Row_Nueva);

                    //se pasan los valores para el total
                    Suma_Cuenta_124 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());

                }// fin del for

                //para el total de la cuenta principal
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Cuentas = Rs_Consulta.Consulta_Cuentas();
                Row_Nueva = Dt_Nueva.NewRow();
                foreach (DataRow Registro in Dt_Cuentas.Rows)
                {
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                }
                Row_Nueva["FLUJO"] = Suma_Cuenta_124;
                Dt_Nueva.Rows.Add(Row_Nueva);

                Suma_Cuenta_Final_124 += Suma_Cuenta_124;

                //inicializar variables para otra cuenta
                Suma_Cuenta_124 = 0.0;

            }// fin del for principal

            //para el total de la cuenta 1235, 1236
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "MUEBLES";
            Row_Nueva["FLUJO"] = Suma_Cuenta_Final_124;
            //Row_Nueva["SUB"] = 0.0;
            Dt_Nueva.Rows.Add(Row_Nueva);

            //para el total de la cuenta 1235, 1236
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "TOTAL";
            Row_Nueva["FLUJO"] = Suma_Cuenta_Final_124 + Suma_Cuenta_Final_123;
            Row_Nueva["SUB"] = 100.0;
            Dt_Nueva.Rows.Add(Row_Nueva);

            return Dt_Nueva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Efe_01
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte de la nota efe-01
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramirez Aguilera
    ///FECHA_CREO:  05/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Efe_01()
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Estado_Anterior = new DataTable();
        DataTable Dt_Temp_Consulta= new DataTable();
        DataRow Row_Nueva;
        String Mes_Anterior = "";
        String Indice = "";
        String Auxiliar = "";
        String Filtro = "";
        Double Suma_Saldo_Final = 0.0;
        Double Suma_Saldo_Inicial = 0.0;
        Double Suma_Saldo_Flujo = 0.0;
        Double Suma_Saldo_Cuenta_Final = 0.0;
        Double Suma_Saldo_Cuenta_Inicial = 0.0;
        Double Suma_Saldo_Cuenta_Flujo = 0.0;
        Double Suma_Saldo_Final_Final = 0.0;
        Double Suma_Saldo_Final_Inicial = 0.0;
        Double Suma_Saldo_Final_Flujo = 0.0;
        Int32 Contador_For=0;

        try
        {
            Dt_Nueva.Columns.Add("CUENTA", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_INICIAL", typeof(System.Double));
            Dt_Nueva.Columns.Add("SALDO_FINAL", typeof(System.Double));
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.Double));
            Dt_Nueva.TableName = "Dt_Efe_01";

            for (Contador_For = 0; Contador_For < 5; Contador_For++)
            {
                if (Contador_For == 0)
                    Filtro = "1111";
                else if (Contador_For == 1)
                    Filtro = "1112";
                else if (Contador_For == 2)
                    Filtro = "1114";
                else if (Contador_For == 3)
                    Filtro = "1115";
                else if (Contador_For == 4)
                    Filtro = "1116";

                Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
                //para el mes anterior al reporte a reportar
                Mes_Anterior = Periodo_Anterior(Cmb_Mes.SelectedValue.ToString());
                Rs_Consulta.P_Mes_Año = Mes_Anterior;
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Estado_Anterior = Rs_Consulta.Consulta_Situacion_Financiera();

                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    Indice = (Registro["CUENTA"].ToString());

                    foreach (DataRow Cuenta_Anterior in Dt_Estado_Anterior.Rows)
                    {
                        Auxiliar = (Cuenta_Anterior["CUENTA"].ToString());
                        if (Indice == Auxiliar)
                        {
                            if (!String.IsNullOrEmpty(Cuenta_Anterior["SALDO_FINAL"].ToString()))
                                Suma_Saldo_Inicial += Convert.ToDouble(Cuenta_Anterior["SALDO_FINAL"].ToString());
                            else
                                Suma_Saldo_Inicial += 0;
                        }
                    }
                    Suma_Saldo_Final += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                    Suma_Saldo_Flujo = Suma_Saldo_Final - Suma_Saldo_Inicial;
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString();
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                    Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                    Row_Nueva["FLUJO"] = Suma_Saldo_Flujo;
                    Dt_Nueva.Rows.Add(Row_Nueva);

                    //se pasan los valores para el total
                    Suma_Saldo_Cuenta_Inicial += Suma_Saldo_Inicial;
                    Suma_Saldo_Cuenta_Final += Suma_Saldo_Final;
                    Suma_Saldo_Cuenta_Flujo += Suma_Saldo_Flujo;
                    //inicializar variables
                    Suma_Saldo_Inicial = 0.0;
                    Suma_Saldo_Final = 0.0;
                    Suma_Saldo_Flujo = 0.0;
                }// fin del for

                //para el total de la cuenta principal
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Cuentas = Rs_Consulta.Consulta_Cuentas();
                Row_Nueva = Dt_Nueva.NewRow();
                foreach (DataRow Registro in Dt_Cuentas.Rows)
                {
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                }
                Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Cuenta_Inicial;
                Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Cuenta_Final;
                Row_Nueva["FLUJO"] = Suma_Saldo_Cuenta_Flujo;
                Dt_Nueva.Rows.Add(Row_Nueva);

                Suma_Saldo_Final_Inicial += Suma_Saldo_Cuenta_Inicial;
                Suma_Saldo_Final_Final += Suma_Saldo_Cuenta_Final;
                Suma_Saldo_Final_Flujo += Suma_Saldo_Cuenta_Flujo;
                //inicializar variables para otra cuenta
                Suma_Saldo_Inicial = 0.0;
                Suma_Saldo_Final = 0.0;
                Suma_Saldo_Flujo = 0.0;
                Suma_Saldo_Cuenta_Inicial = 0.0;
                Suma_Saldo_Cuenta_Final = 0.0;
                Suma_Saldo_Cuenta_Flujo = 0.0;

            }// fin del for principal

            //para el total final de la nota
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "TOTAL";
            Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Final_Inicial;
            Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final_Final;
            Row_Nueva["FLUJO"] = Suma_Saldo_Final_Flujo;
            Dt_Nueva.Rows.Add(Row_Nueva);

            return Dt_Nueva;
        }
         catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
       
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Final
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  23/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Final(DataTable Dt_Consulta, DataTable Dt_Perido_Anterior)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Temp_Consulta= new DataTable();
        DataRow Row_Nueva;
        String Cuenta;
        int Contador_For = 0;
        Double SALDO = 0.0;
        Double CARGO = 0.0;
        Double ABONO = 0.0;
        Double SALDO_INICIAL = 0.0;
        try
        {
            Dt_Nueva.Columns.Add("INDICE", typeof(System.Double));
            Dt_Nueva.Columns.Add("DESCRIPCION ", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_INICIAL", typeof(System.String));
            Dt_Nueva.Columns.Add("CARGO", typeof(System.String));
            Dt_Nueva.Columns.Add("ABONO", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_FINAL", typeof(System.String));
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.String));
            Dt_Nueva.TableName = "Dt_Estado_Analitico";
            Dt_Final.Columns.Add("INDICE", typeof(System.Double));
            Dt_Final.Columns.Add("DESCRIPCION ", typeof(System.String));
            Dt_Final.Columns.Add("SALDO_INICIAL", typeof(System.String));
            Dt_Final.Columns.Add("CARGO", typeof(System.String));
            Dt_Final.Columns.Add("ABONO", typeof(System.String));
            Dt_Final.Columns.Add("SALDO_FINAL", typeof(System.String));
            Dt_Final.Columns.Add("FLUJO", typeof(System.String));
            Dt_Final.TableName = "Dt_Estado_Analitico";

            Dt_Cuentas = Rs_Consulta.Consulta_Cuentas2();

            Dt_Temp_Consulta.Columns.Add("CUENTA", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("DESCRIPCION", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("NIVEL_ID", typeof(System.String));
            DataRow row;
            foreach (DataRow Renglon in Dt_Cuentas.Rows)
            {
               
                if (Renglon["CUENTA"].ToString().Substring(0, 1) == "1")
                {
                    row = Dt_Temp_Consulta.NewRow();
                    row["CUENTA"] = Renglon["CUENTA"].ToString(); ;
                    row["DESCRIPCION"] = Renglon["DESCRIPCION"].ToString();
                    row["NIVEL_ID"] = Renglon["NIVEL_ID"].ToString();
                    Dt_Temp_Consulta.Rows.Add(row);
                    Dt_Temp_Consulta.AcceptChanges();
                }

            }
            //se agregaran los saldos de las cuentas 
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count ; Contador_For++)
            {
                
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00004")
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    SALDO = 0;
                    CARGO=0;
                    ABONO=0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Consulta.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 4) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString()) - Convert.ToDouble(Renglon["TOTAL_HABER"].ToString()); //Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString()) + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString()) - Convert.ToDouble(Renglon["TOTAL_HABER"].ToString()); 
                            CARGO = CARGO + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                            //SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString());
                        }
                    }
                    foreach (DataRow Renglon in Dt_Perido_Anterior.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 4) == Cuenta)
                        {
                           SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                           SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0,4);
                    Row_Nueva["DESCRIPCION "] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                    Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO).Replace("$", "");
                    Row_Nueva["CARGO"] = String.Format("{0:c}", CARGO).Replace("$", "");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", SALDO - SALDO_INICIAL).Replace("$", ""); 
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Dt_Nueva.AcceptChanges();
                    }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00003")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 3);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0,3) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString().Replace(",", "")) + Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", "")) - Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",",""));
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",",""));
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString().Replace(",", ""));
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0,4);
                    Row_Nueva["DESCRIPCION "] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$","");
                    Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO).Replace("$","");
                    Row_Nueva["CARGO"] = String.Format("{0:c}", CARGO).Replace("$","");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$","");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", SALDO - SALDO_INICIAL).Replace("$", "");
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
               }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00002")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 2);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 2) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString().Replace(",", "")) + Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", "")) - Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", ""));
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString().Replace(",", ""));
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["DESCRIPCION "] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                    Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO).Replace("$", "");
                    Row_Nueva["CARGO"] = String.Format("{0:c}", CARGO).Replace("$", "");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", SALDO - SALDO_INICIAL).Replace("$", ""); 
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00001")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0,1);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 1) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString().Replace(",", "")) + Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", "")) - Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", ""));
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString().Replace(",", ""));
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["DESCRIPCION "] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                    Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO).Replace("$", "");
                    Row_Nueva["CARGO"] = String.Format("{0:c}", CARGO).Replace("$", "");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", SALDO - SALDO_INICIAL).Replace("$", ""); 
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            foreach (DataRow Renglon in Dt_Final.Rows)
            {
                Row_Nueva = Dt_Nueva.NewRow();
                Row_Nueva["INDICE"] = Renglon["INDICE"].ToString();
                Row_Nueva["DESCRIPCION "] = Renglon["DESCRIPCION "].ToString();
                Row_Nueva["SALDO_INICIAL"] = Renglon["SALDO_INICIAL"].ToString();
                Row_Nueva["SALDO_FINAL"] = Renglon["SALDO_FINAL"].ToString();
                Row_Nueva["CARGO"] =Renglon["CARGO"].ToString();
                Row_Nueva["ABONO"] = Renglon["ABONO"].ToString();
                Row_Nueva["FLUJO"] = Renglon["FLUJO"].ToString();
                Dt_Nueva.Rows.Add(Row_Nueva);
                Dt_Nueva.AcceptChanges();

            }
            
            return Dt_Nueva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Final
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  23/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Final_DyP(DataTable Dt_Consulta,DataTable Dt_Consulta_Anterior)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Temp_Consulta = new DataTable();
        DataRow Row_Nueva;
        String Cuenta;
        int Contador_For = 0;
        Double SALDO = 0.0;
        Double CARGO = 0.0;
        Double ABONO = 0.0;
        Double SALDO_INICIAL = 0.0;
        try
        {
            //se declaran las columnas de los datatable
            Dt_Nueva.Columns.Add("INDICE", typeof(System.Double));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_INICIAL", typeof(System.String));
            Dt_Nueva.Columns.Add("CARGO", typeof(System.String));
            Dt_Nueva.Columns.Add("ABONO", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_FINAL", typeof(System.String));
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.String));
            Dt_Nueva.TableName = "Dt_Est_Analitico_DyP";
            Dt_Final.Columns.Add("INDICE", typeof(System.Double));
            Dt_Final.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Final.Columns.Add("SALDO_INICIAL", typeof(System.String));
            Dt_Final.Columns.Add("CARGO", typeof(System.String));
            Dt_Final.Columns.Add("ABONO", typeof(System.String));
            Dt_Final.Columns.Add("SALDO_FINAL", typeof(System.String));
            Dt_Final.Columns.Add("FLUJO", typeof(System.String));
            Dt_Final.TableName = "Dt_Est_Analitico_DyP";
            Dt_Cuentas = Rs_Consulta.Consulta_Cuentas2();
            Dt_Temp_Consulta.Columns.Add("CUENTA", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("NIVEL_ID", typeof(System.String));
            DataRow row;
            foreach (DataRow Renglon in Dt_Cuentas.Rows)
            {

                if (Renglon["CUENTA"].ToString().Substring(0, 1) == "2")
                {
                    row = Dt_Temp_Consulta.NewRow();
                    row["CUENTA"] = Renglon["CUENTA"].ToString(); ;
                    row["NOMBRE"] = Renglon["DESCRIPCION"].ToString();
                    row["NIVEL_ID"] = Renglon["NIVEL_ID"].ToString();
                    Dt_Temp_Consulta.Rows.Add(row);
                    Dt_Temp_Consulta.AcceptChanges();
                }
            }
            //se agregaran los saldos de las cuentas 
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {

                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00004")
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Consulta.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 4) == Cuenta)
                        {
                            SALDO = SALDO + ((Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString()) - Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString())) + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString()));
                            CARGO = CARGO + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                            //SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString());
                        }
                    }
                    foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 4) == Cuenta)
                        {
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = String.Format("{0:c}",SALDO_INICIAL).Replace("$","");
                    Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO_INICIAL + ABONO - CARGO).Replace("$", "");
                    Row_Nueva["CARGO"] = String.Format("{0:c}", CARGO).Replace("$", ""); 
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", Math.Round((SALDO_INICIAL + ABONO - CARGO) - SALDO_INICIAL, 4)).Replace("$", "");
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Dt_Nueva.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00003")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 3);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 3) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString().Replace(",", "")) - Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", "")) + Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", ""));
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                        }
                    }
                    foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 3) == Cuenta)
                        {
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                    Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO).Replace("$", "");
                    Row_Nueva["CARGO"] = String.Format("{0:c}", CARGO).Replace("$", "");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", Math.Round(SALDO - SALDO_INICIAL, 4)).Replace("$", "");
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {

                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00002")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 2);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 2) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString().Replace(",", "")) - Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", "")) + Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", ""));
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                        }
                    }
                    foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 2) == Cuenta)
                        {
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                    Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO_INICIAL + ABONO - CARGO).Replace("$", "");
                    Row_Nueva["CARGO"] = String.Format("{0:c}", CARGO).Replace("$", "");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", Math.Round((SALDO_INICIAL + ABONO - CARGO) - SALDO_INICIAL, 4)).Replace("$", "");
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00001")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 1);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 1) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString().Replace(",", "")) - Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", "")) + Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString().Replace(",", ""));
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString().Replace(",", ""));
                        }
                    }
                    foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 1) == Cuenta)
                        {
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                    Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO_INICIAL + ABONO - CARGO).Replace("$", "");
                    Row_Nueva["CARGO"] = String.Format("{0:c}", CARGO).Replace("$", "");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", Math.Round((SALDO_INICIAL + ABONO - CARGO) - SALDO_INICIAL, 4)).Replace("$", "");
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            foreach (DataRow Renglon in Dt_Final.Rows)
            {
                Row_Nueva = Dt_Nueva.NewRow();
                Row_Nueva["INDICE"] = Renglon["INDICE"].ToString();
                Row_Nueva["NOMBRE"] = Renglon["NOMBRE"].ToString();
                Row_Nueva["SALDO_INICIAL"] = Renglon["SALDO_INICIAL"].ToString();
                Row_Nueva["SALDO_FINAL"] = Renglon["SALDO_FINAL"].ToString();
                Row_Nueva["CARGO"] = Renglon["CARGO"].ToString();
                Row_Nueva["ABONO"] = Renglon["ABONO"].ToString();
                Row_Nueva["FLUJO"] = Renglon["FLUJO"].ToString();
                Dt_Nueva.Rows.Add(Row_Nueva);
                Dt_Nueva.AcceptChanges();

            }

            return Dt_Nueva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Rpt_Excel
    /// DESCRIPCION :   Se encarga de generar el archivo de excel pasandole los paramentros
    ///               al documento
    /// PARAMETROS  :   Dt_Consulta_Reporte.- Es la consulta que contiene la informacion que se reportara
    /// CREO        :   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   25/Abril/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Generar_Rpt_Excel(DataSet Ds_Reporte, String Nombre)
    {
        String Nombre_Archivo = "";
        String Ruta_Archivo = "";
        Double Importe = 0.0;
        DateTime Dia_Ultimo = new DateTime();
        Dia_Ultimo = new DateTime(Convert.ToInt32(Cmb_Anio.SelectedItem.Text),(Convert.ToInt32(Cmb_Mes.SelectedValue.Substring(0,2)) + 1), 1);
        Dia_Ultimo = Dia_Ultimo.AddDays(-1);
        int Cantidad=0;
        String Indice="";
        String Tipo = "NO";
        string Titulo_Reporte_Unificado = string.Empty; //Variable que contendra el titulo del reporte
        string aux = string.Empty; //variable auxiliar para la construccion del titulo
        string Clave_CONAC = string.Empty; //variable para la clave de los archivos de la CONAC

        try
        {
            Nombre_Archivo = Nombre + "_" + Cmb_Mes.SelectedItem.Text.Substring(0,1)+""+Cmb_Mes.SelectedItem.Text.Substring(1,2).ToLower() + "_" + DateTime.Now.ToString("yy") + ".xls";
            Ruta_Archivo = @Server.MapPath("../../Reporte/" + Nombre_Archivo);
            //Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            Libro.Properties.Title = "FLUJO DE EFECTIVO";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI_Irapuato";
            Worksheet Hoja;
            //Creamos una hoja que tendrá el libro.
            if (Nombre == "05_EAA_15_AP")
            {
                 Hoja = Libro.Worksheets.Add("EAA");
            }
            else
            {
                if (Nombre == "04_EFE_15_AP")
                {
                     Hoja = Libro.Worksheets.Add("EFE");
                }
                else
                {
                    if (Nombre == "06_EADOP_15_AP")
                    {
                         Hoja = Libro.Worksheets.Add("EADOP");
                    }
                    else
                    {
                        Hoja = Libro.Worksheets.Add("BDMC");
                    }
                }
            }            
            //Agregamos un renglón a la hoja de excel.
            WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //Creamos el estilo cabecera para la hoja de excel. 
            WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("Encabezado");
            WorksheetStyle Estilo_Texto= Libro.Styles.Add("Texto");
            //Creamos el estilo Totales para la hoja de excel. 
            WorksheetStyle Estilo_Totales = Libro.Styles.Add("Totales");
            //Creamos el estilo Totales_Rojo para la hoja de excel. 
            WorksheetStyle Estilo_Totales_Color_Rojo = Libro.Styles.Add("Totales_Rojo");
            //Creamos el estilo Totales_Rojo para la hoja de excel. 
            WorksheetStyle Estilo_Totales_Negritas = Libro.Styles.Add("Totales_Negritas");
            //Creamos el estilo Totales_Negro para la hoja de excel. 
            WorksheetStyle Estilo_Totales_Negritas_Montos = Libro.Styles.Add("Totales_Negritas_Montos");
            //Estilo del campo oculto de la CONAC
            WorksheetStyle Estilo_Encabezado_Oculto = Libro.Styles.Add("Encabezado_Oculto");
            //Creamos una celda
            WorksheetCell Celda = new WorksheetCell();
            #region Estilos
                Estilo_Cabecera.Font.FontName = "Arial";
                Estilo_Cabecera.Font.Size = 8;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cabecera.Alignment.Rotate = 0;
                Estilo_Cabecera.Font.Color = "#000000";
                Estilo_Cabecera.Interior.Color = "#F79646";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                //Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Alignment.WrapText = true; //Indica que puede haber multilinea

                Estilo_Texto.Font.FontName = "Arial";
                Estilo_Texto.Font.Size = 8;
                Estilo_Texto.Font.Bold = false;
                Estilo_Texto.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Texto.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Texto.Alignment.Rotate = 0;
                Estilo_Texto.Font.Color = "#000000";
                Estilo_Texto.Interior.Color = "White";
                Estilo_Texto.Interior.Pattern = StyleInteriorPattern.None;
                Estilo_Texto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Texto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Texto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Texto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");


                Estilo_Totales.Font.FontName = "Arial";
                Estilo_Totales.Font.Size = 8;
                Estilo_Totales.Font.Bold = false;
                Estilo_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Totales.Alignment.Rotate = 0;
                Estilo_Totales.Font.Color = "#000000";
                Estilo_Totales.Interior.Color = "White";
                Estilo_Totales.NumberFormat = "_0* #,##0.00;\\-* #,##0.00_0;* \"0.00\";_-@_-";
                Estilo_Totales.Interior.Pattern = StyleInteriorPattern.None;
                Estilo_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Totales_Color_Rojo.Font.FontName = "Arial";
                Estilo_Totales_Color_Rojo.Font.Size = 8;
                Estilo_Totales_Color_Rojo.Font.Bold = false;
                Estilo_Totales_Color_Rojo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Totales_Color_Rojo.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Totales_Color_Rojo.Alignment.Rotate = 0;
                Estilo_Totales_Color_Rojo.Font.Color = "RED";
                Estilo_Totales_Color_Rojo.Interior.Color = "White";
                Estilo_Totales_Color_Rojo.Interior.Pattern = StyleInteriorPattern.None;
                Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Totales_Negritas.Font.FontName = "Arial";
                Estilo_Totales_Negritas.Font.Size = 8;
                Estilo_Totales_Negritas.Font.Bold = true;
                Estilo_Totales_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Totales_Negritas.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Totales_Negritas.Alignment.Rotate = 0;
                Estilo_Totales_Negritas.Font.Color = "#000000";
                Estilo_Totales_Negritas.Interior.Color = "White";
                Estilo_Totales_Negritas.Interior.Pattern = StyleInteriorPattern.None;
                Estilo_Totales_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Totales_Negritas_Montos.Font.FontName = "Arial";
                Estilo_Totales_Negritas_Montos.Font.Size = 8;
                Estilo_Totales_Negritas_Montos.Font.Bold = true;
                Estilo_Totales_Negritas_Montos.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Totales_Negritas_Montos.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Totales_Negritas_Montos.Alignment.Rotate = 0;
                Estilo_Totales_Negritas_Montos.Font.Color = "#000000";
                Estilo_Totales_Negritas_Montos.Interior.Color = "White";
                Estilo_Totales_Negritas_Montos.NumberFormat = "_0* #,##0.00;\\-* #,##0.00_0;* \"0.00\";_-@_-";
                Estilo_Totales_Negritas_Montos.Interior.Pattern = StyleInteriorPattern.None;
                Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Encabezado_Oculto.Font.FontName = "Arial";
                Estilo_Encabezado_Oculto.Font.Size = 8;
                Estilo_Encabezado_Oculto.Font.Bold = true;
                Estilo_Encabezado_Oculto.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Encabezado_Oculto.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Encabezado_Oculto.Alignment.Rotate = 0;
                Estilo_Encabezado_Oculto.Font.Color = "#F79646";
                Estilo_Encabezado_Oculto.Interior.Color = "#F79646";
                Estilo_Encabezado_Oculto.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Encabezado_Oculto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Encabezado_Oculto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Encabezado_Oculto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Encabezado_Oculto.Alignment.WrapText = true; //Indica que puede haber multilinea
            #endregion

                if (Nombre == "05_EAA_15_AP" || Nombre == "06_EADOP_15_AP")
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//  1 indice.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(400));//  4 Tipo.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  1 indice.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  4 Tipo.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  1 indice.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  4 Tipo.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  1 indice.
                }
                else
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//  1 indice.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(400));//  4 Tipo.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  1 indice.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  4 Tipo.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//  1 indice

                }
            foreach (DataTable Dt_Consulta_Reporte in Ds_Reporte.Tables)
            {
                Cantidad = Dt_Consulta_Reporte.Columns.Count;
                //se llena el encabezado principal
                aux = "JUNTA DE AGUA POTABLE DRENAJE ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO";
                aux = aux.PadRight(1024 - aux.Length);
                Titulo_Reporte_Unificado = aux;
                switch (Dt_Consulta_Reporte.TableName)
                {
                    case "Dt_Estado_Analitico":
                        aux = "ESTADO ANALÍTICO DEL ACTIVO";
                        Titulo_Reporte_Unificado += aux.PadRight(1024 - aux.Length);
                        Titulo_Reporte_Unificado += "Del 01 al " + Dia_Ultimo.Day + " DE " + Cmb_Mes.SelectedItem.Text + " DEL " + Cmb_Anio.SelectedValue;
                        Tipo = "SI";
                        break;
                    case "Dt_Flujo_De_Efectivo":
                        aux = "ESTADO DE FLUJOS DE EFECTIVO";
                        aux = aux.PadRight(1024 - aux.Length);
                        Titulo_Reporte_Unificado += aux;
                        Titulo_Reporte_Unificado += "Del 01 al " + Dia_Ultimo.Day + " DE " + Cmb_Mes.SelectedItem.Text + " DEL " + Cmb_Anio.SelectedValue;
                        break;
                    case "Dt_Est_Analitico_DyP":
                        aux = "ESTADO ANALÍTICO DE LA DEUDA Y OTROS PASIVOS";
                        Titulo_Reporte_Unificado += aux.PadRight(1024 - aux.Length);
                        Titulo_Reporte_Unificado += " al " + Dia_Ultimo.Day + " DE " + Cmb_Mes.SelectedItem.Text + " DEL " + Cmb_Anio.SelectedValue;
                        Tipo = "SI";
                        break;
                    case "Dt_Movimientos_Contables":
                        Titulo_Reporte_Unificado += "BASE DE DATOS CON MOVIMIENTOS CONTABLES";
                        break;
                    case "Dt_Efe_01":
                        Titulo_Reporte_Unificado += "FLUJO DE EFECTIVO";
                        break;
                    case "Dt_Efe_02":
                        Titulo_Reporte_Unificado += "ADQUISICION BIENES MUEBLES E INMUEBLES";
                        break;
                    default:
                        break;
                }

                //Colocar el texto del encabezado
                Celda = Renglon.Cells.Add(Titulo_Reporte_Unificado, DataType.String, "Encabezado");
                Celda.Row.Height = 50;
                Celda.MergeAcross = Dt_Consulta_Reporte.Columns.Count - 2;

                //Colocar la celda de la clave de CONAC
                Clave_CONAC = Consulta_Campo_Oculto_CONAC();
                Renglon.Cells.Add(new WorksheetCell(Clave_CONAC, DataType.String, "Encabezado_Oculto"));

                Renglon = Hoja.Table.Rows.Add();
                foreach (DataColumn COLUMNA in Dt_Consulta_Reporte.Columns)
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));

                    //Verificar el nombre de la columna
                    switch (COLUMNA.ColumnName.ToUpper().Trim())
                    {
                        case "IMPORTE":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PERIODO ACTUAL", "Encabezado"));
                            break;

                        case "IMPORTE_ANT":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PERIODO ANTERIOR", "Encabezado"));
                            break;

                        case "INDICE":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ÍNDICE", "Encabezado"));
                            break;

                        case "SALDO_INICIAL":
                            //Verificar el tipo de reporte
                            switch (Nombre)
                            {
                                case "05_EAA_15_AP":
                                case "06_EADOP_15_AP":
                                    aux = "SALDO INICIAL";
                                    aux = aux.PadRight(1024 - aux.Length);
                                    aux += "(A)";
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(aux, "Encabezado"));
                                    break;

                                default:
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
                                    break;
                            }
                            break;

                        case "SALDO_FINAL":
                            //Verificar el tipo de reporte
                            switch (Nombre)
                            {
                                case "05_EAA_15_AP":
                                case "06_EADOP_15_AP":
                                    aux = "SALDO FINAL";
                                    aux = aux.PadRight(1024 - aux.Length);
                                    aux += "(B)";
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(aux, "Encabezado"));
                                    break;

                                default:
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
                                    break;
                            }
                            break;

                        case "FLUJO":
                            //Verificar el tipo de reporte
                            switch (Nombre)
                            {
                                case "05_EAA_15_AP":
                                case "06_EADOP_15_AP":
                                    aux = "FLUJO";
                                    aux = aux.PadRight(1024 - aux.Length);
                                    aux += "(B-A)";
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(aux, "Encabezado"));
                                    break;

                                default:
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
                                    break;
                            }
                            break;

                        case "DESCRIPCION":
                            if (Nombre == "05_EAA_15_AP")
                            {
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE", "Encabezado"));
                            }
                            else
                            {
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DESCRIPCION", "Encabezado"));
                            }
                            break;

                        default:
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName.Replace('_', ' '), "Encabezado"));
                            break;
                    }
                }
                foreach (DataRow FILA in Dt_Consulta_Reporte.Rows)
                {
                    Renglon = Hoja.Table.Rows.Add();
                    if (FILA[0].ToString() != "")
                    {
                        if (Tipo == "SI")
                        {
                            Indice = FILA[0].ToString();
                        }
                        else
                        {
                            Indice = "1111";
                        }
                    }
                    else
                    {
                        Indice = "0000";
                    }
                    foreach (DataColumn COLUMNA in Dt_Consulta_Reporte.Columns)
                    {
                        switch (COLUMNA.ToString().ToUpper())
                        {
                            case "FECHA":
                                {
                                    if (Indice.Substring(3, 1) == "0")
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:dd/MM/yyyy}", FILA[COLUMNA]),
                                            DataType.String,
                                            "Totales_Negritas"));
                                    }
                                    else
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:dd/MM/yyyy}", FILA[COLUMNA]),
                                            DataType.String,
                                            "Totales"));
                                    }
                                }
                                break;
                            case "IMPORTE":
                                {
                                    if (Indice.Substring(3, 1) == "0")
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales_Negritas_Montos" : "Totales_Negritas_Montos"));
                                    }
                                    else
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales" : "Totales"));
                                    }
                                }
                                break;
                            case "IMPORTE_ANT":
                                {
                                    if (Indice.Substring(3, 1) == "0")
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales_Negritas_Montos" : "Totales_Negritas_Montos"));
                                    }
                                    else
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales" : "Totales"));
                                    }
                                }
                                break;
                            case "CARGO":
                                {
                                    if (Indice.Substring(3, 1) == "0")
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales_Negritas_Montos" : "Totales_Negritas_Montos"));
                                    }
                                    else
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales" : "Totales"));
                                    }
                                }
                                break;
                            case "ABONO":
                                {
                                    if (Indice.Substring(3, 1) == "0")
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales_Negritas_Montos" : "Totales_Negritas_Montos"));
                                    }
                                    else
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales" : "Totales"));
                                    }
                                }
                                break;
                            case "FLUJO":
                                {
                                    if (Indice.Substring(3, 1) == "0")
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales_Negritas_Montos" : "Totales_Negritas_Montos"));
                                    }
                                    else
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales" : "Totales"));
                                    }
                                }
                                break;
                            case "SALDO_INICIAL":
                                {
                                    if (Indice.Substring(3, 1) == "0")
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales_Negritas_Montos" : "Totales_Negritas_Montos"));
                                    }
                                    else
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales" : "Totales"));
                                    }
                                }
                                break;
                            case "SALDO_FINAL":
                                {
                                    if (Indice.Substring(3, 1) == "0")
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales_Negritas_Montos" : "Totales_Negritas_Montos"));
                                    }
                                    else
                                    {
                                        Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                            String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                            DataType.Number,
                                            Importe >= 0 ? "Totales" : "Totales"));
                                    }
                                }
                                break;
                            default:
                                if (Indice.Substring(3, 1) == "0")
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA].ToString(), "Totales_Negritas"));
                                }
                                else
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA].ToString(), "Texto"));
                                }
                                break;
                        }
                    }
                }
                Renglon = Hoja.Table.Rows.Add();
            }
            Renglon = Hoja.Table.Rows.Add();
            Celda = Renglon.Cells.Add("Bajo protesta de decir verdad declaramos que los Estados Financieros y sus notas, son razonablemente correctos y son responsabilidad del emisor.");
            Celda.MergeAcross = Cantidad - 1;
            Renglon = Hoja.Table.Rows.Add();
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Nombre_Archivo);
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            //Hoja.Protected = true;
            Libro.Save(Response.OutputStream);
            Response.End();
        }
        catch (System.Threading. ThreadAbortException Ex)
        {}
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
        }
    }
 #endregion
    #region Eventos
    #region (Botones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_Analitico_Click
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  21/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Reporte_Analitico_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Reporte())
            {
                    Consulta_Estado_Analitico("PDF");
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    /// *************************************************************************************
    /// NOMBRE:         Btn_Generar_Reporte_Excel_Click
    /// DESCRIPCIÓN:    Genera el reporte en el formato de excel
    /// PARÁMETROS:     No Aplica
    /// 
    /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
    /// FECHA CREO:     23/Abril/2012
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Btn_Generar_Reporte_Excel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Reporte())
            {
                Consulta_Estado_Analitico("EXCEL");
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (System.Threading.ThreadAbortException ex)
        { }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte Catálogo de empleados. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operacion actual que se este realizando
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  18/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }
    #endregion
    #endregion
}
