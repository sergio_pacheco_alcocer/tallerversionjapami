﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Conciliaciones.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Cheques_Por_Fecha" Title="Reporte Cheques Dia" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server" >
    <asp:ScriptManager ID="ScriptManager_Areas" runat="server"  EnableScriptGlobalization="true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>        
            <asp:UpdateProgress ID="Uprg_Conciliaciones" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
               <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Reporte_Cuentas_Afectables" style="background-color:#ffffff; width:100%; height:100%;">    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Conciliaciones Bancarias</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td colspan="2">                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;">
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                CssClass="Img_Button" TabIndex="1"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                onclick="Btn_Nuevo_Click" />
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                onclick="Btn_Salir_Click"/>
                                        </td>
                                      <td align="right" style="width:41%;">&nbsp;</td>
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>   
                <table width="99%" class="estilo_fuente">
                    <tr>
                            <td style="width:10%">
                              * Archivo
                            </td>
                            <td  style="text-align: left; width: 50%;" colspan="2">
<%--                                <asp:Label ID="Throbber" Text="wait" runat="server"  Width="80%">
                                    <div id="Div5" class="progressBackgroundFilter"></div>
                                    <div  class="processMessage" id="div6">
                                         <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                    </div>
                                </asp:Label>--%>
                                <cc1:AsyncFileUpload ID="AFU_Archivo" runat="server" CompleteBackColor="LightBlue" ErrorBackColor="Red" 
                                     ThrobberID="Throbber"  UploadingBackColor="LightGray" onuploadedcomplete="AFU_Archivo_Excel_UploadedComplete" Width="450px" Enabled="true"  />
                            </td>
                            <td>
                                    <asp:Button ID="Btn_Cargar_Archivo" runat="server"  Text="Subir Archivo" CssClass="button"  
                                    CausesValidation="false"  Width="30%" OnClick="Btn_Cargar_Excel_Click" />
                            </td>
                                </tr>
                                <tr>
                                    <td style="width:10%;text-align:left;">* Banco</td>
                                    <td style="width:40%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Banco" runat="server" Width="90%" TabIndex="1" Font-Size="X-Small" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Banco_Transferencia_OnSelectedIndexChanged"/>
                                    </td>
                                    <td style="width:10%">
                                            <asp:Label ID="Lbl_Cuenta" runat="server" Text="* Cuenta"></asp:Label>
                                    </td>
                                    <td style="width:40%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Cuenta" runat="server" Width="90%" TabIndex="2" Font-Size="X-Small"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:10%">
                                        <asp:Label ID="Lbl_Fecha_Inicio" runat="server" Text="*Fecha Inicio"></asp:Label>
                                    </td>
                                    <td style="width:40%">
                                        <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="15px" Font-Size="X-Small" />
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_inicio"/>
                                         <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Inicio" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Inicio" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="Mev_Txt_Fecha_Poliza" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Inicio"
                                            ControlExtender="Mee_Txt_Fecha_Inicio" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Inicio Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                        </td>
                                        <td style="width:10%">
                                            <asp:Label ID="Lbl_Fecha_Final" runat="server" Text="*Fecha Final"></asp:Label>
                                        </td>
                                        <td style="width:40%">
                                            <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="15px"  Font-Size="X-Small"/>
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Final" runat="server" 
                                            TargetControlID="Txt_Fecha_Final" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año"  />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Final" runat="server" 
                                            TargetControlID="Txt_Fecha_Final" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Final"/>
                                         <asp:ImageButton ID="Btn_Fecha_Final" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Final" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Final" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="MaskedEditValidator1" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Final"
                                            ControlExtender="Mee_Txt_Fecha_Final" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Final Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                     <tr>
                        <td  runat="server" id="Tr_Grid_Cuentas" colspan="4" style="display:none;">
                               <div>
                                    <table width="100%"  border="0" cellspacing="0">
                                        <tr>
                                            <td colspan="6">
                                                <asp:Label ID="Lbl_Titulo" runat="server" Text="Registros NO encontrados en el sistema"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr >
                                             <td Font-Size="XX-Small"  style=" width:20%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;"> &nbsp;Cuenta</td>
                                            <td Font-Size="XX-Small" style="width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Fecha Ingreso</td>
                                            <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;Referencia</td>
                                            <td Font-Size="XX-Small" style="width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;Ref. Banco</td>
                                            <td Font-Size="XX-Small" style="width:30%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;Descripcion</td>
                                            <td Font-Size="XX-Small" style="width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;&nbsp;Monto</td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="overflow:auto;height:300px;width:99%;vertical-align:top;border-style:outset;border-color:Silver; position:static" >
                                    <asp:GridView ID="Grid_Cuentas_No_Identificadas" runat="server" AllowPaging="False"  ShowHeader="false"
                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="99%" EmptyDataText="No se encontraron coincidencias">
                                        <Columns>
                                            <asp:BoundField DataField="NUMERO_CUENTA" ShowHeader="false" HeaderStyle-Font-Size="X-Small">
                                                <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="15%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA_INGRESO"  HeaderStyle-Font-Size="X-Small"  ShowHeader="false" DataFormatString="{0:dd/MMM/yyyy}">
                                                <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Center" Width="15%" />
                                            </asp:BoundField>                                                            
                                            <asp:BoundField DataField="REFERENCIA_CLIENTE"  HeaderStyle-Font-Size="X-Small" ShowHeader="false" HeaderText="Solicitud Pago">
                                                <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="center"  Width="15%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="REFERENCIA_BANCO" ShowHeader="false" HeaderStyle-Font-Size="X-Small">
                                                <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="center"  Width="10%" />
                                            </asp:BoundField>                                                            
                                            <asp:BoundField DataField="DESCRIPCION"  HeaderStyle-Font-Size="X-Small"  ShowHeader="false">
                                                <HeaderStyle HorizontalAlign="left" Width="30%" />
                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="30%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MONTO"  HeaderStyle-Font-Size="X-Small" ShowHeader="false" DataFormatString="{0:C}">
                                                <HeaderStyle HorizontalAlign="right" Width="15%" />
                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="right" Width="15%" />
                                            </asp:BoundField>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                        <FooterStyle CssClass="GridPager" />
                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                        <PagerStyle CssClass="GridPager" />
                                        <RowStyle CssClass="GridItem" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                    </asp:GridView>
                                </div>
                        </td>
                    </tr>
                    <tr>
                    <td  runat="server" id="Tr_Grid_Cuentas_Conciliadas" colspan="4" style="display:none;">
                               <div>
                                    <table width="100%"  border="0" cellspacing="0">
                                        <tr>
                                            <td colspan="6">
                                                <asp:Label ID="Lbl_Titulo_Encontrados" runat="server" Text="Registros Encontrados en el sistema"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr >
                                             <td Font-Size="XX-Small"  style=" width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;"> &nbsp;Cuenta</td>
                                            <td Font-Size="XX-Small" style="width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Fecha Ingreso</td>
                                            <td Font-Size="XX-Small" style="width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;Referencia</td>
                                            <td Font-Size="XX-Small" style="width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;Referencia  Banco</td>
                                            <td Font-Size="XX-Small" style="width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;Descripcion</td>
                                            <td Font-Size="XX-Small" style="width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;&nbsp;Monto</td>
                                           <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;&nbsp;No Poliza</td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="overflow:auto;height:300px;width:99%;vertical-align:top;border-style:outset;border-color:Silver; position:static" >
                                    <asp:GridView ID="Grid_Cuentas_Identificadas" runat="server" AllowPaging="False"  ShowHeader="false"
                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="99%" EmptyDataText="No se encontraron coincidencias">
                                        <Columns>
                                            <asp:BoundField DataField="NUMERO_CUENTA" ShowHeader="false">
                                                <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA_INGRESO"  HeaderStyle-Font-Size="X-Small"  ShowHeader="false" DataFormatString="{0:dd/MMM/yyyy}">
                                                <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="15%" />
                                            </asp:BoundField>                                                            
                                            <asp:BoundField DataField="REFERENCIA_CLIENTE"  HeaderStyle-Font-Size="X-Small" ShowHeader="false" HeaderText="Solicitud Pago">
                                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="center"  Width="20%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="REFERENCIA_BANCO" ShowHeader="false" HeaderStyle-Font-Size="X-Small">
                                                <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="center"  Width="15%" />
                                            </asp:BoundField>                                                            
                                            <asp:BoundField DataField="DESCRIPCION"  HeaderStyle-Font-Size="X-Small"  ShowHeader="false">
                                                <HeaderStyle HorizontalAlign="left" Width="15%" />
                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="15%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MONTO"  HeaderStyle-Font-Size="X-Small" ShowHeader="false" DataFormatString="{0:C}">
                                                <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left" Width="15%" />
                                            </asp:BoundField>
                                    	    <asp:TemplateField  Visible="True">
                                            <ItemTemplate>
                                                <asp:LinkButton Font-Size="X-Small"  ID="Btn_Seleccionar_Poliza" runat="server" Text= '<%# Eval("NO_POLIZA") %>'                                             
                                                OnClick="Btn_Poliza_Click" CommandArgument='<%# Eval("MES_ANO") %>' CssClass='<%# Eval("TIPO_POLIZA_ID") %>' ForeColor="Blue"  />
                                            </ItemTemplate >
                                            <HeaderStyle HorizontalAlign="Center"  Font-Size="XX-Small"/>
                                            <ItemStyle Font-Size="X-Small"  HorizontalAlign="Center"  Width="10%"/>
                                        </asp:TemplateField> 
                                        </Columns>
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                        <FooterStyle CssClass="GridPager" />
                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                        <PagerStyle CssClass="GridPager" />
                                        <RowStyle CssClass="GridItem" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                    </asp:GridView>
                                </div>
                        </td>
                   </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

