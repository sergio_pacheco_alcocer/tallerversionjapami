﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Con_Parametros.aspx.cs" Inherits="paginas_Contabilidad_Frm_Con_Parametros" Title="Par&aacute;metros Impuestos" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <%--<asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
     <ProgressTemplate>
       <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
       <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
     </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <cc1:ToolkitScriptManager ID="ScriptManager_Parametros_Contabilidad" runat="server"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <div id="Div_Parametros_Contabilidad">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Par&aacute;metros Impuestos</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>               
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" 
                                TabIndex="1" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                onclick="Btn_Nuevo_Click" autopostback="True" Visible="False"/>
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" 
                                TabIndex="2" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                                onclick="Btn_Modificar_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%">
                        </td> 
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente" style="display:none;">
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="30%">*Usuario Autoriza Inv.Publicas</td>
                        <td width="30%">
                            <asp:TextBox ID="Txt_Autoriza_Inv_Publicas" runat="server" ReadOnly="False" MaxLength="6"  ToolTip="ingresa el número de empleado"></asp:TextBox><cc1:FilteredTextBoxExtender ID="Flt_Inv_Publicas"
                                    TargetControlID="Txt_Autoriza_Inv_Publicas" FilterType="Numbers" runat="server">
                                </cc1:FilteredTextBoxExtender>
                        </td>
                         <td width="25%">Usuario Autoriza Inv.Publicas</td>
                        <td width="35%">
                            <asp:TextBox ID="Txt_Autoriza_Inv_Publicas_2" runat="server" ReadOnly="False" MaxLength="6"  ToolTip="ingresa el número de empleado"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Flt_Inv_Publicas_2" runat="server" TargetControlID="Txt_Autoriza_Inv_Publicas" FilterType="Numbers">
                        </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Lbl_Iva" runat="server" Text="IVA%"></asp:Label>
                        </td>
                        <td>                            
                        </td>
                        <td >Iva Inversiones%</td>
                        <td width="30%">
                            <asp:TextBox ID="Txt_Iva_Inverciones" runat="server" ReadOnly="False" MaxLength="35"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTxt_Iva_Inverciones" runat="server"  TargetControlID="Txt_Iva_Inverciones"
                                FilterType="Custom, Numbers" ValidChars="." />
                            <asp:CustomValidator ID="Cv_Txt_Iva_Inverciones" runat="server"  Display="None"
                                 EnableClientScript="true" ErrorMessage="Iva Inverciones [0-100]"
                                 Enabled="true"
                                 ClientValidationFunction="TextBox_Txt_Iva_Inverciones"
                                 HighlightCssClass="highlight" 
                                 ControlToValidate="Txt_Iva_Inverciones"/>
                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_Iva_Inverciones" runat="server" TargetControlID="Cv_Txt_Iva_Inverciones" PopupPosition="BottomLeft"/>    
                            <script type="text/javascript" >
                                function TextBox_Txt_Iva_Inverciones(sender, args) {
                                    var Porcentaje_ISR = document.getElementById("<%=Txt_Iva_Inverciones.ClientID%>").value;
                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                        document.getElementById("<%=Txt_Iva_Inverciones.ClientID%>").value = "";
                                        args.IsValid = false;
                                    }
                                }
                            </script>
                        </td>
                    </tr>
                    </Table>
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                       <td style="width:99%">
                            <asp:Panel ID="Pnl_Impuestos_Inversiones" runat="server" GroupingText="Impuestos Inversiones" Width="99%" Visible="false">
                                <table width="99%">
                                    <tr>
                                    <td style="width:17%">*CAP%</td>
                                        <td style="width:10%">
                                            <asp:TextBox ID="Txt_Cap" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35"  ></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTxt_Cap" runat="server"  TargetControlID="Txt_Cap"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_Cap" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="Cap [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_Cap"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_Cap"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_Cap" runat="server" TargetControlID="Cv_Txt_Cap" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_Cap(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_Cap.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_Cap.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                          </td>   
                                        <td style="width:58%">
                                            <asp:DropDownList ID="Cmb_CTA_CAP" runat="server" Width="99%"></asp:DropDownList>
                                        </td>  
                                        <td style="width:10%">
                                            <asp:TextBox ID="Txt_Busqueda_CAP" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td style="width:5%">
                                            <asp:ImageButton ID="Btn_Busqueda_Cap" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_Cap_Click"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >*ICIC%</td>
                                        <td >
                                            <asp:TextBox ID="Txt_ICIC" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTxt_ICIC" runat="server"  TargetControlID="Txt_ICIC"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_ICIC" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="ICIC [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_ICIC"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_ICIC"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_ICIC" runat="server" TargetControlID="Cv_Txt_ICIC" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_ICIC(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_ICIC.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_ICIC.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="Cmb_CTA_ICIC" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda_ICIC" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="Btn_Busqueda_ICIC" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_ICIC_Click"/>
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td>*RAPCE%</td>
                                        <td  >
                                            <asp:TextBox ID="Txt_RAPCE" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTxt_RAPCE" runat="server"  TargetControlID="Txt_RAPCE"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_RAPCE" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="RAPCE [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_RAPCE"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_RAPCE"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_RAPCE" runat="server" TargetControlID="Cv_Txt_RAPCE" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_RAPCE(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_RAPCE.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_RAPCE.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                         <td >
                                            <asp:DropDownList ID="Cmb_CTA_RAPCE" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda_RAPCE" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="Btn_Busqueda_RAPCE" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_RAPCE_Click"/>
                                        </td>
                                   </tr>
                                    <tr>
                                        <td>*DIVO%</td>
                                        <td>
                                            <asp:TextBox ID="Txt_DIVO" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTxt_DIVO" runat="server"  TargetControlID="Txt_DIVO"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_DIVO" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="DIVO [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_DIVO"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_DIVO"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_DIVO" runat="server" TargetControlID="Cv_Txt_DIVO" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_DIVO(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_DIVO.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_DIVO.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td width="30%">
                                            <asp:DropDownList ID="Cmb_CTA_DIVO" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda_DIVO" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="Btn_Busqueda_DIVO" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_DIVO_Click"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>*SEFUPU%</td>
                                        <td >
                                            <asp:TextBox ID="Txt_SEFUPU" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTxt_SEFUPU" runat="server"  TargetControlID="Txt_SEFUPU"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_SEFUPU" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="SEFUPU [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_SEFUPU"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_SEFUPU"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_SEFUPU" runat="server" TargetControlID="Cv_Txt_SEFUPU" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_SEFUPU(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_SEFUPU.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_SEFUPU.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td >
                                            <asp:DropDownList ID="Cmb_CTA_SEFUPU" runat="server" Width="99%">
                                         
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda_SEFUPU" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="Btn_SEFUPU" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_SEFUPU_Click"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >*OBS%</td>
                                        <td >
                                            <asp:TextBox ID="Txt_OBS" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTxt_OBS" runat="server"  TargetControlID="Txt_OBS"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_OBS" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="OBS [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_OBS"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_OBS"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_OBS" runat="server" TargetControlID="Cv_Txt_OBS" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_OBS(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_OBS.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_OBS.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="Cmb_CTA_OBS" runat="server" Width="99%"></asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda_OBS" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="Btn_Busqueda_OBS" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_OBS_Click"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>*EST Y PROY%</td>
                                        <td >
                                            <asp:TextBox ID="Txt_EST_PROY" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35"></asp:TextBox>
                                         <cc1:FilteredTextBoxExtender ID="FTxt_EST_PROY" runat="server"  TargetControlID="Txt_EST_PROY"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_EST_PROY" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="EST y PROY [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_EST_PROY"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_EST_PROY"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_EST_PROY" runat="server" TargetControlID="Cv_Txt_EST_PROY" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_EST_PROY(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_EST_PROY.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_EST_PROY.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="Cmb_CTA_EST_Y_PROY" runat="server" Width="99%"></asp:DropDownList>
                                        </td>  
                                        <td>
                                            <asp:TextBox ID="Txt_busqueda_PROY" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="Btn_Busqueda_PROY" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_PROY_Click"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >*LAB CONTROL C%</td>
                                        <td>
                                            <asp:TextBox ID="Txt_Lab_Control_C" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTxt_Lab_Control_C" runat="server"  TargetControlID="Txt_Lab_Control_C"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_Lab_Control_C" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="Lab Control C [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_Lab_Control_C"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_Lab_Control_C"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_Lab_Control_C" runat="server" TargetControlID="Cv_Txt_Lab_Control_C" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_Lab_Control_C(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_Lab_Control_C.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_Lab_Control_C.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td >
                                            <asp:DropDownList ID="Cmb_CTA_LAB_CONTROL_C" runat="server" Width="99%"></asp:DropDownList>
                                        </td>  
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda_LAB" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="Btn_Busqueda_LAB" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_LAB_Click"/>
                                        </td>    
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                       <td>
                            <asp:Panel ID="Pnl_Imospuestos_Generales" runat="server" GroupingText="Impuestos " Width="100%">
                                <table width="100%">
                                     <tr>
                                        <td style="width:17%;">IVA %</td>
                                        <td style="width:10%;">
                                            <asp:TextBox ID="Txt_IVA" runat="server" Width="80%" MaxLength="35"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTxt_IVA" runat="server"  TargetControlID="Txt_IVA"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_IVA" runat="server"  Display="None"
                                                    EnableClientScript="true" ErrorMessage="IVA [0-100]"
                                                    Enabled="true"
                                                    ClientValidationFunction="TextBox_Txt_IVA"
                                                    HighlightCssClass="highlight" 
                                                    ControlToValidate="Txt_IVA"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_IVA" runat="server" TargetControlID="Cv_Txt_IVA" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_IVA(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_IVA.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_IVA.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>                                        
                                        </td>
                                     </tr>
                                     <tr>
                                        <td style=" width:17%">ISR Honorarios%</td>
                                        <td style=" width:10%">
                                            <asp:TextBox ID="Txt_ISR" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35" ></asp:TextBox>
                                                 <cc1:FilteredTextBoxExtender ID="FTxt_ISR" runat="server"  TargetControlID="Txt_ISR"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_ISR" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="ISR [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_ISR"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_ISR"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_ISR" runat="server" TargetControlID="Cv_Txt_ISR" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_ISR(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_ISR.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_ISR.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td style=" width:58%">
                                            <asp:DropDownList ID="Cmb_CTA_ISR" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                        <td style=" width:10%">
                                            <asp:TextBox ID="Txt_Busqueda_ISR" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td style=" width:5%">
                                            <asp:ImageButton ID="Btn_Busqueda_ISR" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_ISR_Click"/>
                                        </td>   
                                   </tr>
                                     <tr>
                                        <td style=" width:17%">ISR Arren%</td>
                                        <td style=" width:10%">
                                            <asp:TextBox ID="Txt_ISR_Arren" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35" ></asp:TextBox>
                                                 <cc1:FilteredTextBoxExtender ID="FTxt_ISR_Arren" runat="server"  TargetControlID="Txt_ISR_Arren"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_ISR_Arren" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="ISR [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_ISR_Arren"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_ISR_Arren"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_ISR_Arren" runat="server" TargetControlID="Cv_Txt_ISR_Arren" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_ISR_Arren(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_ISR_Arren.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_ISR_Arren.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td style=" width:58%">
                                            <asp:DropDownList ID="Cmb_CTA_ISR_Arren" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                        <td style=" width:10%">
                                            <asp:TextBox ID="Txt_Busqueda_ISR_Arren" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td style=" width:5%">
                                            <asp:ImageButton ID="Btn_Busqueda_ISR_Arren" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_ISR_Arren_Click"/>
                                        </td>   
                                   </tr>
                                     <tr>
                                        <td >Retencion de Iva%</td>
                                        <td >
                                            <asp:TextBox ID="Txt_Retencion_Iva" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35" ></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTxt_Retencion_Iva" runat="server"  TargetControlID="Txt_Retencion_Iva"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_Retencion_Iva" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage=" Retencion Iva [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_Retencion_Iva"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_Retencion_Iva"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_Retencion_Iva" runat="server" TargetControlID="Cv_Txt_Retencion_Iva" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_Retencion_Iva(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_Retencion_Iva.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_Retencion_Iva.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="Cmb_CTA_Retencion_Iva" runat="server" Width="99%"></asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="Btn_Buscar" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_Click"/>
                                        </td>
                                   </tr>
                                     <tr>
                                         <td>Cedular Honorarios%</td>
                                        <td>
                                            <asp:TextBox ID="Txt_Cedular" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTxt_Cedular" runat="server"  TargetControlID="Txt_Cedular"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_Cedular" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="Cedular [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_Cedular"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_Cedular"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_Cedular" runat="server" TargetControlID="Cv_Txt_Cedular" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_Cedular(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_Cedular.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_Cedular.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="Cmb_CTA_Cedular" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda_Cedular" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="Btn_Busqueda_Cedular" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_Cedular_Click"/>
                                        </td>
                                  </tr>
                                  <tr>
                                         <td>Cedular Arren%</td>
                                            <td>
                                                <asp:TextBox ID="Txt_Cedular_Arren" runat="server" ReadOnly="False" 
                                                    Width="80%" MaxLength="35"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTxt_Cedular_Arren" runat="server"  TargetControlID="Txt_Cedular_Arren"
                                                    FilterType="Custom, Numbers" ValidChars="." />
                                                <asp:CustomValidator ID="Cv_Txt_Cedular_Arren" runat="server"  Display="None"
                                                     EnableClientScript="true" ErrorMessage="Cedular [0-100]"
                                                     Enabled="true"
                                                     ClientValidationFunction="TextBox_Txt_Cedular_Arren"
                                                     HighlightCssClass="highlight" 
                                                     ControlToValidate="Txt_Cedular_Arren"/>
                                                <cc1:ValidatorCalloutExtender ID="Vce_Txt_Cedular_Arren" runat="server" TargetControlID="Cv_Txt_Cedular_Arren" PopupPosition="BottomLeft"/>    
                                                <script type="text/javascript" >
                                                    function TextBox_Txt_Cedular_Arren(sender, args) {
                                                        var Porcentaje_ISR = document.getElementById("<%=Txt_Cedular_Arren.ClientID%>").value;
                                                        if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                            document.getElementById("<%=Txt_Cedular_Arren.ClientID%>").value = "";
                                                            args.IsValid = false;
                                                        }
                                                    }
                                                </script>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="Cmb_CTA_Cedular_Arren" runat="server" Width="99%"></asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_Busqueda_Cedular_Arren" runat="server" ReadOnly="False" 
                                                    Width="125px" MaxLength="35" ></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="Btn_Busqueda_Cedular_Arren" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                    TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_Cedular_Arren_Click"/>
                                            </td>
                                      </tr>
                                   <tr>
                                        <td> Honorarios Asimilables %</td>
                                        <td>
                                            <asp:TextBox ID="Txt_Honorarios_Asimilables" runat="server" ReadOnly="False" 
                                                Width="80%" MaxLength="35"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTxt_Honorarios_Asimilables" runat="server"  TargetControlID="Txt_Honorarios_Asimilables"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:CustomValidator ID="Cv_Txt_Honorarios_Asimilables" runat="server"  Display="None"
                                                 EnableClientScript="true" ErrorMessage="Cedular [0-100]"
                                                 Enabled="true"
                                                 ClientValidationFunction="TextBox_Txt_Honorarios_Asimilables"
                                                 HighlightCssClass="highlight" 
                                                 ControlToValidate="Txt_Honorarios_Asimilables"/>
                                            <cc1:ValidatorCalloutExtender ID="Vce_Txt_Honorarios_Asimilables" runat="server" TargetControlID="Cv_Txt_Honorarios_Asimilables" PopupPosition="BottomLeft"/>    
                                            <script type="text/javascript" >
                                                function TextBox_Txt_Honorarios_Asimilables(sender, args) {
                                                    var Porcentaje_ISR = document.getElementById("<%=Txt_Honorarios_Asimilables.ClientID%>").value;
                                                    if ((Porcentaje_ISR < 0) || (Porcentaje_ISR > 100)) {
                                                        document.getElementById("<%=Txt_Honorarios_Asimilables.ClientID%>").value = "";
                                                        args.IsValid = false;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="Cmb_CTA_Honorarios_Asimilables" runat="server" Width="99%"></asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda_Honorarios_Asimilables" runat="server" ReadOnly="False" 
                                                Width="125px" MaxLength="35" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="Btn_Busqueda_Honorarios_Asimilables" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"  OnClick="Btn_Buscar_Honorarios_Asimilables_Click"/>
                                        </td>
                                  </tr>
                                </table>
                             </asp:Panel>
                       </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Pnl_Datos_Ingresos" runat="server" GroupingText="Datos Ingresos" Width="100%" Visible="false">
                                <table width="100%">
                                    <tr>
                                        <td style="width:23%; text-align:left;">Fuente Ingresos Municipal</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList id="Cmb_Fte_Municipal" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox id="Txt_Buscar_Fte_Municipal" runat="server" Width="20%" MaxLength="100"
                                                AutoPostBack="true" OnTextChanged ="Txt_Buscar_Fte_Municipal_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TBWE_Fte_Municipal" runat="server" WatermarkCssClass="watermarked"
                                                 WatermarkText="Busq. Fuente" TargetControlID="Txt_Buscar_Fte_Municipal">
                                            </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Buscar_Fte_Municipal" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                             ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px"
                                             style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Programa Ingresos Municipal</td>
                                        <td style="text-align:left;">
                                            <asp:DropDownList id="Cmb_Programa_Municipal" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox id="Txt_Buscar_Programa_Municipal" runat="server" Width="20%" MaxLength="100"
                                                 AutoPostBack="true" OnTextChanged ="Txt_Buscar_Programa_Municipal_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TBWE_Programa_Municipal" runat="server" WatermarkCssClass="watermarked"
                                                 WatermarkText="Busq. Programa" TargetControlID="Txt_Buscar_Programa_Municipal">
                                            </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Programa_Municipal" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                             ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px"
                                             style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Fuente Ingresos Ramo33</td>
                                        <td style="text-align:left;">
                                            <asp:DropDownList id="Cmb_Fte_Ramo33" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox id="Txt_Buscar_Fte_Ramo33" runat="server" Width="20%" MaxLength="100"
                                                AutoPostBack="true" OnTextChanged ="Txt_Buscar_Fte_Ramo33_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TBWE_Fte_Ramo33" runat="server" WatermarkCssClass="watermarked"
                                                 WatermarkText="Busq. Fuente" TargetControlID="Txt_Buscar_Fte_Ramo33">
                                            </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Buscar_Fte_Ramo33" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                             ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px"
                                             style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Programa Ingresos Ramo33</td>
                                        <td style="text-align:left;">
                                            <asp:DropDownList id="Cmb_Programa_Ramo33" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox id="Txt_Buscar_Programa_Ramo33" runat="server" Width="20%" MaxLength="100"
                                                AutoPostBack="true" OnTextChanged ="Txt_Buscar_Programa_Ramo33_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TBWE_Programa_Ramo33" runat="server" WatermarkCssClass="watermarked"
                                                 WatermarkText="Busq. Programa" TargetControlID="Txt_Buscar_Programa_Ramo33">
                                            </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Buscar_Programa_Ramo33" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                             ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px"
                                             style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Cuenta Devengado Ingresos</td>
                                        <td style="text-align:left;">
                                            <asp:DropDownList id="Cmb_Devengado_Ing" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox id="Txt_Devengado_Ing" runat="server" Width="20%" MaxLength="100"
                                                AutoPostBack="true" OnTextChanged ="Txt_Devengado_Ing_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TBWE_Devengado_Ing" runat="server" WatermarkCssClass="watermarked"
                                                 WatermarkText="Busq. Cuenta" TargetControlID="Txt_Devengado_Ing">
                                            </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Buscar_Devengado_Ing" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                             ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px"
                                             style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                         <td>
                            <asp:Panel ID="Pnl_Cuneta_Obras_Proceso" runat="server" GroupingText="Datos Obras" Width="100%" Visible="false">
                                <table width="100%">
                                    <tr>
                                        <td style="width:23%; text-align:left;">Cuenta Obras En Proceso</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList id="Cmb_Obras_Proceso" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox id="Txt_Obras_Proceso" runat="server" Width="20%" MaxLength="100"
                                                AutoPostBack="true" OnTextChanged ="Txt_Obras_Proceso_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Obras_Proceso" runat="server" WatermarkCssClass="watermarked"
                                                 WatermarkText="Busq. Cuenta" TargetControlID="Txt_Obras_Proceso">
                                            </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Buscar_Cuenta_Obra" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                             ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px"
                                             style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                         <td>
                            <asp:Panel ID="Pnl_Cuenta_Activos" runat="server" GroupingText="Cuenta Activos" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td style="width:23%; text-align:left;">Tipo P&oacute;liza Baja</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Tipo_Poliza_Baja" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:23%; text-align:left;">C. Baja Activo</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList id="Cmb_Cuenta_Baja_Activo" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox id="Txt_Cuenta_Baja_Activo" runat="server" Width="20%" MaxLength="100"
                                                AutoPostBack="true" OnTextChanged ="Txt_Cuenta_Baja_Activo_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Cuenta_Baja_Activo" runat="server" WatermarkCssClass="watermarked"
                                                 WatermarkText="Busq. Cuenta" TargetControlID="Txt_Cuenta_Baja_Activo">
                                            </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Buscar_Cuenta_Baja_Activo" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                             ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px"
                                             style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Pnl_Comisiones" runat="server" GroupingText="Comisiones Bancarias" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td style="width:23%; text-align:left;">C. Comisi&oacute;n Bancaria</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Cuenta_Comision_Bancaria" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox ID="Txt_Cuenta_Comision_Bancaria" runat="server" Width="20%" 
                                                MaxLength="100" AutoPostBack="true" 
                                                ontextchanged="Txt_Cuenta_Comision_Bancaria_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Cuenta_Comision_Bancaria" runat="server" WatermarkCssClass="watermarked"
                                                 WatermarkText="Busq. Cuenta" TargetControlID="Txt_Cuenta_Comision_Bancaria">
                                            </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Buscar_Cuenta_Comision_Bancaria" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                             ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px"
                                             style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Pnl_CUentas_Gastos_Por_Comprobar" runat="server" GroupingText="Cuentas Fondo Revolvente" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td style="width:23%; text-align:left;">Tipo Fondo Revolvente</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Tipo_Revolvente" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:23%; text-align:left;">Tipo Caja Chica</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Tipo_Caja_Chica" runat="server">
                                            </asp:DropDownList>
                                            <asp:Label ID="Lbl_Monto_Caja_Chica" runat="server" Text="  Monto Maximo Caja Chica    :"></asp:Label>
                                            <asp:TextBox ID="Txt_Monto_caja_Chica"
                                                runat="server"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="Filtered_Txt_Monto_caja_Chica" runat="server"  TargetControlID="Txt_Monto_caja_Chica"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:23%; text-align:left;">C. Bancos(Otros)</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Cuenta_Bancos_Otros" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox ID="Txt_Cuenta_Bancos_Otros" runat="server" Width="20%" 
                                                MaxLength="100" AutoPostBack="true"  ontextchanged="Txt_Cuenta_Bancos_Otros_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Cuenta_Bancos_Otros" runat="server"  WatermarkCssClass="watermarked"
                                                WatermarkText="Busq. Cuenta" TargetControlID="Txt_Cuenta_Bancos_Otros">
                                                </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Buscar_Cuenta_Bancos_Otros" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                              ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px" style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:23%; text-align:left;">C. por Pagar</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Cuenta_por_Pagar" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox ID="Txt_Cuenta_por_Pagar" runat="server" Width="20%" 
                                                MaxLength="100" AutoPostBack="true"  ontextchanged="Txt_Cuenta_por_Pagar_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Cuenta_por_Pagar" runat="server"  WatermarkCssClass="watermarked"
                                                WatermarkText="Busq. Cuenta" TargetControlID="Txt_Cuenta_por_Pagar">
                                                </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Buscar_Cuenta_por_Pagar" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                              ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px" style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:23%; text-align:left;">Deudor Caja Chica</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Deudor_Caja_Chica" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox ID="Txt_Deudor_Caja_Chica" runat="server" Width="20%" 
                                                MaxLength="100" AutoPostBack="true"  ontextchanged="Txt_Deudor_Caja_Chica_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Deudor_Caja_Chica" runat="server"  WatermarkCssClass="watermarked"
                                                WatermarkText="Busq. Cuenta" TargetControlID="Txt_Deudor_Caja_Chica">
                                                </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Deudor_Caja_Chica" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                              ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px" style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Pnl_Gastos_Por_Comprobar_Parametros" runat="server" GroupingText="Gastos por Comprobar" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td style="width:23%; text-align:left;">Dias Comporbación</td>
                                        <td style="width:77%; text-align:left;">
											<asp:TextBox ID="Txt_Dias_Comprobacion" runat="server" Width="20%" 
                                                MaxLength="100" OnKeyUp="javascript:calculo();" OnKeyPress="this.value=(this.value.match(/^[0-9]*(\.[0-9]{0,2})?$/))?this.value :'';" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:23%; text-align:left;">Autoriza Gastos</td>
                                        <td style="width:77%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Usuario_Autoriza_Gastos" runat="server" Width="72%"></asp:DropDownList>
                                            <asp:TextBox ID="Txt_Usuario_Autoriza_Gastos" runat="server" Width="20%" 
                                                MaxLength="100" AutoPostBack="true"  ontextchanged="Txt_Usuario_Autoriza_Gastos_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Usuario_Autoriza_Gastos" runat="server"  WatermarkCssClass="watermarked"
                                                WatermarkText="Busq. Usuario" TargetControlID="Txt_Usuario_Autoriza_Gastos">
                                                </cc1:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="Btn_Buscar_Usuario_Autoriza_Gastos" runat="server" ToolTip="Buscar" CssClass="Img_Button" 
                                              ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Enabled ="false" Width="20px" Height ="20px" style = " cursor:pointer; "/>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr align="center">
                        <td style="width:99%">
                            <asp:GridView ID="Grid_Parametros" runat="server" AllowPaging="True"  Width="100%"
                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                PageSize="5" HeaderStyle-CssClass="tblHead" 
                                onselectedindexchanged="Grid_Parametros_SelectedIndexChanged">
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="7%" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="Parametro_Contabilidad_ID" HeaderText="Parametro ID" Visible="True" SortExpression="Parametro_Contabilidad_ID">
                                        <HeaderStyle HorizontalAlign="Left" Width="18%" />
                                        <ItemStyle HorizontalAlign="Left" Width="18%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="USUARIO1" HeaderText="USUARIO AUTORIZA" Visible="False" SortExpression="Usuario_Autoriza_Inv_Public">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle HorizontalAlign="Left" Width="20%" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="USUARIO2" HeaderText="USUARIO AUTORIZA 2" Visible="False" SortExpression="Usuario_Autoriza_Inv_Public2">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle HorizontalAlign="Left" Width="20%" />                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IVA" HeaderText="IVA%" Visible="True" SortExpression="IVA">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RETENCION_IVA" HeaderText="Retención_IVA%" Visible="True" SortExpression="RETENCION_IVA">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CEDULAR" HeaderText="Cedular%" Visible="True" SortExpression="CEDULAR">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ISR" HeaderText="ISR%" Visible="True" SortExpression="ISR">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:BoundField>
                                </Columns>
                                <SelectedRowStyle CssClass="GridSelected" />
                                <PagerStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>                            
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

