﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Reporte_Gastos_Gerencia.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Grupos_Dependencias.Negocio;
using System.Text;

public partial class paginas_Contabilidad_Frm_Rpt_Con_Gastos_Gerencia : System.Web.UI.Page
{
    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

        try
        {
            if (!IsPostBack)
            {
                Estado_Inicial();
            }
            else
            {
                Mostrar_Error("", false);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error(" Error: (Page_Load): " + ex.Message, true);
        }
    }
    #endregion

    #region (Metodos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Mostrar_Error
    ///DESCRIPCION:             Mostrar el mensaje de error
    ///PARAMETROS:              1. Mensaje: Cadena de texto con el mensaje de error a mostrar
    ///                         2. Mostrar: Booleano que indica si se va a mostrar el mensaje de error.
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              17/Abril/2012 17:39
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Mostrar_Error(String Mensaje, Boolean Mostrar)
    {
        try
        {
            Lbl_Informacion.Text = Mensaje;
            Div_Contenedor_Msj_Error.Visible = Mostrar;
        }
        catch (Exception ex)
        {
            Lbl_Informacion.Text = "Error: (Mostrar_Error)" + ex.ToString();
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    private void Estado_Inicial()
    {
        try
        {
            Limpiar_Controles();
            Llena_Combo_Anios();
            LLenar_Grupos_Gerenciales();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Limpiar_Controles()
    {
        try
        {
            Cmb_Grupo_Dependencias.Items.Clear();
            Cmb_Dependencias.Items.Clear();
            Cmb_Anios.Items.Clear();
            Cmb_Mes_Inicial.Items.Clear();
            Cmb_Mes_Final.Items.Clear();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Llena_Combo_Grupo_Dependencias()
    {
        //Declaracion de variables
        Cls_Cat_Grupos_Dependencias_Negocio Grupos_Dependencias_Negocio = new Cls_Cat_Grupos_Dependencias_Negocio(); //variable para la capa de negocios
        DataTable Dt_Grupos_Dependencias = new DataTable(); //tabla para los grupos de las dependencias

        try
        {
            //Ejecutar consulta
            Dt_Grupos_Dependencias = Grupos_Dependencias_Negocio.Consultar_Grupos_Dependencias();

            //Llenar el combo
            Cmb_Grupo_Dependencias.DataSource = Dt_Grupos_Dependencias;
            Cmb_Grupo_Dependencias.DataTextField = "";
            Cmb_Grupo_Dependencias.DataValueField = "";
            Cmb_Grupo_Dependencias.DataBind();
            Cmb_Grupo_Dependencias.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
            Cmb_Grupo_Dependencias.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Llena_Combo_Depedencias(string Grupo_Dependencia_ID)
    {
        //Declaracion de variables
        Cls_Cat_Dependencias_Negocio Dependencias_Negocio = new Cls_Cat_Dependencias_Negocio(); //variable para la capa de negocios
        DataTable Dt_Dependencias = new DataTable(); //tabla para el resultado de la consulta

        try
        {
            //Ejecutar consulta
            Dependencias_Negocio.P_Grupo_Dependencia_ID = Grupo_Dependencia_ID;
            Dt_Dependencias = Dependencias_Negocio.Consulta_Dependencias();

            //Llenar el combo
            Cmb_Dependencias.DataSource = Dt_Dependencias;
            Cmb_Dependencias.DataTextField = "Clave_Nombre";
            Cmb_Dependencias.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Dependencias.DataBind();
            Cmb_Dependencias.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
            Cmb_Dependencias.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    private void Llena_Combo_Anios()
    {
        //Declaracion de variables
        Cls_Rpt_Con_Gastos_Gerencias_Negocio Gastos_Gerencia_Negocio = new Cls_Rpt_Con_Gastos_Gerencias_Negocio(); //Variable para la capa de negocios
        DataTable Dt_Anios = new DataTable(); //tabla para el llenado de los años

        try
        {
            //Ejecutar consulta
            Dt_Anios = Gastos_Gerencia_Negocio.Consulta_Anios_Cerrados();
            
            //Llenar el combo de los años
            Cmb_Anios.DataSource = Dt_Anios;
            Cmb_Anios.DataTextField = "ANIO";
            Cmb_Anios.DataValueField = "ANIO";
            Cmb_Anios.DataBind();
            Cmb_Anios.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
            Cmb_Anios.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    private void LLenar_Grupos_Gerenciales()
    {
        Cls_Rpt_Con_Gastos_Gerencias_Negocio Rs_Gastos_Negocio = new Cls_Rpt_Con_Gastos_Gerencias_Negocio();
        DataTable Dt_Grupo_Gerencial = new DataTable();
        try
        {
            Dt_Grupo_Gerencial = Rs_Gastos_Negocio.Consulta_Grupo_Gerencial();
            //LLENA EL COMBO DEL GRUPO DE DEPENDENCIAS
            Cmb_Grupo_Dependencias.Items.Clear();
            Cmb_Grupo_Dependencias.DataSource = Dt_Grupo_Gerencial;
            Cmb_Grupo_Dependencias.DataTextField = "NOMBRE";
            Cmb_Grupo_Dependencias.DataValueField = "GRUPO_DEPENDENCIA_ID";
            Cmb_Grupo_Dependencias.DataBind();
            Cmb_Grupo_Dependencias.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
            Cmb_Grupo_Dependencias.SelectedIndex = 0;
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message, Ex);
        }
    }
    protected void Cmb_Grupo_Dependencia_OnseledIndexChanged(object Sender, EventArgs e)
    {
        Cls_Rpt_Con_Gastos_Gerencias_Negocio Rs_Gastos_Por_Gerencia = new Cls_Rpt_Con_Gastos_Gerencias_Negocio();
        DataTable Dt_Resultado = new DataTable();
        try
        {
            if (Cmb_Grupo_Dependencias.SelectedIndex > 0)
            {
                Rs_Gastos_Por_Gerencia.P_Grupo_Dependencia_ID = Cmb_Grupo_Dependencias.SelectedValue;
                Dt_Resultado = Rs_Gastos_Por_Gerencia.Consultar_Gerencias();
                if (Dt_Resultado.Rows.Count > 0)
                {
                    Cmb_Dependencias.Items.Clear();
                    Cmb_Dependencias.DataSource = Dt_Resultado;
                    Cmb_Dependencias.DataTextField = "NOMBRE";
                    Cmb_Dependencias.DataValueField = "DEPENDENCIA_ID";
                    Cmb_Dependencias.DataBind();
                    Cmb_Dependencias.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                    if (Cmb_Dependencias.Items.Count == 2)
                    {
                        Cmb_Dependencias.SelectedIndex = 1;
                    }
                    else
                    {
                        Cmb_Dependencias.SelectedIndex = 0;
                    }
                }
            }
            else
            {
                Cmb_Dependencias.Items.Clear();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message , Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Anio_OnSelectedIndexChanged
    ///DESCRIPCIÓN: cargara el combo de meses con los que se encuentren cerrados
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  22/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Anio_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta_Meses = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Consulta = new DataTable();
        String Año = "";
        String Mes;
        String Mes_Numero;
        DataTable Dt_Temp_Consulta = new DataTable();
        try
        {
            Año = Cmb_Anios.SelectedValue;
            Año = Año.Substring(2, 2);
            Rs_Consulta_Meses.P_Año = Año;
            Dt_Consulta = Rs_Consulta_Meses.Consulta_Meses_Cerrados();
            // se definen los campos del Dt_Temp_Consulta
            Dt_Temp_Consulta.Columns.Add("MES", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("MES_ANIO", typeof(System.String));
            DataRow row;
            foreach (DataRow Renglon in Dt_Consulta.Rows)
            {
                row = Dt_Temp_Consulta.NewRow();
                Mes = Renglon["Mes_Anio"].ToString().Substring(0, 2);
                Mes_Numero = Renglon["Mes_Anio"].ToString().Substring(0, 2);
                switch (Mes)
                {
                    case "01":
                        Mes = "ENERO";
                        break;
                    case "02":
                        Mes = "FEBRERO";
                        break;
                    case "03":
                        Mes = "MARZO";
                        break;
                    case "04":
                        Mes = "ABRIL";
                        break;
                    case "05":
                        Mes = "MAYO";
                        break;
                    case "06":
                        Mes = "JUNIO";
                        break;
                    case "07":
                        Mes = "JULIO";
                        break;
                    case "08":
                        Mes = "AGOSTO";
                        break;
                    case "09":
                        Mes = "SEPTIEMBRE";
                        break;
                    case "10":
                        Mes = "OCTUBRE";
                        break;
                    case "11":
                        Mes = "NOVIEMBRE";
                        break;
                    case "12":
                        Mes = "DICIEMBRE";
                        break;
                    default:
                        Mes = "";
                        break;
                }
                if (Mes != "")
                {
                    row["MES"] = Mes;
                    row["MES_ANIO"] = Mes_Numero;
                    Dt_Temp_Consulta.Rows.Add(row);
                    Dt_Temp_Consulta.AcceptChanges();
                }
            }
            Cmb_Mes_Inicial.Items.Clear();
            Cmb_Mes_Inicial.DataSource = Dt_Temp_Consulta;
            Cmb_Mes_Inicial.DataValueField = "MES_ANIO";
            Cmb_Mes_Inicial.DataTextField = "MES";
            Cmb_Mes_Inicial.DataBind();
            Cmb_Mes_Inicial.Items.Insert(0, HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"));
            Cmb_Mes_Inicial.SelectedIndex = 0;

            Cmb_Mes_Final.Items.Clear();
            Cmb_Mes_Final.DataSource = Dt_Temp_Consulta;
            Cmb_Mes_Final.DataValueField = "MES_ANIO";
            Cmb_Mes_Final.DataTextField = "MES";
            Cmb_Mes_Final.DataBind();
            Cmb_Mes_Final.Items.Insert(0, HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"));
            Cmb_Mes_Final.SelectedIndex = 0;
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llena_Combo_Meses
    ///DESCRIPCIÓN: cargara el combo de meses con los que se encuentren cerrados
    ///PARAMETROS: 
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  22/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Llena_Combo_Meses(int Anio)
    {
        //Declaracion de variables
        Cls_Rpt_Con_Gastos_Gerencias_Negocio Gastos_Gerencia_Negocio = new Cls_Rpt_Con_Gastos_Gerencias_Negocio(); //Variable para la capa de negocios
        DataTable Dt_Meses = new DataTable(); //Tabla para el llenado del combo

        try
        {
            //Ejecutar consulta
            Gastos_Gerencia_Negocio.P_Anio = Anio;
            Dt_Meses = Gastos_Gerencia_Negocio.Consulta_Meses_Anio();

            //Llenar los combos
            Cmb_Mes_Inicial.DataSource = Dt_Meses;
            Cmb_Mes_Inicial.DataTextField = "MES_LETRA";
            Cmb_Mes_Inicial.DataValueField = "MES_NUMERO";
            Cmb_Mes_Inicial.DataBind();
            Cmb_Mes_Inicial.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));

            Cmb_Mes_Final.DataSource = Dt_Meses;
            Cmb_Mes_Final.DataTextField = "MES_LETRA";
            Cmb_Mes_Final.DataValueField = "MES_NUMERO";
            Cmb_Mes_Final.DataBind();
            Cmb_Mes_Final.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llena_Combo_Meses
    ///DESCRIPCIÓN: cargara el combo de meses con los que se encuentren cerrados
    ///PARAMETROS: 
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Agosto/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Reporte_Gastos_Gerencia_Excel(string Grupo_Dependencia_ID, string Dependencia_ID, int Anio, int Mes_Inicial, int Mes_Final)
    {
        //Declaracion de variables
        String Cuenta_Padre_Temporal = "";
        String Meses = string.Empty;
        Cls_Rpt_Con_Gastos_Gerencias_Negocio Gastos_Gerencia_Negocio = new Cls_Rpt_Con_Gastos_Gerencias_Negocio(); //Variable para la capa de negocios
        DataTable Dt_Gastos_Gerencia = new DataTable(); //variable para la consulta
        DataTable Dt_Gastos_Grupo_Gerencia = new DataTable(); //variable para la consulta
        DataTable Dt_Temporal_Gerencias = new DataTable();
        DataTable Dt_Cuentas_Gerencias = new DataTable();
        DataTable Dt_Gastos_Por_Mes_Gerenciales = new DataTable(); //variable para la consulta
        string Grupo_Dependencia_ID_Actual = string.Empty; //Variable para el ID del grupo dependencia actual
        string Dependencia_ID_Actual = string.Empty; //variable para el ID de la dependencia actual
        int Cont_Elementos = 0; //variable para el contador
        String Ruta_Archivo = String.Empty; //variable para la ruta del archivo
        String Nombre_Archivo = "Reporte_Gasto_Gerencia.xls"; //variable para el nombre del archivo
        Workbook book = new Workbook(); //Variable para el libro
        WorksheetStyle style; //Variable para el estilo
        Worksheet sheet; //variable para la hoja
        WorksheetRow row; //variable para el renglon
        WorksheetCell cell; //Variable para la celda
        string Estilo_Renglon = string.Empty; //variable para el estilo del renglon
        int Cont_Meses = 0; //variable para el contador de los meses
        int Primer_Mes;
        int Ultimo_Mes;
        try
        {
            if (String.IsNullOrEmpty(Grupo_Dependencia_ID))
            {
                Dt_Gastos_Grupo_Gerencia = Gastos_Gerencia_Negocio.Consulta_Grupo_Gerencial();
                Dt_Gastos_Gerencia = Gastos_Gerencia_Negocio.Consultar_Gerencias();
            }
            else
            {
                if (String.IsNullOrEmpty(Dependencia_ID))
                {
                    Gastos_Gerencia_Negocio.P_Grupo_Dependencia_ID = Grupo_Dependencia_ID;
                    Dt_Gastos_Grupo_Gerencia = Gastos_Gerencia_Negocio.Consulta_Grupo_Gerencial();
                    Gastos_Gerencia_Negocio.P_Grupo_Dependencia_ID = Grupo_Dependencia_ID;
                    Dt_Gastos_Gerencia = Gastos_Gerencia_Negocio.Consultar_Gerencias();
                }
                else
                {
                    if (Dt_Gastos_Grupo_Gerencia.Rows.Count <= 0)
                    {
                        Dt_Gastos_Grupo_Gerencia.Columns.Add("GRUPO_DEPENDENCIA_ID", typeof(String));
                        Dt_Gastos_Grupo_Gerencia.Columns.Add("CLAVE_CONTABILIDAD", typeof(String));
                        Dt_Gastos_Grupo_Gerencia.Columns.Add("NOMBRE", typeof(String));
                        DataRow Fila = Dt_Gastos_Grupo_Gerencia.NewRow();
                        Fila["GRUPO_DEPENDENCIA_ID"] = Grupo_Dependencia_ID;
                        Gastos_Gerencia_Negocio.P_Grupo_Dependencia_ID = Grupo_Dependencia_ID;
                        Dt_Temporal_Gerencias= Gastos_Gerencia_Negocio.Consulta_Grupo_Gerencial();
                        Fila["CLAVE_CONTABILIDAD"] = Dt_Temporal_Gerencias.Rows[0]["CLAVE_CONTABILIDAD"].ToString();
                        Fila["NOMBRE"] = Dt_Temporal_Gerencias.Rows[0]["NOMBRE"].ToString();
                        Dt_Gastos_Grupo_Gerencia.Rows.Add(Fila);
                        Dt_Gastos_Grupo_Gerencia.AcceptChanges();
                    }
                    Dt_Temporal_Gerencias = null;
                    if (Dt_Gastos_Gerencia.Rows.Count <= 0)
                    {
                        Dt_Gastos_Gerencia.Columns.Add("GRUPO_DEPENDENCIA_ID", typeof(String));
                        Dt_Gastos_Gerencia.Columns.Add("CLAVE_CONTABILIDAD", typeof(String));
                        Dt_Gastos_Gerencia.Columns.Add("NOMBRE", typeof(String));
                        DataRow Filas = Dt_Gastos_Gerencia.NewRow();
                        Filas["GRUPO_DEPENDENCIA_ID"] = Grupo_Dependencia_ID;
                        Gastos_Gerencia_Negocio.P_Dependencia_ID = Dependencia_ID;
                        Dt_Temporal_Gerencias = Gastos_Gerencia_Negocio.Consultar_Gerencias();
                        Filas["NOMBRE"] = Dt_Temporal_Gerencias.Rows[0]["NOMBRE"].ToString();
                        Filas["CLAVE_CONTABILIDAD"] = Dt_Temporal_Gerencias.Rows[0]["CLAVE_CONTABILIDAD"].ToString();
                        Dt_Gastos_Gerencia.Rows.Add(Filas);
                        Dt_Gastos_Gerencia.AcceptChanges();
                    }
                }
            }
            if (Dt_Gastos_Por_Mes_Gerenciales.Rows.Count <= 0 && Dt_Gastos_Por_Mes_Gerenciales.Columns.Count <= 0)
            {
                Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("GRUPO_DEPENDENCIA",typeof(System.String));
                Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DEPENDENCIA", typeof(System.String));
                Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("CUENTA",typeof(System.String));
                Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DESCRIPCION", typeof(System.String));
                #region "agregar meses"
                    Primer_Mes = Mes_Inicial;
                    Ultimo_Mes = Mes_Final;
                    if (Primer_Mes == 1)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("ENERO", typeof(System.Decimal));
                        Meses = "ENERO <> 0 ";
                        if (Ultimo_Mes >= 2)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("FEBRERO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "FEBRERO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR FEBRERO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 3)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("MARZO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "MARZO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR  MARZO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 4)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("ABRIL", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "ABRIL <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + "  OR ABRIL <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 5)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("MAYO", typeof(System.Decimal)); 
                            if (Meses == "")
                            {
                                Meses = "MAYO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR MAYO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 6)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JUNIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JUNIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JUNIO <>0 ";
                            }
                        }
                        if (Ultimo_Mes >= 7)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JULIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JULIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JULIO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 8)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("AGOSTO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "AGOSTO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR AGOSTO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 9)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("SEPTIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "SEPTIEMBRE <> 0";
                            }
                            else
                            {
                                Meses = Meses + " OR SEPTIEMBRE <> 0";
                            }
                        }
                        if (Ultimo_Mes >= 10)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("OCTUBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "OCTUBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR OCTUBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 11)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "NOVIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR NOVIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 2)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("FEBRERO", typeof(System.Decimal));
                        Meses = "FEBRERO <> 0 ";
                        if (Ultimo_Mes >= 3)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("MARZO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "MARZO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR MARZO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 4)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("ABRIL", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "ABRIL <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR ABRIL <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 5)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("MAYO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "MAYO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR MAYO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 6)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JUNIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JUNIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JUNIO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 7)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JULIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JULIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JULIO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 8)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("AGOSTO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "AGOSTO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR AGOSTO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 9)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("SEPTIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "SEPTIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR SEPTIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 10)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("OCTUBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "OCTUBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR OCTUBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 11)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "NOVIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR NOVIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 3)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("MARZO", typeof(System.Decimal));
                        Meses = "MARZO <> 0 ";
                        if (Ultimo_Mes >= 4)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("ABRIL", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "ABRIL <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR ABRIL <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 5)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("MAYO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "MAYO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR MAYO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 6)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JUNIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JUNIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JUNIO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 7)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JULIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JULIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JULIO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 8)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("AGOSTO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "AGOSTO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR AGOSTO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 9)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("SEPTIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "SEPTIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR SEPTIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 10)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("OCTUBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "OCTUBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR OCTUBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 11)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "NOVIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR NOVIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 4)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("ABRIL", typeof(System.Decimal));
                        Meses = "ABRIL <> 0 ";
                        if (Ultimo_Mes >= 5)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("MAYO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "MAYO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR MAYO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 6)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JUNIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JUNIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JUNIO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 7)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JULIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JULIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JULIO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 8)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("AGOSTO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "AGOSTO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR AGOSTO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 9)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("SEPTIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "SEPTIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR SEPTIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 10)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("OCTUBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "OCTUBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR OCTUBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 11)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "NOVIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR NOVIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 5)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("MAYO", typeof(System.Decimal));
                        Meses = "MAYO <> 0 ";
                        if (Ultimo_Mes >= 6)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JUNIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JUNIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JUNIO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 7)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JULIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JULIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JULIO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 8)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("AGOSTO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "AGOSTO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR AGOSTO <>  0 ";
                            }
                        }
                        if (Ultimo_Mes >= 9)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("SEPTIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "SEPTIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR SEPTIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 10)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("OCTUBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "OCTUBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR OCTUBRE <>  0 ";
                            }
                        }
                        if (Ultimo_Mes >= 11)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "NOVIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR NOVIEMBRE<> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 6)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JUNIO", typeof(System.Decimal));
                        Meses = "JUNIO <> 0 ";
                        if (Ultimo_Mes >= 7)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JULIO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "JULIO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR JULIO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 8)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("AGOSTO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "AGOSTO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR AGOSTO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 9)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("SEPTIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "SEPTIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR SEPTIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 10)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("OCTUBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "OCTUBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR OCTUBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 11)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "NOVIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR NOVIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 7)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("JULIO", typeof(System.Decimal));
                        Meses = "JULIO <> 0 ";
                        if (Ultimo_Mes >= 8)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("AGOSTO", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "AGOSTO <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR AGOSTO <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 9)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("SEPTIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "SEPTIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR SEPTIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 10)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("OCTUBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "OCTUBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR OCTUBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 11)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "NOVIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR NOVIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 8)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("AGOSTO", typeof(System.Decimal));
                        Meses = "AGOSTO <> 0 ";
                        if (Ultimo_Mes >= 9)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("SEPTIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "SEPTIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR SEPTIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 10)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("OCTUBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "OCTUBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR OCTUBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 11)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "NOVIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR NOVIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 9)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("SEPTIEMBRE", typeof(System.Decimal));
                        Meses = "SEPTIEMBRE <> 0 ";
                        if (Ultimo_Mes >= 1 )
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("OCTUBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "OCTUBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR OCTUBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 11)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "NOVIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR NOVIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 10)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("OCTUBRE", typeof(System.Decimal));
                        Meses = "OCTUBRE <> 0 ";
                        if (Ultimo_Mes >= 11)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "NOVIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR NOVIEMBRE <> 0 ";
                            }
                        }
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 11)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("NOVIEMBRE", typeof(System.Decimal));
                        Meses = "NOVIEMBRE <> 0 ";
                        if (Ultimo_Mes >= 12)
                        {
                            Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                            if (Meses == "")
                            {
                                Meses = "DICIEMBRE <> 0 ";
                            }
                            else
                            {
                                Meses = Meses + " OR DICIEMBRE <> 0 ";
                            }
                        }
                    }
                    if (Primer_Mes == 12)
                    {
                        Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("DICIEMBRE", typeof(System.Decimal));
                        Meses = "DICIEMBRE <> 0 ";
                    }
                #endregion
                Dt_Gastos_Por_Mes_Gerenciales.Columns.Add("TOTAL", typeof(System.Decimal));
            }
            if (Dt_Cuentas_Gerencias.Rows.Count <= 0 && Dt_Cuentas_Gerencias.Columns.Count <= 0)
            {
                Dt_Cuentas_Gerencias.Columns.Add("GRUPO_DEPENDENCIA", typeof(System.String));
                Dt_Cuentas_Gerencias.Columns.Add("DEPENDENCIA", typeof(System.String));
                Dt_Cuentas_Gerencias.Columns.Add("CUENTA", typeof(System.String));
                Dt_Cuentas_Gerencias.Columns.Add("DESCRIPCION", typeof(System.String));
            }
            //Recorre el Dt de los grupos dependencias para optener las cuentas por dependencia y grupo dependencia
            foreach (DataRow Renglon in Dt_Gastos_Grupo_Gerencia.Rows)
            {
                foreach (DataRow Fila in Dt_Gastos_Gerencia.Rows)
                {
                    DataTable Dt_Temporal_Cuentas = new DataTable();
                    //se consultas las cuentas de la dependencia y del grupo de dependencias
                    Gastos_Gerencia_Negocio.P_Clave_Grupo_Dependencia = Renglon["CLAVE_CONTABILIDAD"].ToString().Trim();
                    Gastos_Gerencia_Negocio.P_Clave_Dependencia = Fila["CLAVE_CONTABILIDAD"].ToString().Trim();
                    Dt_Temporal_Cuentas = Gastos_Gerencia_Negocio.Consultar_Cuentas_Por_Grupo_y_Gernecia();
                    foreach (DataRow Fila_Cuentas in Dt_Temporal_Cuentas.Rows)
                    {
                        DataRow Renglon_Cuenta = Dt_Cuentas_Gerencias.NewRow();
                        Renglon_Cuenta["GRUPO_DEPENDENCIA"] = Renglon["GRUPO_DEPENDENCIA_ID"].ToString();
                        Renglon_Cuenta["DEPENDENCIA"] = Fila["NOMBRE"].ToString();
                        Renglon_Cuenta["CUENTA"] = Fila_Cuentas["CUENTA"].ToString();
                        Renglon_Cuenta["DESCRIPCION"] = Fila_Cuentas["DESCRIPCION"].ToString();
                        Dt_Cuentas_Gerencias.Rows.Add(Renglon_Cuenta);
                        Dt_Cuentas_Gerencias.AcceptChanges();
                    }
                }
            }
            foreach (DataRow Fila_Cuentas_Dependencia in Dt_Cuentas_Gerencias.Rows)
            {
                Decimal Monto_Mes = 0;
                String Cuenta_Padre = "";
                Decimal Monto_total = 0;
                String Cuentas_Montos = "";
                DataTable Dt_Padre_Temporal = new DataTable();
                DataTable Dt_Cuentas_Hijo = new DataTable();
                Cuenta_Padre = Fila_Cuentas_Dependencia["CUENTA"].ToString().Substring(0, 9);
                if (Cuenta_Padre != Cuenta_Padre_Temporal)
                {
                    DataRow FIla_Registro = Dt_Gastos_Por_Mes_Gerenciales.NewRow();
                    FIla_Registro["GRUPO_DEPENDENCIA"] = Fila_Cuentas_Dependencia["GRUPO_DEPENDENCIA"].ToString();
                    FIla_Registro["DEPENDENCIA"] = Fila_Cuentas_Dependencia["DEPENDENCIA"].ToString();
                    Cuenta_Padre = Fila_Cuentas_Dependencia["CUENTA"].ToString().Substring(0, 9);
                    Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuenta_Padre;
                    Dt_Padre_Temporal = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                    if (Dt_Padre_Temporal.Rows.Count > 0)
                    {
                        FIla_Registro["CUENTA"] = Dt_Padre_Temporal.Rows[0]["CUENTA"].ToString().Substring(0, 9) + Fila_Cuentas_Dependencia["CUENTA"].ToString().Substring(9, 6);
                        FIla_Registro["DESCRIPCION"] = Dt_Padre_Temporal.Rows[0]["DESCRIPCION"].ToString();
                        Cuentas_Montos = Dt_Padre_Temporal.Rows[0]["CUENTA"].ToString().Substring(0, 9) + Fila_Cuentas_Dependencia["CUENTA"].ToString().Substring(9, 6);
                    }
                    #region"meses_montos"
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("ENERO"))
                        {
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "01" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["ENERO"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["ENERO"] = 0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("FEBRERO"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "02" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["FEBRERO"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["FEBRERO"] = 0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("MARZO"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "03" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["MARZO"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["MARZO"] =0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("ABRIL"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "04" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["ABRIL"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["ABRIL"] =0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("MAYO"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "05" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["MAYO"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["MAYO"] = 0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("JUNIO"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "06" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["JUNIO"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["JUNIO"] =0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("JULIO"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "07" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["JULIO"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["JULIO"] = 0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("AGOSTO"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "08" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["AGOSTO"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["AGOSTO"] = 0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("SEPTIEMBRE"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "09" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["SEPTIEMBRE"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["SEPTIEMBRE"] = 0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("OCTUBRE"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "10" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["OCTUBRE"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["OCTUBRE"] = 0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("NOVIEMBRE"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "11" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["NOVIEMBRE"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["NOVIEMBRE"] = 0;
                                }
                            }
                        }
                        if (Dt_Gastos_Por_Mes_Gerenciales.Columns.Contains("DICIEMBRE"))
                        {
                            Monto_Mes = 0;
                            Gastos_Gerencia_Negocio.P_Cuenta_Padre = Cuentas_Montos;
                            Dt_Cuentas_Hijo = Gastos_Gerencia_Negocio.Consultar_Cuenta_Padre();
                            if (Dt_Cuentas_Hijo.Rows.Count > 0)
                            {
                                String Cuentas = String.Empty;
                                foreach (DataRow Cuentas_Hijo in Dt_Cuentas_Hijo.Rows)
                                {
                                    if (String.IsNullOrEmpty(Cuentas))
                                    {
                                        Cuentas = Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                    else
                                    {
                                        Cuentas = Cuentas + Cuentas_Hijo["CUENTA_CONTABLE_ID"].ToString() + ",";
                                    }
                                }
                                Cuentas = Cuentas.Substring(0, Cuentas.Length - 1);
                                //se calcula el monto del mes
                                if (Cuentas != "")
                                {
                                    Gastos_Gerencia_Negocio.P_Cuentas_Hijo = Cuentas;
                                    Gastos_Gerencia_Negocio.P_Mes_Anio = "12" + Cmb_Anios.SelectedItem.Text.Substring(2, 2);
                                    Monto_Mes = Gastos_Gerencia_Negocio.Consultar_Monto_Por_Mes();
                                    FIla_Registro["DICIEMBRE"] = Monto_Mes;
                                    Monto_total = Monto_total + Monto_Mes;
                                }
                                else
                                {
                                    FIla_Registro["DICIEMBRE"] =0;
                                }
                            }
                        }
                    #endregion
                    Cuenta_Padre_Temporal = Cuenta_Padre;
                    FIla_Registro["TOTAL"] = Monto_total;
                    Dt_Gastos_Por_Mes_Gerenciales.Rows.Add(FIla_Registro);
                    Dt_Gastos_Por_Mes_Gerenciales.AcceptChanges();
                }
            }
            Dt_Gastos_Por_Mes_Gerenciales.DefaultView.RowFilter = "("+Meses+")";
            if (Dt_Gastos_Por_Mes_Gerenciales.DefaultView.ToTable().Rows.Count>0)
            {
                Generar_Rpt_Excel(Dt_Gastos_Por_Mes_Gerenciales.DefaultView.ToTable());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Rpt_Excel
    /// DESCRIPCION :   Se encarga de generar el archivo de excel pasandole los paramentros
    ///               al documento
    /// PARAMETROS  :   Dt_Consulta_Reporte.- Es la consulta que contiene la informacion que se reportara
    /// CREO        :   Sergio Manuel Gallardo Andrade 
    /// FECHA_CREO  :   02/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Generar_Rpt_Excel(DataTable Dt_Reporte)
    {
        Cls_Rpt_Con_Gastos_Gerencias_Negocio Rs_Gastos_Dependencia = new Cls_Rpt_Con_Gastos_Gerencias_Negocio();
        String Nombre_Archivo = "";
        String Ruta_Archivo = "";
        Double Importe = 0.0;
        DateTime Dia_Ultimo = new DateTime();
        int Cantidad = 0;
        String Indice = "";
        String Tipo = "NO";
        string Titulo_Reporte_Unificado = string.Empty; //Variable que contendra el titulo del reporte
        string aux = string.Empty; //variable auxiliar para la construccion del titulo
        string Clave_CONAC = string.Empty; //variable para la clave de los archivos de la CONAC
        Decimal Total_Enero = 0;
        Decimal Total_Febrero = 0;
        Decimal Total_Marzo = 0;
        Decimal Total_Abril = 0;
        Decimal Total_Mayo = 0;
        Decimal Total_Junio = 0;
        Decimal Total_Julio = 0;
        Decimal Total_Agosto = 0;
        Decimal Total_Septiembre = 0;
        Decimal Total_Octubre = 0;
        Decimal Total_Noviembre = 0;
        Decimal Total_Diciembre = 0;
        Decimal Total_Grupo_Meses = 0;
        Decimal Total_Enero_Dependencia = 0;
        Decimal Total_Febrero_Dependencia = 0;
        Decimal Total_Marzo_Dependencia = 0;
        Decimal Total_Abril_Dependencia = 0;
        Decimal Total_Mayo_Dependencia = 0;
        Decimal Total_Junio_Dependencia = 0;
        Decimal Total_Julio_Dependencia = 0;
        Decimal Total_Agosto_Dependencia = 0;
        Decimal Total_Septiembre_Dependencia = 0;
        Decimal Total_Octubre_Dependencia = 0;
        Decimal Total_Noviembre_Dependencia = 0;
        Decimal Total_Diciembre_Dependencia = 0;
        Decimal Total_Grupo_Meses_Dependencia = 0;
        Decimal Total_Enero_Unidad_Responsable= 0;
        Decimal Total_Febrero_Unidad_Responsable = 0;
        Decimal Total_Marzo_Unidad_Responsable = 0;
        Decimal Total_Abril_Unidad_Responsable = 0;
        Decimal Total_Mayo_Unidad_Responsable = 0;
        Decimal Total_Junio_Unidad_Responsable = 0;
        Decimal Total_Julio_Unidad_Responsable = 0;
        Decimal Total_Agosto_Unidad_Responsable = 0;
        Decimal Total_Septiembre_Unidad_Responsable = 0;
        Decimal Total_Octubre_Unidad_Responsable = 0;
        Decimal Total_Noviembre_Unidad_Responsable = 0;
        Decimal Total_Diciembre_Unidad_Responsable = 0;
        Decimal Total_Grupo_Meses_Unidad_Responsable = 0;
        try
        {
            Nombre_Archivo = "Reporte_Gastos_"+String.Format("{0:ddMMMyyyy}",DateTime.Now) +".xls";
            Ruta_Archivo = @Server.MapPath("../../Reporte/" + Nombre_Archivo);
            //Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            Libro.Properties.Title = "GASTOS_POR_DEPENDENCIA";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "SIAC";
            Worksheet Hoja;
            //Creamos una hoja que tendrá el libro.
                Hoja = Libro.Worksheets.Add("DEPENDENCIAS");
            //Agregamos un renglón a la hoja de excel.
            WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //Creamos el estilo cabecera para la hoja de excel. 
            WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("Encabezado");
            WorksheetStyle Estilo_Texto = Libro.Styles.Add("Texto");
            //Creamos el estilo Totales para la hoja de excel. 
            WorksheetStyle Estilo_Totales = Libro.Styles.Add("Totales");
            //Creamos el estilo Totales_Negro para la hoja de excel. 
            WorksheetStyle Estilo_Totales_Negritas_Montos = Libro.Styles.Add("Totales_Negritas_Montos");
            //Creamos una celda
            WorksheetCell Celda = new WorksheetCell();
            #region Estilos
            Estilo_Cabecera.Font.FontName = "Arial";
            Estilo_Cabecera.Font.Size = 8;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#000000";
            Estilo_Cabecera.Interior.Color = "#F79646";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            //Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Alignment.WrapText = true; //Indica que puede haber multilinea

            Estilo_Texto.Font.FontName = "Arial";
            Estilo_Texto.Font.Size = 8;
            Estilo_Texto.Font.Bold = false;
            Estilo_Texto.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Texto.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Texto.Alignment.Rotate = 0;
            Estilo_Texto.Font.Color = "#000000";
            Estilo_Texto.Interior.Color = "White";
            Estilo_Texto.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Texto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Texto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Texto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Texto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");


            Estilo_Totales.Font.FontName = "Arial";
            Estilo_Totales.Font.Size = 8;
            Estilo_Totales.Font.Bold = false;
            Estilo_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales.Alignment.Rotate = 0;
            Estilo_Totales.Font.Color = "#000000";
            Estilo_Totales.Interior.Color = "White";
            Estilo_Totales.NumberFormat = "_0* #,##0.00;\\-* #,##0.00_0;* \"0.00\";_-@_-";
            Estilo_Totales.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");


            Estilo_Totales_Negritas_Montos.Font.FontName = "Arial";
            Estilo_Totales_Negritas_Montos.Font.Size = 8;
            Estilo_Totales_Negritas_Montos.Font.Bold = true;
            Estilo_Totales_Negritas_Montos.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales_Negritas_Montos.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales_Negritas_Montos.Alignment.Rotate = 0;
            Estilo_Totales_Negritas_Montos.Font.Color = "#000000";
            Estilo_Totales_Negritas_Montos.Interior.Color = "White";
            Estilo_Totales_Negritas_Montos.NumberFormat = "_0* #,##0.00;\\-* #,##0.00_0;* \"0.00\";_-@_-";
            Estilo_Totales_Negritas_Montos.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            #endregion
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  1 cuenta.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(400));//  4 Concepto.
                if (Dt_Reporte.Columns.Contains("ENERO"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("FEBRERO"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("MARZO"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("ABRIL"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("MAYO"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("JUNIO"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("JULIO"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("AGOSTO"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("SEPTIEMBRE"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("OCTUBRE"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("NOVIEMBRE"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                if (Dt_Reporte.Columns.Contains("DICIEMBRE"))
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                }
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  1 indice.
            
            if (Dt_Reporte.Rows.Count>0)
            {
                //se llena el encabezado principal
                aux = "JUNTA DE AGUA POTABLE DRENAJE ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO";
                aux = aux.PadRight(1024 - aux.Length);
                Titulo_Reporte_Unificado = aux;
                aux = "REPORTE DE GASTOS POR GERENCIA";
                Titulo_Reporte_Unificado += aux.PadRight(1024 - aux.Length);//  COMPARATIVO DE GASTOS DE ENERO A ABRIL 2013
                Titulo_Reporte_Unificado += "COMPARATIVO DE GASTOS DE" + Cmb_Mes_Inicial.SelectedItem.Text + " A " + Cmb_Mes_Final.SelectedItem.Text + " DEL " + Cmb_Anios.SelectedItem.Text;
               // Tipo = "SI";
                //Colocar el texto del encabezado
                Celda = Renglon.Cells.Add(Titulo_Reporte_Unificado, DataType.String, "Encabezado");
                Celda.Row.Height = 50;
                Celda.MergeAcross = Dt_Reporte.Columns.Count - 3;

                //////Colocar la celda de la clave de CONAC
                ////Renglon.Cells.Add(new WorksheetCell(Clave_CONAC, DataType.String, "Encabezado_Oculto"));

                Renglon = Hoja.Table.Rows.Add();
                foreach (DataColumn COLUMNA in Dt_Reporte.Columns)
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));

                    //Verificar el nombre de la columna
                    switch (COLUMNA.ColumnName.ToUpper().Trim())
                    {
                        case "CUENTA":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
                            break;

                        case "DESCRIPCION":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CONCEPTO", "Encabezado"));
                            break;

                        case "ENERO":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ENERO", "Encabezado"));
                            break;
                        case "FEBRERO":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FEBREO", "Encabezado"));
                            break;
                        case "MARZO":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MARZO", "Encabezado"));
                            break;
                        case "ABRIL":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ABRIL", "Encabezado"));
                            break;
                        case "MAYO":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MAYO", "Encabezado"));
                            break;
                        case "JUNIO":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("JUNIO", "Encabezado"));
                            break;
                        case "JULIO":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("JULIO", "Encabezado"));
                            break;
                        case "AGOSTO":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("AGOSTO", "Encabezado"));
                            break;
                        case "SEPTIEMBRE":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SEPTIEMBRE", "Encabezado"));
                            break;
                        case "OCTUBRE":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("OCTUBRE", "Encabezado"));
                            break;
                        case "NOVIEMBRE":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOVIEMBRE", "Encabezado"));
                            break;
                        case "DICIEMBRE":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DICIEMBRE", "Encabezado"));
                            break;
                        case "TOTAL":
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL", "Encabezado"));
                            break;
                    }
                }
                String Grupo_Dependencia="";
                String Grupo_Dependencia_Anterior="";
                foreach (DataRow Dr_Grupo_Dependencia in Dt_Reporte.Rows)
                {
                    Grupo_Dependencia = Dr_Grupo_Dependencia["GRUPO_DEPENDENCIA"].ToString();
                    if (Grupo_Dependencia != Grupo_Dependencia_Anterior)
                    {
                        DataTable Dt_Temporal_GD = new DataTable();
                        Rs_Gastos_Dependencia.P_Grupo_Dependencia_ID = Dr_Grupo_Dependencia["GRUPO_DEPENDENCIA"].ToString();
                        Dt_Temporal_GD = Rs_Gastos_Dependencia.Consulta_Grupo_Gerencial();
                        //Se recorre el datatable para obtener el total por mes de cada grupo de dependencias 
                        Total_Enero = 0;
                        Total_Febrero = 0;
                        Total_Marzo = 0;
                        Total_Abril = 0;
                        Total_Mayo = 0;
                        Total_Junio = 0;
                        Total_Julio = 0;
                        Total_Agosto = 0;
                        Total_Septiembre = 0;
                        Total_Octubre = 0;
                        Total_Noviembre = 0;
                        Total_Diciembre = 0;
                        Total_Grupo_Meses = 0;
                        foreach (DataRow Renglon_Grupo_Dependencia in Dt_Reporte.Rows)
                        {
                            if (Grupo_Dependencia == Renglon_Grupo_Dependencia["GRUPO_DEPENDENCIA"].ToString())
                            {
                                if (Dt_Reporte.Columns.Contains("ENERO"))
                                {
                                    Total_Enero = Total_Enero + Convert.ToDecimal(Renglon_Grupo_Dependencia["ENERO"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("FEBRERO"))
                                {
                                    Total_Febrero = Total_Febrero + Convert.ToDecimal(Renglon_Grupo_Dependencia["FEBRERO"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("MARZO"))
                                {
                                    Total_Marzo = Total_Marzo + Convert.ToDecimal(Renglon_Grupo_Dependencia["MARZO"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("ABRIL"))
                                {
                                    Total_Abril = Total_Abril + Convert.ToDecimal(Renglon_Grupo_Dependencia["ABRIL"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("MAYO"))
                                {
                                    Total_Mayo = Total_Mayo + Convert.ToDecimal(Renglon_Grupo_Dependencia["MAYO"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("JUNIO"))
                                {
                                    Total_Junio = Total_Junio + Convert.ToDecimal(Renglon_Grupo_Dependencia["JUNIO"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("JULIO"))
                                {
                                    Total_Julio = Total_Julio + Convert.ToDecimal(Renglon_Grupo_Dependencia["JULIO"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("AGOSTO"))
                                {
                                    Total_Agosto = Total_Agosto + Convert.ToDecimal(Renglon_Grupo_Dependencia["AGOSTO"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("SEPTIEMBRE"))
                                {
                                    Total_Septiembre = Total_Septiembre + Convert.ToDecimal(Renglon_Grupo_Dependencia["SEPTIEMBRE"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("OCTUBRE"))
                                {
                                    Total_Octubre = Total_Octubre + Convert.ToDecimal(Renglon_Grupo_Dependencia["OCTUBRE"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("NOVIEMBRE"))
                                {
                                    Total_Noviembre = Total_Noviembre + Convert.ToDecimal(Renglon_Grupo_Dependencia["NOVIEMBRE"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("DICIEMBRE"))
                                {
                                    Total_Diciembre = Total_Diciembre + Convert.ToDecimal(Renglon_Grupo_Dependencia["DICIEMBRE"].ToString());
                                }
                                if (Dt_Reporte.Columns.Contains("TOTAL"))
                                {
                                    Total_Grupo_Meses = Total_Grupo_Meses + Convert.ToDecimal(Renglon_Grupo_Dependencia["TOTAL"].ToString());
                                }
                            }
                        }
                        Renglon = Hoja.Table.Rows.Add();
                        Renglon = Hoja.Table.Rows.Add();
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("",DataType.String,"Totales"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dt_Temporal_GD.Rows[0]["NOMBRE"].ToString(), DataType.String, "Totales_Negritas_Montos"));
                        if (Dt_Reporte.Columns.Contains("ENERO"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Enero), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("FEBRERO"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Febrero), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("MARZO"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Marzo), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("ABRIL"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Abril), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("MAYO"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Mayo), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("JUNIO"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Junio), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("JULIO"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Julio), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("AGOSTO"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Agosto), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("SEPTIEMBRE"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Septiembre), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("OCTUBRE"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Octubre), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("NOVIEMBRE"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Noviembre), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("DICIEMBRE"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Diciembre), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        if (Dt_Reporte.Columns.Contains("TOTAL"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Grupo_Meses), DataType.Number, "Totales_Negritas_Montos"));
                        }
                        //segun yo aqui deben de ir los resumenes de los totales 
                        if (Cmb_Dependencias.Items.Count > 2 && Cmb_Dependencias.SelectedIndex==0)
                        {
                            Renglon = Hoja.Table.Rows.Add();
                            Renglon = Hoja.Table.Rows.Add();
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Texto"));
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ACUMULADOS POR CUENTA", DataType.String, "Texto"));
                            //Se consultan las dependencias de cada grupo de dependencias
                            DataTable Dt_Dependecias_Por_Grupo_Cuenta = new DataTable();
                            String Cuenta_Anterior = "";
                            String Cuenta_Actual = "";
                            Dt_Reporte.DefaultView.RowFilter = "GRUPO_DEPENDENCIA =" + Dr_Grupo_Dependencia["GRUPO_DEPENDENCIA"].ToString();
                            Dt_Dependecias_Por_Grupo_Cuenta = Dt_Reporte.DefaultView.ToTable();
                            Dt_Dependecias_Por_Grupo_Cuenta.DefaultView.Sort = "CUENTA ";
                            foreach (DataRow Dr_Cuenta_Grupo_Dependencias in Dt_Dependecias_Por_Grupo_Cuenta.DefaultView.ToTable().Rows)
                            {
                                Cuenta_Actual = Dr_Cuenta_Grupo_Dependencias["Descripcion"].ToString();
                                if (Cuenta_Actual != Cuenta_Anterior)
                                {
                                    Total_Enero_Unidad_Responsable = 0;
                                    Total_Febrero_Unidad_Responsable = 0;
                                    Total_Marzo_Unidad_Responsable = 0;
                                    Total_Abril_Unidad_Responsable = 0;
                                    Total_Mayo_Unidad_Responsable = 0;
                                    Total_Junio_Unidad_Responsable = 0;
                                    Total_Julio_Unidad_Responsable = 0;
                                    Total_Agosto_Unidad_Responsable = 0;
                                    Total_Septiembre_Unidad_Responsable = 0;
                                    Total_Octubre_Unidad_Responsable = 0;
                                    Total_Noviembre_Unidad_Responsable = 0;
                                    Total_Diciembre_Unidad_Responsable = 0;
                                    Total_Grupo_Meses_Unidad_Responsable = 0;
                                    DataTable Dt_Cuentas_Cuentas_Por_Grupo = new DataTable();
                                    Dt_Reporte.DefaultView.RowFilter = "GRUPO_DEPENDENCIA = '" + Dr_Grupo_Dependencia["GRUPO_DEPENDENCIA"].ToString() + "' AND DESCRIPCION= '" + Cuenta_Actual + "'";
                                    Dt_Cuentas_Cuentas_Por_Grupo = Dt_Reporte.DefaultView.ToTable();
                                     Renglon = Hoja.Table.Rows.Add();
                                     foreach (DataRow Dr_Cuentas_Grupo_Resumen in Dt_Cuentas_Cuentas_Por_Grupo.Rows)
                                     {
                                         //Renglon = Hoja.Table.Rows.Add();
                                         //Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr_Cuentas_Grupo_Resumen["CUENTA"].ToString(), DataType.String, "Texto"));
                                         //Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr_Cuentas_Grupo_Resumen["DESCRIPCION"].ToString(), DataType.String, "Texto"));
                                         if (Dt_Reporte.Columns.Contains("ENERO"))
                                         {
                                           //  Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["ENERO"].ToString())), DataType.Number, "Totales"));
                                             Total_Enero_Unidad_Responsable = Total_Enero_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["ENERO"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("FEBRERO"))
                                         {
                                           //  Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["FEBRERO"].ToString())), DataType.Number, "Totales"));
                                             Total_Febrero_Unidad_Responsable = Total_Febrero_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["FEBRERO"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("MARZO"))
                                         {
                                            // Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["MARZO"].ToString())), DataType.Number, "Totales"));
                                             Total_Marzo_Unidad_Responsable = Total_Marzo_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["MARZO"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("ABRIL"))
                                         {
                                            // Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["ABRIL"].ToString())), DataType.Number, "Totales"));
                                             Total_Abril_Unidad_Responsable = Total_Abril_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["ABRIL"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("MAYO"))
                                         {
                                            // Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["MAYO"].ToString())), DataType.Number, "Totales"));
                                             Total_Mayo_Unidad_Responsable = Total_Mayo_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["MAYO"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("JUNIO"))
                                         {
                                           //  Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["JUNIO"].ToString())), DataType.Number, "Totales"));
                                             Total_Junio_Unidad_Responsable = Total_Junio_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["JUNIO"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("JULIO"))
                                         {
                                            // Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["JULIO"].ToString())), DataType.Number, "Totales"));
                                             Total_Julio_Unidad_Responsable = Total_Julio_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["JULIO"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("AGOSTO"))
                                         {
                                           //  Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["AGOSTO"].ToString())), DataType.Number, "Totales"));
                                             Total_Agosto_Unidad_Responsable = Total_Agosto_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["AGOSTO"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("SEPTIEMBRE"))
                                         {
                                            // Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["SEPTIEMBRE"].ToString())), DataType.Number, "Totales"));
                                             Total_Septiembre_Unidad_Responsable = Total_Septiembre_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["SEPTIEMBRE"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("OCTUBRE"))
                                         {
                                           //  Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["OCTUBRE"].ToString())), DataType.Number, "Totales"));
                                             Total_Octubre_Unidad_Responsable = Total_Octubre_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["OCTUBRE"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("NOVIEMBRE"))
                                         {
                                            // Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["NOVIEMBRE"].ToString())), DataType.Number, "Totales"));
                                             Total_Noviembre_Unidad_Responsable = Total_Noviembre_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["NOVIEMBRE"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("DICIEMBRE"))
                                         {
                                            // Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["DICIEMBRE"].ToString())), DataType.Number, "Totales"));
                                             Total_Diciembre_Unidad_Responsable = Total_Diciembre_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["DICIEMBRE"].ToString());
                                         }
                                         if (Dt_Reporte.Columns.Contains("TOTAL"))
                                         {
                                            // Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["TOTAL"].ToString())), DataType.Number, "Totales_Negritas_Montos"));
                                             Total_Grupo_Meses_Unidad_Responsable = Total_Grupo_Meses_Unidad_Responsable + Convert.ToDecimal(Dr_Cuentas_Grupo_Resumen["TOTAL"].ToString());
                                         }
                                     }
                                    Cuenta_Anterior = Cuenta_Actual;
                                    //Se agregan los totales por dependencia
                                    //Renglon = Hoja.Table.Rows.Add();
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr_Cuenta_Grupo_Dependencias["Cuenta"].ToString().Substring(0, 11), DataType.String, "Texto"));
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Cuenta_Actual, DataType.String, "Texto"));
                                    if (Dt_Reporte.Columns.Contains("ENERO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Enero_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("FEBRERO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Febrero_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("MARZO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Marzo_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("ABRIL"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Abril_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("MAYO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Mayo_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("JUNIO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Junio_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("JULIO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Julio_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("AGOSTO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Agosto_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("SEPTIEMBRE"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Septiembre_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("OCTUBRE"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Octubre_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("NOVIEMBRE"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Noviembre_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("DICIEMBRE"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Diciembre_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                    if (Dt_Reporte.Columns.Contains("TOTAL"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Grupo_Meses_Unidad_Responsable), DataType.Number, "Totales_Negritas_Montos"));
                                    }
                                }//fin de cuenta_anterior != cuenta_actual
                            }
                        }
                        //Se consultan las dependencias de cada grupo de dependencias
                        DataTable Dt_Dependecias_Por_Grupo= new DataTable();
                        Dt_Reporte.DefaultView.RowFilter="GRUPO_DEPENDENCIA ="+Dr_Grupo_Dependencia["GRUPO_DEPENDENCIA"].ToString();
                        Dt_Dependecias_Por_Grupo = Dt_Reporte.DefaultView.ToTable();
                        String Dependencia_Anterior = "";
                        String Dependencia = "";
                        foreach (DataRow DR_Dependencia in Dt_Dependecias_Por_Grupo.Rows)
                        {
                            Dependencia = DR_Dependencia["DEPENDENCIA"].ToString();
                            if (Dependencia != Dependencia_Anterior)
                            {
                                Total_Enero_Dependencia = 0;
                                Total_Febrero_Dependencia = 0;
                                Total_Marzo_Dependencia = 0;
                                Total_Abril_Dependencia = 0;
                                Total_Mayo_Dependencia = 0;
                                Total_Junio_Dependencia = 0;
                                Total_Julio_Dependencia = 0;
                                Total_Agosto_Dependencia = 0;
                                Total_Septiembre_Dependencia = 0;
                                Total_Octubre_Dependencia = 0;
                                Total_Noviembre_Dependencia = 0;
                                Total_Diciembre_Dependencia = 0;
                                Total_Grupo_Meses_Dependencia = 0;
                                Renglon = Hoja.Table.Rows.Add();
                                Renglon = Hoja.Table.Rows.Add();
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Totales"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dependencia, DataType.String, "Totales_Negritas_Montos"));
                                DataTable Dt_Cuentas_Dependecias_Por_Grupo = new DataTable();
                                Dt_Reporte.DefaultView.RowFilter = "GRUPO_DEPENDENCIA = '" + Dr_Grupo_Dependencia["GRUPO_DEPENDENCIA"].ToString() + "' AND DEPENDENCIA= '" + Dependencia+"'";
                                Dt_Cuentas_Dependecias_Por_Grupo = Dt_Reporte.DefaultView.ToTable();
                                Renglon = Hoja.Table.Rows.Add();
                                foreach (DataRow Dr_Cuentas_Dep_Grupo in Dt_Cuentas_Dependecias_Por_Grupo.Rows)
                                {
                                    Renglon = Hoja.Table.Rows.Add();
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr_Cuentas_Dep_Grupo["CUENTA"].ToString(), DataType.String, "Texto"));
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr_Cuentas_Dep_Grupo["DESCRIPCION"].ToString(), DataType.String, "Texto"));
                                    if (Dt_Reporte.Columns.Contains("ENERO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["ENERO"].ToString())), DataType.Number, "Totales"));
                                        Total_Enero_Dependencia = Total_Enero_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["ENERO"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("FEBRERO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["FEBRERO"].ToString())), DataType.Number, "Totales"));
                                        Total_Febrero_Dependencia = Total_Febrero_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["FEBRERO"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("MARZO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["MARZO"].ToString())), DataType.Number, "Totales"));
                                        Total_Marzo_Dependencia = Total_Marzo_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["MARZO"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("ABRIL"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["ABRIL"].ToString())), DataType.Number, "Totales"));
                                        Total_Abril_Dependencia = Total_Abril_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["ABRIL"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("MAYO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["MAYO"].ToString())), DataType.Number, "Totales"));
                                        Total_Mayo_Dependencia = Total_Mayo_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["MAYO"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("JUNIO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["JUNIO"].ToString())), DataType.Number, "Totales"));
                                        Total_Junio_Dependencia = Total_Junio_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["JUNIO"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("JULIO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["JULIO"].ToString())), DataType.Number, "Totales"));
                                        Total_Julio_Dependencia = Total_Julio_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["JULIO"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("AGOSTO"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["AGOSTO"].ToString())), DataType.Number, "Totales"));
                                        Total_Agosto_Dependencia = Total_Agosto_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["AGOSTO"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("SEPTIEMBRE"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["SEPTIEMBRE"].ToString())), DataType.Number, "Totales"));
                                        Total_Septiembre_Dependencia = Total_Septiembre_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["SEPTIEMBRE"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("OCTUBRE"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["OCTUBRE"].ToString())), DataType.Number, "Totales"));
                                        Total_Octubre_Dependencia = Total_Octubre_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["OCTUBRE"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("NOVIEMBRE"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["NOVIEMBRE"].ToString())), DataType.Number, "Totales"));
                                        Total_Noviembre_Dependencia = Total_Noviembre_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["NOVIEMBRE"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("DICIEMBRE"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["DICIEMBRE"].ToString())), DataType.Number, "Totales"));
                                        Total_Diciembre_Dependencia = Total_Diciembre_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["DICIEMBRE"].ToString());
                                    }
                                    if (Dt_Reporte.Columns.Contains("TOTAL"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["TOTAL"].ToString())), DataType.Number, "Totales_Negritas_Montos"));
                                        Total_Grupo_Meses_Dependencia = Total_Grupo_Meses_Dependencia + Convert.ToDecimal(Dr_Cuentas_Dep_Grupo["TOTAL"].ToString());
                                    }
                                }// fin de  foreach (DataRow Dr_Cuentas_Dep_Grupo in Dt_Cuentas_Dependecias_Por_Grupo.Rows)
                                Dependencia_Anterior = Dependencia;
                                //Se agregan los totales por dependencia
                                Renglon = Hoja.Table.Rows.Add();
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Totales"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Totales_Negritas_Montos"));
                                if (Dt_Reporte.Columns.Contains("ENERO"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Enero_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("FEBRERO"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Febrero_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("MARZO"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Marzo_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("ABRIL"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Abril_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("MAYO"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Mayo_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("JUNIO"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Junio_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("JULIO"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Julio_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("AGOSTO"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Agosto_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("SEPTIEMBRE"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Septiembre_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("OCTUBRE"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Octubre_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("NOVIEMBRE"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Noviembre_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("DICIEMBRE"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Diciembre_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                if (Dt_Reporte.Columns.Contains("TOTAL"))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Total_Grupo_Meses_Dependencia), DataType.Number, "Totales_Negritas_Montos"));
                                }
                            } //fin de if (Dependencia != Dependencia_Anterior)
                        }//fin de foreach (DataRow DR_Dependencia in Dt_Dependecias_Por_Grupo.Rows)
                        Grupo_Dependencia_Anterior = Dr_Grupo_Dependencia["GRUPO_DEPENDENCIA"].ToString();
                    }
                }
                Renglon = Hoja.Table.Rows.Add();
                Renglon = Hoja.Table.Rows.Add();
                Ruta_Archivo = HttpContext.Current.Server.MapPath("~") + "\\Exportaciones\\" + Nombre_Archivo;
                //Hoja.Protected = true;
                Libro.Save(Ruta_Archivo);
                Mostrar_Reporte(Nombre_Archivo, "Excel");
            }//fin de if (Dt_Reporte.Rows.Count>0)
        }
        catch (System.Threading.ThreadAbortException Ex)
        {
            throw new Exception(Ex.Message, Ex);
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message, Ex);
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Exportaciones/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion

    #region (Grid)

    #endregion

    #region(Eventos)
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Salir_Click) " + ex.Message, true);
        }
    }
    protected void Btn_Imprimir_Excel_Click(object sender, ImageClickEventArgs e)
    {
        //Delcaracion de variables
        string Grupo_Dependencia_ID = string.Empty; //variable para el ID del grupo de la dependencia
        string Dependencia_ID = string.Empty; //variable para el ID de la dependencia
        int Anio = 0; //Variable para el año de la consulta
        int Mes_Inicial = 0; //variable para el numero del mes inicial
        int Mes_Final = 0; //variable apra el numero del mes final
        
        try
        {
            //Verificar si se ha seleccionado un año
            if (Cmb_Anios.SelectedIndex > 0)
            {
                //Colocar los valores de los combos
                Anio = Convert.ToInt32(Cmb_Anios.SelectedItem.Value);

                //verificar si estan los meses
                if (Cmb_Mes_Inicial.SelectedIndex <= Cmb_Mes_Final.SelectedIndex)
                {
                    Mes_Inicial = Convert.ToInt32(Cmb_Mes_Inicial.SelectedItem.Value);
                    Mes_Final = Convert.ToInt32(Cmb_Mes_Final.SelectedItem.Value);

                    //Verificar si estan los grupos de las dependencias y las dependencias
                    if (Cmb_Grupo_Dependencias.SelectedIndex > 0)
                    {
                        Grupo_Dependencia_ID = Cmb_Grupo_Dependencias.SelectedItem.Value;

                        if (Cmb_Dependencias.SelectedIndex > 0)
                        {
                            Dependencia_ID = Cmb_Dependencias.SelectedItem.Value;
                        }
                    }
                    //Crear el reporte de Excel
                    Reporte_Gastos_Gerencia_Excel(Grupo_Dependencia_ID, Dependencia_ID, Anio, Mes_Inicial, Mes_Final);
                }
                else
                {
                    Mostrar_Error("El mes final debe ser mayor al mes final.", true);
                }
            }
            else
            {
                Mostrar_Error("Favor de seleccionar el año a consultar.", true);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Imprimir_Excel_Click) " + ex.Message, true);
        }
    }
    #endregion

}