﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Catalogo_Compras_Giro_Proveedor.Negocio;
using JAPAMI.Catalogo_Compras_Giros.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using System.Xml.Linq;
using JAPAMI.Catalogo_SAP_Conceptos.Negocio;
using System.Collections.Generic;
using JAPAMI.Catalogo_Con_Proveedores.Negocio;
using System.Text.RegularExpressions;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;
using JAPAMI.Cheques_Bancos.Negocio;



public partial class paginas_compras_Frm_Cat_Con_Proveedores : System.Web.UI.Page
{

    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region PAGE_LOAD

    protected void Page_Load(object sender, EventArgs e)
    {

        //Txt_Password.Attributes.Add("value", Txt_Password.Text);
        if (!IsPostBack)
        {
            ViewState["SortDirection"] = "ASC";
            Configurar_Formulario("Inicio");

            //Cls_Ope_Con_Cheques_Bancos_Negocio Bancos_Negocio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
            //DataTable Dt_Bancos = Bancos_Negocio.Consultar_Bancos_Existentes();
            //Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Banco, Dt_Bancos, "NOMBRE", "NOMBRE");

        }

    }
    #endregion

    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************
    #region Metodos
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables_Avanzada
    /// DESCRIPCION : Ejecuta la busqueda de Cuenta
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Cuentas_Contables_Avanzada()
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cuentas = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Cuentas; //Variable que obtendra los datos de la consulta 
        Double bandera = 0;
        DataRow Renglon;//Es el renglon que se agregara al datatable temporal
        DataTable Dt_Temporal = new DataTable();//contendra las fuentes que esten activas aun
        Lbl_Error_Busqueda_Cuenta_Contable.Visible = false;
        Img_Error_Busqueda_Cienta_Contable.Visible = false;
        try
        {
            if (!string.IsNullOrEmpty(Txt_Busqueda_Cuenta_Contable.Text))
            {
                Rs_Consulta_Cuentas.P_Descripcion = Txt_Busqueda_Cuenta_Contable.Text.ToString().ToUpper();
                bandera = 1;
            }
            Rs_Consulta_Cuentas.P_Afectable = "SI";
            if (Txt_Busqueda_Cuenta_Contable.Text == "")
            {
                bandera = 0;
                //Lbl_Error_Busqueda_Cuenta.Text = "<br> Debes Ingresar una Descripcion <br>";
            }
            if (bandera == 1)
            {
                Dt_Cuentas = Rs_Consulta_Cuentas.Consultar_Cuenta_Contable_Busqueda();
                if (Dt_Cuentas.Rows.Count > 0)
                {
                    String tipo = Dt_Cuentas.Columns["CUENTA"].DataType.ToString();
                    Dt_Cuentas.DefaultView.RowFilter = "CUENTA > 210000000000000000 and CUENTA < 220000000000000000";
                    //Dt_Cuentas.DefaultView.RowFilter = "CUENTA > 110000000 and CUENTA < 120000000";
                    if (Dt_Cuentas.DefaultView.ToTable().Rows.Count > 0)
                    {
                        foreach (DataRow Fila in Dt_Cuentas.DefaultView.ToTable().Rows)
                        {
                            if (Dt_Temporal.Rows.Count <= 0)
                            {
                                //Agrega los campos que va a contener el DataTable
                                Dt_Temporal.Columns.Add("CUENTA_CONTABLE_ID", typeof(System.String));
                                Dt_Temporal.Columns.Add("CUENTA", typeof(System.String));
                                Dt_Temporal.Columns.Add("DESCRIPCION", typeof(System.String));
                            }
                            Renglon = Dt_Temporal.NewRow();
                            Renglon["CUENTA_CONTABLE_ID"] = Fila["CUENTA_CONTABLE_ID"].ToString();
                            Renglon["CUENTA"] = Fila["CUENTA"].ToString();
                            Renglon["DESCRIPCION"] = Fila["DESCRIPCION"].ToString();
                            Dt_Temporal.Rows.Add(Renglon); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Temporal.AcceptChanges();
                        }
                        // se llena el grid de cuentas encontradas
                        Grid_Cuentas_Contables.Columns[1].Visible = true;
                        Grid_Cuentas_Contables.DataSource = Dt_Temporal;   // Se iguala el DataTable con el Grid
                        Grid_Cuentas_Contables.DataBind();    // Se ligan los datos.
                        Grid_Cuentas_Contables.Visible = true;
                        Grid_Cuentas_Contables.Columns[1].Visible = false;
                    }

                }
                else
                {
                    Lbl_Error_Busqueda_Cuenta_Contable.Text = "<br> No se encontraron Coincidencias con la busqueda  <br>";
                    Lbl_Error_Busqueda_Cuenta_Contable.Visible = true;
                    Img_Error_Busqueda_Cienta_Contable.Visible = true;
                }

                Grid_Cuentas_Contables.SelectedIndex = -1;
                Mpe_Busqueda_Cuenta_Contable.Show();
            }
            else
            {
                if (bandera == 0) Lbl_Error_Busqueda_Cuenta_Contable.Text = "<br> Debes ingresar un filtro para poder realizar la busqueda  <br>";
                Lbl_Error_Busqueda_Cuenta_Contable.Visible = true;
                Img_Error_Busqueda_Cienta_Contable.Visible = true;
                Mpe_Busqueda_Cuenta_Contable.Show();
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Cuentas_Contables_Avanzada " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Configurar_Formulario
    ///DESCRIPCIÓN: Metodo que ayuda a configuarar el formulario 
    ///PARAMETROS:Estatus, Puede Ser Inicio, Nuevo, Modificar 
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    ///
    public void Configurar_Formulario(String Estatus)
    {
        switch (Estatus)
        {
            case "Inicio":
                //Limpiar_Componentes();
                Habilitar_Componentes(false);
                Div_Busqueda_Avanzada.Visible = false;
                Grid_Proveedores.Enabled = true;
                Btn_Busqueda_Avanzada.Enabled = true;
                //Boton Nuevo
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                //Boton Modificar
                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Inicio";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Div_Proveedores.Visible = false;

                break;
            case "Nuevo":
                Limpiar_Componentes();
                Habilitar_Componentes(true);
                Div_Busqueda_Avanzada.Visible = false;
                Grid_Proveedores.Enabled = false;
                Btn_Busqueda_Avanzada.Enabled = false;
                //Boton Nuevo
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Dar de Alta";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                //Boton Modificar
                Btn_Modificar.Visible = false;
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Div_Proveedores.Visible = false;

                break;
            case "Modificar":
                
                Habilitar_Componentes(true);
                Div_Busqueda_Avanzada.Visible = false;
                Grid_Proveedores.Enabled = false;
                Btn_Busqueda_Avanzada.Enabled = false;
                //Boton Nuevo
                Btn_Nuevo.Visible = false;
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                //Boton Modificar
                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Actualizar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Div_Proveedores.Visible = false;
                if (Chk_Cuenta_Acreedor.Checked == true)
                {
                    Txt_Cuenta_Acreedor.Enabled = true;
                }
                if (Chk_Cuenta_Contratista.Checked == true)
                {
                    Txt_Cuenta_Contratista.Enabled = true;
                }
                if (Chk_Cuenta_Deudor.Checked == true)
                {
                    Txt_Cuenta_Deudor.Enabled = true;
                }
                if (Chk_Cuenta_Judicial.Checked == true)
                {
                    Txt_Cuenta_Judicial.Enabled = true;
                }
                if (Chk_Cuenta_Nomina.Checked == true)
                {
                    Txt_Cuenta_Nomina.Enabled = true;
                }
                if (Chk_Cuenta_Predial.Checked == true)
                {
                    Txt_Cuenta_Predial.Enabled = true;
                }
                if (Chk_Cuenta_Proveedor.Checked == true)
                {
                    Txt_Cuenta_Proveedor.Enabled = true;
                }
                if (Chk_Cuenta_Anticipo.Checked == true)
                {
                    Txt_Cuenta_Anticipo.Enabled = true;
                } 
                if (Chk_Proveedor_Bancario.Checked == true)
                {
                    Cmb_Proveedor_Bancario.Enabled = true;
                }
                break;


        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Limpiar_Componentes
    ///DESCRIPCIÓN: Metodo que limpia las cajas de texto del catalogo de Proveedores
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Limpiar_Componentes()
    {
        Txt_Proveedor_ID.Text = "";
        Txt_Fecha_Registro.Text = "";
        Txt_Razon_Social.Text = "";
        Txt_Nombre_Comercial.Text = "";
        Txt_Representante_Legal.Text = "";
        Txt_Contacto.Text = "";
        Txt_RFC.Text = "";
        Txt_CURP.Text = "";
        Cmb_Tipo_Proveedor.SelectedIndex = 0;
        Cmb_Estatus.SelectedIndex = 0;
        Chk_Cuenta_Acreedor.Checked = false;
        Chk_Cuenta_Contratista.Checked = false;
        Chk_Cuenta_Deudor.Checked = false;
        Chk_Cuenta_Judicial.Checked = false;
        Chk_Cuenta_Nomina.Checked = false;
        Chk_Cuenta_Predial.Checked  = false;
        Chk_Cuenta_Proveedor.Checked = false;
        Chk_Cuenta_Anticipo.Checked = false;
        Txt_Cuenta_Acreedor.Text="";
        Txt_Cuenta_Contratista.Text="";
        Txt_Cuenta_Deudor.Text="";
        Txt_Cuenta_Judicial.Text="";
        Txt_Cuenta_Nomina.Text="";
        Txt_Cuenta_Predial.Text="";
        Txt_Cuenta_Proveedor.Text = "";
        Txt_Cuenta_Anticipo.Text = "";
        Chk_Fisica.Checked = false;
        Chk_Moral.Checked = false;
        Txt_Direccion.Text = "";
        Txt_Colonia.Text = "";
        Txt_Ciudad.Text = "";
        Txt_Estado.Text = "";
        Txt_Codigo_Postal.Text = "";
        Txt_Telefono1.Text = "";
        Txt_Telefono2.Text = "";
        Txt_Nextel.Text = "";
        Txt_Fax.Text = "";
        Cmb_Tipo_Pago.SelectedIndex = 0;
        Txt_Dias_Credito.Text = "";
        Cmb_Forma_Pago.SelectedIndex = 0;
        Txt_Correo.Text = "";
        //Txt_Password.Text = "";
        Txt_Comentarios.Text = "";
        Txt_Ultima_Actualizacion.Text = "";
        Chk_Actualizacion.Checked = false;

        //// banco
        //Txt_Banco_Proveedor.Text = "";
        //Txt_Clabe.Text = "";
        //Txt_Cuenta_Banco.Text = "";
        //Txt_Banco_ID.Text = "";


        //Limpiamos los Grid
        Grid_Conceptos_Proveedor.DataSource = new DataTable();
        Grid_Conceptos_Proveedor.DataBind();

        Grid_Partidas_Generales.DataSource = new DataTable();
        Grid_Partidas_Generales.DataBind();
        //Variables de Session
        Session["Proveedor_ID"] = null;
        Session["Dt_Conceptos_Proveedor"] = null;
        Session["Dt_Consulta_Partidas_Proveedores"] = null;
        Session["Dt_Correos"] = null;
        Grid_Correos.DataSource = new DataTable();
        Grid_Correos.DataBind();
    }
    public void Limpiar_Controles_Busqueda_Avanzada()
    {
        //Limpiamos las Cajas y variable de session del Div de Busqueda avanzada
        Txt_Busqueda_Padron_Proveedor.Text = "";
        Txt_Busqueda_Nombre_Comercial.Text = "";
        Txt_Busqueda_RFC.Text = "";
        Txt_Busqueda_Razon_Social.Text = "";
        Cmb_Estatus.SelectedIndex = 0;
          
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Habilitar_Componentes
    ///DESCRIPCIÓN: Metodo que  Habilita o Deshabilita los componentes
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Habilitar_Componentes(bool Habilitar)
    {


        Txt_Proveedor_ID.Enabled = false;
        Txt_Fecha_Registro.Enabled = false;
        Txt_Razon_Social.Enabled = Habilitar;
        Txt_Nombre_Comercial.Enabled = Habilitar;
        Txt_Representante_Legal.Enabled = Habilitar;
        Txt_Contacto.Enabled = Habilitar;
        Txt_RFC.Enabled = Habilitar;
        Txt_CURP.Enabled = Habilitar;
        Cmb_Tipo_Proveedor.Enabled = Habilitar;
        Cmb_Estatus.Enabled = Habilitar;
        Chk_Fisica.Enabled = Habilitar;
        Chk_Moral.Enabled = Habilitar;
        Txt_Direccion.Enabled = Habilitar;
        Txt_Colonia.Enabled =Habilitar;
        Txt_Ciudad.Enabled = Habilitar;
        Txt_Estado.Enabled = Habilitar;
        Txt_Codigo_Postal.Enabled = Habilitar;
        Txt_Telefono1.Enabled = Habilitar;
        Txt_Telefono2.Enabled = Habilitar;
        Txt_Nextel.Enabled = Habilitar;
        Txt_Fax.Enabled = Habilitar;
        Cmb_Tipo_Pago.Enabled= Habilitar;
        Txt_Dias_Credito.Enabled = Habilitar;
        Cmb_Forma_Pago.Enabled = Habilitar;
        Txt_Correo.Enabled = Habilitar;
        //Txt_Password.Enabled = Habilitar;
        Txt_Comentarios.Enabled = Habilitar;
        Txt_Ultima_Actualizacion.Enabled = false;
        Chk_Actualizacion.Enabled = Habilitar;
        Cmb_Conceptos.Enabled = Habilitar;
        Cmb_Partidas_Generales.Enabled = Habilitar;
        Grid_Conceptos_Proveedor.Enabled = Habilitar;
        Grid_Partidas_Generales.Enabled = Habilitar;
        Chk_Cuenta_Acreedor.Enabled = Habilitar;
        Chk_Cuenta_Contratista.Enabled = Habilitar;
        Chk_Cuenta_Deudor.Enabled = Habilitar;
        Chk_Cuenta_Judicial.Enabled = Habilitar;
        Chk_Cuenta_Nomina.Enabled = Habilitar;
        Chk_Cuenta_Predial.Enabled = Habilitar;
        Chk_Cuenta_Proveedor.Enabled = Habilitar;
        Chk_Cuenta_Anticipo.Enabled = Habilitar;
        Txt_Cuenta_Acreedor.Enabled = Habilitar;
        Txt_Cuenta_Proveedor.Enabled = Habilitar;
        Txt_Cuenta_Contratista.Enabled = Habilitar;
        Txt_Cuenta_Predial.Enabled = Habilitar;
        Txt_Cuenta_Deudor.Enabled = Habilitar;
        Txt_Cuenta_Nomina.Enabled = Habilitar;
        Txt_Cuenta_Judicial.Enabled = Habilitar;
        Txt_Cuenta_Anticipo.Enabled = Habilitar;
        Chk_Proveedor_Bancario.Enabled = Habilitar;
        Cmb_Proveedor_Bancario.Enabled = Habilitar;
        //banco
        //Txt_Banco_Proveedor.Enabled = Habilitar;
        ////Cmb_Banco.Enabled = Habilitar;
        //Txt_Clabe.Enabled = Habilitar;
        //Txt_Cuenta_Banco.Enabled = Habilitar;
        //Txt_Banco_ID.Enabled = Habilitar;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Conceptos
    ///DESCRIPCIÓN          : Llena el Combo de Conceptos con los existentes en la Base de Datos.
    ///PARAMETROS           :
    ///CREO: Susana Trigueros A.
    ///FECHA_CREO: 7/NOV/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************      
    public void Llenar_Combo_Conceptos()
    {
        try
        {
            Cls_Cat_Com_Proveedores_Negocio Negocio = new Cls_Cat_Com_Proveedores_Negocio();
            DataTable Data_Table = Negocio.Consultar_Conceptos();
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Conceptos, Data_Table);
            
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Bancos
    ///DESCRIPCIÓN          : Llena el Combo de bancos con los existentes en la Base de Datos.
    ///PARAMETROS           :
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 12/Junio/2013
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************      
    public void Llenar_Combo_Bancos()
    {
        try
        {
            Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
            //DataTable Dt_Bancos = Bancos_Negocio.Consulta_Bancos();
            DataTable Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Proveedor_Bancario, Dt_Existencia, "NOMBRE", "NOMBRE");
            Cmb_Proveedor_Bancario.Enabled = false;
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }

    }
    
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partidas
    ///DESCRIPCIÓN          : Llena el Combo de Partidas con los existentes en la Base de Datos.
    ///PARAMETROS           :
    ///CREO: Susana Trigueros A.
    ///FECHA_CREO: 7/NOV/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************      
    public void Llenar_Combo_Partidas()
    {
        Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
        Clase_Negocio.P_Concepto_ID = Cmb_Conceptos.SelectedValue;
        DataTable Dt_Partidas = Clase_Negocio.Consultar_Partidas_Especificas();
        Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Partidas_Generales,Dt_Partidas);


    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Contenido_Controles
    ///DESCRIPCIÓN          : Verifica si la informacion ingresada en las cajas de texto por el usuario es valida
    ///PARAMETROS           :
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 8/NOV/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Validar_Contenido_Controles()
    {
        String Clabe;
        String Clabe_Banco;
        Double Contador;
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Cls_Ope_Con_Cheques_Bancos_Negocio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Cat_Nom_Bancos_Negocio Rs_Bancos = new Cls_Cat_Nom_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        if (Txt_Razon_Social.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la Razon Social<br/>";
        }

        if(Txt_Nombre_Comercial.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el Nombre Comercial<br/>";
        }

        if (Txt_Representante_Legal.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el Representante Legal<br/>";
        }
        if (Txt_Contacto.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el Contacto<br/>";
        }
        //if (Txt_CURP.Text.Trim() == String.Empty)
        //{
        //    Div_Contenedor_Msj_Error.Visible = true;
        //    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el CURP<br/>";
        //}
        //if (!String.IsNullOrEmpty(Txt_CURP.Text.Trim()))
        //{
        //    if (Txt_CURP.Text.Length < 18)
        //    {
        //        Div_Contenedor_Msj_Error.Visible = true;
        //        Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducir todos los caracteres del CURP<br/>";
        //    }
        //}
        if (String.IsNullOrEmpty(Txt_RFC.Text.Trim()))
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el RFC<br/>";
        }
        //if (!String.IsNullOrEmpty(Txt_RFC.Text.Trim()))
        //{
        //    if (Txt_RFC.Text.Length < 13)
        //    {
        //        Div_Contenedor_Msj_Error.Visible = true;
        //        Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducir todos los caracteres del RFC<br/>";
        //    }
        //}
        if (Chk_Proveedor_Bancario.Checked == true )
        {
            if (Cmb_Proveedor_Bancario.SelectedIndex == 0)
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar Banco ya que tienes seleccionado que es un proveedor bancario<br/>";
            }
        }
        if (Chk_Fisica.Checked == false && Chk_Moral.Checked == false)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar si es persona Fisica o Moral<br/>";
        }

        if (Txt_Direccion.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la dirección<br/>";
        }

        if (Txt_Colonia.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la Colonia<br/>";
        }

        if (Txt_Ciudad.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la Ciudad<br/>";
        }
        if (Txt_Estado.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el Estado<br/>";
        }
        if (Cmb_Tipo_Proveedor.SelectedIndex<=0)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el tipo de proveedor<br/>";
        }
        if (Txt_Codigo_Postal.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la CP<br/>";
        }
        if (Txt_Telefono1.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el Telefono <br/>";
        }
        //Validamos el Correo del Proveedor que el correo sea correcto
        //Validar_Email();
        //if (Txt_Password.Text.Trim() == String.Empty)
        //{
        //    Div_Contenedor_Msj_Error.Visible = true;
        //    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario asignar un Password<br/>";
        //}

        //  para el banco

        //if (Txt_Banco_ID.Text.Trim() == String.Empty)
        //{
        //    Div_Contenedor_Msj_Error.Visible = true;
        //    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el id del banco del proveedor<br/>";
        //}
        //if (Txt_Cuenta_Banco.Text.Trim() == String.Empty)
        //{
        //    Div_Contenedor_Msj_Error.Visible = true;
        //    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la cuenta del banco del proveedor<br/>";
        //}
        //if (Txt_Clabe.Text.Trim() == String.Empty)
        //{
        //    Div_Contenedor_Msj_Error.Visible = true;
        //    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la clabe del banco del proveedor<br/>";
        //}
        //if (Txt_Banco_Proveedor.Text == "")
        //{
        //    Div_Contenedor_Msj_Error.Visible = true;
        //    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario Ingresar el banco del Proveedor<br/>";
        //}
        //if (!String.IsNullOrEmpty(Txt_Banco_Proveedor.Text.ToString()))
        //{
        //    if (!String.IsNullOrEmpty(Txt_Clabe.Text.Trim()))
        //    {
        //        Contador = 0;
        //        Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.P_Nombre_Banco = Txt_Banco_Proveedor.Text.ToString();
        //        Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.P_Comisiones = "true";
        //        Dt_Consulta = Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.Consultar_Bancos_Existentes();
        //        if (Dt_Consulta.Rows.Count > 0)
        //        {
        //            Clabe_Banco = Dt_Consulta.Rows[0]["BANCO_CLABE"].ToString();
        //            if (!String.IsNullOrEmpty(Clabe_Banco))
        //            {
        //                Contador = Clabe_Banco.Length;
        //                if (!Txt_Clabe.Text.Substring(0, Convert.ToInt32(Contador)).Equals(Clabe_Banco))
        //                {
        //                    Div_Contenedor_Msj_Error.Visible = true;
        //                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario que la clabe del proveedor contenga la clabe del banco anteriormente agregado<br/>";
        //                }
        //                Clabe = Txt_Clabe.Text.Trim().Substring(Convert.ToInt32(Contador), 10);
        //                if (!Clabe.Equals(Txt_Cuenta_Banco.Text.Trim()))
        //                {
        //                    Div_Contenedor_Msj_Error.Visible = true;
        //                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario que el numero de cuenta este en el número de clabe<br/>";
        //                }
        //            }
        //            else
        //            {
        //                Div_Contenedor_Msj_Error.Visible = true;
        //                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Este banco no tiene asignado la clabe favor de ingresarla en el catalogo de parametros bancarios<br/>";
        //            }
        //        }
        //        else
        //        {
        //            Div_Contenedor_Msj_Error.Visible = true;
        //            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Este banco no tiene asignado la clabe favor de ingresarla en el catalogo de parametros bancarios<br/>";
        //        }
        //    }
        //}
        //if (!String.IsNullOrEmpty(Txt_Clabe.Text.Trim()))
        //{
        //    if (Txt_Clabe.Text.Length < 18)
        //    {
        //        Div_Contenedor_Msj_Error.Visible = true;
        //        Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducir todos los caracteres de la clabe<br/>";
        //    }
        //}
        //if (!String.IsNullOrEmpty(Txt_Cuenta_Banco.Text.Trim()))
        //{
        //    if (Txt_Cuenta_Banco.Text.Length < 10)
        //    {
        //        Div_Contenedor_Msj_Error.Visible = true;
        //        Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducir todos los caracteres del Número Cuenta <br/>";
        //    }
        //}

        if( Div_Contenedor_Msj_Error.Visible == true)
            Lbl_Mensaje_Error.Visible = true;

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Contenido_Cuentas_Contables
    ///DESCRIPCIÓN          : Verifica si la informacion ingresada en las cajas de texto por el usuario es valida
    ///PARAMETROS           :
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 30/03-2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Validar_Contenido_Cuentas_Contables()
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_Existe = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        DataTable Dt_Resultado = new DataTable();
        if (Chk_Cuenta_Acreedor.Checked == true)
        {
            if (!String.IsNullOrEmpty(Txt_Cuenta_Acreedor.Text.Trim()))
            {
                Cuenta_Existe.P_Cuenta = Txt_Cuenta_Acreedor.Text.Trim();
                Dt_Resultado = Cuenta_Existe.Consulta_Existencia_Cuenta_Contable();
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario ingresar una cuenta de Acreedor que exista en el catalogo de cuentas contables <br/>";
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducion una cuenta de acreedor<br/>";
            }
        }
        if (Chk_Cuenta_Contratista.Checked == true)
        {
            if (!String.IsNullOrEmpty(Txt_Cuenta_Contratista.Text.Trim()))
            {
                Cuenta_Existe.P_Cuenta = Txt_Cuenta_Contratista.Text.Trim();
                Dt_Resultado = Cuenta_Existe.Consulta_Existencia_Cuenta_Contable();
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario ingresar una cuenta de Contratista que exista en el catalogo de cuentas contables <br/>";
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducion una cuenta de Contratistar<br/>";
            }
        }
        if (Chk_Cuenta_Deudor.Checked == true)
        {
            if (!String.IsNullOrEmpty(Txt_Cuenta_Deudor.Text.Trim()))
            {
                Cuenta_Existe.P_Cuenta = Txt_Cuenta_Deudor.Text.Trim();
                Dt_Resultado = Cuenta_Existe.Consulta_Existencia_Cuenta_Contable();
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario ingresar una cuenta de Deudor que exista en el catalogo de cuentas contables <br/>";
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducion una cuenta de Deudor<br/>";
            }
        }
        if (Chk_Cuenta_Judicial.Checked == true)
        {
            if (!String.IsNullOrEmpty(Txt_Cuenta_Judicial.Text.Trim()))
            {
                Cuenta_Existe.P_Cuenta = Txt_Cuenta_Judicial.Text.Trim();
                Dt_Resultado = Cuenta_Existe.Consulta_Existencia_Cuenta_Contable();
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario ingresar una cuenta de Judicial que exista en el catalogo de cuentas contables <br/>";
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducion una cuenta de Judicial<br/>";
            }
        }
        if (Chk_Cuenta_Nomina.Checked == true)
        {
            if (!String.IsNullOrEmpty(Txt_Cuenta_Nomina.Text.Trim()))
            {
                Cuenta_Existe.P_Cuenta = Txt_Cuenta_Nomina.Text.Trim();
                Dt_Resultado = Cuenta_Existe.Consulta_Existencia_Cuenta_Contable();
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario ingresar una cuenta de Nomina que exista en el catalogo de cuentas contables <br/>";
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducion una cuenta de Nomina<br/>";
            }
        }
        if (Chk_Cuenta_Predial.Checked == true)
        {
            if (!String.IsNullOrEmpty(Txt_Cuenta_Predial.Text.Trim()))
            {
                Cuenta_Existe.P_Cuenta = Txt_Cuenta_Predial.Text.Trim();
                Dt_Resultado = Cuenta_Existe.Consulta_Existencia_Cuenta_Contable();
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario ingresar una cuenta de Predial que exista en el catalogo de cuentas contables <br/>";
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducion una cuenta de Predial<br/>";
            }
        }
        if (Chk_Cuenta_Proveedor.Checked == true)
        {
            if (!String.IsNullOrEmpty(Txt_Cuenta_Proveedor.Text.Trim()))
            {
                Cuenta_Existe.P_Cuenta = Txt_Cuenta_Proveedor.Text.Trim();
                Dt_Resultado = Cuenta_Existe.Consulta_Existencia_Cuenta_Contable();
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario ingresar una cuenta de Proveedor que exista en el catalogo de cuentas contables <br/>";
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducion una cuenta de Proveedor<br/>";
            }
        }
        if (Chk_Cuenta_Anticipo.Checked == true)
        {
            if (!String.IsNullOrEmpty(Txt_Cuenta_Anticipo.Text.Trim()))
            {
                Cuenta_Existe.P_Cuenta = Txt_Cuenta_Anticipo.Text.Trim();
                Dt_Resultado = Cuenta_Existe.Consulta_Existencia_Cuenta_Contable();
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario ingresar una cuenta de Anticipo Deudor que exista en el catalogo de cuentas contables <br/>";
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducion una cuenta de Proveedor<br/>";
            }
        }
        if (Chk_Cuenta_Acreedor.Checked == false && Chk_Cuenta_Contratista.Checked == false && Chk_Cuenta_Deudor.Checked == false && Chk_Cuenta_Judicial.Checked == false && Chk_Cuenta_Nomina.Checked == false && Chk_Cuenta_Predial.Checked == false && Chk_Cuenta_Proveedor.Checked == false && Chk_Cuenta_Anticipo.Checked == false)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario introducion una cuenta al Proveedor minimo para poder darlo de alta<br/>";
        }

        if (Div_Contenedor_Msj_Error.Visible == true)
            Lbl_Mensaje_Error.Visible = true;
    }
    public void Agregar_Concepto_Proveedor()
    {
        DataTable Dt_Conceptos_Proveedor = new DataTable();
        if (Session["Dt_Conceptos_Proveedor"] != null)
        {
            Dt_Conceptos_Proveedor = (DataTable)Session["Dt_Conceptos_Proveedor"];
            DataRow[] Fila_Nueva;
            Fila_Nueva = Dt_Conceptos_Proveedor.Select("CONCEPTO_ID='" + Cmb_Conceptos.SelectedValue.Trim() + "'");
            if (Fila_Nueva.Length > 0)
            {

            }
            else
            {
                DataRow Fila = Dt_Conceptos_Proveedor.NewRow();
                Fila["CONCEPTO_ID"] = Cmb_Conceptos.SelectedValue.ToString();
                Fila["CONCEPTO"] = Cmb_Conceptos.SelectedItem.Text;
                Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
                Dt_Conceptos_Proveedor.Rows.Add(Fila);
                Dt_Conceptos_Proveedor.AcceptChanges();
                Grid_Conceptos_Proveedor.DataSource = Dt_Conceptos_Proveedor;
                Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
                Grid_Conceptos_Proveedor.DataBind();
                //Agregamos el Concepto al Grid de Conceptos 


            }
        }
        else
        {
            Dt_Conceptos_Proveedor.Columns.Add("CONCEPTO_ID", typeof(System.String));
            Dt_Conceptos_Proveedor.Columns.Add("CONCEPTO", typeof(System.String));
            Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
            DataRow[] Fila_Nueva;
            Fila_Nueva = Dt_Conceptos_Proveedor.Select("CONCEPTO_ID='" + Cmb_Conceptos.SelectedValue.Trim() + "'");
            if (Fila_Nueva.Length > 0)
            {

            }
            else
            {
                DataRow Fila = Dt_Conceptos_Proveedor.NewRow();
                Fila["CONCEPTO_ID"] = Cmb_Conceptos.SelectedValue.ToString();
                Fila["CONCEPTO"] = Cmb_Conceptos.SelectedItem.Text;
                Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
                Dt_Conceptos_Proveedor.Rows.Add(Fila);
                Dt_Conceptos_Proveedor.AcceptChanges();
                Grid_Conceptos_Proveedor.DataSource = Dt_Conceptos_Proveedor;
                Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
                Grid_Conceptos_Proveedor.DataBind();
            }

        }

    }//fin del metodo de Agregar_Concepto_Proveedor
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Email
    ///DESCRIPCIÓN: 
    ///PARAMETROS: Metodo que permite validar el correo ingresado por el usuario
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 07/Octubre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Validar_Email()
    {

        Regex Exp_Regular = new Regex("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$");
        Match Comparar = Exp_Regular.Match(Txt_Correo.Text);

        if (!Comparar.Success)
        {
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ El contenido del Correo Electronico es incorrecto <br />";
            Lbl_Mensaje_Error.Visible = true;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    public Cls_Cat_Con_Proveedores_Negocio Cargar_Datos_Proveedor(Cls_Cat_Con_Proveedores_Negocio Clase_Negocio)
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_ID = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Banco_ID = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Resultado = new DataTable();
        DataTable Dt_Banco = new DataTable();
        if(Txt_Proveedor_ID.Text.Trim() != String.Empty)
         Clase_Negocio.P_Proveedor_ID = Txt_Proveedor_ID.Text.Trim();
        Clase_Negocio.P_Razon_Social = Txt_Razon_Social.Text.Trim();
        Clase_Negocio.P_Nombre_Comercial = Txt_Nombre_Comercial.Text.Trim();
        Clase_Negocio.P_Representante_Legal = Txt_Representante_Legal.Text.Trim();
        Clase_Negocio.P_Contacto = Txt_Contacto.Text.Trim();
        Clase_Negocio.P_RFC = Txt_RFC.Text.Trim();
        Clase_Negocio.P_CURP = Txt_CURP.Text.Trim();
        Clase_Negocio.P_tipo = Cmb_Tipo_Proveedor.SelectedItem.Text.ToString();
        Clase_Negocio.P_Estatus = Cmb_Estatus.SelectedValue;
        if (Chk_Fisica.Checked == true)
        {
            Clase_Negocio.P_Tipo_Persona_Fiscal = "FISICA";
        }
        if (Chk_Moral.Checked == true)
        {
            Clase_Negocio.P_Tipo_Persona_Fiscal = "MORAL";
        }
        Clase_Negocio.P_Direccion = Txt_Direccion.Text.Trim();
        Clase_Negocio.P_Colonia = Txt_Colonia.Text.Trim();
        Clase_Negocio.P_Ciudad = Txt_Ciudad.Text.Trim();
        Clase_Negocio.P_Estado = Txt_Estado.Text.Trim();
        Clase_Negocio.P_CP = int.Parse( Txt_Codigo_Postal.Text.Trim());
        Clase_Negocio.P_Telefono_1 = Txt_Telefono1.Text.Trim();
        //Clase_Negocio.P_Password = Txt_Password.Text.Trim();
        if(Txt_Telefono2.Text.Trim() != String.Empty)
            Clase_Negocio.P_Telefono_2 = Txt_Telefono2.Text.Trim();
        if (Txt_Nextel.Text.Trim() != String.Empty)
            Clase_Negocio.P_Nextel = Txt_Nextel.Text.Trim();
        if (Txt_Fax.Text.Trim() != String.Empty)
            Clase_Negocio.P_Fax = Txt_Fax.Text.Trim();
        if (Cmb_Tipo_Pago.SelectedIndex != 0)
        {
            Clase_Negocio.P_Tipo_Pago = Cmb_Tipo_Pago.SelectedValue;
        }
        if (Cmb_Forma_Pago.SelectedIndex != 0)
        {
            Clase_Negocio.P_Forma_Pago = Cmb_Forma_Pago.SelectedValue;
        }
        if (Txt_Dias_Credito.Text != String.Empty)
        {
            Clase_Negocio.P_Dias_Credito = int.Parse(Txt_Dias_Credito.Text.Trim());
            
        }
        if (Chk_Proveedor_Bancario.Checked==true)
        {
            Clase_Negocio.P_Proveedor_Bancario = Cmb_Proveedor_Bancario.SelectedItem.Text.ToString();
        }
        //if (Txt_Correo.Text.Trim() != String.Empty)
        //{
        //    Clase_Negocio.P_Correo_Electronico = Txt_Correo.Text.Trim();

        //}
        //if (Txt_Password.Text.Trim() != String.Empty)
        //{
        //    Clase_Negocio.P_Password = Txt_Password.Text.Trim();
        //}
        DataTable Dt_Correos = new DataTable();
        Dt_Correos = (DataTable)Session["Dt_Correos"];
        if (Dt_Correos != null && Dt_Correos.Rows.Count > 0)
        {
            //Recorremos el Grid de los correos 
            for (int i = 0; i < Dt_Correos.Rows.Count; i++)
            {
                Clase_Negocio.P_Correo_Electronico += Dt_Correos.Rows[i][0].ToString().Trim() + ";";
            }
        }


        if (Txt_Comentarios.Text.Trim() != String.Empty)
        {
            Clase_Negocio.P_Comentarios = Txt_Comentarios.Text.Trim();
        }
        if (Chk_Cuenta_Acreedor.Checked == true)
        {
            Cuenta_ID.P_Cuenta= Txt_Cuenta_Acreedor.Text.Trim();
            Dt_Resultado= Cuenta_ID.Consulta_Existencia_Cuenta_Contable();
            if(Dt_Resultado.Rows.Count>0){
                Clase_Negocio.P_Cuenta_Acreedor_ID= Dt_Resultado.Rows[0]["CUENTA_CONTABLE_ID"].ToString().Trim();
            }
        }
        if (Chk_Cuenta_Deudor.Checked == true)
        {
            Cuenta_ID.P_Cuenta = Txt_Cuenta_Deudor.Text.Trim();
            Dt_Resultado = Cuenta_ID.Consulta_Existencia_Cuenta_Contable();
            if (Dt_Resultado.Rows.Count > 0)
            {
                Clase_Negocio.P_Cuenta_Deudor_ID = Dt_Resultado.Rows[0]["CUENTA_CONTABLE_ID"].ToString().Trim();
            }
        }
        if (Chk_Cuenta_Proveedor.Checked == true)
        {
            Cuenta_ID.P_Cuenta = Txt_Cuenta_Proveedor.Text.Trim();
            Dt_Resultado = Cuenta_ID.Consulta_Existencia_Cuenta_Contable();
            if (Dt_Resultado.Rows.Count > 0)
            {
                Clase_Negocio.P_Cuenta_Proveedor_ID = Dt_Resultado.Rows[0]["CUENTA_CONTABLE_ID"].ToString().Trim();
            }
        }
        if (Chk_Cuenta_Predial.Checked == true)
        {
            Cuenta_ID.P_Cuenta = Txt_Cuenta_Predial.Text.Trim();
            Dt_Resultado = Cuenta_ID.Consulta_Existencia_Cuenta_Contable();
            if (Dt_Resultado.Rows.Count > 0)
            {
                Clase_Negocio.P_Cuenta_Predial_ID = Dt_Resultado.Rows[0]["CUENTA_CONTABLE_ID"].ToString().Trim();
            }
        }
        if (Chk_Cuenta_Contratista.Checked == true)
        {
            Cuenta_ID.P_Cuenta = Txt_Cuenta_Contratista.Text.Trim();
            Dt_Resultado = Cuenta_ID.Consulta_Existencia_Cuenta_Contable();
            if (Dt_Resultado.Rows.Count > 0)
            {
                Clase_Negocio.P_Cuenta_Contratista_ID = Dt_Resultado.Rows[0]["CUENTA_CONTABLE_ID"].ToString().Trim();
            }
        }
        if (Chk_Cuenta_Judicial.Checked == true)
        {
            Cuenta_ID.P_Cuenta = Txt_Cuenta_Judicial.Text.Trim();
            Dt_Resultado = Cuenta_ID.Consulta_Existencia_Cuenta_Contable();
            if (Dt_Resultado.Rows.Count > 0)
            {
                Clase_Negocio.P_Cuenta_Judicial_ID = Dt_Resultado.Rows[0]["CUENTA_CONTABLE_ID"].ToString().Trim();
            }
        }
        if (Chk_Cuenta_Nomina.Checked == true)
        {
            Cuenta_ID.P_Cuenta = Txt_Cuenta_Nomina.Text.Trim();
            Dt_Resultado = Cuenta_ID.Consulta_Existencia_Cuenta_Contable();
            if (Dt_Resultado.Rows.Count > 0)
            {
                Clase_Negocio.P_Cuenta_Nomina_ID = Dt_Resultado.Rows[0]["CUENTA_CONTABLE_ID"].ToString().Trim();
            }
        }
        if (Chk_Cuenta_Anticipo.Checked == true)
        {
            Cuenta_ID.P_Cuenta = Txt_Cuenta_Anticipo.Text.Trim();
            Dt_Resultado = Cuenta_ID.Consulta_Existencia_Cuenta_Contable();
            if (Dt_Resultado.Rows.Count > 0)
            {
                Clase_Negocio.P_Cuenta_Anticipo_Deudor_ID = Dt_Resultado.Rows[0]["CUENTA_CONTABLE_ID"].ToString().Trim();
            }
        }
        // Agregamos a la clase de negocio si existe una nueva Actualizacion 
        if (Chk_Actualizacion.Checked == true)
            Clase_Negocio.P_Nueva_Actualizacion = true;
        else
            Clase_Negocio.P_Nueva_Actualizacion = false;
        //Cargamos el Dt de Conceptos y el Dt de partidas
        Clase_Negocio.P_Dt_Partidas_Proveedor = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];
        Clase_Negocio.P_Dt_Conceptos_Proveedor = (DataTable)Session["Dt_Conceptos_Proveedor"];

        ////  para los datos del banco
        //Clase_Negocio.P_Cuenta = Txt_Cuenta_Banco.Text;
        //Clase_Negocio.P_Clabe = Txt_Clabe.Text;
        //Clase_Negocio.P_Banco_Proveedor_ID = Txt_Banco_ID.Text;
        //Clase_Negocio.P_Banco_Proveedor = Txt_Banco_Proveedor.Text.ToUpper();
        //Banco_ID.P_Nombre_Banco = Cmb_Banco.SelectedValue;
        //Dt_Banco=Banco_ID.Consultar_Bancos_Like();
        //if (Dt_Banco.Rows.Count > 0)
        //{
        //    Clase_Negocio.P_Banco_ID = Dt_Banco.Rows[0]["BANCO_ID"].ToString().Trim();
        //}
        //else
        //{
        //    Clase_Negocio.P_Banco_ID = "";
        //}
        

        return Clase_Negocio;
    }
    #endregion

    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    #region Grid

    #region Grid_Proveedores


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Habilitar_Componentes
    ///DESCRIPCIÓN: Metodo que  Habilita o Deshabilita los componentes
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:    Jennyfer Ivonne Ceja Lemus 
    ///FECHA_MODIFICO: 23/NOV/2012
    ///CAUSA_MODIFICACIÓN: Se agrego una validacion para cuando el proveedor no tiene tipo especifico
    ///*******************************************************************************
    protected void Grid_Proveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        //Consultamos el Proveedor ID que selecciono
        Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
        Clase_Negocio.P_Proveedor_ID = Grid_Proveedores.SelectedDataKey["Proveedor_ID"].ToString();
        //Consultamos los datos dep Proveedor seleccionado 
        Llenar_Combo_Bancos();
        DataTable Dt_Datos_Proveedor = Clase_Negocio.Consulta_Proveedores();
        Session["Proveedor_ID"]= Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim();
        Txt_Proveedor_ID.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim();

        if (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim() != String.Empty)
        {
            try
            {
                Txt_Fecha_Registro.Text = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Fecha_Registro].ToString().Trim()));
            }
            catch
            {
                Txt_Fecha_Registro.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Fecha_Registro].ToString().Trim();
            }
        }
        Txt_Razon_Social.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Nombre].ToString().Trim();
        Txt_Nombre_Comercial.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Compañia].ToString().Trim();
        Txt_Representante_Legal.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Representante_Legal].ToString().Trim();
        Txt_Contacto.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Contacto].ToString().Trim();
        Txt_RFC.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString().Trim();
        Txt_CURP.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_CURP].ToString().Trim();
        if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Tipo].ToString().Trim()))
        {
            Cmb_Tipo_Proveedor.SelectedValue = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Tipo].ToString().Trim();
        }
        else 
        {
            Cmb_Tipo_Proveedor.SelectedIndex = 0;
        }
        
        switch (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Tipo_Fiscal].ToString().Trim())
        {
            case "FISICA":
                Chk_Fisica.Checked = true;
                Chk_Moral.Checked = false;
                break;
            case "MORAL":
                Chk_Fisica.Checked = false;
                Chk_Moral.Checked = true;
                break;
        }
        Txt_Direccion.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Direccion].ToString().Trim();
        Txt_Colonia.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Colonia].ToString().Trim();
        Txt_Ciudad.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Ciudad].ToString().Trim();
        Txt_Estado.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Estado].ToString().Trim();
        Txt_Codigo_Postal.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_CP].ToString().Trim();
        Txt_Telefono1.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Telefono_1].ToString().Trim();
        Txt_Telefono2.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Telefono_2].ToString().Trim();
        Txt_Nextel.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Nextel].ToString().Trim();
        Txt_Fax.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Fax].ToString().Trim();
        Txt_Dias_Credito.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Dias_Credito].ToString().Trim();
        if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Proveedor_Bancario].ToString()))
        {
            Chk_Proveedor_Bancario.Checked = true;
            Cmb_Proveedor_Bancario.SelectedValue = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Proveedor_Bancario].ToString();
        }
        else
        {
            Chk_Proveedor_Bancario.Checked = false;
        }
        //Txt_Correo.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString().Trim();
        //Txt_Password.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Password].ToString().Trim();

        string correo = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString().Trim();
        string[] array = correo.Split(';');
        //llenamos el grid del correo. 
        DataTable Dt_Correos = new DataTable();
        Dt_Correos.Columns.Add("correo", typeof(System.String));

        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] != String.Empty)
            {
                DataRow Fila = Dt_Correos.NewRow();
                Fila["correo"] = array[i].ToString().Trim();
                Dt_Correos.Rows.Add(Fila);
                Dt_Correos.AcceptChanges();
            }
        }
        Session["Dt_Correos"] = Dt_Correos;
        Grid_Correos.DataSource = Dt_Correos;
        Grid_Correos.DataBind();

        Txt_Comentarios.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Comentarios].ToString().Trim();
        Txt_Ultima_Actualizacion.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Fecha_Actualizacion].ToString().Trim();
        if(Txt_Ultima_Actualizacion.Text.Trim() != String.Empty)
            Session["Ultima_Actualizacion"] = Txt_Ultima_Actualizacion.Text.Trim();
        else
            Session["Ultima_Actualizacion"] = null;
        Chk_Actualizacion.Checked = false;
        //Asignamos los valores de las cuentas que tene asignadas el proveedor
        Cls_Cat_Con_Cuentas_Contables_Negocio Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        DataTable Dt_Cuenta = new DataTable();
        if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Acreedor_ID.Trim()].ToString().Trim()))
        {
            Cuentas_Proveedor.P_Cuenta_Contable_ID = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Acreedor_ID.Trim()].ToString().Trim();
            Dt_Cuenta = Cuentas_Proveedor.Consulta_Cuentas_Contables();
            if (Dt_Cuenta.Rows.Count>0)
            {
                Chk_Cuenta_Acreedor.Checked = true;
                Txt_Cuenta_Acreedor.Text = Dt_Cuenta.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
            }
        }
        if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID.Trim()].ToString().Trim()))
        {
            Cuentas_Proveedor.P_Cuenta_Contable_ID = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID.Trim()].ToString().Trim();
            Dt_Cuenta = Cuentas_Proveedor.Consulta_Cuentas_Contables();
            if (Dt_Cuenta.Rows.Count > 0)
            {
                Chk_Cuenta_Contratista.Checked = true;
                Txt_Cuenta_Contratista.Text = Dt_Cuenta.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
            }
        }
        if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID.Trim()].ToString().Trim()))
        {
            Cuentas_Proveedor.P_Cuenta_Contable_ID = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID.Trim()].ToString().Trim();
            Dt_Cuenta = Cuentas_Proveedor.Consulta_Cuentas_Contables();
            if (Dt_Cuenta.Rows.Count > 0)
            {
                Chk_Cuenta_Deudor.Checked = true;
                Txt_Cuenta_Deudor.Text = Dt_Cuenta.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
            }
        }
        if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Judicial_ID.Trim()].ToString().Trim()))
        {
            Cuentas_Proveedor.P_Cuenta_Contable_ID = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Judicial_ID.Trim()].ToString().Trim();
            Dt_Cuenta = Cuentas_Proveedor.Consulta_Cuentas_Contables();
            if (Dt_Cuenta.Rows.Count > 0)
            {
                Chk_Cuenta_Judicial.Checked = true;
                Txt_Cuenta_Judicial.Text = Dt_Cuenta.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
            }
        }
        if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Nomina_ID.Trim()].ToString().Trim()))
        {
            Cuentas_Proveedor.P_Cuenta_Contable_ID = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Nomina_ID.Trim()].ToString().Trim();
            Dt_Cuenta = Cuentas_Proveedor.Consulta_Cuentas_Contables();
            if (Dt_Cuenta.Rows.Count > 0)
            {
                Chk_Cuenta_Nomina.Checked = true;
                Txt_Cuenta_Nomina.Text = Dt_Cuenta.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
            }
        }
        if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Predial_ID.Trim()].ToString().Trim()))
        {
            Cuentas_Proveedor.P_Cuenta_Contable_ID = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Predial_ID.Trim()].ToString().Trim();
            Dt_Cuenta = Cuentas_Proveedor.Consulta_Cuentas_Contables();
            if (Dt_Cuenta.Rows.Count > 0)
            {
                Chk_Cuenta_Predial.Checked = true;
                Txt_Cuenta_Predial.Text = Dt_Cuenta.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
            }
        }
        if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID.Trim()].ToString().Trim()))
        {
            Cuentas_Proveedor.P_Cuenta_Contable_ID = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID.Trim()].ToString().Trim();
            Dt_Cuenta = Cuentas_Proveedor.Consulta_Cuentas_Contables();
            if (Dt_Cuenta.Rows.Count > 0)
            {
                Chk_Cuenta_Proveedor.Checked = true;
                Txt_Cuenta_Proveedor.Text = Dt_Cuenta.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
            }
        }
        if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Anticipo_ID.Trim()].ToString().Trim()))
        {
            Cuentas_Proveedor.P_Cuenta_Contable_ID = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Anticipo_ID.Trim()].ToString().Trim();
            Dt_Cuenta = Cuentas_Proveedor.Consulta_Cuentas_Contables();
            if (Dt_Cuenta.Rows.Count > 0)
            {
                Chk_Cuenta_Anticipo.Checked = true;
                Txt_Cuenta_Anticipo.Text = Dt_Cuenta.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
            }
        }
        //Asignamos valor del combo Estatus
        switch (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Estatus].ToString().Trim())
        {
            case "ACTIVO":
                Cmb_Estatus.SelectedValue = "ACTIVO";
            break;
            case "INACTIVO":
                Cmb_Estatus.SelectedValue = "INACTIVO";
            break;
        }
        // Asignamos valor del combo Tipo de Pago
        switch (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Tipo_Pago].ToString().Trim())
        {
            case "CREDITO":
                Cmb_Tipo_Pago.SelectedValue = "CREDITO";
                Txt_Dias_Credito.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Dias_Credito].ToString().Trim();
                break;
            case "CONTADO":
                Cmb_Tipo_Pago.SelectedValue = "CONTADO";
                Txt_Dias_Credito.Text = "";
                break;
        }
        //Asignar el valor del combo Forma de pago
        switch (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Forma_Pago].ToString().Trim())
        {
            case "TRANSFERENCIA":
                Cmb_Forma_Pago.SelectedValue = "TRANSFERENCIA";
                break;
            case "CHEQUE":
                Cmb_Forma_Pago.SelectedValue = "CHEQUE";
                break;
            case "EFECTIVO":
                Cmb_Forma_Pago.SelectedValue = "EFECTIVO";
                break;
        }
        //Consultamos los Conceptos asignado al Proveedor.
        Clase_Negocio.P_Proveedor_ID = Session["Proveedor_ID"].ToString().Trim();
        DataTable Dt_Conceptos_Proveedor = Clase_Negocio.Consultar_Detalles_Conceptos();
        if (Dt_Conceptos_Proveedor.Rows.Count != 0)
        {
            Grid_Conceptos_Proveedor.DataSource = Dt_Conceptos_Proveedor;
            Grid_Conceptos_Proveedor.DataBind();
            Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
        }
        else
        {
            Grid_Conceptos_Proveedor.EmptyDataText = "No se encontraron Conceptos de Este Proveedor";
            Grid_Conceptos_Proveedor.DataSource = new DataTable();
            Grid_Conceptos_Proveedor.DataBind();
        }
        //Consultar las Partidas asignadas al Proveedor.
        DataTable Dt_Partidas_Proveedor = Clase_Negocio.Consultar_Detalle_Partidas();
        if (Dt_Partidas_Proveedor.Rows.Count != 0)
        {
            Grid_Partidas_Generales.DataSource = Dt_Partidas_Proveedor;
            Grid_Partidas_Generales.DataBind();
            Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Partidas_Proveedor;
        }
        else
        {
            Grid_Partidas_Generales.EmptyDataText = "No se encontraron partidas de este Proveedor";
            Grid_Partidas_Generales.DataSource = new DataTable();
            Grid_Partidas_Generales.DataBind();
        }
        //Consultamos si hay Historial de Actualizaciones
        DataTable Dt_Historia_Actualizaciones = Clase_Negocio.Consultar_Actualizaciones_Proveedores();
        if (Dt_Historia_Actualizaciones.Rows.Count != 0)
        {
            Grid_Historial_Act.DataSource = Dt_Historia_Actualizaciones;
            Grid_Historial_Act.DataBind();
        }
        else
        {
            Grid_Historial_Act.EmptyDataText = "No se encontro Historial de Actualizaciones de este Proveedor";
            Grid_Historial_Act.DataSource = new DataTable();
            Grid_Historial_Act.DataBind();
        }


        // para los datos del banco

        ////if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Banco_ID].ToString().Trim()))
        ////{
        ////    Cls_Ope_Con_Cheques_Bancos_Negocio  Rp_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio(); 
        ////    Rp_Banco.P_Banco_ID=Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Banco_ID].ToString().Trim();
        ////    DataTable Dt_Banco = new DataTable();
        ////    Dt_Banco = Rp_Banco.Consultar_Folio_Actual();
        ////    if (Dt_Banco.Rows.Count > 0)
        ////    {
        ////        Cmb_Banco.SelectedIndex = Cmb_Banco.Items.IndexOf(Cmb_Banco.Items.FindByValue(Dt_Banco.Rows[0][Cat_Com_Proveedores.Campo_Nombre].ToString()));
        ////    }
        ////}
        //if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Banco_Proveedor].ToString().Trim()))
        //{
        //    Txt_Banco_Proveedor.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Banco_Proveedor].ToString().Trim();
        //}
        //if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Banco_Proveedor_ID].ToString().Trim()))
        //{
        //    Txt_Banco_ID.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Banco_Proveedor_ID].ToString().Trim();
        //}

        //if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Clabe].ToString().Trim()))
        //{
        //    Txt_Clabe.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Clabe].ToString().Trim();
        //}

        //if (!String.IsNullOrEmpty(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta].ToString().Trim()))
        //{
        //    Txt_Cuenta_Banco.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta].ToString().Trim();
        //}


        //llenamos los combos de Conceptos y Partidas
        Llenar_Combo_Conceptos();
        Llenar_Combo_Partidas();

        Div_Proveedores.Visible = false;
        Session["Dt_Proveedores"] = null;

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Llenar_Grid_Proveedores
    ///DESCRIPCIÓN: Metodo que  llena el Grid de Proveedores
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Grid_Proveedores(Cls_Cat_Com_Proveedores_Negocio Clase_Negocio)
    {
        DataTable Dt_Proveedores = Clase_Negocio.Consulta_Proveedores();
        if (Dt_Proveedores.Rows.Count != 0)
        {
            Grid_Proveedores.DataSource = Dt_Proveedores;
            Grid_Proveedores.DataBind();
            Session["Dt_Proveedores"] = Dt_Proveedores;
        }
        else
        {
            Grid_Proveedores.EmptyDataText = "No se han encontrado registros.";
            //Lbl_Mensaje_Error.Text = "+ No se encontraron datos <br />";
            Grid_Proveedores.DataSource = new DataTable();
            Grid_Proveedores.DataBind();
        }

    }
    protected void Grid_Partidas_Generales_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataRow[] Renglones;
        DataRow[] Renglon_Concepto;
        DataRow Renglon;
        //Obtenemos el Id del producto seleccionado
        GridViewRow selectedRow = Grid_Partidas_Generales.Rows[Grid_Partidas_Generales.SelectedIndex];
        String Id = Convert.ToString(selectedRow.Cells[1].Text);
        int num_fila = Grid_Partidas_Generales.SelectedIndex;
        DataTable Dt_Consulta_Giros_Proveedore = (DataTable)Session["Dt_Conceptos_Proveedor"];
        DataTable Dt_Consulta_Partidas_Proveedores = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];

        String Concepto_ID = Dt_Consulta_Partidas_Proveedores.Rows[num_fila]["CONCEPTO_ID"].ToString().Trim();

        Renglones = ((DataTable)Session["Dt_Consulta_Partidas_Proveedores"]).Select(Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID + "='" + Id + "'");
        Renglon_Concepto = ((DataTable)Session["Dt_Consulta_Partidas_Proveedores"]).Select(Cat_SAP_Partida_Generica.Campo_Concepto_ID + "='" + Concepto_ID + "'");
        if (Renglones.Length > 0)
        {
            Renglon = Renglones[0];
            DataTable Tabla = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];
            Tabla.Rows.Remove(Renglon);
            Session["Dt_Consulta_Partidas_Proveedores"] = Tabla;
            Grid_Partidas_Generales.SelectedIndex = (-1);
            Grid_Partidas_Generales.DataSource = Tabla;
            Grid_Partidas_Generales.DataBind();

        }

        if (Renglon_Concepto.Length == 1)
        {
            //Eliminamos el concepto
            DataTable Tabla = (DataTable)Session["Dt_Conceptos_Proveedor"];
            Renglon_Concepto = ((DataTable)Session["Dt_Conceptos_Proveedor"]).Select(Cat_SAP_Partida_Generica.Campo_Concepto_ID + "='" + Concepto_ID + "'");
            Renglon = Renglon_Concepto[0];
            Tabla.Rows.Remove(Renglon);
            Session["Dt_Conceptos_Proveedor"] = Tabla;
            Grid_Conceptos_Proveedor.SelectedIndex = (-1);
            Grid_Conceptos_Proveedor.DataSource = Tabla;
            Grid_Conceptos_Proveedor.DataBind();

        }
    }
    protected void Grid_Proveedores_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable Dt_Proveedores = (DataTable)Session["Dt_Proveedores"];

        if (Dt_Proveedores != null)
        {
            DataView Dv_Proveedores = new DataView(Dt_Proveedores);
            String Orden = ViewState["SortDirection"].ToString();

            if (Orden.Equals("ASC"))
            {
                Dv_Proveedores.Sort = e.SortExpression + " " + "DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Proveedores.Sort = e.SortExpression + " " + "ASC";
                ViewState["SortDirection"] = "ASC";
            }

            Grid_Proveedores.DataSource = Dv_Proveedores;
            Grid_Proveedores.DataBind();
            //Guardamos el cambio dentro de la variable de session de Dt_Requisiciones
            Session["Dt_Proveedores"] = (DataTable)Dv_Proveedores.Table;
            Dt_Proveedores = (DataTable)Session["Dt_Proveedores"];

        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Grid_Proveedores_RowDataBound
    ///DESCRIPCIÓN: Metodo que  llena el Grid de Proveedores
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 28/May/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Proveedores_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            String Cuentas = HttpUtility.HtmlDecode(e.Row.Cells[6].Text.ToString().Trim());
            Cuentas += HttpUtility.HtmlDecode(e.Row.Cells[7].Text.ToString().Trim());
            Cuentas += HttpUtility.HtmlDecode(e.Row.Cells[8].Text.ToString().Trim());
            Cuentas += HttpUtility.HtmlDecode(e.Row.Cells[9].Text.ToString().Trim());
            Cuentas += HttpUtility.HtmlDecode(e.Row.Cells[10].Text.ToString().Trim());
            Cuentas += HttpUtility.HtmlDecode(e.Row.Cells[11].Text.ToString().Trim());
            Cuentas += HttpUtility.HtmlDecode(e.Row.Cells[12].Text.ToString().Trim());
            Cuentas += HttpUtility.HtmlDecode(e.Row.Cells[13].Text.ToString().Trim());
            Cuentas += HttpUtility.HtmlDecode(e.Row.Cells[14].Text.ToString().Trim());
            Cuentas += HttpUtility.HtmlDecode(e.Row.Cells[15].Text.ToString().Trim());

            if (String.IsNullOrEmpty(Cuentas.Trim()))
            {
                ImageButton Boton = (ImageButton)e.Row.FindControl("Btn_Alerta");
                Boton.ToolTip = "Alerta: Es necesario seleccionar una cuenta para el proveedor";
                //Boton.ImageUrl = "../imagenes/gridview/circle_red.png";
                Boton.Visible = true;
            }
        }
    }

    #endregion


    #endregion

    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Cerrar_Modal_Click
    /// DESCRIPCION : Cierra la ventana de busqueda cuentas.
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 02/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Cerrar_Modal_Click(object sender, ImageClickEventArgs e)
    {
        Grid_Cuentas_Contables.DataSource = null;   // Se iguala el DataTable con el Grid
        Grid_Cuentas_Contables.DataBind();    // Se ligan los datos.
        Txt_Busqueda_Cuenta_Contable.Text = "";
        Mpe_Busqueda_Cuenta_Contable.Hide();    //Oculta el ModalPopUp
        Lbl_Error_Busqueda_Cuenta_Contable.Text = "";
        Lbl_Error_Busqueda_Cuenta_Contable.Visible = false;
        Img_Error_Busqueda_Cienta_Contable.Visible = false;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Busqueda_Cuentas_Popup_Click
    /// DESCRIPCION : Busca las cuentas contables referentes a la descripcion
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 02/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Busqueda_Cuentas_Popup_Click(object sender, EventArgs e)
    {
        try
        {
            Consulta_Cuentas_Contables_Avanzada();
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            IBtn_Imagen_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Cuenta_Click
    ///DESCRIPCIÓN: Metodo para asignar al combo la cuenta contable
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 18/mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Cuenta_Click(object sender, ImageClickEventArgs e)
    {
        String Cuenta = ((ImageButton)sender).CommandArgument;
        String Tipo_Cuenta=Cmb_Tipo_Cuenta.SelectedItem.Text;
        try
        {
            if (Cmb_Tipo_Cuenta.SelectedIndex > 0)
            {
                switch (Tipo_Cuenta)
                {
                    case "Acreedor":
                        Chk_Cuenta_Acreedor.Checked = true;
                        Txt_Cuenta_Acreedor.Text = Cuenta;
                        break;
                    case "Proveedor":
                        Chk_Cuenta_Proveedor.Checked = true;
                        Txt_Cuenta_Proveedor.Text = Cuenta;
                        break;
                    case "Contratista":
                        Chk_Cuenta_Contratista.Checked = true;
                        Txt_Cuenta_Contratista.Text = Cuenta;
                        break;
                    case "Predial":
                        Chk_Cuenta_Predial.Checked = true;
                        Txt_Cuenta_Predial.Text = Cuenta;
                        break;
                    case "Deudor":
                        Chk_Cuenta_Deudor.Checked = true;
                        Txt_Cuenta_Deudor.Text = Cuenta;
                        break;
                    case "Nomina":
                        Chk_Cuenta_Nomina.Checked = true;
                        Txt_Cuenta_Nomina.Text = Cuenta;
                        break;
                    case "Judicial":
                        Chk_Cuenta_Judicial.Checked = true;
                        Txt_Cuenta_Judicial.Text = Cuenta;
                        break;
                    case "Anticipo Deudor":
                        Chk_Cuenta_Anticipo.Checked = true;
                        Txt_Cuenta_Anticipo.Text = Cuenta;
                        break;
                }

                Cmb_Tipo_Cuenta.SelectedIndex = 0;
                Txt_Busqueda_Cuenta_Contable.Text = "";
                Grid_Cuentas_Contables.DataSource = null;
                Grid_Cuentas_Contables.DataBind();
            }
            else
            {
                Lbl_Error_Busqueda_Cuenta_Contable.Text = "<br> Seleccione un tipo de cuenta  <br>";
                Lbl_Error_Busqueda_Cuenta_Contable.Visible = true;
                Img_Error_Busqueda_Cienta_Contable.Visible = true;
                Mpe_Busqueda_Cuenta_Contable.Show();
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            IBtn_Imagen_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Menos_Click
    ///DESCRIPCIÓN: Btn_Eliminar_Partida_Click
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 22/Mar/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Eliminar_Correo_Click(object sender, ImageClickEventArgs e)
    {

        DataRow[] Renglones;

        DataRow Renglon;

        //Asignamos el valor del row
        Grid_Correos.SelectedIndex = int.Parse(((ImageButton)sender).CommandArgument);
        // GridViewRow selectedRow = Grid_Partidas_Generales.Rows[Grid_Partidas_Generales.SelectedIndex];
        String Id = Grid_Correos.SelectedDataKey["correo"].ToString().Trim();
        int num_fila = Grid_Correos.SelectedIndex - 1;
        DataTable Dt_Correo = (DataTable)Session["Dt_Correos"];
        if (num_fila == -1)
            num_fila = 0;
        Renglones = ((DataTable)Session["Dt_Correos"]).Select("correo='" + Id + "'");

        if (Renglones.Length > 0)
        {
            Renglon = Renglones[0];
            DataTable Tabla = (DataTable)Session["Dt_Correos"];
            Tabla.Rows.Remove(Renglon);
            Session["Dt_Correos"] = Tabla;
            Grid_Correos.SelectedIndex = (-1);
            Grid_Correos.DataSource = Tabla;
            Grid_Correos.DataBind();
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Ibtn_agregar_Correo_Click
    ///DESCRIPCIÓN:Evento del boton agregar correos
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 26/MAR/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Ibtn_agregar_Correo_Click(object sender, ImageClickEventArgs e)
    {
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;

        Validar_Email();
        if (Lbl_Mensaje_Error.Text == String.Empty)
        {
            //Agregamos el correo. 
            DataTable Dt_Correos = new DataTable();
            if (Session["Dt_Correos"] != null)
            {
                Dt_Correos = (DataTable)Session["Dt_Correos"];
                DataRow[] Fila_Nueva;
                Fila_Nueva = Dt_Correos.Select("correo='" + Txt_Correo.Text.Trim() + "'");
                if (Fila_Nueva.Length > 0)
                {

                }
                else
                {
                    DataRow Fila = Dt_Correos.NewRow();
                    Fila["correo"] = Txt_Correo.Text.Trim();
                    Session["Dt_Correos"] = Dt_Correos;
                    Dt_Correos.Rows.Add(Fila);
                    Dt_Correos.AcceptChanges();
                    Grid_Correos.DataSource = Dt_Correos;
                    Session["Dt_Correos"] = Dt_Correos;
                    Grid_Correos.DataBind();


                }
            }
            else
            {
                Dt_Correos.Columns.Add("correo", typeof(System.String));

                Session["Dt_Correos"] = Dt_Correos;
                DataRow[] Fila_Nueva;
                Fila_Nueva = Dt_Correos.Select("correo='" + Txt_Correo.Text.Trim() + "'");
                if (Fila_Nueva.Length > 0)
                {

                }
                else
                {
                    DataRow Fila = Dt_Correos.NewRow();
                    Fila["correo"] = Txt_Correo.Text.Trim();
                    Session["Dt_Correos"] = Dt_Correos;
                    Dt_Correos.Rows.Add(Fila);
                    Dt_Correos.AcceptChanges();
                    Grid_Correos.DataSource = Dt_Correos;
                    Session["Dt_Correos"] = Dt_Correos;
                    Grid_Correos.DataBind();
                }
            }

        }
    }
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        switch (Btn_Nuevo.ToolTip)
        {
            case "Nuevo":
                Configurar_Formulario("Nuevo");
                Limpiar_Componentes();
                Habilitar_Componentes(true);
                //Asignamos por default el Password con el valor 123456 cuando es un proveedor nuevo
                //Txt_Password.Text = "123456";
                //Txt_Password.Attributes.Add("value", Txt_Password.Text);
                //Llenamos el combo de Conceptos
                Llenar_Combo_Conceptos();
                Llenar_Combo_Bancos();
                //Deshabilitamos el combo de Partidas
                Cmb_Partidas_Generales.Enabled = false;
                Div_Proveedores.Visible = false;
                Grid_Proveedores.DataSource = new DataTable();
                Grid_Proveedores.DataBind();
                Session["Dt_Proveedores"] = null;
                //No se debe permir modificar el Password
                //Txt_Password.Enabled = false;
                Txt_Cuenta_Acreedor.Enabled = false;
                Txt_Cuenta_Contratista.Enabled = false;
                Txt_Cuenta_Deudor.Enabled = false;
                Txt_Cuenta_Judicial.Enabled = false;
                Txt_Cuenta_Nomina.Enabled = false;
                Txt_Cuenta_Predial.Enabled = false;
                Txt_Cuenta_Proveedor.Enabled = false;
                break;
            case "Dar de Alta":
                //Validamos que se llenen todos los campos requeridos
                Validar_Contenido_Controles();
                Validar_Contenido_Cuentas_Contables();
                //En caso de que pase las validaciones

                if (Grid_Correos.Rows.Count < 1)
                {
                    Ibtn_agregar_Correo_Click(sender, null);
                }

                if (Div_Contenedor_Msj_Error.Visible == false)
                {
                    //Como primer paso cargamos los datos del Proveedor
                    Cls_Cat_Con_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Con_Proveedores_Negocio();
                    Clase_Negocio = Cargar_Datos_Proveedor(Clase_Negocio);
                   
                    //Damos de Alta el Proveedor
                    String Mensaje = Clase_Negocio.Alta_Proveedor();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Proveedores", "alert('" + Mensaje + "');", true);
                    Configurar_Formulario("Inicio");
                }


                break;
        }


    }
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        switch (Btn_Modificar.ToolTip)
        {
            case "Modificar":
                if (Txt_Proveedor_ID.Text.Trim() == String.Empty)
                {

                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Es necesario seleccionar un Proveedor";
                    Lbl_Mensaje_Error.Visible = true;
                }
                else
                {
                    Configurar_Formulario("Modificar");
                    
                }
                break;
            case "Actualizar":
                //Validamos que se llenen todos los campos requeridos
                Validar_Contenido_Controles();
                Validar_Contenido_Cuentas_Contables();

                if (Grid_Correos.Rows.Count < 1)
                {
                    Ibtn_agregar_Correo_Click(sender, null);
                }

                //En caso de que pase las validaciones
                if (Div_Contenedor_Msj_Error.Visible == false)
                {
                    //Como primer paso cargamos los datos del Proveedor
                    Cls_Cat_Con_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Con_Proveedores_Negocio();
                    Clase_Negocio = Cargar_Datos_Proveedor(Clase_Negocio);

                    //Modificamos el Proveedor
                    String Mensaje = Clase_Negocio.Modificar_Proveedor();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Proveedores", "alert('" + Mensaje + "');", true);
                    Configurar_Formulario("Inicio");

                }


                break;
        }
    }
    protected void Chk_Actualizacion_CheckedChanged(object sender, EventArgs e)
    {
        if(Chk_Actualizacion.Checked == true)
        {
            if(Txt_Ultima_Actualizacion.Text != String.Empty)
            {
                Session["Ultima_Actualizacion"]= Txt_Ultima_Actualizacion.Text.Trim();
            }
            Txt_Ultima_Actualizacion.Text = DateTime.Now.ToString("dd/MMM/yyyy");
        }

        if(Chk_Actualizacion.Checked == false)
        {
            if(Session["Ultima_Actualizacion"] != null)
                Txt_Ultima_Actualizacion.Text = Session["Ultima_Actualizacion"].ToString().Trim();
            else 
                Txt_Ultima_Actualizacion.Text = "";
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        switch (Btn_Salir.ToolTip)
        {
            case "Cancelar":
                Configurar_Formulario("Inicio");
                Limpiar_Componentes();
                Habilitar_Componentes(false);


                break;
            case "Inicio":
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                Limpiar_Componentes();
                Habilitar_Componentes(false);

                break;
        }
    }
    protected void Cmb_Conceptos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        //LLENAMOS EL GRID DE PARTIDAS PARA AGREGARLAS AL PROVEEDOR
        Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
        Clase_Negocio.P_Concepto_ID = Cmb_Conceptos.SelectedValue;
        DataTable Dt_Partidas = Clase_Negocio.Consultar_Partidas_Especificas();
        Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Partidas_Generales, Dt_Partidas);
        if (Cmb_Conceptos.SelectedIndex == 0)
        {
            //Deshabilitamos el combo de Partidas 
            Cmb_Partidas_Generales.Enabled = false;
            Cmb_Partidas_Generales.Items.Clear();
            Btn_Agregar_Partida.Enabled = false;
        }
        else
        {
            Cmb_Partidas_Generales.Enabled = true;
            Btn_Agregar_Partida.Enabled = true;
        }
    }
    protected void Cmb_Partidas_Generales_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Si se selecciona se agrega al grid.
        Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
        DataTable Dt_Consulta_Partidas_Proveedores = new DataTable();
        if (Session["Dt_Consulta_Partidas_Proveedores"] != null)
        {
            Dt_Consulta_Partidas_Proveedores = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];
            DataRow[] Fila_Nueva;
            Fila_Nueva = Dt_Consulta_Partidas_Proveedores.Select("PARTIDA_GENERICA_ID='" + Cmb_Partidas_Generales.SelectedValue.Trim() + "'");
            if (Fila_Nueva.Length > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                "alert('No se puede agregar la dependencia, ya se ha agregado');", true);
            }
            else
            {
                DataRow Fila = Dt_Consulta_Partidas_Proveedores.NewRow();
                Fila["PARTIDA_GENERICA_ID"] = Cmb_Partidas_Generales.SelectedValue.ToString();
                Fila["PARTIDA"] = Cmb_Partidas_Generales.SelectedItem.Text;
                Fila["CONCEPTO_ID"] = Cmb_Conceptos.SelectedValue.ToString();
                Dt_Consulta_Partidas_Proveedores.Rows.Add(Fila);
                Dt_Consulta_Partidas_Proveedores.AcceptChanges();
                Grid_Partidas_Generales.DataSource = Dt_Consulta_Partidas_Proveedores;
                Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Consulta_Partidas_Proveedores;
                Grid_Partidas_Generales.DataBind();
                //Agregamos el Concepto al Grid de Conceptos 
                Agregar_Concepto_Proveedor();
            }
        }
        else
        {
            Dt_Consulta_Partidas_Proveedores.Columns.Add("PARTIDA_GENERICA_ID", typeof(System.String));
            Dt_Consulta_Partidas_Proveedores.Columns.Add("PARTIDA", typeof(System.String));
            Dt_Consulta_Partidas_Proveedores.Columns.Add("CONCEPTO_ID", typeof(System.String));
            
            DataRow[] Fila_Nueva;
            Fila_Nueva = Dt_Consulta_Partidas_Proveedores.Select("PARTIDA_GENERICA_ID='" + Cmb_Partidas_Generales.SelectedValue.Trim() + "'");
            if (Fila_Nueva.Length > 0)
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "+ No se puede agregar la Partida, pues ya esta agregada";
            }
            else
            {
                DataRow Fila = Dt_Consulta_Partidas_Proveedores.NewRow();
                Fila["PARTIDA_GENERICA_ID"] = Cmb_Partidas_Generales.SelectedValue.ToString();
                Fila["PARTIDA"] = Cmb_Partidas_Generales.SelectedItem.Text;
                Fila["CONCEPTO_ID"] = Cmb_Conceptos.SelectedValue.ToString();
                Dt_Consulta_Partidas_Proveedores.Rows.Add(Fila);
                Dt_Consulta_Partidas_Proveedores.AcceptChanges();
                Grid_Partidas_Generales.DataSource = Dt_Consulta_Partidas_Proveedores;
                Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Consulta_Partidas_Proveedores;
                Grid_Partidas_Generales.DataBind();
                Agregar_Concepto_Proveedor();
            }
            Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Consulta_Partidas_Proveedores;
        }
        Cmb_Partidas_Generales.SelectedIndex = 0;
    }
    protected void Btn_Busqueda_Avanzada_Click(object sender, EventArgs e)
    {
        Div_Busqueda_Avanzada.Visible = true;
    }
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            
            // Consulta_Proveedores(); //Consultar los proveedores que coincidan con el nombre porporcionado por el usuario
            Cls_Cat_Com_Proveedores_Negocio RS_Consulta_Cat_Com_Proveedores = new Cls_Cat_Com_Proveedores_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Proveedores; //Variable que obtendrá los datos de la consulta 
            if (Txt_Busqueda_Nombre_Comercial.Text != "")
            {
                RS_Consulta_Cat_Com_Proveedores.P_Nombre_Comercial = Txt_Busqueda_Nombre_Comercial.Text;
            }
            if (Txt_Busqueda_Padron_Proveedor.Text.Trim() != String.Empty)
            {
                RS_Consulta_Cat_Com_Proveedores.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Txt_Busqueda_Padron_Proveedor.Text.Trim()));
            }
            if (Txt_Busqueda_Razon_Social.Text.Trim() != String.Empty)
            {
                RS_Consulta_Cat_Com_Proveedores.P_Razon_Social = Txt_Busqueda_Razon_Social.Text.Trim();
            }
            if (Txt_Busqueda_RFC.Text.Trim() != String.Empty)
            {
                RS_Consulta_Cat_Com_Proveedores.P_RFC = Txt_Busqueda_RFC.Text.Trim();
            }
            if (Cmb_Busqueda_Estatus.SelectedIndex != 0)
            {
                RS_Consulta_Cat_Com_Proveedores.P_Estatus = Cmb_Busqueda_Estatus.SelectedItem.Text.Trim();
            }
            //Consulta los Proveedores con sus datos generales
            Dt_Proveedores = RS_Consulta_Cat_Com_Proveedores.Consulta_Avanzada_Proveedor();
            if (Dt_Proveedores.Rows.Count != 0)
            {
                Grid_Proveedores.Columns[6].Visible = true;
                Grid_Proveedores.Columns[7].Visible = true;
                Grid_Proveedores.Columns[8].Visible = true;
                Grid_Proveedores.Columns[9].Visible = true;
                Grid_Proveedores.Columns[10].Visible = true;
                Grid_Proveedores.Columns[11].Visible = true;
                Grid_Proveedores.Columns[12].Visible = true;
                Grid_Proveedores.Columns[13].Visible = true;
                Grid_Proveedores.Columns[14].Visible = true;
                Grid_Proveedores.Columns[15].Visible = true;
                Grid_Proveedores.DataSource = Dt_Proveedores;
                Grid_Proveedores.DataBind();
                Session["Dt_Proveedores"] = Dt_Proveedores;
                Grid_Proveedores.Columns[6].Visible = false;
                Grid_Proveedores.Columns[7].Visible = false;
                Grid_Proveedores.Columns[8].Visible = false;
                Grid_Proveedores.Columns[9].Visible = false;
                Grid_Proveedores.Columns[10].Visible = false;
                Grid_Proveedores.Columns[11].Visible = false;
                Grid_Proveedores.Columns[12].Visible = false;
                Grid_Proveedores.Columns[13].Visible = false;
                Grid_Proveedores.Columns[14].Visible = false;
                Grid_Proveedores.Columns[15].Visible = false;
            }
            else
            {
                Grid_Proveedores.EmptyDataText = "No se han encontrado registros.";
                //Lbl_Mensaje_Error.Text = "+ No se encontraron datos <br />";
                Grid_Proveedores.DataSource = new DataTable();
                Grid_Proveedores.DataBind();
            }


            Limpiar_Controles_Busqueda_Avanzada(); //Limpia los controles de la forma
            Limpiar_Componentes();
            //Si no se encontraron Proveedores con un nombre similar al proporcionado por el usuario entonces manda un mensaje al usuario
            Btn_Salir.ToolTip = "Regresar";
            if (Grid_Proveedores.Rows.Count <= 0)
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron Proveedores con el nombre proporcionado <br />";
                Div_Proveedores.Visible = false;
            }
            else
            {
                Div_Proveedores.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
        Div_Busqueda_Avanzada.Visible = false;
    }
    protected void Btn_Limpiar_Busqueda_Avanzada_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Limpiar_Controles_Busqueda_Avanzada();
    }
    protected void Btn_Cerrar_Busqueda_Avanzada_Click(object sender, ImageClickEventArgs e)
    {
        Div_Busqueda_Avanzada.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Limpiar_Controles_Busqueda_Avanzada();
    }
    protected void Chk_Fisica_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk_Fisica.Checked == true)
            Chk_Moral.Checked = false;

    }
    protected void Chk_Moral_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk_Moral.Checked == true)
            Chk_Fisica.Checked = false;
    }
    protected void Btn_Agregar_Partida_Click(object sender, ImageClickEventArgs e)
    {
        //Si se selecciona se agrega al grid.
        if (Cmb_Partidas_Generales.SelectedIndex > 0)
        {
            Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
            DataTable Dt_Consulta_Partidas_Proveedores = new DataTable();
            if (Session["Dt_Consulta_Partidas_Proveedores"] != null)
            {
                Dt_Consulta_Partidas_Proveedores = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];
                DataRow[] Fila_Nueva;
                Fila_Nueva = Dt_Consulta_Partidas_Proveedores.Select("PARTIDA_GENERICA_ID='" + Cmb_Partidas_Generales.SelectedValue.Trim() + "'");
                if (Fila_Nueva.Length > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                    "alert('No se puede agregar la dependencia, ya se ha agregado');", true);
                }
                else
                {
                    DataRow Fila = Dt_Consulta_Partidas_Proveedores.NewRow();
                    Fila["PARTIDA_GENERICA_ID"] = Cmb_Partidas_Generales.SelectedValue.ToString();
                    Fila["PARTIDA"] = Cmb_Partidas_Generales.SelectedItem.Text;
                    Fila["CONCEPTO_ID"] = Cmb_Conceptos.SelectedValue.ToString();
                    Dt_Consulta_Partidas_Proveedores.Rows.Add(Fila);
                    Dt_Consulta_Partidas_Proveedores.AcceptChanges();
                    Grid_Partidas_Generales.DataSource = Dt_Consulta_Partidas_Proveedores;
                    Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Consulta_Partidas_Proveedores;
                    Grid_Partidas_Generales.DataBind();
                    //Agregamos el Concepto al Grid de Conceptos 
                    Agregar_Concepto_Proveedor();
                }
            }
            else
            {
                Dt_Consulta_Partidas_Proveedores.Columns.Add("PARTIDA_GENERICA_ID", typeof(System.String));
                Dt_Consulta_Partidas_Proveedores.Columns.Add("PARTIDA", typeof(System.String));
                Dt_Consulta_Partidas_Proveedores.Columns.Add("CONCEPTO_ID", typeof(System.String));
                
                DataRow[] Fila_Nueva;
                Fila_Nueva = Dt_Consulta_Partidas_Proveedores.Select("PARTIDA_GENERICA_ID='" + Cmb_Partidas_Generales.SelectedValue.Trim() + "'");
                if (Fila_Nueva.Length > 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "+ No se puede agregar la Partida, pues ya esta agregada";
                }
                else
                {
                    DataRow Fila = Dt_Consulta_Partidas_Proveedores.NewRow();
                    Fila["PARTIDA_GENERICA_ID"] = Cmb_Partidas_Generales.SelectedValue.ToString();
                    Fila["PARTIDA"] = Cmb_Partidas_Generales.SelectedItem.Text;
                    Fila["CONCEPTO_ID"] = Cmb_Conceptos.SelectedValue.ToString();
                    Dt_Consulta_Partidas_Proveedores.Rows.Add(Fila);
                    Dt_Consulta_Partidas_Proveedores.AcceptChanges();
                    Grid_Partidas_Generales.DataSource = Dt_Consulta_Partidas_Proveedores;
                    Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Consulta_Partidas_Proveedores;
                    Grid_Partidas_Generales.DataBind();
                    Agregar_Concepto_Proveedor();
                }
            }
            Cmb_Partidas_Generales.SelectedIndex = 0;
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Proveedores", "alert('Seleccione una Partida General.');", true);
    }

    #endregion
}
