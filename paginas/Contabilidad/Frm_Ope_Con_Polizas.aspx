<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Polizas.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Polizas" Title="Polizas"  ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
<script type="text/javascript" language="javascript" >
    //Metodo para mantener los calendarios en una capa mas alat.
        function calendarShown(sender, args)
        {
            sender._popupBehavior._element.style.zIndex = 10000005;
        }
        //Metodos para limpiar los controles de la busqueda.
        function Limpiar_Ctlr()
        {
            var ahora;
            ahora = new Date();
            document.getElementById("<%=Txt_No_Poliza_PopUp.ClientID%>").value="";
            document.getElementById("<%=Cmb_Busqueda_Anio_Poliza.ClientID%>").value="";
            document.getElementById("<%=Cmb_Busqueda_Tipo_Poliza.ClientID%>").value="";
            document.getElementById("<%=Cmb_Busqueda_Mes_Poliza.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Anio_Poliza.ClientID%>").value = ahora.getFullYear();
            document.getElementById("<%=Cmb_Busqueda_Mes_Poliza.ClientID%>").value = ahora.getMonth();
            return false;
        }
        function calculo() {
            if (document.getElementById("<%=Txt_Haber_Partida.ClientID%>").value != "") {
                document.getElementById("<%=Txt_Debe_Partida.ClientID%>").disabled = false;
                document.getElementById("<%=Txt_Debe_Partida.ClientID%>").value = "";
            }
            else{
                document.getElementById("<%=Txt_Debe_Partida.ClientID%>").disabled = true;
            }
        }
        function calculo2() {
            if (document.getElementById("<%=Txt_Debe_Partida.ClientID%>").value != "") {
                document.getElementById("<%=Txt_Haber_Partida.ClientID%>").disabled = false;
                document.getElementById("<%=Txt_Haber_Partida.ClientID%>").value = "";
            }
            else {
                document.getElementById("<%=Txt_Haber_Partida.ClientID%>").disabled = true;
            }
        }
        function Limpiar_Carga()
        {
            
        }
        function Limpiar_Autorizar_Password()
        {
            document.getElementById("<%=Txt_No_Empleado_Popup.ClientID%>").value="";
            document.getElementById("<%=Txt_Password_Popup.ClientID%>").value="";
        }
        function Abrir_Modal_Popup() 
        {
            Limpiar_Ctlr();
            $find('Busqueda_Polizas').show();
            return false;
        }
//        function Abrir_Carga_PopUp() 
//        {
//            Limpiar_Carga();
//            $find('Carga_Masiva').show();
//            return false;
        //        }
        function Abrir_Autorizar_Password() 
        {
            Limpiar_Autorizar_Password();
            $find('Autorizar_Password').show();
            return false;
        }
        function Limpiar_Ctlr_2() {
            document.getElementById("<%=Txt_Busqueda_Cuenta.ClientID%>").value = "";
            return false;
        }
        function Abrir_Modal_Popup_2() {
            $find('Busqueda_Cuenta').show();
            return false;
        }
        function pageLoad() { $('[id*=Txt_Comen').keyup(function() {var Caracteres =  $(this).val().length;if (Caracteres > 250) {this.value = this.value.substring(0, 250);$(this).css("background-color", "Yellow");$(this).css("color", "Red");}else{$(this).css("background-color", "White");$(this).css("color", "Black");}$('#Contador_Caracteres_Comentarios').text('Car�cteres Ingresados [ ' + Caracteres + ' ]/[ 250 ]');});}
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Polizas" runat="server"  ScriptMode="Release" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>        
           <asp:UpdateProgress ID="Uprg_Polizas" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
          <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            
            <div id="Div_Polizas" style="background-color:#ffffff; width:98%; height:100%;">
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">P&oacute;lizas</td>
                    </tr>
                    <tr>
                        <td >&nbsp;
                            <asp:UpdatePanel ID="Upnl_Mensajes_Error" runat="server" >
                                <ContentTemplate>                         
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"/>
                                </ContentTemplate>                                
                            </asp:UpdatePanel>
                        </td>
                    </tr> 
                </table>
                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td>                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;"> 
                                            <asp:UpdatePanel ID="Upnl_Botones_Operacion" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
                                                    <ContentTemplate> 
                                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                        CssClass="Img_Button" TabIndex="1"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                        onclick="Btn_Nuevo_Click" />
                                                    <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" 
                                                        CssClass="Img_Button" TabIndex="2"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                                                        onclick="Btn_Modificar_Click" />
                                                    <asp:ImageButton ID="Btn_Copiar" runat="server" CssClass="Img_Button" 
                                                        ImageUrl="~/paginas/imagenes/paginas/subir.png" TabIndex="3" 
                                                        ToolTip="Carga Masiva" OnClick="Btn_Carga_Masiva_OnClick"/> 
                                                    <asp:ImageButton ID="Btn_Cancelar_Poliza" runat="server" ToolTip="Cancelar P�liza" 
                                                        CssClass="Img_Button" TabIndex="3"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_cancelar.png"
                                                        OnClientClick="return confirm('&iquest;Est&aacute; seguro de Cancelar la P&oacute;liza seleccionada?');" 
                                                        onclick="Btn_Cancelar_Poliza_Click"/>
                                                    <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                        CssClass="Img_Button" TabIndex="4"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                        onclick="Btn_Salir_Click" />
                                                    <asp:ImageButton ID="Btn_Imprimir" runat="server" ToolTip="Imprimir" CssClass="Img_Button" 
                                                        TabIndex="5" ImageUrl="~/paginas/imagenes/gridview/grid_print.png"  Visible="false"
                                                        onclick="Btn_Imprimir_Click" />
                                                     <%--<asp:ImageButton ID="Btn_Borradores_Guardados" runat="server" ToolTip="Pre-Polizas" 
                                                        CssClass="Img_Button" TabIndex="4"
                                                        ImageUrl="~/paginas/imagenes/paginas/report.png" 
                                                        onclick="Btn_Borrador_Consulta_Click" />--%>
                                                    <asp:Button Style="background-color: transparent; border-style:none; visibility:hidden" ID="Btn_Cerrar" runat="server" Text="" />
                                                    <asp:Button  Style="background-color: transparent; border-style:none; visibility:hidden" ID="Btn_Open" runat="server" Text="" />
                                                    <asp:Button  Style="border-style:none; visibility:visible" ID="Btn_Password" 
                                                        runat="server" Text="Password" 
                                                        OnClientClick="javascript:return Abrir_Autorizar_Password();" 
                                                        CausesValidation="false" onclick="Btn_Password_Click"/>
                                                    <cc1:ModalPopupExtender ID="Mpe_Autorizar_Password" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Autorizar_Password"
                                                            PopupControlID="Pnl_Password" TargetControlID="Btn_Open" PopupDragHandleControlID="Pnl_Password_Cabecera" 
                                                            CancelControlID="Btn_Cerrar" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                                </ContentTemplate>
                                            </asp:UpdatePanel>                                                
                                        </td>
                                        <td align="right" style="width:41%;">
                                            <table style="width:100%;height:28px;">
                                                <tr>
                                                    <td style="width:100%;vertical-align:top;" align="right">
                                                        B&uacute;squeda 
                                                        <asp:UpdatePanel ID="Udp_Modal_Popup" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
                                                            <ContentTemplate> 
                                                                    <asp:ImageButton ID="Btn_Mostrar_Popup_Busqueda" runat="server" ToolTip="Busqueda Avanzada" TabIndex="23" 
                                                                        ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Height="24px" Width="24px"
                                                                        OnClientClick="javascript:return Abrir_Modal_Popup();" CausesValidation="false" />
                                                                    <cc1:ModalPopupExtender ID="Mpe_Busqueda_Polizas" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Busqueda_Polizas"
                                                                        PopupControlID="Pnl_Busqueda_Contenedor" TargetControlID="Btn_Comodin_Open"  CancelControlID="Btn_Comodin_Close" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                                                    <asp:Button Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Close" runat="server" Text="" />
                                                                    <asp:Button  Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Open" runat="server" Text="" />                                                                                                    
                                                            </ContentTemplate>
                                                            <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="Btn_Mostrar_Popup_Busqueda" EventName ="Click" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>     
                                                </tr>                                                                          
                                            </table>                                    
                                        </td>       
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>
                    <div id="Div_Carga_Masiva" runat="server" style="color: #5D7B9D;display:none" > 
                         <asp:Panel ID="Panel1" runat="server" GroupingText="Carga Masiva" Width="98%" BackColor="white"> 
                            <table width="99%">
                                <tr>
                                     <td align="right" style="width:10%;">
                                       <asp:ImageButton ID="ImageButton1" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Ventana_Carga_Click"/>  
                                    </td>
                                </tr>
                            </table>
                            <table width="99%">   
                                <tr>
                                    <td style="width:20%">
                                       Ruta del Archivo
                                    </td>
                                    <td style="width:80%">
                                       <cc1:AsyncFileUpload ID="AFU_Archivo_Excel" runat="server" size="200px" UploadingBackColor="LightBlue"
                                            ThrobberID="Throbber" onuploadedcomplete="AFU_Archivo_Excel_UploadedComplete"/>
                                    </td> 
                                </tr>
                                <tr>
                                    <td colspan="2" >
                                        <center>
                                            <asp:Button ID="Btn_Carga_Masiva_Popup" runat="server"  Text="Capturar Polizas" CssClass="button"  
                                            CausesValidation="false" OnClick="Btn_Carga_Masiva_Popup_Click" Width="200px"/> 
                                        </center>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                     <div id="Div_Pre_Poliza" runat="server" style="color: #5D7B9D;display:none" > 
                         <asp:Panel ID="Panel_Borradores" runat="server" GroupingText="PRE-POLIZAS" Width="98%" BackColor="white"> 
                            <table width="99%">
                                <tr>
                                     <td align="right" style="width:10%;">
                                       <asp:ImageButton ID="Btn_Pre_poliza_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Ventana_Borradores_Click"/>  
                                    </td>
                                </tr>
                            </table>
                            <%--<table width="99%">   
                                <tr>
                                    <td style="width:20%">
                                       Pre-Poliza
                                    </td>
                                    <td style="width:80%">
                                        <asp:DropDownList ID="Cmb_Prepolizas" runat="server"  Width ="90%">
                                        </asp:DropDownList>
                                    </td> 
                                </tr>
                                <tr>
                                    <td colspan="2" >
                                        <center>
                                            <asp:Button ID="Btn_Subir_Pre_poliza" runat="server"  Text="Cargar" CssClass="button"  
                                                CausesValidation="false"  Width="200px" OnClick="Btn_Carga_Prepoliza_Popup_Click"/>
                                            <asp:HiddenField ID="Hd_Prepoliza" runat="server" />
                                        </center>
                                    </td>
                                </tr>
                            </table>--%>
                        </asp:Panel>
                    </div>
                <asp:UpdatePanel ID="Upnl_Generales_Poliza" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Pnl_Datos_Generales" runat="server" GroupingText="Datos de la P�liza" Width="98%" BackColor="white">
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td width="70px">*Tipo P&oacute;liza</td>
                                    <td width="200px">
                                        <asp:DropDownList ID="Cmb_Tipo_Poliza"  runat="server" TabIndex="5" Width="98%" Font-Size="11px" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Tipo_Poliza_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="70px">No P&oacute;liza</td>
                                    <td width ="100px">
                                        <asp:TextBox ID="Txt_No_Poliza" runat="server" Enabled="false"></asp:TextBox></td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="70px">Prefijo</td>
                                    <td width="200px"><asp:TextBox ID="Txt_Prefijo" runat="server" Width="98%" Enabled="false"></asp:TextBox></td>
                                    <td width="70px">*Fecha</td>
                                    <td width ="200px">
                                        <asp:TextBox ID="Txt_Fecha_Poliza" runat="server" Width="80%" TabIndex="6" MaxLength="11" Height="18px"/>
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Poliza" runat="server" 
                                            TargetControlID="Txt_Fecha_Poliza" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/A�o" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Poliza" runat="server" 
                                            TargetControlID="Txt_Fecha_Poliza" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Poliza"/>
                                         <asp:ImageButton ID="Btn_Fecha_Poliza" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Poliza" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Poliza" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="Mev_Txt_Fecha_Poliza" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Poliza"
                                            ControlExtender="Mee_Txt_Fecha_Poliza" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha P�liza Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de P�liza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>  
                                    </td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <%--<tr>
                                    <td id="Td_Programa" runat="server">
                                        *Programa
                                    </td>
                                    <td id="Td_Programa_Combo" colspan="3" runat="server">
                                        <asp:DropDownList ID="Cmb_Programas_Poliza" runat="server" Width="99%" Font-Size="11px" AutoPostBack="true" onselectedindexchanged="Cmb_Programas_Poliza_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td id="Td_Monto" runat="server">*Monto
                                    </td>
                                    <td id="Td_Monto_Text" runat="server">
                                        <asp:TextBox ID="Txt_Monto_Programa" runat="server" Width="78.5%" ReadOnly=true ></asp:TextBox>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td width="70px">*Concepto</td>
                                    <td colspan="5">
                                        <asp:TextBox ID="Txt_Concepto_Poliza" runat="server" Width="98%" MaxLength="80" Font-Size="Small"></asp:TextBox>
                                     <cc1:FilteredTextBoxExtender  ID="Txt_Concepto_Poliza_FilteredTextBoxExtender" 
                                        runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                        TargetControlID="Txt_Concepto_Poliza" InvalidChars="<,>,&,',!,"  ValidChars="��.,:;()����������-%/ ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="Upnl_Partidas_Polizas" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Txt_Cuenta_Contable"  EventName="Textchanged"/>
                    <asp:AsyncPostBackTrigger ControlID="Cmb_Descripcion" EventName="Selectedindexchanged"/>
                    <asp:AsyncPostBackTrigger ControlID="Btn_Salir"  EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="Btn_Nuevo" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="Btn_Modificar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="Btn_Copiar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="Btn_Cancelar_Poliza"  EventName="Click" />
                    <asp:AsyncPostBackTrigger controlID="Btn_Agregar_Partida" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="Txt_Empleado_Autorizo" EventName="Textchanged" />
                    <asp:AsyncPostBackTrigger  ControlID="Grid_Polizas" EventName="Selectedindexchanged"/>
                    <asp:AsyncPostBackTrigger ControlID="Grid_Detalles_Poliza" EventName="Selectedindexchanged" />
                </Triggers>
                    <ContentTemplate>
                        <asp:Panel ID="Pnl_Partidas_Polizas" runat="server" GroupingText="Partidas Contables" Width="98%" BackColor="White">
                            <%--<div id="Div_Momento" runat="server" >
                            <asp:Panel ID="Pnl_Momentos_Egresos" runat="server" GroupingText="Cuentas Egreso" Width="98%" BackColor="White">
                                <table width="99%" class="estilo_fuente">
                                    <tr>
                                    <td style="width:15%"> Momento inicial</td>
                                    <td style="width:35%">
                                        <asp:DropDownList ID="Cmb_Momento_incial" runat="server" Width="90%" Font-Size="11px">
                                        <asp:ListItem Value="1">DISPONIBLE</asp:ListItem>
                                        <asp:ListItem Value="2">COMPROMETIDO</asp:ListItem>
                                        <asp:ListItem Value="3">DEVENGADO</asp:ListItem>
                                        <asp:ListItem Value="4">EJERCIDO</asp:ListItem>
                                        <asp:ListItem Value="5">PAGADO</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width:15%"> Momento Final</td>
                                    <td style="width:35%">
                                        <asp:DropDownList ID="Cmb_Momento_Final" runat="server"  Width="90%" Font-Size="11px">
                                        <asp:ListItem Value="1">DISPONIBLE</asp:ListItem>
                                        <asp:ListItem Value="2">COMPROMETIDO</asp:ListItem>
                                        <asp:ListItem Value="3">DEVENGADO</asp:ListItem>
                                        <asp:ListItem Value="4">EJERCIDO</asp:ListItem>
                                        <asp:ListItem Value="5">PAGADO</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            </div>
                           <div id="Div_Momentos_Ingresos" runat="server" >
                            <asp:Panel ID="Pnl_Momentos_Ingresos" runat="server" GroupingText="Cuentas Ingreso" Width="98%" BackColor="White">
                                <table width="99%" class="estilo_fuente">
                                    <tr>
                                    <td style="width:15%"> Momento</td>
                                    <td style="width:35%">
                                        <asp:DropDownList ID="Cmb_Momento_Ingresos" runat="server" Width="90%" Font-Size="11px">
                                        <asp:ListItem Value="1">DEVENGADO</asp:ListItem>
                                        <asp:ListItem Value="2">RECAUDADO</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width:15%"></td>
                                    <td style="width:35%">
                                    </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            </div>--%>
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td width="14%">Descripci&oacute;n</td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:DropDownList ID="Cmb_Descripcion" runat="server" width="78%" Font-Size="11px"
                                            onselectedindexchanged="Cmb_Descripcion_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:UpdatePanel ID="Udp_Busqueda_Modal" runat="server" UpdateMode="Conditional" RenderMode="Inline"  >
                                        <ContentTemplate> 
                                                <asp:ImageButton ID="Btn_Mostrar_Busqueda" runat="server" 
                                                ToolTip="Busqueda Avanzada" TabIndex="1"
                                                ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Height="14px" Width="14px"
                                                OnClientClick="javascript:return Abrir_Modal_Popup_2();" CausesValidation="false" />
                                                <cc1:ModalPopupExtender  ID="Mpe_Busqueda_Cuenta" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Busqueda_Cuenta"
                                                 PopupControlID="Pnl_Busqueda_Cuenta" TargetControlID="Btn_Open_2" 
                                                CancelControlID="Btn_Close" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                                <asp:Button Style="background-color: transparent; border-style:none; width:.5px;" 
                                                ID="Btn_Close" runat="server" Text="" />
                                                <asp:Button  Style="background-color: transparent; border-style:none; width:.5px;" 
                                                ID="Btn_Open_2" runat="server" Text="" />                                                                                                    
                                         </ContentTemplate>
                                         </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">Cuenta</td>
                                    <td width="40%">Concepto</td>
                                    <td width="15%">Debe</td>
                                    <td width="15%">Haber</td>
                                    <td width="5%"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="Txt_Cuenta_Contable" runat="server" width="90%"  Font-Size="11px"
                                            ontextchanged="Txt_Cuenta_Contable_TextChanged" AutoPostBack ="true"></asp:TextBox> 
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="Txt_Cuenta_Contable" FilterType="Custom" ValidChars="1234567890-"></cc1:FilteredTextBoxExtender>                                       
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Concepto_Partida" runat="server" width="98%" MaxLength="250" Font-Size="11px"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Txt_Concepto_Partida_FilteredTextBoxExtender" 
                                                runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Concepto_Partida" 
                                                ValidChars="��.,:;()����������-%/ ">
                                            </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Debe_Partida" runat="server" width="90%"  OnKeyUp="javascript:calculo2();" Enabled="false" Font-Size="11px" AutoPostBack="true" OnTextChanged="Txt_Debe_Partida_TextChanged"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="Txt_Debe_Partida" FilterType="Custom" ValidChars="1234567890."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Haber_Partida" runat="server" width="90%" OnKeyUp="javascript:calculo();" Enabled="false" Font-Size="11px" AutoPostBack="true" OnTextChanged="Txt_Debe_Partida_TextChanged" ></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="Txt_Haber_Partida" FilterType="Custom" ValidChars="1234567890."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td align="center">
                                        <asp:ImageButton ID="Btn_Agregar_Partida" runat="server" ToolTip="Agregar"  
                                            CssClass="Img_Button" TabIndex="16" Height="14px" Width="14px"
                                            ImageUrl="~/paginas/imagenes/gridview/add_grid.png" 
                                            onclick="Btn_Agregar_Partida_Click" AutoPostBack="true" />                                            
                                    </td>
                                </tr>
                                </table>
                                <table width="100%" class="estilo_fuente">
                                    <tr>
                                        <td>
                                        <div id="Div_Presupuestal" runat ="server" style=" display:none">
                                            <table width="100%" class="estilo_fuente">
                                                <tr>
                                                    <td>
                                                        <asp:Panel  ID="Panel_Ingresos" runat="server" GroupingText="Afectacion Presupuestal" Width="99%" BackColor="White">
                                                            <table width="100%" class="estilo_fuente">
                                                            <tr>
                                                            <td>
                                                                <div id="Div_Ingresos" style=" display:none" runat="server">
                                                                    <table width="100%" class="estilo_fuente" >
                                                                        <tr>
                                                                            <td width="15%">F. Financiamiento</td>
                                                                            <td width="35%">
                                                                                <asp:DropDownList ID="Cmb_Fuente_Financiamiento" runat="server" width="80%" Font-Size="11px"
                                                                                 AutoPostBack="true"  OnSelectedIndexChanged="Cmb_Fuente_Financiamiento_Ing_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td width="5%">
                                                                                    <asp:Label ID="Lbl_Programas_ingresos" runat="server" Text="Programas" Font-Size="11px"></asp:Label>
                                                                                </td>
                                                                                <td width="50%">
                                                                                    <asp:DropDownList ID="Cmb_Programas_Ing" runat="server" width="80%"  Font-Size="11px">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div id="Div_Egresos" style=" display:none" runat="server">
                                                                        <table  width="100%" class="estilo_fuente">
                                                                            <tr>
                                                                                <td width="15%">
                                                                                    <asp:Label ID="Lbl_Fuente_Financiamiento_Egresos" runat="server" Text="F. Financiamiento"></asp:Label>
                                                                                </td>
                                                                                <td width="35%">
                                                                                    <asp:DropDownList ID="Cmb_Fuente_Financiamiento_Egr" runat="server" width="96%" Font-Size="11px"
                                                                                         AutoPostBack="true" OnSelectedIndexChanged="Cmb_Fuente_Financiamiento_Egr_SelectedIndexChanged">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td width="15%">
                                                                                    <asp:Label ID="Lbl_Unidad_responsable" runat="server" Text="U.R." Font-Size="11px"></asp:Label>
                                                                                </td>
                                                                                <td width="35%">
                                                                                    <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" width="96%"  Font-Size="11px"
                                                                                    AutoPostBack="true" OnSelectedIndexChanged="Cmb_Unidad_Responsable_SelectedIndexChanged">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="15%">
                                                                                    <asp:Label ID="Lbl_Programa" runat="server" Text="Programa"></asp:Label>
                                                                                </td>
                                                                                <td width="85%" colspan="3">
                                                                                    <asp:DropDownList ID="Cmb_Programa" runat="server" width="98%" Font-Size="11px">
                                                                                    </asp:DropDownList>
                                                                                    <asp:HiddenField ID="Txt_Partida_Presupuestal" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                            
                                        </td>
                                    </tr>
                                </table>

                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td style="width:100%;text-align:center;vertical-align:top;"> 
                                        <center>
                                            <div style="overflow:auto;height:200px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;" >
                                                <asp:GridView ID="Grid_Detalles_Poliza" runat="server" 
                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" 
                                                    OnRowDataBound="Grid_Detalles_Poliza_RowDataBound" Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="PARTIDA" HeaderText="No">
                                                            <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="5%"  Font-Size="XX-Small" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CUENTA_CONTABLE_ID" HeaderText="Cuenta Contable ID">
                                                            <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="0%"  Font-Size="XX-Small" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CUENTA" HeaderText="Cuenta">
                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="15%"  Font-Size="XX-Small" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto">
                                                            <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="30%"   Font-Size="XX-Small"/>
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="DEBE">
                                                        <HeaderStyle HorizontalAlign="Center" Width="12%" Font-Size="X-Small"/>
                                                        <ItemStyle HorizontalAlign="Center" Width="12%" />
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Txt_DEBE_Grid" runat="server" Font-Names="Courier New"
                                                                Font-Size="10px" MaxLength ="10" OnTextChanged="Txt_Poliza_TextChanged" AutoPostBack="true"
                                                                CssClass='<%# Eval("PARTIDA") %>'
                                                                 name="<%=Txt_DEBE_Grid.ClientID %>" 
                                                                Width="75%"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_DEBE_Grid" runat="server" 
                                                                Enabled="True" TargetControlID="Txt_DEBE_Grid" 
                                                                 FilterType="Custom,Numbers" 
                                                                 ValidChars=".">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="HABER">
                                                        <HeaderStyle HorizontalAlign="Center" Width="12%" Font-Size="X-Small"/>
                                                        <ItemStyle HorizontalAlign="Center" Width="12%"  Font-Size="XX-Small"/>
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Txt_HABER_Grid" runat="server" Font-Names="Courier New" 
                                                                Font-Size="10px" MaxLength ="10" OnTextChanged="Txt_Poliza_TextChanged"  AutoPostBack="true"
                                                                CssClass='<%# Eval("PARTIDA") %>'
                                                                 name="<%=Txt_HABER_Grid.ClientID %>" 
                                                                Width="75%"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_HABER_Grid" runat="server" 
                                                                Enabled="True" TargetControlID="Txt_HABER_Grid" 
                                                                 FilterType="Custom,Numbers" 
                                                                 ValidChars=".">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                        <asp:BoundField DataField="FUENTE_FINANCIAMIENTO_ID" 
                                                            HeaderText="Financiamiento">
                                                            <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="0%"  Font-Size="XX-Small" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="PROYECTO_PROGRAMA_ID" HeaderText="Programa">
                                                            <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="0%"  Font-Size="XX-Small"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="PARTIDA_ID" HeaderText="Partida">
                                                            <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="0%"  Font-Size="XX-Small"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="DEPENDENCIA_ID" HeaderText="Dependencia">
                                                            <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="MOMENTO_INICIAL" HeaderText="MOMENTO_INICIAL">
                                                            <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="0%"  Font-Size="XX-Small"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="MOMENTO_FINAL" HeaderText="MOMENTO_FINAL">
                                                            <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="0%"  Font-Size="XX-Small"/>
                                                        </asp:BoundField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <center>
                                                                    <asp:ImageButton ID="Btn_Eliminar_Partida" runat="server" 
                                                                        CausesValidation="false" 
                                                                        ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                                        OnClick="Btn_Eliminar_Partida" 
                                                                        OnClientClick="return confirm('&iquest;Est&aacute; seguro de eliminar de la tabla la partida de la p&oacute;liza seleccionada?');" />
                                                                </center>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="5%"  Font-Size="XX-Small"/>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="DEBE" HeaderText="Debe">
                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="10%"  Font-Size="XX-Small"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="HABER" HeaderText="Haber">
                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="10%"  Font-Size="XX-Small"/>
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>
                                            </div>
                                        </center>     
                                        <asp:HiddenField ID="Hdn_Cambio" runat="server" />                                  
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td style="width:11%">No. Partidas</td>
                                    <td style="width:15%">
                                        <asp:TextBox ID="Txt_No_Partidas" runat="server" Enabled="false" Width="98%"></asp:TextBox></td>
                                    <td style="width:40%;" align="right">Acumulado</td>
                                    <td style="width:15%">
                                        <asp:TextBox ID="Txt_Total_Debe" runat="server" Enabled="false" Width="98%" CssClass=""></asp:TextBox></td>
                                    <td style="width:15%">
                                        <asp:TextBox ID="Txt_Total_Haber" runat="server" Enabled="false" Width="98%"></asp:TextBox></td>
                                    <td style="width:10%"></td>
                                </tr>
                                <%--<tr>
                                    <td colspan="6">
                                    <table width="100%" class="estilo_fuente">
                                        <tr>
                                            <td style="width:10%">
                                              <asp:Label ID="Lbl_Pre_Poliza" runat="server" Text="Pre-Poliza"></asp:Label>
                                            </td>
                                            <td colspan="2" style="width:30%">
                                                <asp:TextBox ID="Txt_Pre_Poliza" runat="server" Width="99%"></asp:TextBox>
                                            </td>
                                            <td style="width:60%">
                                                <asp:Button ID="Btn_Borrador" runat="server" Text="Pre-Poliza" CssClass="button" Height="19px"
                                                OnClientClick="return confirm('�Est� seguro de Guardar como Borrador?');"  
                                                 OnClick="Btn_Guardar_Borrador_Click"/>                                     
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td colspan="2">
                                        <asp:Panel ID="Panel2" runat="server" GroupingText="Empleado elabora" 
                                            Width="98%" BackColor="White">
                                            <asp:TextBox ID="Txt_Empleado_Creo" runat="server"  Width="100%"></asp:TextBox>
                                            <%--<asp:DropDownList ID="Cmb_Empleado_Creo" runat="server" AutoPostBack="true" width="100%"></asp:DropDownList>--%></asp:Panel>
                                    </td>
                                    <td colspan="3">
                                        <asp:Panel ID="Panel3" runat="server" GroupingText="Empleado autoriza" Width="100%" BackColor="White">
                                            <asp:TextBox ID="Txt_Empleado_Autorizo" runat="server" Width="48%" CssClass="" ontextchanged="Txt_Empleado_Autorizo_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Empleado_Autorizo" runat="server" WatermarkCssClass="watermarked"
                                                WatermarkText="No. Empleado o Nombre" TargetControlID="Txt_Empleado_Autorizo" />
                                            <asp:DropDownList ID="Cmb_Nombre_Empleado" runat="server" AutoPostBack="true" width="48%"></asp:DropDownList></asp:Panel>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                            
                            <%--********************************************************
                            ********************************************************
                            ********************************************************--%></asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>      
            </div>
            <script type="text/javascript" language="javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(fin_peticion);

            function fin_peticion() {
                // manejo de ocultar tr dependiendo del tipo de operacion que realicen
//                $(document).ready(function() {
//                $("select[id$=Cmb_Tipo_Poliza]").change(function() {
//                    $(".Cmb_Tipo_Poliza option:selected").each(function() {
//                            var Tipo = $(this).val();
//                            if (Tipo == 4) {
//                                $("#Td_Programa").show();
//                                $("#Td_Programa_Combo").show();
//                                $("#Td_Monto").show();
//                                $("#Td_Monto_Text").show();
//                            }
//                            else {
//                                $("#Td_Programa").hide();
//                                $("#Td_Programa_Combo").hide();
//                                $("#Td_Monto").hide();
//                                $("#Td_Monto_Text").hide();
//                            }
//                        });
//                    }).trigger('change');
//                });
                //Metodo para mantener los calendarios en una capa mas alat.
                function calendarShown(sender, args) {
                    sender._popupBehavior._element.style.zIndex = 10000005;
                }
                //Metodos para limpiar los controles de la busqueda.
                function Limpiar_Ctlr() {
                    document.getElementById("<%=Txt_No_Poliza_PopUp.ClientID%>").value = "";
                    document.getElementById("<%=Cmb_Busqueda_Anio_Poliza.ClientID%>").value = "";
                    document.getElementById("<%=Cmb_Busqueda_Tipo_Poliza.ClientID%>").value = "";
                    document.getElementById("<%=Cmb_Busqueda_Mes_Poliza.ClientID%>").value = "";
                    return false;
                }
                function calculo() {
                    if (document.getElementById("<%=Txt_Haber_Partida.ClientID%>").value != "") {
                        document.getElementById("<%=Txt_Debe_Partida.ClientID%>").disabled = false;
                        document.getElementById("<%=Txt_Debe_Partida.ClientID%>").value = "";
                    }
                    else {
                        document.getElementById("<%=Txt_Debe_Partida.ClientID%>").disabled = true;
                    }
                }
                function calculo2() {
                    if (document.getElementById("<%=Txt_Debe_Partida.ClientID%>").value != "") {
                        document.getElementById("<%=Txt_Haber_Partida.ClientID%>").disabled = false;
                        document.getElementById("<%=Txt_Haber_Partida.ClientID%>").value = "";
                    }
                    else {
                        document.getElementById("<%=Txt_Haber_Partida.ClientID%>").disabled = true;
                    }
                }
                function Limpiar_Carga() {

                }
                function Limpiar_Autorizar_Password() {
                    document.getElementById("<%=Txt_No_Empleado_Popup.ClientID%>").value = "";
                    document.getElementById("<%=Txt_Password_Popup.ClientID%>").value = "";
                }
                function Abrir_Modal_Popup() {
                    Limpiar_Ctlr();
                    $find('Busqueda_Polizas').show();
                    return false;
                }
                //        function Abrir_Carga_PopUp() 
                //        {
                //            Limpiar_Carga();
                //            $find('Carga_Masiva').show();
                //            return false;
                //        }
                function Abrir_Autorizar_Password() {
                    Limpiar_Autorizar_Password();
                    $find('Autorizar_Password').show();
                    return false;
                }
                function Limpiar_Ctlr_2() {
                    document.getElementById("<%=Txt_Busqueda_Cuenta.ClientID%>").value = "";
                    return false;
                }
                function Abrir_Modal_Popup_2() {
                    $find('Busqueda_Cuenta').show();
                    return false;
                }
                function pageLoad() { $('[id*=Txt_Comen').keyup(function() { var Caracteres = $(this).val().length; if (Caracteres > 250) { this.value = this.value.substring(0, 250); $(this).css("background-color", "Yellow"); $(this).css("color", "Red"); } else { $(this).css("background-color", "White"); $(this).css("color", "Black"); } $('#Contador_Caracteres_Comentarios').text('Car�cteres Ingresados [ ' + Caracteres + ' ]/[ 250 ]'); }); }
            }
    </script>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <%--*************BUSQUEDA****************************************
    *************************************************************--%>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
    style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
    <asp:Panel ID="Pnl_Busqueda_Cabecera" runat="server" 
        style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
        <table width="99%">
            <tr>
                <td style="color:Black;font-size:12;font-weight:bold;">
                   <asp:Image ID="Img_Informatcion_Autorizacion" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                     B&uacute;squeda: Polizas
                </td>
                <td align="right" style="width:10%;">
                   <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                        ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Ventana_Click"/>  
                </td>
            </tr>
        </table>            
    </asp:Panel>                                                                          
           <div style="color: #5D7B9D">
             <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Polizas" runat="server" UpdateMode="Conditional" >                                                           
                            <ContentTemplate> 
                         <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Polizas" DisplayAfter="0" >
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>                                                           
                                  <table width="100%">
                                   <tr>
                                    <td colspan="4">
                                        <table style="width:80%;">
                                            <tr>
                                                <td align="left">
                                                    <asp:ImageButton ID="Img_Error_Busqueda" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                        Width="24px" Height="24px" Visible=false />
                                                    <asp:Label ID="Lbl_Error_Busqueda" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" Visible="false" />
                                                </td>            
                                            </tr>         
                                        </table>  
                                    </td>
                                   </tr>
                                   <tr>
                                        <td style="width:100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>                         
                                        </td>
                                  </tr>     
                                   <tr>
                                        <td style="width:100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">
                                           Numero de P&oacute;liza:
                                        </td>
                                        <td style="width:30%;text-align:left;font-size:11px;">
                                           <asp:TextBox ID="Txt_No_Poliza_PopUp" runat="server" Width="98%" MaxLength="5"/>
                                           <cc1:FilteredTextBoxExtender ID="Fte_Txt_No_Poliza_PopUp" runat="server" FilterType="Numbers"
                                                TargetControlID="Txt_No_Poliza_PopUp"/>  
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Txt_No_Poliza_PopUp" runat="server" 
                                                TargetControlID ="Txt_No_Poliza_PopUp" WatermarkText="Busqueda por No. de Poliza" 
                                                WatermarkCssClass="watermarked"/>                                                                                                                                    
                                        </td> 
                                        <td style="width:20%;text-align:left;font-size:11px;">
                                           Tipo de P&oacute;liza 
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Tipo_Poliza" runat="server" Width="100%">   
                                            </asp:DropDownList>                                          
                                        </td>             
                                    </tr>                                                                                                   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">
                                           *Mes 
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Mes_Poliza" runat="server" Width="100%">   
                                                <asp:ListItem Value="">&lt;- Seleccione -&gt;</asp:ListItem>                                         
                                                <asp:ListItem Value="0">ENERO</asp:ListItem>
                                                <asp:ListItem Value="1">FEBRERO</asp:ListItem>
                                                <asp:ListItem Value="2">MARZO</asp:ListItem>
                                                <asp:ListItem Value="3">ABRIL</asp:ListItem>
                                                <asp:ListItem Value="4">MAYO</asp:ListItem>
                                                <asp:ListItem Value="5">JUNIO</asp:ListItem>
                                                <asp:ListItem Value="6">JULIO</asp:ListItem>
                                                <asp:ListItem Value="7">AGOSTO</asp:ListItem>
                                                <asp:ListItem Value="8">SEPTIEMBRE</asp:ListItem>
                                                <asp:ListItem Value="9">OCTUBRE</asp:ListItem>
                                                <asp:ListItem Value="10">NOVIEMBRE</asp:ListItem>
                                                <asp:ListItem Value="11">DICIEMBRE</asp:ListItem>
                                                <asp:ListItem Value="12">MES_13</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width:20%;text-align:left;font-size:11px;">
                                            *A�o
                                        </td>              
                                        <td style="width:30%;text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Anio_Poliza" runat="server" Width="100%">   
                                                <asp:ListItem Value="">&lt;- Seleccione -&gt;</asp:ListItem>                                         
                                                <asp:ListItem>2010</asp:ListItem>
                                                <asp:ListItem>2011</asp:ListItem>
                                                <asp:ListItem>2012</asp:ListItem>
                                                <asp:ListItem>2013</asp:ListItem>
                                                <asp:ListItem>2014</asp:ListItem>
                                                <asp:ListItem>2015</asp:ListItem>
                                                <asp:ListItem>2016</asp:ListItem>
                                                <asp:ListItem>2017</asp:ListItem>
                                                <asp:ListItem>2018</asp:ListItem>
                                                <asp:ListItem>2019</asp:ListItem>
                                                <asp:ListItem>2020</asp:ListItem>
                                                <asp:ListItem>2021</asp:ListItem>
                                                <asp:ListItem>2022</asp:ListItem>
                                                <asp:ListItem>2023</asp:ListItem>
                                                <asp:ListItem>2024</asp:ListItem>
                                                <asp:ListItem>2025</asp:ListItem>
                                                <asp:ListItem>2026</asp:ListItem>
                                                <asp:ListItem>2027</asp:ListItem>
                                                <asp:ListItem>2028</asp:ListItem>
                                                <asp:ListItem>2029</asp:ListItem>
                                                <asp:ListItem>2030</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                   <tr>
                                        <td style="width:100%" colspan ="4">
                                        <div id="Div1" runat="server" style="overflow:auto; max-height:200px; width:99%">
                                            <table style="width:99%">
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="Grid_Polizas" runat="server" CssClass="GridView_1" 
                                                            AutoGenerateColumns="False" GridLines="None" Width="99.9%"
                                                            onpageindexchanging="Grid_Polizas_PageIndexChanging"  
                                                            onselectedindexchanged="Grid_Polizas_SelectedIndexChanged"
                                                            AllowSorting="True" HeaderStyle-CssClass="tblHead">
                                                            <Columns>         
                                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                                    <ItemStyle Width="7%" />
                                                                </asp:ButtonField>                       
                                                                <asp:BoundField DataField="No_Poliza" HeaderText="No Poliza" 
                                                                     SortExpression="No_Poliza">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Tipo_Poliza_ID" HeaderText="Tipo Poliza" SortExpression="Tipo_Poliza">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                                </asp:BoundField>
                                                                 <asp:BoundField DataField="Descripcion" HeaderText="Tipo Poliza" SortExpression="Tipo_Poliza">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Fecha_Poliza" HeaderText="Fecha" 
                                                                    SortExpression="Fecha_Poliza" DataFormatString="{0:dd/MMM/yyyy}">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Concepto" HeaderText="Concepto" 
                                                                    Visible="True" SortExpression="Concepto">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                                                    <ItemStyle HorizontalAlign="left" Width="40%" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Mes_Ano" HeaderText="Mes_Ano" 
                                                                    Visible="True" SortExpression="Concepto">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                    <ItemStyle HorizontalAlign="left" Width="10%" />
                                                                </asp:BoundField>                                    
                                                            </Columns>
                                                            <SelectedRowStyle CssClass="GridSelected" />
                                                            <PagerStyle CssClass="GridHeader" />
                                                            <HeaderStyle CssClass="tblHead" />
                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                               <asp:Button ID="Btn_Busqueda_Poliza_Popup" runat="server"  Text="Busqueda de Polizas" CssClass="button"  
                                                CausesValidation="false" OnClick="Btn_Busqueda_Poliza_Popup_Click" Width="200px"/> 
                                            </center>
                                        </td>                                                     
                                    </tr>                                                                        
                                  </table>                                                                                                                                                                 
                            </ContentTemplate>   
                            <Triggers>
                                 <asp:AsyncPostBackTrigger  ControlID="Btn_Busqueda_Poliza_Popup"  EventName="Click" />
                            </Triggers>                                                         
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>                                                      
                    </td>
                </tr>
             </table>                                                   
           </div>                 
    </asp:Panel>
   <%--*************PASSWORD****************************************
    *************************************************************--%>
    <asp:Panel ID="Pnl_Password" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="400px" 
        style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
        <asp:Panel ID="Pnl_Password_Cabecera" runat="server" 
            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
            <table width="99%">
                <tr>
                    <td style="color:Black;font-size:12;font-weight:bold;">
                       <asp:Image ID="Image2" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                         Autorizaci&oacute;n del Administrador
                    </td>
                    <td align="right" style="width:10%;">
                       <asp:ImageButton ID="ImageButton2" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Ventana_Password_Click"/>  
                    </td>
                </tr>
                <tr>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always" RenderMode="Inline">
                        <ContentTemplate>                         
                            <asp:Image ID="Img_Error_Password" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Error_Password" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"/>
                        </ContentTemplate>                                
                    </asp:UpdatePanel>
                </tr>
            </table>            
        </asp:Panel>                                                                          
        <div style="color: #5D7B9D">
             <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Password" runat="server">
                            <ContentTemplate>                            
                                <asp:UpdateProgress ID="Progress_Upnl_Password" runat="server" AssociatedUpdatePanelID="Upnl_Password" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>                 
                                <table width="100%">
                                    <tr>
                                        <td width="40%" visible="false"></td>
                                        <td width="60%" visible="false"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%" colspan="3">
                                            <hr />
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td>*Clave de Empleado</td>
                                        <td>
                                            <asp:TextBox ID="Txt_No_Empleado_Popup" runat="server" Width="98%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="Txt_No_Empleado_Popup" FilterType="Custom" ValidChars="1234567890"></cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>                                                              
                                    <tr>
                                        <td>*Contrase&ntilde;a</td>
                                        <td>
                                            <asp:TextBox ID="Txt_Password_Popup" runat="server" Width="98%" TextMode="Password"></asp:TextBox>
                                        </td>
                                    </tr>                                   
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="2">
                                            <center>
                                                <asp:Button ID="Btn_Autorizar_Poliza_Popup" runat="server"  Text="Capturar Polizas" CssClass="button"  
                                                CausesValidation="false" OnClick="Btn_Autorizar_Poliza_Popup_Click" Width="200px"/> 
                                            </center>
                                        </td>                                                     
                                    </tr>                                                                        
                                  </table>                                                                                                                                                              
                            </ContentTemplate>  
                            <Triggers>
                                <asp:AsyncPostBackTrigger EventName="Click" ControlID ="Btn_Autorizar_Poliza_Popup" />
                            </Triggers>                                                              
                        </asp:UpdatePanel>
                    </td>
                </tr>
             </table>                                                   
        </div>                 
    </asp:Panel>
     <%--*************BUSQUEDA CUENTA****************************************
    *************************************************************--%>
    <asp:Panel ID="Pnl_Busqueda_Cuenta" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
        style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
        <asp:Panel ID="Pnl_Cabecera" runat="server" 
            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
            <table width="99%">
                <tr>
                    <td style="color:Black;font-size:12;font-weight:bold;">
                       <asp:Image ID="Image1" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                         B&uacute;squeda: Cuentas Contables
                    </td>
                    <td align="right" style="width:10%;">
                       <asp:ImageButton ID="Btn_Cerrar_Ven" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Ven_Click"/>  
                    </td>
                </tr>
            </table>            
        </asp:Panel>                                                                          
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Cuenta" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Cuenta" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Cuenta" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>                                                             
                                <table width="100%">
                                 <tr>
                                    <td colspan="4">
                                        <table style="width:80%;">
                                            <tr>
                                                <td align="left">
                                                    <asp:ImageButton ID="Img_Error_Busqueda_Cuenta" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                        Width="24px" Height="24px" Visible=false />
                                                    <asp:Label ID="Lbl_Error_Busqueda_Cuenta" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" Visible=false />
                                                </td>            
                                            </tr>         
                                        </table>  
                                    </td>
                                   </tr>
                                    <tr>
                                        <td style="width:100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_2();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>                         
                                        </td>
                                    </tr>     
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">Cuenta</td><td colspan="3" style="text-align:left;font-size:11px;">
                                            <asp:TextBox ID="Txt_Busqueda_Cuenta" runat="server" Width="98%" TabIndex="11" MaxLength="100"/> 
                                             <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Cuenta"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="������������. ">
                                        </cc1:FilteredTextBoxExtender>
                                            
                                        </td> 
                                        <td colspan="2" style="width:50%;text-align:left;font-size:11px;"></td>                     
                                    </tr>                                                                                                  
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>                                    
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Cuenta_Contable" runat="server"  Text="Busqueda de Cuenta" CssClass="button"  
                                                    CausesValidation="false" Width="300px" TabIndex="15" OnClick="Btn_Busqueda_Cuenta_Popup_Click" />
                                            </center>
                                        </td>                                                     
                                    </tr>
                                    <tr>
                                        <td style="width:100%" colspan ="4">
                                            <div id="Div_Busqueda" runat="server" style="overflow:auto; max-height:200px; width:99%">
                                            <table style="width:100%" >
                                                <tr> 
                                                    <td>
                                                        <asp:GridView ID="Grid_Cuentas" runat="server" CssClass="GridView_1"  AutoGenerateColumns="False" GridLines="None" Width="99.9%"
                                                            AllowSorting="True" HeaderStyle-CssClass="tblHead">
                                                            <Columns>
                                                               <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="Btn_Seleccionar_Cuenta" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                             CommandArgument='<%# Eval("Cuenta_Contable_ID") %>' OnClick="Btn_Seleccionar_Solicitud_Click" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                                    <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Cuenta_Contable_ID" HeaderText="Cuenta_ID">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CUENTA" HeaderText="No. Cuenta" SortExpression="CUENTA">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                </asp:BoundField>
                                                                 <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" SortExpression="Descripcion">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="60%" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <SelectedRowStyle CssClass="GridSelected" />
                                                            <PagerStyle CssClass="GridHeader" />
                                                            <HeaderStyle CssClass="tblHead" />
                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                        </asp:GridView>
                                                    </td>
                                                 </tr>
                                            </table>
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>                                                       
                                </table>                                                                                                                                                              
                            </ContentTemplate>                                                                   
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>                                                   
        </div>
    </asp:Panel>

</asp:Content>

