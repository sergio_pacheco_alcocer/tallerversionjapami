﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Con_Libro_Mayor.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Libro_Mayor" Title="Libro de Mayor" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Abrir_Modal_Popup_2() {
            $find('Busqueda_Cuenta').show();
            return false;
        }
        function Abrir_Modal_Popup_Cuenta_Inicial() {
            $find('Busqueda_Cuenta_Inicial').show();
            return false;
        }
        function Abrir_Modal_Popup_Cuenta_Final() {
            $find('Busqueda_Cuenta_Final').show();
            return false;
        }
        function Limpiar_Ctlr_2() {
            document.getElementById("<%=Txt_Busqueda_Cuenta.ClientID%>").value = "";
            return false;
        }
        function Limpiar_Ctlr_Cuenta_Inicial() {
            document.getElementById("<%=Txt_Busqueda_Cuenta_Inicial.ClientID%>").value = "";
            return false;
        }
        function Limpiar_Ctlr_Cuenta_Final() {
            document.getElementById("<%=Txt_Busqueda_Cuenta_Final.ClientID%>").value = "";
            return false;
        }
        function Mostrar_Tabla(Renglon, Imagen) {
            object = document.getElementById(Renglon);
            if (object.style.display == "none") {
                object.style.display = "";
                document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
            } else {
                object.style.display = "none";
                document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server"  >
    <cc1:ToolkitScriptManager ID="ScriptManager_Areas" runat="server"  EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" >
        <ContentTemplate>        
            <asp:UpdateProgress ID="Uprg_Libro_Mayor" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
               <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Reporte_Libro_Mayor" style ="height:1000px">
                <table width="100%" >
                    <tr>
                        <td>
                            <table width="100%" class="estilo_fuente">
                                <tr align="center">
                                    <td class="label_titulo">Reporte Libro de Mayor</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                        <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                        <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                     </tr>
                     <tr>
                        <td>
                            <table width="98%"  border="0" cellspacing="0">
                                <tr align="center">
                                    <td colspan="2">                
                                        <div align="right" class="barra_busqueda">                        
                                            <table style="width:100%;height:28px;">
                                                <tr>
                                                    <td align="left" style="width:59%;">
                                                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                            CssClass="Img_Button" TabIndex="1"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                            onclick="Btn_Nuevo_Click" />
                                                        <asp:ImageButton ID="Btn_Reporte_Libro_Mayor" runat="server" ToolTip="Reporte" 
                                                            CssClass="Img_Button" TabIndex="1" Visible="false"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                                                            onclick="Btn_Reporte_Libro_Mayor_Click"/>
                                                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                            CssClass="Img_Button" TabIndex="2"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                            onclick="Btn_Salir_Click"/>
                                                    </td>
                                                  <td align="right" style="width:41%;">&nbsp;</td>       
                                                </tr>         
                                            </table>                      
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                     </tr>
                     <tr>
                        <td>
                            <table width="99%" class="estilo_fuente">
                                <tr>
                                    <td style="width:20%;text-align:left;">*Cuenta</td>
                                    <td style="width:30%;text-align:left;">
                                        <asp:TextBox ID="Txt_Cuenta" runat="server" Width="75%" MaxLength="9" 
                                            TabIndex="3" AutoPostBack="true" ontextchanged="Txt_Cuenta_TextChanged"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Cuenta" runat="server" TargetControlID="Txt_Cuenta" 
                                            FilterType="Custom, Numbers"/>                              
                                        <asp:UpdatePanel ID="Udp_Busqueda_Modal" runat="server" UpdateMode="Conditional" RenderMode="Inline"  >
                                            <ContentTemplate> 
                                                    <asp:ImageButton ID="Btn_Mostrar_Busqueda" runat="server" 
                                                    ToolTip="Busqueda Avanzada" TabIndex="1"
                                                    ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Height="15px" Width="15px"
                                                    OnClientClick="javascript:return Abrir_Modal_Popup_2();" CausesValidation="false" />
                                                    <cc1:ModalPopupExtender  ID="Mpe_Busqueda_Cuenta" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Busqueda_Cuenta"
                                                     PopupControlID="Pnl_Busqueda_Cuenta" TargetControlID="Btn_Open_2" 
                                                    CancelControlID="Btn_Close" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                                    <asp:Button Style="background-color: transparent; border-style:none; width:.5px;" 
                                                    ID="Btn_Close" runat="server" Text="" />
                                                    <asp:Button  Style="background-color: transparent; border-style:none; width:.5px;" 
                                                    ID="Btn_Open_2" runat="server" Text="" />                                                                                                    
                                             </ContentTemplate>
                                             </asp:UpdatePanel>
                                    </td>
                                    <td colspan="2" style="width:50%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Cuenta_Inicial" runat="server" Width="90%"  
                                            AutoPostBack ="true" TabIndex="4" 
                                            onselectedindexchanged="Cmb_Cuenta_Inicial_SelectedIndexChanged"/>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="4">
                                         <asp:Panel ID="Pnl_Rango_Fechas" runat="server" GroupingText="Rango de Cuentas" Width="99.9%" BackColor="White">
                                             <table  width ="98%" class="estilo_fuente">
                                                <tr>
                                                       <td style="width:20%;text-align:left;">Cuenta Inicial</td>
                                                        <td style="width:30%;text-align:left;">
                                                            <asp:TextBox ID="Txt_Cuenta_Inicial" runat="server" Width="80%" MaxLength="9" 
                                                                TabIndex="6" ontextchanged="Txt_Cuenta_Inicial_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Cuenta_Inicial" runat="server" TargetControlID="Txt_Cuenta_Inicial" 
                                                                FilterType="Custom, Numbers" />
                                                                 <asp:UpdatePanel ID="Udp_Busqueda_Cuenta_Inicial" runat="server" UpdateMode="Conditional" RenderMode="Inline"  >
                                                                 <ContentTemplate> 
                                                                            <asp:ImageButton ID="Btn_Mostrar_Busqueda_Cuneta_Inicial" runat="server" 
                                                                            ToolTip="Busqueda Avanzada" TabIndex="5"
                                                                            ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Height="15px" Width="15px"
                                                                            OnClientClick="javascript:return Abrir_Modal_Popup_Cuenta_Inicial();" CausesValidation="false" />
                                                                            <cc1:ModalPopupExtender  ID="Mpe_Busqueda_Cuenta_Inicial" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Busqueda_Cuenta_Inicial"
                                                                             PopupControlID="Pnl_Busqueda_Cuenta_Inicial" TargetControlID="Btn_Abrir_Inicial" 
                                                                            CancelControlID="Btn_Cerrar_Inicial" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                                                            <asp:Button Style="background-color: transparent; border-style:none; width:.5px;" 
                                                                            ID="Btn_Cerrar_Inicial" runat="server" Text="" />
                                                                            <asp:Button  Style="background-color: transparent; border-style:none; width:.5px;" 
                                                                            ID="Btn_Abrir_Inicial" runat="server" Text="" />                                                                                                    
                                                                  </ContentTemplate>
                                                                 </asp:UpdatePanel>
                                                            </td>
                                                        <td style="width:50%;text-align:left;">
                                                            <asp:DropDownList ID="Cmb_Cuenta_Contable_Inicial_Rango" runat="server" Width="90%"  AutoPostBack ="true" TabIndex="8" 
                                                                onselectedindexchanged="Cmb_Cuenta_Inicial_Rango_SelectedIndexChanged"/>
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:20%;text-align:left;">Cuenta Final</td>
                                                    <td style="width:30%;text-align:left;">
                                                        <asp:TextBox ID="Txt_Cuenta_Final" runat="server" Width="80%" MaxLength="9" 
                                                            TabIndex="9" ontextchanged="Txt_Cuenta_Final_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Cuenta_Final" runat="server" TargetControlID="Txt_Cuenta_Final" 
                                                            FilterType="Custom, Numbers" />
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" RenderMode="Inline"  >
                                                            <ContentTemplate> 
                                                                    <asp:ImageButton ID="Btn_Mostrar_Busqueda_Cuneta_Final" runat="server" 
                                                                    ToolTip="Busqueda Avanzada" TabIndex="5"
                                                                    ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Height="15px" Width="15px"
                                                                    OnClientClick="javascript:return Abrir_Modal_Popup_Cuenta_Final();" CausesValidation="false" />
                                                                    <cc1:ModalPopupExtender  ID="Mpe_Busqueda_Cuenta_Final" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Busqueda_Cuenta_Final"
                                                                     PopupControlID="Pnl_Busqueda_Cuenta_Final" TargetControlID="Btn_Abrir_Final" 
                                                                    CancelControlID="Btn_Cerrar_Final" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                                                    <asp:Button Style="background-color: transparent; border-style:none; width:.5px;" 
                                                                    ID="Btn_Cerrar_Final" runat="server" Text="" />
                                                                    <asp:Button  Style="background-color: transparent; border-style:none; width:.5px;" 
                                                                    ID="Btn_Abrir_Final" runat="server" Text="" />                                                                                                    
                                                             </ContentTemplate>
                                                         </asp:UpdatePanel>
                                                    </td>
                                                    <td style="width:50%;text-align:left;">
                                                        <asp:DropDownList ID="Cmb_Cuenta_Contable_Final" runat="server" Width="90%"  AutoPostBack ="true" TabIndex="11" 
                                                            onselectedindexchanged="Cmb_Cuenta_Contable_Final_SelectedIndexChanged"/>
                                                    </td>
                                                </tr>
                                             </table>
                                         </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%>
                                            <tr>
                                                <td style="width:20%">
                                                    <asp:Label ID="Lbl_Fecha_Inicio" runat="server" Text="*Fecha Inicio"></asp:Label>
                                                </td>
                                                <td style="width:30%">
                                                    <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="18px" />
                                                    <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Inicio" runat="server" 
                                                        TargetControlID="Txt_Fecha_Inicio" WatermarkCssClass="watermarked" 
                                                        WatermarkText="Dia/Mes/Año" Enabled="True" />
                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicio" runat="server" 
                                                        TargetControlID="Txt_Fecha_Inicio" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_inicio"/>
                                                     <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server"
                                                        ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                                        Height="18px" CausesValidation="false"/>           
                                                    <cc1:MaskedEditExtender 
                                                        ID="Mee_Txt_Fecha_Inicio" 
                                                        Mask="99/LLL/9999" 
                                                        runat="server"
                                                        MaskType="None" 
                                                        UserDateFormat="DayMonthYear" 
                                                        UserTimeFormat="None" Filtered="/"
                                                        TargetControlID="Txt_Fecha_Inicio" 
                                                        Enabled="True" 
                                                        ClearMaskOnLostFocus="false"/>  
                                                    <cc1:MaskedEditValidator 
                                                        ID="Mev_Txt_Fecha_Poliza" 
                                                        runat="server" 
                                                        ControlToValidate="Txt_Fecha_Inicio"
                                                        ControlExtender="Mee_Txt_Fecha_Inicio" 
                                                        EmptyValueMessage="Fecha Requerida"
                                                        InvalidValueMessage="Fecha Inicio Invalida" 
                                                        IsValidEmpty="false" 
                                                        TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                                        Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                                    </td>
                                                    <td style="width:20%">
                                                        <asp:Label ID="Lbl_Fecha_Final" runat="server" Text="*Fecha Final"></asp:Label>
                                                    </td>
                                                    <td style="width:30%">
                                                        <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="18px" />
                                                    <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Final" runat="server" 
                                                        TargetControlID="Txt_Fecha_Final" WatermarkCssClass="watermarked" 
                                                        WatermarkText="Dia/Mes/Año" Enabled="True" />
                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Final" runat="server" 
                                                        TargetControlID="Txt_Fecha_Final" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Final"/>
                                                     <asp:ImageButton ID="Btn_Fecha_Final" runat="server"
                                                        ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                                        Height="18px" CausesValidation="false"/>           
                                                    <cc1:MaskedEditExtender 
                                                        ID="Mee_Txt_Fecha_Final" 
                                                        Mask="99/LLL/9999" 
                                                        runat="server"
                                                        MaskType="None" 
                                                        UserDateFormat="DayMonthYear" 
                                                        UserTimeFormat="None" Filtered="/"
                                                        TargetControlID="Txt_Fecha_Final" 
                                                        Enabled="True" 
                                                        ClearMaskOnLostFocus="false"/>  
                                                    <cc1:MaskedEditValidator 
                                                        ID="MaskedEditValidator1" 
                                                        runat="server" 
                                                        ControlToValidate="Txt_Fecha_Final"
                                                        ControlExtender="Mee_Txt_Fecha_Final" 
                                                        EmptyValueMessage="Fecha Requerida"
                                                        InvalidValueMessage="Fecha Final Invalida" 
                                                        IsValidEmpty="false" 
                                                        TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                                        Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                 <tr>
                                    <td runat="server" id="Tr_Grid_Libro" colspan="4" style="display:none; width="100%" >
                                           <div>
                                                <table width="100%"  border="0" cellspacing="0">
                                                    <tr >
                                                        <td Font-Size="XX-Small"  style=" width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="Center">Cuenta</td>
                                                        <td Font-Size="XX-Small" style="width:85%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Descripcion</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="overflow:auto;height:500px;width:99%;vertical-align:top;border-style:outset;border-color:Silver; position:static" >
                                                <asp:GridView ID="Grid_Cuentas_Movimientos" runat="server" AllowPaging="False"  ShowHeader="false"
                                                    AutoGenerateColumns="False" CssClass="GridView_Finanzas" OnRowDataBound="Grid_Cuentas_RowDataBound"
                                                    DataKeyNames="Cuenta_ID" GridLines="None" Width="99%">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Image ID="Img_Btn_Expandir" runat="server"
                                                                    ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="2%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Cuenta" HeaderText="Cuenta">
                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion">
                                                            <HeaderStyle HorizontalAlign="Left" Width="80%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="80%" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Lbl_Movimientos" runat="server" 
                                                                    Text='<%# Bind("Cuenta_ID") %>' Visible="false"></asp:Label>
                                                                <asp:Literal ID="Ltr_Inicio" runat="server" 
                                                                    Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' &gt;&lt;td colspan='11'; width='100%';&gt;"  />
                                                                <asp:GridView ID="Grid_Movimientos" runat="server" AllowPaging="False" 
                                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="100%">
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="Fecha" DataField="FECHA"  DataFormatString="{0:dd/MMM/yyyy}">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                            <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="8%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto" >
                                                                            <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                                            <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="30%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="SALDO_INICIAL"  HeaderText="Saldo Inicial" DataFormatString="{0:C}">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="10%" />
                                                                            <ItemStyle Font-Size="X-Small"  HorizontalAlign="Right"  Width="10%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DEBE"  HeaderText="Debe" DataFormatString="{0:C}">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="10%" />
                                                                            <ItemStyle Font-Size="X-Small"  HorizontalAlign="Right"  Width="10%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="HABER"  HeaderText="Haber" DataFormatString="{0:C}" >
                                                                            <HeaderStyle HorizontalAlign="Right" Width="9%" />
                                                                            <ItemStyle Font-Size="X-Small"  HorizontalAlign="Right" Width="10%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="SALDO"  HeaderText="Saldo final" DataFormatString="{0:C}">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="10%" />
                                                                            <ItemStyle Font-Size="X-Small"  HorizontalAlign="Right"  Width="10%" />
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField  Visible="True" HeaderText="Poliza">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton Font-Size="X-Small"  ID="Btn_Seleccionar_Poliza" runat="server" Text= '<%# Eval("No_Poliza") %>'                                             
                                                                                    OnClick="Btn_Poliza_Click" CommandArgument='<%# Eval("MES_ANO") %>' CssClass='<%# Eval("Tipo_Poliza_ID") %>' ForeColor="Blue"  />
                                                                                </ItemTemplate >
                                                                                <HeaderStyle HorizontalAlign="right" />
                                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Center"  Width="10%"/>
                                                                            </asp:TemplateField>
                                                                    </Columns>
                                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                                    <FooterStyle CssClass="GridPager" />
                                                                    <HeaderStyle CssClass="GridHeader_Nested" />
                                                                    <PagerStyle CssClass="GridPager" />
                                                                    <RowStyle CssClass="GridItem" />
                                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                                </asp:GridView>
                                                                <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="GridAltItem_Finanzas" />
                                                    <FooterStyle CssClass="GridPager" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <PagerStyle CssClass="GridPager" />
                                                    <RowStyle CssClass="GridItem" />
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                </asp:GridView>
                                            </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
         <%--*************BUSQUEDA CUENTA****************************************
    *************************************************************--%>
    <asp:Panel ID="Pnl_Busqueda_Cuenta" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
        style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
        <asp:Panel ID="Pnl_Cabecera" runat="server" 
            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
            <table width="99%">
                <tr>
                    <td style="color:Black;font-size:12;font-weight:bold;">
                       <asp:Image ID="Image1" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                         B&uacute;squeda: Cuentas Contables
                    </td>
                    <td align="right" style="width:10%;">
                       <asp:ImageButton ID="Btn_Cerrar_Ven" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Ven_Click"/>  
                    </td>
                </tr>
            </table>            
        </asp:Panel>                                                                          
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Cuenta" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Cuenta" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Cuenta" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>                                                             
                                <table width="100%">
                                 <tr>
                                        <td colspan="4">

                                            <table style="width:80%;">

                                              <tr>

                                                <td align="left">

                                                  <asp:ImageButton ID="Img_Error_Busqueda_Cuenta" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 

                                                    Width="24px" Height="24px" Visible=false />

                                                    <asp:Label ID="Lbl_Error_Busqueda_Cuenta" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" Visible=false />

                                                </td>            

                                              </tr>         

                                            </table>  

                                        </td>
                                   </tr>
                                    <tr>
                                        <td style="width:100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_2();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>                         
                                        </td>
                                    </tr>     
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">Cuenta</td><td colspan="3" style="text-align:left;font-size:11px;">
                                            <asp:TextBox ID="Txt_Busqueda_Cuenta" runat="server" Width="98%" TabIndex="11" MaxLength="100"/> 
                                             <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Cuenta"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                            
                                        </td> 
                                        <td colspan="2" style="width:50%;text-align:left;font-size:11px;"></td>                     
                                    </tr>                                                                                                  
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>                                    
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Cuenta_Contable" runat="server"  Text="Busqueda de Cuenta" CssClass="button"  
                                                    CausesValidation="false" Width="300px" TabIndex="15" OnClick="Btn_Busqueda_Cuenta_Inicial_Popup_Click"  CommandArgument="1"/>
                                            </center>
                                        </td>                                                     
                                    </tr>
                                    <tr>
                                        <td style="width:100%" colspan ="4">
                                            <div id="Div_Busqueda" runat="server" style="overflow:auto; max-height:200px; width:99%">
                                            <table style="width:100%" >
                                                <tr> 
                                                    <td>
                                                    <asp:GridView ID="Grid_Cuentas" runat="server" CssClass="GridView_1"  AutoGenerateColumns="False" GridLines="None" Width="99.9%"
                                                    AllowSorting="True" HeaderStyle-CssClass="tblHead">
                                                    <Columns>
                                                       <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="Btn_Seleccionar_Cuenta" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                     CommandArgument='<%# Eval("Cuenta_Contable_ID") %>' OnClick="Btn_Seleccionar_Solicitud_Click"/>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Cuenta_Contable_ID" HeaderText="Cuenta_ID">
                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CUENTA" HeaderText="No. Cuenta" SortExpression="CUENTA">
                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                        </asp:BoundField>
                                                         <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" SortExpression="Descripcion">
                                                            <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="60%" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="tblHead" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>
                                                    </td>
                                                 </tr>
                                            </table>
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>                                                       
                                </table>                                                                                                                                                              
                            </ContentTemplate>                                                                   
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>                                                   
        </div>
    </asp:Panel>
             <%--*************BUSQUEDA CUENTA INICIAL****************************************
    *************************************************************--%>
    <asp:Panel ID="Pnl_Busqueda_Cuenta_Inicial" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
        style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
        <asp:Panel ID="Pnl_Cabecera_Inicial" runat="server" 
            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
            <table width="99%">
                <tr>
                    <td style="color:Black;font-size:12;font-weight:bold;">
                       <asp:Image ID="Image2" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                         B&uacute;squeda: Cuentas Contables
                    </td>
                    <td align="right" style="width:10%;">
                       <asp:ImageButton ID="Btn_Cerrar_Ven_Inicial" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Ven_Inicial_Click"/>  
                    </td>
                </tr>
            </table>            
        </asp:Panel>                                                                          
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Cuenta_Inicial" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Cuenta_inicial" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Cuenta_Inicial" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress_Inicial">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>                                                             
                                <table width="100%">
                                 <tr>
                                        <td colspan="4">
                                            <table style="width:80%;">
                                              <tr>
                                                <td align="left">
                                                  <asp:ImageButton ID="Img_Error_Busqueda_Cuenta_inicial" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                    Width="24px" Height="24px" Visible=false />
                                                  <asp:Label ID="Lbl_Error_Busqueda_Cuenta_Inicial" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" Visible=false />
                                                </td>            
                                              </tr>         
                                            </table>  
                                        </td>
                                   </tr>
                                    <tr>
                                        <td style="width:100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_Inicial" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_Cuenta_Inicial();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>                         
                                        </td>
                                    </tr>     
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">Cuenta</td><td colspan="3" style="text-align:left;font-size:11px;">
                                            <asp:TextBox ID="Txt_Busqueda_Cuenta_Inicial" runat="server" Width="98%" TabIndex="11" MaxLength="100"/> 
                                             <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Cuenta_Inicial" runat="server" TargetControlID="Txt_Busqueda_Cuenta_Inicial"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                        </td> 
                                        <td colspan="2" style="width:50%;text-align:left;font-size:11px;"></td>                     
                                    </tr>                                                                                                  
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>                                    
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Cuenta_Contable_Inicial" runat="server"  Text="Busqueda de Cuenta" CssClass="button"  
                                                    CausesValidation="false" Width="300px" TabIndex="15" OnClick="Btn_Busqueda_Cuenta_Inicial_Popup_Click"  CommandArgument="2"/>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%" colspan ="4">
                                            <div id="Div1" runat="server" style="overflow:auto; max-height:200px; width:99%">
                                            <table style="width:100%" >
                                                <tr> 
                                                    <td>
                                        <asp:GridView ID="Grid_Cuentas_Inicial" runat="server" CssClass="GridView_1"  AutoGenerateColumns="False" GridLines="None" Width="99.9%"
                                                    AllowSorting="True" HeaderStyle-CssClass="tblHead">
                                                    <Columns>
                                                       <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="Btn_Seleccionar_Cuenta" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                     CommandArgument='<%# Eval("Cuenta_Contable_ID") %>' OnClick="Btn_Seleccionar_Cuenta_Inicial_Click" />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Cuenta_Contable_ID" HeaderText="Cuenta_ID">
                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CUENTA" HeaderText="No. Cuenta" SortExpression="CUENTA">
                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                        </asp:BoundField>
                                                         <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" SortExpression="Descripcion">
                                                            <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="60%" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="tblHead" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>
                                                    </td>
                                                 </tr>
                                            </table>
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>                                                       
                                </table>                                                                                                                                                              
                            </ContentTemplate>                                                                   
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>                                                   
        </div>
    </asp:Panel>
             <%--*************BUSQUEDA CUENTA FINAL****************************************
    *************************************************************--%>
    <asp:Panel ID="Pnl_Busqueda_Cuenta_Final" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
        style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
        <asp:Panel ID="Pnl_Cabecera_Final" runat="server" 
            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
            <table width="99%">
                <tr>
                    <td style="color:Black;font-size:12;font-weight:bold;">
                       <asp:Image ID="Image3" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                         B&uacute;squeda: Cuentas Contables
                    </td>
                    <td align="right" style="width:10%;">
                       <asp:ImageButton ID="Btn_Cerrar_Ven_Final" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Ven_Final_Click"/>  
                    </td>
                </tr>
            </table>            
        </asp:Panel>                                                                          
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Cuenta_Final" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Cuenta_Final" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Cuenta_Final" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress_Final">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>                                                             
                                <table width="100%">
                                 <tr>
                                        <td colspan="4">
                                            <table style="width:80%;">
                                              <tr>
                                                <td align="left">
                                                  <asp:ImageButton ID="Img_Error_Busqueda_Cuenta_Final" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                    Width="24px" Height="24px" Visible=false />
                                                  <asp:Label ID="Lbl_Error_Busqueda_Cuenta_Final" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" Visible=false />
                                                </td>            
                                              </tr>         
                                            </table>  
                                        </td>
                                   </tr>
                                    <tr>
                                        <td style="width:100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_Final" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_Cuenta_Final();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>                         
                                        </td>
                                    </tr>     
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">Cuenta</td><td colspan="3" style="text-align:left;font-size:11px;">
                                            <asp:TextBox ID="Txt_Busqueda_Cuenta_Final" runat="server" Width="98%" TabIndex="11" MaxLength="100"/> 
                                             <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Cuenta_Final" runat="server" TargetControlID="Txt_Busqueda_Cuenta_Final"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                        </td> 
                                        <td colspan="2" style="width:50%;text-align:left;font-size:11px;"></td>                     
                                    </tr>                                                                                                  
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>                                    
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Cuenta_Contable_Final" runat="server"  Text="Busqueda de Cuenta" CssClass="button"  
                                                    CausesValidation="false" Width="300px" TabIndex="15" OnClick="Btn_Busqueda_Cuenta_Inicial_Popup_Click"  CommandArgument="3"/>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%" colspan ="4">
                                            <div id="Div2" runat="server" style="overflow:auto; max-height:200px; width:99%">
                                            <table style="width:100%" >
                                                <tr> 
                                                    <td>
                                        <asp:GridView ID="Grid_Cuentas_Final" runat="server" CssClass="GridView_1"  AutoGenerateColumns="False" GridLines="None" Width="99.9%"
                                                    AllowSorting="True" HeaderStyle-CssClass="tblHead">
                                                    <Columns>
                                                       <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="Btn_Seleccionar_Cuenta_Final" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                     CommandArgument='<%# Eval("Cuenta_Contable_ID") %>' OnClick="Btn_Seleccionar_Cuenta_Final_Click" />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Cuenta_Contable_ID" HeaderText="Cuenta_ID">
                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CUENTA" HeaderText="No. Cuenta" SortExpression="CUENTA">
                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                        </asp:BoundField>
                                                         <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" SortExpression="Descripcion">
                                                            <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="60%" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="tblHead" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>
                                                    </td>
                                                 </tr>
                                            </table>
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>                                                       
                                </table>                                                                                                                                                              
                            </ContentTemplate>                                                                   
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>                                                   
        </div>
    </asp:Panel>
</asp:Content>

