﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Deudores.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Deudores" Title="Deudor" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
<script type="text/javascript" language="javascript">
    function Abrir_Modal_Popup_2() {
        $find('Busqueda_Deudor').show();
        return false;
    }
    function Limpiar_Ctlr_2() {
        document.getElementById("<%=Txt_Busqueda_Deudor.ClientID%>").value = "";
        return false;
    }
    function Contar_Caracteres() {
        $('textarea[id$=Txt_Concepto]').keyup(function() {
            var Caracteres = $(this).val().length;

            if (Caracteres > 250) {
                this.value = this.value.substring(0, 250);
                $(this).css("background-color", "Yellow");
                $(this).css("color", "Red");
            } else {
                $(this).css("background-color", "White");
                $(this).css("color", "Black");
            }

            $('#Contador_Caracteres_Concepto').text('Carácteres Ingresados [ ' + Caracteres + ' ]/[ 250 ]');
        });
    }
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:UpdateProgress ID="Uprg_Reporte" runat="server" 
        AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
        <ProgressTemplate>
           <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
            <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <cc1:ToolkitScriptManager ID="ScriptManager_Polizas" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" />
    <%--<asp:ScriptManager ID="ScriptManager_Deudores" runat="server" />--%>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <div id="Div_Deudor" style="height:500px;" >
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Solicitud de Gastos Por Comprobar</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>               
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" 
                                TabIndex="1" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                onclick="Btn_Nuevo_Click" />
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" 
                                TabIndex="2" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                                onclick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Imprimir" runat="server" ToolTip="Imprimir" CssClass="Img_Button" 
                                TabIndex="3" ImageUrl="~/paginas/imagenes/gridview/grid_print.png"  Visible="false"
                                onclick="Btn_Imprimir_Click" />
                            <%--<asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" CssClass="Img_Button" 
                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                OnClientClick="return confirm('¿Está seguro de eliminar el Tipo de Poliza seleccionado?');" 
                                onclick="Btn_Eliminar_Click" />--%>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="4" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                onclick="Btn_Salir_Click" />
                        </td>
                        <td style="width:50%">B&uacute;squeda
                            <asp:TextBox ID="Txt_Busqueda_Deudores" runat="server" MaxLength="10" TabIndex="5" ToolTip="Buscar por No. Solicitud"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Deudores" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Ingrese Descripción>" TargetControlID="Txt_Busqueda_Deudores" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Deudores" runat="server" 
                                TargetControlID="Txt_Busqueda_Deudores" FilterType="Numbers" >
                            </cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="Btn_Buscar_Descripcion_Deudores" runat="server" 
                                ToolTip="Consultar" TabIndex="6" 
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" onclick="Btn_Buscar_Descripcion_Deudores_Click" />
                        </td> 
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                <tr>
                    <td colspan="4" style="width:99%;">
                         <div id="Div_Presentacion_Deudores" runat="server" style=" display:block;">
                             <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                                    <tr>
                                        <td colspan="4" style="width:99%">&nbsp;</td>
                                    </tr>
                                     <tr>
                                        <td colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr align="center">
                                        <td colspan="4">
                                            <div style="overflow:auto;height:300px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;" >
                                                <asp:GridView ID="Grid_Deudores_Diversos" runat="server"  EmptyDataText="En este momento no se tienen pagos pendientes por autorizar"
                                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None">
                                                    <Columns>
                                                       <asp:TemplateField HeaderText="Selecciona">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="Btn_Seleccionar" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                     CommandArgument='<%# Eval("NO_DEUDA") %>' OnClick="Btn_Seleccionar_Click" />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="NO_DEUDA" HeaderText="Deuda" Visible="True" SortExpression="NO_DEUDA">
                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="NOMBRE" HeaderText="Deudor" Visible="True" SortExpression="Deudor">
                                                            <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                        </asp:BoundField>                                                        
                                                        <asp:BoundField DataField="TIPO_MOVIMIENTO" HeaderText="TIPO" Visible="True" SortExpression="TIPO">
                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="IMPORTE" HeaderText="Importe" Visible="True" DataFormatString="{0:c}" SortExpression="Importe">
                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                        </asp:BoundField>
                                                         <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" Visible="True" SortExpression="Importe">
                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>   
                                            </div>
                                        </td>
                                    </tr>
                              </table>
                         </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="width:99%;">
                        <div id="Div_Datos_Deudores" runat="server" style=" display:none;">
                            <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                                    <tr>
                                        <td colspan="4" style="width:99%">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%">No.Deuda</td>
                                        <td style="width:25%">
                                            <asp:TextBox ID="Txt_No_Movimiento" runat="server" ReadOnly="True" Width="80%"></asp:TextBox>
                                        </td>
                                         <td style="width:15%">
                                            <asp:Label ID="Lbl_Estatus" runat="server" Text="*Estatus"></asp:Label>
                                        </td>
                                        <td style="width:45%">
                                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="75%">
                                            <asp:ListItem value="0">&larr;SELECCIONE&rarr;</asp:ListItem>
                                            <asp:ListItem Value="1">PENDIENTE</asp:ListItem>
                                            <asp:ListItem Value="2">AUTORIZADO</asp:ListItem>
                                            <asp:ListItem Value="3">CANCELADO</asp:ListItem>
                                            <asp:ListItem Value="5">PORPAGAR</asp:ListItem>
                                            <asp:ListItem Value="6">TERMINADO</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>*Fecha L&iacute;mite</td>
                                        <td>
                                            <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="80%" TabIndex="6" MaxLength="11" />
                                            <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Inicio" runat="server" 
                                                TargetControlID="Txt_Fecha_Inicio" WatermarkCssClass="watermarked" 
                                                WatermarkText="Dia/Mes/Año" Enabled="True" />
                                            <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicio" runat="server" 
                                                TargetControlID="Txt_Fecha_Inicio" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_inicio"/>
                                             <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server"
                                                ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                                Height="18px" CausesValidation="false"/>
                                            <cc1:MaskedEditExtender 
                                                ID="Mee_Txt_Fecha_Inicio" 
                                                Mask="99/LLL/9999" 
                                                runat="server"
                                                MaskType="None" 
                                                UserDateFormat="DayMonthYear" 
                                                UserTimeFormat="None" Filtered="/"
                                                TargetControlID="Txt_Fecha_Inicio" 
                                                Enabled="True" 
                                                ClearMaskOnLostFocus="false"/>  
                                            <cc1:MaskedEditValidator 
                                                ID="Mev_Txt_Fecha" 
                                                runat="server" 
                                                ControlToValidate="Txt_Fecha_Inicio"
                                                ControlExtender="Mee_Txt_Fecha_Inicio" 
                                                EmptyValueMessage="Fecha Requerida"
                                                InvalidValueMessage="Fecha Inicio Invalida" 
                                                IsValidEmpty="false" 
                                                TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                                Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                        </td>
                                         <td style="width:20%">
                                            <asp:Label ID="Lbl_Tipo" runat="server" Text="*Tipo"></asp:Label>
                                        </td>
                                        <td style="width:30%">
                                            <asp:DropDownList ID="Cmb_Tipo" runat="server" Width="75%">
                                            </asp:DropDownList>
                                            <%--<asp:DropDownList ID="Cmb_Tipo" runat="server" Width="75%">
                                            <asp:ListItem value="0"><--SELECCIONE--></asp:ListItem>
                                            <asp:ListItem Value="1">ANTICIPO</asp:ListItem>
                                            <asp:ListItem Value="2">FONDO FIJO</asp:ListItem>
                                            <asp:ListItem Value="3">GASTOS POR COMPROBAR </asp:ListItem>
                                            </asp:DropDownList>--%>
                                        </td>
                                    </tr><tr>
                                        <td>
                                            <asp:Label ID="Lbl_Deudor" runat="server" Text="*Deudor"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:DropDownList ID="Cmb_Deudores" runat="server" Width="85%">
                                            </asp:DropDownList>
                                            <asp:UpdatePanel ID="Udp_Busqueda_Modal" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                            <ContentTemplate> 
                                                    <asp:ImageButton ID="Btn_Mostrar_Busqueda" runat="server" 
                                                    ToolTip="Busqueda Avanzada" TabIndex="1"
                                                    ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Height="17px" Width="17px"
                                                    OnClientClick="javascript:return Abrir_Modal_Popup_2();" CausesValidation="false" />
                                                    <cc1:ModalPopupExtender  ID="Mpe_Busqueda_Deudor" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Busqueda_Deudor"
                                                     PopupControlID="Pnl_Busqueda_Deudor" TargetControlID="Btn_Open_2" 
                                                    CancelControlID="Btn_Close" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                                    <asp:Button Style="background-color: transparent; border-style:none; width:.5px;" 
                                                    ID="Btn_Close" runat="server" Text="" />
                                                    <asp:Button  Style="background-color: transparent; border-style:none; width:.5px;" 
                                                    ID="Btn_Open_2" runat="server" Text="" />                                                                                                    
                                             </ContentTemplate>
                                             </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Lbl_Importe" runat="server" Text="*Importe"></asp:Label></td><td>
                                            <asp:TextBox ID="Txt_Importe" runat="server" Width="80%" OnKeyPress="this.value=(this.value.match(/^[0-9]*(\.[0-9]{0,2})?$/))?this.value :'';"></asp:TextBox></td></tr><tr>
                                        <td>
                                            <asp:Label ID="Lbl_Concepto" runat="server" Text="*Concepto"></asp:Label></td><td colspan="3">
                                            <asp:TextBox ID="Txt_Concepto" runat="server" TextMode="MultiLine" width="88%"  MaxLength="200"></asp:TextBox><cc1:TextBoxWatermarkExtender ID="Txt_Concepto_TextBoxWatermarkExtender" 
                                                runat="server" Enabled="True" TargetControlID="Txt_Concepto" 
                                                WatermarkCssClass="watermarked" 
                                                WatermarkText="Ingrese sus comentarios &lt;Maximo de 255 caracteres&gt;">
                                            </cc1:TextBoxWatermarkExtender>
                                            <cc1:FilteredTextBoxExtender ID="Txt_Concepto_FilteredTextBoxExtender" 
                                                runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Concepto" 
                                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ ">
                                            </cc1:FilteredTextBoxExtender>
                                            <span id="Contador_Caracteres_Concepto" class="watermarked"></span>
                                        </td>
                                    </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
       <%--BUSCAR FUENTE DE FINANCIAMIENTO--%>
     <asp:Panel ID="Pnl_Busqueda_Deudor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
        style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
        <asp:Panel ID="Pnl_Cabecera" runat="server" 
            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
            <table width="99%">
                <tr>
                    <td style="color:Black;font-size:12;font-weight:bold;">
                       <asp:Image ID="Image1" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                         B&uacute;squeda: Deudores
                    </td>
                    <td align="right" style="width:10%;">
                       <asp:ImageButton ID="Btn_Cerrar_Ven" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />  <%--OnClick="Btn_Cerrar_Ven_Click"--%>
                    </td>
                </tr>
            </table>            
        </asp:Panel>                                                                          
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Deudor" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Deudor" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Deudor" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>                                                             
                                <table width="100%">
                                 <tr>
                                        <td colspan="4">
                                            <table style="width:80%;">
                                              <tr>
                                                <td align="left">
                                                  <asp:ImageButton ID="Img_Error_Busqueda_Cuenta" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                    Width="24px" Height="24px" Visible="false" />
                                                    <asp:Label ID="Lbl_Error_Busqueda_Cuenta" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" Visible=false />
                                                </td>
                                              </tr>
                                            </table>
                                        </td>
                                   </tr>
                                    <tr>
                                        <td style="width:100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_2();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>                         
                                        </td>
                                    </tr>     
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">Deudor</td><td colspan="3" style="text-align:left;font-size:11px;">
                                            <asp:TextBox ID="Txt_Busqueda_Deudor" runat="server" Width="98%" TabIndex="11" MaxLength="100"/> 
                                             <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Deudor" runat="server" TargetControlID="Txt_Busqueda_Deudor"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"  InvalidChars="&lt;,&gt;,&amp;,',!," ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                        </td> 
                                        <td colspan="2" style="width:50%;text-align:left;font-size:11px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Deudor" runat="server"  Text="Buscar Deudor" CssClass="button"  
                                                    CausesValidation="false" Width="300px" TabIndex="15" OnClick="Btn_Busqueda_Deudor_Popup_Click" />
                                            </center>
                                        </td>                                                     
                                    </tr>
                                    <tr>
                                        <td style="width:100%" colspan ="4">
                                            <div id="Div_Busqueda" runat="server" style="overflow:auto; max-height:200px; width:99%">
                                            <table style="width:100%" >
                                                <tr> 
                                                    <td>
                                        <asp:GridView ID="Grid_Deudores" runat="server" CssClass="GridView_1"  AutoGenerateColumns="False" GridLines="None" Width="99.9%"
                                                    AllowSorting="True" HeaderStyle-CssClass="tblHead" EmptyDataText="No se encontro coincidencia con la busqueda">
                                                   <Columns>
                                                       <asp:TemplateField HeaderText="Selecciona">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="Btn_Seleccionar_Deudor" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                     CommandArgument='<%# Eval("Deudor_ID") %>' OnClick="Btn_Seleccionar_Deudor_Click" />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" SortExpression="NOMBRE">
                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="tblHead" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>
                                                    </td>
                                                 </tr>
                                            </table>
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>

