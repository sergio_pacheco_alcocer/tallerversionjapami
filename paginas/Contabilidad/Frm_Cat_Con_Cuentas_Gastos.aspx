﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Cat_Con_Cuentas_Gastos.aspx.cs" Inherits="paginas_Contabilidad_Frm_Cat_Con_Cuentas_Gastos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <script language="javascript" type="text/javascript">
        function Enter_Busqueda_Partida(textbox, evento) {
            if (evento.which == 13) { 
                (document.getElementById("<%=Btn_Buscar_Partida.ClientID %>")).click();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="SM_Bancos" runat="server" />
    <asp:UpdatePanel ID="UPnl_Bancos" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="UPnl_Bancos"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Antiguedad_Sindicato" style="background-color: #ffffff; width: 100%;
                height: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">
                            Catalogo Gastos
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error" />
                        </td>
                    </tr>
                </table>
                <table width="98%" border="0" cellspacing="0">
                    <tr align="center">
                        <td>
                            <div align="right" class="barra_busqueda">
                                <table style="width: 100%; height: 28px;">
                                    <tr>
                                        <td align="left" style="width: 59%;">
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" OnClick="Btn_Nuevo_Click" />
                                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" OnClick="Btn_Modificar_Click" />
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" OnClick="Btn_Salir_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="98%">
                    <tr>
                        <td style="width: 100%" colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 20%;">
                            <b>*</b>Descripcion
                        </td>
                        <td style="text-align: left; width: 30%;">
                            <asp:TextBox ID="Txt_Descripcion" runat="server" Width="98%" MaxLength="20" onkeyup="this.value=this.value.toUpperCase();" />
                        </td>
                        <td style="text-align: left; width: 20%;">
                            &nbsp;&nbsp;<b>*</b>Estatus
                        </td>
                        <td style="text-align: left; width: 30%;">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="96%">
                                <asp:ListItem Value="">&lt;-- Seleccione -- &gt;</asp:ListItem>
                                <asp:ListItem Value="ACTIVA">ACTIVA</asp:ListItem>
                                <asp:ListItem Value="INACTIVA">INACTIVA</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 20%; vertical-align: top;">
                            Concepto
                        </td>
                        <td style="text-align: left; width: 30%;" colspan="3">
                            <asp:TextBox ID="Txt_Concepto" runat="server" Width="98%" MaxLength="100" TextMode="MultiLine"
                                TabIndex="5" Height="45px" Wrap="true" onkeyup="this.value=this.value.toUpperCase();" />
                            <cc1:FilteredTextBoxExtender ID="FTxt_Comentarios" runat="server" TargetControlID="Txt_Concepto"
                                FilterType="Custom, LowercaseLetters, UppercaseLetters, Numbers" ValidChars="áéíóúÁÉÍÓÚ ñÑ" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Comentarios" runat="server" TargetControlID="Txt_Concepto"
                                WatermarkText="Límite de Caractes 100" WatermarkCssClass="watermarked" />
                            <span id="Contador_Caracteres_Comentarios" class="watermarked"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 20%;">
                            <asp:HiddenField ID="Hdf_Partida_ID" runat="server" />
                            <b>*</b> Partida
                        </td>
                        <td colspan="3" style="text-align: left; width: 80%;">
                            <asp:TextBox ID="Txt_Clave_Partida" runat="server" Width="50px" MaxLength="4" style="text-align:center;" onkeydown="javascript:return Enter_Busqueda_Partida(this, event);" ></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Clave_Partida" runat="server" TargetControlID="Txt_Clave_Partida" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="Btn_Buscar_Partida" runat="server" ToolTip="Buscar"
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Partida_Click"
                                CausesValidation="false" />
                            <asp:TextBox ID="Txt_Nombre_Partida" runat="server" Width="80%" ReadOnly="true"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Agregar_Documento" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_add.png"
                                            CssClass="Img_Button" ToolTip="Agregar"  Height="15px" Width="15px"  AutoPostBack="true"  OnClick="Btn_Agregar_Documento_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan ="4">
                           <asp:Panel ID="Pnl_Partidas_Agregadas" runat="server" GroupingText="Partidas Presupuestales" Width="99%">
                              <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                                 <tr align="center">
                                    <td>
                                      <div style="width:98%;vertical-align:top;">
                                         <asp:GridView ID="Grid_Partidas" runat="server" Width="97%"
                                              AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                              OnRowDataBound="Grid_Partidas_RowDataBound">
                                           <Columns>
                                              <asp:BoundField DataField="Partida_ID" HeaderText="Partida" ItemStyle-Font-Size="X-Small">
                                                <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                <ItemStyle HorizontalAlign="Left" Width="20%" />
                                              </asp:BoundField>
                                              <asp:BoundField DataField="Partida" HeaderText="Partida" ItemStyle-Font-Size="X-Small">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                              </asp:BoundField>
                                              <asp:BoundField DataField="Clave" HeaderText="Clave" ItemStyle-Font-Size="X-Small">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                              </asp:BoundField>
                                              <asp:BoundField DataField="Descripcion" HeaderText="Partida" ItemStyle-Font-Size="X-Small">
                                                <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                                <ItemStyle HorizontalAlign="Left" Width="60%" />
                                              </asp:BoundField>
                                              <asp:TemplateField>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                            CausesValidation="false" 
                                                            ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                            OnClientClick="return confirm('¿Está seguro de eliminar de la tabla la partida seleccionada?');" OnClick="Btn_Eliminar_Partida" />
                                                    </center>
                                                 </ItemTemplate>
                                                 <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                 <ItemStyle HorizontalAlign="Left" Width="10%" />
                                               </asp:TemplateField>
                                             </Columns>
                                             <SelectedRowStyle CssClass="GridSelected" />
                                             <PagerStyle CssClass="GridHeader" />
                                             <HeaderStyle CssClass="tblHead" />
                                             <AlternatingRowStyle CssClass="GridAltItem" />
                                           </asp:GridView> 
                                         </div>
                                      </td>
                                   </tr>
                               </table>
                           </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" colspan="4">
                            <hr />
                        </td>
                    </tr>
                </table>
                <table style="width: 97%;">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Grid_Gastos" runat="server" AllowPaging="True" CssClass="GridView_1"
                                AutoGenerateColumns="False" PageSize="10" Width="100%" AllowSorting="True" HeaderStyle-CssClass="tblHead"
                                Style="white-space: normal;" OnSelectedIndexChanged="Grid_Gastos_SelectedIndexChanged"
                                OnPageIndexChanging="Grid_Gastos_PageIndexChanging" DataKeyNames="CUENTA_GASTO_ID">
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="CUENTA_GASTO_ID" HeaderText="CUENTA_GASTO_ID">
                                        <HeaderStyle HorizontalAlign="Left" Width="1%" />
                                        <ItemStyle HorizontalAlign="Left" Width="1%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PARTIDA" HeaderText="Partida Presupuestal">
                                        <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                        <ItemStyle HorizontalAlign="Left" Width="30%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle HorizontalAlign="Left" Width="20%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                        <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                        <ItemStyle HorizontalAlign="Center" Width="5%" />
                                    </asp:BoundField>
                                </Columns>
                                <SelectedRowStyle CssClass="GridSelected" />
                                <PagerStyle CssClass="GridHeader" />
                                <HeaderStyle CssClass="tblHead" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
