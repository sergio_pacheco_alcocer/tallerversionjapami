﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Con_Estado_Cuentas.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Estado_Cuentas" Title="Reporte de estado de cuentas"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    
    <script language="javascript" type="text/javascript">
    
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        
   </script>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Polizas" runat="server" AsyncPostBackTimeout="36000"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>        
           <asp:UpdateProgress ID="Uprg_Polizas" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>                
            <div id="Div_Reporte_Balance_Mensual" style="background-color:#ffffff; width:100%; height:100%;">    
                    <table width="100%" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo">Reporte de estado de cuentas </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                                <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table width="98%"  border="0" cellspacing="0">
                        <tr align="center">
                            <td>                
                                <div align="right" class="barra_busqueda">                        
                                    <table style="width:100%;height:28px;">
                                        <tr>
                                            <td>
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>      
                                                    <asp:ImageButton ID="Btn_Reporte" runat="server" ToolTip="Generar Reporte" 
                                                    CssClass="Img_Button" TabIndex="1"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                                                    onclick="Btn_Reporte_Click"/>
                                                    </ContentTemplate>
                                                    </asp:UpdatePanel>
                                              </td>
                                              <td align="left" style="width:50%;">
                                                <asp:ImageButton ID="Btn_Imprimir_Excel" runat="server" 
                                                     ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png"
                                                      CssClass="Img_Button"  AlternateText="Imprimir Excel" 
                                                     ToolTip="Exportar Excel"
                                                     OnClick="Btn_Imprimir_Excel_Click" Visible="true"/> 
                                                <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                    CssClass="Img_Button" TabIndex="2"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                    onclick="Btn_Salir_Click"/>
                                            </td>
                                            <td align="right" style="width:50%;">&nbsp;</td>       
                                        </tr>         
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <%--para los tipos de reportes--%>
                     <table width="98%" class="estilo_fuente"> 
                        <tr >
                            <td style="width:20%;text-align:left;" class="button_agregar">*Reportes:</td>
                            <td  colspan="3"style="width:80%;text-align:left;">
                                <asp:DropDownList id="Cmb_Tipo_Reporte" runat="server" Width="98%" >
                                    <asp:ListItem>--- SELECCIONE ---</asp:ListItem>
                                    <asp:ListItem>SITUACIÓN FINANCIERA</asp:ListItem>
                                    <asp:ListItem>ESTADO DE ACTIVIDADES/RESULTADOS</asp:ListItem>
                                    <asp:ListItem>VARIACIONES EN LA HACIENDA PÚBLICA</asp:ListItem>
                                    <asp:ListItem>NOTAS DE LOS ESTADOS FINANCIEROS</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                          <tr >
                            <td style="width:20%;text-align:left;">*A&ntilde;o</td>
                            <td style="width:30%;text-align:left;">
                                <asp:DropDownList id="Cmb_Anio" runat="server" Width="98%" AutoPostBack="true" 
                                    OnSelectedIndexChanged="Cmb_Anio_OnSelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td style="width:20%;text-align:left;">*Mes</td>
                            <td style="width:30%;text-align:left;">
                                <asp:DropDownList id="Cmb_Mes" runat="server" Width="95%">
                                </asp:DropDownList>
                            </td>
                        </tr>                        
                        <tr>
                            <td align="left">Campo Oculto</td>
                            <td align="left"><asp:TextBox ID="Txt_Campo_Oculto" runat="server" ReadOnly="true" Width="150px"></asp:TextBox></td>                            
                            <td colspan="2" style="width:100%;text-align:center;">
                                <asp:CheckBox ID="Chk_Movimientos_Saldo_No_Cero" Text="Movimientos con Saldo no Ceros" runat="server" TabIndex="12"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left">
                                <b>NOTA:&nbsp;</b>El campo oculto es un campo que tienen los reportes de CONAC generalmente al lado del encabezado y del mismo color, favor de verificar 
                                que este valor sea el mismo de los reportes descargados de la pagina de CONAC, de lo contrario, hay que modificar este valor en el cat&aacute;logo 
                                de par&aacute;metros.                        
                            </td>
                        </tr>
                    </table>         
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
