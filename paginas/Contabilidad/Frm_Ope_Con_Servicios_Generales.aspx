<%@ Page Title="Solicitud de Pagos Directas de Servicios Generales" Language="C#"
    MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true"
    CodeFile="Frm_Ope_Con_Servicios_Generales.aspx.cs" Inherits="paginas_Servicios_Generales_Frm_Ope_Con_Servicios_Generales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script language="javascript" type="text/javascript">
        function EnterBusquedaProveedor(textbox, event) {
            if (event.which == 13) {
                (document.getElementById("<%=Btn_Buscar_Proveedor_Solicitud_Pagos.ClientID %>")).click();
                return false;
            }
            return true;
        }
        function EnterDetallesPagos(nombre, tipo, event) {
            if (event.which == 13) {
                Calcular_Totales(nombre, tipo);
                return false;
            }
            return true;
        }
        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }

        function Calcular_Totales(nombre_txt, tipo) {
            var dependencia_partida_id;
            if (tipo.toString() == "IMPORTE") dependencia_partida_id = nombre_txt.toString().substr(12, 15);
            if (tipo.toString() == "IVA") dependencia_partida_id = nombre_txt.toString().substr(8, 15);
            var importe = parseFloat($(".Txt_Importe_" + dependencia_partida_id).val().replace(",", ""));
            var iva = parseFloat($(".Txt_IVA_" + dependencia_partida_id).val().replace(",", ""));
            if (tipo.toString() == "IMPORTE" && iva == 0) {
                iva = importe * 0.16;
                $(".Txt_IVA_" + dependencia_partida_id).val(formatCurrency(iva));
            }
            var total = importe + iva;
            $(".Txt_Total_" + dependencia_partida_id).val(formatCurrency(total));
            Calcular_Totales_Finales();
            return true;
        }

        function Calcular_Totales_Finales() {
            var importe_total = 0;
            var iva_total = 0;
            var total = 0;
            var grid = document.getElementById("<%=Grid_Detalles_Pago.ClientID%>");
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "text") {
                    if (inputs[i].title == "Importe") {
                        importe_total += parseFloat(inputs[i].value.replace(',',''));
                    }
                    if (inputs[i].title == "IVA") {
                        iva_total += parseFloat(inputs[i].value.replace(',', ''));
                    }
                    if (inputs[i].title == "Total") {
                        total += parseFloat(inputs[i].value.replace(',', ''));
                    }
                }
            }
            document.getElementById("<%=Txt_Total_Importe.ClientID %>").value = formatCurrency(importe_total);
            document.getElementById("<%=Txt_Total_IVA.ClientID %>").value = formatCurrency(iva_total);
            document.getElementById("<%=Txt_Total_Total.ClientID %>").value = formatCurrency(total);
        }
        
    </script>

<!--SCRIPT PARA LA VALIDACION QUE NO EXPERE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesi�n
        var CONTROLADOR = "../../Mantenedor_Session.ashx";

        //Ejecuta el script en segundo plano evitando as� que caduque la sesi�n de esta p�gina
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesi�n activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        
    //-->
   </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Polizas" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"  AsyncPostBackTimeout="360000" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Servicios" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Servicios" style="background-color: #ffffff; width: 100%; height: 100%;">
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">
                            Creaci�n de Solicitudes de Pago [Servicios Generales]
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                            <asp:UpdatePanel ID="Upnl_Mensajes_Error" runat="server" UpdateMode="Always" RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                        Visible="false" />&nbsp;
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <table width="98%" border="0" cellspacing="0">
                    <tr class="barra_busqueda">
                        <td align="left">
                            <asp:UpdatePanel ID="Upnl_Botones_Operacion" runat="server" UpdateMode="Conditional"
                                RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" OnClick="Btn_Nuevo_Click"
                                        ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" />
                                    <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" OnClick="Btn_Salir_Click"
                                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <table width="100%" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="Hdf_Cuenta_Contable_Proveedor" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            *Fte. Financiamiento
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Fuente_Financiamiento" runat="server" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            *Gasto
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Gasto" runat="server" Width="100%" AutoPostBack="true"
                                OnSelectedIndexChanged="Cmb_Gastos_SelectedIndexChanged">
                                <asp:ListItem Value="0" Text="<-Seleccione->"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            *Tipo de Solicitud
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Tipo_Solicitud_Pago" runat="server" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            <asp:Label ID="Lbl_Proveedor" runat="server" Text="*Proveedor"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" MaxLength="30"
                                onkeydown="javascript:return EnterBusquedaProveedor(this, event);" Width="80%"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Proveedor_Solicitud_Pago" runat="server"
                                TargetControlID="Txt_Nombre_Proveedor_Solicitud_Pago" FilterType="Custom, UppercaseLetters, LowercaseLetters"
                                ValidChars="������������. ">
                            </cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="Btn_Buscar_Proveedor_Solicitud_Pagos" runat="server" ToolTip="Consultar"
                                OnClick="Btn_Buscar_Proveedor_Solicitud_Pagos_Click" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" />
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="Cmb_Proveedor_Solicitud_Pago" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Proveedor_Solicitud_Pago_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="0000000000" Text="<- Seleccione ->"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            <asp:Label ID="Lbl_Cuenta_Contable_Proveedor" runat="server" Text="*CC Proveedor"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Cuenta_Contable_Proveedor" runat="server" Width="99.5%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            <asp:Label ID="Lbl_Factura" runat="server" Text="*No. Documento"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_No_Factura" runat="server" MaxLength="15" Width="99%"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="Fil_Txt_No_Factura" runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_No_Factura" ValidChars="��.,:;()����������-%/ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width: 15%">
                            <asp:Label ID="Lbl_Fecha_Factura" runat="server" Text="*Fecha Documento"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Fecha_Factura" runat="server" Width="90%" MaxLength="11" Height="15px" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Fecha_Factura" runat="server" TargetControlID="Txt_Fecha_Factura"
                                WatermarkCssClass="watermarked" WatermarkText="Dia/Mes/A�o" Enabled="True" />
                            <cc1:CalendarExtender ID="CE_Txt_Fecha_Factura" runat="server" TargetControlID="Txt_Fecha_Factura"
                                Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Factura" />
                            <asp:ImageButton ID="Btn_Fecha_Factura" runat="server" ImageUrl="../imagenes/paginas/SmallCalendar.gif"
                                Style="vertical-align: top;" CausesValidation="false" />
                            <cc1:MaskedEditExtender ID="Mee_Txt_Fecha_Factura" Mask="99/LLL/9999" runat="server"
                                MaskType="None" UserDateFormat="DayMonthYear" UserTimeFormat="None" Filtered="/"
                                TargetControlID="Txt_Fecha_Factura" Enabled="True" ClearMaskOnLostFocus="false" />
                            <cc1:MaskedEditValidator ID="Mev_Txt_Fecha_Factura" runat="server" ControlToValidate="Txt_Fecha_Factura"
                                ControlExtender="Mee_Txt_Fecha_Factura" EmptyValueMessage="Fecha Requerida" InvalidValueMessage="Fecha Factura Invalida"
                                IsValidEmpty="false" TooltipMessage="Ingrese o Seleccione la Fecha de Factura"
                                Enabled="true" Style="font-size: 10px; background-color: #F0F8FF; color: Black;
                                font-weight: bold;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            <asp:Label ID="Lbl_Concepto" runat="server" Text="*Concepto"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Conceptos" runat="server" TextMode="MultiLine" Width="99.5%"
                                onkeyup="this.value=this.value.toUpperCase();"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="Txt_Conceptos_TextBoxWatermarkExtender" runat="server"
                                Enabled="True" TargetControlID="Txt_Conceptos" WatermarkCssClass="watermarked"
                                WatermarkText="Ingrese sus comentarios &lt;M�ximo de 255 caracteres&gt;">
                            </cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="Txt_Conceptos_FilteredTextBoxExtender" runat="server"
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" InvalidChars="&lt;,&gt;,&amp;,',!,"
                                TargetControlID="Txt_Conceptos" ValidChars="��.,:;()����������-%/ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="Grid_Detalles_Pago" runat="server" CssClass="GridView_1" AutoGenerateColumns="False"
                                OnRowDataBound="Grid_Detalles_Pago_RowDataBound"
                                AllowPaging="false" Width="100%" GridLines="None">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:BoundField DataField="PARTIDA_ID" HeaderText="PARTIDA_ID" SortExpression="PARTIDA_ID">
                                        <ItemStyle Width="1%" HorizontalAlign="Left" Font-Size="X-Small" />
                                        <HeaderStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLAVE_PARTIDA" HeaderText="Partida" SortExpression="CLAVE_PARTIDA">
                                        <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLAVE_DESCRIPCION_PARTIDA" HeaderText="CLAVE_DESCRIPCION_PARTIDA" SortExpression="CLAVE_DESCRIPCION_PARTIDA">
                                        <ItemStyle Width="1%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DEPENDENCIA_ID" HeaderText="DEPENDENCIA_ID" SortExpression="DEPENDENCIA_ID">
                                        <ItemStyle Width="1%" HorizontalAlign="Left" Font-Size="X-Small" />
                                        <HeaderStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA">
                                        <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLAVE_DESCRIPCION_DEPENDENCIA" HeaderText="CLAVE_DESCRIPCION_DEPENDENCIA"
                                        SortExpression="CLAVE_DESCRIPCION_DEPENDENCIA">
                                        <ItemStyle Width="1%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLAVE_PROGRAMA" HeaderText="Programa" SortExpression="CLAVE_PROGRAMA">
                                        <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE_PROGRAMA" HeaderText="NOMBRE_PROGRAMA" SortExpression="NOMBRE_PROGRAMA">
                                        <ItemStyle Width="1%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLAVE_DESCRIPCION_PROGRAMA" HeaderText="CLAVE_DESCRIPCION_PROGRAMA"
                                        SortExpression="CLAVE_DESCRIPCION_PROGRAMA">
                                        <ItemStyle Width="1%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DISPONIBLE" HeaderText="Disponible [$]" SortExpression="DISPONIBLE" DataFormatString="{0:#,###,##0.00}">
                                        <ItemStyle Width="15%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="15%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MONTO" HeaderText="MONTO" SortExpression="MONTO">
                                        <ItemStyle Width="1%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Importe [$]">
                                        <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="Txt_Importe" ToolTip="Importe" class='<%# "Txt_Importe_" + Eval("DEPENDENCIA_ID") + Eval("PARTIDA_ID") %>' runat="server" Text="0.00" style="text-align: right;" onBlur="this.value=formatCurrency(this.value); Calcular_Totales(this.getAttribute('class'), 'IMPORTE');" onkeydown="javascript:return EnterDetallesPagos(this.getAttribute('class'), 'IMPORTE', event);" Width="100px" ></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IVA [$]">
                                        <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="Txt_IVA" ToolTip="IVA" class='<%# "Txt_IVA_" + Eval("DEPENDENCIA_ID") + Eval("PARTIDA_ID") %>' runat="server" Text="0.00" style="text-align: right;" onBlur="this.value=formatCurrency(this.value); Calcular_Totales(this.getAttribute('class'), 'IVA');" onkeydown="javascript:return EnterDetallesPagos(this.getAttribute('class'), 'IVA', event);" Width="100px" ></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total [$]">
                                        <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="Txt_Total" ToolTip="Total" class='<%# "Txt_Total_" + Eval("DEPENDENCIA_ID") + Eval("PARTIDA_ID") %>' runat="server" Text="0.00" Enabled="false" Font-Bold="true" ForeColor="Black" style="text-align: right;" Width="100px" ></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="PROGRAMA_ID" HeaderText="PROGRAMA_ID" SortExpression="PROGRAMA_ID">
                                        <ItemStyle Width="1%" HorizontalAlign="Left" Font-Size="X-Small" />
                                        <HeaderStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CAPITULO_ID" HeaderText="CAPITULO_ID" SortExpression="CAPITULO_ID">
                                        <ItemStyle Width="1%" HorizontalAlign="Left" Font-Size="X-Small" />
                                        <HeaderStyle Width="1%" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="4">
                            <asp:Label ID="Lbl_Total_Importe" runat="server" Text="Importe"></asp:Label>&nbsp;
                            <asp:TextBox ID="Txt_Total_Importe" runat="server" Width="100px" Enabled="false" Font-Bold="true" ForeColor="Blue" style="text-align: right;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="4">
                            <asp:Label ID="Lbl_Total_IVA" runat="server" Text="IVA"></asp:Label>&nbsp;
                            <asp:TextBox ID="Txt_Total_IVA" runat="server" Width="100px" Enabled="false" Font-Bold="true" ForeColor="Blue" style="text-align: right;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="4">
                            <asp:Label ID="Lbl_Total_Total" runat="server" Text="Total"></asp:Label>&nbsp;
                            <asp:TextBox ID="Txt_Total_Total" runat="server" Width="100px" Enabled="false" Font-Bold="true" ForeColor="Blue" style="text-align: right;"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <br /><br /><br /><br /><br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
