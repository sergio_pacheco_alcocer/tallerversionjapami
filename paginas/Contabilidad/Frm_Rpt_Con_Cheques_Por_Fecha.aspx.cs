﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
public partial class paginas_Contabilidad_Frm_Rpt_Con_Cheques_Por_Fecha : System.Web.UI.Page
{
    #region (Load/Init)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Refresca la session del usuario lagueado al sistema.
                Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
                //Valida que exista algun usuario logueado al sistema.
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    DateTime _DateTime = DateTime.Now;
                    //int dias = _DateTime.Day;
                    //dias = dias * -1;
                    //dias++;
                    //_DateTime = _DateTime.AddDays(dias);
                    //Txt_Fecha_Inicio.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                    Txt_Fecha_Inicio.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                    Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion
    #region (Metodos)
        #region (Métodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION : Inicializa los controles de la forma para prepararla para el
            ///               reporte
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
                DataTable Dt_Existencia = new DataTable();
                try
                {
                    Limpia_Controles(); //limpia los campos de la forma
                    Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Banco, Dt_Existencia, "NOMBRE", "NOMBRE");
                }
                catch (Exception ex)
                {
                    throw new Exception("Inicializa_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION : Limpia los controles que se encuentran en la forma
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Controles()
            {
                try
                {
                    Cmb_Banco.SelectedIndex = -1;
                    Txt_Fecha_Final.Enabled = false;
                    Txt_Fecha_Inicio.Enabled = false;
                    Session["Dt_Datos_Detalles"] = null;
                    Session["Contador"] = null;
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Validar_Datos_Reporte
            /// DESCRIPCION : Validar que se hallan proporcionado todos los datos para la
            ///               generación del reporte
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 08-Octubre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Validar_Datos_Reporte()
            {
                Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                if (Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()).CompareTo(Convert.ToDateTime(Txt_Fecha_Final.Text.Trim())) > 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+La fecha inicial no puede ser mayor a la fecha final <br>";
                    Datos_Validos = false;
                }
                return Datos_Validos;
            }
        #endregion
        #region (Consultas)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Libro_Mayor
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Cheques_Emitidos()
            {
                String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
                String Nombre_Archivo = "Cheques_Emitidos" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyyHHmmss}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
                DataTable Dt_Cheques = new DataTable();       //Va a conter los valores de la consulta realizada
                Ds_Rpt_Con_Cheques_Emitidos Ds_Cheques_Emitidos = new Ds_Rpt_Con_Cheques_Emitidos();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Ope_Con_Cehques = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
                
                try
                {
                    if (Cmb_Banco.SelectedIndex > 0)
                    {
                        Rs_Consulta_Ope_Con_Cehques.P_Nombre_Banco = Cmb_Banco.SelectedItem.Text.ToString();
                    }
                    Rs_Consulta_Ope_Con_Cehques.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                    Rs_Consulta_Ope_Con_Cehques.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));
                    Dt_Cheques = Rs_Consulta_Ope_Con_Cehques.Consulta_Cheques_Banco_Fecha(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
                    if (Dt_Cheques.Rows.Count > 0)
                    {
                        Dt_Cheques.TableName = "Dt_Cheques_Emitidos";
                        Ds_Cheques_Emitidos.Clear();
                        Ds_Cheques_Emitidos.Tables.Clear();
                        Ds_Cheques_Emitidos.Tables.Add(Dt_Cheques.Copy());
                    }
                    ReportDocument Reporte = new ReportDocument();
                    Reporte.Load(Ruta_Archivo + "Rpt_Con_Cheques_Emitidos.rpt");
                    Reporte.SetDataSource(Ds_Cheques_Emitidos);

                    ParameterFieldDefinitions Cr_Parametros;
                    ParameterFieldDefinition Cr_Parametro;
                    ParameterValues Cr_Valor_Parametro = new ParameterValues();
                    ParameterDiscreteValue Cr_Valor = new ParameterDiscreteValue();

                    Cr_Parametros = Reporte.DataDefinition.ParameterFields;

                    Cr_Parametro = Cr_Parametros["Fecha_Inicial"];
                    Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = String.Format("{0:dd/mm/yyyy}",Convert.ToDateTime(Txt_Fecha_Inicio.Text.ToString()));
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);

                    Cr_Parametro = Cr_Parametros["Fecha_Final"];
                    Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = String.Format("{0:dd/mm/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.ToString()));
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);

                    DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                    Nombre_Archivo += ".pdf";
                    Ruta_Archivo = @Server.MapPath("../../Reporte/");
                    m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                    ExportOptions Opciones_Exportacion = new ExportOptions();
                    Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);

                    Abrir_Ventana(Nombre_Archivo);
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Mostrar_Reporte
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 24-Mayo-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Mostrar_Reporte()
            {
                DataTable Dt_Cheques = new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Cheques_Diarios = new DataTable(); //Obtiene los valores a pasar al reporte
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Ope_Con_Cehques = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                DataTable Dt_Datos_Detalles = new DataTable();
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                int Contador = 0;
                Session["Contador"] = Contador;
                Boolean Insertar = true;
                try
                {
                    if (Cmb_Banco.SelectedIndex > 0)
                    {
                        Rs_Consulta_Ope_Con_Cehques.P_Nombre_Banco = Cmb_Banco.SelectedItem.Text.ToString();
                    }
                    Rs_Consulta_Ope_Con_Cehques.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                    Rs_Consulta_Ope_Con_Cehques.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));
                    Dt_Cheques = Rs_Consulta_Ope_Con_Cehques.Consulta_Cheques_Por_Fecha(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
                    if (Dt_Cheques.Rows.Count > 0)
                    {
                        Session["Dt_Datos_Detalles"] = Dt_Cheques;
                        Grid_Cuentas_Movimientos.DataSource = Dt_Cheques;
                        Grid_Cuentas_Movimientos.DataBind();
                        Btn_Reporte_Cheques.Visible = true;
                        Tr_Grid_Bancos.Style.Add("display","block");
                    }
                    else
                    {
                        Grid_Cuentas_Movimientos.DataSource = null;
                        Grid_Cuentas_Movimientos.DataBind();
                        Btn_Reporte_Cheques.Visible = false;
                        Tr_Grid_Bancos.Style.Add("display", "none");
                        Lbl_Mensaje_Error.Text="No tiene Movimientos en el rango de fechas que coloco <br>";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            //'****************************************************************************************
            //'NOMBRE DE LA FUNCION: Grid_Proveedores_RowDataBound
            //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            //'PARAMETROS  : 
            //'CREO        : Sergio Manuel Gallardo
            //'FECHA_CREO  : 16/julio/2011 10:01 am
            //'MODIFICO          :
            //'FECHA_MODIFICO    :
            //'CAUSA_MODIFICACION:
            //'****************************************************************************************
            protected void Grid_Cuentas_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                DataTable Ds_Consulta_Modificada = new DataTable();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Cheques_Movimientos = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
                String Banco = "";
                int Contador;
                Image Img = new Image();
                Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
                Label Lbl_facturas = new Label();
                Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos");
                try
                {
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {
                        Contador = (int)(Session["Contador"]);
                        Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                        Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                        + (Lbl_facturas.Text + ("\',\'"
                                        + (Img.ClientID + "\')")))));
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                        Banco = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["NOMBRE"].ToString());
                        Rs_Cheques_Movimientos.P_Nombre_Banco = Banco;
                        Rs_Cheques_Movimientos.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                        Rs_Cheques_Movimientos.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));                    
                        Ds_Consulta = Rs_Cheques_Movimientos.Consulta_Cheques_Banco_Fecha();
                        if (Ds_Consulta.Rows.Count > 0)
                        {
                            if (Ds_Consulta_Modificada.Rows.Count <= 0)
                            {
                                Ds_Consulta_Modificada.Columns.Add("NO_PAGO", typeof(System.String));
                                Ds_Consulta_Modificada.Columns.Add("NO_POLIZA", typeof(System.String));
                                Ds_Consulta_Modificada.Columns.Add("TIPO_POLIZA_ID", typeof(System.String));
                                Ds_Consulta_Modificada.Columns.Add("MES_ANO", typeof(System.String));
                                Ds_Consulta_Modificada.Columns.Add("FECHA_PAGO", typeof(System.String));
                                Ds_Consulta_Modificada.Columns.Add("BENEFICIARIO_PAGO", typeof(System.String));
                                Ds_Consulta_Modificada.Columns.Add("NO_CHEQUE", typeof(System.String));
                                Ds_Consulta_Modificada.Columns.Add("USUARIO", typeof(System.String));
                                Ds_Consulta_Modificada.Columns.Add("NOMBRE", typeof(System.String));
                                Ds_Consulta_Modificada.Columns.Add("NO_SOLICITUD_PAGO", typeof(System.String));
                                Ds_Consulta_Modificada.Columns.Add("MONTO", typeof(System.Double));
                            }
                            foreach (DataRow Fila in Ds_Consulta.Rows)
                            {
                                DataRow row = Ds_Consulta_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                                //Asigna los valores al nuevo registro creado a la tabla
                                row["NO_PAGO"] = Fila["NO_PAGO"].ToString();
                                row["NO_POLIZA"] = Fila["NO_POLIZA"].ToString();
                                row["TIPO_POLIZA_ID"] = Fila["TIPO_POLIZA_ID"].ToString();
                                row["MES_ANO"] = Fila["MES_ANO"].ToString();
                                row["FECHA_PAGO"] = String.Format("{0:dd/MMM/yyyy}",Convert.ToDateTime(Fila["FECHA_PAGO"].ToString()));
                                if (Fila["BENEFICIARIO_PAGO"].ToString().Contains('-'))
                                {
                                    row["BENEFICIARIO_PAGO"] = Fila["BENEFICIARIO_PAGO"].ToString().Substring(Fila["BENEFICIARIO_PAGO"].ToString().IndexOf('-')+1);
                                }
                                else
                                {
                                    row["BENEFICIARIO_PAGO"] = Fila["BENEFICIARIO_PAGO"].ToString();
                                }
                                row["NO_CHEQUE"] = Fila["NO_CHEQUE"].ToString();
                                row["USUARIO"] = Fila["USUARIO_CREO"].ToString();
                                row["NOMBRE"] = Fila["NOMBRE"].ToString();
                                row["MONTO"] = Fila["MONTO"].ToString();
                                row["NO_SOLICITUD_PAGO"] = Fila["NO_SOLICITUD_PAGO"].ToString();
                                Ds_Consulta_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Ds_Consulta_Modificada.AcceptChanges();
                            }
                        }
                        Gv_Detalles = (GridView)e.Row.Cells[2].FindControl("Grid_Movimientos");
                        Gv_Detalles.DataSource = Ds_Consulta_Modificada;
                        Gv_Detalles.DataBind();
                        Session["Contador"] = Contador + 1;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
            ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
            ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
            ///                             para mostrar los datos al usuario
            ///CREO       : Yazmin A Delgado Gómez
            ///FECHA_CREO : 12-Octubre-2011
            ///MODIFICO          :
            ///FECHA_MODIFICO    :
            ///CAUSA_MODIFICACIÓN:
            ///******************************************************************************
            private void Abrir_Ventana(String Nombre_Archivo)
            {
                String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
                try
                {
                    Pagina = Pagina + Nombre_Archivo;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
                }
            }
        #endregion
    #endregion
    protected void Btn_Reporte_Cheques_Emitidos_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Datos_Reporte())
            {
                Consulta_Cheques_Emitidos(); //Consulta los cheques emitidos por fecha
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Btn_Nuevo_Click
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    /// PARÁMETROS:
    /// USUARIO CREO:       Sergio Manuel Gallardo Andrade
    /// FECHA CREO:         24/Mayo/2012
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Datos_Reporte())
            {
                Mostrar_Reporte(); //Consulta el libro de mayor de la cuenta seleccionada
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }
    #region Mostrar_Poliza 
    protected void Btn_Poliza_Click(object sender, EventArgs e)
    {
        String fecha = ((LinkButton)sender).CommandArgument;
        String Tipo_poliza = ((LinkButton)sender).CssClass;
        String No_poliza = ((LinkButton)sender).Text;
        Imprimir(No_poliza, Tipo_poliza, fecha);
    }
    protected void Imprimir(String NO_POLIZA, String TIPO_POLIZA, String FECHA)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        String Mes = "";
        String Ano = "";
        try
        {
            Mes = FECHA.Substring(0, 2);
            Ano = FECHA.Substring(2, 2);
            Cls_Ope_Con_Polizas_Negocio Poliza = new Cls_Ope_Con_Polizas_Negocio();
            Ds_Reporte = new DataSet();
            Poliza.P_No_Poliza = NO_POLIZA;
            Poliza.P_Tipo_Poliza_ID = TIPO_POLIZA;
            Poliza.P_Mes_Ano = Mes + Ano;
            Dt_Pagos = Poliza.Consulta_Detalle_Poliza();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Datos_Poliza";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Poliza.rpt", "Poliza" + NO_POLIZA, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            //Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            //Lbl_Mensaje_Error.Visible = true;
        }

    }
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Poliza(Nombre_Reporte_Generar, Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Poliza(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte_Generar + Formato;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
}
