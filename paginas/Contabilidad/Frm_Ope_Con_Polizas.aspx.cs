﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
//using System.Windows.Forms;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using AjaxControlToolkit;
using System.IO;
using Excel;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using JAPAMI.Sindicatos.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Tipo_Polizas.Negocios;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Area_Funcional.Negocio;
using JAPAMI.Catalogo_Compras_Proyectos_Programas.Negocio;
using JAPAMI.SAP_Operacion_Departamento_Presupuesto.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Compromisos_Contabilidad.Negocios;
using JAPAMI.Bitacora_Polizas.Negocio;
using JAPAMI.Cierre_Mensual.Negocio;
using JAPAMI.Ope_Con_Poliza_Ingresos.Datos;

//using JAPAMI.

public partial class paginas_Contabilidad_Frm_Ope_Con_Polizas : System.Web.UI.Page
{
    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

        try
        {
            if (!IsPostBack)
            {
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                Limpia_Controles();     //Limpia los controles del forma
                ViewState["SortDirection"] = "ASC";
            }
            else
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Text = "";                
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Cancelar_Poliza);
            //Botones.Add(Btn_Mostrar_Popup_Busqueda);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion
    #region(Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11-Julio-2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpia_Controles();             //Limpia los controles de la forma
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Consultar_Cuentas_Contables_Tipo_Polizas(); //Consulta todas las Cuentas Contables y los tipos de pólizas que fueron dadas de alta en la BD
            //Consultar_Programas();
            //Td_Monto.Style.Add("Display", "none");
            //Td_Monto_Text.Style.Add("Display", "none");
            //Td_Programa.Style.Add("Display", "none");
            //Td_Programa_Combo.Style.Add("Display", "none");
            //Consultar_Pre_Polizas();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11-Julio-2011
    /// MODIFICO          : Salvador L. Rea Ayala
    /// FECHA_MODIFICO    : 10/Octubre/2011
    /// CAUSA_MODIFICACION: Se agregaron los nuevos controles para su correcto
    ///                     funcionamiento.
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            //Hd_Prepoliza.Value = "";
           // Txt_Pre_Poliza.Text = "";
            Txt_No_Poliza.Text = "";
            Txt_Concepto_Poliza.Text = "";
            Txt_Fecha_Poliza.Text = "";
            Txt_Concepto_Partida.Text = "";
            Txt_Cuenta_Contable.Text = "";
            Txt_Debe_Partida.Text = "";
            Txt_Haber_Partida.Text = "";
            Txt_Total_Debe.Text = "";
            Txt_Total_Haber.Text = "";
            Txt_No_Partidas.Text = "";
            Txt_Partida_Presupuestal.Value = "";
            Txt_Empleado_Autorizo.Text = "";
            Txt_Prefijo.Text = "";
            Cmb_Descripcion.SelectedIndex = -1;
            Cmb_Tipo_Poliza.SelectedIndex = -1;
            Cmb_Unidad_Responsable.Items.Clear();
            Cmb_Programa.Items.Clear();
            Cmb_Fuente_Financiamiento.Items.Clear();
            Cmb_Fuente_Financiamiento_Egr.Items.Clear();
            Cmb_Nombre_Empleado.Items.Clear();
            Grid_Detalles_Poliza.DataSource = new DataTable();
            Grid_Detalles_Poliza.DataBind();
            Grid_Polizas.DataSource = new DataTable();
            Grid_Polizas.DataBind();
            //Cmb_Momento_Final.SelectedIndex = 0;
            //Cmb_Momento_incial.SelectedIndex = 0;
            //Cmb_Momento_Ingresos.SelectedIndex = 0;
            if (Session["Dt_Partidas_Poliza"] != null)
            {
                Session.Remove("Dt_Partidas_Poliza");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Limpiar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                          si es una alta, modificacion
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          : Salvador L. Rea Ayala
    /// FECHA_MODIFICO    : 10/Octubre/2011
    /// CAUSA_MODIFICACION: Se agregaron los nuevos controles.
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Nuevo.Visible = true;
                    //Btn_Borradores_Guardados.Visible = true;
                    Btn_Modificar.Visible = true;
                    Btn_Imprimir.Visible = false;
                    Btn_Cancelar_Poliza.Visible = true;
                    Btn_Copiar.Visible = true;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Modificar.CausesValidation = false;
                    Btn_Copiar.CausesValidation = false;
                    Txt_Empleado_Creo.Enabled = false;
                    Btn_Password.Visible = false;
                    // Cmb_Empleado_Creo.Enabled = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    //Configuracion_Acceso("Frm_Ope_Con_Polizas.aspx");
                    Div_Carga_Masiva.Style.Value = "display:none";
                    Div_Pre_Poliza.Style.Value = "display:none";
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Cancelar_Poliza.Visible = false;
                    //Btn_Borradores_Guardados.Visible = false;
                    Btn_Copiar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    //Btn_Borrador.Visible = true;
                    Btn_Imprimir.Visible = false;
                    Btn_Modificar.CausesValidation = true;
                    Txt_Empleado_Creo.Enabled = false;
                    Btn_Fecha_Poliza.Enabled = true;
                    //Cmb_Empleado_Creo.Enabled = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Div_Carga_Masiva.Style.Value = "display:none";
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Cancelar_Poliza.Visible = false;
                    Btn_Copiar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Txt_Empleado_Creo.Enabled = false;
                    Btn_Imprimir.Visible = false;
                    //Btn_Borrador.Visible = true;
                    //Btn_Borradores_Guardados.Visible = false;
                    //Cmb_Empleado_Creo.Enabled = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    Div_Carga_Masiva.Style.Value = "display:none";
                    break;

                case "Carga":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Cancelar_Poliza.Visible = false;
                    Btn_Copiar.Visible = false;
                    //Btn_Borradores_Guardados.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Imprimir.Visible = false;
                    Btn_Modificar.CausesValidation = true;
                    Txt_Empleado_Creo.Enabled = false;
                    //Cmb_Empleado_Creo.Enabled = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Div_Carga_Masiva.Style.Value = "display:none";
                    break;

                case "Prepoliza":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Cancelar_Poliza.Visible = false;
                    Btn_Copiar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Imprimir.Visible = false;
                    Btn_Modificar.CausesValidation = true;
                    Txt_Empleado_Creo.Enabled = false;
                    //Btn_Borradores_Guardados.Visible = true;
                    //Cmb_Empleado_Creo.Enabled = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Div_Carga_Masiva.Style.Value = "display:none";
                    break;
            }
            Txt_Concepto_Partida.Enabled = Habilitado;
            Txt_Concepto_Poliza.Enabled = Habilitado;
            Txt_Cuenta_Contable.Enabled = Habilitado;
            Txt_Debe_Partida.Enabled = Habilitado;
            Txt_Fecha_Poliza.Enabled = false;
            Txt_Haber_Partida.Enabled = Habilitado;
            //Txt_Pre_Poliza.Enabled = Habilitado;
            //Btn_Borrador.Enabled = Habilitado;
            //Cmb_Momento_incial.Enabled = Habilitado;
            //Cmb_Momento_Final.Enabled = Habilitado;
            //Cmb_Momento_Ingresos.Enabled = Habilitado;
            Txt_Empleado_Autorizo.Enabled = Habilitado;
            Cmb_Nombre_Empleado.Enabled = Habilitado;
            Btn_Agregar_Partida.Enabled = Habilitado;
            Cmb_Tipo_Poliza.Enabled = Habilitado;
            Cmb_Descripcion.Enabled = Habilitado;
            Btn_Mostrar_Busqueda.Enabled = Habilitado;
            Grid_Detalles_Poliza.Enabled = Habilitado;

            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Montos_Debe_Haber_Poliza
    /// DESCRIPCION : Obtiene los montos totales del Debe y Haber de la póliza de acuerdo
    ///               a las partidas
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Montos_Debe_Haber_Poliza()
    {
        DataTable Dt_Partidas = (DataTable)Session["Dt_Partidas_Poliza"];
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Txt_Total_Haber.Text = "0";
            Txt_Total_Debe.Text = "0";
            Txt_No_Partidas.Text = Dt_Partidas.Rows.Count.ToString();

            //Suma todos los Debe y Haber de la póliza
            foreach (DataRow Registro in Dt_Partidas.Rows)
            {
                Txt_Total_Debe.Text = (Convert.ToDouble(Txt_Total_Debe.Text.ToString()) + Convert.ToDouble(Registro[Ope_Con_Polizas_Detalles.Campo_Debe].ToString())).ToString();
                Txt_Total_Haber.Text = (Convert.ToDouble(Txt_Total_Haber.Text.ToString()) + Convert.ToDouble(Registro[Ope_Con_Polizas_Detalles.Campo_Haber].ToString())).ToString();
            }
            Txt_Total_Debe.Text = String.Format("{0:#,###,##0.00}", Convert.ToDouble((String.IsNullOrEmpty(Txt_Total_Debe.Text.ToString()) ? "0" : Txt_Total_Debe.Text.ToString())));
            Txt_Total_Haber.Text = String.Format("{0:#,###,##0.00}", Convert.ToDouble((String.IsNullOrEmpty(Txt_Total_Haber.Text.ToString()) ? "0" : Txt_Total_Haber.Text.ToString())));
        }
        catch (Exception ex)
        {
            throw new Exception("Montos_Debe_Haber_Poliza " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Partida_Presupuestal
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : Sergio Manuel Gallardo Andrade 
    /// FECHA_CREO  : 20/Abril/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Partida_Presupuestal()
    {
        //Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Disponible = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión a la capa de negocios
        //DataTable Dt_Disponible = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario
        //Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Partida = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Boolean Datos_Validos = true;
        //Lbl_Mensaje_Error.Visible = false;
        //Img_Error.Visible = false;
        //Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";

        //if (Div_Presupuestal.Style.Value == "display:block;")
        //{
        //    if (Div_Ingresos.Style.Value == "display:block;")
        //    {
        //        if (Cmb_Fuente_Financiamiento.SelectedIndex == 0)
        //        {
        //            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La fuente de financiamiento <br>";
        //            Datos_Validos = false;
        //        }
        //    }
        //    if (Div_Egresos.Style.Value == "display:block;")
        //    {
        //        if (Cmb_Fuente_Financiamiento_Egr.SelectedIndex == 0)
        //        {
        //            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La fuente de financiamiento <br>";
        //            Datos_Validos = false;
        //        }
        //        else
        //        {
        //            Rs_Consulta_Disponible.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Egr.SelectedValue;
        //        }
        //        if (Cmb_Unidad_Responsable.SelectedIndex == 0)
        //        {
        //            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La Unidad Responsable <br>";
        //            Datos_Validos = false;
        //        }
        //        else
        //        {
        //            Rs_Consulta_Disponible.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue;
        //        }
        //        if (Cmb_Programa.SelectedIndex == 0)
        //        {
        //            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El Programa <br>";
        //            Datos_Validos = false;
        //        }
        //        else
        //        {
        //            Rs_Consulta_Disponible.P_Programa_ID = Cmb_Programa.SelectedValue;
        //        }
        //        if (Datos_Validos)
        //        {
        //            Rs_Consulta_Disponible.P_Partida_ID = Txt_Partida_Presupuestal.Value;
        //            Rs_Consulta_Disponible.P_Momento = "";// Cmb_Momento_incial.SelectedItem.Text.Trim();
        //            Rs_Consulta_Disponible.P_Momento_Final = "";//Cmb_Momento_Final.SelectedItem.Text.Trim();
        //            Rs_Consulta_Disponible.P_Filtro_Mes = Consultar_Nombre_Mes(String.Format("{0:0#}", (DateTime.Now.Month)));
        //            Dt_Disponible = Rs_Consulta_Disponible.Consulta_Programas();
        //            if (Dt_Disponible.Rows.Count > 0)
        //            {
        //                if (!String.IsNullOrEmpty(Txt_Debe_Partida.Text.Trim()) || !String.IsNullOrEmpty(Txt_Haber_Partida.Text.Trim()))
        //                {
        //                    if ((Txt_Debe_Partida.Text.Trim() != "0" && Txt_Haber_Partida.Text.Trim() == "") ||
        //                        (Txt_Debe_Partida.Text.Trim() == "" && Txt_Haber_Partida.Text.Trim() != "0"))
        //                    {
        //                        if (Txt_Debe_Partida.Text != "")
        //                        {
        //                            if (Convert.ToDouble(Txt_Debe_Partida.Text.Trim()) > 0)
        //                            {
        //                                if (Convert.ToDouble(Txt_Debe_Partida.Text.Trim()) > Convert.ToDouble(Dt_Disponible.Rows[0]["MOMENTO"].ToString()))
        //                                {
        //                                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La partida no tiene " + Cmb_Momento_Final.SelectedItem.Text.Trim() + " suficiente para realizar la partida <br>";
        //                                    Datos_Validos = false;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (Txt_Haber_Partida.Text != "")
        //                                {
        //                                    if (Convert.ToDouble(Txt_Haber_Partida.Text.Trim()) > 0)
        //                                    {
        //                                        if (Convert.ToDouble(Txt_Haber_Partida.Text.Trim()) > Convert.ToDouble(Dt_Disponible.Rows[0]["MOMENTO_FINAL"].ToString()))
        //                                        {
        //                                            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La partida no tiene " + Cmb_Momento_incial.SelectedItem.Text.Trim() + " suficiente para realizar la partida <br>";
        //                                            Datos_Validos = false;
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El monto ingresado no puede ser CERO <br>";
        //                        Datos_Validos = false;
        //                    }
        //                }

        //                else
        //                {
        //                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Ingrese el monto en el campo debe o haber segun el movimiento correspondiente <br>";
        //                    Datos_Validos = false;
        //                }
        //            }
        //        }
        //    }
        //}
        return Datos_Validos;
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Consultar_Nombre_Mes
    // DESCRIPCIÓN: Consultara el nombre del mes correspondiente a la fecha actual
    // RETORNA: 
    // CREO: Hugo Enrique Ramírez Aguilera
    // FECHA_CREO: 17/Noviembre/2011 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public String Consultar_Nombre_Mes(String Mes)
    {
        try
        {
            switch (Mes)
            {
                case "01":
                    Mes = "_ENERO";
                    break;
                case "02":
                    Mes = "_FEBRERO";
                    break;
                case "03":
                    Mes = "_MARZO";
                    break;
                case "04":
                    Mes = "_ABRIL";
                    break;
                case "05":
                    Mes = "_MAYO";
                    break;
                case "06":
                    Mes = "_JUNIO";
                    break;
                case "07":
                    Mes = "_JULIO";
                    break;
                case "08":
                    Mes = "_AGOSTO";
                    break;
                case "09":
                    Mes = "_SEPTIEMBRE";
                    break;
                case "10":
                    Mes = "_OCTUBRE";
                    break;
                case "11":
                    Mes = "_NOVIEMBRE";
                    break;
                default:
                    Mes = "_DICIEMBRE";
                    break;
            }
        }
        catch (Exception Ex)
        {

        }
        return Mes;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos_Poliza
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          : Salvador L. Rea Ayala
    /// FECHA_MODIFICO    : 10/Octubre/2011
    /// CAUSA_MODIFICACION: Se valido que la cuenta contable este en el formato
    ///                     correcto de acuerdo a la BD.
    ///*******************************************************************************
    private Boolean Validar_Datos_Poliza()
    {
        Boolean Datos_Validos = true;
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";

        if (Cmb_Tipo_Poliza.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Tipo Poliza <br>";
            Datos_Validos = false;
        }
        if (string.IsNullOrEmpty(Txt_Fecha_Poliza.Text) || (Txt_Fecha_Poliza.Text.Trim().Equals("__/___/____")))
        {
            Lbl_Mensaje_Error.Text += "+ La Fecha de la Póliza <br>";
            Datos_Validos = false;
        }
        if (String.IsNullOrEmpty(Txt_Concepto_Poliza.Text))
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Concepto de la Poliza <br>";
            Datos_Validos = false;
        }
        if (String.IsNullOrEmpty(Txt_Empleado_Autorizo.Text))
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Empleado que autoriza <br>";
            Datos_Validos = false;
        }
        if (String.IsNullOrEmpty(Txt_No_Partidas.Text))
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Las Partidas de la Poliza <br>";
            Datos_Validos = false;
        }
        else
        {
            if (Convert.ToInt32(Txt_No_Partidas.Text.ToString()) == 0)
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Las Partidas de la Poliza <br>";
                Datos_Validos = false;
            }
        }
        if (String.IsNullOrEmpty(Txt_Total_Haber.Text))
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Debes ingresar partidas con montos en haber <br>";
            Datos_Validos = false;
        }

        if (String.IsNullOrEmpty(Txt_Total_Debe.Text))
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Debes ingresar partidas con montos en Debe <br>";
            Datos_Validos = false;
        }
        // if (Txt_Clave_Presupuestal.Text != "")
        // {
        //    if (Cmb_Area_Funcional.Items.Count == 0)
        //   {
        //       Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Debe ingresar el codigo programatico. <br>";
        //       Datos_Validos = false;
        //   }
        // }

        //***************************************************
        string Mascara_Cuenta_Contable = Consulta_Parametros();
        string Texto = Txt_Cuenta_Contable.Text;
        if (Texto.Length == Mascara_Cuenta_Contable.Length)
        {
            for (int Cont_Caracteres = 0; Cont_Caracteres < Mascara_Cuenta_Contable.Length; Cont_Caracteres++)
            {
                if (Texto.Substring(Cont_Caracteres, 1) == "-")
                {
                    if (Mascara_Cuenta_Contable.Substring(Cont_Caracteres, 1) != Texto.Substring(Cont_Caracteres, 1))
                        Datos_Validos = false;
                }
                else
                {
                    if (Mascara_Cuenta_Contable.Substring(Cont_Caracteres, 1) != "#")
                        Datos_Validos = false;
                }
            }
        }
        //***************************************************
        return Datos_Validos;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Poliza_Borrador
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : Manuel Gallardo Andrade
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private Boolean Validar_Poliza_Borrador()
    {
        Boolean Datos_Validos = true;
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";

        //if (Txt_Pre_Poliza.Text=="")
        //{
        //    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Una Descripcion en la Pre-Poliza <br>";
        //    Datos_Validos = false;
        //}
        if (Cmb_Tipo_Poliza.SelectedIndex <=0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El Tipo de Poliza <br>";
            Datos_Validos = false;
        }
        return Datos_Validos;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Formato_Fecha
    /// DESCRIPCION : Valida el formato de las fechas.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 23/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Formato_Fecha(String Fecha)
    {
        String Cadena_Fecha = @"^(([0-9])|([0-2][0-9])|([3][0-1]))\/(ene|feb|mar|abr|may|jun|jul|ago|sep|oct|nov|dic)\/\d{4}$";
        if (Fecha != null)
        {
            return Regex.IsMatch(Fecha, Cadena_Fecha);
        }
        else
        {
            return false;
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Mascara_Cuenta_Contable
    /// DESCRIPCION : Validar que el formato de la mascara sea el correcto.
    /// PARAMETROS  : TEXTO: Recibe el valor contenido en la propiedad Text de la Txt_Cuenta_Contable
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 19/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Mascara_Cuenta_Contable(string Texto)
    {
        try
        {
            string Mascara_Cuenta_Contable = Consulta_Parametros(); //Almacena el formato actual que debe tener la cuenta contable.
            int Cont_Guiones = 0;   //Almacenara la cantidad de guiones presentes en la mascara.
            for (int Cont_Caracter = 0; Cont_Caracter < Mascara_Cuenta_Contable.Length; Cont_Caracter++)    //Ciclo para contar los guiones
            {
                if (Mascara_Cuenta_Contable.Substring(Cont_Caracter, 1) == "-")
                    Cont_Guiones++;
            }
            if (Texto.Length == Mascara_Cuenta_Contable.Length) //Valida que Texto y la Mascara sean del mismo tamaño
            {
                for (int Cont_Caracteres = 0; Cont_Caracteres < Mascara_Cuenta_Contable.Length; Cont_Caracteres++)  //Recorre Texto para compararlo con cada caracter de la mascara contable
                {
                    if (Texto.Substring(Cont_Caracteres, 1) == "-")
                    {
                        if (Mascara_Cuenta_Contable.Substring(Cont_Caracteres, 1) != Texto.Substring(Cont_Caracteres, 1))
                            throw new Exception("El formato de entrada de la cuenta contable no coincide con lo establecido en la mascara.");
                    }
                    else
                    {
                        if (Mascara_Cuenta_Contable.Substring(Cont_Caracteres, 1) != "#")
                            throw new Exception("El formato de entrada de la cuenta contable no coincide con lo establecido en la mascara.");
                    }
                }
                return (Boolean)true;
            }
            else if (Texto.Length == (Mascara_Cuenta_Contable.Length - Cont_Guiones))
            {
                return (Boolean)true;
            }
            else
            {
                throw new Exception("La Cuenta Contable no cumple con lo requerido.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Validar_Mascara_Cuenta_Contable " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Aplicar_Mascara_Cuenta_Contable
    /// DESCRIPCION : Aplica la Mascara a la Cuenta Contable
    /// PARAMETROS  : Cuenta_Contable: Recibe el numero de cuenta contable3613  1
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 20/Septiembre/2011
    /// MODIFICO          : sergio manuel gallardo andrade
    /// FECHA_MODIFICO    :3-noviembre-2011
    /// CAUSA_MODIFICACION:no funciona correctamente al aplicarle la mascara 
    ///*******************************************************************************
    private string Aplicar_Mascara_Cuenta_Contable(string Cuenta_Contable)
    {
        try
        {
            string Mascara_Cuenta_Contable = String.Empty;
            string Cuenta_Contable_Con_Formato = String.Empty;    //Almacenara la cuenta contable ya estandarizada de acuerdo al formato.
            Int64 Cuenta_Contable_Numerica = 0; //Variable para la cuenta contable

            try
            {
                //COnsultar la mascara de los parametros
                Mascara_Cuenta_Contable = Consulta_Parametros(); //Consulta y almacena la mascara contable actual.

                //Convertir la mascara
                Cuenta_Contable_Numerica = Convert.ToInt64(Cuenta_Contable);
                Cuenta_Contable_Con_Formato = Cuenta_Contable_Numerica.ToString(Mascara_Cuenta_Contable);

                return Cuenta_Contable_Con_Formato;
            }
            catch (Exception ex)
            {
                throw new Exception("Aplicar_Mascara_Cuenta_Contable " + ex.Message.ToString(), ex);
            }
            ////string Mascara_Cuenta_Contable = Consulta_Parametros(); //Consulta y almacena la mascara contable actual.
            ////string Cuenta_Contable_Con_Formato = "";    //Almacenara la cuenta contable ya estandarizada de acuerdo al formato.
            ////Boolean Primer_Numero = true;   //Detecta si el primer caracter es un numero.
            ////int Caracteres_Extraidos_Cuenta_Contable = 0;  //Variable que almacena la cantidad de caracteres extraidos de la cuenta.
            ////int contador_nuevo = 0;
            ////int Inicio_Extraccion = 0; //Variable que almacena el inicio de la cadena a extraer.
            ////int Fin_Extraccion = 0;    //Variable que almacena el fin de la cadena a extraer.
            ////for (int Cont_Desplazamiento = 0; Cont_Desplazamiento < Mascara_Cuenta_Contable.Length; Cont_Desplazamiento++)  //Ciclo de desplazamiento
            ////{
            ////    if (Primer_Numero == true && Mascara_Cuenta_Contable.Substring(Cont_Desplazamiento, 1) == "#")  //Detecta el primer numero dentro de la mascara contable
            ////    {
            ////        if (Cont_Desplazamiento == 0)
            ////            Inicio_Extraccion = Cont_Desplazamiento;
            ////        else
            ////            Inicio_Extraccion = contador_nuevo;
            ////        Primer_Numero = false;
            ////    }
            ////    if (Mascara_Cuenta_Contable.Substring(Cont_Desplazamiento, 1) != "#") //Detecta si el caracter es diferente de un numero en la mascara contable.
            ////    {
            ////        Fin_Extraccion = Cont_Desplazamiento;
            ////        if (Inicio_Extraccion == 0)
            ////        {
            ////            Cuenta_Contable_Con_Formato += Cuenta_Contable.Substring(Inicio_Extraccion, Fin_Extraccion - Inicio_Extraccion);
            ////            Caracteres_Extraidos_Cuenta_Contable = Fin_Extraccion - Inicio_Extraccion;
            ////        }
            ////        else
            ////        {
            ////            Cuenta_Contable_Con_Formato += Cuenta_Contable.Substring(Inicio_Extraccion, Fin_Extraccion - Inicio_Extraccion - contador_nuevo);
            ////            Caracteres_Extraidos_Cuenta_Contable += Fin_Extraccion - Inicio_Extraccion - contador_nuevo;
            ////        }
            ////        if (contador_nuevo < Cuenta_Contable.Length)
            ////        {
            ////            contador_nuevo = contador_nuevo + 1;
            ////        }
            ////        Primer_Numero = true;
            ////        Cuenta_Contable_Con_Formato += "-";

            ////    }
            ////}
            ////if (Caracteres_Extraidos_Cuenta_Contable != Cuenta_Contable.Length) //Concatena los caracteres sobrantes en la cuenta contable.
            ////{
            ////    Cuenta_Contable_Con_Formato += Cuenta_Contable.Substring(Caracteres_Extraidos_Cuenta_Contable, Cuenta_Contable.Length - Caracteres_Extraidos_Cuenta_Contable);
            ////}
            ////return Cuenta_Contable_Con_Formato;
        }
        catch (Exception ex)
        {
            throw new Exception("Aplicar_Mascara_Cuenta_Contable " + ex.Message.ToString(), ex);
        }
    }
    /////*******************************************************************************
    ///// NOMBRE DE LA FUNCION: Convertir_Datos_Poliza
    ///// DESCRIPCION : Busca los IDs correspondientes para formar el codigo programatico
    ///// PARAMETROS  : Dt_Poliza_Detalles: Almacena los datos capturados desde el Excel.
    ///// CREO        : Salvador L. Rea Ayala
    ///// FECHA_CREO  : 10/Octubre/2011
    ///// MODIFICO          :
    ///// FECHA_MODIFICO    :
    ///// CAUSA_MODIFICACION:
    /////*******************************************************************************
    //private void Convertir_Datos_Poliza(DataTable Dt_Poliza_Detalles)
    //{
    //    Cls_Cat_Dependencias_Negocio Rs_Dependencias = new Cls_Cat_Dependencias_Negocio();  //Variable para Dependencias.
    //    Cls_Cat_SAP_Fuente_Financiamiento_Negocio Rs_Financiamiento = new Cls_Cat_SAP_Fuente_Financiamiento_Negocio();  //Variable para Financiamiento.
    //    Cls_Ope_Con_Polizas_Negocio Rs_Area = new Cls_Ope_Con_Polizas_Negocio();  //Variable para el Area Funcional.
    //    Cls_Cat_Com_Proyectos_Programas_Negocio Rs_Proyectos = new Cls_Cat_Com_Proyectos_Programas_Negocio();   //Variable para los Proyectos.
    //    Cls_Ope_Con_Polizas_Negocio Rs_Partidas = new Cls_Ope_Con_Polizas_Negocio();  //Variable para las Partidas.
    //    Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable para las Cuentas Contables.
    //    Cls_Ope_Con_Polizas_Negocio Rs_Consulta_ID = new Cls_Ope_Con_Polizas_Negocio();
    //    Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Descripcion = new Cls_Cat_Con_Cuentas_Contables_Negocio();
    //    DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los datos de la póliza que fueron proporcionados por el usuario
    //    DataTable Dt_Uso_Multiple = null;
    //    int Cont_Rows = 1;
    //    Int32 Clave = 0;
    //    int Contador = 2;
    //    double Total_Debe = 0;
    //    double Total_Haber = 0;
    //    DataTable Dt_Partida_ID = new DataTable();
    //    DataTable Dt_Concepto = new DataTable();
    //    DataTable Dt_Consulta_ID = new DataTable();
    //    String PEP = "";
    //    String Tipo = "";
    //    String Concepto = String.Empty;
    //    Boolean Estado_Tabla = true;
    //    Boolean Estado_Momentos = true;
    //    String Cuenta_Id = "" ;

    //    #region Formato de fechas
    //    String[] Formatos = new String[]{
    //                "MM/dd/yyyy",
    //                "MMM/dd/yyyy",
    //                "dd/MM/yyyy",
    //                "dd/MMM/yyyy",
    //                "dd/MM/yy",
    //                "d/MM/yy",
    //                "d/M/yy",
    //                "dd-MM-yy",
    //                "yyyy-MM-dd",
    //                "dd-MMM-yyyy",
    //                "d-M-yyyy",
    //                "d-M-yy",
    //                "yyyy/MMM/dd",
    //                "yy/MM/dd",
    //                "yy/M/dd",
    //                "dd/MMMM/yyyy",
    //                "MMMM/dd/yyyy",
    //                "yyyy/MMMM/dd",
    //                "ddMMyyyy",
    //                "MMddyyyy",
    //                "ddMMMyyyy",
    //                "MMMddyyyy",
    //                "dddd, dd MMMM yyyy",
    //                "dddd, dd MMMM yyyy HH:mm",
    //                "dddd, dd MMMM yyyy HH:mm:ss",
    //                "MM/dd/yyyy HH:mm",
    //                "MM/dd/yyyy HH:mm:ss",
    //                "MMMM dd",
    //                "ddd, dd MMM yyyy",
    //                "dddd, dd MMMM yyyy HH:mm:ss",
    //                "yyyy MMMM",
    //                "yyyy-MM-dd HH:mm:ss",
    //                "MMMMddyyyy",
    //                "ddMMMMyyyy",
    //                "yyyyddMMMM",
    //                "MMddyy",
    //                "ddMMyy",
    //                "yyMMdd"};
    //    #endregion

    //    try
    //    {
    //        //Agrega los campos que va a contener el DataTable
    //        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
    //        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
    //        Dt_Partidas_Polizas.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
    //        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
    //        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
    //        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
    //        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
    //        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
    //        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
    //        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
    //        Dt_Partidas_Polizas.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
    //        Dt_Partidas_Polizas.Columns.Add("MOMENTO_FINAL", typeof(System.String));

    //        DataRow row;

    //        if (Dt_Poliza_Detalles is DataTable)
    //        {
    //            if (Dt_Poliza_Detalles.Rows.Count > 0)
    //            {
    //                if (Dt_Poliza_Detalles.Columns.Contains("Momento_Inicial") &&
    //                    Dt_Poliza_Detalles.Columns.Contains("Momento_Final"))
    //                {

    //                    foreach (DataRow Registro in Dt_Poliza_Detalles.Rows)
    //                    {

    //                        row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

    //                        //  se busca que la cuenta exista
    //                        Rs_Cuentas.P_Cuenta = Registro["Cuenta"].ToString();
    //                        Dt_Uso_Multiple = Rs_Cuentas.Consulta_Existencia_Cuenta_Contable();
    //                        if (Dt_Uso_Multiple.Rows.Count > 0)
    //                        {
    //                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Uso_Multiple.Rows[0][0].ToString();
    //                            Cuenta_Id = Dt_Uso_Multiple.Rows[0][0].ToString();
    //                            Dt_Uso_Multiple = null;
    //                        }

    //                        //  se llena la tabla con la informacion de la cuenta

    //                        //  para la informacion principal que aparece en los txt
    //                        if (Convert.ToInt32(Cont_Rows.ToString()) == 1)
    //                        {
    //                            Concepto = Registro["Texto de cabecera"].ToString();
    //                            Txt_Concepto_Poliza.Text = Concepto;

    //                            DateTime Date = DateTime.ParseExact(Registro["Fecha"].ToString(), Formatos, CultureInfo.CurrentCulture, DateTimeStyles.AllowWhiteSpaces);
    //                            Txt_Fecha_Poliza.Text = String.Format("{0:dd/MMM/yyyy}", Date);
    //                        }

    //                        if (!String.IsNullOrEmpty(Registro["Clave"].ToString()))
    //                            Clave = Convert.ToInt32(Registro["Clave"].ToString());

    //                        //  para el momento inicial y final
    //                        if (!String.IsNullOrEmpty(Registro["Momento_Inicial"].ToString()) &&
    //                            !String.IsNullOrEmpty(Registro["Momento_Final"].ToString()))
    //                        {
    //                            //  para el momento inicial
    //                            if (Registro["Momento_Inicial"].ToString().Trim().ToUpper() == "DISPONIBLE" ||
    //                                Registro["Momento_Inicial"].ToString().Trim().ToUpper() == "COMPROMETIDO" ||
    //                                Registro["Momento_Inicial"].ToString().Trim().ToUpper() == "DEVENGADO" ||
    //                                Registro["Momento_Inicial"].ToString().Trim().ToUpper() == "EJERCIDO" ||
    //                                Registro["Momento_Inicial"].ToString().Trim().ToUpper() == "PAGADO")
    //                            {
    //                                row["MOMENTO_INICIAL"] = (Registro["Momento_Inicial"].ToString().Trim().ToUpper());
    //                            }

    //                            else
    //                            {
    //                                //  si no pertenece a los momentos se sale
    //                                Lbl_Mensaje_Error.Text = "El registro " + Contador + " no contiene un momento iniciala valido verifique el archivo <br>";
    //                                Estado_Tabla = false;
    //                                break;
    //                            }

    //                            if (Registro["Momento_Final"].ToString().Trim().ToUpper() == "DISPONIBLE" ||
    //                                Registro["Momento_Final"].ToString().Trim().ToUpper() == "COMPROMETIDO" ||
    //                                Registro["Momento_Final"].ToString().Trim().ToUpper() == "DEVENGADO" ||
    //                                Registro["Momento_Final"].ToString().Trim().ToUpper() == "EJERCIDO" ||
    //                                Registro["Momento_Final"].ToString().Trim().ToUpper() == "PAGADO")
    //                            {
    //                                //  para el momento 
    //                                row["MOMENTO_FINAL"] = (Registro["Momento_Final"].ToString().Trim().ToUpper());
    //                            }
    //                            else
    //                            {
    //                                //  si no pertenece a los momentos se sale
    //                                Lbl_Mensaje_Error.Text = "El registro " + Contador + " no contiene un momento final valido verifique el archivo <br>";
    //                                Estado_Tabla = false;
    //                                break;
    //                            }
    //                        }
    //                        else
    //                        {
    //                            //  si no se encuentra informacion en algun registro no permitira cargarse el el gird
    //                            Lbl_Mensaje_Error.Text = "El registro " + Contador + " no contiene el momento iniciala y/o final verifique el archivo <br>";
    //                            Estado_Tabla = false;
    //                            break;
    //                        }


    //                        if (Clave == 40 || Clave == 1)
    //                        {
    //                            Rs_Cuentas_Descripcion.P_Cuenta_Contable_ID =Cuenta_Id;
    //                            Dt_Concepto = Rs_Cuentas_Descripcion.Consulta_Cuentas_Contables();
    //                            if (Dt_Concepto.Rows.Count > 0)
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Dt_Concepto.Rows[0]["Descripcion"].ToString();
    //                            }
    //                            else
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto;
    //                            }
    //                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Cont_Rows.ToString();
    //                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Registro["Importe"].ToString();
    //                            Total_Debe += Convert.ToDouble(Registro["Importe"].ToString());
    //                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
    //                            row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Aplicar_Mascara_Cuenta_Contable(Registro["Cuenta"].ToString());

    //                            Rs_Consulta_ID.P_Clave_Cuenta_Contable = Registro["Cuenta"].ToString();
    //                            Dt_Partida_ID = Rs_Consulta_ID.Consulta_Cuenta_Partida_ID();
    //                            Dt_Consulta_ID = new DataTable();
    //                            if (Dt_Partida_ID.Rows.Count > 0)
    //                            {
    //                                foreach (DataRow Registro_Partida_ID in Dt_Partida_ID.Rows)
    //                                {
    //                                    row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Registro_Partida_ID[Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString().Trim();
    //                                }
    //                                Tipo = Dt_Partida_ID.Rows[0]["TIPO_PRESUPUESTAL"].ToString().Trim();
    //                            }
    //                            if (Registro["Fondo"].ToString() == "/")
    //                            {
    //                                Rs_Consulta_ID.P_Clave_Fte_Financiamiento = "";
    //                            }
    //                            else
    //                            {
    //                                Rs_Consulta_ID.P_Clave_Fte_Financiamiento = Registro["Fondo"].ToString().Trim();
    //                            }

    //                            Rs_Consulta_ID.P_Partida_ID = row[Ope_Con_Polizas_Detalles.Campo_Partida_ID].ToString();
    //                            if (Tipo == "INGRESOS")
    //                            {
    //                                Dt_Consulta_ID = Rs_Consulta_ID.Consulta_ID_Fte_Financiamiento();
    //                            }
    //                            else
    //                            {
    //                                if (Tipo == "EGRESOS")
    //                                {
    //                                    Dt_Consulta_ID = Rs_Consulta_ID.Consulta_ID_Fte_Financiamiento_Egr();
    //                                }
    //                            }
    //                            if (Dt_Consulta_ID.Rows.Count > 0)
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Consulta_ID.Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
    //                            }
    //                            else
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = "";
    //                            }

    //                            if (Registro["Uni# Resp"].ToString() == "/")
    //                            {
    //                                Rs_Consulta_ID.P_Clave_Dependencia = "";
    //                            }
    //                            else
    //                            {
    //                                Rs_Consulta_ID.P_Clave_Dependencia = Registro["Uni# Resp"].ToString().Trim();
    //                            }

    //                            Rs_Consulta_ID.P_Partida_ID = row[Ope_Con_Polizas_Detalles.Campo_Partida_ID].ToString();
    //                            Dt_Consulta_ID = new DataTable();
    //                            if (Tipo == "EGRESOS")
    //                            {
    //                                Dt_Consulta_ID = Rs_Consulta_ID.Consulta_Dependencia();
    //                            }
    //                            if (Dt_Consulta_ID.Rows.Count > 0)
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Consulta_ID.Rows[0]["DEPENDENCIA"].ToString().Trim();
    //                            }
    //                            else
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = "";
    //                            }
    //                            Dt_Consulta_ID = new DataTable();
    //                            if (Registro["Elemento PEP"].ToString() == "/")
    //                            {
    //                                PEP = "";
    //                            }
    //                            else
    //                            {
    //                                PEP = Registro["Elemento PEP"].ToString();
    //                                PEP = PEP.Substring(6);
    //                            }
    //                            Rs_Consulta_ID.P_Partida_ID = row[Ope_Con_Polizas_Detalles.Campo_Partida_ID].ToString();
    //                            Rs_Consulta_ID.P_Clave_Programa = PEP;
    //                            Rs_Consulta_ID.P_Clave_Dependencia = row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID].ToString();
    //                            Dt_Consulta_ID = new DataTable();
    //                            if (Tipo == "EGRESOS")
    //                            {
    //                                Dt_Consulta_ID = Rs_Consulta_ID.Consulta_Programas();
    //                            }
    //                            if (Dt_Consulta_ID.Rows.Count > 0)
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Consulta_ID.Rows[0]["PROGRAMA_ID"].ToString().Trim();
    //                            }
    //                            else
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = "";
    //                            }

    //                        }// fin del Clave == 40 || Clave == 1

    //                        else
    //                        {
    //                            Rs_Cuentas_Descripcion.P_Cuenta_Contable_ID = "";
    //                            Dt_Concepto = new DataTable();
    //                            Rs_Cuentas_Descripcion.P_Cuenta_Contable_ID = Cuenta_Id;
    //                            Dt_Concepto = Rs_Cuentas_Descripcion.Consulta_Cuentas_Contables();
    //                            if (Dt_Concepto.Rows.Count > 0)
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Dt_Concepto.Rows[0]["Descripcion"].ToString();
    //                            }
    //                            else
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto;
    //                            }
    //                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Cont_Rows.ToString();
    //                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
    //                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Registro["Importe"].ToString();
    //                            Total_Haber += Convert.ToDouble(Registro["Importe"].ToString());
    //                            //row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto;
    //                            row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Aplicar_Mascara_Cuenta_Contable(Registro["Cuenta"].ToString());
    //                            //Contador = 0;
    //                            Rs_Consulta_ID.P_Clave_Cuenta_Contable = Registro["Cuenta"].ToString(); ;
    //                            Dt_Partida_ID = Rs_Consulta_ID.Consulta_Cuenta_Partida_ID();
    //                            Dt_Consulta_ID = new DataTable();
    //                            if (Dt_Partida_ID.Rows.Count > 0)
    //                            {
    //                                foreach (DataRow Registro_Partida_ID in Dt_Partida_ID.Rows)
    //                                {
    //                                    row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Registro_Partida_ID[Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString().Trim();
    //                                }
    //                                Tipo = Dt_Partida_ID.Rows[0]["TIPO_PRESUPUESTAL"].ToString().Trim();
    //                            }
    //                            if (Registro["Fondo"].ToString() == "/")
    //                            {
    //                                Rs_Consulta_ID.P_Clave_Fte_Financiamiento = "";
    //                            }
    //                            else
    //                            {
    //                                Rs_Consulta_ID.P_Clave_Fte_Financiamiento = Registro["Fondo"].ToString().Trim();
    //                            }

    //                            Rs_Consulta_ID.P_Partida_ID = row[Ope_Con_Polizas_Detalles.Campo_Partida_ID].ToString();
    //                            Dt_Consulta_ID = new DataTable();
    //                            if (Tipo == "INGRESOS")
    //                            {
    //                                Dt_Consulta_ID = Rs_Consulta_ID.Consulta_ID_Fte_Financiamiento();
    //                            }
    //                            else
    //                            {
    //                                if (Tipo == "EGRESOS")
    //                                {
    //                                    Dt_Consulta_ID = Rs_Consulta_ID.Consulta_ID_Fte_Financiamiento_Egr();
    //                                }
    //                            }
    //                            if (Dt_Consulta_ID.Rows.Count > 0)
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Consulta_ID.Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
    //                            }
    //                            else
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = "";
    //                            }

    //                            if (Registro["Uni# Resp"].ToString() == "/")
    //                            {
    //                                Rs_Consulta_ID.P_Clave_Dependencia = "";
    //                            }
    //                            else
    //                            {
    //                                Rs_Consulta_ID.P_Clave_Dependencia = Registro["Uni# Resp"].ToString().Trim();
    //                            }

    //                            Rs_Consulta_ID.P_Partida_ID = row[Ope_Con_Polizas_Detalles.Campo_Partida_ID].ToString();
    //                            Dt_Consulta_ID = new DataTable();
    //                            if (Tipo == "EGRESOS")
    //                            {
    //                                Dt_Consulta_ID = Rs_Consulta_ID.Consulta_Dependencia();
    //                            }
    //                            if (Dt_Consulta_ID.Rows.Count > 0)
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Consulta_ID.Rows[0]["DEPENDENCIA"].ToString().Trim();
    //                            }
    //                            else
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = "";
    //                            }
    //                            Dt_Consulta_ID = new DataTable();
    //                            if (Registro["Elemento PEP"].ToString() == "/")
    //                            {
    //                                PEP = "";
    //                            }
    //                            else
    //                            {
    //                                PEP = Registro["Elemento PEP"].ToString();
    //                                PEP = PEP.Substring(6);
    //                            }
    //                            Rs_Consulta_ID.P_Partida_ID = row[Ope_Con_Polizas_Detalles.Campo_Partida_ID].ToString();
    //                            Rs_Consulta_ID.P_Clave_Programa = PEP;
    //                            Rs_Consulta_ID.P_Clave_Dependencia = row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID].ToString();
    //                            Dt_Consulta_ID = new DataTable();
    //                            if (Tipo == "EGRESOS")
    //                            {
    //                                Dt_Consulta_ID = Rs_Consulta_ID.Consulta_Programas();
    //                            }
    //                            if (Dt_Consulta_ID.Rows.Count > 0)
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Consulta_ID.Rows[0]["PROGRAMA_ID"].ToString().Trim();
    //                            }
    //                            else
    //                            {
    //                                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = "";
    //                            }

    //                        }// fin del else
    //                        Cls_Ope_Con_Polizas_Negocio Rs_Poliza = new Cls_Ope_Con_Polizas_Negocio();
    //                        Rs_Poliza.P_Partida_ID = row[Ope_Con_Polizas_Detalles.Campo_Partida_ID].ToString();
    //                        Rs_Poliza.P_Programa_ID = row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID].ToString();
    //                        Rs_Poliza.P_Dependencia_ID = row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID].ToString();
    //                        if (!String.IsNullOrEmpty(row[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) && Convert.ToDouble(row[Ope_Con_Polizas_Detalles.Campo_Debe]) > 0)
    //                        {
    //                            Rs_Poliza.P_Validar_Saldo = row[Ope_Con_Polizas_Detalles.Campo_Debe].ToString();
    //                            Rs_Poliza.P_Fuente_Financiamiento_ID = row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID].ToString();
    //                        }
    //                        //if (Rs_Poliza.Consulta_Saldo_Disponible_Poliza())
    //                        //{
                               
    //                            Dt_Partidas_Polizas.Rows.Add(row);
    //                            Dt_Partidas_Polizas.AcceptChanges();
    //                            Contador++;
    //                        //}
    //                        Cont_Rows++;

    //                    }// fin del foreach

    //                }// fin del if de si existen la columna estado inicial y final

    //                else
    //                {
    //                    Lbl_Mensaje_Error.Text = "El archivo no contiene las columnas de Momento_Inicial y/o Momento_Final <br>";
    //                    Estado_Tabla = false;
    //                }// fin del else de si existen la columna estado inicial y final

    //            }// fin del datatable count

    //            else
    //            {
    //                Lbl_Mensaje_Error.Text = "No contiene informacion el archivo de excel <br>";
    //                Estado_Tabla = false;
    //            } //    else de count

    //        }// fin del if is datatable

    //        if (Estado_Tabla == true)
    //        {
    //            Session["Dt_Partidas_Poliza"] = Dt_Partidas_Polizas;//Agrega los valores del registro a la sesión
    //            Txt_No_Partidas.Text = (Cont_Rows - 1).ToString(); //Inserta el numero de partidas
    //            Txt_Total_Debe.Text = Total_Debe.ToString();    //Inserta el total del debe
    //            Txt_Total_Haber.Text = Total_Haber.ToString();  //Inserta el tottal de haber

    //            Grid_Detalles_Poliza.Columns[1].Visible = true;
    //            Grid_Detalles_Poliza.Columns[2].Visible = true;
    //            Grid_Detalles_Poliza.Columns[3].Visible = true;
    //            Grid_Detalles_Poliza.Columns[4].Visible = true;
    //            Grid_Detalles_Poliza.Columns[5].Visible = true;
    //            Grid_Detalles_Poliza.Columns[6].Visible = true;
    //            Grid_Detalles_Poliza.Columns[13].Visible = true;
    //            Grid_Detalles_Poliza.Columns[14].Visible = true;
    //            Grid_Detalles_Poliza.DataSource = Dt_Partidas_Polizas; //Agrega los valores de todas las partidas que se tienen al grid
    //            Grid_Detalles_Poliza.DataBind();
    //            Grid_Detalles_Poliza.Columns[1].Visible = false;
    //            Grid_Detalles_Poliza.Columns[13].Visible = false;
    //            Grid_Detalles_Poliza.Columns[14].Visible = false;
                
    //            //poner las condiciones para la carga
    //            Habilitar_Controles("Carga");               
    //            Txt_Empleado_Creo.Text = Cls_Sessiones.Nombre_Empleado;
    //        }
    //        else
    //        {
    //            Lbl_Mensaje_Error.Visible = true;
    //            Img_Error.Visible = true;
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception("Convertir_Datos_Poliza " + ex.Message.ToString(), ex);
    //    }
    //}

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Convertir_Datos_Poliza
    /// DESCRIPCION : Busca los IDs correspondientes para formar el codigo programatico
    /// PARAMETROS  : Dt_Poliza_Detalles: Almacena los datos capturados desde el Excel.
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 10/Octubre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Convertir_Datos_Poliza(DataTable Dt_Poliza_Detalles)
    {
        Cls_Cat_Dependencias_Negocio Rs_Dependencias = new Cls_Cat_Dependencias_Negocio();  //Variable para Dependencias.
        Cls_Cat_SAP_Fuente_Financiamiento_Negocio Rs_Financiamiento = new Cls_Cat_SAP_Fuente_Financiamiento_Negocio();  //Variable para Financiamiento.
        Cls_Ope_Con_Polizas_Negocio Rs_Area = new Cls_Ope_Con_Polizas_Negocio();  //Variable para el Area Funcional.
        Cls_Cat_Com_Proyectos_Programas_Negocio Rs_Proyectos = new Cls_Cat_Com_Proyectos_Programas_Negocio();   //Variable para los Proyectos.
        Cls_Ope_Con_Polizas_Negocio Rs_Partidas = new Cls_Ope_Con_Polizas_Negocio();  //Variable para las Partidas.
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable para las Cuentas Contables.
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_ID = new Cls_Ope_Con_Polizas_Negocio();
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Descripcion = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los datos de la póliza que fueron proporcionados por el usuario
        DataTable Dt_Uso_Multiple = null;
        int Cont_Rows = 1;
        Int32 Clave = 0;
        int Contador = 2;
        double Total_Debe = 0;
        double Total_Haber = 0;
        DataTable Dt_Partida_ID = new DataTable();
        DataTable Dt_Concepto = new DataTable();
        DataTable Dt_Consulta_ID = new DataTable();
        String PEP = "";
        String Tipo = "";
        String Concepto = String.Empty;
        Boolean Estado_Tabla = true;
        Boolean Estado_Momentos = true;
        String Cuenta_Id = "";

        #region Formato de fechas
        String[] Formatos = new String[]{
                    "MM/dd/yyyy",
                    "MMM/dd/yyyy",
                    "dd/MM/yyyy",
                    "dd/MMM/yyyy",
                    "dd/MM/yy",
                    "d/MM/yy",
                    "d/M/yy",
                    "dd-MM-yy",
                    "yyyy-MM-dd",
                    "dd-MMM-yyyy",
                    "d-M-yyyy",
                    "d-M-yy",
                    "yyyy/MMM/dd",
                    "yy/MM/dd",
                    "yy/M/dd",
                    "dd/MMMM/yyyy",
                    "MMMM/dd/yyyy",
                    "yyyy/MMMM/dd",
                    "ddMMyyyy",
                    "MMddyyyy",
                    "ddMMMyyyy",
                    "MMMddyyyy",
                    "dddd, dd MMMM yyyy",
                    "dddd, dd MMMM yyyy HH:mm",
                    "dddd, dd MMMM yyyy HH:mm:ss",
                    "MM/dd/yyyy HH:mm",
                    "MM/dd/yyyy HH:mm:ss",
                    "MMMM dd",
                    "ddd, dd MMM yyyy",
                    "dddd, dd MMMM yyyy HH:mm:ss",
                    "yyyy MMMM",
                    "yyyy-MM-dd HH:mm:ss",
                    "MMMMddyyyy",
                    "ddMMMMyyyy",
                    "yyyyddMMMM",
                    "MMddyy",
                    "ddMMyy",
                    "yyMMdd"};
        #endregion

        try
        {
            //Agrega los campos que va a contener el DataTable
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            //Dt_Partidas_Polizas.Columns.Add("CODIGO_PROGRAMATICO", typeof(System.String));
            //Dt_Partidas_Polizas.Columns.Add("DEPENDENCIA_ID", typeof(System.String));
            //Dt_Partidas_Polizas.Columns.Add("FUENTE_FINANCIAMIENTO_ID", typeof(System.String));
            //Dt_Partidas_Polizas.Columns.Add("AREA_FUNCIONAL_ID", typeof(System.String));
            //Dt_Partidas_Polizas.Columns.Add("PROYECTO_PROGRAMA_ID", typeof(System.String));
            //Dt_Partidas_Polizas.Columns.Add("PARTIDA_ID", typeof(System.String));
            //Dt_Partidas_Polizas.Columns.Add("COMPROMISO_ID", typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("MOMENTO_FINAL", typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("Nombre_Cuenta", typeof(System.String));
            DataRow row;

            if (Dt_Poliza_Detalles is DataTable)
            {
                if (Dt_Poliza_Detalles.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Poliza_Detalles.Rows)
                    {

                        row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                        //  se busca que la cuenta exista
                        Rs_Cuentas.P_Cuenta = Registro["Cuenta"].ToString();
                        Dt_Uso_Multiple = new DataTable();
                        Dt_Uso_Multiple = Rs_Cuentas.Consulta_Existencia_Cuenta_Contable();
                        if (Dt_Uso_Multiple.Rows.Count > 0)
                        {
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Uso_Multiple.Rows[0][0].ToString();
                            row["Nombre_Cuenta"] = Dt_Uso_Multiple.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Text = "No existe la cuenta contable " + Registro["Cuenta"].ToString().Trim() + " (renglon " + Cont_Rows.ToString().Trim() + "), Favor de darla de alta primero <br />";
                            Estado_Tabla = false;
                            break;
                        }

                        //  se llena la tabla con la informacion de la cuenta

                        //  para la informacion principal que aparece en los txt
                        if (Convert.ToInt32(Cont_Rows.ToString()) == 1)
                        {
                            Concepto = Registro["Concepto"].ToString();
                            Txt_Concepto_Poliza.Text = Concepto;
                        }

                        //     Codigo_Programatico = "";
                        #region (Codigo_Programatico)
                        Rs_Cuentas.P_Cuenta = Registro["Cuenta"].ToString();
                        Dt_Uso_Multiple = Rs_Cuentas.Consulta_Existencia_Cuenta_Contable();
                        if (Dt_Uso_Multiple.Rows.Count > 0)
                        {
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Uso_Multiple.Rows[0][0].ToString();
                            Dt_Uso_Multiple = null;
                        }
                        #endregion   //Obtiene los IDs del codigo programatico
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = Cont_Rows.ToString();
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = Registro["DEBE"].ToString();
                        Total_Debe += Convert.ToDouble(Registro["DEBE"].ToString());
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = Registro["HABER"].ToString();
                        Total_Haber += Convert.ToDouble(Registro["HABER"].ToString());
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Registro["CONCEPTO"].ToString();
                        row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Aplicar_Mascara_Cuenta_Contable(Registro["Cuenta"].ToString());
                        row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = "";
                        row["MOMENTO_INICIAL"] = "COMPROMETIDO";
                        row["MOMENTO_INICIAL"] = "DEVENGADO";

                        Cont_Rows++;
                        Dt_Partidas_Polizas.Rows.Add(row);
                        Dt_Partidas_Polizas.AcceptChanges();
                        Contador++;

                    }// fin del foreach


                }// fin del datatable count

                else
                {
                    Lbl_Mensaje_Error.Text = "No contiene informacion el archivo de excel <br>";
                    Estado_Tabla = false;
                } //    else de count

            }// fin del if is datatable

            if (Estado_Tabla == true)
            {
                Session["Dt_Partidas_Poliza"] = Dt_Partidas_Polizas;//Agrega los valores del registro a la sesión
                Txt_No_Partidas.Text = (Cont_Rows - 1).ToString(); //Inserta el numero de partidas
                Txt_Total_Debe.Text = Math.Round(Total_Debe, 2).ToString();    //Inserta el total del debe
                Txt_Total_Haber.Text = Math.Round(Total_Haber, 2).ToString();  //Inserta el tottal de haber

                Grid_Detalles_Poliza.Columns[1].Visible = true;
                Grid_Detalles_Poliza.Columns[6].Visible = true;
                Grid_Detalles_Poliza.Columns[7].Visible = true;
                Grid_Detalles_Poliza.Columns[8].Visible = true;
                Grid_Detalles_Poliza.Columns[9].Visible = true;
                Grid_Detalles_Poliza.Columns[10].Visible = true;
                Grid_Detalles_Poliza.Columns[11].Visible = true;
                Grid_Detalles_Poliza.Columns[13].Visible = true;
                Grid_Detalles_Poliza.Columns[14].Visible = true;
                Grid_Detalles_Poliza.DataSource = Dt_Partidas_Polizas; //Agrega los valores de todas las partidas que se tienen al grid
                Grid_Detalles_Poliza.DataBind();
                Grid_Detalles_Poliza.Columns[1].Visible = false;
                Grid_Detalles_Poliza.Columns[6].Visible = false;
                Grid_Detalles_Poliza.Columns[7].Visible = false;
                Grid_Detalles_Poliza.Columns[8].Visible = false;
                Grid_Detalles_Poliza.Columns[9].Visible = false;
                Grid_Detalles_Poliza.Columns[10].Visible = false;
                Grid_Detalles_Poliza.Columns[11].Visible = false;
                Grid_Detalles_Poliza.Columns[13].Visible = false;
                Grid_Detalles_Poliza.Columns[14].Visible = false;
                Habilitar_Controles("Carga");
                //Cmb_Tipo_Poliza.SelectedIndex = Cmb_Tipo_Poliza.Items.IndexOf(Cmb_Tipo_Poliza.Items.FindByText("EGRESOS"));
                Txt_Empleado_Creo.Text = Cls_Sessiones.Nombre_Empleado;
                Txt_Empleado_Autorizo.Text = Convert.ToInt64(Cls_Sessiones.No_Empleado).ToString().Trim();
                Txt_Empleado_Autorizo_TextChanged(Txt_Empleado_Autorizo, new EventArgs());
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Convertir_Datos_Poliza " + ex.Message.ToString(), ex);
        }
    }


    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Interpretar_Excel
    /// DESCRIPCION : Interpreta los datos contenidos por el archivo de Excel.
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 13/Octubre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Interpretar_Excel(String Rta)
    {
        //Declaracion de variables
        DataSet Ds_Informacion = new DataSet(); //Dataset para leer el archivo de Excel
        DataTable Dt_Poliza_Detalles = new DataTable();    //Almacena los detalles de la poliza
        IExcelDataReader excelReader; //variable para el lector de los datos del excel
        Stream archivoExcel; //Variable para el archivo

        try
        {
            //Abrir el archivo
            archivoExcel = File.Open(Rta,FileMode.Open, FileAccess.Read);

            //verificar la extension del archivo para asignar el tipo de lectura a realizar
            if (Rta.ToUpper().EndsWith(".XLSX") == true)
            {
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(archivoExcel);
            }
            else
            {
                excelReader = ExcelReaderFactory.CreateBinaryReader(archivoExcel);
            }

            //Leer el documento de Excel
            excelReader.IsFirstRowAsColumnNames = true;
            Ds_Informacion = excelReader.AsDataSet();

            //Cerrar lector
            archivoExcel.Close();
            excelReader.Close();

            //Asignar el datatable
            Dt_Poliza_Detalles = Ds_Informacion.Tables[0];

            Convertir_Datos_Poliza(Dt_Poliza_Detalles); //Convierte los datos extraidos para que sean visualizados de manera correcta.

            //Eliminar el archivo
            System.IO.File.Delete(Rta);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString(), ex);
        }
    }

    public DataSet Leer_Excel(String sqlExcel, String Path)
    {
        //Para empezar definimos la conexión OleDb a nuestro fichero Excel.
        //String Rta = @MapPath("../../Archivos/PRESUPUESTO_IRAPUATO.xls");
        String Rta = @MapPath(Path);
        string sConnectionString = "";// @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Rta + ";Extended Properties=Excel 8.0;";

        if (Rta.Contains(".xlsx"))       // Formar la cadena de conexion si el archivo es Exceml xml
        {
            sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                    "Data Source=" + Rta + ";" +
                    "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
        }
        else if (Rta.Contains(".xls"))   // Formar la cadena de conexion si el archivo es Exceml binario
        {
            sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                    "Data Source=" + Rta + ";" +
                    "Extended Properties=Excel 8.0;";
        }

        //Definimos el DataSet donde insertaremos los datos que leemos del excel
        DataSet DS = new DataSet();

        //Definimos la conexión OleDb al fichero Excel y la abrimos
        OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
        oledbConn.Open();

        //Creamos un comand para ejecutar la sentencia SELECT.
        OleDbCommand oledbCmd = new OleDbCommand(sqlExcel, oledbConn);

        //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
        OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
        da.Fill(DS);
        return DS;
    }
    #endregion
    #region (Metodos Consulta)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Contable
    /// DESCRIPCION : Consulta las cuentas contables
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 10/OCtubre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Consulta_Cuenta_Contable()
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Cuenta_Contable = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Descripcion.SelectedValue;

            Dt_Cuenta_Contable = Rs_Consulta_Con_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();
            if (Dt_Cuenta_Contable.Rows.Count > 0)
            {
                //Agrega la cuenta contable a la caja de texto correspondiente
                foreach (DataRow Registro in Dt_Cuenta_Contable.Rows)
                {
                    Txt_Cuenta_Contable.Text = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Cuenta_Contable " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Cuentas_Contables_Tipo_Polizas
    /// DESCRIPCION : Carga las Percepciones Deducciones Fijas o Variables que no son calculadas
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consultar_Cuentas_Contables_Tipo_Polizas()
    {
        DataTable Dt_Cuentas_Contables = null;  //Almacenara los datos de las cuentas contables.
        DataTable Dt_Cuentas = new DataTable();  //Almacenara los datos de las cuentas contables.
        DataTable Dt_Tipo_Polizas = null;   //Almacenara los tipos de polizas.
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexion con la capa de Datos
        Cls_Cat_Con_Tipo_Polizas_Negocio Rs_Consulta_Cat_Con_Tipo_Polizas = new Cls_Cat_Con_Tipo_Polizas_Negocio(); //Variable de conexion con la capa de Datos

        try
        {
            Rs_Consulta_Con_Cuentas_Contables.P_Afectable = "SI";
            Dt_Cuentas_Contables = Rs_Consulta_Con_Cuentas_Contables.Consulta_Cuentas_Contables(); //Consulta las cuentas contables
            if (Dt_Cuentas_Contables.Rows.Count > 0)
            {
                if (Dt_Cuentas.Rows.Count <= 0)
                {
                    Dt_Cuentas.Columns.Add("DESCRIPCION", typeof(System.String));
                    Dt_Cuentas.Columns.Add("CUENTA_CONTABLE_ID", typeof(System.String));
                }
                Dt_Cuentas_Contables.DefaultView.Sort = "CUENTA";
                foreach (DataRow Fila in Dt_Cuentas_Contables.DefaultView.ToTable().Rows)
                {
                    DataRow row = Dt_Cuentas.NewRow();
                    row["DESCRIPCION"] = Fila["CUENTA"].ToString() + "-" + Fila["DESCRIPCION"].ToString();
                    row["CUENTA_CONTABLE_ID"] = Fila["CUENTA_CONTABLE_ID"].ToString();
                    Dt_Cuentas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Cuentas.AcceptChanges();
                }
            }
            Cmb_Descripcion.DataSource = Dt_Cuentas; //Liga los datos con el combo
            Cmb_Descripcion.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;    //Asigna el campo de la tabla que se visualizara
            Cmb_Descripcion.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;    //Asigna el campo de la tabla que sera usado como valor
            Cmb_Descripcion.DataBind();
            Cmb_Descripcion.Items.Insert(0, new ListItem("< Seleccione >", ""));
            Cmb_Descripcion.SelectedIndex = -1;

            Dt_Tipo_Polizas = Rs_Consulta_Cat_Con_Tipo_Polizas.Consulta_Tipos_Poliza(); //Consulta los tipos de polizas
            Cmb_Tipo_Poliza.DataSource = Dt_Tipo_Polizas;   //Liga los datos con el combo
            Cmb_Tipo_Poliza.DataTextField = Cat_Con_Tipo_Polizas.Campo_Descripcion; //Asigna el campo de la tabla que se visualizara
            Cmb_Tipo_Poliza.DataValueField = Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID; //Asigna el campo de la tabla que sera usado como valor
            Cmb_Tipo_Poliza.DataBind();
            Cmb_Tipo_Poliza.Items.Insert(0, new ListItem("< Seleccione >", ""));
            Cmb_Tipo_Poliza.SelectedIndex = -1;
            Cmb_Busqueda_Tipo_Poliza.DataSource = Dt_Tipo_Polizas;
            Cmb_Busqueda_Tipo_Poliza.DataTextField = Cat_Con_Tipo_Polizas.Campo_Descripcion; //Asigna el campo de la tabla que se visualizara
            Cmb_Busqueda_Tipo_Poliza.DataValueField = Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID; //Asigna el campo de la tabla que sera usado como valor
            Cmb_Busqueda_Tipo_Poliza.DataBind();
            Cmb_Busqueda_Tipo_Poliza.Items.Insert(0, new ListItem("< Seleccione >", ""));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error generado al consultar las Percepciones Deducciones. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Programas
    /// DESCRIPCION : se cargan los programas que estan activos 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 28/junio/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consultar_Programas()
    {
        DataTable Dt_Programas = null;  //Almacenara los datos de las cuentas contables.
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_programas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexion con la capa de Datos
        Lbl_Error_Busqueda.Visible = false;
        Img_Error_Busqueda.Visible = false;
        Lbl_Error_Busqueda.Text = "";
        try
        {

            Dt_Programas = Rs_Consulta_programas.Consulta_Programas_Ing(); //Consulta las cuentas contables
            if (Dt_Programas.Rows.Count > 0)
            {
                //Cmb_Programas_Poliza.DataSource = Dt_Programas; //Liga los datos con el combo
                //Cmb_Programas_Poliza.DataTextField = "CLAVE_NOMBRE";    //Asigna el campo de la tabla que se visualizara
                //Cmb_Programas_Poliza.DataValueField = "PROYECTO_PROGRAMA_ID";    //Asigna el campo de la tabla que sera usado como valor
                //Cmb_Programas_Poliza.DataBind();
                //Cmb_Programas_Poliza.Items.Insert(0, new ListItem("< Seleccione >", ""));
                //Cmb_Programas_Poliza.SelectedIndex = -1;
            }
            else
            {
                Lbl_Error_Busqueda.Text = "<br> No hay programas creados activos aun  <br>";
                Lbl_Error_Busqueda.Visible = true;
                Img_Error_Busqueda.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error generado al consultar las Percepciones Deducciones. Error: [" + Ex.Message + "]");
        }
    }
    /////*******************************************************************************
    ///// NOMBRE DE LA FUNCION: Consultar_Pre_Polizas
    ///// DESCRIPCION : se cargan las pre-polizas activas
    ///// CREO        : Sergio Manuel Gallardo Andrade
    ///// FECHA_CREO  : 12/septiembre/2012
    ///// MODIFICO          :
    ///// FECHA_MODIFICO    :
    ///// CAUSA_MODIFICACION:
    /////*******************************************************************************
    //private void Consultar_Pre_Polizas()
    //{
    //    DataTable Dt_Prepolizas = null;  //Almacenara los datos de las cuentas contables.
    //    Cls_Ope_Con_Polizas_Negocio Rs_Consulta_prepolizas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexion con la capa de Datos
    //    Lbl_Error_Busqueda.Visible = false;
    //    Img_Error_Busqueda.Visible = false;
    //    Lbl_Error_Busqueda.Text = "";
    //    try
    //    {
    //        Rs_Consulta_prepolizas.P_Estatus="ACTIVO";
    //        Dt_Prepolizas = Rs_Consulta_prepolizas.Consulta_Prepolizas(); //Consulta las cuentas contables
    //        if (Dt_Prepolizas.Rows.Count > 0)
    //        {
    //            Cmb_Prepolizas.DataSource = Dt_Prepolizas; //Liga los datos con el combo
    //            Cmb_Prepolizas.DataTextField = Ope_Con_Prepolizas.Campo_Concepto;    //Asigna el campo de la tabla que se visualizara
    //            Cmb_Prepolizas.DataValueField = Ope_Con_Prepolizas.Campo_Prepoliza_ID;    //Asigna el campo de la tabla que sera usado como valor
    //            Cmb_Prepolizas.DataBind();
    //            Cmb_Prepolizas.Items.Insert(0, new ListItem("< Seleccione >", ""));
    //            Cmb_Prepolizas.SelectedIndex = -1;
    //        }
    //        else
    //        {
    //            Lbl_Error_Busqueda.Text = "<br> No hay programas creados activos aun  <br>";
    //            Lbl_Error_Busqueda.Visible = true;
    //            Img_Error_Busqueda.Visible = true;
    //        }
    //    }
    //    catch (Exception Ex)
    //    {
    //        throw new Exception("Error generado al consultar las Percepciones Deducciones. Error: [" + Ex.Message + "]");
    //    }
    //}
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Parametros
    /// DESCRIPCION : Consulta la mascara para la cuenta contable actual.
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 19/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private string Consulta_Parametros()
    {
        Cls_Cat_Con_Parametros_Negocio Rs_Consulta_Cat_Con_Parametros_Negocio = new Cls_Cat_Con_Parametros_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Parametros; //Variable que obtendra los datos de la consulta 
        string Mascara_Cuenta_Contable; //Recibe la mascara contable actual.
        try
        {
            Session.Remove("Consulta_Parametros");
            Dt_Parametros = Rs_Consulta_Cat_Con_Parametros_Negocio.Consulta_Parametros();//Consulta los datos generales de las Cuentas Contables dados de alta en la BD
            Session["Consulta_Parametros"] = Dt_Parametros;
            Mascara_Cuenta_Contable = Dt_Parametros.Rows[0][0].ToString();
            return Mascara_Cuenta_Contable;
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Parametros" + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Poliza_Avanzada
    /// DESCRIPCION : Ejecuta la busqueda de Poliza
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Poliza_Avanzada()
    {
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Ca_Poliza = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Poliza; //Variable que obtendra los datos de la consulta 
        Double bandera = 0;
        Session["Consulta_Poliza_Avanzada"] = null;
        Lbl_Error_Busqueda.Visible = false;
        Img_Error_Busqueda.Visible = false;
        try
        {
            if (!string.IsNullOrEmpty(Txt_No_Poliza_PopUp.Text))
            {
                Rs_Consulta_Ca_Poliza.P_No_Poliza = String.Format("{0:0000000000}", Convert.ToInt16(Txt_No_Poliza_PopUp.Text.ToString()));
                bandera = 1;
            }
            if (Cmb_Busqueda_Tipo_Poliza.SelectedIndex > 0)
            {
                Rs_Consulta_Ca_Poliza.P_Tipo_Poliza_ID = Consulta_Tipo_Poliza();
                bandera = 1;
            }
            if (Cmb_Busqueda_Mes_Poliza.SelectedIndex > 0 && Cmb_Busqueda_Anio_Poliza.SelectedIndex > 0)
            {
                #region MES
                switch (Cmb_Busqueda_Mes_Poliza.SelectedItem.Text)
                {
                    case "ENERO":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "01";
                        break;
                    case "FEBRERO":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "02";
                        break;
                    case "MARZO":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "03";
                        break;
                    case "ABRIL":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "04";
                        break;
                    case "MAYO":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "05";
                        break;
                    case "JUNIO":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "06";
                        break;
                    case "JULIO":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "07";
                        break;
                    case "AGOSTO":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "08";
                        break;
                    case "SEPTIEMBRE":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "09";
                        break;
                    case "OCTUBRE":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "10";
                        break;
                    case "NOVIEMBRE":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "11";
                        break;
                    case "DICIEMBRE":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "12";
                        break;
                    case "MES_13":
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "13";
                        break;
                    default:
                        Rs_Consulta_Ca_Poliza.P_Mes_Ano = "";
                        break;
                }
                #endregion
                #region ANIO
                if (Cmb_Busqueda_Anio_Poliza.SelectedIndex > 0)
                    Rs_Consulta_Ca_Poliza.P_Mes_Ano += Cmb_Busqueda_Anio_Poliza.SelectedItem.Text.Substring(2, 2);
                #endregion
                bandera = 1;
            }
            if (Cmb_Busqueda_Mes_Poliza.SelectedIndex > 0 && Cmb_Busqueda_Anio_Poliza.SelectedIndex == 0)
            {
                bandera = 2;
                Lbl_Error_Busqueda.Text = "<br> Debes Seleccionar un año para poder realizar la busqueda por año y mes  <br>";
            }
            if (Cmb_Busqueda_Anio_Poliza.SelectedIndex > 0 && Cmb_Busqueda_Anio_Poliza.SelectedIndex == 0)
            {
                bandera = 2;
                Lbl_Error_Busqueda.Text = "<br> Debes Seleccionar un Mes para poder realizar la busqueda por año y mes   <br>";
            }
            if (bandera == 1)
            {
                Dt_Poliza = Rs_Consulta_Ca_Poliza.Consulta_Poliza_Popup();
                Session["Consulta_Poliza_Avanzada"] = Dt_Poliza;
                Llena_Grid_Polizas_Avanzada();
                Mpe_Busqueda_Polizas.Show();
            }
            else
            {
                if (bandera == 0) Lbl_Error_Busqueda.Text = "<br> Debes ingresar un filtro para poder realizar la busqueda  <br>";
                Lbl_Error_Busqueda.Visible = true;
                Img_Error_Busqueda.Visible = true;
                Mpe_Busqueda_Polizas.Show();
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Poliza_Avanzada " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Requisicion_Click
    ///DESCRIPCIÓN: Metodo para consultar la reserva
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Solicitud_Click(object sender, ImageClickEventArgs e)
    {

        String Cuenta = ((ImageButton)sender).CommandArgument;
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Cuenta_Contable = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario
        Cls_Ope_Con_Polizas_Negocio Rs_Fuentes_financiamiento = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Fuentes_financiamiento = new DataTable();   //Almacena la partida especifica asociada
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Presupuesto = new Cls_Ope_Con_Polizas_Negocio();    //Variable de conexión a la capa de negocios
        try
        {
            Cmb_Descripcion.SelectedValue = Cuenta;
            Cmb_Fuente_Financiamiento.Items.Clear();
            Cmb_Programa.Items.Clear();
            Cmb_Unidad_Responsable.Items.Clear();
            Cmb_Fuente_Financiamiento_Egr.Items.Clear();

            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Descripcion.SelectedValue;
            Dt_Cuenta_Contable = Rs_Consulta_Con_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();  //Consulta los datos de la cuenta contable seleccionada
            if (Dt_Cuenta_Contable.Rows.Count > 0)
            {
                //Agrega la cuenta contable a la caja de texto correspondiente
                foreach (DataRow Registro in Dt_Cuenta_Contable.Rows)
                {
                    Txt_Cuenta_Contable.Text = Aplicar_Mascara_Cuenta_Contable(Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString()).ToString();

                    if (String.IsNullOrEmpty(Txt_Concepto_Poliza.Text))
                        Txt_Concepto_Partida.Text = Cmb_Descripcion.SelectedItem.Text.ToString().Substring(10);
                    else
                        Txt_Concepto_Partida.Text = Txt_Concepto_Poliza.Text;
                }
                if (Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Tipo_Presupuestal].ToString() == "INGRESOS" && Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString() != "")
                {
                    Div_Presupuestal.Style.Add("display", "block");
                    Div_Ingresos.Style.Add("display", "block");
                    Div_Egresos.Style.Add("display", "none");
                    Rs_Fuentes_financiamiento.P_Partida_ID = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                    Dt_Fuentes_financiamiento = Rs_Fuentes_financiamiento.Consulta_Fuente_Financiamiento();
                    Cmb_Fuente_Financiamiento.DataSource = Dt_Fuentes_financiamiento; //Liga los datos con el combo
                    Cmb_Fuente_Financiamiento.DataTextField = "CLAVE_NOMBRE";    //Asigna el campo de la tabla que se visualizara
                    Cmb_Fuente_Financiamiento.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;    //Asigna el campo de la tabla que sera usado como valor
                    Cmb_Fuente_Financiamiento.DataBind();
                    Cmb_Fuente_Financiamiento.Items.Insert(0, new ListItem("< Seleccione >", ""));
                    Cmb_Fuente_Financiamiento.SelectedIndex = -1;
                    Txt_Partida_Presupuestal.Value = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                }
                else
                {
                    if (Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Tipo_Presupuestal].ToString() == "EGRESOS" && Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString() != "")
                    {
                        Div_Presupuestal.Style.Add("display", "block");
                        Div_Ingresos.Style.Add("display", "none");
                        Div_Egresos.Style.Add("display", "block");
                        Rs_Fuentes_financiamiento.P_Partida_ID = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                        Dt_Fuentes_financiamiento = Rs_Fuentes_financiamiento.Consulta_Fuente_Financiamiento_Egr();
                        Cmb_Fuente_Financiamiento_Egr.DataSource = Dt_Fuentes_financiamiento; //Liga los datos con el combo
                        Cmb_Fuente_Financiamiento_Egr.DataTextField = "CLAVE_NOMBRE";    //Asigna el campo de la tabla que se visualizara
                        Cmb_Fuente_Financiamiento_Egr.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;    //Asigna el campo de la tabla que sera usado como valor
                        Cmb_Fuente_Financiamiento_Egr.DataBind();
                        Cmb_Fuente_Financiamiento_Egr.Items.Insert(0, new ListItem("< Seleccione >", ""));
                        Cmb_Fuente_Financiamiento_Egr.SelectedIndex = -1;
                        Txt_Partida_Presupuestal.Value = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                    }
                    else
                    {
                        Div_Presupuestal.Style.Add("display", "none");
                        Div_Ingresos.Style.Add("display", "none");
                        Div_Egresos.Style.Add("display", "none");
                        Txt_Partida_Presupuestal.Value = "";
                    }
                }
            }
            Grid_Cuentas.DataSource = null;   // Se iguala el DataTable con el Grid
            Grid_Cuentas.DataBind();    // Se ligan los datos.
            Txt_Busqueda_Cuenta.Text = "";
            //Upnl_Partidas_Polizas.Update();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Avanzada
    /// DESCRIPCION : Ejecuta la busqueda de Cuenta
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Cuenta_Avanzada()
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cuenta = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Cuentas; //Variable que obtendra los datos de la consulta 
        Double bandera = 0;
        Lbl_Error_Busqueda_Cuenta.Visible = false;
        Img_Error_Busqueda_Cuenta.Visible = false;
        try
        {
            if (!string.IsNullOrEmpty(Txt_Busqueda_Cuenta.Text))
            {
                Rs_Consulta_Cuenta.P_Descripcion = Txt_Busqueda_Cuenta.Text.ToString().ToUpper();
                bandera = 1;
            }
            if (Txt_Busqueda_Cuenta.Text == "")
            {
                bandera = 2;
                Lbl_Error_Busqueda_Cuenta.Text = "<br> Debes Ingresar una Descripcion  <br>";
            }
            if (bandera == 1)
            {
                Rs_Consulta_Cuenta.P_Afectable = "SI";
                Dt_Cuentas = Rs_Consulta_Cuenta.Consulta_Cuentas_Contables();
                if (Dt_Cuentas.Rows.Count > 0)
                {
                    // se llena el grid de cuentas encontradas
                    Grid_Cuentas.Columns[1].Visible = true;
                    Grid_Cuentas.DataSource = Dt_Cuentas;   // Se iguala el DataTable con el Grid
                    Grid_Cuentas.DataBind();    // Se ligan los datos.
                    Grid_Cuentas.Visible = true;
                    Grid_Cuentas.Columns[1].Visible = false;
                }
                else
                {
                    Lbl_Error_Busqueda_Cuenta.Text = "<br> No se encontraron Coincidencias con la busqueda  <br>";
                    Lbl_Error_Busqueda_Cuenta.Visible = true;
                    Img_Error_Busqueda_Cuenta.Visible = true;
                }

                Grid_Cuentas.SelectedIndex = -1;
                Mpe_Busqueda_Cuenta.Show();
            }
            else
            {
                if (bandera == 0) Lbl_Error_Busqueda_Cuenta.Text = "<br> Debes ingresar un filtro para poder realizar la busqueda  <br>";
                Lbl_Error_Busqueda_Cuenta.Visible = true;
                Img_Error_Busqueda_Cuenta.Visible = true;
                Mpe_Busqueda_Cuenta.Show();
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Cuenta_Avanzada " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Tipo_Poliza
    /// DESCRIPCION : Ejecuta la busqueda del Tipo de Poliza
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private string Consulta_Tipo_Poliza()
    {
        Cls_Cat_Con_Tipo_Polizas_Negocio Rs_Consulta_Tipo_Poliza = new Cls_Cat_Con_Tipo_Polizas_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Tipo_Poliza; //Variable que obtendra los datos de la consulta 

        try
        {
            Rs_Consulta_Tipo_Poliza.P_Descripcion = Cmb_Busqueda_Tipo_Poliza.SelectedItem.Text;
            Dt_Tipo_Poliza = Rs_Consulta_Tipo_Poliza.Consulta_Tipos_Poliza(); //Consulta el ID de acuerdo a la Descripcion
            Session["Consulta_Tipo_Poliza"] = Dt_Tipo_Poliza;
            return Dt_Tipo_Poliza.Rows[0][0].ToString();
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Tipo_Poliza " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Partida_Presupuestal
    /// DESCRIPCION : Realiza la busqueda de la partida especifica asociada a la cuenta contable.
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 4/Octubre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private string Consulta_Partida_Presupuestal()
    {
        Cls_Cat_Con_Tipo_Polizas_Negocio Rs_Consulta_Tipo_Poliza = new Cls_Cat_Con_Tipo_Polizas_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Tipo_Poliza; //Variable que obtendra los datos de la consulta 

        try
        {
            Rs_Consulta_Tipo_Poliza.P_Descripcion = Cmb_Busqueda_Tipo_Poliza.SelectedItem.Text;
            Dt_Tipo_Poliza = Rs_Consulta_Tipo_Poliza.Consulta_Tipos_Poliza(); //Consulta el ID de acuerdo a la Descripcion
            Session["Consulta_Tipo_Poliza"] = Dt_Tipo_Poliza;
            return Dt_Tipo_Poliza.Rows[0][0].ToString();
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Tipo_Poliza " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Crear_DataTable_Egresos
    /// DESCRIPCION : Crear el datatable que contenga los movimientos presupuestales de Egresos;
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 11/Enero/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private DataTable Crear_DataTable_Egresos(DataTable Dt_Datos)
    {
        DataRow Fila;
        DataTable Dt_Final = new DataTable();
        try
        {
            if (Dt_Final != null && Dt_Final.Rows.Count <= 0)
            {
                Dt_Final.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                Dt_Final.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                Dt_Final.Columns.Add("PROYECTO_PROGRAMA_ID", System.Type.GetType("System.String"));
                Dt_Final.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID, System.Type.GetType("System.String"));
                Dt_Final.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, System.Type.GetType("System.String"));
                Dt_Final.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, System.Type.GetType("System.String"));
                Dt_Final.Columns.Add("MOMENTO_FINAL", System.Type.GetType("System.String"));
                Dt_Final.Columns.Add("MOMENTO_INICIAL", System.Type.GetType("System.String"));
            }
            foreach (DataRow Dr in Dt_Datos.Rows)
            {
                if (!String.IsNullOrEmpty(Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim()) && !String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()) && !String.IsNullOrEmpty(Dr["DEPENDENCIA_ID"].ToString().Trim()))
                {
                    Fila = Dt_Final.NewRow();
					Fila["DEPENDENCIA_ID"] = Dr["DEPENDENCIA_ID"].ToString().Trim();
					Fila["FUENTE_FINANCIAMIENTO_ID"] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
					Fila["PROYECTO_PROGRAMA_ID"] = Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                    Fila[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID] = Dr[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
					Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Replace(",", "");;
					Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Replace(",", "");;
                    Fila["MOMENTO_FINAL"] = Dr["MOMENTO_FINAL"].ToString();
                    Fila["MOMENTO_INICIAL"] = Dr["MOMENTO_INICIAL"].ToString();
					Dt_Final.Rows.Add(Fila);
                }
            }

            return Dt_Final;
        }
        catch (Exception ex)
        {
            throw new Exception("Crear_DataTable_Egresos " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Crear_DataTable_Ingresos
    /// DESCRIPCION :Crear el datatable que contenga los movimientos presupuestales de  ingresos;
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 11/Enero/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private DataTable Crear_DataTable_Ingresos(DataTable Dt_Datos,SqlCommand P_Cmmd)
    {
        DataRow Fila;
        DataTable Dt_Final = new DataTable();
        Cls_Ope_Con_Polizas_Negocio Rs_Concepto_Cuenta = new Cls_Ope_Con_Polizas_Negocio();
        String Concepto = "";
        try
        {
            if (Dt_Final != null && Dt_Final.Rows.Count <= 0)
            {
                Dt_Final.Columns.Add("Fte_Financiamiento_ID", System.Type.GetType("System.String"));
                Dt_Final.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, System.Type.GetType("System.String"));
                Dt_Final.Columns.Add(Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID, System.Type.GetType("System.String"));
                Dt_Final.Columns.Add(Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID, System.Type.GetType("System.String"));
                Dt_Final.Columns.Add("Anio", System.Type.GetType("System.String"));
                Dt_Final.Columns.Add("Importe", System.Type.GetType("System.String"));
            }
            foreach (DataRow Dr in Dt_Datos.Rows)
            {
                if (!String.IsNullOrEmpty(Dr["FUENTE_FINANCIAMIENTO_ID"].ToString()) && String.IsNullOrEmpty(Dr["DEPENDENCIA_ID"].ToString()))
                {
                    Concepto = null;
                    Rs_Concepto_Cuenta.P_Cuenta_Contable_ID = Dr[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                    Rs_Concepto_Cuenta.P_Cmmd = P_Cmmd;
                   // Concepto=Rs_Concepto_Cuenta.Consulta_Concepto_Ingresos_ID();
                    //se insertan los registros de las cuentas que afectan ingresos
                    
                    Fila = Dt_Final.NewRow();
                    Fila["Fte_Financiamiento_ID"] = Dr["Fte_Financiamiento_ID"].ToString().Trim();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dr[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                    Fila[Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID] = Concepto;
                    Fila[Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID] = "";
                    if (Convert.ToDouble(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Fila["Importe"] = Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Replace(",", "");
                    }
                    else
                    {
                        if (Convert.ToDouble(Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                        {
                            Fila["Importe"] = Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Replace(",", "");
                        }
                    }
                    Dt_Final.Rows.Add(Fila);
                }
            }
            return Dt_Final;
        }
        catch (Exception ex)
        {
            throw new Exception("Crear_DataTable_Egresos " + ex.Message.ToString(), ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Construye_Prefijo
    /// DESCRIPCION : Construir el prefijo de acuerdo al tipo de la poliza
    /// CREO        : Noe Mosqueda Valadez
    /// FECHA_CREO  : 11/Abril/2012 18:47
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Construye_Prefijo(String Tipo_Poliza_ID, String Mes_Anio)
    {
        //Declaracion de variables
        String Prefijo = String.Empty; //variable para el prefijo
        Cls_Ope_Con_Polizas_Negocio Polizas_Negocio = new Cls_Ope_Con_Polizas_Negocio(); //variable para la capa de negocios
        DataTable Dt_Polizas_Tipo = new DataTable(); //Tabla para el resultado de la consulta
        String[] vec_prefijo; //vector para el prefijo

        try
        {
            //Asignar propiedades
            Polizas_Negocio.P_Tipo_Poliza_ID = Tipo_Poliza_ID;
            Polizas_Negocio.P_Mes_Ano = Mes_Anio;

            //Ejecutar consulta
            Dt_Polizas_Tipo = Polizas_Negocio.Consulta_Polizas_Tipo();

            //Seleccionar el tipo de poliza
            switch (Tipo_Poliza_ID)
            {
                case "00001": //Ingresos
                    //verificar si la consulta arrojo resultados
                    if (Dt_Polizas_Tipo.Rows.Count > 0)
                    {
                        //Crear el vector con el ultimo prefijo
                        vec_prefijo = Dt_Polizas_Tipo.Rows[0]["Prefijo"].ToString().Split('-');

                        //verificar si el vectpr tiene 2 elementos
                        if (vec_prefijo.Length == 2)
                        {
                            //COnstruir el prefijo
                            Prefijo = (Convert.ToInt32(vec_prefijo[0]) + 1).ToString().Trim() + "-" + (Convert.ToInt32(vec_prefijo[1]) + 1).ToString().Trim();
                        }
                    }
                    else
                    {
                        Prefijo = "301-1";
                    }
                    break;

                case "00002": //Egresos
                    //verificar si la consulta arrojo resultados
                    if (Dt_Polizas_Tipo.Rows.Count > 0)
                    {
                        //Crear el vector con el ultimo prefijo
                        vec_prefijo = Dt_Polizas_Tipo.Rows[0]["Prefijo"].ToString().Split('-');

                        //verificar si el vectpr tiene 2 elementos
                        if (vec_prefijo.Length == 2)
                        {
                            //COnstruir el prefijo
                            Prefijo = (Convert.ToInt32(vec_prefijo[0]) + 1).ToString().Trim() + "-" + (Convert.ToInt32(vec_prefijo[1]) + 1).ToString().Trim();
                        }
                    }
                    else
                    {
                        Prefijo = "1-1";
                    }
                    break;

                case "00003": //Diario
                    //verificar si la consulta arrojo resultados
                    if (Dt_Polizas_Tipo.Rows.Count > 0)
                    {
                        //Crear el vector con el ultimo prefijo
                        vec_prefijo = Dt_Polizas_Tipo.Rows[0]["Prefijo"].ToString().Split('-');

                        //verificar si el vectpr tiene 2 elementos
                        if (vec_prefijo.Length == 2)
                        {
                            //COnstruir el prefijo
                            Prefijo = (Convert.ToInt32(vec_prefijo[0]) + 1).ToString().Trim() + "-" + (Convert.ToInt32(vec_prefijo[1]) + 1).ToString().Trim();
                        }
                    }
                    else
                    {
                        Prefijo = "401-1";
                    }
                    break;

                default:
                    break;
            }

            //Entregar resultado
            return Prefijo;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.ToString(), ex);
        }
    }

    #endregion
    #region (Metodos de Operacion [Alta - Modificar - Eliminar])
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Poliza
    /// DESCRIPCION : Da de Alta la poliza con los datos proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11-Julio-2011
    /// MODIFICO          : Salvador L. Rea Ayala
    /// FECHA_MODIFICO    : 10/Octubre/2011
    /// CAUSA_MODIFICACION: Se agregaron los nuevos campos al metodo para su correcto
    ///                     funcionamiento.
    ///*******************************************************************************
    private String Alta_Poliza(SqlCommand P_Cmmd)
    {
        Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas; //Variable de conexión hacia la capa de negocios para envio de los datos a dar de alta
        Cls_Ope_Con_Compromisos_Negocio Rs_Compromisos = new Cls_Ope_Con_Compromisos_Negocio(); //Variable de conexion con la capa de negocios.
        // DataTable Dt_Compromisos = null;    //Almacena el compromiso de acuerdo a los datos proporcionados.
        Cls_Ope_SAP_Dep_Presupuesto_Negocio Rs_Presupuesto = new Cls_Ope_SAP_Dep_Presupuesto_Negocio(); //Variable de conexion con la capa de Negocios.
        DataTable Dt_Partidas_Poliza = null;    //Almacenara los registros encontrados en la tabla de Presupuestos        
        // DataTable Dt_Presupuesto = null;
        DataTable Dt_Filtrado = new DataTable();
        DataTable Dt_Jefe_Dependencia = null;
        String Momento_Ingresos = "";
        String Resultado = "SI";
        Boolean Afectacion=true;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();
            Rs_Alta_Ope_Con_Polizas.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
            Dt_Jefe_Dependencia = Rs_Alta_Ope_Con_Polizas.Consulta_Empleado_Jefe_Dependencia();
            Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();
            //if (Cmb_Tipo_Poliza.SelectedItem.Text.ToString().Equals("PROGRAMAS"))
            //{
            //    Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = "00001";
            //}
            //else
            //{
                Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = Cmb_Tipo_Poliza.SelectedValue;
            //}

            Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text));
            Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = Convert.ToDateTime(Txt_Fecha_Poliza.Text);
            Rs_Alta_Ope_Con_Polizas.P_Concepto = Convert.ToString(Txt_Concepto_Poliza.Text.ToString());
            Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Convert.ToDouble(Convert.ToString(Txt_Total_Debe.Text.ToString()).Replace(",", ""));
            Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Convert.ToDouble(Convert.ToString(Txt_Total_Haber.Text.ToString()).Replace(",", ""));
            Rs_Alta_Ope_Con_Polizas.P_No_Partida = Convert.ToInt32(Txt_No_Partidas.Text.ToString());
            Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = (DataTable)Session["Dt_Partidas_Poliza"];
            Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
            Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Cmb_Nombre_Empleado.SelectedValue.ToString();
            Rs_Alta_Ope_Con_Polizas.P_Cmmd = Cmmd;
            Rs_Alta_Ope_Con_Polizas.P_Prefijo = Txt_Prefijo.Text;
            string[] Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD

            Dt_Partidas_Poliza = (DataTable)Session["Dt_Partidas_Poliza"];
            if ((Convert.ToInt16(String.Format("{0:MM}", Convert.ToDateTime(Txt_Fecha_Poliza.Text))) < Convert.ToInt16(String.Format("{0:MM}", DateTime.Now)))
                || (Convert.ToInt16(String.Format("{0:yy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text))) <= Convert.ToInt16(String.Format("{0:yy}", DateTime.Now))))
              //  Alta_Poliza_Desfasada(Datos_Poliza, (DataTable)Session["Dt_Partidas_Poliza"],Cmmd);
            //if (Cmb_Momento_Ingresos.SelectedValue == "1")
            //{
            //    Momento_Ingresos = "DEVENGADO";
            //}
            //else
            //{
            //    Momento_Ingresos = "RECAUDADO";
            //}
            //Dt_Filtrado = Crear_DataTable_Egresos(Dt_Partidas_Poliza);
            //if (Dt_Filtrado.Rows.Count > 0 && Dt_Filtrado.Columns.Count > 0)
            //{
            //    Resultado = Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Poliza_Egresos(Dt_Filtrado, Datos_Poliza[0], Datos_Poliza[1], Datos_Poliza[2], Cmmd);
            //}
            //if (Resultado == "SI")
            //{
            //    Dt_Filtrado = new DataTable();
            //    Dt_Filtrado = Crear_DataTable_Ingresos(Dt_Partidas_Poliza, Cmmd);
            //    if (Dt_Filtrado.Rows.Count > 0 && Dt_Filtrado.Columns.Count > 0)
            //    {
            //        if(Momento_Ingresos == "DEVENGADO"){
            //            Afectacion = Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Filtrado,Cmmd,"",Datos_Poliza[0], Datos_Poliza[1], Datos_Poliza[2],"POR_RECAUDAR","DEVENGADO");
            //        }else{
            //            Afectacion = Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Filtrado,Cmmd,"",Datos_Poliza[0], Datos_Poliza[1], Datos_Poliza[2],"DEVENGADO","RECAUDADO");
            //        }
                    
            //    }
            //    if (Afectacion)
            //    {
            if (P_Cmmd == null)
            {
                Trans.Commit();
            }
            Limpia_Controles(); 
            Imprimir(Datos_Poliza[0], Datos_Poliza[1], String.Format("{0:MMMM/dd/yyyy}", Convert.ToDateTime(Datos_Poliza[3])),Datos_Poliza[2], Cmmd);
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            Txt_No_Poliza.Text = "";
            Cmb_Tipo_Poliza.SelectedIndex = -1;
                   
            //    }
            //    else
            //    {
            //        if (P_Cmmd == null)
            //        {
            //            Trans.Rollback();
            //        }
            //        Resultado ="NO";
            //    }

            //}
            //else
            //{
            //    if (P_Cmmd == null)
            //    {
            //        Trans.Rollback();
            //    }
            //}
            return Resultado;
            // Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Poliza_Ingresos(Dt_Partidas_Poliza,Cmmd, "", Datos_Poliza[0], Datos_Poliza[1], Datos_Poliza[2],Momento_Ingresos);
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }          
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Prepoliza
    /// DESCRIPCION : Da de Alta la poliza con los datos proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 11-septiembre-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    ////private void Alta_Prepoliza(SqlCommand P_Cmmd)
    ////{
    ////    Cls_Ope_Con_Polizas_Negocio Rs_Alta_Prepoliza_Ope_Con_Polizas= new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión hacia la capa de negocios para envio de los datos a dar de alta
    ////    SqlConnection Cn = new SqlConnection();
    ////    SqlCommand Cmmd = new SqlCommand();
    ////    SqlTransaction Trans = null;
    ////    SqlDataAdapter Dt_Sql = new SqlDataAdapter();
    ////    DataSet Ds_Sql = new DataSet();
    ////    if (P_Cmmd != null)
    ////    {
    ////        Cmmd = P_Cmmd;
    ////    }
    ////    else
    ////    {
    ////        Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
    ////        Cn.Open();
    ////        Trans = Cn.BeginTransaction();
    ////        Cmmd.Connection = Trans.Connection;
    ////        Cmmd.Transaction = Trans;
    ////    }
    ////    try
    ////    {
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Concepto = Txt_Pre_Poliza.Text.ToString().ToUpper();
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Estatus = "ACTIVO";
    ////        if (Cmb_Tipo_Poliza.SelectedIndex >0)
    ////        {
    ////            Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Tipo_Poliza_ID = Cmb_Tipo_Poliza.SelectedValue;
    ////        }
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Momento = Cmb_Momento_Ingresos.SelectedItem.Text.ToString();
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Total_Debe = Convert.ToDouble(Convert.ToString(Txt_Total_Debe.Text.ToString()).Replace(",", ""));
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Total_Haber = Convert.ToDouble(Convert.ToString(Txt_Total_Haber.Text.ToString()).Replace(",", ""));
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_No_Partida = Convert.ToInt32(Txt_No_Partidas.Text.ToString());
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Dt_Detalles_Polizas = (DataTable)Session["Dt_Partidas_Poliza"];
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
    ////        Rs_Alta_Prepoliza_Ope_Con_Polizas.P_Cmmd = Cmmd;
    ////        string[] Datos_Poliza = Rs_Alta_Prepoliza_Ope_Con_Polizas.Alta_Prepoliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD
    ////        Limpia_Controles();
    ////        Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
    ////        Habilitar_Controles("Inicial");
    ////        Txt_No_Poliza.Text = "";
    ////        Cmb_Tipo_Poliza.SelectedIndex = -1;
    ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pre-Póliza", "alert('El alta de la Pre-Póliza No." + Datos_Poliza[0].ToString() + "  fue exitosa');", true);
    ////        if (P_Cmmd == null)
    ////        {
    ////            Trans.Commit();
    ////        }
    ////    }
    ////    catch (SqlException Ex)
    ////    {
    ////        if (P_Cmmd == null)
    ////        {
    ////            Trans.Rollback();
    ////        }
    ////        if (Trans != null)
    ////        {
    ////            Trans.Rollback();
    ////        }
    ////        throw new Exception("Error: " + Ex.Message);
    ////    }
    ////    catch (DBConcurrencyException Ex)
    ////    {
    ////        if (Trans != null)
    ////        {
    ////            Trans.Rollback();
    ////        }
    ////        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
    ////    }
    ////    catch (Exception Ex)
    ////    {
    ////        if (Trans != null)
    ////        {
    ////            Trans.Rollback();
    ////        }
    ////        throw new Exception("Error: " + Ex.Message);
    ////    }
    ////    finally
    ////    {
    ////        if (P_Cmmd == null)
    ////        {
    ////            Cn.Close();
    ////        }
    ////    }
    ////}
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Poliza
    /// DESCRIPCION : Modifica la poliza con los datos proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11-Julio-2011
    /// MODIFICO          : Salvador L. Rea Ayala
    /// FECHA_MODIFICO    : 10/Octubre/2011
    /// CAUSA_MODIFICACION: Se agregaron los nuevos campos al metodo para su correcto
    ///                     funcionamiento.
    ///*******************************************************************************
    private void Modificar_Poliza(SqlCommand P_Cmmd)
    {
        Cls_Ope_Con_Polizas_Negocio Rs_Modificar_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión hacia la capa de negocios para envio de los datos a dar de alta
        //DataTable Dt_Polizas;
        Cls_Ope_Con_Compromisos_Negocio Rs_Compromisos = new Cls_Ope_Con_Compromisos_Negocio(); //Variable de conexion con la capa de negocios.
        //DataTable Dt_Compromisos = null;    //Almacena el compromiso de acuerdo a los datos proporcionados.
        Cls_Ope_SAP_Dep_Presupuesto_Negocio Rs_Presupuesto = new Cls_Ope_SAP_Dep_Presupuesto_Negocio(); //Variable de conexion con la capa de Negocios.
        DataTable Dt_Partidas_Poliza = null;    //Almacenara los registros encontrados en la tabla de Presupuestos        
        //DataTable Dt_Presupuesto = null;
        DataTable Dt_Partidas_Originales = new DataTable();
        Rs_Modificar_Ope_Con_Polizas.P_No_Poliza = Txt_No_Poliza.Text;
        Rs_Modificar_Ope_Con_Polizas.P_Fecha_Poliza = Convert.ToDateTime(Txt_Fecha_Poliza.Text);
        DataTable Dt_Partidad_Modificadas = new DataTable();//Este datatable contiene las cuentas que se modificaron de la poliza original 
        String Cuenta_Contable = "";
        Double Debe = 0;
        Double Haber = 0;
        Boolean Cuenta_Existe = false;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            Rs_Modificar_Ope_Con_Polizas.P_No_Poliza = Convert.ToString(Txt_No_Poliza.Text);
            Rs_Modificar_Ope_Con_Polizas.P_Tipo_Poliza_ID = Cmb_Tipo_Poliza.SelectedValue;
            Rs_Modificar_Ope_Con_Polizas.P_Mes_Ano = Convert.ToString(String.Format("{0:MMyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text)));
            Rs_Modificar_Ope_Con_Polizas.P_Cmmd = Cmmd;
            Dt_Partidas_Originales = Rs_Modificar_Ope_Con_Polizas.Consulta_Detalle_Poliza();
            if (Dt_Partidas_Originales.Rows.Count > 0)
            {
                if (Dt_Partidad_Modificadas.Rows.Count <= 0)
                {
                    Dt_Partidad_Modificadas.Columns.Add("CUENTA_CONTABLE_ID", typeof(System.String));
                    Dt_Partidad_Modificadas.Columns.Add("DEBE", typeof(System.Double));
                    Dt_Partidad_Modificadas.Columns.Add("HABER", typeof(System.Double));
                    Dt_Partidad_Modificadas.Columns.Add("NO_POLIZA", typeof(System.String));
                    Dt_Partidad_Modificadas.Columns.Add("TIPO_POLIZA_ID",typeof(System.String));
                    Dt_Partidad_Modificadas.Columns.Add("MES_ANIO",typeof(System.String));
                    Dt_Partidad_Modificadas.Columns.Add("CONCEPTO", typeof(System.String));
                }
                //se recorre el datatable que contiene los detalles de la poliza antes de ser modificada para identificar cuales partidas fueron las que se modificaron y poder 
                //realizar el registro contable de las cuentas y el registro de las polizas que se eliminaron
                foreach(DataRow Renglon in Dt_Partidas_Originales.Rows)
                {
                    Cuenta_Contable = Renglon["CUENTA_CONTABLE_ID"].ToString();
                    Debe = Convert.ToDouble(Renglon["DEBE"].ToString());
                    Haber = Convert.ToDouble(Renglon["HABER"].ToString());
                    Cuenta_Existe = false;
                    foreach(DataRow Fila in ((DataTable)Session["Dt_Partidas_Poliza"]).Rows)
                    {
                        if(Cuenta_Contable==Fila["CUENTA_CONTABLE_ID"].ToString() && Debe == Convert.ToDouble(Fila["DEBE"].ToString()) && Haber == Convert.ToDouble(Fila["HABER"].ToString()))
                        {
                            Cuenta_Existe = true;
                        }
                    }
                    if (!Cuenta_Existe)
                    {
                        DataRow row = Dt_Partidad_Modificadas.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        row["CUENTA_CONTABLE_ID"] = Renglon["CUENTA_CONTABLE_ID"].ToString();
                        row["DEBE"] = Convert.ToDouble(Renglon["DEBE"].ToString());
                        row["HABER"] = Convert.ToDouble(Renglon["HABER"].ToString());
                        row["NO_POLIZA"] = Renglon["NO_POLIZA"].ToString();
                        row["TIPO_POLIZA_ID"] = Renglon["TIPO_POLIZA_ID"].ToString();
                        row["MES_ANIO"] = Renglon["MES_ANO"].ToString();
                        row["CONCEPTO"] = Convert.ToString(Txt_Concepto_Poliza.Text.ToString());
                        Dt_Partidad_Modificadas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Partidad_Modificadas.AcceptChanges();
                    }
                }
                Rs_Modificar_Ope_Con_Polizas.P_Dt_Partidas_Modificadas = Dt_Partidad_Modificadas;
                Rs_Modificar_Ope_Con_Polizas.P_Fecha_Poliza = Convert.ToDateTime(Txt_Fecha_Poliza.Text);
                Rs_Modificar_Ope_Con_Polizas.P_Concepto = Convert.ToString(Txt_Concepto_Poliza.Text.ToString());
                Rs_Modificar_Ope_Con_Polizas.P_Total_Debe = Convert.ToDouble(Convert.ToString(Txt_Total_Debe.Text.ToString()).Replace(",", ""));
                Rs_Modificar_Ope_Con_Polizas.P_Total_Haber = Convert.ToDouble(Convert.ToString(Txt_Total_Haber.Text.ToString()).Replace(",", ""));
                Rs_Modificar_Ope_Con_Polizas.P_No_Partida = Convert.ToInt32(Txt_No_Partidas.Text.ToString());
                Rs_Modificar_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Polizas.P_Dt_Detalles_Polizas = (DataTable)Session["Dt_Partidas_Poliza"];
                Rs_Modificar_Ope_Con_Polizas.P_Cmmd = Cmmd;
                Rs_Modificar_Ope_Con_Polizas.P_Prefijo = Txt_Prefijo.Text;
                Rs_Modificar_Ope_Con_Polizas.Modificar_Polizas(); //Da de alta los datos de el Sindicato proporcionados por el usuario en la BD
                Dt_Partidas_Poliza = (DataTable)Session["Dt_Partidas_Poliza"];
                Imprimir(Convert.ToString(Txt_No_Poliza.Text), Cmb_Tipo_Poliza.SelectedValue, String.Format("{0:MMMM/dd/yyyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text)), String.Format("{0:MMyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text)), Cmmd);
                Limpia_Controles();
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                Txt_No_Poliza.Text = "";
                Cmb_Tipo_Poliza.SelectedIndex = -1;
                //Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pólizas", "alert('La modificación de la Póliza fue Exitosa');", true);
                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cancelar_Poliza
    /// DESCRIPCION :Realiza la cancelacion de la poliza Contablemente
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 24-Octubre-2012
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Cancelar_Poliza( SqlCommand P_Cmmd)
    {
        Cls_Ope_Con_Polizas_Negocio Rs_Cancelar_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión hacia la capa de Negocios para la eliminación de los datos
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();
        //DataTable Dt_Polizas = null;
        string Fecha_Poliza;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        SqlDataAdapter Dt_Sql = new SqlDataAdapter();
        DataSet Ds_Sql = new DataSet();
        DataTable Dt_Detalle_Solicitud = new DataTable();
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            Rs_Cancelar_Ope_Con_Polizas.P_Fecha_Poliza = Convert.ToDateTime(Txt_Fecha_Poliza.Text);
            Rs_Cancelar_Ope_Con_Polizas.P_No_Poliza = Convert.ToString(Txt_No_Poliza.Text);
            Rs_Cancelar_Ope_Con_Polizas.P_Tipo_Poliza_ID = Cmb_Tipo_Poliza.SelectedValue;//> 0 ? Cmb_Tipo_Poliza.SelectedItem.Text : "";
            Rs_Cancelar_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text));
            Rs_Cancelar_Ope_Con_Polizas.P_Concepto = Convert.ToString(Txt_Concepto_Poliza.Text.ToString());
            Rs_Cancelar_Ope_Con_Polizas.P_Total_Debe = 0;
            Rs_Cancelar_Ope_Con_Polizas.P_Total_Haber = 0;
            if (Txt_No_Partidas.Text.Contains("."))
            {
                Rs_Cancelar_Ope_Con_Polizas.P_No_Partida = Convert.ToInt32(Txt_No_Partidas.Text.Substring(0, Txt_No_Partidas.Text.IndexOf(".")));
            
            }
            else
            {
                Rs_Cancelar_Ope_Con_Polizas.P_No_Partida = Convert.ToInt32(Txt_No_Partidas.Text);
            
            }
            Rs_Cancelar_Ope_Con_Polizas.P_Cmmd = Cmmd;
            Rs_Cancelar_Ope_Con_Polizas.Cancelar_Poliza(); //Elimina la póliza que selecciono el usuario de la BD
            Fecha_Poliza = String.Format("{0:MMyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text));
            Imprimir(Convert.ToString(Txt_No_Poliza.Text), Cmb_Tipo_Poliza.SelectedValue, String.Format("{0:MMMM/dd/yyyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text)), String.Format("{0:MMyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text)),Cmmd);
            Limpia_Controles();
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            Txt_No_Poliza.Text = "";
            Cmb_Tipo_Poliza.SelectedIndex = -1;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Polizas", "alert('Se ha Cancelado la poliza correctamente');", true);
            //Rs_Consulta_Ope_Con_Polizas.P_Mes_Ano = Fecha_Poliza;
            //Dt_Polizas = Rs_Consulta_Ope_Con_Polizas.Consulta_Poliza_Popup();
            //Session.Remove("Consulta_Poliza_Avanzada");
            //Session["Consulta_Poliza_Avanzada"] = Dt_Polizas;
            //Llena_Grid_Polizas_Avanzada();
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Empleado_Autoriza
    /// DESCRIPCION :Realiza la cancelacion de la poliza Contablemente
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 24-Octubre-2012
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Consultar_Empleado_Autoriza()
    {
        try
        {
            Cls_Ope_Con_Polizas_Negocio Rs_Cat_Empleados = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexion con la capa de negocios.
            DataTable Dt_Empleados = null;  //Almacena los datos de los empleados encontrados.
            int Cont_Empleados = 1; //Contador para cambiar la posicion de insercion al combo.
            Txt_Empleado_Autorizo.Text = Cls_Sessiones.No_Empleado.ToString();
            Rs_Cat_Empleados.P_Empleado_ID = String.Format("{0:000000}", Txt_Empleado_Autorizo.Text).ToString();
            Rs_Cat_Empleados.P_Nombre = "";
            Cmb_Nombre_Empleado.Items.Clear();
            Dt_Empleados = Rs_Cat_Empleados.Consulta_Empleados_Especial();
            Cmb_Nombre_Empleado.Items.Insert(0, new ListItem("<- Seleccione ->", ""));

            foreach (DataRow Registro in Dt_Empleados.Rows) //Agrega los Items al combo.
            {
                Cmb_Nombre_Empleado.Items.Insert(Cont_Empleados, new ListItem(Registro["EMPLEADO"].ToString(), Registro[Cat_Empleados.Campo_Empleado_ID].ToString()));
                Cont_Empleados++;
            }
            if (Cont_Empleados > 2) //Si existe mas de un empleado da la opcion de seleccionar
            {
                Cmb_Nombre_Empleado.SelectedIndex = 0;
            }
            else    //De lo contrario selecciona el unico registro encontrado.
            {
                Cmb_Nombre_Empleado.SelectedIndex = 1;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Poliza_Desfasada
    /// DESCRIPCION : Da de alta los datos de la poliza desfasada
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 24/Octubre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Alta_Poliza_Desfasada(string[] Datos_Poliza, DataTable Dt_Movimientos, SqlCommand P_Cmmd)
    {
        Cls_Ope_Con_Bitacora_Polizas_Negocio Rs_Bitacora_Polizas = new Cls_Ope_Con_Bitacora_Polizas_Negocio();  //Variable de conexion con la capa de negocios.
        Cls_Ope_Con_Cierre_Mensual_Negocio Rs_Cierre_Polizas = new Cls_Ope_Con_Cierre_Mensual_Negocio();
        string mes;
        DateTime fecha;
        string anio;
        //SqlConnection Cn = new SqlConnection();
        //SqlCommand Cmmd = new SqlCommand();
        //SqlTransaction Trans = null;
        //if (P_Cmmd != null)
        //{
        //    Cmmd = P_Cmmd;
        //}
        //else
        //{
        //    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
        //    Cn.Open();
        //    Trans = Cn.BeginTransaction();
        //    Cmmd.Connection = Trans.Connection;
        //    Cmmd.Transaction = Trans;
        //}
        try
        {
            Rs_Bitacora_Polizas.P_No_Poliza = Datos_Poliza[0];
            Rs_Bitacora_Polizas.P_Tipo_Poliza_ID = Datos_Poliza[1];
            Rs_Bitacora_Polizas.P_Mes_Ano = Datos_Poliza[2];
            Rs_Bitacora_Polizas.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
           // Rs_Bitacora_Polizas.P_Cmmd = Cmmd;
            foreach (DataRow Registro in Dt_Movimientos.Rows)
            {
                Rs_Bitacora_Polizas.P_Cuenta_Contable_ID = Registro["CUENTA_CONTABLE_ID"].ToString();
                Rs_Bitacora_Polizas.P_Debe = Registro["DEBE"].ToString();
                Rs_Bitacora_Polizas.P_Haber = Registro["HABER"].ToString();
                Rs_Bitacora_Polizas.Alta_Bitacora();
            }
            fecha = Convert.ToDateTime(String.Format("{0:MM/dd/yy}", Convert.ToDateTime(Datos_Poliza[2].Substring(0, 2) + "/01/" + Datos_Poliza[2].Substring(2, 2))));
            mes = String.Format("{0:MMMM}", fecha).ToUpper();
            anio = String.Format("{0:yyyy}", fecha).ToUpper();
            Rs_Cierre_Polizas.P_Mes = mes;
            Rs_Cierre_Polizas.P_Anio = anio;
            Rs_Cierre_Polizas.P_Estatus = "AFECTADO";
           // Rs_Cierre_Polizas.P_Cmmd = Cmmd;
            Rs_Cierre_Polizas.Modifica_Cierre_Mensual();

            //if (P_Cmmd == null)
            //{
            //    Trans.Commit();
            //}
        }
        catch (SqlException Ex)
        {
            //if (P_Cmmd == null)
            //{
            //    Trans.Rollback();
            //}
            //if (Trans != null)
            //{
            //    Trans.Rollback();
            //}
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            //if (P_Cmmd == null)
            //{
            //    Trans.Rollback();
            //}
            //if (Trans != null)
            //{
            //    Trans.Rollback();
            //}
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            //if (P_Cmmd == null)
            //{
            //    Trans.Rollback();
            //}
            //if (Trans != null)
            //{
            //    Trans.Rollback();
            //}
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            //Cn.Close();
        }
    }
    #endregion
    #region (Grid)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Polizas_SelectedIndexChanged
    /// DESCRIPCION : Consulta los datos de la Poliza seleccionada
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 12-Septiembre-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Polizas_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Cat_Polizas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión a la capa de Negocios para la consulta de los datos del empleado
        DataTable Dt_Polizas; //Variable que obtendra los datos de la consulta
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Detalles_Poliza = new Cls_Ope_Con_Polizas_Negocio();
        DataTable Dt_Detalles_Poliza;
        DataTable Dt_Detalle = new DataTable();
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Empleados = new Cls_Ope_Con_Polizas_Negocio();
        DataRow Fila;
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Rs_Consulta_Cat_Polizas.P_No_Poliza = Grid_Polizas.SelectedRow.Cells[1].Text;
            Rs_Consulta_Cat_Polizas.P_Tipo_Poliza_ID = Grid_Polizas.SelectedRow.Cells[2].Text;
            Rs_Consulta_Cat_Polizas.P_Mes_Ano = Grid_Polizas.SelectedRow.Cells[6].Text;
           // Rs_Consulta_Cat_Polizas.P_Concepto = Grid_Polizas.SelectedRow.Cells[5].Text;
            Dt_Polizas = Rs_Consulta_Cat_Polizas.Consulta_Poliza(); //Consulta los datos de la poliza que fue seleccionada
            if (Dt_Polizas.Rows.Count > 0)
            {
                //Agrega los valores de los campos a los controles correspondientes de la forma
                foreach (DataRow Registro in Dt_Polizas.Rows)
                {
                    if (!string.IsNullOrEmpty(Registro[Ope_Con_Polizas.Campo_No_Poliza].ToString()))
                        Txt_No_Poliza.Text = Registro[Ope_Con_Polizas.Campo_No_Poliza].ToString();
                    if (!string.IsNullOrEmpty(Registro[Ope_Con_Polizas.Campo_Tipo_Poliza_ID].ToString()))
                        Cmb_Tipo_Poliza.SelectedIndex = Cmb_Tipo_Poliza.Items.IndexOf(Cmb_Tipo_Poliza.Items.FindByValue(Registro[Ope_Con_Polizas.Campo_Tipo_Poliza_ID].ToString()));
                    if (!string.IsNullOrEmpty(Registro[Ope_Con_Polizas.Campo_Fecha_Poliza].ToString()))
                        Txt_Fecha_Poliza.Text = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Registro[Ope_Con_Polizas.Campo_Fecha_Poliza].ToString()));
                    if (!string.IsNullOrEmpty(Registro[Ope_Con_Polizas.Campo_Concepto].ToString()))
                        Txt_Concepto_Poliza.Text = Registro[Ope_Con_Polizas.Campo_Concepto].ToString();
                    if (!string.IsNullOrEmpty(Registro[Ope_Con_Polizas.Campo_No_Partidas].ToString()))
                        Txt_No_Partidas.Text = Registro[Ope_Con_Polizas.Campo_No_Partidas].ToString();
                    if (!string.IsNullOrEmpty(Registro[Ope_Con_Polizas.Campo_Total_Debe].ToString()))
                        Txt_Total_Debe.Text = Registro[Ope_Con_Polizas.Campo_Total_Debe].ToString();
                    if (!string.IsNullOrEmpty(Registro[Ope_Con_Polizas.Campo_Total_Haber].ToString()))
                        Txt_Total_Haber.Text = Registro[Ope_Con_Polizas.Campo_Total_Haber].ToString();
                    if (string.IsNullOrEmpty(Registro[Ope_Con_Polizas.Campo_Prefijo].ToString().Trim()) == false)
                    {
                        Txt_Prefijo.Text = Registro[Ope_Con_Polizas.Campo_Prefijo].ToString().Trim();
                    }

                    Rs_Consulta_Detalles_Poliza.P_No_Poliza = Txt_No_Poliza.Text;
                    Rs_Consulta_Empleados.P_No_Poliza = Txt_No_Poliza.Text;
                    Rs_Consulta_Detalles_Poliza.P_Tipo_Poliza_ID = String.Format("{0:00000}", Registro[Ope_Con_Polizas.Campo_Tipo_Poliza_ID].ToString());
                    Rs_Consulta_Detalles_Poliza.P_Mes_Ano = Registro[Ope_Con_Polizas.Campo_Mes_Ano].ToString();
                }
                Dt_Detalles_Poliza = Rs_Consulta_Detalles_Poliza.Consulta_Detalles_Poliza_Seleccionada();
                if (Dt_Detalle.Rows.Count <= 0)
                {
                    //Agrega los campos que va a contener el DataTable
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Detalle.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Detalle.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
                    Dt_Detalle.Columns.Add("MOMENTO_FINAL", typeof(System.String));
                }
                foreach (DataRow Filas in Dt_Detalles_Poliza.Rows)
                {
                    Fila = Dt_Detalle.NewRow();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = Convert.ToInt32(Filas["PARTIDA"].ToString());
                    Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Filas[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
                    Fila["CUENTA"] = Filas["CUENTA"].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Concepto] = Filas[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Filas[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Filas[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    Fila[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Filas[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Filas[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Filas[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Filas[Ope_Con_Polizas_Detalles.Campo_Partida_ID].ToString();
                    Fila["MOMENTO_INICIAL"] = "";
                    Fila["MOMENTO_FINAL"] = "";
                    Dt_Detalle.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Detalle.AcceptChanges();
                }
                Session["Consulta_Poliza"] = Dt_Detalle;
                Session["Dt_Partidas_Poliza"] = Dt_Detalle;
                Llena_Grid_Polizas();
                Dt_Polizas = Rs_Consulta_Empleados.Consulta_Detalles_Empleado_Aprobo();
                foreach (DataRow Registro in Dt_Polizas.Rows)
                {
                    Cmb_Nombre_Empleado.Items.Clear();
                    Cmb_Nombre_Empleado.Items.Insert(0, new ListItem(Registro["EMPLEADO_AUTORIZO"].ToString(), Registro[Cat_Empleados.Campo_Empleado_ID].ToString()));
                }
                Dt_Polizas = Rs_Consulta_Empleados.Consulta_Detalles_Empleado_Creo();
                foreach (DataRow Registro in Dt_Polizas.Rows)
                {
                    Txt_Empleado_Creo.Text = Registro["EMPLEADO_CREO"].ToString();
                    //Cmb_Empleado_Creo.Items.Clear();
                    //Cmb_Empleado_Creo.Items.Insert(0, new ListItem(Registro["EMPLEADO_CREO"].ToString(), Registro[Cat_Empleados.Campo_Empleado_ID].ToString()));
                }
            }
            Grid_Polizas.DataSource = null;
            Grid_Polizas.DataBind();
            Txt_No_Poliza_PopUp.Text = "";
            Btn_Imprimir.Visible = true;
            //Btn_Borrador.Visible = false;

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Polizas_PageIndexChanging
    /// DESCRIPCION : Cambia la pagina de la tabla de Polizas
    ///               
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10-Septiembre-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Polizas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Limpia_Controles();                        //Limpia todos los controles de la forma
            Grid_Polizas.PageIndex = e.NewPageIndex; //Indica la Página a visualizar
            Llena_Grid_Polizas_Avanzada();                    //Carga los Polizas que estan asignados a la página seleccionada
            Grid_Polizas.SelectedIndex = -1;
            Mpe_Busqueda_Polizas.Show();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llena_Grid_Polizas
    /// DESCRIPCION : Llena el grid con las Polizas encontradas
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llena_Grid_Polizas()
    {
        DataTable Dt_Polizas; //Variable que obtendra los datos de la consulta 
        try
        {
            Grid_Detalles_Poliza.Columns[1].Visible = true;
            Grid_Detalles_Poliza.Columns[6].Visible = true;
            Grid_Detalles_Poliza.Columns[7].Visible = true;
            Grid_Detalles_Poliza.Columns[8].Visible = true;
            Grid_Detalles_Poliza.Columns[9].Visible = true;
            Grid_Detalles_Poliza.Columns[10].Visible = true;
            Grid_Detalles_Poliza.Columns[11].Visible = true;
            Grid_Detalles_Poliza.Columns[13].Visible = true;
            Grid_Detalles_Poliza.Columns[14].Visible = true;
            Dt_Polizas = (DataTable)Session["Consulta_Poliza"]; //Se obtienen los datos de la sesion.
            Grid_Detalles_Poliza.DataSource = Dt_Polizas;   // Se iguala el DataTable con el Grid.
            Grid_Detalles_Poliza.DataBind();    // Se ligan los datos.
            Grid_Detalles_Poliza.Columns[1].Visible = false;
            Grid_Detalles_Poliza.Columns[6].Visible = false;
            Grid_Detalles_Poliza.Columns[7].Visible = false;
            Grid_Detalles_Poliza.Columns[8].Visible = false;
            Grid_Detalles_Poliza.Columns[9].Visible = false;
            Grid_Detalles_Poliza.Columns[10].Visible = false;
            Grid_Detalles_Poliza.Columns[11].Visible = false;
            Grid_Detalles_Poliza.Columns[12].Visible = false;
            Grid_Detalles_Poliza.Columns[13].Visible = false;
            Grid_Detalles_Poliza.Columns[14].Visible = false;
            Grid_Detalles_Poliza.Visible = true;
            Grid_Detalles_Poliza.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Llena_Grid_Polizas " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llena_Grid_Polizas_Avanzada
    /// DESCRIPCION : Llena el grid con las Polizas encontradas
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 27/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llena_Grid_Polizas_Avanzada()
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        DataTable Dt_Polizas; //Variable que obtendra los datos de la consulta 
        try
        {
            Grid_Polizas.Columns[1].Visible = true;
            Grid_Polizas.Columns[2].Visible = true;
            Grid_Polizas.Columns[6].Visible = true;
            Grid_Polizas.DataSource = null;   // Se iguala el DataTable con el Grid
            Grid_Polizas.DataBind();    // Se ligan los datos.
            Dt_Polizas = (DataTable)Session["Consulta_Poliza_Avanzada"];    //Se obtienen los datos de la sesion.
            if (Dt_Polizas.Rows.Count == 0)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontrarón registros en la busqueda";
            }
            Grid_Polizas.DataSource = Dt_Polizas;   // Se iguala el DataTable con el Grid
            Grid_Polizas.DataBind();    // Se ligan los datos.
            Grid_Polizas.Visible = true;
            Grid_Polizas.Columns[2].Visible = false;
            Grid_Polizas.Columns[6].Visible = false;
            Grid_Polizas.SelectedIndex = -1;

        }
        catch (Exception ex)
        {
            throw new Exception("Llena_Grid_Polizas_Avanzada " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Detalles_Poliza_RowDataBound
    /// DESCRIPCION : Agrega un identificador al boton de cancelar de la tabla
    ///               para identicar la fila seleccionada de tabla.
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Detalles_Poliza_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            TextBox DEBE = (TextBox)e.Row.FindControl("Txt_DEBE_Grid");
            TextBox HABER = (TextBox)e.Row.FindControl("Txt_HABER_Grid");
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                ((ImageButton)e.Row.Cells[6].FindControl("Btn_Eliminar_Partida")).CommandArgument = e.Row.Cells[0].Text.Trim();
                ((ImageButton)e.Row.Cells[6].FindControl("Btn_Eliminar_Partida")).ToolTip = "Quitar la Partida " + e.Row.Cells[2].Text + " a la Poliza";

                if (e.Row.Cells[13].Text.ToString() == "0" && (Hdn_Cambio.Value == "DEBE" || Hdn_Cambio.Value == ""))
                {
                    DEBE.Text = "0";
                    DEBE.Enabled = false;
                    HABER.Text = e.Row.Cells[14].Text.ToString();
                   // Hdn_Cambio.Value = "";
                }
                else
                {
                    if (e.Row.Cells[14].Text.ToString() != "0" && (Hdn_Cambio.Value == "HABER" || Hdn_Cambio.Value == ""))
                    {
                        DEBE.Text = "0";
                        DEBE.Enabled = false;
                        HABER.Text = e.Row.Cells[14].Text.ToString();
                    }
                    else
                    {
                        HABER.Text = "0";
                        HABER.Enabled = false;
                        DEBE.Text = e.Row.Cells[13].Text.ToString();
                    }
                    //Hdn_Cambio.Value = "";
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Txt_Debe_TextChanged
    /// DESCRIPCION : Verfica si el contenido de la caja de texto cambio.
    /// CREO        : Salvador L. Rea Ayala
    /// FEHA_CREO  : 20/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Txt_Poliza_TextChanged(object sender, EventArgs e)
    {
        DataTable Dt_Partidas = (DataTable)Session["Dt_Partidas_Poliza"];
        try
        {
            if (Dt_Partidas != null && Dt_Partidas.Rows.Count > 0)
            {
                TextBox campo = new TextBox();
                campo = (TextBox)sender;
                DataRow[] Filas = Dt_Partidas.Select(Ope_Con_Polizas_Detalles.Campo_Partida +
                   "='" + campo.CssClass + "'");

                if (!(Filas == null))
                {
                    if (Filas.Length > 0)
                    {
                        if (campo.ID == "Txt_DEBE_Grid")
                        {
                            Filas[0]["DEBE"] = (campo.Text == "") ? "0" : campo.Text;
                            Filas[0]["HABER"] = "0";
                            Hdn_Cambio.Value = "DEBE";
                        }
                        else
                        {
                            Filas[0]["DEBE"] = "0";
                            Filas[0]["HABER"] = (campo.Text == "") ? "0" : campo.Text;
                            Hdn_Cambio.Value = "HABER";
                        }
                        Session["Dt_Partidas_Poliza"] = Dt_Partidas;
                        Grid_Detalles_Poliza.Columns[1].Visible = true;
                        Grid_Detalles_Poliza.Columns[6].Visible = true;
                        Grid_Detalles_Poliza.Columns[7].Visible = true;
                        Grid_Detalles_Poliza.Columns[8].Visible = true;
                        Grid_Detalles_Poliza.Columns[9].Visible = true;
                        Grid_Detalles_Poliza.Columns[10].Visible = true;
                        Grid_Detalles_Poliza.Columns[11].Visible = true;
                        Grid_Detalles_Poliza.Columns[13].Visible = true;
                        Grid_Detalles_Poliza.Columns[14].Visible = true;
                        Grid_Detalles_Poliza.DataSource = (DataTable)Session["Dt_Partidas_Poliza"];
                        Grid_Detalles_Poliza.DataBind();
                        Grid_Detalles_Poliza.Columns[1].Visible = false;
                        Grid_Detalles_Poliza.Columns[6].Visible = false;
                        Grid_Detalles_Poliza.Columns[7].Visible = false;
                        Grid_Detalles_Poliza.Columns[8].Visible = false;
                        Grid_Detalles_Poliza.Columns[9].Visible = false;
                        Grid_Detalles_Poliza.Columns[10].Visible = false;
                        Grid_Detalles_Poliza.Columns[11].Visible = false;
                        Grid_Detalles_Poliza.Columns[13].Visible = false;
                        Grid_Detalles_Poliza.Columns[14].Visible = false;
                        Montos_Debe_Haber_Poliza(); //Obtiwene el monto Total del Debe y Haber de la Poliza
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Txt_Poliza_TextChanged " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Cuentas_Contables_Tipo_Polizas
    /// DESCRIPCION : Carga las Percepciones Deducciones Fijas o Variables que no son calculadas
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10/Noviembre/Txt_Poliza_TextChanged
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consultar_Cuentas_Contables_Haber(String Cuenta_Id)
    {
        DataTable Dt_Cuentas_Contables = null;  //Almacenara los datos de las cuentas contables.
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexion con la capa de Datos

        try
        {
            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cuenta_Id;
            Dt_Cuentas_Contables = Rs_Consulta_Con_Cuentas_Contables.Consulta_Matriz(); //Consulta las cuentas contables
            Cmb_Descripcion.DataSource = Dt_Cuentas_Contables; //Liga los datos con el combo
            Cmb_Descripcion.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;    //Asigna el campo de la tabla que se visualizara
            Cmb_Descripcion.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;    //Asigna el campo de la tabla que sera usado como valor
            Cmb_Descripcion.DataBind();
            Cmb_Descripcion.Items.Insert(0, new ListItem("< Seleccione >", ""));
            Cmb_Descripcion.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error generado al consultar las Percepciones Deducciones. Error: [" + Ex.Message + "]");
        }
    }
    private void Consultar_Cuentas_Contables_Debe(String Cuenta_Id)
    {
        DataTable Dt_Cuentas_Contables = null;  //Almacenara los datos de las cuentas contables.
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexion con la capa de Datos

        try
        {
            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cuenta_Id;
            //  Dt_Cuentas_Contables = Rs_Consulta_Con_Cuentas_Contables.Consulta_Matriz_Debe(); //Consulta las cuentas contables
            Cmb_Descripcion.DataSource = Dt_Cuentas_Contables; //Liga los datos con el combo
            Cmb_Descripcion.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;    //Asigna el campo de la tabla que se visualizara
            Cmb_Descripcion.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;    //Asigna el campo de la tabla que sera usado como valor
            Cmb_Descripcion.DataBind();
            Cmb_Descripcion.Items.Insert(0, new ListItem("< Seleccione >", ""));
            Cmb_Descripcion.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error generado al consultar las Percepciones Deducciones. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
    #region (Eventos)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Carga_Prepoliza_Popup_Click
    /// DESCRIPCION : Consulta los datos de la Pre-Poliza seleccionada
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 12-Septiembre-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    ////protected void Btn_Carga_Prepoliza_Popup_Click(object sender, EventArgs e)
    ////{
    ////    Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Cat_Polizas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión a la capa de Negocios para la consulta de los datos del empleado
    ////    DataTable Dt_Polizas; //Variable que obtendra los datos de la consulta
    ////    Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Detalles_Poliza = new Cls_Ope_Con_Polizas_Negocio();
    ////    DataTable Dt_Detalles_Poliza;
    ////    DataTable Dt_Detalle = new DataTable();
    ////    Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Empleados = new Cls_Ope_Con_Polizas_Negocio();
    ////    DataRow Fila;
    ////    try
    ////    {
    ////        Lbl_Mensaje_Error.Visible = false;
    ////        Img_Error.Visible = false;
    ////        if (Cmb_Prepolizas.SelectedIndex > 0)
    ////        {
    ////            Rs_Consulta_Cat_Polizas.P_Prepoliza_ID = Cmb_Prepolizas.SelectedValue;
    ////            Dt_Polizas = Rs_Consulta_Cat_Polizas.Consulta_Prepoliza_Datos(); //Consulta los datos de la poliza que fue seleccionada
    ////            if (Dt_Polizas.Rows.Count > 0)
    ////            {
    ////                Hd_Prepoliza.Value = Cmb_Prepolizas.SelectedValue;
    ////                //Agrega los valores de los campos a los controles correspondientes de la forma
    ////                if (!string.IsNullOrEmpty(Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_Concepto].ToString()))
    ////                    Txt_Concepto_Poliza.Text = Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_Concepto].ToString();
    ////                if (!string.IsNullOrEmpty(Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_Tipo_Poliza_ID].ToString()))
    ////                    Cmb_Tipo_Poliza.SelectedIndex = Cmb_Tipo_Poliza.Items.IndexOf(Cmb_Tipo_Poliza.Items.FindByValue(Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_Tipo_Poliza_ID].ToString()));
    ////                if (!string.IsNullOrEmpty(Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_Momento_Ingresos].ToString()))
    ////                    Cmb_Momento_Ingresos.SelectedIndex = Cmb_Momento_Ingresos.Items.IndexOf(Cmb_Momento_Ingresos.Items.FindByValue(Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_Momento_Ingresos].ToString()));
    ////                if (!string.IsNullOrEmpty(Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_No_Partidas].ToString()))
    ////                    Txt_No_Partidas.Text = Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_No_Partidas].ToString();
    ////                if (!string.IsNullOrEmpty(Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_Total_Debe].ToString()))
    ////                    Txt_Total_Debe.Text = Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_Total_Debe].ToString();
    ////                if (!string.IsNullOrEmpty(Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_Total_Haber].ToString()))
    ////                    Txt_Total_Haber.Text = Dt_Polizas.Rows[0][Ope_Con_Prepolizas.Campo_Total_Haber].ToString();

    ////                Rs_Consulta_Detalles_Poliza.P_Prepoliza_ID = Hd_Prepoliza.Value;
    ////                Dt_Detalles_Poliza = Rs_Consulta_Detalles_Poliza.Consulta_Detalles_Prepoliza_Seleccionada();
    ////                if (Dt_Detalle.Rows.Count <= 0)
    ////                {
    ////                    //Agrega los campos que va a contener el DataTable
    ////                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
    ////                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
    ////                    Dt_Detalle.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
    ////                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
    ////                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
    ////                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
    ////                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
    ////                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
    ////                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
    ////                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
    ////                    Dt_Detalle.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
    ////                    Dt_Detalle.Columns.Add("MOMENTO_FINAL", typeof(System.String));
    ////                }
    ////                foreach (DataRow Filas in Dt_Detalles_Poliza.Rows)
    ////                {
    ////                    if (Filas[Ope_Con_PrePolizas_Detalles.Campo_Momento_Inicial].ToString() != "" && Filas[Ope_Con_PrePolizas_Detalles.Campo_Momento_Final].ToString() != "")
    ////                    {
    ////                        Cmb_Momento_incial.SelectedItem.Text  = Filas[Ope_Con_PrePolizas_Detalles.Campo_Momento_Inicial].ToString();
    ////                        Cmb_Momento_Final.SelectedItem.Text = Filas[Ope_Con_PrePolizas_Detalles.Campo_Momento_Final].ToString();
    ////                    }
    ////                    Fila = Dt_Detalle.NewRow();
    ////                    Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = Convert.ToInt32(Filas["PARTIDA"].ToString());
    ////                    Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Filas[Ope_Con_PrePolizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
    ////                    Fila["CUENTA"] = Filas["CUENTA"].ToString();
    ////                    Fila[Ope_Con_Polizas_Detalles.Campo_Concepto] = Filas[Ope_Con_PrePolizas_Detalles.Campo_Concepto].ToString();
    ////                    Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Filas[Ope_Con_PrePolizas_Detalles.Campo_Debe].ToString());
    ////                    Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Filas[Ope_Con_PrePolizas_Detalles.Campo_Haber].ToString());
    ////                    Fila[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Filas[Ope_Con_PrePolizas_Detalles.Campo_Dependencia_ID].ToString();
    ////                    Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Filas[Ope_Con_PrePolizas_Detalles.Campo_Proyecto_Programa_ID].ToString();
    ////                    Fila[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Filas[Ope_Con_PrePolizas_Detalles.Campo_Fuente_Financiamiento_ID].ToString();
    ////                    Fila[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Filas[Ope_Con_PrePolizas_Detalles.Campo_Partida_ID].ToString();
    ////                    Fila["MOMENTO_INICIAL"] = Filas[Ope_Con_PrePolizas_Detalles.Campo_Momento_Inicial].ToString();
    ////                    Fila["MOMENTO_FINAL"] = Filas[Ope_Con_PrePolizas_Detalles.Campo_Momento_Final].ToString();
    ////                    Dt_Detalle.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
    ////                    Dt_Detalle.AcceptChanges();
    ////                }
    ////                Session["Consulta_Poliza"] = Dt_Detalle;
    ////                Session["Dt_Partidas_Poliza"] = Dt_Detalle;
    ////                Grid_Detalles_Poliza.Columns[1].Visible = true;
    ////                Grid_Detalles_Poliza.Columns[6].Visible = true;
    ////                Grid_Detalles_Poliza.Columns[7].Visible = true;
    ////                Grid_Detalles_Poliza.Columns[8].Visible = true;
    ////                Grid_Detalles_Poliza.Columns[9].Visible = true;
    ////                Grid_Detalles_Poliza.Columns[10].Visible = true;
    ////                Grid_Detalles_Poliza.Columns[11].Visible = true;
    ////                Grid_Detalles_Poliza.Columns[13].Visible = true;
    ////                Grid_Detalles_Poliza.Columns[14].Visible = true;
    ////                Grid_Detalles_Poliza.DataSource = Dt_Detalle;   // Se iguala el DataTable con el Grid.
    ////                Grid_Detalles_Poliza.DataBind();    // Se ligan los datos.
    ////                Grid_Detalles_Poliza.Columns[1].Visible = false;
    ////                Grid_Detalles_Poliza.Columns[6].Visible = false;
    ////                Grid_Detalles_Poliza.Columns[7].Visible = false;
    ////                Grid_Detalles_Poliza.Columns[8].Visible = false;
    ////                Grid_Detalles_Poliza.Columns[9].Visible = false;
    ////                Grid_Detalles_Poliza.Columns[10].Visible = false;
    ////                Grid_Detalles_Poliza.Columns[11].Visible = false;
    ////                Grid_Detalles_Poliza.Columns[13].Visible = false;
    ////                Grid_Detalles_Poliza.Columns[14].Visible = false;
    ////            }
    ////            Div_Pre_Poliza.Style.Value = "display:none";
    ////            Habilitar_Controles("Prepoliza"); //Habilita los controles para la modificación de los datos
    ////            Txt_Empleado_Creo.Text = Cls_Sessiones.Nombre_Empleado;
    ////            Btn_Borradores_Guardados.Visible = false;
    ////        }
    ////        else
    ////        {
    ////            Hd_Prepoliza.Value = "";
    ////        }

    ////    }
    ////    catch (Exception ex)
    ////    {
    ////        Lbl_Mensaje_Error.Visible = true;
    ////        Img_Error.Visible = true;
    ////        Lbl_Mensaje_Error.Text = ex.Message.ToString();
    ////    }
    ////}
    protected void Btn_Agregar_Partida_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los datos de la póliza que fueron proporcionados por el usuario
        String Espacios = "";
        //string Codigo_Programatico = "";
        int error = 0;
        Boolean Mensaje_Error = false;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Hdn_Cambio.Value = "";
        //Valida que todos los datos requeridos los haya proporcionado el usuario
        if ((!String.IsNullOrEmpty(Txt_Debe_Partida.Text) || !String.IsNullOrEmpty(Txt_Haber_Partida.Text)) && !String.IsNullOrEmpty(Txt_Concepto_Partida.Text))
        {
            if (Validar_Partida_Presupuestal())
            {
                //Si no se a agregado ninguna partida entonces crea la Estructura necesaria para poder visualizar la información al usuario
                if (Session["Dt_Partidas_Poliza"] == null)
                {
                    //Agrega los campos que va a contener el DataTable
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("MOMENTO_FINAL", typeof(System.String));
                }
                //Si se tiene agregada ya partidas a la póliza entonces agrega la estructura a la tabla para poder insertar
                //los datos que el usuario introdujo a la póliza
                else
                {
                    Dt_Partidas_Polizas = (DataTable)Session["Dt_Partidas_Poliza"];
                    Session.Remove("Dt_Partidas_Poliza");
                }
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                if (String.IsNullOrEmpty(Txt_Debe_Partida.Text))
                {
                    Txt_Debe_Partida.Text = "0";
                }
                if (String.IsNullOrEmpty(Txt_Haber_Partida.Text))
                {
                    Txt_Haber_Partida.Text = "0";
                }
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = Dt_Partidas_Polizas.Rows.Count + 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cmb_Descripcion.SelectedValue;
                row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Convert.ToString(Txt_Cuenta_Contable.Text.ToString());
                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Partida.Text.ToString();
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Debe_Partida.Text.ToString());
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Txt_Haber_Partida.Text.ToString());
                if (Div_Presupuestal.Style.Value == "display:block;")
                {
                    if (Div_Ingresos.Style.Value == "display:block;")
                    {
                        row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Convert.ToString(Cmb_Fuente_Financiamiento.SelectedValue);
                        row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = "";
                        row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Convert.ToString(Cmb_Programas_Ing.SelectedValue);
                        row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = "";
                    }
                    else
                    {
                        if (Div_Egresos.Style.Value == "display:block;")
                        {
                            row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Convert.ToString(Cmb_Fuente_Financiamiento_Egr.SelectedValue);
                            row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Convert.ToString(Cmb_Unidad_Responsable.SelectedValue);
                            row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Convert.ToString(Cmb_Programa.SelectedValue);
                            row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Convert.ToString(Txt_Partida_Presupuestal.Value);
                        }
                    }
                }
                else
                {
                    row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = "";
                    row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = "";
                    row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = "";
                    row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = "";
                }
                row["MOMENTO_INICIAL"] = "";// Cmb_Momento_incial.SelectedItem.Text.Trim();
                row["MOMENTO_FINAL"] = "";//Cmb_Momento_Final.SelectedItem.Text.Trim();

                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                Session["Dt_Partidas_Poliza"] = Dt_Partidas_Polizas;//Agrega los valores del registro a la sesión

                Grid_Detalles_Poliza.Columns[1].Visible = true;
                Grid_Detalles_Poliza.Columns[6].Visible = true;
                Grid_Detalles_Poliza.Columns[7].Visible = true;
                Grid_Detalles_Poliza.Columns[8].Visible = true;
                Grid_Detalles_Poliza.Columns[9].Visible = true;
                Grid_Detalles_Poliza.Columns[10].Visible = true;
                Grid_Detalles_Poliza.Columns[11].Visible = true;
                Grid_Detalles_Poliza.Columns[13].Visible = true;
                Grid_Detalles_Poliza.Columns[14].Visible = true;
                Grid_Detalles_Poliza.DataSource = Dt_Partidas_Polizas; //Agrega los valores de todas las partidas que se tienen al grid
                Grid_Detalles_Poliza.DataBind();
                Grid_Detalles_Poliza.Columns[1].Visible = false;
                Grid_Detalles_Poliza.Columns[6].Visible = false;
                Grid_Detalles_Poliza.Columns[7].Visible = false;
                Grid_Detalles_Poliza.Columns[8].Visible = false;
                Grid_Detalles_Poliza.Columns[9].Visible = false;
                Grid_Detalles_Poliza.Columns[10].Visible = false;
                Grid_Detalles_Poliza.Columns[11].Visible = false;
                Grid_Detalles_Poliza.Columns[13].Visible = false;
                Grid_Detalles_Poliza.Columns[14].Visible = false;
                Txt_Cuenta_Contable.Text = "";
                Txt_Debe_Partida.Text = "";
                Txt_Haber_Partida.Text = "";
                Txt_Debe_Partida.Enabled = true;
                Txt_Haber_Partida.Enabled = true;
                Txt_Concepto_Partida.Text = "";
                Cmb_Descripcion.SelectedIndex = -1;
                Cmb_Unidad_Responsable.Items.Clear();
                Cmb_Programa.Items.Clear();
                Cmb_Fuente_Financiamiento.Items.Clear();
                Cmb_Fuente_Financiamiento_Egr.Items.Clear();
                Cmb_Programas_Ing.Items.Clear();
                Txt_Partida_Presupuestal.Value = "";
                Montos_Debe_Haber_Poliza(); //Obtiene el monto Total del Debe y Haber de la Poliza
                Div_Ingresos.Style.Value = "display:none;";
                Div_Egresos.Style.Value = "display:none;";
            }
            else
            {
                Mensaje_Error = true;
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        else
        {
            error = 1;
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
            if (Cmb_Descripcion.SelectedIndex == -1)
            {
                Lbl_Mensaje_Error.Text += Espacios + " + Numero de Cuenta <br>";
            }
            if (String.IsNullOrEmpty(Txt_Concepto_Partida.Text))
            {
                Lbl_Mensaje_Error.Text += Espacios + " + Concepto de la Partida de la Cuenta Contable <br>";
            }
            if (String.IsNullOrEmpty(Txt_Debe_Partida.Text) && String.IsNullOrEmpty(Txt_Haber_Partida.Text))
            {
                Lbl_Mensaje_Error.Text += Espacios + " + El Monto del Debe o Haber de la partida <br>";
            }
        }
        if (error == 1)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
    }
    protected void Btn_Eliminar_Partida(object sender, EventArgs e)
    {
        Int32 Count_Fila = 1;
        ImageButton Btn_Eliminar_Partida_Poliza = (ImageButton)sender;
        DataTable Dt_Partidas = (DataTable)Session["Dt_Partidas_Poliza"];
        DataRow[] Filas = Dt_Partidas.Select(Ope_Con_Polizas_Detalles.Campo_Partida +
                "='" + Btn_Eliminar_Partida_Poliza.CommandArgument + "'");

        if (!(Filas == null))
        {
            if (Filas.Length > 0)
            {
                Dt_Partidas.Rows.Remove(Filas[0]);

                //Suma todos los Debe y Haber de la póliza
                foreach (DataRow Registro in Dt_Partidas.Rows)
                {
                    Registro[Ope_Con_Polizas_Detalles.Campo_Partida] = Count_Fila;
                    Count_Fila = Convert.ToInt32(Count_Fila) + 1;
                }
                Session["Dt_Partidas_Poliza"] = Dt_Partidas;
                Grid_Detalles_Poliza.Columns[1].Visible = true;
                Grid_Detalles_Poliza.Columns[6].Visible = true;
                Grid_Detalles_Poliza.Columns[7].Visible = true;
                Grid_Detalles_Poliza.Columns[8].Visible = true;
                Grid_Detalles_Poliza.Columns[9].Visible = true;
                Grid_Detalles_Poliza.Columns[10].Visible = true;
                Grid_Detalles_Poliza.Columns[11].Visible = true;
                Grid_Detalles_Poliza.Columns[13].Visible = true;
                Grid_Detalles_Poliza.Columns[14].Visible = true;
                Grid_Detalles_Poliza.DataSource = (DataTable)Session["Dt_Partidas_Poliza"];
                Grid_Detalles_Poliza.DataBind();
                Grid_Detalles_Poliza.Columns[1].Visible = false;
                Grid_Detalles_Poliza.Columns[6].Visible = false;
                Grid_Detalles_Poliza.Columns[7].Visible = false;
                Grid_Detalles_Poliza.Columns[8].Visible = false;
                Grid_Detalles_Poliza.Columns[9].Visible = false;
                Grid_Detalles_Poliza.Columns[10].Visible = false;
                Grid_Detalles_Poliza.Columns[11].Visible = false;
                Grid_Detalles_Poliza.Columns[13].Visible = false;
                Grid_Detalles_Poliza.Columns[14].Visible = false;
                Montos_Debe_Haber_Poliza(); //Obtiwene el monto Total del Debe y Haber de la Poliza
            }
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Descripcion_SelectedIndexChanged
    /// DESCRIPCION : Consulta la cuenta contable de la descripción de la cuentan
    ///               contable que fue seleccionada por el usuario
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Descripcion_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Cuenta_Contable = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario
        Cls_Ope_Con_Polizas_Negocio Rs_Fuentes_financiamiento = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Fuentes_financiamiento = new DataTable();   //Almacena la partida especifica asociada
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Presupuesto = new Cls_Ope_Con_Polizas_Negocio();    //Variable de conexión a la capa de negocios
        // DataTable Dt_Presupuesto = null;    //Almacena el ID de la Dependencia asociada
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cmb_Fuente_Financiamiento.Items.Clear();
            Cmb_Programa.Items.Clear();
            Cmb_Unidad_Responsable.Items.Clear();
            Cmb_Fuente_Financiamiento_Egr.Items.Clear();

            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Descripcion.SelectedValue;
            Dt_Cuenta_Contable = Rs_Consulta_Con_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();  //Consulta los datos de la cuenta contable seleccionada
            if (Dt_Cuenta_Contable.Rows.Count > 0)
            {
                //Agrega la cuenta contable a la caja de texto correspondiente
                foreach (DataRow Registro in Dt_Cuenta_Contable.Rows)
                {
                    Txt_Cuenta_Contable.Text = Aplicar_Mascara_Cuenta_Contable(Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString()).ToString();

                    if (String.IsNullOrEmpty(Txt_Concepto_Poliza.Text))
                        Txt_Concepto_Partida.Text = Cmb_Descripcion.SelectedItem.Text.ToString().Substring(10);
                    else
                        Txt_Concepto_Partida.Text = Txt_Concepto_Poliza.Text;
                }
                ////if (Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Tipo_Presupuestal].ToString() == "INGRESOS" && Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString() != "")
                ////{
                ////    Div_Presupuestal.Style.Add("display", "block");
                ////    Div_Ingresos.Style.Add("display", "block");
                ////    Div_Egresos.Style.Add("display", "none");
                ////    Rs_Fuentes_financiamiento.P_Partida_ID = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                ////    Dt_Fuentes_financiamiento = Rs_Fuentes_financiamiento.Consulta_Fuente_Financiamiento();
                ////    Cmb_Fuente_Financiamiento.DataSource = Dt_Fuentes_financiamiento; //Liga los datos con el combo
                ////    Cmb_Fuente_Financiamiento.DataTextField = "CLAVE_NOMBRE";    //Asigna el campo de la tabla que se visualizara
                ////    Cmb_Fuente_Financiamiento.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;    //Asigna el campo de la tabla que sera usado como valor
                ////    Cmb_Fuente_Financiamiento.DataBind();
                ////    Cmb_Fuente_Financiamiento.Items.Insert(0, new ListItem("< Seleccione >", ""));
                ////    Cmb_Fuente_Financiamiento.SelectedIndex = -1;
                ////    Txt_Partida_Presupuestal.Value = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                ////}
                ////else
                ////{
                ////    if (Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Tipo_Presupuestal].ToString() == "EGRESOS" && Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString() != "")
                ////    {
                ////        Div_Presupuestal.Style.Add("display", "block");
                ////        Div_Ingresos.Style.Add("display", "none");
                ////        Div_Egresos.Style.Add("display", "block");
                ////        Rs_Fuentes_financiamiento.P_Partida_ID = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                ////        Dt_Fuentes_financiamiento = Rs_Fuentes_financiamiento.Consulta_Fuente_Financiamiento_Egr();
                ////        Cmb_Fuente_Financiamiento_Egr.DataSource = Dt_Fuentes_financiamiento; //Liga los datos con el combo
                ////        Cmb_Fuente_Financiamiento_Egr.DataTextField = "CLAVE_NOMBRE";    //Asigna el campo de la tabla que se visualizara
                ////        Cmb_Fuente_Financiamiento_Egr.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;    //Asigna el campo de la tabla que sera usado como valor
                ////        Cmb_Fuente_Financiamiento_Egr.DataBind();
                ////        Cmb_Fuente_Financiamiento_Egr.Items.Insert(0, new ListItem("< Seleccione >", ""));
                ////        Cmb_Fuente_Financiamiento_Egr.SelectedIndex = -1;
                ////        Txt_Partida_Presupuestal.Value = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                ////    }
                ////    else
                ////    {
                ////        Div_Presupuestal.Style.Add("display", "none");
                ////        Div_Ingresos.Style.Add("display", "none");
                ////        Div_Egresos.Style.Add("display", "none");
                ////        Txt_Partida_Presupuestal.Value = "";
                ////    }
                ////}
                Txt_Debe_Partida.Focus();
            }
            Upnl_Partidas_Polizas.Update();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Programas_Poliza_SelectedIndexChanged
    /// DESCRIPCION : Consulta la cuenta contable de la descripción de la cuentan
    ///               contable que fue seleccionada por el usuario
    /// CREO        : Sergio Manuel Gallardo
    /// FECHA_CREO  : 29/Junio/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Programas_Poliza_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable Dt_Monto = new DataTable();   //Almacena la partida especifica asociada
        DataTable Dt_Cuentas = new DataTable();   //Almacena la partida especifica asociada
        DataTable Dt_Partidas_Polizas = new DataTable();
        Double Partida = 0;
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Monto_Programa = new Cls_Ope_Con_Polizas_Negocio();    //Variable de conexión a la capa de negocios
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Cuentas_Contables_ID = new Cls_Ope_Con_Polizas_Negocio();
        // DataTable Dt_Presupuesto = null;    //Almacena el ID de la Dependencia asociada
        try
        {
            //Session["Dt_Partidas_Poliza"] = null;
            //Grid_Detalles_Poliza.DataSource = null; //Agrega los valores de todas las partidas que se tienen al grid
            //Grid_Detalles_Poliza.DataBind();
            //Txt_Monto_Programa.Text = "";

            //if (Cmb_Programas_Poliza.SelectedIndex > 0)
            //{
            //    Rs_Consulta_Monto_Programa.P_Programa_ID = Cmb_Programas_Poliza.SelectedValue;
            //    Dt_Monto = Rs_Consulta_Monto_Programa.Consulta_Programas_Ing();  //Consulta los datos de la cuenta contable seleccionada
            //    if (Dt_Monto.Rows.Count > 0)
            //    {
            //        if (String.IsNullOrEmpty(Dt_Monto.Rows[0]["IMPORTE"].ToString()))
            //        {
            //            Txt_Monto_Programa.Text = "0";
            //        }
            //        else
            //        {
            //            Txt_Monto_Programa.Text = Dt_Monto.Rows[0]["IMPORTE"].ToString();
            //        }
            //        Rs_Consulta_Cuentas_Contables_ID.P_Programa_ID = Cmb_Programas_Poliza.SelectedValue;
            //        Dt_Cuentas = Rs_Consulta_Cuentas_Contables_ID.Consulta_Cuentas_Contables_De_Conceptos();
            //        if (Dt_Cuentas.Rows.Count > 0)
            //        {
            //            if (Session["Dt_Partidas_Poliza"] == null)
            //            {
            //                //Agrega los campos que va a contener el DataTable
            //                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            //                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            //                Dt_Partidas_Polizas.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
            //                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            //                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            //                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            //                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
            //                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
            //                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
            //                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
            //                Dt_Partidas_Polizas.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
            //                Dt_Partidas_Polizas.Columns.Add("MOMENTO_FINAL", typeof(System.String));
            //            }
            //            //Si se tiene agregada ya partidas a la póliza entonces agrega la estructura a la tabla para poder insertar
            //            //los datos que el usuario introdujo a la póliza
            //            else
            //            {
            //                Dt_Partidas_Polizas = (DataTable)Session["Dt_Partidas_Poliza"];
            //                Session.Remove("Dt_Partidas_Poliza");
            //            }
            //            Partida = Partida + 1;
            //            foreach (DataRow Fila in Dt_Cuentas.Rows)
            //            {
            //                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
            //                row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
            //                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Fila["CUENTA_CONTABLE_ID"].ToString();
            //                row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Fila["CUENTA"].ToString();
            //                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Fila["DESCRIPCION"].ToString();
            //                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Fila["IMPORTE"].ToString());
            //                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble("0");
            //                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Fila["FTE_FINANCIAMIENTO_ID"].ToString();
            //                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = "";
            //                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = "";
            //                row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = "";
            //                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            //                Dt_Partidas_Polizas.AcceptChanges();
            //                Session["Dt_Partidas_Poliza"] = Dt_Partidas_Polizas;//Agrega los valores del registro a la sesión
            //                Partida = Partida + 1;
            //            }
            //            Grid_Detalles_Poliza.Columns[1].Visible = true;
            //            Grid_Detalles_Poliza.Columns[6].Visible = true;
            //            Grid_Detalles_Poliza.Columns[7].Visible = true;
            //            Grid_Detalles_Poliza.Columns[8].Visible = true;
            //            Grid_Detalles_Poliza.Columns[9].Visible = true;
            //            Grid_Detalles_Poliza.Columns[10].Visible = true;
            //            Grid_Detalles_Poliza.Columns[11].Visible = true;
            //            Grid_Detalles_Poliza.Columns[13].Visible = true;
            //            Grid_Detalles_Poliza.Columns[14].Visible = true;
            //            Grid_Detalles_Poliza.DataSource = Dt_Partidas_Polizas; //Agrega los valores de todas las partidas que se tienen al grid
            //            Grid_Detalles_Poliza.DataBind();
            //            Grid_Detalles_Poliza.Columns[1].Visible = false;
            //            Grid_Detalles_Poliza.Columns[6].Visible = false;
            //            Grid_Detalles_Poliza.Columns[7].Visible = false;
            //            Grid_Detalles_Poliza.Columns[8].Visible = false;
            //            Grid_Detalles_Poliza.Columns[9].Visible = false;
            //            Grid_Detalles_Poliza.Columns[10].Visible = false;
            //            Grid_Detalles_Poliza.Columns[11].Visible = false;
            //            Grid_Detalles_Poliza.Columns[13].Visible = false;
            //            Grid_Detalles_Poliza.Columns[14].Visible = false;
            //            Montos_Debe_Haber_Poliza(); //Obtiene el monto Total del Debe y Haber de la Poliza
            //        }
            //    }
            //}
            //else
            //{
            //    Dt_Partidas_Polizas = null;
            //    Session["Dt_Partidas_Poliza"] = null;
            //    Grid_Detalles_Poliza.DataSource = Dt_Partidas_Polizas; //Agrega los valores de todas las partidas que se tienen al grid
            //    Grid_Detalles_Poliza.DataBind();
            //    Txt_Monto_Programa.Text = "";
            //}
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Fuente_Financiamiento_Egr_SelectedIndexChanged
    /// DESCRIPCION : Al momento de seleccionar una FUENTE DE FINANCIAMIENTO SE SELECCIONA BUSCAN LAS DEPENDENCIAS 
    /// pueden manejar
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 20/ABRIL/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Fuente_Financiamiento_Egr_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Con_Dependencias = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión a la capa de negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Partida = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        DataTable Dt_Partida = null;
        DataTable Dt_Dependencias = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario
        String Partida = "";
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cmb_Programa.Items.Clear();
            Cmb_Unidad_Responsable.Items.Clear();
            Rs_Consulta_Partida.P_Cuenta_Contable_ID = Cmb_Descripcion.SelectedValue;
            Dt_Partida = Rs_Consulta_Partida.Consulta_Datos_Cuentas_Contables();
            if (Dt_Partida.Rows.Count > 0)
            {
                Partida = Dt_Partida.Rows[0]["PARTIDA_ID"].ToString();
            }
            Rs_Consulta_Con_Dependencias.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Egr.SelectedValue;
            Rs_Consulta_Con_Dependencias.P_Partida_ID = Partida;
            Dt_Dependencias = Rs_Consulta_Con_Dependencias.Consulta_Dependencia();  //Consulta los datos de la cuenta contable seleccionada
            if (Dt_Dependencias.Rows.Count > 0)
            {
                Cmb_Unidad_Responsable.DataSource = Dt_Dependencias; //Liga los datos con el combo
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";    //Asigna el campo de la tabla que se visualizara
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA";    //Asigna el campo de la tabla que sera usado como valor
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< Seleccione >", ""));
                Cmb_Unidad_Responsable.SelectedIndex = -1;
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Tipo_Poliza_SelectedIndexChanged
    /// DESCRIPCION : Al momento de seleccionar una FUENTE DE FINANCIAMIENTO SE SELECCIONA BUSCAN LAS DEPENDENCIAS 
    /// pueden manejar
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 20/ABRIL/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Tipo_Poliza_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //if (Cmb_Tipo_Poliza.SelectedItem.Text.ToString().Equals("PROGRAMAS"))
            //{
            //    Td_Monto.Style.Add("Display", "block");
            //    Td_Monto_Text.Style.Add("Display", "block");
            //    Td_Programa.Style.Add("Display", "block");
            //    Td_Programa_Combo.Style.Add("Display", "block");
            //}
            //else
            //{
            //    Cmb_Programas_Poliza.SelectedIndex = 0;
            //    Txt_Monto_Programa.Text = "";
            //    Td_Monto.Style.Add("Display", "none");
            //    Td_Monto_Text.Style.Add("Display", "none");
            //    Td_Programa.Style.Add("Display", "none");
            //    Td_Programa_Combo.Style.Add("Display", "none");
            //    Session["Dt_Partidas_Poliza"] = null;
            //    Grid_Detalles_Poliza.DataSource = null; //Agrega los valores de todas las partidas que se tienen al grid
            //    Grid_Detalles_Poliza.DataBind();
            //    Txt_Monto_Programa.Text = "";
            //    Txt_Total_Debe.Text = "";
            //    Txt_Total_Haber.Text = "";

            //}

            //Colocar el tipo del prefijo
            if (Cmb_Tipo_Poliza.SelectedIndex > 0 && String.IsNullOrEmpty(Txt_Fecha_Poliza.Text) == false)
            {
                //verificar si la fecha es de tipo fecha
                if (Validar_Formato_Fecha(Txt_Fecha_Poliza.Text.Trim()))
                {
                    //Guardar las sesiones del tipo de la poliza y la fecha
                    HttpContext.Current.Session["Fecha_Poliza"] = Convert.ToDateTime(Txt_Fecha_Poliza.Text);
                    HttpContext.Current.Session["Tipo_Poliza"] = Cmb_Tipo_Poliza.SelectedItem.Value;

                    //Seleccionar el tipo de poliza para hacer el prefijo
                    switch (Cmb_Tipo_Poliza.SelectedItem.Value)
                    {
                        case "00001": //Ingresos
                        case "00002": //Egresos
                        case "00003": //Diario
                            Txt_Prefijo.Text = Construye_Prefijo(Cmb_Tipo_Poliza.SelectedItem.Value, String.Format("{0:MMyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text)));
                            break;

                        default:
                            //limpiar la caja de texto del prefijo
                            Txt_Prefijo.Text = "";
                            break;
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Favor de seleccionar una fecha.";
                    Cmb_Tipo_Poliza.SelectedIndex = 0;

                    //Eliminar las sesiones de las fechas y el tipo de la poliza
                    HttpContext.Current.Session.Remove("Fecha_Poliza");
                    HttpContext.Current.Session.Remove("Tipo_Poliza");
                }
            }
            else
            {
                //limpiar la caja de texto del prefijo
                Txt_Prefijo.Text = "";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Fuente_Financiamiento_Ing_SelectedIndexChanged
    /// DESCRIPCION : Al momento de seleccionar una FUENTE DE FINANCIAMIENTO SE SELECCIONA BUSCAN LAS DEPENDENCIAS 
    /// pueden manejar
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 26/junio/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Fuente_Financiamiento_Ing_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Polizas_Negocio Rs_Programas_Ingresos = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Programas_Ingresos = new DataTable();   //Almacena la partida especifica asociada
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cmb_Programas_Ing.Items.Clear();
            if (Cmb_Fuente_Financiamiento.SelectedIndex > 0)
            {
                Rs_Programas_Ingresos.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento.SelectedValue;
                Dt_Programas_Ingresos = Rs_Programas_Ingresos.Consulta_Programas_Ing();
                if (Dt_Programas_Ingresos.Rows.Count > 0)
                {
                    Cmb_Programas_Ing.DataSource = Dt_Programas_Ingresos; //Liga los datos con el combo
                    Cmb_Programas_Ing.DataTextField = "CLAVE_NOMBRE";    //Asigna el campo de la tabla que se visualizara
                    Cmb_Programas_Ing.DataValueField = "PROYECTO_PROGRAMA_ID";    //Asigna el campo de la tabla que sera usado como valor
                    Cmb_Programas_Ing.DataBind();
                    Cmb_Programas_Ing.Items.Insert(0, new ListItem("< Seleccione >", ""));
                    Cmb_Programas_Ing.SelectedIndex = -1;
                }
            }
            Rs_Programas_Ingresos.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento.SelectedValue;
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Fuente_Financiamiento_Egr_SelectedIndexChanged
    /// DESCRIPCION : Al momento de seleccionar una FUENTE DE FINANCIAMIENTO SE SELECCIONA BUSCAN LAS DEPENDENCIAS 
    /// pueden manejar
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 20/ABRIL/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Unidad_Responsable_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Programas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Programas = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Partida = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        DataTable Dt_Partida = null;
        String Partida = "";
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cmb_Programa.Items.Clear();
            Rs_Consulta_Partida.P_Cuenta_Contable_ID = Cmb_Descripcion.SelectedValue;
            Dt_Partida = Rs_Consulta_Partida.Consulta_Datos_Cuentas_Contables();
            if (Dt_Partida.Rows.Count > 0)
            {
                Partida = Dt_Partida.Rows[0]["PARTIDA_ID"].ToString();
            }
            Rs_Consulta_Programas.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue;
            Rs_Consulta_Programas.P_Partida_ID = Partida;
            Rs_Consulta_Programas.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Egr.SelectedValue;
            Dt_Programas = Rs_Consulta_Programas.Consulta_Programas();  //Consulta los datos de la cuenta contable seleccionada
            if (Dt_Programas.Rows.Count > 0)
            {
                Cmb_Programa.DataSource = Dt_Programas; //Liga los datos con el combo
                Cmb_Programa.DataTextField = "CLAVE_NOMBRE";    //Asigna el campo de la tabla que se visualizara
                Cmb_Programa.DataValueField = "PROGRAMA_ID";    //Asigna el campo de la tabla que sera usado como valor
                Cmb_Programa.DataBind();
                Cmb_Programa.Items.Insert(0, new ListItem("< Seleccione >", ""));
                Cmb_Programa.SelectedIndex = -1;
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Txt_Cuenta_Contable_TextChanged
    /// DESCRIPCION : Consulta la Descripción más cercana de la cuenta que esta 
    ///               proporcionando el usuario
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Txt_Cuenta_Contable_TextChanged(object sender, EventArgs e)
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cat_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio();  //Variable de conexion con la capa de negocio
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_SAP_Partidas_Especificas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexion con la capa de negocio
        Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Presupuesto = new Cls_Ope_Con_Polizas_Negocio();    //Variable de conexión a la capa de negocios
        DataTable Dt_Descripcion_Cuenta_Contable;   //Almacena la descripcion de la cuenta contable proporcionada.
        Cls_Ope_Con_Polizas_Negocio Rs_Fuentes_financiamiento = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Fuentes_financiamiento = new DataTable();   //Almacena la partida especifica asociada
        DataTable Dt_Cuenta_Contable = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario

        try
        {
            if (Validar_Mascara_Cuenta_Contable(Txt_Cuenta_Contable.Text))
            {
                Rs_Consulta_Cat_Con_Cuentas_Contables.P_Descripcion = Txt_Cuenta_Contable.Text;
                Dt_Descripcion_Cuenta_Contable = Rs_Consulta_Cat_Con_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();  //Consulta las cuentas contables
                if (Dt_Descripcion_Cuenta_Contable.Rows.Count > 0)
                {
                    if (Dt_Descripcion_Cuenta_Contable.Rows[0]["AFECTABLE"].ToString().Trim() == "SI")
                    {
                        foreach (DataRow Registro in Dt_Descripcion_Cuenta_Contable.Rows)
                        {
                            Cmb_Descripcion.SelectedValue = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
                            Consulta_Cuenta_Contable();
                            if (String.IsNullOrEmpty(Txt_Concepto_Partida.Text))
                            {
                                Txt_Concepto_Partida.Text = Cmb_Descripcion.SelectedItem.Text.ToString().Substring(10);
                            }
                        }
                        ////if (Dt_Descripcion_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Tipo_Presupuestal].ToString() == "INGRESOS" && Dt_Descripcion_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString() != "")
                        ////{
                        ////    Div_Presupuestal.Style.Add("display", "block");
                        ////    Div_Ingresos.Style.Add("display", "block");
                        ////    Div_Egresos.Style.Add("display", "none");
                        ////    Rs_Fuentes_financiamiento.P_Partida_ID = Dt_Descripcion_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                        ////    Dt_Fuentes_financiamiento = Rs_Fuentes_financiamiento.Consulta_Fuente_Financiamiento();
                        ////    Cmb_Fuente_Financiamiento.DataSource = Dt_Fuentes_financiamiento; //Liga los datos con el combo
                        ////    Cmb_Fuente_Financiamiento.DataTextField = "CLAVE_NOMBRE";    //Asigna el campo de la tabla que se visualizara
                        ////    Cmb_Fuente_Financiamiento.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;    //Asigna el campo de la tabla que sera usado como valor
                        ////    Cmb_Fuente_Financiamiento.DataBind();
                        ////    Cmb_Fuente_Financiamiento.Items.Insert(0, new ListItem("< Seleccione >", ""));
                        ////    Cmb_Fuente_Financiamiento.SelectedIndex = -1;
                        ////    Txt_Partida_Presupuestal.Value = Dt_Descripcion_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                        ////}
                        ////else
                        ////{
                        ////    if (Dt_Descripcion_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Tipo_Presupuestal].ToString() == "EGRESOS" && Dt_Descripcion_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString() != "")
                        ////    {
                        ////        Div_Presupuestal.Style.Add("display", "block");
                        ////        Div_Ingresos.Style.Add("display", "none");
                        ////        Div_Egresos.Style.Add("display", "block");
                        ////        Rs_Fuentes_financiamiento.P_Partida_ID = Dt_Descripcion_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                        ////        Dt_Fuentes_financiamiento = Rs_Fuentes_financiamiento.Consulta_Fuente_Financiamiento_Egr();
                        ////        Cmb_Fuente_Financiamiento_Egr.DataSource = Dt_Fuentes_financiamiento; //Liga los datos con el combo
                        ////        Cmb_Fuente_Financiamiento_Egr.DataTextField = "CLAVE_NOMBRE";    //Asigna el campo de la tabla que se visualizara
                        ////        Cmb_Fuente_Financiamiento_Egr.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;    //Asigna el campo de la tabla que sera usado como valor
                        ////        Cmb_Fuente_Financiamiento_Egr.DataBind();
                        ////        Cmb_Fuente_Financiamiento_Egr.Items.Insert(0, new ListItem("< Seleccione >", ""));
                        ////        Cmb_Fuente_Financiamiento_Egr.SelectedIndex = -1;
                        ////        Txt_Partida_Presupuestal.Value = Dt_Descripcion_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Partida_ID].ToString();
                        ////    }
                        ////    else
                        ////    {
                        ////        Div_Presupuestal.Style.Add("display", "none");
                        ////        Div_Ingresos.Style.Add("display", "none");
                        ////        Div_Egresos.Style.Add("display", "none");
                        ////        Txt_Partida_Presupuestal.Value = "";
                        ////    }
                        ////}
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La cuenta que se ingreso no se puede afectar favor de ingresar otra <br>";
                    }

                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La cuenta que ingresaste no existe <br>";
                }
                Txt_Cuenta_Contable.Text = Aplicar_Mascara_Cuenta_Contable(Txt_Cuenta_Contable.Text);


            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
        // upd_panel
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String NO_POLIZA, String TIPO_POLIZA, String FECHA, String MES_ANO, SqlCommand P_Cmmd)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        DataTable Dt_Detalles = null;
        Cls_Ope_Con_Polizas_Negocio Rs_polizas_Detalles = new Cls_Ope_Con_Polizas_Negocio();
        String Mes = "";
        String Ano = "";
        DateTime Fecha_Poliza;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            Rs_polizas_Detalles.P_Tipo_Poliza_ID = TIPO_POLIZA;
            Rs_polizas_Detalles.P_No_Poliza = NO_POLIZA;
            Fecha_Poliza = Convert.ToDateTime(FECHA);
            Rs_polizas_Detalles.P_Mes_Ano = MES_ANO;
            //Rs_polizas_Detalles.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Fecha_Poliza);
            Rs_polizas_Detalles.P_Cmmd = Cmmd;
            Dt_Detalles = Rs_polizas_Detalles.Consulta_Poliza();
            if (Dt_Detalles.Rows.Count > 0)
            {
                Mes = Dt_Detalles.Rows[0][Ope_Con_Polizas.Campo_Mes_Ano].ToString().Substring(0, 2);
                Ano = Dt_Detalles.Rows[0][Ope_Con_Polizas.Campo_Mes_Ano].ToString().Substring(2, 2);
            }
            else
            {
                Fecha_Poliza = Convert.ToDateTime(FECHA);
                Mes = String.Format("{0:00}", (Fecha_Poliza.Month));
                Ano = Convert.ToString(Fecha_Poliza.Year).Substring(2, 2);
            }
            Cls_Ope_Con_Polizas_Negocio Poliza = new Cls_Ope_Con_Polizas_Negocio();
            Ds_Reporte = new DataSet();
            Poliza.P_No_Poliza = NO_POLIZA;
            Poliza.P_Tipo_Poliza_ID = TIPO_POLIZA;
            Poliza.P_Mes_Ano = Mes + Ano;
            Poliza.P_Cmmd = Cmmd;
            Dt_Pagos = Poliza.Consulta_Detalle_Poliza();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Datos_Poliza";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Poliza.rpt", "Poliza" + NO_POLIZA, ".pdf");
            }
        }
        //}
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }

    }
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Con_Cierre_Mensual_Negocio Rs_Cierre = new Cls_Ope_Con_Cierre_Mensual_Negocio();  //Variable de conexion con la capa de negocio
        DataTable Dt_Cierre = null;
        Cls_Ope_Con_Polizas_Negocio Rs_Prepoliza = new Cls_Ope_Con_Polizas_Negocio();
        Lbl_Mensaje_Error.Visible = false;
        String Resultado = "";
        Img_Error.Visible = false;
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                Limpia_Controles();           //Limpia los controles de la forma para poder introducir nuevos datos
                //Txt_Fecha_Poliza.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                Txt_Empleado_Creo.Text = Cls_Sessiones.Nombre_Empleado;
                //Cmb_Empleado_Creo.Items.Insert(0, new ListItem(Cls_Sessiones.Nombre_Empleado, ""));
                Consultar_Empleado_Autoriza();
            }
            else
            {
                //Valida los datos ingresados por el usuario.
                if (Validar_Datos_Poliza())
                {
                    if (Convert.ToDouble(Txt_Total_Debe.Text.ToString().Replace(",", "")) == Convert.ToDouble(Txt_Total_Haber.Text.ToString().Replace(",", "")))
                    {
                        if ((Convert.ToInt16(String.Format("{0:MM}", Convert.ToDateTime(Txt_Fecha_Poliza.Text))) < Convert.ToInt16(String.Format("{0:MM}", DateTime.Now)))
                            || (Convert.ToInt16(String.Format("{0:yy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text))) < Convert.ToInt16(String.Format("{0:yy}", DateTime.Now))))
                        {
                            Rs_Cierre.P_Anio = String.Format("{0:yyyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text));
                            Rs_Cierre.P_Mes = String.Format("{0:MMMM}", Convert.ToDateTime(Txt_Fecha_Poliza.Text)).ToUpper();
                            Dt_Cierre = Rs_Cierre.Consulta_Cierre_General();
                            if (Dt_Cierre.Rows.Count > 0)
                            {
                                if (Dt_Cierre.Rows[0]["Estatus"].ToString() == "CERRADO")
                                {
                                    Btn_Password.Visible = true;
                                    Btn_Password_Click(sender, e);
                                }
                            }
                        }
                        if (Convert.ToDateTime(Txt_Fecha_Poliza.Text) > DateTime.Now)
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + No puede capturar una poliza posterior a esta fecha Actual. <br>";
                        }
                        if (Convert.ToInt16(String.Format("{0:MM}", Convert.ToDateTime(Txt_Fecha_Poliza.Text))) == Convert.ToInt16(String.Format("{0:MM}", DateTime.Now))
                            || (Convert.ToInt16(String.Format("{0:yy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text))) <= Convert.ToInt16(String.Format("{0:yy}", DateTime.Now))))
                        {
                            SqlConnection Cn = new SqlConnection();
                            SqlCommand Cmmd = new SqlCommand();
                            SqlTransaction Trans = null;

                            // crear transaccion para crear el convenio 
                            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                            Cn.Open();
                            Trans = Cn.BeginTransaction();
                            Cmmd.Connection = Cn;
                            Cmmd.Transaction = Trans;
                            try
                            {
                                Resultado=Alta_Poliza(Cmmd); //Da de Alta el detalles de la póliza y sus características

                                if (Resultado == "SI")
                                {
                                    //if (!String.IsNullOrEmpty(Hd_Prepoliza.Value))
                                    //{
                                    //    DataTable Dt_Prepoliza = new DataTable();
                                    //    Rs_Prepoliza.P_Prepoliza_ID = Hd_Prepoliza.Value;
                                    //    Rs_Prepoliza.P_Cmmd = Cmmd;
                                    //   // Dt_Prepoliza = Rs_Prepoliza.Consulta_Prepolizas();
                                    //    if (Dt_Prepoliza.Rows.Count > 0)
                                    //    {
                                    //        Rs_Prepoliza.P_Prepoliza_ID = Hd_Prepoliza.Value;
                                    //        Rs_Prepoliza.P_Estatus = "CANCELADO";
                                    //        Rs_Prepoliza.P_Cmmd = Cmmd;
                                    //      //  Rs_Prepoliza.Modificar_Prepoliza();
                                    //    }
                                    //}
                                    Trans.Commit();
                                }
                                else
                                {
                                    Trans.Rollback();
                                    Lbl_Mensaje_Error.Text = "Validacion:";
                                    Lbl_Mensaje_Error.Text = "No tienes suficiente presupuesto en el momento inicial para realizar la poliza";
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Visible = true;
                                }
                            }
                            catch (SqlException Ex)
                            {
                                Trans.Rollback();
                                Lbl_Mensaje_Error.Text = "Error:";
                                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                            }
                            catch (Exception Ex)
                            {
                                Lbl_Mensaje_Error.Text = "Error:";
                                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                            }
                            finally
                            {
                                Cn.Close();
                            }
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El total Debe y el Total Haber deben ser iguales <br>";
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
    }
    ////protected void Btn_Guardar_Borrador_Click(object sender,  EventArgs e)
    ////{
    ////    try
    ////    {
    ////        Lbl_Mensaje_Error.Visible = false;
    ////        Img_Error.Visible = false;
    ////            //Valida los datos ingresados por el usuario.
    ////        if (Validar_Poliza_Borrador())
    ////        {
    ////            SqlConnection Cn = new SqlConnection();
    ////            SqlCommand Cmmd = new SqlCommand();
    ////            SqlTransaction Trans = null;

    ////            // crear transaccion para crear el convenio 
    ////            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
    ////            Cn.Open();
    ////            Trans = Cn.BeginTransaction();
    ////            Cmmd.Connection = Cn;
    ////            Cmmd.Transaction = Trans;
    ////            try
    ////            {
    ////            Alta_Prepoliza(Cmmd);
    ////            Trans.Commit();
    ////            }
    ////            catch (SqlException Ex)
    ////            {
    ////                Trans.Rollback();
    ////                Lbl_Mensaje_Error.Text = "Error:";
    ////                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
    ////                Img_Error.Visible = true;
    ////            }
    ////            catch (Exception Ex)
    ////            {
    ////                Lbl_Mensaje_Error.Text = "Error:";
    ////                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
    ////                Img_Error.Visible = true;
    ////            }
    ////            finally
    ////            {
    ////                Cn.Close();
    ////            }
    ////        }
    ////        else
    ////        {
    ////            Lbl_Mensaje_Error.Visible = true;
    ////            Img_Error.Visible = true;
    ////        }
    ////    }
    ////    catch (Exception ex)
    ////    {
    ////        Lbl_Mensaje_Error.Visible = true;
    ////        Img_Error.Visible = true;
    ////        Lbl_Mensaje_Error.Text = ex.Message.ToString();
    ////    }
    ////}
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                if (Txt_No_Poliza.Text != "")
                {
                    Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                    Txt_Fecha_Poliza.Enabled = false;
                    Btn_Fecha_Poliza.Enabled = false;
                    Cmb_Tipo_Poliza.Enabled = false;
                    Grid_Detalles_Poliza.Columns[12].Visible = true;
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione la Poliza que desea modificar <br>";
                }
            }
            else
            {
                //Valida los datos ingresados por el usuario.
                if (Validar_Datos_Poliza())
                {
                    if (Convert.ToDouble(Txt_Total_Debe.Text.ToString().Replace(",", "")) == Convert.ToDouble(Txt_Total_Haber.Text.ToString().Replace(",", "")))
                    {
                        SqlConnection Cn = new SqlConnection();
                        SqlCommand Cmmd = new SqlCommand();
                        SqlTransaction Trans = null;

                        // crear transaccion para crear el convenio 
                        Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                        Cn.Open();
                        Trans = Cn.BeginTransaction();
                        Cmmd.Connection = Cn;
                        Cmmd.Transaction = Trans;
                        try
                        {
                            Modificar_Poliza(Cmmd); //Modifica el detalles de la póliza y sus características
                            Trans.Commit();
                        }
                        catch (SqlException Ex)
                        {
                            Trans.Rollback();
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                            Img_Error.Visible = true;
                        }
                        catch (Exception Ex)
                        {
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                            Img_Error.Visible = true;
                        }
                        finally
                        {
                            Cn.Close();
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El total Debe y el Total Haber deben ser iguales <br>";
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (!String.IsNullOrEmpty(Txt_No_Poliza.Text))
            {
                Imprimir(Txt_No_Poliza.Text, Cmb_Tipo_Poliza.SelectedValue, String.Format("{0:MMMM/dd/yyyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text)), String.Format("{0:MMyy}", Convert.ToDateTime(Txt_Fecha_Poliza.Text)), null);
            }
            else
            {
                Lbl_Mensaje_Error.Text = "Es necesario Tener el número de POLIZA: <br>";
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Copiar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Habilitar_Controles("Carga");
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Cancelar_Poliza_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            //Si el usuario selecciono un Sindicato entonces lo elimina de la base de datos
            if (Txt_No_Poliza.Text != "")
            {
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;

                // crear transaccion para crear el convenio 
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Cn;
                Cmmd.Transaction = Trans;
                try
                {
                    Cancelar_Poliza(Cmmd); //Elimina la Poliza y sus Detalles que fue seleccionado por el usuario
                    Trans.Commit();
                }
                catch (SqlException Ex)
                {
                    Trans.Rollback();
                    Lbl_Mensaje_Error.Text = "Error:";
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                    Img_Error.Visible = true;
                }
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Text = "Error:";
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                    Img_Error.Visible = true;
                }
                    finally
                    {
                        Cn.Close();
                    }
            }
            //Si el usuario no selecciono alguna póliza manda un mensaje indicando que es necesario que seleccione algun para
            //poder eliminar
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Seleccione la Póliza que desea eliminar <br>";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.ToolTip == "Salir")
        {
            Session.Remove("Dt_Partidas_Poliza");
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else
        {
            Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            Limpia_Controles();//Limpia los controles de la forma
        }


    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Txt_Debe_Partida_TextChanged
    /// DESCRIPCION : Verfica si el contenido de la caja de texto cambio.
    /// CREO        : Sergio Manuel Gallardo
    /// FEHA_CREO  : 20/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Txt_Debe_Partida_TextChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los datos de la póliza que fueron proporcionados por el usuario
            String Espacios = "";
            //string Codigo_Programatico = "";
            int error = 0;
            Boolean Mensaje_Error = false;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Hdn_Cambio.Value = "";
            //Valida que todos los datos requeridos los haya proporcionado el usuario
            if ((!String.IsNullOrEmpty(Txt_Debe_Partida.Text) || !String.IsNullOrEmpty(Txt_Haber_Partida.Text)) && !String.IsNullOrEmpty(Txt_Concepto_Partida.Text))
            {
                if (Validar_Partida_Presupuestal())
                {
                    //Si no se a agregado ninguna partida entonces crea la Estructura necesaria para poder visualizar la información al usuario
                    if (Session["Dt_Partidas_Poliza"] == null)
                    {
                        //Agrega los campos que va a contener el DataTable
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add("MOMENTO_FINAL", typeof(System.String));
                    }
                    //Si se tiene agregada ya partidas a la póliza entonces agrega la estructura a la tabla para poder insertar
                    //los datos que el usuario introdujo a la póliza
                    else
                    {
                        Dt_Partidas_Polizas = (DataTable)Session["Dt_Partidas_Poliza"];
                        Session.Remove("Dt_Partidas_Poliza");
                    }
                    DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                    if (String.IsNullOrEmpty(Txt_Debe_Partida.Text))
                    {
                        Txt_Debe_Partida.Text = "0";
                    }
                    if (String.IsNullOrEmpty(Txt_Haber_Partida.Text))
                    {
                        Txt_Haber_Partida.Text = "0";
                    }
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = Dt_Partidas_Polizas.Rows.Count + 1;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cmb_Descripcion.SelectedValue;
                    row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Convert.ToString(Txt_Cuenta_Contable.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Partida.Text.ToString();
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Debe_Partida.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Txt_Haber_Partida.Text.ToString());
                    if (Div_Presupuestal.Style.Value == "display:block;")
                    {
                        if (Div_Ingresos.Style.Value == "display:block;")
                        {
                            row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Convert.ToString(Cmb_Fuente_Financiamiento.SelectedValue);
                            row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = "";
                            row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Convert.ToString(Cmb_Programas_Ing.SelectedValue);
                            row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = "";
                        }
                        else
                        {
                            if (Div_Egresos.Style.Value == "display:block;")
                            {
                                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Convert.ToString(Cmb_Fuente_Financiamiento_Egr.SelectedValue);
                                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Convert.ToString(Cmb_Unidad_Responsable.SelectedValue);
                                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Convert.ToString(Cmb_Programa.SelectedValue);
                                row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Convert.ToString(Txt_Partida_Presupuestal.Value);
                            }
                        }
                    }
                    else
                    {
                        row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = "";
                        row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = "";
                        row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = "";
                        row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = "";
                    }
                    row["MOMENTO_INICIAL"] = "";// Cmb_Momento_incial.SelectedItem.Text.Trim();
                    row["MOMENTO_FINAL"] = "";//Cmb_Momento_Final.SelectedItem.Text.Trim();

                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                    Session["Dt_Partidas_Poliza"] = Dt_Partidas_Polizas;//Agrega los valores del registro a la sesión

                    Grid_Detalles_Poliza.Columns[1].Visible = true;
                    Grid_Detalles_Poliza.Columns[6].Visible = true;
                    Grid_Detalles_Poliza.Columns[7].Visible = true;
                    Grid_Detalles_Poliza.Columns[8].Visible = true;
                    Grid_Detalles_Poliza.Columns[9].Visible = true;
                    Grid_Detalles_Poliza.Columns[10].Visible = true;
                    Grid_Detalles_Poliza.Columns[11].Visible = true;
                    Grid_Detalles_Poliza.Columns[13].Visible = true;
                    Grid_Detalles_Poliza.Columns[14].Visible = true;
                    Grid_Detalles_Poliza.DataSource = Dt_Partidas_Polizas; //Agrega los valores de todas las partidas que se tienen al grid
                    Grid_Detalles_Poliza.DataBind();
                    Grid_Detalles_Poliza.Columns[1].Visible = false;
                    Grid_Detalles_Poliza.Columns[6].Visible = false;
                    Grid_Detalles_Poliza.Columns[7].Visible = false;
                    Grid_Detalles_Poliza.Columns[8].Visible = false;
                    Grid_Detalles_Poliza.Columns[9].Visible = false;
                    Grid_Detalles_Poliza.Columns[10].Visible = false;
                    Grid_Detalles_Poliza.Columns[11].Visible = false;
                    Grid_Detalles_Poliza.Columns[13].Visible = false;
                    Grid_Detalles_Poliza.Columns[14].Visible = false;
                    Txt_Cuenta_Contable.Text = "";
                    Txt_Debe_Partida.Text = "";
                    Txt_Haber_Partida.Text = "";
                    Txt_Debe_Partida.Enabled = true;
                    Txt_Haber_Partida.Enabled = true;
                    Txt_Concepto_Partida.Text = "";
                    Cmb_Descripcion.SelectedIndex = -1;
                    Cmb_Unidad_Responsable.Items.Clear();
                    Cmb_Programa.Items.Clear();
                    Cmb_Fuente_Financiamiento.Items.Clear();
                    Cmb_Fuente_Financiamiento_Egr.Items.Clear();
                    Cmb_Programas_Ing.Items.Clear();
                    Txt_Partida_Presupuestal.Value = "";
                    Montos_Debe_Haber_Poliza(); //Obtiene el monto Total del Debe y Haber de la Poliza
                    Div_Ingresos.Style.Value = "display:none;";
                    Div_Egresos.Style.Value = "display:none;";
                }
                else
                {
                    Mensaje_Error = true;
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
            else
            {
                error = 1;
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                if (Cmb_Descripcion.SelectedIndex == -1)
                {
                    Lbl_Mensaje_Error.Text += Espacios + " + Numero de Cuenta <br>";
                }
                if (String.IsNullOrEmpty(Txt_Concepto_Partida.Text))
                {
                    Lbl_Mensaje_Error.Text += Espacios + " + Concepto de la Partida de la Cuenta Contable <br>";
                }
                if (String.IsNullOrEmpty(Txt_Debe_Partida.Text) && String.IsNullOrEmpty(Txt_Haber_Partida.Text))
                {
                    Lbl_Mensaje_Error.Text += Espacios + " + El Monto del Debe o Haber de la partida <br>";
                }
            }
            if (error == 1)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Txt_Debe_Partida_TextChanged " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Txt_Haber_Partida_TextChanged
    /// DESCRIPCION : Verfica si el contenido de la caja de texto cambio.
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 20/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Txt_Haber_Partida_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            //Si el control Txt_Haber cambio su contenido bloquea el control Txt_Debe
            if (Txt_Haber_Partida.Text != "")
            {
                Txt_Debe_Partida.ReadOnly = true;
            }
            else
                Txt_Debe_Partida.ReadOnly = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Txt_Haber_Partida_TextChanged " + ex.Message.ToString(), ex);
        }
    }
    protected void Txt_Empleado_Autorizo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Cls_Ope_Con_Polizas_Negocio Rs_Cat_Empleados = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexion con la capa de negocios.
            DataTable Dt_Empleados = null;  //Almacena los datos de los empleados encontrados.
            int Cont_Empleados = 1; //Contador para cambiar la posicion de insercion al combo.
            if (Txt_Empleado_Autorizo.Text == "")
            {
                Txt_Empleado_Autorizo.Text = "0";
            }

            if (Es_Numero(Txt_Empleado_Autorizo.Text.Trim()))
            {
                Rs_Cat_Empleados.P_Empleado_ID = String.Format("{0:000000}", Txt_Empleado_Autorizo.Text).ToString();
                Rs_Cat_Empleados.P_Nombre = "";
                Cmb_Nombre_Empleado.Items.Clear();
            }
            else
            {
                Rs_Cat_Empleados.P_Empleado_ID = "";
                Rs_Cat_Empleados.P_Nombre = Txt_Empleado_Autorizo.Text.ToString();
                Cmb_Nombre_Empleado.Items.Clear();
            }
            Dt_Empleados = Rs_Cat_Empleados.Consulta_Empleados_Especial();
            Cmb_Nombre_Empleado.Items.Insert(0, new ListItem("<- Seleccione ->", ""));

            foreach (DataRow Registro in Dt_Empleados.Rows) //Agrega los Items al combo.
            {
                Cmb_Nombre_Empleado.Items.Insert(Cont_Empleados, new ListItem(Registro["EMPLEADO"].ToString(), Registro[Cat_Empleados.Campo_Empleado_ID].ToString()));
                Cont_Empleados++;
            }
            if (Cont_Empleados > 2) //Si existe mas de un empleado da la opcion de seleccionar
            {
                Cmb_Nombre_Empleado.SelectedIndex = 0;
            }
            else    //De lo contrario selecciona el unico registro encontrado.
            {
                Cmb_Nombre_Empleado.SelectedIndex = 1;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    protected void Btn_Password_Click(object sender, EventArgs e)
    {
        Txt_No_Empleado_Popup.Text = "";
        Txt_Password_Popup.Text = "";
        Mpe_Autorizar_Password.Show();
    }
    #endregion
    #region (ModalPopup)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ventana_Click
    /// DESCRIPCION : Cierra la ventana de busqueda de Polizas.
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Cerrar_Ventana_Click(object sender, ImageClickEventArgs e)
    {
        Mpe_Busqueda_Polizas.Hide();    //Oculta el ModalPopUp
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ventana_Carga_Click
    /// DESCRIPCION : Cierra la ventana de busqueda de Polizas.
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Cerrar_Ventana_Carga_Click(object sender, ImageClickEventArgs e)
    {
        //Mpe_Carga_Masiva.Hide();
        Div_Carga_Masiva.Style.Value = "display:none";
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ventana_Borradores_Click
    /// DESCRIPCION : Cierra la ventana de busqueda de Polizas.
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 12/Septiembre/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Cerrar_Ventana_Borradores_Click(object sender, ImageClickEventArgs e)
    {
        //Cmb_Prepolizas.SelectedIndex = 0;
        //Div_Pre_Poliza.Style.Value = "display:none";
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Carga_Masiva_OnClick
    /// DESCRIPCION : Cierra la ventana de busqueda de Polizas.
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Carga_Masiva_OnClick(object sender, ImageClickEventArgs e)
    {
        Div_Carga_Masiva.Style.Value = "display:block";
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Borrador_Consulta_Click
    /// DESCRIPCION : Cierra la ventana de busqueda de Polizas.
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 12/Septiembre/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Borrador_Consulta_Click(object sender, ImageClickEventArgs e)
    {
        Div_Pre_Poliza.Style.Value = "display:block";
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ventana_Password_Click
    /// DESCRIPCION : Cierra la ventana de Autorizacion de Polizas
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 29/Octubre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Cerrar_Ventana_Password_Click(object sender, ImageClickEventArgs e)
    {
        Mpe_Autorizar_Password.Hide();
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Busqueda_Poliza_Popup_Click
    /// DESCRIPCION : Ejecuta la busqueda de Polizas
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Busqueda_Poliza_Popup_Click(object sender, EventArgs e)
    {
        try
        {
            Consulta_Poliza_Avanzada(); //Consulta las polizas de acuerdo a la informacion proporcionada.
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Busqueda_Cuenta_Popup_Click
    /// DESCRIPCION : Ejecuta la busqueda de Polizas
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 02/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Busqueda_Cuenta_Popup_Click(object sender, EventArgs e)
    {
        try
        {
            Consulta_Cuenta_Avanzada(); //Consulta las polizas de acuerdo a la informacion proporcionada.
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }

    protected void Btn_Carga_Masiva_Popup_Click(object sender, EventArgs e)
    {
        //Declaracion de variables
        string Ruta_Archivo = string.Empty; //variable para la ruta del archivo a leer

        try
        {
            //verificar si existe la variable de sesion
            if (HttpContext.Current.Session["Nombre_Archivo_Excel"] != null)
            {
                //Asignar la ruta del archivo
                Ruta_Archivo = HttpContext.Current.Server.MapPath("~") + "\\polizas\\" + HttpContext.Current.Session["Nombre_Archivo_Excel"].ToString().Trim();

                //Interpretar el archivo de Excel
                Interpretar_Excel(Ruta_Archivo);
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Favor de seleccionar un archivo de Excel.";
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }

    protected void AFU_Archivo_Excel_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        //****************************************
        //CUANDO LA CARGA DEL ARCHIVO TERMINA LO
        //GUARDA EN LA UBICACION PRESTABLECIDA
        //****************************************

        //Declaracion de variables
        string currentDatetime = string.Empty; //variable para la fecha actual
        string fileNameOnServer = string.Empty; //variable para el nombre del archivo en el servidor
        bool xls = false; //variable que indica si el archivo es un xls
        bool xlsx = false; //variable que indica si el archivo es un xlsx

        try
        {
            //verificar si se tiene un archivo
            if (AFU_Archivo_Excel.HasFile == true)
            {
                //obtener las caracteristicas del archivo
                currentDatetime = string.Format("{0:yyyy-MM-dd_HH.mm.sstt}", DateTime.Now);
                fileNameOnServer = System.IO.Path.GetFileName(AFU_Archivo_Excel.FileName).Replace(" ", "_");
                xls = fileNameOnServer.ToUpper().EndsWith(".XLS");
                xlsx = fileNameOnServer.ToUpper().EndsWith(".XLSX");

                //Verificar si el archivo es de excel
                if (xls == true || xlsx == true)
                {
                    //guardar el archivo
                    AFU_Archivo_Excel.SaveAs(HttpContext.Current.Server.MapPath("~") + "\\polizas\\" + fileNameOnServer);
                    AFU_Archivo_Excel.FileContent.Close();

                    //Colocar el nombre en una variable de sesion
                    HttpContext.Current.Session["Nombre_Archivo_Excel"] = fileNameOnServer;
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Favor de seleccionar un archivo de formato Excel.";
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "El archivo no se pudo leer, favor de seleccionarlo nuevamente.";                
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error (AFU_Archivo_Excel_UploadedComplete): " + ex.Message;
        }
    }
    protected void Btn_Autorizar_Poliza_Popup_Click(object sender, EventArgs e)
    {
        try
        {
            Lbl_Error_Password.Visible = false;
            Img_Error_Password.Visible = false;
            if (Txt_Password_Popup.Text != "" || Txt_No_Empleado_Popup.Text != "")
            {
                Cls_Cat_Empleados_Negocios Rs_Empleados = new Cls_Cat_Empleados_Negocios();
                DataTable Dt_Empleados = null;
                DataTable Dt_EmpleadosRol = null;
                Rs_Empleados.P_No_Empleado = String.Format("{0:000000}", Convert.ToInt16(Txt_No_Empleado_Popup.Text));
                Rs_Empleados.P_Password = Txt_Password_Popup.Text;
                Dt_Empleados = Rs_Empleados.Consulta_Usuario_Password();
                if (Dt_Empleados.Rows.Count == 0)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "El Numero de Empleado o la contraseña no coinciden.";
                }
                else
                {
                    Cls_Ope_Con_Polizas_Negocio Rs_Cat_Empleados = new Cls_Ope_Con_Polizas_Negocio();
                    Rs_Cat_Empleados.P_Empleado_ID = String.Format("{0:000000}", Convert.ToInt16(Txt_No_Empleado_Popup.Text));
                    Dt_EmpleadosRol = Rs_Cat_Empleados.Consulta_GrupoRol();
                    if (Dt_EmpleadosRol.Rows[0]["Grupo_Roles_ID"].ToString() == "00003")
                    {
                        SqlConnection Cn = new SqlConnection();
                        SqlCommand Cmmd = new SqlCommand();
                        SqlTransaction Trans = null;

                        // crear transaccion para crear el convenio 
                        Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                        Cn.Open();
                        Trans = Cn.BeginTransaction();
                        Cmmd.Connection = Cn;
                        Cmmd.Transaction = Trans;
                        try
                        {
                            Alta_Poliza(Cmmd);
                            Trans.Commit();
                        }
                        catch (SqlException Ex)
                        {
                            Trans.Rollback();
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                            Img_Error.Visible = true;
                        }
                        catch (Exception Ex)
                        {
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                            Img_Error.Visible = true;
                        }
                        finally
                        {
                            Cn.Close();
                        }
                    }
                    else
                    {
                        Mpe_Autorizar_Password.Hide();
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "El numero de empleado no pertenece a los administradores.";
                    }
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Debes proporcionar el password y la Clave de Empleado.";
            }

        }
        catch (Exception Ex)
        {
            Lbl_Error_Password.Visible = true;
            Img_Error_Password.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    #endregion
    #region Metodos Reportes
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }


    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";//"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte_Generar + Formato;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ven_Click
    /// DESCRIPCION : Cierra la ventana de busqueda cuentas.
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 02/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Cerrar_Ven_Click(object sender, ImageClickEventArgs e)
    {
        Grid_Polizas.DataSource = null;   // Se iguala el DataTable con el Grid
        Grid_Polizas.DataBind();    // Se ligan los datos.
        Txt_Busqueda_Cuenta.Text = "";
        Mpe_Busqueda_Cuenta.Hide();    //Oculta el ModalPopUp
    }
}