﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Con_Consulta_Tramite.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Consulta_Tramite" Title="Consulta Tramite de Solicitud" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:UpdateProgress ID="Uprg_Reporte" runat="server" 
        AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
        <ProgressTemplate>
            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
            <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <cc1:ToolkitScriptManager ID="ScriptManager_Parametros_Contabilidad" runat="server"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <div id="Div_Parametros_Contabilidad" >
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Consulta Solicitud  de Pagos</td>
                    </tr>
                    <caption>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                                <asp:Image ID="Img_Error" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />
                                &nbsp;
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" 
                                    CssClass="estilo_fuente_mensaje_error" Text="Mensaje" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr align="right" class="barra_busqueda">
                          <td align="left">
                                <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                            </td>
                            <td style="width:50%">
                            </td>
                        </tr>
                    </caption>
                </table>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="20%">*No.Solicitud de Pago</td>
                        <td width="296px">
                            <asp:TextBox ID="Txt_Lector_Codigo_Barras" runat="server" Width="25%" MaxLength="10" 
                                TabIndex="1" AutoPostBack="true" ontextchanged="Txt_Lector_Codigo_Barras_TextChanged"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Lector_Codigo_Barras" runat="server" 
                                TargetControlID="Txt_Lector_Codigo_Barras" FilterType="Numbers, Custom"  ValidChars="*" >
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="Div_Informacion" runat="server" style="display:none;"> 
                                    <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                                        <tr style="border:outset 1px Silver;">
                                            <td style="width:25%; font-size:small;">Solicitud Pago</td>
                                            <td style="width:25%;"> 
                                                <asp:TextBox ID="Txt_No_Solicitud_Pagos" runat="server" Width="80%" ReadOnly="true"  font-size="small"/>
                                            </td>
                                            <td  colspan="2" style="width:50%; font-size:small;">
                                                <asp:ImageButton ID="Btn_Imprimir" runat="server" ToolTip="Imprimir" CssClass="Img_Button" 
                                                TabIndex="2" ImageUrl="~/paginas/imagenes/gridview/grid_print.png"
                                                onclick="Btn_Imprimir_Click" />
                                            </td>
                                        </tr>
                                        <tr style="border:outset 1px Silver;">
                                            <td style="width:25%; font-size:small;">Estatus</td>
                                            <td style="width:25%;"> 
                                                <asp:TextBox ID="Txt_No_Pago_Det" runat="server" Width="80%" ReadOnly="true"  font-size="small"/>
                                            </td>
                                            <td style="width:20%; font-size:small;">&nbsp;No. Reserva</td>
                                            <td style="width:30%;"> 
                                                <asp:TextBox ID="Txt_No_Reserva_Det" runat="server" Width="98%" ReadOnly="true" style="text-align:right; font-size:small;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:Panel ID="Pnl_Datos_Reserva" runat="server" GroupingText="Datos De la Reserva" Width="100%" >
                                                 <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                                                        <tr>  
                                                            <td>
                                                                <div style="overflow:auto;width:99%;vertical-align:top; max-height:150px">
                                                                        <asp:GridView ID="Grid_Partidas" runat="server" Width="97%"
                                                                            AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None">
                                                                            <Columns>                                               
                                                                                   <asp:BoundField DataField="Dependencia" HeaderText="Dependencia" ItemStyle-Font-Size="XX-Small">
                                                                                     <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"  />
                                                                                     <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                                   </asp:BoundField>
                                                                                   <asp:BoundField DataField="Fuente_Financiamiento" HeaderText="Fuente" ItemStyle-Font-Size="XX-Small">
                                                                                     <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                                                     <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                                   </asp:BoundField>
                                                                                   <asp:BoundField DataField="PROYECTOS_PROGRAMAS" HeaderText="Programas" ItemStyle-Font-Size="XX-Small">
                                                                                   <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                                                   <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                                   </asp:BoundField>
                                                                                   <asp:BoundField DataField="AREA_FUNCIONAL" HeaderText="Area funcional" ItemStyle-Font-Size="XX-Small">
                                                                                   <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                                                   <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                                   </asp:BoundField>
                                                                                   <asp:BoundField DataField="PARTIDA" HeaderText="Partida" ItemStyle-Font-Size="XX-Small">
                                                                                   <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                                                   <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                                   </asp:BoundField>
                                                                                   <asp:BoundField DataField="CODIGO_PROGRAMATICO" HeaderText="Codigo_Programatico" ItemStyle-Font-Size="XX-Small">
                                                                                   <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                                                   <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                                   </asp:BoundField>
                                                                                   <asp:BoundField DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:c}" ItemStyle-Font-Size="XX-Small">
                                                                                   <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small" />
                                                                                   <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                                   </asp:BoundField>
                                                                            </Columns>                                                    
                                                                            <SelectedRowStyle CssClass="GridSelected" />
                                                                            <PagerStyle CssClass="GridHeader" />
                                                                            <HeaderStyle CssClass="tblHead" />
                                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                                        </asp:GridView> 
                                                                   </div>                            
                                                                </td>                                  
                                                        </tr>
                                                    </table>
                                                  </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:small;">Concepto Reserva</td>
                                            <td colspan="3"> 
                                                <asp:TextBox ID="Txt_Concepto_Reserva_Det" runat="server" Width="99%" ReadOnly="true" TextMode="MultiLine" Font-Size="Small"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:small;">Beneficiario</td>
                                            <td colspan="3"> 
                                                <asp:TextBox ID="Txt_Beneficiario_Det" runat="server" Width="99%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:small;">Fecha Solicitud Pago</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Fecha_Solicitud_Det" runat="server" Width="80%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                            <td style="font-size:small;"> &nbsp;Monto</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Monto_Solicitud_Det" runat="server" Width="98%" style="text-align:right;" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:small;">Fecha Autorizó Director</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Fecha_Autoriza_Director_Det" runat="server" Width="80%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                            <td colspan="2"> &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:small;">Fecha Recibio Doc.</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Fecha_Recibio_Documentacion_Fisica" runat="server" Width="80%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                            <td style="font-size:small;">Recibio Doc.</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Recibio_Documentacion_Fisica" runat="server" Width="99%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:small;">Fecha Autorizo Doc.</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Doc_Det" runat="server" Width="80%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                            <td style="font-size:small;">Autorizo Doc.</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Aut_Documentacion_Fisica" runat="server" Width="99%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:small;">Fecha Recibio Contabilidad</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Doc_Contabilidad" runat="server" Width="80%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                            <td style="font-size:small;">Recibio Contabilidad</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Recibio_Documentacion_Contabilidad" runat="server" Width="99%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:small;">Fecha Autorizo Contabilidad</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Fecha_Aut_Contabilidad" runat="server" Width="80%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                            <td style="font-size:small;">Autorizo Contabilidad</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Aut_Contabilidad" runat="server" Width="99%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:small;">Fecha Recibio Ejercido</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Doc_Ejercido" runat="server" Width="80%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                            <td style="font-size:small;">Recibio Ejercido</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Recibio_Documentacion_Ejercido" runat="server" Width="99%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:small;">Fecha Autorizo Ejercido</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Fecha_Aut_Ejercido" runat="server" Width="80%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                            <td style="font-size:small;">Autorizo Ejercido</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Aut_Ejercido" runat="server" Width="99%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                        </tr>
                                        <tr id="Tr_Poliza" runat="server">
                                            <td style="font-size:small;">No_Poliza</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_No_poliza_Det" runat="server" Width="80%" ReadOnly="true" Font-Size="Small"/>
                                            </td>
                                            <td colspan="2">
                                            </td>
                                        </tr>
                                      </table>
                                       <table width="100%" style="border:outset 1px Silver;" class="button_autorizar">
                                        <tr>
                                            <td style="width:100%">
                                                 <div style=" max-height:150px; overflow:auto; width:100%; vertical-align:top;">
                                                    <table style="width:97%;">
                                                        <tr>
                                                            <td style="width:100%;">
                                                                     <asp:GridView ID="Grid_Documentos" runat="server" Width="100%"
                                                                            AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                                                            EmptyDataText="No se encuentra ningun documento">
                                                                            <Columns>
                                                                                    <asp:TemplateField HeaderText="Link">
                                                                                        <ItemTemplate>
                                                                                            <asp:HyperLink ID="Hyp_Lnk_Ruta" ForeColor="Blue" runat="server" >Archivo</asp:HyperLink>
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle HorizontalAlign ="Left" width ="7%" />
                                                                                        <ItemStyle HorizontalAlign="Left" Width="7%" />
                                                                                    </asp:TemplateField> 
                                                                                    <asp:BoundField DataField="Partida" HeaderText="Partida" ItemStyle-Font-Size="X-Small">
                                                                                       <HeaderStyle HorizontalAlign="Center" Width="63%" />
                                                                                       <ItemStyle HorizontalAlign="Center" Width="63%" />
                                                                                   </asp:BoundField>   
                                                                                   <asp:BoundField DataField="MONTO_FACTURA" HeaderText="Monto" DataFormatString="{0:c}" ItemStyle-Font-Size="X-Small">
                                                                                       <HeaderStyle HorizontalAlign="Right" Width="30%" />
                                                                                       <ItemStyle HorizontalAlign="Right" Width="30%" />
                                                                                   </asp:BoundField>
                                                                                    <asp:BoundField DataField="Ruta" HeaderText="Ruta" ItemStyle-Font-Size="X-Small">
                                                                                       <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                                       <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                                   </asp:BoundField>
                                                                                   <asp:BoundField DataField="Archivo" HeaderText="Archivo" ItemStyle-Font-Size="X-Small">
                                                                                       <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                                       <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                                   </asp:BoundField>
                                                                            </Columns>                                                    
                                                                            <SelectedRowStyle CssClass="GridSelected" />
                                                                            <PagerStyle CssClass="GridHeader" />
                                                                            <HeaderStyle CssClass="tblHead" />
                                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                       </div>
                                            </td>
                                        </tr>
                                    </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

