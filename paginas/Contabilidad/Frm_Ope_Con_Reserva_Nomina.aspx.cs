﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sindicatos.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using AjaxControlToolkit;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Area_Funcional.Negocio;
using JAPAMI.Catalogo_Compras_Proyectos_Programas.Negocio;
using JAPAMI.SAP_Operacion_Departamento_Presupuesto.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Compromisos_Contabilidad.Negocios;
using JAPAMI.Generar_Reserva_Nomina.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Generar_Reservas.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Reserva_Nomina : System.Web.UI.Page
{
    private static String P_Dt_Reservas = "P_Dt_Reservas";
    private static String P_Dt_Programas = "P_Dt_Programas";
    private static String P_Dt_Partidas = "P_Dt_Partidas";
    private static String Importe = "Importe";

    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty))
            Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

        try
        {
            if (!IsPostBack)
            {
                Session["Activa"] = true;
                Session["P_Dt_Reservas"] = null;
                Session["P_Dt_Programas"] = null;
                Session["P_Dt_Partidas"] = null;
                Session["Importe"] = null;
                Div_Partidas_Reserva_Nueva.Visible = true;
                Div_Cabecera_Modificar.Visible = false;
                Div_Partidas_A_Modificar.Visible = false;
                ViewState["SortDirection"] = "ASC";
                Limpia_Controles();
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                Consultar_Tipos_Solicitud();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region(Metodos Generales)
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Reservas
    // DESCRIPCIÓN: Llena el grid principal de reservas
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 17/noviembre/2011 
    // MODIFICO:Leslie Gonzalez Vazquez
    // FECHA_MODIFICO:19/Enero/2012
    // CAUSA_MODIFICACIÓN: Hice la validacion del rango de fechas
    //********************************************************************************/
    public void Llenar_Grid_Reservas()
    {
        Cls_Ope_Con_Reserva_Nomina_Negocio Reserva_Negocio = new Cls_Ope_Con_Reserva_Nomina_Negocio();
        DataTable Dt_Partidas = new DataTable();
        try
        {
            Reserva_Negocio.P_Fecha_Inicial = String.Empty;
            Reserva_Negocio.P_Fecha_Final = String.Empty;

            if (!String.IsNullOrEmpty(Txt_No_Folio.Text.Trim()))
            {
                Reserva_Negocio.P_No_Reserva = Txt_No_Folio.Text.Trim();
            }
            else
            {
                Reserva_Negocio.P_No_Reserva = String.Empty;
            }

            Reserva_Negocio.P_Anio_Presupuesto = Cmb_Anio_Psp.SelectedItem.Value;
            Dt_Partidas = Reserva_Negocio.Consultar_Reservas_Detalladas();

            if (Dt_Partidas.Rows.Count > 0)
            {
                Cmb_Estatus.SelectedValue = Dt_Partidas.Rows[0][Ope_Psp_Reservas.Campo_Estatus].ToString();
            }
            Session["Dt_Partidas_Asignadas"] = Dt_Partidas;
            // Llenar_Grid_Partida_Asignada(false);

        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenado de la tabla de reservas Erro:[" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 13/Octubre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Cmb_Estatus.SelectedIndex = 0;
            Llenar_Combo_Programa();
            Llenar_Combo_Dependecias();
            Llenar_Combo_Anio();
            Validar_Reserva_Anio();
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Llenar_Combo_Tipo_Modificacion();

            if (Btn_Modificar.Visible)
            {
                Div_Partidas_Reserva_Nueva.Visible = false;
                Div_Cabecera_Modificar.Visible = true;
                Div_Partidas_A_Modificar.Visible = true;
                Llenar_Grid_Partidas_De_Reserva();
                Grid_Partidas_Reserva.Enabled = false;
                Btn_Agregar.Visible = false;
                Chk_Movimiento_Masivo.Checked = false;
                Pnl_Partidas_Asignadas.Visible = false;
            }
            else
            {
                Llenar_Grid();
                Btn_Agregar.Visible = true;
                Pnl_Partidas_Asignadas.Visible = true;
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 13/Octubre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Txt_No_Folio.Text = "";
            Txt_Conceptos.Text = "";
            //Cmb_Tipo_Solicitud_Pago.SelectedIndex = -1;
            Cmb_Proveedor_Solicitud_Pago.SelectedIndex = -1;
            //Cmb_Deudores.SelectedIndex = -1;
            //Cmb_anticipos.SelectedIndex = -1;
            //Txt_Deudor.Text = "";
            Cmb_Nombre_Empleado.SelectedIndex = -1; ;
            Txt_Nombre_Empleado.Text = "";
            Txt_Nombre_Proveedor_Solicitud_Pago.Text = "";
            //Txt_Gastos_Anticipos.Text = "";
            Grid_Partida_Asignada.DataSource = null;
            Grid_Partidas.DataBind();
            Txt_Busqueda.Text = String.Empty;

            //Grid_Partida_Saldo.DataSource = null;
            //Grid_Partida_Saldo.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Limpiar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                          si es una alta, modificacion
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";

                    if (Hf_Existe_Reserva.Value.Trim().Equals("SI"))
                    {
                        Btn_Nuevo.Visible = false;
                        Btn_Modificar.Visible = true;
                    }
                    else
                    {
                        Btn_Nuevo.Visible = true;
                        Btn_Modificar.Visible = false;
                    }

                    Btn_Nuevo.CausesValidation = false;
                    Btn_Modificar.CausesValidation = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Cmb_Estatus.Enabled = false;
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Cmb_Estatus.Enabled = false;
                    Txt_Conceptos.Enabled = true;
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    Cmb_Estatus.Enabled = true;
                    Txt_Conceptos.Enabled = false;
                    break;
            }
            Cmb_Anio_Psp.Enabled = !Habilitado;
            Cmb_Tipo_Modificacion.Enabled = Habilitado;
            Txt_No_Folio.Enabled = false;
            Cmb_Proveedor_Solicitud_Pago.Enabled = false;
            Cmb_Nombre_Empleado.Enabled = false;
            Txt_Nombre_Empleado.Enabled = false;
            Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = false;
            Btn_Buscar_Empleado.Enabled = false;
            Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = false;
            Btn_Buscar_Reserva.Enabled = !Habilitado;
            Tr_Empleado.Visible = false;
            Tr_Proveedor.Visible = false;
            Tr_Div_Partidas.Visible = false;
            Txt_Fuente_Ramo.Enabled = Habilitado;
            Cmb_Unidad_Responsable_Busqueda.Enabled = Habilitado;
            Cmb_Programa.Enabled = Habilitado;
            Cmb_Tipo_Reserva.Enabled = false;
            
            Cmb_Tipo_Solicitud_Pago.Enabled = false;
            Btn_Agregar.Enabled = Habilitado;
            Txt_Busqueda.Enabled = Habilitado;
            Btn_Busqueda_Partida.Enabled = Habilitado;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Consultar_Nombre_Mes
    // DESCRIPCIÓN: Consultara el nombre del mes correspondiente a la fecha actual
    // RETORNA: 
    // CREO: Hugo Enrique Ramírez Aguilera
    // FECHA_CREO: 17/Noviembre/2011 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public String Consultar_Nombre_Mes(String Mes)
    {
        try
        {
            switch (Mes)
            {
                case "01":
                    Mes = "_ENERO";
                    break;
                case "02":
                    Mes = "_FEBRERO";
                    break;
                case "03":
                    Mes = "_MARZO";
                    break;
                case "04":
                    Mes = "_ABRIL";
                    break;
                case "05":
                    Mes = "_MAYO";
                    break;
                case "06":
                    Mes = "_JUNIO";
                    break;
                case "07":
                    Mes = "_JULIO";
                    break;
                case "08":
                    Mes = "_AGOSTO";
                    break;
                case "09":
                    Mes = "_SEPTIEMBRE";
                    break;
                case "10":
                    Mes = "_OCTUBRE";
                    break;
                case "11":
                    Mes = "_NOVIEMBRE";
                    break;
                default:
                    Mes = "_DICIEMBRE";
                    break;
            }
        }
        catch (Exception Ex)
        {
            Mostrar_Informacion(Ex.ToString(), true);
        }
        return Mes;
    }

    private void Mostrar_Informacion(String txt, Boolean mostrar)
    {
        Lbl_Mensaje_Error.Style.Add("color", "#990000");
        Lbl_Mensaje_Error.Visible = mostrar;
        Img_Error.Visible = mostrar;
        Lbl_Mensaje_Error.Text = txt;
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Validaciones
    // DESCRIPCIÓN: Genera el String con la informacion que falta y ejecuta la 
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private Boolean Validaciones(bool Validar_Completo)
    {
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        String Tipo_Comprobacion = "";
        Boolean Bln_Bandera;
        Bln_Bandera = true;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "Es necesario: <br />";
        if (Grid_Partida_Asignada.Rows.Count <= 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Debes agregar una partida minimo para realizar una reserva<br />";
            Bln_Bandera = false;
        }
        //if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex == 0)
        //{
        //    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Seleccionar La Unidad Responsable <br />";
        //    Bln_Bandera = false;
        //}
        if (String.IsNullOrEmpty(Txt_Conceptos.Text))
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Introducir un Concepto. <br />";
            Bln_Bandera = false;
        }
        else
        {
            if (Txt_Conceptos.Text.Length >= 250)
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  El concepto solo permite un maximo de 250 caracteres. <br />";
                Bln_Bandera = false;
            }
        }
        if (Cmb_Tipo_Solicitud_Pago.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Seleccionar el Tipo de Solicitud <br />";
            Bln_Bandera = false;
        }
        //else
        //{
        //    Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Cmb_Tipo_Solicitud_Pago.SelectedValue;
        //    Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
        //    //Verificar si fue la opcion de los empleados
        //    if (Cmb_Tipo_Solicitud_Pago.SelectedItem.Value == "00001")
        //    {
        //        //Verificar si se ha seleccionado un empleado
        //        if (Cmb_Nombre_Empleado.SelectedIndex <= 0)
        //        {
        //            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Seleccionar un Empleado <br />";
        //            Bln_Bandera = false;
        //        }
        //    }
        //    else
        //    {
        //        if (Cmb_Proveedor_Solicitud_Pago.SelectedIndex <= 0)
        //        {
        //            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Seleccionar el Proveedor <br />";
        //            Bln_Bandera = false;
        //        }
        //    }
        //}

        if (!Bln_Bandera)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
        return Bln_Bandera;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Tipos_Solicitud
    /// DESCRIPCION : Llena el Cmb_Tipo_Solicitud_Pago con los tipos de solicitud de pago.
    /// PARAMETROS  : 
    /// CREO        : Yazmin Abigail Delgado Gómez
    /// FECHA_CREO  : 18-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consultar_Tipos_Solicitud()
    {
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Cat_Con_Tipo_Solicitud_Pagos = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
        DataTable Dt_Tipo_Solicitud; //Variable a obtener los datos de la consulta
        try
        {

            Dt_Tipo_Solicitud = Rs_Cat_Con_Tipo_Solicitud_Pagos.Consulta_Tipo_Solicitud_Pagos_Combo(); //Consulta los tipos de solicitud que fueron dados de alta en la base de datos
            Cmb_Tipo_Solicitud_Pago.Items.Clear();
            Cmb_Tipo_Solicitud_Pago.DataSource = Dt_Tipo_Solicitud;
            Cmb_Tipo_Solicitud_Pago.DataTextField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion;
            Cmb_Tipo_Solicitud_Pago.DataValueField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
            Cmb_Tipo_Solicitud_Pago.DataBind();
            Cmb_Tipo_Solicitud_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
            //PONEMOS POR EL MOMENTO EL TIPO DE SOLICITUD DE GASTOS POR PAGAR, EN LO QUE NOS DEFINEN EL TIPO Y
            //LO PONEMOS POR PARAMETROS
            Cmb_Tipo_Solicitud_Pago.SelectedValue = "00004";
        }
        catch (Exception ex)
        {
            throw new Exception("Consultar_Tipos_Solicitud " + ex.Message.ToString(), ex);
        }
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Datos
    ///DESCRIPCIÓN          : Metodo para validar los datos de las partidas
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 20/Enero/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private Boolean Validar_Datos()
    {
        Boolean Datos_Validos = true;
        Lbl_Mensaje_Error.Text = String.Empty;
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];

        try
        {
            if (Cmb_Tipo_Solicitud_Pago.SelectedIndex == 0)
            {
                Lbl_Mensaje_Error.Text += " Seleccionar el Tipo de Solicitud <br />";
                Datos_Validos = false;
            }
            else
            {
                //if (Cmb_Tipo_Solicitud_Pago.SelectedItem.ToString().Trim().Equals("Gastos por Pagar") || Cmb_Tipo_Solicitud_Pago.SelectedItem.ToString().Trim().Equals("Gastos por Comprobar Proveedores"))
                //{
                //    if (Cmb_Proveedor_Solicitud_Pago.SelectedIndex <= 0)
                //    {
                //        Lbl_Mensaje_Error.Text += " Seleccionar el Proveedor <br />";
                //        Datos_Validos = false;
                //    }
                //}
            }
            return Datos_Validos;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
        }
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Partida_Asignada
    ///DESCRIPCIÓN          : Metodo para crear el datatable de las partidas asignadas del grid anidado
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 20/Enero/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Crear_Dt_Partida_Asignada()
    {
        DataTable Dt_Partida_Asignada = new DataTable();
        DataTable Dt_Session = new DataTable();
        Dt_Partida_Asignada = (DataTable)Session["Dt_Partidas_Asignadas"];
        DataRow Fila;
        Double Total = 0.00;
        String Importe = String.Empty;

        try
        {

            if (Dt_Partida_Asignada is DataTable)
            {
                if (Dt_Partida_Asignada.Columns.Count <= 0)
                {
                    //creamos las columnas del datatable donde se guardaran los datos
                    Dt_Session.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("DEPENDENCIA", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("FUENTE_FINANCIAMIENTO", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("PROGRAMA", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("NO_RESERVA", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("BENEFICIARIO", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("EMPLEADO_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("PROVEEDOR_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("CONCEPTO", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("TOTAL", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("PARTIDA", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("SALDO", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("ANIO", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada = Dt_Session;
                }
            }

            for (int Contador_For = 0; Contador_For < Grid_Partida_Saldo.Rows.Count; Contador_For++)
            {
                System.Web.UI.WebControls.TextBox Txt_Importe = (System.Web.UI.WebControls.TextBox)Grid_Partida_Saldo.Rows[Contador_For].Cells[11].FindControl("Txt_Importe_Partida");

                if (Btn_Nuevo.Visible)
                {
                    Importe = Grid_Partida_Saldo.Rows[Contador_For].Cells[28].Text.ToString();
                }
                else
                {
                    Importe = Txt_Importe.Text;
                }

                if (Importe != "" && Convert.ToDouble(Importe) > 0)
                {
                    Total += Convert.ToDouble(Importe);

                    Fila = Dt_Partida_Asignada.NewRow();
                    Fila["DEPENDENCIA_ID"] = Grid_Partida_Saldo.Rows[Contador_For].Cells[0].Text.ToString();
                    Fila["FUENTE_FINANCIAMIENTO_ID"] = Convert.ToString(Grid_Partida_Saldo.Rows[Contador_For].Cells[1].Text.ToString());//1
                    Fila["PROGRAMA_ID"] = Grid_Partida_Saldo.Rows[Contador_For].Cells[3].Text.ToString();
                    Fila["DEPENDENCIA"] = HttpUtility.HtmlDecode(Grid_Partida_Saldo.Rows[Contador_For].Cells[6].Text.ToString());
                    Fila["FUENTE_FINANCIAMIENTO"] = Grid_Partida_Saldo.Rows[Contador_For].Cells[2].Text.ToString();
                    Fila["PROGRAMA"] = HttpUtility.HtmlDecode(Convert.ToString(Grid_Partida_Saldo.Rows[Contador_For].Cells[4].Text.ToString()));

                    Fila["NO_RESERVA"] = "";
                    if (Cmb_Tipo_Solicitud_Pago.SelectedItem.ToString().Trim().Equals("Gastos por Comprobar Empleados"))
                    {
                        Fila["PROVEEDOR_ID"] = "";
                    }
                    else if (Cmb_Tipo_Solicitud_Pago.SelectedItem.ToString().Trim().Equals("Gastos por Pagar") || Cmb_Tipo_Solicitud_Pago.SelectedItem.ToString().Trim().Equals("Gastos por Comprobar Proveedores"))
                    {
                        if (Btn_Nuevo.Visible)
                        {
                            Fila["BENEFICIARIO"] = String.Empty;
                            Fila["EMPLEADO_ID"] = "";
                            Fila["PROVEEDOR_ID"] = String.Empty;
                        }
                        else
                        {
                            Fila["BENEFICIARIO"] = Cmb_Proveedor_Solicitud_Pago.SelectedItem.Text.Trim();
                            Fila["EMPLEADO_ID"] = "";
                            Fila["PROVEEDOR_ID"] = Cmb_Proveedor_Solicitud_Pago.SelectedItem.Value.Trim();
                        }
                    }
                    Fila["CONCEPTO"] = HttpUtility.HtmlDecode(Txt_Conceptos.Text.Trim());
                    Fila["TOTAL"] = String.Format("{0:##,###,##0.00}", Total);
                    Fila["PARTIDA_ID"] = Grid_Partida_Saldo.Rows[Contador_For].Cells[5].Text.ToString();
                    Fila["PARTIDA"] = HttpUtility.HtmlDecode(Grid_Partida_Saldo.Rows[Contador_For].Cells[7].Text.ToString());
                    Fila["IMPORTE"] = String.Format("{0:##,###,##0.00}", Importe);
                    Fila["SALDO"] = String.Format("{0:##,###,##0.00}", Importe);
                    Fila["ANIO"] = DateTime.Now.ToString("yyyy");
                    Fila["CAPITULO_ID"] = Convert.ToString(Grid_Partida_Saldo.Rows[Contador_For].Cells[15].Text.ToString());//1
                    Dt_Partida_Asignada.Rows.Add(Fila);
                }

            }// fin del for

            Session["Dt_Partidas_Asignadas"] = Dt_Partida_Asignada;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear la tabla de partidas asignadas Error[" + ex.Message + "]");
        }
    }

    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Unidad_Responsable_Busqueda_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de unidad responsable
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 22/Diciembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Grid()
    {
        Cls_Ope_Con_Reserva_Nomina_Negocio Rs_Consulta_Dependencias = new Cls_Ope_Con_Reserva_Nomina_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex > 0)
            {
                Rs_Consulta_Dependencias.P_Dependencia_ID = Cmb_Unidad_Responsable_Busqueda.SelectedValue;
            }
            else
            {
                Rs_Consulta_Dependencias.P_Dependencia_ID = String.Empty;
            }

            if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value))
            {
                Rs_Consulta_Dependencias.P_Proyecto_Programa_ID = Cmb_Programa.SelectedValue;
            }
            else
            {
                Rs_Consulta_Dependencias.P_Proyecto_Programa_ID = String.Empty;
            }

            if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
            {
                Rs_Consulta_Dependencias.P_Comentarios = Txt_Busqueda.Text.Trim();
            }
            else
            {
                Rs_Consulta_Dependencias.P_Comentarios = String.Empty;
            }

            if (Btn_Nuevo.Visible)
            {
                Rs_Consulta_Dependencias.P_Tipo_Psp = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
            }
            else if (Btn_Modificar.Visible)
            {
                if (Cmb_Tipo_Modificacion.SelectedItem.Value.Trim().Equals("AMPLIACION"))
                {
                    Rs_Consulta_Dependencias.P_Tipo_Psp = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                }
                else
                {
                    Rs_Consulta_Dependencias.P_Tipo_Psp = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido;
                }
            }

            if (Cmb_Anio_Psp.SelectedIndex > 0)
            {
                Rs_Consulta_Dependencias.P_Anio_Presupuesto = Cmb_Anio_Psp.SelectedValue;
            }
            else
            {
                Rs_Consulta_Dependencias.P_Anio_Presupuesto = DateTime.Now.Year.ToString().Trim();
            }

            Rs_Consulta_Dependencias.P_Ramo_33 = "NO";
            Rs_Consulta_Dependencias.P_Filtro_Campo_Mes = Consultar_Nombre_Mes(String.Format("{0:0#}", (DateTime.Now.Month)));
            Dt_Consulta = Rs_Consulta_Dependencias.Consultar_Reservas_Unidad_Responsable();

            //  Para el grid
            Grid_Partida_Saldo.Columns[0].Visible = true;
            Grid_Partida_Saldo.Columns[1].Visible = true;
            Grid_Partida_Saldo.Columns[2].Visible = true;
            Grid_Partida_Saldo.Columns[3].Visible = true;
            Grid_Partida_Saldo.Columns[4].Visible = true;
            Grid_Partida_Saldo.Columns[5].Visible = true;
            Grid_Partida_Saldo.Columns[8].Visible = true;
            Grid_Partida_Saldo.Columns[9].Visible = true;
            Grid_Partida_Saldo.Columns[10].Visible = true;
            Grid_Partida_Saldo.Columns[11].Visible = true;
            Grid_Partida_Saldo.Columns[12].Visible = true;
            Grid_Partida_Saldo.Columns[13].Visible = true;
            Grid_Partida_Saldo.Columns[14].Visible = true;
            Grid_Partida_Saldo.Columns[15].Visible = true;
            Grid_Partida_Saldo.Columns[16].Visible = true;
            Grid_Partida_Saldo.Columns[17].Visible = true;
            Grid_Partida_Saldo.Columns[18].Visible = true;
            Grid_Partida_Saldo.Columns[19].Visible = true;
            Grid_Partida_Saldo.Columns[20].Visible = true;
            Grid_Partida_Saldo.Columns[21].Visible = true;
            Grid_Partida_Saldo.Columns[22].Visible = true;
            Grid_Partida_Saldo.Columns[23].Visible = true;
            Grid_Partida_Saldo.Columns[24].Visible = true;
            Grid_Partida_Saldo.Columns[25].Visible = true;
            Grid_Partida_Saldo.Columns[26].Visible = true;
            Grid_Partida_Saldo.Columns[27].Visible = true;
            Grid_Partida_Saldo.Columns[28].Visible = true;
            Grid_Partida_Saldo.DataSource = null;
            Grid_Partida_Saldo.DataBind();

            Grid_Partida_Saldo.DataSource = Dt_Consulta;
            Grid_Partida_Saldo.DataBind();
            Grid_Partida_Saldo.Columns[0].Visible = false;
            Grid_Partida_Saldo.Columns[1].Visible = false;
            Grid_Partida_Saldo.Columns[2].Visible = false;
            Grid_Partida_Saldo.Columns[3].Visible = false;
            Grid_Partida_Saldo.Columns[5].Visible = false;
            Grid_Partida_Saldo.Columns[15].Visible = false;

            if (Btn_Nuevo.Visible)
            {
                //Grid_Partida_Saldo.Columns[4].Visible = false;
                Grid_Partida_Saldo.Columns[8].Visible = false;
                Grid_Partida_Saldo.Columns[9].Visible = false;
                Grid_Partida_Saldo.Columns[10].Visible = false;
                Grid_Partida_Saldo.Columns[11].Visible = false;
                Grid_Partida_Saldo.Columns[12].Visible = false;
                Grid_Partida_Saldo.Columns[13].Visible = false;
                Grid_Partida_Saldo.Columns[14].Visible = false;
                Grid_Partida_Saldo.Columns[16].Visible = false;
                Grid_Partida_Saldo.Columns[17].Visible = false;
                Grid_Partida_Saldo.Columns[18].Visible = false;
                Grid_Partida_Saldo.Columns[19].Visible = false;
                Grid_Partida_Saldo.Columns[20].Visible = false;
                Grid_Partida_Saldo.Columns[21].Visible = false;
                Grid_Partida_Saldo.Columns[22].Visible = false;
                Grid_Partida_Saldo.Columns[23].Visible = false;
                Grid_Partida_Saldo.Columns[24].Visible = false;
                Grid_Partida_Saldo.Columns[25].Visible = false;
                Grid_Partida_Saldo.Columns[26].Visible = false;
                Grid_Partida_Saldo.Columns[27].Visible = false;
            }
            else if (Btn_Modificar.Visible)
            {
                Grid_Partida_Saldo.Columns[16].Visible = false;
                Grid_Partida_Saldo.Columns[17].Visible = false;
                Grid_Partida_Saldo.Columns[18].Visible = false;
                Grid_Partida_Saldo.Columns[19].Visible = false;
                Grid_Partida_Saldo.Columns[20].Visible = false;
                Grid_Partida_Saldo.Columns[21].Visible = false;
                Grid_Partida_Saldo.Columns[22].Visible = false;
                Grid_Partida_Saldo.Columns[23].Visible = false;
                Grid_Partida_Saldo.Columns[24].Visible = false;
                Grid_Partida_Saldo.Columns[25].Visible = false;
                Grid_Partida_Saldo.Columns[26].Visible = false;
                Grid_Partida_Saldo.Columns[27].Visible = false;
                Grid_Partida_Saldo.Columns[28].Visible = false;
            }
            else
            {
                Grid_Partida_Saldo.Columns[8].Visible = false;
                Grid_Partida_Saldo.Columns[9].Visible = false;
                Grid_Partida_Saldo.Columns[10].Visible = false;
                Grid_Partida_Saldo.Columns[11].Visible = false;
                Grid_Partida_Saldo.Columns[12].Visible = false;
                Grid_Partida_Saldo.Columns[13].Visible = false;
                Grid_Partida_Saldo.Columns[14].Visible = false;
                Grid_Partida_Saldo.Columns[16].Visible = false;
                Grid_Partida_Saldo.Columns[17].Visible = false;
                Grid_Partida_Saldo.Columns[18].Visible = false;
                Grid_Partida_Saldo.Columns[19].Visible = false;
                Grid_Partida_Saldo.Columns[20].Visible = false;
                Grid_Partida_Saldo.Columns[21].Visible = false;
                Grid_Partida_Saldo.Columns[22].Visible = false;
                Grid_Partida_Saldo.Columns[23].Visible = false;
                Grid_Partida_Saldo.Columns[24].Visible = false;
                Grid_Partida_Saldo.Columns[25].Visible = false;
                Grid_Partida_Saldo.Columns[26].Visible = false;
                Grid_Partida_Saldo.Columns[27].Visible = false;
                Grid_Partida_Saldo.Columns[28].Visible = false;
            }

            //Session["Dt_Partidas_Asignadas"] = Dt_Consulta;
            //Llenar_Grid_Partida_Asignada(false);
            Tr_Div_Partidas.Style.Add("display", "none");
        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el evento del combo de unidad responsable Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Partida_Asignada
    ///DESCRIPCIÓN          : Metodo para llenar el grid anidado de las partidas asignadas
    ///PROPIEDADES          1 Dt_Partidas: Datos de las partidas 
    ///                     2 Estado:-para daber cuando limpiar el grid
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 20/Enero/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Grid_Partida_Asignada(Boolean Estado)
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        DataTable Dt_Datos_Generales = new DataTable();
        DataRow Fila;
        Double Importe = 0.00;

        try
        {
            if (Estado == true)
            {
                Session["Dt_Partidas_Asignadas"] = null;
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
            }
            else
            {
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
            }

            Grid_Partida_Asignada.DataSource = new DataTable();
            Grid_Partidas.DataBind();

            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    if (Dt_Partidas_Asignadas.Columns["IMPORTE"].DataType.FullName.ToString() == "System.String")
                    {
                        Importe = Dt_Partidas_Asignadas.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<String>("IMPORTE").ToString())
                            ? "0" : x.Field<String>("IMPORTE").ToString()));
                    }
                    else if (Dt_Partidas_Asignadas.Columns["IMPORTE"].DataType.FullName.ToString() == "System.Decimal")
                    {
                        Importe = Dt_Partidas_Asignadas.AsEnumerable().Sum(x => Convert.ToDouble(x.Field<decimal>("IMPORTE")));
                    }

                    Dt_Datos_Generales = Agrupar_Dependencias(Dt_Partidas_Asignadas);

                    if (Btn_Nuevo.ToolTip.Equals("Nuevo"))
                    {
                        Grid_Partida_Asignada.Columns[1].Visible = true;
                        Grid_Partida_Asignada.Columns[2].Visible = true;
                        Grid_Partida_Asignada.Columns[3].Visible = true;
                        Grid_Partida_Asignada.Columns[4].Visible = true;
                        Grid_Partida_Asignada.Columns[5].Visible = true;
                        Grid_Partida_Asignada.Columns[6].Visible = true;
                        Grid_Partida_Asignada.Columns[7].Visible = true;
                        Grid_Partida_Asignada.Columns[9].Visible = true;
                        Grid_Partida_Asignada.Columns[12].Visible = true;
                        Grid_Partida_Asignada.Columns[11].Visible = true;
                        Grid_Partida_Asignada.DataSource = Dt_Datos_Generales;
                        Grid_Partida_Asignada.DataBind();
                        Grid_Partida_Asignada.Columns[1].Visible = false;
                        Grid_Partida_Asignada.Columns[2].Visible = false;
                        Grid_Partida_Asignada.Columns[3].Visible = false;
                        Grid_Partida_Asignada.Columns[4].Visible = false;
                        Grid_Partida_Asignada.Columns[5].Visible = false;
                        Grid_Partida_Asignada.Columns[6].Visible = false;
                        Grid_Partida_Asignada.Columns[9].Visible = false;
                        Grid_Partida_Asignada.Columns[12].Visible = false;
                        Grid_Partida_Asignada.Columns[11].Visible = false;
                    }
                    else
                    {
                        Grid_Partida_Asignada.Columns[1].Visible = true;
                        Grid_Partida_Asignada.Columns[2].Visible = true;
                        Grid_Partida_Asignada.Columns[3].Visible = true;
                        Grid_Partida_Asignada.Columns[4].Visible = true;
                        Grid_Partida_Asignada.Columns[5].Visible = true;
                        Grid_Partida_Asignada.Columns[6].Visible = true;
                        Grid_Partida_Asignada.Columns[7].Visible = true;
                        Grid_Partida_Asignada.Columns[9].Visible = true;
                        Grid_Partida_Asignada.Columns[12].Visible = true;
                        Grid_Partida_Asignada.Columns[11].Visible = true;
                        Grid_Partida_Asignada.DataSource = Dt_Datos_Generales;
                        Grid_Partida_Asignada.DataBind();
                        Grid_Partida_Asignada.Columns[1].Visible = false;
                        Grid_Partida_Asignada.Columns[2].Visible = false;
                        Grid_Partida_Asignada.Columns[3].Visible = false;
                        Grid_Partida_Asignada.Columns[4].Visible = false;
                        Grid_Partida_Asignada.Columns[5].Visible = false;
                        Grid_Partida_Asignada.Columns[6].Visible = false;
                        Grid_Partida_Asignada.Columns[7].Visible = false;
                        Grid_Partida_Asignada.Columns[9].Visible = false;
                        Grid_Partida_Asignada.Columns[12].Visible = false;
                        Grid_Partida_Asignada.Columns[11].Visible = false;
                    }
                }
                else
                {
                    Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                    Grid_Partida_Asignada.DataBind();
                }
            }
            else
            {
                Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
            }

            Txt_Total_Reserva.Text = String.Format("{0:n}", Importe);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de llenar la tabla de las partidas asignadas Error[" + ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Tipo_Modificacion_SelectedIndexChanged
    ///DESCRIPCIÓN          : Metodo para modificar el grid de las reservas para ampliar o reducir
    ///CREO                 : Sergio Manuel Gallardo Andrade 
    ///FECHA_CREO           : 27/Abril/2013 
    ///MODIFICO             : 
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Tipo_Modificacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Llenar_Grid_Partidas_De_Reserva();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de agregar un registro a la tabla. Error[" + ex.Message + "]");
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Partidas_De_Reserva
    // DESCRIPCIÓN: Llena el grid principal de reservas con las partidas de la reserva consultada
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 26/Abril/2013 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Grid_Partidas_De_Reserva()
    {
        Cls_Ope_Con_Reservas_Negocio Reserva_Negocio = new Cls_Ope_Con_Reservas_Negocio();
        Cls_Ope_Con_Reserva_Nomina_Negocio Reserva_Nomina_Negocios = new Cls_Ope_Con_Reserva_Nomina_Negocio();
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Partidas_Reserva = new DataTable();
        String No_Reserva = "";
        DataRow Fila_Reserva;
        Cls_Ope_Con_Reservas_Negocio Rs_Consulta_Dependencias = new Cls_Ope_Con_Reservas_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Reserva_Nomina_Negocios.P_Anio_Presupuesto = Convert.ToString(DateTime.Now.Year);
            No_Reserva = Reserva_Nomina_Negocios.Consultar_Reserva_Nomina_Anio();
            Reserva_Nomina_Negocios.P_No_Reserva = No_Reserva;

            if (!String.IsNullOrEmpty(Cmb_Unidad_Responsable_Busqueda.SelectedItem.Value))
            {
                Reserva_Nomina_Negocios.P_Dependencia_ID = "'" + Cmb_Unidad_Responsable_Busqueda.SelectedItem.Value + "'";
            }

            if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value))
            {
                Reserva_Nomina_Negocios.P_Proyecto_Programa_ID = Cmb_Programa.SelectedItem.Value;
            }

            if (!String.IsNullOrEmpty(Txt_Busqueda.Text))
            {
                Reserva_Nomina_Negocios.P_Comentarios = Txt_Busqueda.Text;
            }

            Dt_Partidas = Reserva_Nomina_Negocios.Consultar_Reservas_Nomina_Detalladas();
            if (Dt_Partidas.Rows.Count > 0)
            {
                Cmb_Estatus.SelectedValue = Dt_Partidas.Rows[0][Ope_Psp_Reservas.Campo_Estatus].ToString();
                if (Dt_Partidas_Reserva.Rows.Count <= 0)
                {
                    Dt_Partidas_Reserva.Columns.Add("NO_RESERVA", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("FTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("CLAVE_FINANCIAMIENTO", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PROYECTO_PROGRAMA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("CLAVE_PROGRAMA", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PROGRAMA", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("CLAVE_PARTIDA", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("DISPONIBLE", System.Type.GetType("System.Double"));
                    Dt_Partidas_Reserva.Columns.Add("COMPROMETIDO", System.Type.GetType("System.Double"));
                    Dt_Partidas_Reserva.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("CLAVE_DEPENDENCIA", System.Type.GetType("System.String"));
                }
                foreach (DataRow Registro_Reserva in Dt_Partidas.Rows)
                {
                    Fila_Reserva = Dt_Partidas_Reserva.NewRow();
                    //Se consulta el presupuesto
                    Dt_Consulta = new DataTable();
                    Rs_Consulta_Dependencias.P_Dependencia_ID = Registro_Reserva["DEPENDENCIA_ID"].ToString().Trim();
                    Rs_Consulta_Dependencias.P_Ramo_33 = "NO";
                    Rs_Consulta_Dependencias.P_Partida_ID = Registro_Reserva["PARTIDA_ID"].ToString().Trim();
                    Rs_Consulta_Dependencias.P_Fuente_Financiamiento = Registro_Reserva["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                    Rs_Consulta_Dependencias.P_Proyecto_Programa_ID = Registro_Reserva["PROGRAMA_ID"].ToString().Trim();
                    Rs_Consulta_Dependencias.P_Capitulo_ID = Registro_Reserva["CAPITULO_ID"].ToString().Trim();
                    Rs_Consulta_Dependencias.P_Filtro_Campo_Mes = Consultar_Nombre_Mes(String.Format("{0:0#}", (DateTime.Now.Month)));
                    Dt_Consulta = Rs_Consulta_Dependencias.Consultar_Reservas_Unidad_Responsable_y_Partida_de_Nomina();

                    Fila_Reserva["NO_RESERVA"] = Registro_Reserva["NO_RESERVA"].ToString().Trim();
                    Fila_Reserva["DEPENDENCIA_ID"] = Registro_Reserva["DEPENDENCIA_ID"].ToString().Trim();
                    Fila_Reserva["FTE_FINANCIAMIENTO_ID"] = Registro_Reserva["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                    Fila_Reserva["CLAVE_FINANCIAMIENTO"] = Dt_Consulta.Rows[0]["CLAVE_FINANCIAMIENTO"].ToString().Trim();
                    Fila_Reserva["PROYECTO_PROGRAMA_ID"] = Registro_Reserva["PROGRAMA_ID"].ToString().Trim();
                    Fila_Reserva["CLAVE_PROGRAMA"] = Dt_Consulta.Rows[0]["CLAVE_PROGRAMA"].ToString().Trim();
                    Fila_Reserva["PARTIDA_ID"] = Registro_Reserva["PARTIDA_ID"].ToString().Trim();
                    Fila_Reserva["PROGRAMA"] = Registro_Reserva["PROGRAMA"].ToString().Trim(); ;
                    Fila_Reserva["CLAVE_PARTIDA"] = Dt_Consulta.Rows[0]["CLAVE_PARTIDA"].ToString().Trim();
                    Fila_Reserva["DISPONIBLE"] = Convert.ToDouble(Dt_Consulta.Rows[0]["DISPONIBLE"].ToString().Trim());

                    if (Registro_Reserva["NO_RESERVA"].ToString().Trim() == No_Reserva)
                    {
                        //primero verificamos si lo que esta comprometido es mayor al saldo de la partida de la reserva si no lo es asi entonces se coloca el saldo de la partida que tiene la reserva 
                        if (Convert.ToDouble(Dt_Consulta.Rows[0]["COMPROMETIDO"].ToString()) > Convert.ToDouble(Registro_Reserva["SALDO"].ToString()))
                        {
                            Fila_Reserva["COMPROMETIDO"] = Convert.ToDouble(Registro_Reserva["SALDO"].ToString().Trim());
                        }
                        else
                        {
                            Fila_Reserva["COMPROMETIDO"] = Convert.ToDouble(Dt_Consulta.Rows[0]["COMPROMETIDO"].ToString().Trim());
                        }
                    }
                    else 
                    {
                        Fila_Reserva["COMPROMETIDO"] = Convert.ToDouble(Dt_Consulta.Rows[0]["COMPROMETIDO"].ToString().Trim());
                    }

                    Fila_Reserva["CAPITULO_ID"] = Registro_Reserva["CAPITULO_ID"].ToString().Trim();
                    Fila_Reserva["CLAVE_DEPENDENCIA"] = Dt_Consulta.Rows[0]["CLAVE_DEPENDENCIA"].ToString().Trim();
                    Dt_Partidas_Reserva.Rows.Add(Fila_Reserva);
                    Dt_Partidas_Reserva.AcceptChanges();
                }
                Grid_Partidas_Reserva.Columns[0].Visible = true;
                Grid_Partidas_Reserva.Columns[1].Visible = true;
                Grid_Partidas_Reserva.Columns[2].Visible = true;
                Grid_Partidas_Reserva.Columns[3].Visible = true;
                Grid_Partidas_Reserva.Columns[6].Visible = true;
                Grid_Partidas_Reserva.Columns[7].Visible = true;
                Grid_Partidas_Reserva.Columns[12].Visible = true;
                Grid_Partidas_Reserva.Columns[13].Visible = true;
                //if (Cmb_Tipo_Modificacion.SelectedItem.Value.Trim().Equals("AMPLIACION") == 1)
                //{
                //    Dt_Partidas_Reserva = (from Fila_ in Dt_Partidas_Reserva.AsEnumerable()
                //                           where Fila_.Field<string>("NO_RESERVA") == No_Reserva
                //                            select Fila_).AsDataView().ToTable();

                //    Dt_Partidas_Reserva.DefaultView.RowFilter = "Disponible>0";
                //}
                //else 
                if (Cmb_Tipo_Modificacion.SelectedItem.Value.Trim().Equals("REDUCCION"))
                {
                    Dt_Partidas_Reserva = (from Fila_ in Dt_Partidas_Reserva.AsEnumerable()
                                           where Fila_.Field<string>("NO_RESERVA") == No_Reserva
                                           select Fila_).AsDataView().ToTable();

                    Dt_Partidas_Reserva.DefaultView.RowFilter = "Comprometido>0";
                }
                else 
                {
                    //Dt_Partidas_Reserva = (from Fila_ in Dt_Partidas_Reserva.AsEnumerable()
                    //                       where Fila_.Field<string>("NO_RESERVA") != No_Reserva
                    //                       select Fila_).AsDataView().ToTable();

                    Dt_Partidas_Reserva.DefaultView.RowFilter = "Disponible>0";
                }
                Grid_Partidas_Reserva.DataSource = Dt_Partidas_Reserva.DefaultView;
                Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Reserva.DefaultView.ToTable();
                Grid_Partidas_Reserva.DataBind();
                Grid_Partidas_Reserva.Columns[0].Visible = false;
                Grid_Partidas_Reserva.Columns[1].Visible = false;
                Grid_Partidas_Reserva.Columns[2].Visible = false;
                Grid_Partidas_Reserva.Columns[3].Visible = false;
                Grid_Partidas_Reserva.Columns[6].Visible = false;
                Grid_Partidas_Reserva.Columns[7].Visible = false;
                Grid_Partidas_Reserva.Columns[12].Visible = false;
                Grid_Partidas_Reserva.Columns[13].Visible = false;

                Grid_Partidas_Reserva.SelectedIndex = -1;

                Cmb_Tipo_Solicitud_Pago.Enabled = false;
                Cmb_Tipo_Reserva.Enabled = false;
                //Cmb_Unidad_Responsable_Busqueda.Enabled = false;
                Txt_Conceptos.Enabled = false;
                //Btn_Busqueda_Partida.Enabled = false;
                Div_Cabecera_Modificar.Visible = true;
                Btn_Agregar.Enabled = false;
            }
            else
            {
                Mostrar_Informacion("La Reserva no existe o no se encuentra dentro del rango seleccionado \n Favor de Verificar la busqueda", true);
                Div_Cabecera_Modificar.Visible = false;
                Habilitar_Controles("Inicial");
                return;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenado de la tabla de reservas Erro:[" + Ex.Message + "]");
        }
    }

    #endregion

    #region (Metodos Reserva Nomina)
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Dependecias
    ///DESCRIPCIÓN          : Metodo para llenar el combo de las dependencias
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 27/Abril/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    public void Llenar_Combo_Dependecias()
    {
        Cls_Ope_Con_Reserva_Nomina_Negocio Negocio = new Cls_Ope_Con_Reserva_Nomina_Negocio(); //instancia de la clase de negocio
        DataTable Dt_Dependencias = new DataTable();

        try
        {
            Negocio.P_Dependencia_ID = String.Empty;
            Negocio.P_Tipo_Psp = String.Empty;

            if (String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value))
            {
                Negocio.P_Proyecto_Programa_ID = String.Empty;
            }
            else 
            {
                Negocio.P_Proyecto_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim();
            }

            Dt_Dependencias = Negocio.Consultar_Dependencias();

            Cmb_Unidad_Responsable_Busqueda.Items.Clear();
            Cmb_Unidad_Responsable_Busqueda.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Busqueda.DataTextField = "CLAVE_NOMBRE";
            Cmb_Unidad_Responsable_Busqueda.DataSource = Dt_Dependencias;
            Cmb_Unidad_Responsable_Busqueda.DataBind();
            Cmb_Unidad_Responsable_Busqueda.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            Cmb_Unidad_Responsable_Busqueda.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al llenar el combo de las Generencias. Error[" + Ex.Message + "]";
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anio
    ///DESCRIPCIÓN          : Metodo para llenar el combo de los años presupuestados
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 29/Abril/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    public void Llenar_Combo_Anio()
    {
        Cls_Ope_Con_Reserva_Nomina_Negocio Negocio = new Cls_Ope_Con_Reserva_Nomina_Negocio(); //instancia de la clase de negocio
        DataTable Dt_Anios = new DataTable();

        try
        {
            Dt_Anios = Negocio.Consultar_Anio_Presupuesto();

            Cmb_Anio_Psp.Items.Clear();
            Cmb_Anio_Psp.DataValueField = Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
            Cmb_Anio_Psp.DataTextField = Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
            Cmb_Anio_Psp.DataSource = Dt_Anios;
            Cmb_Anio_Psp.DataBind();
            Cmb_Anio_Psp.SelectedIndex = Cmb_Anio_Psp.Items.IndexOf(Cmb_Anio_Psp.Items.FindByValue(DateTime.Now.Year.ToString().Trim()));
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al llenar el combo de los años presupuestados. Error[" + Ex.Message + "]";
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programa
    ///DESCRIPCIÓN          : Metodo para llenar el combo de los programas
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Septiembre/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    public void Llenar_Combo_Programa()
    {
        Cls_Ope_Con_Reserva_Nomina_Negocio Negocio = new Cls_Ope_Con_Reserva_Nomina_Negocio();
        DataTable Dt_Programas = new DataTable();

        try
        {
            Cmb_Programa.Items.Clear(); //limpiamos el combo

            Negocio.P_Proyecto_Programa_ID = String.Empty;
            Dt_Programas = Negocio.Consultar_Programa();

            Cmb_Programa.DataSource = Dt_Programas;
            Cmb_Programa.DataTextField = "NOMBRE";
            Cmb_Programa.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
            Cmb_Programa.DataBind();
            Cmb_Programa.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al llenar el combo de los programas. Error[" + Ex.Message + "]";
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Tipo_Modificacion
    ///DESCRIPCIÓN          : Metodo para llenar el combo de con los tipos de modificaciones
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 29/Abril/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    public void Llenar_Combo_Tipo_Modificacion()
    {
        try
        {
            Cmb_Tipo_Modificacion.Items.Clear();
            Cmb_Tipo_Modificacion.Items.Insert(0, new ListItem("Economía Reserva", "REDUCCION"));
            Cmb_Tipo_Modificacion.Items.Insert(1, new ListItem("Reserva Adicional", "SUPLEMENTO"));
            Cmb_Tipo_Modificacion.SelectedIndex = -1;

            if (Btn_Modificar.Visible)
            {
                Lbl_Tipo_Modificacion.Visible = true;
                Cmb_Tipo_Modificacion.Visible = true;
            }
            else
            {
                Lbl_Tipo_Modificacion.Visible = false;
                Cmb_Tipo_Modificacion.Visible = false;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al llenar el combo de los tipos de modificación de reserva. Error[" + Ex.Message + "]";
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Reserva_Anio
    ///DESCRIPCIÓN          : Metodo para validar si existe reserva en el año seleccionado
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 29/Abril/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    public void Validar_Reserva_Anio()
    {
        Cls_Ope_Con_Reserva_Nomina_Negocio Negocio = new Cls_Ope_Con_Reserva_Nomina_Negocio(); //instancia de la clase de negocio
        DataTable Dt_Anios = new DataTable();
        String Anio = String.Empty;
        DataTable Dt = new DataTable();
        Hf_Existe_Reserva.Value = String.Empty;
        String Existe_Reserva = String.Empty;

        try
        {
            Dt_Anios = Negocio.Consultar_Reserva_Nomina();
            Existe_Reserva = "NO";

            //obtenemos el año seleccionado
            if (Cmb_Anio_Psp.SelectedIndex > -1)
            {
                Anio = Cmb_Anio_Psp.SelectedItem.Value.Trim();
            }

            Txt_No_Folio.Text = String.Empty;
            Txt_Conceptos.Text = String.Empty;

            if (Dt_Anios != null)
            {
                if (Dt_Anios.Rows.Count > 0)
                {
                    Dt = new DataTable();
                    Dt = (from Fila in Dt_Anios.AsEnumerable()
                          where Fila.Field<Decimal>("ANIO") == Convert.ToInt32(Anio)
                          select Fila).AsDataView().ToTable();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            Existe_Reserva = "SI";
                            Txt_No_Folio.Text = Dt.Rows[0][Ope_Nom_Reserva_Nomina.Campo_No_Reserva].ToString().Trim();
                            Txt_Conceptos.Text = Dt.Rows[0][Ope_Psp_Reservas.Campo_Concepto].ToString().Trim();
                        }
                    }
                }
            }

            Hf_Existe_Reserva.Value = Existe_Reserva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al validar si el año seleccionado cuenta con una reserva. Error[" + Ex.Message + "]";
        }
    }

    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Anio_Psp_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de año presupuestal
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 29/Abril/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Cmb_Anio_Psp_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        try
        {
            Validar_Reserva_Anio();
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Llenar_Combo_Tipo_Modificacion();
            if (Btn_Modificar.Visible)
            {
                Div_Partidas_Reserva_Nueva.Visible = false;
                Div_Cabecera_Modificar.Visible = true;
                Div_Partidas_A_Modificar.Visible = true;
                Llenar_Grid_Partidas_De_Reserva();
                Grid_Partidas_Reserva.Enabled = false;
                Btn_Agregar.Visible = false;
                Chk_Movimiento_Masivo.Checked = false;
                Pnl_Partidas_Asignadas.Visible = false;
            }
            else
            {
                Llenar_Grid();
                Btn_Agregar.Visible = true;
                Pnl_Partidas_Asignadas.Visible = true;
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el evento del combo de anio presupuestal Error[" + ex.Message + "]");
        }
    }
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Agrupar_Dependencias
    ///DESCRIPCIÓN          : metodo para agrupar las dependencias de la reserva de nomina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 29/Abril/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    public DataTable Agrupar_Dependencias(DataTable Dt_Partidas_Asignadas)
    {
        //DataTable Dt_Agrupados = new DataTable();
        DataTable Dt_Datos_Generales = new DataTable();
        DataRow Fila = null;

        try
        {
            //declaramos las columnas de la tabla
            Dt_Datos_Generales.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("DEPENDENCIA", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("FUENTE_FINANCIAMIENTO", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("PROGRAMA", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("NO_RESERVA", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("BENEFICIARIO", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("EMPLEADO_ID", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("PROVEEDOR_ID", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("CONCEPTO", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("TOTAL", System.Type.GetType("System.Double"));
            Dt_Datos_Generales.Columns.Add("SALDO", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("ANIO", System.Type.GetType("System.String"));
            Dt_Datos_Generales.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));

            if (Dt_Partidas_Asignadas is DataTable)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                    {
                        Fila = Dt_Datos_Generales.NewRow();
                        Fila["DEPENDENCIA_ID"] = Dr["DEPENDENCIA_ID"].ToString().Trim();
                        Fila["FUENTE_FINANCIAMIENTO_ID"] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                        Fila["PROGRAMA_ID"] = Dr["PROGRAMA_ID"].ToString().Trim();
                        Fila["DEPENDENCIA"] = Dr["DEPENDENCIA"].ToString().Trim();
                        Fila["FUENTE_FINANCIAMIENTO"] = Dr["FUENTE_FINANCIAMIENTO"].ToString().Trim();
                        Fila["PROGRAMA"] = Dr["PROGRAMA"].ToString().Trim();
                        Fila["NO_RESERVA"] = Dr["NO_RESERVA"].ToString().Trim(); ;
                        Fila["BENEFICIARIO"] = Dr["BENEFICIARIO"].ToString().Trim();
                        Fila["EMPLEADO_ID"] = String.Empty;
                        Fila["PROVEEDOR_ID"] = String.Empty;
                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                        Fila["TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"].ToString().Trim());
                        Fila["SALDO"] = String.Empty;
                        Fila["ANIO"] = Dr["ANIO"].ToString().Trim();
                        Fila["CAPITULO_ID"] = String.Empty;
                        Dt_Datos_Generales.Rows.Add(Fila);
                    }
                }
            }

            if (Dt_Datos_Generales != null)
            {
                if (Dt_Datos_Generales.Rows.Count > 0)
                {
                    //sumamos los montos para agruparlos por grupo dependencia
                    var Agrupacion = (from Fila_Dep in Dt_Datos_Generales.AsEnumerable()
                                      group Fila_Dep by new
                                      {
                                          Dep_ID = Fila_Dep.Field<String>("DEPENDENCIA_ID"),
                                          FF_ID = Fila_Dep.Field<String>("FUENTE_FINANCIAMIENTO_ID"),
                                          PP_ID = Fila_Dep.Field<String>("PROGRAMA_ID"),
                                          Dep = Fila_Dep.Field<String>("DEPENDENCIA"),
                                          Fte = Fila_Dep.Field<String>("FUENTE_FINANCIAMIENTO"),
                                          Pro = Fila_Dep.Field<String>("PROGRAMA"),
                                          No_Reserva = Fila_Dep.Field<String>("NO_RESERVA"),
                                          Beneficia = Fila_Dep.Field<String>("BENEFICIARIO"),
                                          Emp_ID = Fila_Dep.Field<String>("EMPLEADO_ID"),
                                          Pro_ID = Fila_Dep.Field<String>("PROVEEDOR_ID"),
                                          Concepto = Fila_Dep.Field<String>("CONCEPTO"),
                                          Saldo = Fila_Dep.Field<String>("SALDO"),
                                          Anio = Fila_Dep.Field<String>("ANIO"),
                                          Cap = Fila_Dep.Field<String>("CAPITULO_ID")
                                      }
                                          into psp
                                          orderby psp.Key.Dep
                                          select Dt_Datos_Generales.LoadDataRow(
                                            new object[]
                                                        {
                                                            psp.Key.Dep_ID,
                                                            psp.Key.FF_ID,
                                                            psp.Key.PP_ID,
                                                            psp.Key.Dep,
                                                            psp.Key.Fte,
                                                            psp.Key.Pro,
                                                            psp.Key.No_Reserva,
                                                            psp.Key.Beneficia,
                                                            psp.Key.Emp_ID,
                                                            psp.Key.Pro_ID,
                                                            psp.Key.Concepto,
                                                            psp.Sum(g => g.Field<Double>("TOTAL")),
                                                            psp.Key.Saldo,
                                                            psp.Key.Anio,
                                                            psp.Key.Cap,
                                                        }, LoadOption.PreserveChanges));

                    //obtenemos los datos agrupados en una tabla
                    Dt_Datos_Generales = Agrupacion.CopyToDataTable();
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(" Error al crear la tabla de gerencias agrupadas. Error[" + Ex.Message + "]");
        }

        return Dt_Datos_Generales;
    }
    #endregion

    #region (Metodos de Operacion [Alta - Modificar - Eliminar])
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Reserva
    /// DESCRIPCION : Modifica la reserva con los datos proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 22/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Modificar_Reserva()
    {
        Cls_Ope_Con_Reserva_Nomina_Negocio Rs_Reserva = new Cls_Ope_Con_Reserva_Nomina_Negocio(); //Variable de conexion hacia la capa de negocios
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Partidas_Reserva = new DataTable();
        DataTable Dt_Datos_Reserva = new DataTable();
        DataRow Fila;
        Double Saldo_Reserva;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;

        // crear transaccion para crear el convenio 
        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
        Cn.Open();
        Trans = Cn.BeginTransaction();
        Cmmd.Connection = Cn;
        Cmmd.Transaction = Trans;
        try
        {
            int Registro_Presupuestal;
            Rs_Reserva.P_No_Reserva = Txt_No_Folio.Text.Trim();
            Rs_Reserva.P_Estatus = "CANCELADA";
            Rs_Reserva.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
            Rs_Reserva.P_Cmmd = Cmmd;
            Dt_Partidas = (DataTable)Session["Dt_Partidas_Asignadas"];
            Saldo_Reserva = 0;
            if (Dt_Partidas_Reserva != null)
            {
                Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                foreach (DataRow Registro in Dt_Partidas.Rows)
                {
                    Fila = Dt_Partidas_Reserva.NewRow();
                    Fila["DEPENDENCIA_ID"] = Registro["DEPENDENCIA_ID"].ToString();
                    Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                    Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                    Fila["PARTIDA_ID"] = Registro["PARTIDA_ID"].ToString();
                    Fila["IMPORTE"] = Registro["COMPROMETIDO"].ToString();
                    Fila["ANIO"] = Cmb_Anio_Psp.SelectedItem.Value.Trim();
                    Saldo_Reserva = Saldo_Reserva + Convert.ToDouble(Registro["COMPROMETIDO"].ToString());
                    Dt_Partidas_Reserva.Rows.Add(Fila);
                    Dt_Partidas_Reserva.AcceptChanges();
                }
            }

            Saldo_Reserva = Rs_Reserva.Cancelar_Reserva_Nomina(Txt_No_Folio.Text.Trim(), Cmmd, Dt_Partidas_Reserva, Cmb_Anio_Psp.SelectedItem.Value);
            Rs_Reserva.Modificar_Reserva(); //Modifica el registro en base a los datos proporcionado
            //Registro_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DISPONIBLE", "COMPROMETIDO", Dt_Partidas_Reserva, Cmmd);
            if (Saldo_Reserva > 0)
            {
                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Txt_No_Folio.Text.Trim(), "DISPONIBLE", "COMPROMETIDO", Saldo_Reserva, "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                //para actualizar el tipo de reserva
                //Actualizar el registro del deudor
                Rs_Reserva.P_No_Reserva = Txt_No_Folio.Text.Trim();
                Rs_Reserva.P_Cmmd = Cmmd;
                Dt_Datos_Reserva = Rs_Reserva.Consultar_Reservas();
                Trans.Commit();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La Cancelacion de la reserva fue exitosa');", true);
                Limpia_Controles(); //Limpia los controles del modulo
                Inicializa_Controles();
                Session["Dt_Partidas_Asignadas"] = new DataTable();
                Llenar_Grid_Partida_Asignada(false);
            }
            else
            {
                Trans.Rollback();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La Cancelacion de la reserva no se pudo efectuar debido a  que no hay suficiencia presupuestal');", true);
            }

        }
        catch (SqlException Ex)
        {
            Trans.Rollback();
            Lbl_Mensaje_Error.Text = "Error:";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
            Img_Error.Visible = true;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = "Error:";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
            Img_Error.Visible = true;
        }
        finally
        {
            Cn.Close();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Saldo_Reserva
    /// DESCRIPCION : Modifica la reserva con los datos proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 29/Abril/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Modificar_Saldo_Reserva()
    {
        Cls_Ope_Con_Reservas_Negocio Rs_Reserva = new Cls_Ope_Con_Reservas_Negocio(); //Variable de conexion hacia la capa de negocios
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Partidas_Reserva = new DataTable();
        DataTable Dt_Datos_Reserva = new DataTable();
        Double Monto = 0;
        Double Total_Modificado = 0;
        DataRow Fila_Reserva;
        Int32 Indice_Grid_Saldos = 0;
        try
        {
            if (Dt_Partidas_Reserva.Rows.Count <= 0)
            {
                Dt_Partidas_Reserva.Columns.Add("NO_RESERVA", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.Double"));
                Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
            }
            if (Chk_Movimiento_Masivo.Checked)
            {
                //Se recorre el grid con los saldos modificados
                foreach (GridViewRow Renglon_Grid_Saldos in Grid_Partidas_Reserva.Rows)
                {
                    Indice_Grid_Saldos++;
                    Grid_Partidas_Reserva.SelectedIndex = Indice_Grid_Saldos;
                    Fila_Reserva = Dt_Partidas_Reserva.NewRow();
                    if (Cmb_Tipo_Modificacion.SelectedItem.Value.ToUpper() == "SUPLEMENTO")
                    {
                        Fila_Reserva["NO_RESERVA"] = Txt_No_Folio.Text.Trim();
                    }
                    else
                    {
                        Fila_Reserva["NO_RESERVA"] = Renglon_Grid_Saldos.Cells[13].Text.ToString();
                    }
                    Fila_Reserva["DEPENDENCIA_ID"] = Renglon_Grid_Saldos.Cells[0].Text.ToString().Trim();
                    Fila_Reserva["FUENTE_FINANCIAMIENTO_ID"] = Renglon_Grid_Saldos.Cells[1].Text.ToString().Trim();
                    Fila_Reserva["PROGRAMA_ID"] = Renglon_Grid_Saldos.Cells[3].Text.ToString().Trim();
                    Fila_Reserva["PARTIDA_ID"] = Renglon_Grid_Saldos.Cells[6].Text.ToString().Trim();
                    Fila_Reserva["CAPITULO_ID"] = Renglon_Grid_Saldos.Cells[12].Text.ToString().Trim();
                    if (Cmb_Tipo_Modificacion.SelectedItem.Value == "SUPLEMENTO")
                    {
                        Fila_Reserva["IMPORTE"] = Convert.ToDouble(Renglon_Grid_Saldos.Cells[9].Text.ToString().Trim());
                        Total_Modificado = Total_Modificado + Convert.ToDouble(Renglon_Grid_Saldos.Cells[9].Text.ToString().Trim());
                    }
                    else
                    {
                        Fila_Reserva["IMPORTE"] = Renglon_Grid_Saldos.Cells[10].Text.ToString().Trim();
                        Total_Modificado = Total_Modificado + Convert.ToDouble(Renglon_Grid_Saldos.Cells[10].Text.ToString().Trim());
                    }
                    Fila_Reserva["ANIO"] = DateTime.Now.Year;
                    Dt_Partidas_Reserva.Rows.Add(Fila_Reserva);
                    Dt_Partidas_Reserva.AcceptChanges();
                }
            }
            else
            {
                //Se recorre el grid con los saldos modificados
                foreach (GridViewRow Renglon_Grid_Saldos in Grid_Partidas_Reserva.Rows)
                {
                    Indice_Grid_Saldos++;
                    Grid_Partidas_Reserva.SelectedIndex = Indice_Grid_Saldos;
                    if (!String.IsNullOrEmpty(((System.Web.UI.WebControls.TextBox)Renglon_Grid_Saldos.FindControl("Txt_Importe_Comprometido")).Text.ToString()))
                    {
                        Monto = Convert.ToDouble(((System.Web.UI.WebControls.TextBox)Renglon_Grid_Saldos.FindControl("Txt_Importe_Comprometido")).Text.Replace(",", ""));
                        if (Monto > 0)
                        {
                            Fila_Reserva = Dt_Partidas_Reserva.NewRow();
                            if (Cmb_Tipo_Modificacion.SelectedItem.Value.ToUpper() == "SUPLEMENTO")
                            {
                                Fila_Reserva["NO_RESERVA"] = Txt_No_Folio.Text.Trim();
                            }
                            else 
                            {
                                Fila_Reserva["NO_RESERVA"] = Renglon_Grid_Saldos.Cells[13].Text.ToString();
                            }

                            Fila_Reserva["DEPENDENCIA_ID"] = Renglon_Grid_Saldos.Cells[0].Text.ToString().Trim();
                            Fila_Reserva["FUENTE_FINANCIAMIENTO_ID"] = Renglon_Grid_Saldos.Cells[1].Text.ToString().Trim();
                            Fila_Reserva["PROGRAMA_ID"] = Renglon_Grid_Saldos.Cells[3].Text.ToString().Trim();
                            Fila_Reserva["PARTIDA_ID"] = Renglon_Grid_Saldos.Cells[6].Text.ToString().Trim();
                            Fila_Reserva["CAPITULO_ID"] = Renglon_Grid_Saldos.Cells[12].Text.ToString().Trim();
                            Fila_Reserva["IMPORTE"] = Monto;
                            Total_Modificado = Total_Modificado + Monto;
                            Fila_Reserva["ANIO"] = DateTime.Now.Year;
                            Dt_Partidas_Reserva.Rows.Add(Fila_Reserva);
                            Dt_Partidas_Reserva.AcceptChanges();
                        }
                    }
                }
            }
            Modificar_Reserva(Dt_Partidas_Reserva, Cmb_Tipo_Modificacion.SelectedItem.Value.ToUpper().Trim(), Total_Modificado, null);
        }
        catch (SqlException Ex)
        {
            Lbl_Mensaje_Error.Text = "Error:";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
            Img_Error.Visible = true;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = "Error:";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
            Img_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Reserva
    /// DESCRIPCION : Modifica la reserva con los datos proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 22/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Modificar_Reserva(DataTable Dt_Partidas_Reserva, String Tipo_Modificacion, Double Importe_Total_Modificado, SqlCommand Command)
    {
        Cls_Ope_Con_Reserva_Nomina_Negocio Rs_Reserva = new Cls_Ope_Con_Reserva_Nomina_Negocio(); //Variable de conexion hacia la capa de negocios
        int Registro_Presupuestal;
        SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
        SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
        SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
        if (Command == null)
        {
            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open(); //Abre la conexión a la base de datos            
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
            Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
            Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
        }
        else
        {
            Comando_SQL = Command;
        }
        try
        {
            if (Tipo_Modificacion == "SUPLEMENTO")
            {
                Registro_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "DISPONIBLE", Dt_Partidas_Reserva, Comando_SQL);
            }
            else
            {
                Registro_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DISPONIBLE", "COMPROMETIDO", Dt_Partidas_Reserva, Comando_SQL);
            }
            if (Registro_Presupuestal > 0)
            {
                if (Tipo_Modificacion == "SUPLEMENTO")
                {
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Dt_Partidas_Reserva.Rows[0]["NO_RESERVA"].ToString(), "COMPROMETIDO", "DISPONIBLE", Importe_Total_Modificado, "", "", "", "", Comando_SQL); //Agrega el historial del movimiento de la partida presupuestal
                }
                else if (Tipo_Modificacion == "REDUCCION")
                {
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Dt_Partidas_Reserva.Rows[0]["NO_RESERVA"].ToString(), "DISPONIBLE", "COMPROMETIDO", Importe_Total_Modificado, "", "", "", "", Comando_SQL); //Agrega el historial del movimiento de la partida presupuestal
                }

                //Afectar el saldo de la reserva
                Rs_Reserva.P_No_Reserva = Dt_Partidas_Reserva.Rows[0]["NO_RESERVA"].ToString();
                Rs_Reserva.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                Rs_Reserva.P_Dt_Detelles = Dt_Partidas_Reserva;
                Rs_Reserva.P_Tipo_Modificacion = Tipo_Modificacion;
                Rs_Reserva.P_Importe = Convert.ToString(Importe_Total_Modificado);
                Rs_Reserva.P_Cmmd = Comando_SQL;
                Rs_Reserva.Modificar_Saldos_Reserva(); //Modifica el registro en base a los datos proporcionado
                if (Command == null)
                {
                    Transaccion_SQL.Commit();
                }
            }
            else
            {
                if (Command == null)
                {
                    Transaccion_SQL.Rollback();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La Modificación de la reserva no se pudo efectuar debido a  que no hay suficiencia presupuestal');", true);
            }

        }
        catch (SqlException Ex)
        {
            if (Command == null)
            {
                Transaccion_SQL.Rollback();
            }
            Lbl_Mensaje_Error.Text = "Error:";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
            Img_Error.Visible = true;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = "Error:";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
            Img_Error.Visible = true;
        }
        finally
        {
            if (Command == null)
            {
                Conexion_Base.Close();
            }
        }
    }

    #endregion

    #region (Eventos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
    ///DESCRIPCIÓN          : Metodo que permite dar de alta una reserva
    ///CREO                 : 
    ///FECHA_CREO           : 17/Noviembre/2011 
    ///MODIFICO             : Leslie Gonzalez Vazquez
    ///FECHA_MODIFICO       : 20/enero/2012
    ///CAUSA_MODIFICACIÓN   : Para agregar los detalles de las reservas
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        int No_reserva;
        Boolean Bandera = true;
        int Registro_Afectado = 0;
        int Registro_Movimiento = 0;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        String Beneficiario = String.Empty;
        String Proveedor_Id = String.Empty;
        String Empleado_Id = String.Empty;
        DataTable Dt_Partidas = new DataTable();
        Cls_Ope_Con_Reserva_Nomina_Negocio Rs_Actualizar_Tipo_Reserva = new Cls_Ope_Con_Reserva_Nomina_Negocio();
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        String Tipo_Comprobacion = "";
        Btn_Agregar.Visible = false;
        Pnl_Partidas_Asignadas.Visible = true;
        double Total = 0.00;
        try
        {
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                Limpia_Controles();           //Limpia los controles de la forma para poder introducir nuevos datos
                Cmb_Estatus.SelectedIndex = 0;
                Session["Dt_Partidas_Asignadas"] = new DataTable();
                Crear_Dt_Partida_Asignada();
                Llenar_Grid_Partida_Asignada(false);
                Llenar_Grid();
            }
            else
            {
                //Valida los datos ingresados por el usuario.
                if (Validaciones(true))
                {
                    Dt_Partidas = (DataTable)Session["Dt_Partidas_Asignadas"];
                    if (Dt_Partidas != null)
                    {
                        if (Dt_Partidas.Rows.Count > 0)
                        {
                            if (Dt_Partidas.Columns["IMPORTE"].DataType.FullName.ToString() == "System.String")
                            {
                                Total = Dt_Partidas.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<String>("IMPORTE").ToString())
                                    ? "0" : x.Field<String>("IMPORTE").ToString()));
                            }
                            else if (Dt_Partidas.Columns["IMPORTE"].DataType.FullName.ToString() == "System.Decimal")
                            {
                                Total = Dt_Partidas.AsEnumerable().Sum(x => Convert.ToDouble(x.Field<decimal>("IMPORTE")));
                            }
                        }
                    }
                    Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Cmb_Tipo_Solicitud_Pago.SelectedValue;

                    if (Bandera)
                    {
                        SqlConnection Cn = new SqlConnection();
                        SqlCommand Cmmd = new SqlCommand();
                        SqlTransaction Trans = null;

                        // crear transaccion para crear el convenio 
                        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                        Cn.Open();
                        Trans = Cn.BeginTransaction();
                        Cmmd.Connection = Cn;
                        Cmmd.Transaction = Trans;
                        try
                        {

                            DataTable Dt_Partidas_Saldo = (DataTable)Session["Dt_Partidas_Asignadas"];
                            No_reserva = Cls_Ope_Psp_Manejo_Presupuesto.Crear_Reserva(String.Empty, "GENERADA", Beneficiario, "", "", Txt_Conceptos.Text, Convert.ToString(DateTime.Now.Year), Total, Proveedor_Id, Empleado_Id, Cmb_Tipo_Solicitud_Pago.SelectedValue.Trim(), Dt_Partidas, Txt_Fuente_Ramo.Text, Cmmd);

                            if (!String.IsNullOrEmpty(No_reserva.ToString().Trim()))
                            {
                                Rs_Actualizar_Tipo_Reserva.P_Cmmd = Cmmd;
                                Rs_Actualizar_Tipo_Reserva.P_Anio_Presupuesto = Cmb_Anio_Psp.SelectedItem.Value.Trim();
                                Rs_Actualizar_Tipo_Reserva.P_No_Reserva = No_reserva.ToString().Trim();
                                Rs_Actualizar_Tipo_Reserva.P_Dt_Detelles = Dt_Partidas;
                                Registro_Afectado = Rs_Actualizar_Tipo_Reserva.Afectar_Presupuesto_Nomina();
                            }

                            if (Registro_Afectado > 0)
                            {
                                Registro_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(No_reserva), "COMPROMETIDO", "DISPONIBLE", Total, "", "", "", "", Cmmd);

                                //para actualizar el tipo de reserva
                                Rs_Actualizar_Tipo_Reserva.P_No_Reserva = "" + No_reserva;
                                Rs_Actualizar_Tipo_Reserva.P_Tipo_Reserva = Cmb_Tipo_Reserva.SelectedValue;
                                Rs_Actualizar_Tipo_Reserva.Modificar_Tipo_Reserva();

                                Trans.Commit();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Reservas", "alert('El alta de la reserva No." + No_reserva + "  fue exitosa');", true);

                                Inicializa_Controles();
                                Limpia_Controles();
                            }
                            else
                            {
                                Trans.Rollback();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Reservas", "alert('El alta de la reserva No se pudo crear debido a que no hay suficiencia presupuestal');", true);
                            }
                        }
                        catch (SqlException Ex)
                        {
                            Trans.Rollback();
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                            Img_Error.Visible = true;
                        }
                        catch (Exception Ex)
                        {
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                            Img_Error.Visible = true;
                        }
                        finally
                        {
                            Cn.Close();
                        }
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Metodo que permite modificar la reserva
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/Noviembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Dt_Partidas = new DataTable();
        Cls_Ope_Con_Reserva_Nomina_Negocio Reservas_Datos = new Cls_Ope_Con_Reserva_Nomina_Negocio();
        DataTable Dt_Detalles = new DataTable();
        String No_Reserva = "";
        Pnl_Partidas_Asignadas.Visible = false;
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            Btn_Agregar.Visible = false;
            Llenar_Combo_Dependecias();
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                Grid_Partidas_Reserva.Enabled = true;
            }
            else
            {
                if (Cmb_Estatus.SelectedItem.Text == "CANCELADA")
                {
                    //if (Dt_Partidas.Rows.Count > 0)
                    //{
                    Dt_Partidas = (DataTable)Session["Dt_Partidas_Asignadas"];
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ToolTip = "Cancelar Reserva";
                    Reservas_Datos.P_No_Reserva = Dt_Partidas.Rows[0]["NO_RESERVA"].ToString();
                    No_Reserva = Dt_Partidas.Rows[0]["NO_RESERVA"].ToString();
                    Dt_Detalles = Reservas_Datos.Consultar_Reservas();
                    if (Dt_Detalles.Rows.Count > 0)
                    {
                        if (Dt_Detalles.Rows[0]["ESTATUS"].ToString() == "CANCELADA")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La reserva ya fue cancelada anteriormente');", true);
                        }
                        else
                        {
                            Modificar_Reserva(); //Modifica los datos de la Cuenta Contable con los datos proporcionados por el usuario
                        }
                    }
                    ////}
                }
                else
                {
                    Dt_Partidas = (DataTable)Session["Dt_Partidas_Asignadas"];
                    Modificar_Saldo_Reserva();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La Modificación de la reserva No. " + No_Reserva + " fue exitosa');", true);
                    Limpia_Controles(); //Limpia los controles del modulo
                    Inicializa_Controles();
                    Session["Dt_Partidas_Asignadas"] = new DataTable();
                    Llenar_Grid_Partida_Asignada(false);
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Reserva_Click
    ///DESCRIPCIÓN          : Metodo que permite buscar la reserva
    ///CREO                 : Gustavo Angeles
    ///FECHA_CREO           : 9/Diciembre/2010 
    ///MODIFICO             : Leslie Gonzalez Vazquez
    ///FECHA_MODIFICO       : 23/Enero/2012
    ///CAUSA_MODIFICACIÓN   : realice la busqueda de los detalles de la reserva
    ///*******************************************************************************
    protected void Btn_Buscar_Reserva_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Con_Reserva_Nomina_Negocio Reserva_Negocio = new Cls_Ope_Con_Reserva_Nomina_Negocio();
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Detalles = new DataTable();
        try
        {
            Habilitar_Controles("Inicial");
            Llenar_Grid_Reservas();
            Dt_Partidas = (DataTable)Session["Dt_Partidas_Asignadas"];
            if (Dt_Partidas.Rows.Count > 0)
            {
                Btn_Modificar.Visible = true;
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Btn_Modificar.ToolTip = "Cancelar Reserva";
            }

        }
        catch (Exception Ex)
        {
            throw new Exception("Error en la busqueda de reservas. Error[" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Metodo que permite modificar la reserva
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/Noviembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Session["Dt_Partidas_Asignadas"] = new DataTable();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Limpia_Controles(); //Limpia los controles del modulo
                Inicializa_Controles();
                Session["Dt_Partidas_Asignadas"] = new DataTable();
                Llenar_Grid_Partida_Asignada(false);
                Mostrar_Informacion("", false);
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Requisicion_Click
    ///DESCRIPCIÓN: Metodo para consultar la reserva
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Reserva_Click(object sender, ImageClickEventArgs e)
    {
        String No_Reserva = ((ImageButton)sender).CommandArgument;
    }

    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Tipo_Solicitud_Pago_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de tipo de solicitud
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Diciembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Cmb_Tipo_Solicitud_Pago_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        String Tipo_Comprobacion = "";
        try
        {
            if (Cmb_Tipo_Solicitud_Pago.SelectedIndex > 0)
            {
                Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Cmb_Tipo_Solicitud_Pago.SelectedValue;

                //Verificar si es el de los empleados
                if (Cmb_Tipo_Solicitud_Pago.SelectedItem.Value == "00001")
                {
                    Cmb_Proveedor_Solicitud_Pago.Enabled = false;
                    Cmb_Nombre_Empleado.Enabled = true;
                    Txt_Nombre_Empleado.Enabled = true;
                    Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = false;
                    Btn_Buscar_Empleado.Enabled = true;
                    Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = false;
                    Tr_Empleado.Visible = true;
                    Tr_Proveedor.Visible = false;
                    Cmb_Nombre_Empleado.Items.Clear();
                    Cmb_Proveedor_Solicitud_Pago.Items.Clear();
                    Cmb_Nombre_Empleado.Items.Insert(0, new ListItem("<- Seleccione ->", ""));

                    //Tr_Deudor_Anticipos.Style.Add("display", "none");
                    //Tr_Deudor.Style.Add("display", "none");
                }
                else
                {
                    //Cmb_Deudores.Enabled = false;
                    //Txt_Deudor.Enabled = false;
                    //Btn_Buscar_Deudor.Enabled = false;
                    //Tr_Deudor.Visible = false;
                    //Cmb_Deudores.Items.Clear();
                    Cmb_Proveedor_Solicitud_Pago.Enabled = true;
                    Cmb_Nombre_Empleado.Enabled = false;
                    Txt_Nombre_Empleado.Enabled = false;
                    Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = true;
                    Btn_Buscar_Empleado.Enabled = false;
                    Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = true;
                    Tr_Empleado.Visible = false;
                    Tr_Proveedor.Visible = true;
                    //Tr_Deudor_Anticipos.Visible = false;
                    Cmb_Nombre_Empleado.Items.Clear();
                    //Cmb_anticipos.Items.Clear();
                    //Txt_Gastos_Anticipos.Text = "";
                    Cmb_Proveedor_Solicitud_Pago.Items.Clear();
                    Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                    Cmb_Tipo_Reserva.Enabled = true;
                }
            }
            else
            {
                //Tr_Deudor_Anticipos.Visible = false;
                //Tr_Deudor.Visible = false;
                Tr_Empleado.Visible = false;
                Tr_Proveedor.Visible = false;
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el evento del combo de tipos de solicitud Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Buscar_Proveedor_Solicitud_Pagos_Click
    /// DESCRIPCION : Consulta a todos los proveedores que coincidan con el nombre, rfc
    ///               o compañia de acuerdo a lo proporcionado por el usuario
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 23-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Buscar_Proveedor_Solicitud_Pagos_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Cat_Com_Proveedores_Negocio Rs_Consulta_Cat_Com_Proveedores = new Cls_Cat_Com_Proveedores_Negocio(); //Variable de conexión hacia la capa de negocios
        DataTable Dt_Proveedores;
        DataTable Dt_Proveedores_Tipo = new DataTable();
        DataRow Fila;
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (!String.IsNullOrEmpty(Txt_Nombre_Proveedor_Solicitud_Pago.Text))
            {
                Rs_Consulta_Cat_Com_Proveedores.P_Nombre_Comercial = Txt_Nombre_Proveedor_Solicitud_Pago.Text;
                Rs_Consulta_Cat_Com_Proveedores.P_Estatus = "ACTIVO";
                Rs_Consulta_Cat_Com_Proveedores.P_tipo = "CONTABILIDAD";
                Dt_Proveedores = Rs_Consulta_Cat_Com_Proveedores.Consulta_Avanzada_Proveedor(); //Consulta los proveedores que coincidan con el nombre, compañia, rfc
                Dt_Proveedores_Tipo.Columns.Add("COMPANIA", typeof(System.String));
                Dt_Proveedores_Tipo.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                foreach (DataRow Renglon in Dt_Proveedores.Rows)
                {

                    if (!String.IsNullOrEmpty(Renglon["CUENTA_PROVEEDOR_ID"].ToString().Trim()))
                    {
                        Fila = Dt_Proveedores_Tipo.NewRow();
                        Fila["COMPANIA"] = "P-" + Renglon["COMPANIA"].ToString().Trim();
                        Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                        Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Proveedores_Tipo.AcceptChanges();
                    }
                    if (!String.IsNullOrEmpty(Renglon["CUENTA_CONTRATISTA_ID"].ToString().Trim()))
                    {
                        Fila = Dt_Proveedores_Tipo.NewRow();
                        Fila["COMPANIA"] = "C-" + Renglon["COMPANIA"].ToString().Trim();
                        Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                        Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Proveedores_Tipo.AcceptChanges();
                    }
                    if (!String.IsNullOrEmpty(Renglon["CUENTA_JUDICIAL_ID"].ToString().Trim()))
                    {
                        Fila = Dt_Proveedores_Tipo.NewRow();
                        Fila["COMPANIA"] = "J-" + Renglon["COMPANIA"].ToString().Trim();
                        Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                        Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Proveedores_Tipo.AcceptChanges();
                    }
                    if (!String.IsNullOrEmpty(Renglon["CUENTA_NOMINA_ID"].ToString().Trim()))
                    {
                        Fila = Dt_Proveedores_Tipo.NewRow();
                        Fila["COMPANIA"] = "N-" + Renglon["COMPANIA"].ToString().Trim();
                        Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                        Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Proveedores_Tipo.AcceptChanges();
                    }
                    if (!String.IsNullOrEmpty(Renglon["CUENTA_ACREEDOR_ID"].ToString().Trim()))
                    {
                        Fila = Dt_Proveedores_Tipo.NewRow();
                        Fila["COMPANIA"] = "A-" + Renglon["COMPANIA"].ToString().Trim();
                        Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                        Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Proveedores_Tipo.AcceptChanges();
                    }
                    if (!String.IsNullOrEmpty(Renglon["CUENTA_PREDIAL_ID"].ToString().Trim()))
                    {
                        Fila = Dt_Proveedores_Tipo.NewRow();
                        Fila["COMPANIA"] = "Z-" + Renglon["COMPANIA"].ToString().Trim();
                        Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                        Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Proveedores_Tipo.AcceptChanges();
                    }
                }
                Cmb_Proveedor_Solicitud_Pago.DataSource = new DataTable();
                Cmb_Proveedor_Solicitud_Pago.DataBind();
                Cmb_Proveedor_Solicitud_Pago.DataSource = Dt_Proveedores_Tipo;
                Cmb_Proveedor_Solicitud_Pago.DataTextField = Cat_Com_Proveedores.Campo_Compañia;
                Cmb_Proveedor_Solicitud_Pago.DataValueField = Cat_Com_Proveedores.Campo_Proveedor_ID;
                Cmb_Proveedor_Solicitud_Pago.DataBind();
                Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                Cmb_Proveedor_Solicitud_Pago.SelectedIndex = -1;
            }
            else
            {
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Unidad_Responsable_Busqueda_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de unidad responsable
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 22/Diciembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Cmb_Unidad_Responsable_Busqueda_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Reserva_Nomina_Negocio Rs_Consulta_Dependencias = new Cls_Ope_Con_Reserva_Nomina_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            if (Btn_Nuevo.Visible)
            {
                Llenar_Grid();
            }
            else 
            {
                Llenar_Grid_Partidas_De_Reserva();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el evento del combo de unidad responsable Error[" + ex.Message + "]");
        }
    }

    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Programa_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de programa
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Septiembre/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Cmb_Programa_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Llenar_Combo_Dependecias();

            if (Btn_Nuevo.Visible)
            {
                Llenar_Grid();
            }
            else
            {
                Llenar_Grid_Partidas_De_Reserva();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el evento del combo de programas Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Fuente_Ramo_OnSelectedIndexChanged
    ///DESCRIPCIÓN          : Metodo que filtra el tipo de reserva para que solo muestre el ramo 33 o los que no son de ramo 33 
    ///CREO                 : Sergio Manuel Gallardo Andrade 
    ///FECHA_CREO           : 28/marzo/2012 
    ///MODIFICO             : 
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Fuente_Ramo_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Llenar_Grid_Partida_Asignada(false);
            Cls_Ope_Con_Reserva_Nomina_Negocio Rs_Consulta_Dependencias = new Cls_Ope_Con_Reserva_Nomina_Negocio();
            DataTable Dt_Consulta = new DataTable();
            if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex > 0)
            {
                Rs_Consulta_Dependencias.P_Dependencia_ID = Cmb_Unidad_Responsable_Busqueda.SelectedValue;
                Rs_Consulta_Dependencias.P_Ramo_33 = "NO";
                Rs_Consulta_Dependencias.P_Filtro_Campo_Mes = Consultar_Nombre_Mes(String.Format("{0:0#}", (DateTime.Now.Month)));
                Dt_Consulta = Rs_Consulta_Dependencias.Consultar_Reservas_Unidad_Responsable();
                Grid_Partida_Saldo.Columns[0].Visible = true;
                Grid_Partida_Saldo.Columns[1].Visible = true;
                Grid_Partida_Saldo.Columns[2].Visible = true;
                Grid_Partida_Saldo.Columns[3].Visible = true;
                Grid_Partida_Saldo.Columns[4].Visible = true;
                Grid_Partida_Saldo.Columns[5].Visible = true;
                Grid_Partida_Saldo.Columns[15].Visible = true;
                Grid_Partida_Saldo.DataSource = Dt_Consulta;
                Grid_Partida_Saldo.DataBind();
                Grid_Partida_Saldo.Columns[0].Visible = false;
                Grid_Partida_Saldo.Columns[1].Visible = false;
                Grid_Partida_Saldo.Columns[2].Visible = false;
                Grid_Partida_Saldo.Columns[3].Visible = false;
                Grid_Partida_Saldo.Columns[4].Visible = false;
                Grid_Partida_Saldo.Columns[5].Visible = false;
                Grid_Partida_Saldo.Columns[15].Visible = false;
                Session["Dt_Partidas_Asignadas"] = new DataTable();
                Llenar_Grid_Partida_Asignada(false);
                Tr_Div_Partidas.Style.Add("display", "none");
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de agregar un registro a la tabla. Error[" + ex.Message + "]");
        }

    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Agregar_Click
    ///DESCRIPCIÓN          : Evento del boton de agregar una partida a la reserva
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 20/Enero/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Agregar_Click(object sender, EventArgs e)
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
        Mostrar_Informacion("", false);
        try
        {
            if (Validar_Datos())
            {
                Llenar_Grid_Partida_Asignada(true);
                Crear_Dt_Partida_Asignada();
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                Llenar_Grid_Partida_Asignada(false);
            }
            else
            {
                Mostrar_Informacion(Lbl_Mensaje_Error.Text.Trim(), true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de agregar un registro a la tabla. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
    ///DESCRIPCIÓN          : Evento del boton de eliminar un registro de las partidas
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 20/Enero/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Eliminar_Click(object sender, EventArgs e)
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        ImageButton Btn_Eliminar = (ImageButton)sender;
        Int32 No_Fila = -1;
        String Id = String.Empty;
        String Partida_Id = String.Empty;
        String Dependencia_Id = String.Empty;
        String[] Arreglo = null;

        try
        {
            Id = Btn_Eliminar.CommandArgument.ToString().Trim();
            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Id))
                    {
                        //obtenemos el id de la partida y de la dependecia
                        Arreglo = Id.Split('_');
                        if (Arreglo != null)
                        {
                            Partida_Id = Arreglo[0].ToString().Trim();
                            Dependencia_Id = Arreglo[1].ToString().Trim();
                        }

                        foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                        {
                            No_Fila++;
                            if (Dr["PARTIDA_ID"].ToString().Trim().Equals(Partida_Id.Trim())
                                && Dr["DEPENDENCIA_ID"].ToString().Trim().Equals(Dependencia_Id.Trim()))
                            {
                                Dt_Partidas_Asignadas.Rows.RemoveAt(No_Fila);
                                Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Asignadas;
                                Llenar_Grid_Partida_Asignada(false);
                                break;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Busqueda_Cuentas_Popup_Click
    /// DESCRIPCION : Busca las cuentas contables referentes a la descripcion
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 02/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Busqueda_Partida_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.Visible)
            {
                Llenar_Grid();
            }
            else
            {
                Llenar_Grid_Partidas_De_Reserva();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de buscar un registro a la tabla. Error[" + ex.Message + "]");
        }
    }

    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Empleado_Click
    ///DESCRIPCIÓN          : Evento del boton de busqueda de empleados
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Diciembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Buscar_Empleado_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Cat_Empleados_Negocios Rs_Consulta_Ca_Empleados = new Cls_Cat_Empleados_Negocios(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Empleados; //Variable que obtendra los datos de la consulta 
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (!String.IsNullOrEmpty(Txt_Nombre_Empleado.Text))
            {
                //Verificar si es numerico
                if (Cls_Util.EsNumerico(Txt_Nombre_Empleado.Text.Trim()) == false)
                {
                    Rs_Consulta_Ca_Empleados.P_Nombre = Txt_Nombre_Empleado.Text.Trim();
                }
                else
                {
                    Rs_Consulta_Ca_Empleados.P_No_Empleado = String.Format("{0:000000}", Convert.ToInt32(Txt_Nombre_Empleado.Text.Trim()));
                }

                Rs_Consulta_Ca_Empleados.P_Estatus = "ACTIVO";
                Dt_Empleados = Rs_Consulta_Ca_Empleados.Consulta_Empleados_General();
                Cmb_Nombre_Empleado.DataSource = new DataTable();
                Cmb_Nombre_Empleado.DataBind();
                Cmb_Nombre_Empleado.DataSource = Dt_Empleados;
                Cmb_Nombre_Empleado.DataTextField = "Empleado";
                Cmb_Nombre_Empleado.DataValueField = Cat_Empleados.Campo_Empleado_ID;
                Cmb_Nombre_Empleado.DataBind();
                Cmb_Nombre_Empleado.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                Cmb_Nombre_Empleado.SelectedIndex = -1;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    #endregion

    #region (Grid)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Partidas_Saldo_RowDataBound
    ///DESCRIPCIÓN: 
    ///
    ///PARAMETROS:  
    ///CREO:    Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO: 29/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Partidas_Saldo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                ((System.Web.UI.WebControls.TextBox)e.Row.Cells[13].FindControl("Txt_Importe_Partida")).ToolTip = "Disponible: " + e.Row.Cells[9].Text;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_RowDataBound
    ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 20/enero/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Grid_Partida_Asignada_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
        String Dependencia_Id = String.Empty;
        DataTable Dt_Ur = new DataTable();

        try
        {
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    GridView Grid_Partidas_Detalle = (GridView)e.Row.Cells[4].FindControl("Grid_Partidas_Asignadas_Detalle");
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        //obtenemos solo los datos de esa dependencia
                        Dependencia_Id = e.Row.Cells[2].Text.Trim();
                        Dt_Ur = (from Fila in Dt_Partidas_Asignadas.AsEnumerable()
                                 where Fila.Field<string>("DEPENDENCIA_ID") == Dependencia_Id
                                 select Fila).AsDataView().ToTable();

                        Grid_Partidas_Detalle.Columns[0].Visible = false;
                        Grid_Partidas_Detalle.Columns[1].Visible = true;
                        Grid_Partidas_Detalle.Columns[2].Visible = true;
                        Grid_Partidas_Detalle.Columns[5].Visible = true;
                        Grid_Partidas_Detalle.Columns[6].Visible = true;
                        Grid_Partidas_Detalle.Columns[7].Visible = true;
                        Grid_Partidas_Detalle.DataSource = Dt_Ur;
                        Grid_Partidas_Detalle.DataBind();
                        Grid_Partidas_Detalle.Columns[1].Visible = false;
                        Grid_Partidas_Detalle.Columns[2].Visible = false;
                        Grid_Partidas_Detalle.Columns[6].Visible = false;
                        Grid_Partidas_Detalle.Columns[7].Visible = false;

                        if (Btn_Nuevo.ToolTip == "Nuevo")
                        {
                            Grid_Partidas_Detalle.Columns[5].Visible = false;
                        }
                        else
                        {
                            Grid_Partidas_Detalle.Columns[0].Visible = false;
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error:[" + Ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_Detalle_RowCreated
    ///DESCRIPCIÓN          : Evento del grid del movimiento del cursor sobre los registros
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 22/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Grid_Partidas_Asignadas_Detalle_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';this.style.color='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;this.style.color=this.originalstyle;");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Partidas_Saldo_RowDataBound
    ///DESCRIPCIÓN: 
    ///
    ///PARAMETROS:  
    ///CREO:    Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO: 29/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Partidas_d_Reserva_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                if (Cmb_Tipo_Modificacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                {
                    ((System.Web.UI.WebControls.TextBox)e.Row.Cells[11].FindControl("Txt_Importe_Comprometido")).ToolTip = "Disponible: " + e.Row.Cells[9].Text;
                }
                else
                {
                    ((System.Web.UI.WebControls.TextBox)e.Row.Cells[11].FindControl("Txt_Importe_Comprometido")).ToolTip = "Disponible: " + e.Row.Cells[10].Text;
                }

            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #endregion
}