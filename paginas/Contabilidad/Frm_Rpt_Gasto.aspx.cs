﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Gasto.Negocio;

public partial class paginas_Contabilidad_Frm_Rpt_Gasto : System.Web.UI.Page
{
    #region (Variables Locales)
    private const String P_Dt_Gasto = "Dt_Gasto";
    #endregion

    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Estado_Inicial();
        }
        else
        {
            Mostrar_Error("", false);
        }
    }
    #endregion

    #region (Metodos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Mostrar_Error
    ///DESCRIPCION:             Mostrar el mensaje de error
    ///PARAMETROS:              1. Mensaje: Cadena de texto con el mensaje de error a mostrar
    ///                         2. Mostrar: Booleano que indica si se va a mostrar el mensaje de error.
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              17/Abril/2012 17:39
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Mostrar_Error(String Mensaje, Boolean Mostrar)
    {
        try
        {
            Lbl_Informacion.Text = Mensaje;
            Div_Contenedor_Msj_Error.Visible = Mostrar;
        }
        catch (Exception ex)
        {
            Lbl_Informacion.Text = "Error: (Mostrar_Error)" + ex.ToString();
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    private void Estado_Inicial()
    {
        try
        {
            Elimina_Sesiones();
            Llena_Combo_Gerencias();
            Llena_Combo_Unidades_Responsables(null);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Elimina_Sesiones
    ///DESCRIPCION:             Eliminar las sesiones utilizadas en esta pagina
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              23/Abril/2012 18:20
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Elimina_Sesiones()
    {
        try
        {
            HttpContext.Current.Session.Remove(P_Dt_Gasto);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Combo_Gerencias
    ///DESCRIPCION:             Llenar el combo de las gerencias
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              24/Abril/2012 14:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Combo_Gerencias()
    {
        //Declaracion de variables
        Cls_Rpt_Gastos_Negocio Reporte_Gasto_Negocio = new Cls_Rpt_Gastos_Negocio(); //variable para la capa de negocios
        DataTable Dt_Gerencias = new DataTable(); //tabla para el llenado del combo

        try
        {
            //Ejecutar la consulta
            Dt_Gerencias = Reporte_Gasto_Negocio.Consulta_Gerencias();

            //Llenar el combo
            Cmb_Gerencias.Items.Clear();
            Cmb_Gerencias.DataSource = Dt_Gerencias;
            Cmb_Gerencias.DataTextField = "Gerencia";
            Cmb_Gerencias.DataValueField = Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID;
            Cmb_Gerencias.DataBind();
            Cmb_Gerencias.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Gerencias.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Combo_Unidades_Responsables
    ///DESCRIPCION:             Llenar el combo de las unidades responsables
    ///PARAMETROS:              Grupo_Dependencia_ID: Cadena de texto que contiene el ID del Grupo al cual pertenece la dependencia
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              24/Abril/2012 14:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Combo_Unidades_Responsables(String Grupo_Dependencia_ID)
    {
        //Declaracion de variables
        DataTable Dt_Unidades_Responsables = new DataTable(); //tabla para el llenado del combo
        Cls_Rpt_Gastos_Negocio Reporte_Gasto_Negocio = new Cls_Rpt_Gastos_Negocio(); //variable para la capa de negocios

        try
        {
            //Ejecutar consulta
            if (String.IsNullOrEmpty(Grupo_Dependencia_ID) == false)
            {
                Reporte_Gasto_Negocio.P_Gerencia_ID = Grupo_Dependencia_ID;
            }
            Dt_Unidades_Responsables = Reporte_Gasto_Negocio.Consulta_Unidades_Responsables();

            //Llenar el combo
            Cmb_Unidades_Reponsables.Items.Clear();
            Cmb_Unidades_Reponsables.DataSource = Dt_Unidades_Responsables;
            Cmb_Unidades_Reponsables.DataTextField = "Unidad_Responsable";
            Cmb_Unidades_Reponsables.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidades_Reponsables.DataBind();
            Cmb_Unidades_Reponsables.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Unidades_Reponsables.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Reporte_Excel
    ///DESCRIPCION:             LLenar un reporte de Excel con el resultado de la consulta
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              25/Abril/2012 11:50
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Reporte_Excel()
    {
        //Declaracion de variables
        DataTable Dt_Gasto = new DataTable(); //tabla para el resultado de la consulta
        int Cont_Elementos = 0; //variable para el contador
        Cls_Rpt_Gastos_Negocio Gastos_Negocio = new Cls_Rpt_Gastos_Negocio(); //variable para la capa de negocios
        String Ruta_Archivo = String.Empty; //variable para la ruta del archivo
        String Nombre_Archivo = "Reporte_Gasto.xls"; //variable para el nombre del archivo
        Workbook book = new Workbook(); //Variable para el libro
        WorksheetStyle style; //Variable para el estilo
        Worksheet sheet; //variable para la hoja
        WorksheetRow row; //variable para el renglon
        WorksheetCell cell; //Variable para la celda
        String Script_js = String.Empty; //variable para el Script de javascript
        String Encabezado = String.Empty; //variable para el encabezado del reporte

        try
        {
            //Verificar si esta seleccionada una unidad responsable
            if (Cmb_Unidades_Reponsables.SelectedIndex > 0)
            {
                Gastos_Negocio.P_Unidad_Responsable_ID = Cmb_Unidades_Reponsables.SelectedItem.Value;
            }
            else
            {
                //verificar si esta seleccionada una gerencia
                if (Cmb_Gerencias.SelectedIndex > 0)
                {
                    Gastos_Negocio.P_Gerencia_ID = Cmb_Gerencias.SelectedItem.Value;
                }
            }

            //Verificar si hay fechas
            if (String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) == false)
            {
                Gastos_Negocio.P_Fecha_Inicio = Convert.ToDateTime(Txt_Fecha_Inicial.Text);

                if (String.IsNullOrEmpty(Txt_Fecha_Final.Text) == false)
                {
                    Gastos_Negocio.P_Fecha_Fin = Convert.ToDateTime(Txt_Fecha_Final.Text);
                }
            }

            //Ejecutar consulta
            Dt_Gasto = Gastos_Negocio.Consulta_Gasto();

            //Verificar si la consulta arrojo resultados
            if (Dt_Gasto.Rows.Count > 0)
            {
                //Asignar la ruta del archivo
                Ruta_Archivo = HttpContext.Current.Server.MapPath("~") + "\\Exportaciones\\" + Nombre_Archivo;

                // Especificar qué hoja debe ser abierto y el tamaño de la ventana por defecto
                book.ExcelWorkbook.ActiveSheetIndex = 0;
                book.ExcelWorkbook.WindowTopX = 100;
                book.ExcelWorkbook.WindowTopY = 200;
                book.ExcelWorkbook.WindowHeight = 7000;
                book.ExcelWorkbook.WindowWidth = 8000;

                // Propiedades del documento
                book.Properties.Author = "CONTEL";
                book.Properties.Title = "REPORTE";
                book.Properties.Created = DateTime.Now;

                // Se agrega estilo al libro
                style = book.Styles.Add("HeaderStyle");
                style.Font.FontName = "Tahoma";
                style.Font.Size = 14;
                style.Font.Bold = true;
                style.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                style.Font.Color = "White";
                style.Interior.Color = "Blue";
                style.Interior.Pattern = StyleInteriorPattern.DiagCross;

                // Se Crea el estilo a usar
                style = book.Styles.Add("Default");
                style.Font.FontName = "Tahoma";
                style.Font.Size = 10;

                //Asignar el nombre a la hoja
                sheet = book.Worksheets.Add("Hoja1");

                //Agregar renglon para el encabezado
                row = sheet.Table.Rows.Add();
                row.Index = 0;//Para saltarse Filas

                // Se agrega el encabezado
                //row.Cells.Add(new WorksheetCell(Encabezado, "HeaderStyle"));

                //Agregar renglones para separar
                row = sheet.Table.Rows.Add();
                row = sheet.Table.Rows.Add();
                row = sheet.Table.Rows.Add();

                //Encabezado de la tabla
                row.Cells.Add(new WorksheetCell("Gerencia", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Unidad Responsable", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Partida", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Total", "HeaderStyle"));

                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Gasto.Rows.Count; Cont_Elementos++)
                {
                    //Agregar renglon
                    row = sheet.Table.Rows.Add();

                    //Agregar las columnas
                    if (Dt_Gasto.Rows[Cont_Elementos]["Gerencia"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Gasto.Rows[Cont_Elementos]["Gerencia"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Gasto.Rows[Cont_Elementos]["Unidad_Responsable"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Gasto.Rows[Cont_Elementos]["Unidad_Responsable"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Gasto.Rows[Cont_Elementos]["Partida"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Gasto.Rows[Cont_Elementos]["Partida"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Gasto.Rows[Cont_Elementos]["Total"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("0", DataType.Number));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Gasto.Rows[Cont_Elementos]["Total"].ToString().Trim(), DataType.Number));
                    }
                }

                // Se Guarda el archivo                
                book.Save(Ruta_Archivo);
                string script = @"<script type='text/javascript'>alert('Registros Exportados a Excel');</script>";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);

                Mostrar_Reporte(Nombre_Archivo, "Excel");
            }
            else
            {
                Mostrar_Error("La consulta no arroj&oacute; resultados.", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Exportaciones/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Llena_Reporte_PDF
    /// DESCRIPCION:         Llenar el reporte PDF con el resultado de la consulta
    /// PARAMETROS:          
    /// USUARIO CREO:        Noe Mosqueda Valadez
    /// FECHA CREO:          25/Abril/2012 16:54
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACION:  
    /// *************************************************************************************
    private void Llena_Reporte_PDF()
    {
        //Declaracion de variables
        DataTable Dt_Gasto = new DataTable(); //tabla para el resultado de la consulta
        Cls_Rpt_Gastos_Negocio Gastos_Negocio = new Cls_Rpt_Gastos_Negocio(); //variable para la capa de negocios
        DataSet Ds_Reporte = new DataSet(); //variable para el dataset con la infromacion del reporte
        Ds_Rpt_Con_Gasto Ds_Rpt_Con_Gasto_src = new Ds_Rpt_Con_Gasto(); //Dataset archivo para el llenado del reporte de Crystal

        try
        {
            //Verificar si esta seleccionada una unidad responsable
            if (Cmb_Unidades_Reponsables.SelectedIndex > 0)
            {
                Gastos_Negocio.P_Unidad_Responsable_ID = Cmb_Unidades_Reponsables.SelectedItem.Value;
            }
            else
            {
                //verificar si esta seleccionada una gerencia
                if (Cmb_Gerencias.SelectedIndex > 0)
                {
                    Gastos_Negocio.P_Gerencia_ID = Cmb_Gerencias.SelectedItem.Value;
                }
            }

            //Verificar si hay fechas
            if (String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) == false)
            {
                Gastos_Negocio.P_Fecha_Inicio = Convert.ToDateTime(Txt_Fecha_Inicial.Text);

                if (String.IsNullOrEmpty(Txt_Fecha_Final.Text) == false)
                {
                    Gastos_Negocio.P_Fecha_Fin = Convert.ToDateTime(Txt_Fecha_Final.Text);
                }
            }

            //Ejecutar consulta
            Dt_Gasto = Gastos_Negocio.Consulta_Gasto();

            //Verificar si la consulta arrojo resultados
            if (Dt_Gasto.Rows.Count > 0)
            {
                //Colocar tabla en el dataset
                Ds_Reporte.Tables.Add(Dt_Gasto);

                //Generar el reporte
                Generar_Reporte(Ds_Reporte, Ds_Rpt_Con_Gasto_src, "Rpt_Con_Gasto.rpt", "Reporte_Gasto.pdf");
            }
            else
            {
                Mostrar_Error("La consulta no arroj&oacute; resultados.", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Nombre_PDF)
    {

        ReportDocument Reporte = new ReportDocument();
        DataRow Renglon; //Renglon para el llenado de las tablas
        int Cont_Elementos; //Variable para el contador

        //Ciclo para el barrido de los detalles
        for (Cont_Elementos = 0; Cont_Elementos < Data_Set_Consulta_DB.Tables[0].Rows.Count; Cont_Elementos++)
        {
            //Instanciar el renglon
            Renglon = Data_Set_Consulta_DB.Tables[0].Rows[Cont_Elementos];

            //Importar el renglon
            Ds_Reporte.Tables[0].ImportRow(Renglon);
        }

        String File_Path = Server.MapPath("../Rpt/Contabilidad/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        //Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        String Ruta = "../../Reporte/" + Nombre_PDF;
        Mostrar_Reporte(Nombre_PDF, "PDF");
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
    }

    #endregion

    #region (Grid)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Grid_Gasto
    ///DESCRIPCION:             Llenar el Grid con la consulta del gasto
    ///PARAMETROS:              Pagina: Entero que contiene la pagina del Grid
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              25/Abril/2012 11:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Grid_Gasto(int Pagina)
    {
        //Declaracion de variables
        DataTable Dt_Resultado = new DataTable(); //variable para el resultado
        Cls_Rpt_Gastos_Negocio Gastos_Negocio = new Cls_Rpt_Gastos_Negocio(); //variable para la capa de negocios

        try
        {
            //verificar si existe la variable de sesion
            if (HttpContext.Current.Session[P_Dt_Gasto] != null)
            {
                //Convertir la variable de sesion a una tabla
                Dt_Resultado = ((DataTable)HttpContext.Current.Session[P_Dt_Gasto]);
            }
            else
            {
                //Verificar si esta seleccionada una unidad responsable
                if (Cmb_Unidades_Reponsables.SelectedIndex > 0)
                {
                    Gastos_Negocio.P_Unidad_Responsable_ID = Cmb_Unidades_Reponsables.SelectedItem.Value;
                }
                else
                {
                    //verificar si esta seleccionada una gerencia
                    if (Cmb_Gerencias.SelectedIndex > 0)
                    {
                        Gastos_Negocio.P_Gerencia_ID = Cmb_Gerencias.SelectedItem.Value;
                    }
                }

                //Verificar si hay fechas
                if (String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) == false)
                {
                    Gastos_Negocio.P_Fecha_Inicio = Convert.ToDateTime(Txt_Fecha_Inicial.Text);

                    if (String.IsNullOrEmpty(Txt_Fecha_Final.Text) == false)
                    {
                        Gastos_Negocio.P_Fecha_Fin = Convert.ToDateTime(Txt_Fecha_Final.Text);
                    }
                }

                //Ejecutar consulta
                Dt_Resultado = Gastos_Negocio.Consulta_Gasto();
            }

            //Verificar si la consulta arrojo resultados
            if (Dt_Resultado.Rows.Count > 0)
            {
                //Llenar el grid
                Grid_Gasto.DataSource = Dt_Resultado;

                //Verificar si se tiene una pagina
                if (Pagina > -1)
                {
                    Grid_Gasto.PageIndex = Pagina;
                }

                Grid_Gasto.DataBind();
            }
            else
            {
                Mostrar_Error("La consulta no arroj&oacute; resultados.", true);
                Grid_Gasto.DataBind();
            }

            //Colocar la tabla en la variable de sesion
            HttpContext.Current.Session[P_Dt_Gasto] = Dt_Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    protected void Grid_Gasto_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Llena_Grid_Gasto(e.NewPageIndex);
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Grid_Gasto_PageIndexChanging) " + ex.Message, true);
        }
    }

    #endregion

    #region (Eventos)
    protected void Cmb_Gerencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Llena_Combo_Unidades_Responsables(Cmb_Gerencias.SelectedItem.Value);
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Cmb_Gerencias_SelectedIndexChanged) " + ex.Message, true);
        }
    }

    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Elimina_Sesiones();
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Salir_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Imprimir_Excel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Llena_Reporte_Excel();
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Imprimir_Excel_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Llena_Reporte_PDF();            
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Imprimir_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Consultar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Elimina_Sesiones();
            Llena_Grid_Gasto(-1);
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Consultar_Click) " + ex.Message, true);
        }
    }
    #endregion
}