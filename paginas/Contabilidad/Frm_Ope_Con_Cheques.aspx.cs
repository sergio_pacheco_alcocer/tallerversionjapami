﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
//using System.Windows.Forms;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sindicatos.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using AjaxControlToolkit;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Area_Funcional.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Compromisos_Contabilidad.Negocios;
using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Cheque.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago_Contabilidad.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Numalet;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Contabilidad_Transferencia.Negocio;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Catalogo_Con_Proveedores.Negocio;
//using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;
using JAPAMI.Parametros_Almacen_Cuentas.Negocio;
using JAPAMI.Cheque.Negocio;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Cheques : System.Web.UI.Page
{
    private static String P_Dt_Solicitud = "P_Dt_Solicitud";
    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        try
        {
            if (!IsPostBack)
            {
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);
                Txt_Fecha.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Inicio.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_No_Pago.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                Cls_Ope_Con_Cheques_Negocio Tipos_Negocio = new Cls_Ope_Con_Cheques_Negocio();
                DataTable Dt_Tipos = Tipos_Negocio.Consulta_Tipos_Solicitudes();
                RBL_Orden.SelectedIndex = 0;
                Llenar_Combos_Generales();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// 
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            //Botones.Add(Btn_Eliminar);
            //Botones.Add(Btn_Mostrar_Popup_Busqueda);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Solicitudes_Pendientes
    // DESCRIPCIÓN: Llena el grid principal de las solicitudes de pago que estan autorizadas
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 18/noviembre/2011 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Grid_Solicitudes_Pendientes()
    {
        DataTable Temporal = null;
        String Tipo;
        String Banco_Tipo="";
        String Tipo_Pago="";
        String Forma_Pago;
        DataTable Dt_Modificada = new DataTable();
        Boolean Insertar;
        Double Monto;
        DataTable Dt_Datos_Detalles = new DataTable();
        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
        DataTable Dt_Datos = new DataTable();
        Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
        Int32 Contador = 0;
        Session["Contador"] = Contador;
        Cls_Ope_Con_Cheques_Negocio Solicitudes = new Cls_Ope_Con_Cheques_Negocio();
        Solicitudes.P_Estatus = "EJERCIDO";
        Session[P_Dt_Solicitud] = Solicitudes.Consulta_Solicitudes_Autorizadas();
        if (Session[P_Dt_Solicitud] != null && ((DataTable)Session[P_Dt_Solicitud]).Rows.Count > 0)
        {
            Temporal = Session[P_Dt_Solicitud] as DataTable;
            foreach (DataRow Fila in Temporal.Rows)
            {
                if (Dt_Modificada.Rows.Count <=0 && Dt_Modificada.Columns.Count<=0)
                {
                    Dt_Modificada.Columns.Add("TIPO", typeof(System.String));
                    Dt_Modificada.Columns.Add("MONTO_TIPO", typeof(System.Double));
                    Dt_Modificada.Columns.Add("FORMA_PAGO", typeof(System.String));
                    Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                    Dt_Modificada.Columns.Add("TIPO_PAGO_FINANZAS", typeof(System.String));
                    Dt_Modificada.Columns.Add("BANCO_PAGO", typeof(System.String));
                }
                DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                if (RBL_Orden.SelectedItem.Text.ToString() == "Banco")
                {
                    Tipo = Fila["BANCO"].ToString();
                }
                else
                {
                    Tipo = Fila["TIPO_PAGO_FINANZAS"].ToString();
                } 
                if (RBL_Orden.SelectedItem.Text.ToString() == "Banco")
                {
                    Banco_Tipo = Fila["BANCO_PAGO"].ToString();
                }
                //if (Cmb_Tipo_Pago_filtro.SelectedIndex>0 )
                //{
                //    Tipo_Pago = Cmb_Tipo_Pago_filtro.SelectedItem.Text.ToString();
                //}
                Forma_Pago = Cmb_Forma_Pago.SelectedItem.Text.ToString();
                Insertar = true;
                Monto = 0;
                //insertar los que no se repiten
                foreach (DataRow Fila2 in Dt_Modificada.Rows)
                {
                    //se verifica si selecciono un tipo de pago para solo mostrar las solicitudes de ese pago
                    if (Tipo_Pago != "")
                    {
                        if (Tipo.Equals(Fila2["TIPO"].ToString()) && Tipo_Pago.Equals(Fila2["TIPO_PAGO_FINANZAS"].ToString()))
                        {
                            Insertar = false;
                        }
                    }
                    else
                    {
                        if (Tipo.Equals(Fila2["TIPO"].ToString()))
                        {
                            Insertar = false;
                        }
                    }
                }
                //se calcula el monto por tipo
                foreach (DataRow Renglon in Temporal.Rows)
                {
                     if (Tipo_Pago != ""){
                         if (RBL_Orden.SelectedItem.Text.ToString() == "Banco" )
                            {
                                if (Tipo.Equals(Renglon["BANCO"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()) && Tipo_Pago.Equals(Renglon["TIPO_PAGO_FINANZAS"].ToString()))
                                {
                                    Monto = Monto + Convert.ToDouble(Renglon["MONTO"].ToString());
                                }
                            }
                            else
                            {
                                if (Tipo.Equals(Renglon["TIPO_PAGO_FINANZAS"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()) && Tipo_Pago.Equals(Renglon["TIPO_PAGO_FINANZAS"].ToString()))
                                {
                                    Monto = Monto + Convert.ToDouble(Renglon["MONTO"].ToString());
                                }
                            }
                    }else{
                          if (RBL_Orden.SelectedItem.Text.ToString() == "Banco")
                            {
                                if (Tipo.Equals(Renglon["BANCO"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()))
                                {
                                    Monto = Monto + Convert.ToDouble(Renglon["MONTO"].ToString());
                                }
                            }
                            else
                            {
                                if (Tipo.Equals(Renglon["TIPO_PAGO_FINANZAS"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()))
                                {
                                    Monto = Monto + Convert.ToDouble(Renglon["MONTO"].ToString());
                                }
                            }
                    }
                }
                if (Insertar && Monto > 0)
                {
                    row["TIPO"] = Tipo;
                    row["MONTO_TIPO"] = Monto;
                    row["FORMA_PAGO"] = Forma_Pago;
                    row["IDENTIFICADOR_TIPO"] = Forma_Pago+Tipo+Monto;
                    row["TIPO_PAGO_FINANZAS"] = Fila["TIPO_PAGO_FINANZAS"].ToString();
                    row["BANCO_PAGO"] = Banco_Tipo;
                    Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Modificada.AcceptChanges();
                }
            }
            Session["Dt_Datos_Detalles"] = Temporal;
            Session["Dt_Datos"] = Dt_Modificada;
            Grid_Pagos.DataSource = Dt_Modificada;
            Grid_Pagos.DataBind();
            //Grid_Pagos.Columns[3].Visible = false;
        }
        else
        {
            Session[P_Dt_Solicitud] = null;
            Grid_Pagos.DataSource = null;
            Grid_Pagos.DataBind();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 18/Noviembre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpia_Controles();
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Llenar_Grid_Solicitudes_Pendientes();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 18/Noviembre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Txt_No_Solicitud_Pago.Text = "";
            // text de solicitud de pago
            Txt_No_Solicitud.Text = "";
            Txt_Fecha.Text = "";
            Txt_Motivo_Cancelacion.Text = "";
            Txt_Tipo_Solicitud_Pago.Text = "";
            Txt_MesAnio.Text = "";
            Txt_No_Reserva_Solicitud.Text = "";
            Txt_Estatus_Solicitud.Text = "";
            Txt_Concepto.Text = "";
            Txt_Monto.Text = "";
            Txt_No_Poliza.Text = "";
            // Text de Datos de pago
            Txt_No_Pago.Text = "";
            Txt_Fecha_No_Pago.Text = "";
            Cmb_Tipo_Pago.SelectedIndex = 0;
            Cmb_Estatus.SelectedIndex = 0;
            Cmb_Banco.SelectedIndex = 0;
            if (Cmb_Cuenta_Banco.SelectedIndex > 0)
            {
                Cmb_Cuenta_Banco.SelectedIndex = 0;
            }
            Txt_No_Cheque.Text = "";
            Txt_Referencia_Pago.Text = "";
            Txt_Comentario_Pago.Text = "";
            Txt_Beneficiario_Pago.Text = "";
            Txt_Cuenta_Contable_ID_Banco.Value = "";
            Txt_Cuenta_Contable_Proveedor.Value = "";
            Txt_Tipo_Solicitud.Value = "";
            Grid_No_Documentos.DataSource = null;
            Grid_No_Documentos.DataBind();
            Txt_Orden.Text = "";
            RBL_Orden.SelectedIndex = 0;
            RBL_Orden_Busqueda.SelectedIndex = 0;
            Txt_No_Solicitud_Autorizar.Value = "";
            Txt_Rechazo.Value = "";
            Txt_Comentario.Text = "";
            Session["Dt_Datos_Detalles_Proveedor"] = null;
            Session["Dt_Datos_Proveedor"] = null;
            Session["Dt_Datos_Detalles"] = null;
            Session["Dt_Datos"] = null;
            Session["Contador"] = null;
            Session["Contador_Proveedor"] = null;
            Session["Dt_Forma_Pago"] = null;
        }
        catch (Exception ex)
        {
            throw new Exception("Limpiar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                          si es una alta, modificacion
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 18/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    //Btn_Eliminar.Visible = true;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Modificar.CausesValidation = false;
                    //Btn_Imprimir_Excel.Visible = true;
                    //Btn_Imprimir_Excel.ToolTip = "Generar Layout traspasos";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Div_Solicitudes_Pendientes.Style.Add("display", "block");
                    Div_Datos_Solicitud.Style.Add("display", "none");
                    //Configuracion_Acceso("Frm_Ope_Con_Cheques.aspx");
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = false;
                    Cmb_Estatus.Enabled = false;
                    Chk_Generar_Layout.Checked = true;
                    //Btn_transferir.Style.Add("display", "block");// = false;
                    //Btn_Transferir_Sin_Layout.Style.Add("display", "none");// = false;
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    //Btn_Eliminar.Visible = false;  
                    //Btn_Imprimir_Excel.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Div_Solicitudes_Pendientes.Style.Add("display", "none");
                    Div_Datos_Solicitud.Style.Add("display", "block");
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Cmb_Estatus.Enabled = false;
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = false;
                    //Btn_Imprimir_Excel.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    Cmb_Estatus.Enabled = true;
                    break;
            }

            Txt_No_Solicitud.Enabled = false;
            Txt_Fecha.Enabled = false;
            Txt_Tipo_Solicitud_Pago.Enabled = false;
            Txt_MesAnio.Enabled = false;
            Txt_No_Reserva_Solicitud.Enabled = false;
            Txt_Estatus_Solicitud.Enabled = false;
            Txt_Concepto.Enabled = false;
            Txt_Monto.Enabled = false;
            Txt_No_Poliza.Enabled = false;
            Txt_No_Pago.Enabled = Habilitado;
            Txt_Fecha_No_Pago.Enabled = Habilitado;
            Cmb_Tipo_Pago.Enabled = Habilitado;
            Cmb_Banco.Enabled = Habilitado;
            Txt_No_Cheque.Enabled = Habilitado;
            Txt_Referencia_Pago.Enabled = Habilitado;
            Txt_Comentario_Pago.Enabled = Habilitado;
            Txt_Beneficiario_Pago.Enabled = Habilitado;
            Txt_Fecha_Inicio.Enabled = false;
            Txt_Fecha_Final.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String Numero_Solicitud)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        try
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            Ds_Reporte = new DataSet();
            Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
            Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                Ds_Rpt_Con_Solicitud_Pagos Obj_DataSET = new Ds_Rpt_Con_Solicitud_Pagos();
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(Ds_Reporte, Obj_DataSET,"Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud + ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            Lbl_Mensaje_Error.Visible = true;
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Requisicion_Click
    ///DESCRIPCIÓN: Metodo para consultar la reserva
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Solicitud_Click(object sender, ImageClickEventArgs e)
    {
        String No_Solicitud = ((ImageButton)sender).CommandArgument;
        Evento_Grid_Solicitudes_Seleccionar(No_Solicitud);
        if (Txt_Estatus_Solicitud.Text != "PAGADO" && Txt_Estatus_Solicitud.Text != "CANCELADO")
        {
            Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Solicitud_Click
    ///DESCRIPCIÓN: manda llamar el metodo de la impresion de la caratula 
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO  : 21-mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Solicitud_Click(object sender, EventArgs e)
    {
        String No_Solicitud = ((LinkButton)sender).Text;
        Imprimir(No_Solicitud);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Btn_Buscar_Solicitud_Click
    ///DESCRIPCIÓN: Metodo para consultar la reserva
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Solicitud_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Temporal = null;
        String Tipo;
        DataTable Dt_Modificada = new DataTable();
        Boolean Insertar;
        Double Monto;
        String Forma_Pago;
        DataTable Dt_Datos_Detalles = new DataTable();
        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
        DataTable Dt_Datos = new DataTable();
        Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
        int Contador = 0;
        Session["Contador"] = Contador;
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        consulta_Negocio.P_Fecha_Inicio = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text));
        consulta_Negocio.P_Fecha_Final = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text));
        if (Txt_No_Solicitud_Pago.Text != "")
        {
            consulta_Negocio.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt16(Txt_No_Solicitud_Pago.Text.ToString()));
        }
        if (Cmb_Estatus_filtro.SelectedIndex > 0)
        {
            consulta_Negocio.P_Estatus = Cmb_Estatus_filtro.SelectedItem.Text.ToString();
        }
        Session[P_Dt_Solicitud] = consulta_Negocio.Consulta_Solicitudes_Autorizadas();
        if (((DataTable)Session[P_Dt_Solicitud]).Rows.Count == 1)
        {
            Temporal = Session[P_Dt_Solicitud] as DataTable;
            String No_Solicitud = Temporal.Rows[0]["No_Solicitud_Pago"].ToString().Trim();
            Evento_Grid_Solicitudes_Seleccionar(No_Solicitud);
            if (Txt_Estatus_Solicitud.Text != "PAGADO" && Txt_Estatus_Solicitud.Text != "CANCELADO")
            {
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                Llenar_Combos_Generales();
            }
        }
        else
        {
            if (Session[P_Dt_Solicitud] != null && ((DataTable)Session[P_Dt_Solicitud]).Rows.Count > 0)
            {
                Temporal=Session[P_Dt_Solicitud] as DataTable;
                foreach (DataRow Fila in Temporal.Rows)
                {
                    if (Dt_Modificada.Rows.Count <= 0 && Dt_Modificada.Columns.Count <= 0)
                    {
                        Dt_Modificada.Columns.Add("TIPO", typeof(System.String));
                        Dt_Modificada.Columns.Add("MONTO_TIPO", typeof(System.String));
                        Dt_Modificada.Columns.Add("FORMA_PAGO", typeof(System.String));
                        Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                    }
                    DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                    if (RBL_Orden.SelectedItem.Text.ToString() == "Banco")
                    {
                        Tipo = Fila["BANCO"].ToString();
                    }
                    else
                    {
                        Tipo = Fila["TIPO_PAGO_FINANZAS"].ToString();
                    }
                    Forma_Pago = Cmb_Forma_Pago_Filtro.SelectedItem.Text.ToString();
                    Insertar = true;
                    Monto = 0;
                    //insertar los que no se repiten
                    foreach (DataRow Fila2 in Dt_Modificada.Rows)
                    {
                        if (Tipo.Equals(Fila2["TIPO"].ToString()))
                        {
                            Insertar = false;
                        }
                    }
                    //se calcula el monto por tipo
                    foreach (DataRow Renglon in Temporal.Rows)
                    {
                        if (RBL_Orden.SelectedItem.Text.ToString() == "Banco")
                        {
                            if (Tipo.Equals(Renglon["BANCO"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()))
                            {
                                Monto = Monto + Convert.ToDouble(Renglon["MONTO"].ToString());
                            }
                        }
                        else
                        {
                            if (Tipo.Equals(Renglon["TIPO_PAGO_FINANZAS"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()))
                            {
                                Monto = Monto + Convert.ToDouble(Renglon["MONTO"].ToString());
                            }
                        }
                    }
                    if (Insertar && Monto > 0)
                    {
                        row["TIPO"] = Tipo;
                        row["MONTO_TIPO"] = Monto;
                        row["FORMA_PAGO"] = Forma_Pago;
                        row["IDENTIFICADOR_TIPO"] = Forma_Pago + Tipo + Monto;
                        Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Modificada.AcceptChanges();
                    }
                }
                Session["Dt_Datos_Detalles"] = Temporal;
                Session["Dt_Datos"] = Dt_Modificada;
                Grid_Pagos.DataSource = Dt_Modificada;
                Grid_Pagos.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "No. Pago", "alert('El numero de Pago que busca No se pago con cheque ');", true);
                Session[P_Dt_Solicitud] = null;
                Grid_Pagos.DataSource = null;
                Grid_Pagos.DataBind();
            }
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Pagos_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView Gv_Detalles = new GridView();
        String Banco_Pago="";
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Dt_Datos = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Modificada = new DataTable();
        DataTable Dt_Datos_Detalles_Proveedor = new DataTable();
        Dt_Datos_Detalles_Proveedor = ((DataTable)(Session["Dt_Datos_Detalles_Proveedor"]));
        DataTable Dt_Datos_Proveedor = new DataTable();
        Dt_Datos_Proveedor = ((DataTable)(Session["Dt_Datos_Proveedor"]));
        Int32 Contador_Proveedor = 0;
        Session["Contador_Proveedor"] = Contador_Proveedor;
        String Proveedor_ID = "";
        String Tipo = "";
        String Forma_Pago = "";
        Boolean Agregar;
        Double Total;
        Int32 Contador;
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Image Img = new Image();
        Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
        Literal Lit = new Literal();
        Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
        Label Lbl_facturas = new Label();
        Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos");
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador = (Int32)(Session["Contador"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                foreach(DataRow Filas in Dt_Datos_Detalles.Rows)
                {
                    Proveedor_ID = Filas["Proveedor_id"].ToString();
                    if (Proveedor_ID == "")
                    {
                        Proveedor_ID = Filas["Empleado_id"].ToString();
                        Tipo = Convert.ToString(Dt_Datos.Rows[Contador]["Tipo"].ToString());
                        Forma_Pago = Convert.ToString(Dt_Datos.Rows[Contador]["Forma_Pago"].ToString());
                        Banco_Pago=Convert.ToString(Dt_Datos.Rows[Contador]["Banco_Pago"].ToString());
                        if (RBL_Orden.SelectedItem.Text.ToString() == "Banco")
                        {
                            Dt_Datos_Detalles.DefaultView.RowFilter = "Banco='" + Tipo + "' and Empleado_Id='" + Proveedor_ID + "' and Forma_Pago='" + Forma_Pago + "'";
                        }
                        else
                        {
                            Dt_Datos_Detalles.DefaultView.RowFilter = "Tipo_Pago_Finanzas='" + Tipo + "' and Empleado_Id='" + Proveedor_ID + "' and Forma_Pago='" + Forma_Pago + "'";
                        }

                        if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                        {
                            Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Proveedores");
                            foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                            {
                                if (Dt_Modificada.Rows.Count <= 0 && Dt_Modificada.Columns.Count <= 0)
                                {
                                    Dt_Modificada.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                                    Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                                    Dt_Modificada.Columns.Add("TIPO_SOLICITUD_PAGO_ID", typeof(System.String));
                                    Dt_Modificada.Columns.Add("TIPO_PAGO", typeof(System.String));
                                    Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                                    Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                                    Dt_Modificada.Columns.Add("TIPO", typeof(System.String));
                                    Dt_Modificada.Columns.Add("FORMA_PAGO", typeof(System.String));
                                    Dt_Modificada.Columns.Add("IDENTIFICADOR", typeof(System.String));
                                    Dt_Modificada.Columns.Add("BANCO_PAGO", typeof(System.String));
                                }
                                DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla

                                Agregar = true;
                                Total = 0;
                                //insertar los que no se repiten
                                foreach (DataRow Fila2 in Dt_Modificada.Rows)
                                {
                                    if (Proveedor_ID.Equals(Fila2["PROVEEDOR_ID"].ToString()) && Forma_Pago.Equals(Fila2["Forma_Pago"].ToString()))
                                    {
                                        Agregar = false;
                                    }
                                }
                                //se calcula el monto por tipo
                                foreach (DataRow Renglon in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                                {
                                    if (Proveedor_ID.Equals(Renglon["EMPLEADO_ID"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()))
                                    {
                                        Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                    }
                                }
                                if (Agregar && Total > 0)
                                {
                                    row["PROVEEDOR_ID"] = Proveedor_ID;
                                    row["BENEFICIARIO"] = Fila["EMPLEADO"].ToString();
                                    row["TIPO_SOLICITUD_PAGO_ID"] = Fila["EMPLEADO"].ToString();
                                    row["TIPO_PAGO"] = Fila["Tipo_Pago_Finanzas"].ToString();
                                    row["MONTO"] = Total;
                                    row["TIPO"] = Tipo;
                                    row["FORMA_PAGO"] = Forma_Pago;
                                    row["ESTATUS"] = Fila["Estatus"].ToString();
                                    row["IDENTIFICADOR"] = Proveedor_ID + Tipo + Forma_Pago;
                                    row["BANCO_PAGO"] = Banco_Pago;
                                    Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Modificada.AcceptChanges();
                                }
                            }
                        }
                    }
                    else
                    {
                        Tipo = Convert.ToString(Dt_Datos.Rows[Contador]["Tipo"].ToString());
                        Forma_Pago = Convert.ToString(Dt_Datos.Rows[Contador]["Forma_Pago"].ToString());
                        Banco_Pago =Convert.ToString(Dt_Datos.Rows[Contador]["Banco_Pago"].ToString());
                        if (RBL_Orden.SelectedItem.Text.ToString() == "Banco")
                        {
                            Dt_Datos_Detalles.DefaultView.RowFilter = "Banco='" + Tipo + "' and proveedor_id='" + Proveedor_ID + "' and Forma_Pago='" + Forma_Pago + "'";
                        }
                        else
                        {
                            Dt_Datos_Detalles.DefaultView.RowFilter = "Tipo_Pago_Finanzas='" + Tipo + "' and proveedor_id='" + Proveedor_ID + "' and Forma_Pago='" + Forma_Pago + "'";
                        }

                        if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                        {
                            Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Proveedores");
                            foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                            {
                                if (Dt_Modificada.Rows.Count <= 0 && Dt_Modificada.Columns.Count<=0)
                                {
                                    Dt_Modificada.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                                    Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                                    Dt_Modificada.Columns.Add("TIPO_SOLICITUD_PAGO_ID", typeof(System.String));
                                    Dt_Modificada.Columns.Add("TIPO_PAGO", typeof(System.String));
                                    Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                                    Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                                    Dt_Modificada.Columns.Add("TIPO", typeof(System.String));
                                    Dt_Modificada.Columns.Add("FORMA_PAGO", typeof(System.String));
                                    Dt_Modificada.Columns.Add("IDENTIFICADOR", typeof(System.String));
                                    Dt_Modificada.Columns.Add("BANCO_PAGO", typeof(System.String));
                                }
                                DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla

                                Agregar = true;
                                Total = 0;
                                //insertar los que no se repiten
                                foreach (DataRow Fila2 in Dt_Modificada.Rows)
                                {
                                    if (Proveedor_ID.Equals(Fila2["Proveedor_ID"].ToString()) && Forma_Pago.Equals(Fila2["Forma_Pago"].ToString()))
                                    {
                                        Agregar = false;
                                    }
                                }
                                //se calcula el monto por tipo
                                foreach (DataRow Renglon in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                                {
                                    if (Proveedor_ID.Equals(Renglon["Proveedor_ID"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()))
                                    {
                                        Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                    }
                                }
                                if (Agregar && Total > 0)
                                {
                                    row["PROVEEDOR_ID"] = Proveedor_ID;
                                    row["BENEFICIARIO"] = Fila["Proveedor"].ToString();
                                    row["TIPO_SOLICITUD_PAGO_ID"] = Fila["Proveedor"].ToString();
                                    row["TIPO_PAGO"] = Fila["Tipo_Pago_Finanzas"].ToString();
                                    row["MONTO"] = Total;
                                    row["TIPO"] = Tipo;
                                    row["FORMA_PAGO"] = Forma_Pago;
                                    row["ESTATUS"] = Fila["Estatus"].ToString();
                                    row["IDENTIFICADOR"] = Proveedor_ID + Tipo + Forma_Pago;
                                    row["BANCO_PAGO"] = Banco_Pago;
                                    Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Modificada.AcceptChanges();
                                }
                            }
                        }
                    }
                }
                Session["Dt_Datos_Detalles_Proveedor"] = Dt_Datos_Detalles;
                Session["Dt_Datos_Proveedor"] = Dt_Modificada;
                Gv_Detalles.Columns[6].Visible = true;
                Gv_Detalles.Columns[3].Visible = true;
                Gv_Detalles.DataSource = Dt_Modificada;
                Gv_Detalles.DataBind();
                Gv_Detalles.Columns[6].Visible = false;
                Session["Contador"] = Contador + 1;
                if (Cmb_Forma_Pago.SelectedItem.Text == "Transferencia")
                {
                    Gv_Detalles.Columns[7].Visible = false;
                    Gv_Detalles.Columns[4].Visible = false;
                }
                else
                {
                    Gv_Detalles.Columns[7].Visible = true;
                    Gv_Detalles.Columns[4].Visible = true;
                }

            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Proveedores_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Proveedores_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView Gv_Detalles = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Dt_Datos = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Modificada = new DataTable();
        String Proveedor_ID = "";
        String Tipo = "";
        String No_Solicitud = "";
        Int32 Contador_Proveedor;
        String Forma_Pago = "";
        String Banco_Pago = "";
        String Banco_Pago_Real = "";
        DataTable Dt_Existencia_Cuenta = new DataTable();
        Cls_Cat_Nom_Bancos_Negocio Rs_Bancos = new Cls_Cat_Nom_Bancos_Negocio();
        TextBox Txt = new TextBox(); 
        TextBox Txt_No_Cheque = new TextBox();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Banco_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
       // DropDownList Cmb2 = new DropDownList();
        DataTable Dt_Banco = new DataTable();
        DataTable Dt_Existencia = new DataTable();
        Cls_Cat_Nom_Bancos_Negocio Rs_Bancos_Solicitud = new Cls_Cat_Nom_Bancos_Negocio();
        DataTable Dt_Forma_Pago = new DataTable();
        Dt_Forma_Pago = ((DataTable)(Session["Dt_Forma_Pago"]));
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Image Img = new Image();
        Img = (Image)e.Row.FindControl("Img_Btn_Expandir_Proveedor");
        Literal Lit = new Literal();
        Lit = (Literal)e.Row.FindControl("Ltr_Inicio2");
        Label Lbl_facturas = new Label();
        Lbl_facturas = (Label)e.Row.FindControl("Lbl_Solicitud_proveedores");
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador_Proveedor = (Int32)(Session["Contador_Proveedor"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos = ((DataTable)(Session["Dt_Datos_Proveedor"]));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles_Proveedor"]));
                foreach (DataRow Filas in Dt_Datos_Detalles.Rows)
                {
                    No_Solicitud = Filas["NO_SOLICITUD_PAGO"].ToString();
                    Proveedor_ID = Dt_Datos.Rows[Contador_Proveedor]["PROVEEDOR_ID"].ToString();
                    Tipo = Convert.ToString(Dt_Datos.Rows[Contador_Proveedor]["Tipo"].ToString());
                    Forma_Pago = Convert.ToString(Dt_Datos.Rows[Contador_Proveedor]["Forma_Pago"].ToString());
                    Banco_Pago = Filas["Banco_Pago"].ToString();// Convert.ToString(Dt_Datos.Rows[Contador_Proveedor]["Banco_Pago"].ToString()); //Dt_Datos_Detalles.DefaultView.ToTable().Rows[0]["Banco_Pago"].ToString();
                    Banco_Pago_Real = Convert.ToString(Dt_Datos.Rows[Contador_Proveedor]["Banco_Pago"].ToString());
                    if (RBL_Orden.SelectedItem.Text.ToString() == "Banco")
                    {
                        if (Proveedor_ID.Substring(0, 1) == "E")
                        {
                            Dt_Datos_Detalles.DefaultView.RowFilter = "Banco='" + Tipo + "' and Empleado_ID='" + Proveedor_ID + "' and No_Solicitud_Pago='" + No_Solicitud + "' and Forma_Pago='" + Forma_Pago + "'";
                        }
                        else
                        {
                            Dt_Datos_Detalles.DefaultView.RowFilter = "Banco='" + Tipo + "' and proveedor_id='" + Proveedor_ID + "' and No_Solicitud_Pago='" + No_Solicitud + "' and Forma_Pago='" + Forma_Pago + "'";
                        }
                    }
                    else
                    {
                        if (Proveedor_ID.Substring(0, 1) == "E")
                        {
                            Dt_Datos_Detalles.DefaultView.RowFilter = "Banco='" + Tipo + "' and Empleado_ID='" + Proveedor_ID + "' and No_Solicitud_Pago='" + No_Solicitud + "' and Forma_Pago='" + Forma_Pago + "'";
                        }
                        else
                        {
                            Dt_Datos_Detalles.DefaultView.RowFilter = "Tipo_Pago_Finanzas='" + Tipo + "' and proveedor_id='" + Proveedor_ID + "' and No_Solicitud_Pago='" + No_Solicitud + "' and Forma_Pago='" + Forma_Pago + "'";
                        }
                    }
                    
                    if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Datos_Solicitud");
                        foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                        {
                            if (Dt_Modificada.Rows.Count <= 0)
                            {
                                Dt_Modificada.Columns.Add("NO_SOLICITUD", typeof(System.String));
                                Dt_Modificada.Columns.Add("NO_RESERVA", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO_SOLICITUD_PAGO_ID", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO_PAGO", typeof(System.String));
                                Dt_Modificada.Columns.Add("BANCO", typeof(System.String));
                                Dt_Modificada.Columns.Add("CONCEPTO", typeof(System.String));
                                Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                                Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                                Dt_Modificada.Columns.Add("FORMA_PAGO", typeof(System.String));
                                Dt_Modificada.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                            }
                            DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            row["NO_SOLICITUD"] = Fila["NO_SOLICITUD_PAGO"].ToString();
                            row["NO_RESERVA"] = Fila["NO_RESERVA"].ToString();
                            row["TIPO_SOLICITUD_PAGO_ID"] = Fila["TIPO_SOLICITUD_PAGO_ID"].ToString();
                            row["TIPO_PAGO"] = Fila["TIPO_PAGO_FINANZAS"].ToString();
                            row["MONTO"] = Fila["MONTO"].ToString();
                            row["BANCO"] = Fila["BANCO"].ToString();
                            row["CONCEPTO"] = Fila["CONCEPTO"].ToString();
                            row["ESTATUS"] = Fila["ESTATUS"].ToString();
                            row["FORMA_PAGO"] = Forma_Pago;
                            row["PROVEEDOR_ID"] = Proveedor_ID;
                            Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificada.AcceptChanges();
                        }
                        Session["Dt_Forma_Pago"] = Dt_Modificada;
                        Gv_Detalles.Columns[2].Visible = true;
                        Gv_Detalles.Columns[3].Visible = true;
                        Gv_Detalles.Columns[9].Visible = true;
                        Gv_Detalles.Columns[10].Visible = true;
                        Gv_Detalles.Columns[8].Visible = true;
                        Gv_Detalles.Columns[7].Visible = true;
                        //Gv_Detalles.Columns[6].Visible = true;
                        Gv_Detalles.DataSource = Dt_Modificada;
                        Gv_Detalles.DataBind();
                        Gv_Detalles.Columns[2].Visible = false;
                        Gv_Detalles.Columns[3].Visible = false;
                        if (Cmb_Forma_Pago.SelectedItem.Text == "Transferencia")
                        {
                            Gv_Detalles.Columns[8].Visible = true;
                            Gv_Detalles.Columns[9].Visible = false;
                            Gv_Detalles.Columns[10].Visible = true;
                            Gv_Detalles.Columns[7].Visible = false;
                        }
                        else
                        {
                            Gv_Detalles.Columns[8].Visible = false;
                            Gv_Detalles.Columns[9].Visible = true;
                            Gv_Detalles.Columns[10].Visible = false;
                            Gv_Detalles.Columns[7].Visible = true;
                        }
                        //Gv_Detalles.Columns[6].Visible = false;
                    }
                }
                Session["Contador_Proveedor"] = Contador_Proveedor + 1;
                //llenar el combon de bancos
                //DataTable Dt_Bancos = Bancos_Negocio.Consulta_Bancos();

                ///consultamos el Banco_Pago del Pago
                Rs_Bancos_Solicitud.P_Banco_ID = Banco_Pago_Real;
                Dt_Existencia = Rs_Bancos_Solicitud.Consulta_Bancos();
                if (Dt_Existencia.Rows.Count > 0)
                {
                    Txt = (TextBox)e.Row.Cells[4].FindControl("Txt_Cuenta");
                    Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Dt_Existencia.Rows[0]["NOMBRE"].ToString(); //Cmb.SelectedItem.Text.ToString();
                    Dt_Existencia_Cuenta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                    Txt.Text = Dt_Existencia_Cuenta.Rows[0]["CUENTA"].ToString();
                    Txt.CssClass = Dt_Existencia_Cuenta.Rows[0]["BANCO_ID"].ToString();
                    Txt.Text = Txt.Text.Replace(" ", "");
                    Txt.Enabled = false;
                    Txt_No_Cheque = (TextBox)e.Row.Cells[5].FindControl("Txt_No_Cheque");
                    Txt_No_Cheque.Text = Dt_Existencia.Rows[0][Cat_Nom_Bancos.Campo_Folio_Actual].ToString();
                    Txt_No_Cheque.Enabled=false;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Solicitudes_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Solicitudes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        TextBox Txt_Fecha;
        DataTable Dt_Datos = new DataTable();
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Dt_Datos = ((DataTable)(Session["Dt_Forma_Pago"]));
                if (Dt_Datos.Rows[0]["Forma_Pago"].ToString() == "Transferencia")
                {
                    e.Row.Cells[0].Enabled = false;
                    e.Row.Cells[8].Enabled = true;
                    e.Row.Cells[10].Enabled = true;
                    Txt_Fecha = (TextBox)e.Row.Cells[8].FindControl("Txt_Fecha_Transferencia");
                    Txt_Fecha.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                }
                else
                {
                    if (Dt_Datos.Rows[0]["Forma_Pago"].ToString() == "Cheque")
                    {
                        e.Row.Cells[0].Enabled = true;
                        e.Row.Cells[8].Enabled = false;
                        e.Row.Cells[10].Enabled = false;
                    }
                }
                
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Chk_Autorizado_OnCheckedChanged
    // DESCRIPCIÓN: manipulacion de los checks
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 24/Junio/2013 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    protected void Chk_Autorizado_OnCheckedChanged(object sender, EventArgs e)
    {
        Int32 Indice = 0;
        Int32 Indice_Solicitudes = 0; 
        Boolean Autorizado= false;
        String Proveedor_ID = ((CheckBox)sender).CssClass;
        Txt_Total_Cheque.Text = "0";
        foreach (GridViewRow Renglon_Grid in Grid_Pagos.Rows)
        {
            GridView Grid_Proveedores = (GridView)Renglon_Grid.FindControl("Grid_Proveedores");
            foreach (GridViewRow Renglon in Grid_Proveedores.Rows)
            {
                Indice++;
                Grid_Proveedores.SelectedIndex = Indice;
                Autorizado = ((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).Checked;
                if (Autorizado)
                {
                        GridView Grid_Solicitudes_Pago = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                        foreach (GridViewRow Fila in Grid_Solicitudes_Pago.Rows)
                        {
                            Indice_Solicitudes++;
                            Grid_Solicitudes_Pago.SelectedIndex = Indice;
                            if (((CheckBox)Fila.FindControl("Chk_Cheque")).ToolTip == Proveedor_ID)
                            {
                                ((CheckBox)Fila.FindControl("Chk_Cheque")).Checked = true;
                                Txt_Total_Cheque.Text = (Convert.ToDecimal(Txt_Total_Cheque.Text) + Convert.ToDecimal(Fila.Cells[6].Text.Replace(",", "").Replace("$", ""))).ToString();
                            }
                        }
                }
                else
                {
                    GridView Grid_Solicitudes_Pago = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                    foreach (GridViewRow Fila in Grid_Solicitudes_Pago.Rows)
                    {
                        Indice_Solicitudes++;
                        Grid_Solicitudes_Pago.SelectedIndex = Indice;
                        if (((CheckBox)Fila.FindControl("Chk_Cheque")).ToolTip == Proveedor_ID)
                        {
                            ((CheckBox)Fila.FindControl("Chk_Cheque")).Checked = false;
                        }
                    }
                }
            }
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Chk_Cheque_Solicitud_OnCheckedChanged
    // DESCRIPCIÓN: manipulacion de los checks
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 24/Junio/2013 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    protected void Chk_Cheque_Solicitud_OnCheckedChanged(object sender, EventArgs e)
    {
        Int32 Indice = 0;
        Int32 Indice_Solicitudes = 0;
        Boolean Autorizado= false;
        Boolean Otro_Seleccionado = false;
        String Proveedor_ID = ((CheckBox)sender).ToolTip;
        Txt_Total_Cheque.Text = "0";
        foreach (GridViewRow Renglon_Grid in Grid_Pagos.Rows)
        {
            GridView Grid_Proveedores = (GridView)Renglon_Grid.FindControl("Grid_Proveedores");
            foreach (GridViewRow Renglon in Grid_Proveedores.Rows)
            {
                Indice++;
                Grid_Proveedores.SelectedIndex = Indice;
                Autorizado = ((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).Checked;
                if (Autorizado)
                {
                    GridView Grid_Solicitudes_Pago = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                    foreach (GridViewRow Fila in Grid_Solicitudes_Pago.Rows)
                    {
                        Indice_Solicitudes++;
                        Grid_Solicitudes_Pago.SelectedIndex = Indice;
                        if (((CheckBox)Fila.FindControl("Chk_Cheque")).Checked == true)
                        {
                            Otro_Seleccionado = true;
                            Txt_Total_Cheque.Text = (Convert.ToDecimal(Txt_Total_Cheque.Text) + Convert.ToDecimal(Fila.Cells[6].Text.Replace(",","").Replace("$",""))).ToString();
                        }
                    }
                    if (!Otro_Seleccionado)
                    {
                        if (((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).CssClass == Proveedor_ID)
                        {
                            ((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).Checked = false;
                            Txt_Total_Cheque.Text = "0";
                        }
                    }
                }
                else
                {
                    if(((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).CssClass==Proveedor_ID){
                        ((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).Checked = true;
                    }
                }
            }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Evento_Grid_Solicitudes_Seleccionar
    ///DESCRIPCIÓN:
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Evento_Grid_Solicitudes_Seleccionar(String Dato)
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de coneción hacia la capa de datos
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();        
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Cat_Nom_Bancos_Negocio Rs_Banco_Pago = new Cls_Cat_Nom_Bancos_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Banco_Pago = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
         DataTable Dt_Consulta = new DataTable();
         DataTable Dt_Consulta_Cuentas = new DataTable();
        DataTable Dt_Solicitud_Pagos = new DataTable(); //Variable a contener los datos de la consulta de la solicitud de pago
        DataTable Dt_Cuenta = new DataTable(); //Variable a contener los datos de la consulta
        DataTable Dt_Solicitud_Detalles = new DataTable();
        Habilitar_Controles("Inicial");
        Llenar_Combos_Generales();
        //Btn_Imprimir_Excel.Visible = false;
        Btn_Modificar.ToolTip = "Cancelar Pago";
        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
        Btn_Modificar.Visible = false;
        Div_Datos_Solicitud.Style.Add("display", "block");
        Div_Solicitudes_Pendientes.Style.Add("display", "none");
        Int32 Folio_Actual = 0;
        Int32 Folio_Final = 0;
        Boolean Estado_Folio = false;
        Txt_Fecha_No_Pago.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
        Cls_Ope_Con_Cheques_Negocio Cheques = new Cls_Ope_Con_Cheques_Negocio();
        Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
        Cls_Ope_Con_Reservas_Negocio Reserva_Negocio = new Cls_Ope_Con_Reservas_Negocio();
        String No_Solicitud = Dato;//Grid_Requisiciones.SelectedDataKey["No_Requisicion"].ToString();
        DataRow[] Solicitud = ((DataTable)Session[P_Dt_Solicitud]).Select("NO_SOLICITUD_PAGO = '" + No_Solicitud + "'");
        Txt_No_Solicitud.Text = Solicitud[0]["NO_SOLICITUD_PAGO"].ToString();
        String Fecha = Solicitud[0]["FECHA_SOLICITUD"].ToString();
        Fecha = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Fecha));
        Txt_Fecha.Text = Fecha.ToUpper();
        //Seleccionar combo dependencia
        Txt_Tipo_Solicitud_Pago.Text =
            Solicitud[0]["TIPO_PAGO"].ToString().Trim();
        Txt_MesAnio.Text =
            Solicitud[0]["MES_ANO"].ToString();
        //Total de la requisición
        Txt_No_Reserva_Solicitud.Text =
            Solicitud[0]["NO_RESERVA"].ToString();
        //Se llena el combo partidas
        Txt_Estatus_Solicitud.Text =
            Solicitud[0]["ESTATUS"].ToString();
        Txt_Concepto.Text =
            Solicitud[0]["CONCEPTO"].ToString();
        String Monto = Solicitud[0]["MONTO"].ToString();
        Monto = String.Format("{0:c}", Monto);
        Txt_Monto.Text = Monto;
        Txt_No_Poliza.Text =
             Solicitud[0]["NO_POLIZA"].ToString();
        if (!String.IsNullOrEmpty(Solicitud[0]["BENEFICIARIO"].ToString()))
        {
            Txt_Beneficiario_Pago.Text =
                Solicitud[0]["BENEFICIARIO"].ToString();
        }
        else
        {
            Txt_Beneficiario_Pago.Text =
                Solicitud[0]["EMPLEADO"].ToString();
        }
        Btn_Nuevo.Visible = true;
        Txt_Comentario_Pago.Text = Solicitud[0]["CONCEPTO"].ToString();
        //se consulta el banco y la cuenta con la que se pagara la solicitud
        Rs_Banco_Pago.P_Banco_ID = Solicitud[0]["Banco_Pago"].ToString();
        Dt_Banco_Pago = Rs_Banco_Pago.Consulta_Bancos();
        if (Dt_Banco_Pago.Rows.Count > 0)
        {
            //se selecciona el banco que tiene asignada la fuente de financiamiento para pagar
            Cmb_Banco.SelectedValue = Dt_Banco_Pago.DefaultView.ToTable().Rows[0]["NOMBRE"].ToString();
            // se llena el combo de cuentas con las cuentas del banco que se selecciono anteriormente
            Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
            Dt_Consulta_Cuentas = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Cuenta_Banco, Dt_Consulta_Cuentas, "CUENTA", "BANCO_ID");
            Dt_Consulta_Cuentas.DefaultView.RowFilter = "BANCO_ID=" + Solicitud[0]["Banco_Pago"].ToString();
            if (Dt_Consulta_Cuentas.DefaultView.ToTable().Rows.Count > 0)
            {
                //Se selecciona la cuenta del banco que tiene la fuente de financiamiento para pagar
                Cmb_Cuenta_Banco.SelectedValue = Solicitud[0]["Banco_Pago"].ToString();
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Txt_No_Cheque.Text = "";
                Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();
                //  obtendra el numero de folio de los cheques
                if (Dt_Consulta.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                            Folio_Actual = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()));

                        else
                            Estado_Folio = true;

                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()))
                            Folio_Final = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()));

                        else
                            Estado_Folio = true;
                    }
                }
                //  si contiene informacion
                if (Estado_Folio == false)
                {
                    if (Folio_Actual <= Folio_Final)
                        Txt_No_Cheque.Text = "" + Folio_Actual;
                    //  si el folio se pasa del final manda un mensaje
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Se terminaron los folios del Talonario de cheques actual por favor ingrese nuevos folios";
                    }
                }
                //  no tiene asignado los numeros de folio de los chuques
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Por favor defina el rango de los folio del Talonario de cheques para poder utilizarlos en esta ventana";
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "La cuenta bancaria no tiene asignada cuenta contable favor de agregarla en el catalogo de bancos";
            }
        }
        else
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "La cuenta bancaria no tiene asignada cuenta contable favor de agregarla en el catalogo de bancos";
        }
        Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud;
        Dt_Solicitud_Detalles = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Detalles_Solicitud();
        if (Dt_Solicitud_Detalles.Rows.Count > 0)
        {
            foreach (DataRow Fila in Dt_Solicitud_Detalles.Rows)
            {
                Grid_No_Documentos.Columns[4].Visible = true;
                Grid_No_Documentos.Columns[5].Visible = true;
                Grid_No_Documentos.DataSource = Dt_Solicitud_Detalles; //Agrega los valores de todas las partidas que se tienen al grid
                Grid_No_Documentos.DataBind();
                Grid_No_Documentos.Columns[4].Visible = false;
                Grid_No_Documentos.Columns[5].Visible = false;
            }
        }
        Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Solicitud[0]["NO_SOLICITUD_PAGO"].ToString();
        Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
        foreach (DataRow Registro in Dt_Solicitud_Pagos.Rows)
        {
            if (!String.IsNullOrEmpty(Registro["EMPLEADO_CUENTA"].ToString()))
            {
                Txt_Cuenta_Contable_Proveedor.Value = Registro["EMPLEADO_CUENTA"].ToString();
                Txt_Cuenta.Text = Registro["EMPLEADO_CUENTA"].ToString();
            }
            else
            {
                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Acreedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "A")
                {
                    Txt_Cuenta_Contable_Proveedor.Value = Registro["Pro_Cuenta_Acreedor"].ToString();
                    Txt_Cuenta.Text = Registro["Pro_Cuenta_Acreedor"].ToString();
                }
                else
                {
                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Contratista"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "C")
                    {
                        Txt_Cuenta_Contable_Proveedor.Value = Registro["Pro_Cuenta_Contratista"].ToString();
                        Txt_Cuenta.Text = Registro["Pro_Cuenta_Contratista"].ToString();
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Deudor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "D")
                        {
                            Txt_Cuenta_Contable_Proveedor.Value = Registro["Pro_Cuenta_Deudor"].ToString();
                            Txt_Cuenta.Text = Registro["Pro_Cuenta_Deudor"].ToString();
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Judicial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "J")
                            {
                                Txt_Cuenta_Contable_Proveedor.Value = Registro["Pro_Cuenta_Judicial"].ToString();
                                Txt_Cuenta.Text = Registro["Pro_Cuenta_Judicial"].ToString();
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Nomina"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "N")
                                {
                                    Txt_Cuenta_Contable_Proveedor.Value = Registro["Pro_Cuenta_Nomina"].ToString();
                                    Txt_Cuenta.Text = Registro["Pro_Cuenta_Nomina"].ToString();
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Predial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "Z")
                                    {
                                        Txt_Cuenta_Contable_Proveedor.Value = Registro["Pro_Cuenta_Predial"].ToString();
                                        Txt_Cuenta.Text = Registro["Pro_Cuenta_Predial"].ToString();
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Proveedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                                        {
                                            Txt_Cuenta_Contable_Proveedor.Value = Registro["Pro_Cuenta_Proveedor"].ToString();
                                            Txt_Cuenta.Text = Registro["Pro_Cuenta_Proveedor"].ToString();
                                        }
                                        else
                                        {
                                            if (Registro["Tipo_Solicitud_Pago_ID"].ToString() == "00008" && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                                            {
                                                Rs_Cuentas_Proveedor.P_Cuenta = "211200001";
                                                Dt_C_Proveedor = Rs_Cuentas_Proveedor.Consulta_Existencia_Cuenta_Contable();
                                                if (Dt_C_Proveedor.Rows.Count > 0)
                                                {
                                                    Txt_Cuenta_Contable_Proveedor.Value = Dt_C_Proveedor.Rows[0]["Cuenta_Contable_ID"].ToString();
                                                    Txt_Cuenta.Text = Dt_C_Proveedor.Rows[0]["Cuenta"].ToString();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (!String.IsNullOrEmpty(Registro["Tipo_Solicitud_Pago_ID"].ToString()))
            {
                Txt_Tipo_Solicitud.Value = Registro["Tipo_Solicitud_Pago_ID"].ToString();
            }
            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Txt_Cuenta.Text;
            Dt_Cuenta = Rs_Consulta_Con_Cuentas_Contables.Consulta_Cuentas_Contables();
            foreach (DataRow fila in Dt_Cuenta.Rows)
            {
                Txt_Cuenta.Text = fila["CUENTA"].ToString();
            }
        }
        if (Txt_Estatus_Solicitud.Text == "PAGADO")
        {
            Cheques.P_No_Solicitud_Pago = No_Solicitud;
            Cheques.P_Estatus = "PAGADO";
            DataTable Pago = Cheques.Consulta_Datos_Pago();
            if (Pago.Rows.Count > 0)
            {
                Txt_No_Pago.Text =
                Pago.Rows[0]["NO_PAGO"].ToString().Trim();
                Txt_Fecha.Text =
                 string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Pago.Rows[0]["FECHA_PAGO"].ToString()));
                Cmb_Tipo_Pago.SelectedValue = Pago.Rows[0]["FORMA_PAGO"].ToString().Trim();
                Cmb_Estatus.SelectedValue = Pago.Rows[0]["ESTATUS"].ToString().Trim();
                if (Pago.Rows[0]["ESTATUS"].ToString().Trim() == "CANCELADO")
                {
                    Txt_Motivo_Cancelacion.Text =
                   Pago.Rows[0]["MOTIVO_CANCELACION"].ToString();
                }
                Txt_No_Cheque.Text =
                    Pago.Rows[0]["NO_CHEQUE"].ToString();
                Txt_Referencia_Pago.Text =
                    Pago.Rows[0]["REFERENCIA_TRANSFERENCIA_BANCA"].ToString();
                Txt_Comentario_Pago.Text =
                    Pago.Rows[0]["COMENTARIOS"].ToString();
                Txt_Beneficiario_Pago.Text =
                    Pago.Rows[0]["BENEFICIARIO_PAGO"].ToString();
                Cmb_Banco.SelectedValue = Pago.Rows[0]["NOMBRE"].ToString();
                Txt_Cuenta_Contable_ID_Banco.Value = Pago.Rows[0]["CUENTA_CONTABLE_ID"].ToString();
            }
            Btn_Modificar.Visible = true;
            Cmb_Estatus.Enabled = true;
        }
        //se comento porque se cambio la jugada
        //Consultar_Fuente_Banco(Txt_No_Reserva_Solicitud.Text);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Fuente_Banco
    ///DESCRIPCIÓN:Se consulta la reserva para ver que fuente de financiamiento esta asignada
    ///             y asi saber de que banco se puede pagar esta solicitud
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consultar_Fuente_Banco(String  Reserva)
    {
        Cls_Ope_Con_Reservas_Negocio Rs_Reservas = new Cls_Ope_Con_Reservas_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Datos_Reserva = new DataTable();
        DataTable Dt_Bancos = new DataTable();
        DataTable Dt_Consulta_Cuentas = new DataTable();
        String Fuente_Financiamiento="";
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        Int32 Folio_Actual = 0;
        Int32 Folio_Final = 0;
        Boolean Estado_Folio = false;
        try
        {
            Rs_Reservas.P_No_Reserva = Reserva;
            Dt_Datos_Reserva = Rs_Reservas.Consultar_Reservas_Detalladas();
            if (Dt_Datos_Reserva.Rows.Count > 0)
            {
                Fuente_Financiamiento = Dt_Datos_Reserva.Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString();
                if (!String.IsNullOrEmpty(Fuente_Financiamiento))
                {
                    Dt_Bancos=Rs_Bancos.Consultar_Bancos();
                    if (Dt_Bancos.Rows.Count > 0)
                    {
                        Dt_Bancos.DefaultView.RowFilter = "FUENTE_FINANCIAMIENTO_ID=" + Fuente_Financiamiento;
                        if (Dt_Bancos.DefaultView.ToTable().Rows.Count > 0)
                        {
                            //se selecciona el banco que tiene asignada la fuente de financiamiento para pagar
                            Cmb_Banco.SelectedValue = Dt_Bancos.DefaultView.ToTable().Rows[0]["NOMBRE"].ToString();
                            // se llena el combo de cuentas con las cuentas del banco que se selecciono anteriormente
                            Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
                            Dt_Consulta_Cuentas = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Cuenta_Banco, Dt_Consulta_Cuentas, "CUENTA", "BANCO_ID");
                            Dt_Consulta_Cuentas.DefaultView.RowFilter = "BANCO_ID=" + Dt_Bancos.DefaultView.ToTable().Rows[0]["BANCO_ID"].ToString();
                            if (Dt_Consulta_Cuentas.DefaultView.ToTable().Rows.Count > 0)
                            {
                                //Se selecciona la cuenta del banco que tiene la fuente de financiamiento para pagar
                                Cmb_Cuenta_Banco.SelectedValue = Dt_Bancos.DefaultView.ToTable().Rows[0]["BANCO_ID"].ToString();
                                Lbl_Mensaje_Error.Visible = false;
                                Img_Error.Visible = false;
                                Txt_No_Cheque.Text = "";
                                Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                                Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();
                                //  obtendra el numero de folio de los cheques
                                if (Dt_Consulta.Rows.Count > 0)
                                {
                                    foreach (DataRow Registro in Dt_Consulta.Rows)
                                    {
                                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                                            Folio_Actual = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()));

                                        else
                                            Estado_Folio = true;

                                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()))
                                            Folio_Final = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()));

                                        else
                                            Estado_Folio = true;
                                    }
                                }
                                //  si contiene informacion
                                if (Estado_Folio == false)
                                {
                                    if (Folio_Actual <= Folio_Final)
                                        Txt_No_Cheque.Text = "" + Folio_Actual;
                                    //  si el folio se pasa del final manda un mensaje
                                    else
                                    {
                                        Lbl_Mensaje_Error.Visible = true;
                                        Img_Error.Visible = true;
                                        Lbl_Mensaje_Error.Text = "Se terminaron los folios del Talonario de cheques actual por favor ingrese nuevos folios";
                                    }
                                }
                                //  no tiene asignado los numeros de folio de los chuques
                                else
                                {
                                    Lbl_Mensaje_Error.Visible = true;
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Text = "Por favor defina el rango de los folio del Talonario de cheques para poder utilizarlos en esta ventana";
                                }
                            }
                            else
                            {
                                Lbl_Mensaje_Error.Visible = true;
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Text = "La cuenta bancaria no tiene asignada cuenta contable favor de agregarla en el catalogo de bancos";
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Consultar la fuente de financiamiento para conocer el banco " + ex.Message.ToString(), ex);
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Combos_Generales()
    // DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 17/Noviembre/2011 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Combos_Generales()
    {
        Cls_Cat_Nom_Bancos_Negocio Bancos_Negocio = new Cls_Cat_Nom_Bancos_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        //DataTable Dt_Bancos = Bancos_Negocio.Consulta_Bancos();
        DataTable Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
        Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Banco, Dt_Existencia, "NOMBRE", "NOMBRE");
        Consultar_Tipos_Solicitud();
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Tipos_Solicitud
    /// DESCRIPCION : Llena el Cmb_Tipo_Solicitud_Pago con los tipos de solicitud de pago.
    /// PARAMETROS  : 
    /// CREO        : Yazmin Abigail Delgado Gómez
    /// FECHA_CREO  : 18-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consultar_Tipos_Solicitud()
    {
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Cat_Con_Tipo_Solicitud_Pagos = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
        DataTable Dt_Tipo_Solicitud; //Variable a obtener los datos de la consulta
        DataTable Dt_Modificada = new DataTable();
        Boolean insertar;
        String Tipo_Pago;
        try
        {

            Dt_Tipo_Solicitud = Rs_Cat_Con_Tipo_Solicitud_Pagos.Consulta_Tipo_Solicitud_Pagos_Combo(); //Consulta los tipos de solicitud que fueron dados de alta en la base de datos
           // Cmb_Tipo_Pago_filtro.Items.Clear();
            if (Dt_Tipo_Solicitud.Rows.Count > 0)
            {
                if (Dt_Modificada.Rows.Count <= 0 && Dt_Modificada.Columns.Count <= 0)
                {
                    //  se crea la tabla para las solicitudes de pago
                    Dt_Modificada.Columns.Add("Tipo_Solicitud_Pago_ID", typeof(System.String));
                    Dt_Modificada.Columns.Add("Descripcion_Finanzas", typeof(System.String));
                }
                foreach (DataRow Fila in Dt_Tipo_Solicitud.Rows)
                {
                    Tipo_Pago = Fila["Descripcion_Finanzas"].ToString();
                    DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                    insertar = true;
                    foreach (DataRow Fila2 in Dt_Modificada.Rows)
                    {
                        if (Tipo_Pago.Equals(Fila2["Descripcion_Finanzas"].ToString()) && Tipo_Pago.Equals(Fila2["Descripcion_Finanzas"].ToString()))
                        {
                            insertar = false;
                        }
                    }
                    if (insertar)
                    {
                        row["Tipo_Solicitud_Pago_ID"] = Fila["Tipo_Solicitud_Pago_ID"].ToString();
                        row["Descripcion_Finanzas"] = Tipo_Pago; 
                        Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Modificada.AcceptChanges();
                    }
                }
                //Cmb_Tipo_Pago_filtro.DataSource = Dt_Modificada;
                //Cmb_Tipo_Pago_filtro.DataTextField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion_Finanzas;
                //Cmb_Tipo_Pago_filtro.DataValueField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
                //Cmb_Tipo_Pago_filtro.DataBind();
            }
            //Cmb_Tipo_Pago_filtro.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
            //Cmb_Tipo_Pago_filtro.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Consultar_Tipos_Solicitud " + ex.Message.ToString(), ex);
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Validaciones
    // DESCRIPCIÓN: Genera el String con la informacion que falta y ejecuta la 
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private Boolean Validaciones(bool Validar_Completo)
    {
        Boolean Bln_Bandera;
        Bln_Bandera = true;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
        //Verifica que campos esten seleccionados o tengan valor valor
        if (Cmb_Banco.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += " Seleccionar un Banco. <br>";
            Bln_Bandera = false;
        }
        if (Cmb_Tipo_Pago.SelectedValue == "CHEQUE")
        {
            if (Cmb_Cuenta_Banco.SelectedIndex == 0)
            {
                Lbl_Mensaje_Error.Text += " Seleccione la cuenta del banco <br>";
                Bln_Bandera = false;
            }
            //if (Txt_No_Cheque.Text == "")
            //{
            //    Lbl_Mensaje_Error.Text += " Introduccir el número de cheque <br>";
            //    Bln_Bandera = false;
            //}
        }
        else
        {
            if (Txt_Referencia_Pago.Text == "")
            {
                Lbl_Mensaje_Error.Text += " Introduccir la Referencia de la transferencia<br>";
                Bln_Bandera = false;
            }
        }
        if (Txt_Comentario_Pago.Text == "")
        {
            Lbl_Mensaje_Error.Text += " Introduccir un comentario <br>";
            Bln_Bandera = false;
        }
        if (Txt_Beneficiario_Pago.Text == "")
        {
            Lbl_Mensaje_Error.Text += " Introduccir un beneficiario <br>";
            Bln_Bandera = false;
        }
        if (Cmb_Estatus.SelectedValue == "CANCELADO")
        {
            if (Txt_Motivo_Cancelacion.Text == "")
            {
                Lbl_Mensaje_Error.Text += " Introduccir el motivo de la cancelación  <br>";
                Bln_Bandera = false;
            }
        }
        if (!Bln_Bandera)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
        return Bln_Bandera;
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Validacion_Transferencia
    // DESCRIPCIÓN: Genera el String con la informacion que falta y ejecuta la 
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private Boolean Validacion_Transferencia(bool Validar_Completo, String No_Solicitud, String Tipo)
    {
        Boolean Bln_Bandera;
        Bln_Bandera = true;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
        if (Tipo.Equals("1"))
        {
            if (Txt_Orden.Text == "")
            {
                Lbl_Mensaje_Error.Text += " Introduccir el número de orden de la transferencia <br>";
                Bln_Bandera = false;
            }
        }
        else
        {
            if (Tipo.Equals("2"))
            {
                try
                {
                    DataTable Dt_Consulta = new DataTable();
                    Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos = new Cls_Ope_Con_Transferencia_Negocio();
                    Rs_Consulta_Ejercidos.P_No_solicitud_Pago = No_Solicitud;
                    Dt_Consulta = Rs_Consulta_Ejercidos.Consultar_Datos_Bancarios_Proveedor();
                    if (Dt_Consulta.Rows.Count > 0)
                    {
                        foreach (DataRow Registro in Dt_Consulta.Rows)
                        {
                            if (Registro["NOMBRE_BANCO"].ToString() == "" ||
                                        Registro[Cat_Com_Proveedores.Campo_Banco_Proveedor_ID].ToString() == "" ||
                                        Registro[Cat_Com_Proveedores.Campo_Clabe].ToString() == "")
                            {
                                Lbl_Mensaje_Error.Text += "**El Proveedor " + Registro["Nombre_Proveedor"].ToString() + " Con numero de solicitud de pago " +
                                        Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString() + " no tiene informacion bancaria asignada, favor de ingresar los datos" + "<br>";
                                Bln_Bandera = false;
                            }

                        }// fin del foreach

                    }// fin del if Dt_Consulta

                }// fin del try
                catch
                {
                }
            }
            else
            {

                try
                {
                    DataTable Dt_Consulta = new DataTable();
                    Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos = new Cls_Ope_Con_Transferencia_Negocio();
                    Rs_Consulta_Ejercidos.P_No_solicitud_Pago = No_Solicitud;
                    Dt_Consulta = Rs_Consulta_Ejercidos.Consultar_Datos_Bancarios_Proveedor();
                    if (Dt_Consulta.Rows.Count > 0)
                    {
                        foreach (DataRow Registro in Dt_Consulta.Rows)
                        {
                            if (Registro["NOMBRE_BANCO"].ToString() == "" ||
                                        Registro[Cat_Com_Proveedores.Campo_Banco_Proveedor_ID].ToString() == "" ||
                                        Registro[Cat_Com_Proveedores.Campo_Tipo_Cuenta].ToString() == "" ||
                                        Registro[Cat_Com_Proveedores.Campo_Cuenta].ToString() == "") 
                                        //Registro[Cat_Com_Proveedores.Campo_Clabe].ToString() == "" ||
                            {
                                Lbl_Mensaje_Error.Text += "**El Proveedor " + Registro["Nombre_Proveedor"].ToString() + " Con numero de solicitud de pago " +
                                        Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString() + " no tiene informacion bancaria asignada, favor de ingresar los datos" + "<br>";
                                Bln_Bandera = false;
                            }

                        }// fin del foreach

                    }// fin del if Dt_Consulta

                }// fin del try
                catch
                {
                }
            }
        }
        if (!Bln_Bandera)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
        return Bln_Bandera;
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Validaciones
    // DESCRIPCIÓN: Genera el String con la informacion que falta y ejecuta la 
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private Boolean Validar_Modificacion(bool Validar_Completo)
    {
        Boolean Bln_Bandera;
        Bln_Bandera = true;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
        //Verifica que campos esten seleccionados o tengan valor valor
        if (Txt_Motivo_Cancelacion.Text == "" || Txt_Motivo_Cancelacion.Text == null)
        {
            Lbl_Mensaje_Error.Text += " El Motivo de la cancelacion del Pago <br>";
            Bln_Bandera = false;
        }
        if (!Bln_Bandera)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
        return Bln_Bandera;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Cheque
    /// DESCRIPCION : Modifica los datos del pago  con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 25/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Modificar_Cheque()
    {
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Cheques_Negocio Rs_Modificar_Ope_Con_Pagos = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Tipo_Solicitud = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de coneción hacia la capa de datos
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Datos_Polizas = new DataTable();
        DataTable Dt_Tipo_Solicitud = new DataTable();
        Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
        ReportDocument Reporte = new ReportDocument();
        String Tipo_Solicitud = "";
        String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
        String Nombre_Archivo = "Rechazado";// +Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
        String Usuario = "";
        DataRow Row;
        String CTA_IVA_ACREDITABLE_PEND = "";
        String CTA_IVA_ACREDITABLE = "";
        Decimal Total_Iva_por_Acreditar = 0;
        String Respuesta = "SI";
        DataTable Dt_Reporte = new DataTable();
        DataTable Dt_Solicitud_Pagos = new DataTable();
        try
        {
            Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = Txt_No_Solicitud.Text;
            Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
            if (Dt_Datos_Solicitud.Rows.Count > 0)
            {
                foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                {
                    if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                    {
                        Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                    }
                }
            }
            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));

            DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
            //Agrega el cargo del registro de la póliza
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_Proveedor.Value;
            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + Txt_No_Pago.Text.ToString();
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Txt_Monto.Text.ToString());
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();

            row = Dt_Partidas_Polizas.NewRow();
            //Agrega el abono del registro de la póliza
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Banco.Value;
            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + Txt_No_Pago.Text.ToString();
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Monto.Text.ToString());
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();
            //Se agrega la partida de iva pendiente de acreditar
            Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
            if (Dt_Cuentas_Iva.Rows.Count > 0)
            {
                if (Total_Iva_por_Acreditar > 0)
                {
                    // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                    if (Dt_Cuentas_Iva.Rows.Count > 0)
                    {
                        CTA_IVA_ACREDITABLE = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString().Trim();
                    }
                    if (!String.IsNullOrEmpty(CTA_IVA_ACREDITABLE))
                    {
                        row = Dt_Partidas_Polizas.NewRow();
                        //Agrega el cargo del registro de la póliza
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = 3;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_ACREDITABLE;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + Txt_No_Pago.Text.ToString();
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                        Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Partidas_Polizas.AcceptChanges();
                    }
                    // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                    if (Dt_Cuentas_Iva.Rows.Count > 0)
                    {
                        CTA_IVA_ACREDITABLE_PEND = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString().Trim();
                    }
                    if (!String.IsNullOrEmpty(CTA_IVA_ACREDITABLE_PEND))
                    {
                        row = Dt_Partidas_Polizas.NewRow();
                        //Agrega el cargo del registro de la póliza
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = 4;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_ACREDITABLE_PEND;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Iva_por_Acreditar; ;
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION-" + Txt_No_Pago.Text.ToString();
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                        Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Partidas_Polizas.AcceptChanges();
                    }
                }
            }
            //Agrega los valores a pasar a la capa de negocios para ser dados de alta
            Rs_Modificar_Ope_Con_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
            Rs_Modificar_Ope_Con_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud.Text;
            Rs_Modificar_Ope_Con_Pagos.P_Estatus = "CANCELADO";
            Rs_Modificar_Ope_Con_Pagos.P_Monto = Txt_Monto.Text;
            Rs_Modificar_Ope_Con_Pagos.P_Comentario = "CANCELACION-" + Txt_No_Pago.Text;
            Rs_Modificar_Ope_Con_Pagos.P_Motivo_Cancelacion = Txt_Motivo_Cancelacion.Text;
            Rs_Modificar_Ope_Con_Pagos.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Ope_Con_Pagos.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva_Solicitud.Text);
            Rs_Modificar_Ope_Con_Pagos.P_No_Pago = Txt_No_Pago.Text.ToString();
            Rs_Modificar_Ope_Con_Pagos.P_IVA = Total_Iva_por_Acreditar;
            Respuesta = Rs_Modificar_Ope_Con_Pagos.Modificar_Pago(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
            if (Respuesta == "SI")
            {
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud.Text;
                Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                //  se crea la tabla para el reporte de la cancelacion
                Dt_Reporte.Columns.Add("NO_SOLICITUD", typeof(System.String));
                Dt_Reporte.Columns.Add("NO_RESERVA", typeof(System.String));
                Dt_Reporte.Columns.Add("PROVEEDOR", typeof(System.String));
                Dt_Reporte.Columns.Add("MONTO", typeof(System.Double));
                Dt_Reporte.Columns.Add("FECHA_CREO", typeof(System.DateTime));
                Dt_Reporte.Columns.Add("FECHA_RECHAZO", typeof(System.DateTime));
                Dt_Reporte.Columns.Add("TIPO_SOLICITUD_PAGO_ID", typeof(System.String));
                Dt_Reporte.Columns.Add("CONCEPTO_SOLICITUD", typeof(System.String));
                Dt_Reporte.Columns.Add("COMENTARIO", typeof(System.String));
                Dt_Reporte.Columns.Add("USUARIO_CREO", typeof(System.String));
                Dt_Reporte.Columns.Add("USUARIO_RECHAZO", typeof(System.String));
                Dt_Reporte.TableName = "Dt_Cancelacion";


                foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
                {
                    Row = Dt_Reporte.NewRow();

                    Row["NO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString());
                    Row["NO_RESERVA"] = (Registro["Reserva"].ToString());
                    Row["PROVEEDOR"] = (Registro["Proveedor"].ToString());
                    Row["MONTO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                    Row["FECHA_CREO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo].ToString());
                    Row["FECHA_RECHAZO"] = "" + DateTime.Now;

                    //  para el tipo de solicitud
                    Rs_Tipo_Solicitud.P_Tipo_Solicitud = (Registro[Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString());
                    Dt_Tipo_Solicitud = Rs_Tipo_Solicitud.Consulta_Tipo_Solicitud();

                    foreach (DataRow Tipo in Dt_Tipo_Solicitud.Rows)
                    {
                        Tipo_Solicitud = (Tipo[Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion].ToString());
                    }
                    Row["TIPO_SOLICITUD_PAGO_ID"] = Tipo_Solicitud;
                    Row["CONCEPTO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString());
                    Row["COMENTARIO"] = Txt_Motivo_Cancelacion.Text;
                    Usuario = (Registro[Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo].ToString());
                    Usuario = Usuario.Replace(".", "");
                    Row["USUARIO_CREO"] = Usuario;
                    Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                    Usuario = Usuario.Replace(".", "");
                    Row["USUARIO_RECHAZO"] = Usuario;
                    Dt_Reporte.Rows.Add(Row);
                    Dt_Reporte.AcceptChanges();
                }
                Ds_Reporte.Clear();
                Ds_Reporte.Tables.Clear();
                Ds_Reporte.Tables.Add(Dt_Reporte.Copy());
                Reporte.Load(Ruta_Archivo + "Rpt_Con_Cancelacion_Recepcion.rpt");
                Reporte.SetDataSource(Ds_Reporte);
                DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                Nombre_Archivo += ".pdf";
                Ruta_Archivo = @Server.MapPath("../../Reporte/");
                m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                ExportOptions Opciones_Exportacion = new ExportOptions();
                Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);

                Abrir_Ventana(Nombre_Archivo);
                Recepcion_Documentos_Inicio();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);   
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);
            }
            else
            {
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para realizar el pago ";
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Visible = true;
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Cheque
    /// DESCRIPCION : Da de Alta el Cheque con los datos proporcionados
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 21/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Cheque()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Reserva = new DataTable();
        String Cheque = "";
        DataTable Dt_Deducciones_Persepciones = new DataTable();
        DataTable Dt_Solicitud_Pagos = new DataTable();
        DataTable Dt_Banco_Cuenta_Contable = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        Cls_Ope_Con_Reservas_Negocio Rs_Reserva = new Cls_Ope_Con_Reservas_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad_Negocio();
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos            
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de coneción hacia la capa de datos
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Decimal Monto_Cedular = 0;
        Decimal Monto_ISR = 0;
        String Resultado = "";
        Decimal Total_Monto = 0;
        Decimal Total_Debe = 0;
        String Servicios_Generales = "";
        Decimal Total_Haber = 0;
        Boolean Finiquito = false;
        DataTable Dt_Cuenta_Empleado = new DataTable();
        Decimal Total_Iva_por_Acreditar = 0;
        String CTA_IVA_Acreditable = "";
        String CTA_IVA_Pendiente_Acreditar = "";
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
        Cn.Open();
        Trans = Cn.BeginTransaction();
        Cmmd.Connection = Trans.Connection;
        Cmmd.Transaction = Trans;
        // Cls_Ope_SAP_Dep_Presupuesto_Negocio Rs_Presupuesto = new Cls_Ope_SAP_Dep_Presupuesto_Negocio(); //Variable de conexion con la capa de Negocios.
        try
        {
            Rs_Ope_Con_Cheques.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
            Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
            foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
            {
                Txt_Cuenta_Contable_ID_Banco.Value = Renglon[Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
            }
            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));

            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud.Text.Trim();
            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
            if (!String.IsNullOrEmpty(Dt_Solicitud_Pagos.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString()))
            {
                Servicios_Generales=Dt_Solicitud_Pagos.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString();
            }
            else
            {
                Servicios_Generales = "NO";
            }
            //Verificar lo del Finiquito de Nomina
            DataTable Dt_Tipo = new DataTable();
            Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_Solicitud_Pagos.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
            Rs_Consulta.P_Cmmd = Cmmd;
            Dt_Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos();
            if (Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().ToUpper() != "FINIQUITO" || Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().Substring(0, 4).ToUpper() != "FINI")
            {
                Finiquito = false;
                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud.Text.Trim();
                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                if (Servicios_Generales == "NO")
                {
                    Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();

                    if (Dt_Solicitud_Pagos.Rows.Count > 0)
                    {
                        foreach (DataRow Registro in Dt_Solicitud_Pagos.Rows)
                        {
                            Monto_Cedular = Monto_Cedular + Convert.ToDecimal(Registro["MONTO_CEDULAR"].ToString());
                            Monto_ISR = Monto_ISR + Convert.ToDecimal(Registro["MONTO_ISR"].ToString());
                        }
                    }
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = Txt_No_Solicitud.Text.Trim();
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                    Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                    if (Dt_Datos_Solicitud.Rows.Count > 0)
                    {
                        foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                        {
                            if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                            {
                                Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                            }
                        }
                    }
                }
                else
                {
                    Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago_Servicios_Generales();
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = Txt_No_Solicitud.Text.Trim();
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                    Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Datos_Solicitud_Pago_Servicios_Generales();
                    if (Dt_Datos_Solicitud.Rows.Count > 0)
                    {
                        foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                        {
                            if (!String.IsNullOrEmpty(Fila_Iva["IVA_MONTO"].ToString()))
                            {
                                Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva["IVA_MONTO"].ToString());
                            }
                        }
                    }
                }
                //Agrega el abono del registro de la póliza
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Banco.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Txt_Monto.Text.ToString()) - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();

                //Agrega el cargo del registro de la póliza
                row = Dt_Partidas_Polizas.NewRow();
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_Proveedor.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Txt_Monto.Text.ToString()) - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();

                //Se agrega la partida de iva pendiente de acreditar
                Rs_Parametros_Iva.P_Cmmd = Cmmd;
                Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                if (Dt_Cuentas_Iva.Rows.Count > 0)
                {
                    if (Total_Iva_por_Acreditar > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Acreditable = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Acreditable))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 3;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Acreditable;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Iva_por_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = "";
                            row["Tipo_Beneficiario"] = "";
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Pendiente_Acreditar = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Pendiente_Acreditar))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 4;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Pendiente_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                            row["Beneficiario_ID"] = "";
                            row["Tipo_Beneficiario"] = "";
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
                Rs_Ope_Con_Cheques.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva_Solicitud.Text.Trim());
                Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Ope_Con_Cheques.P_No_Solicitud_Pago = Txt_No_Solicitud.Text.Trim();
                Rs_Ope_Con_Cheques.P_Comentario = Txt_Comentario_Pago.Text.Trim();
                Rs_Ope_Con_Cheques.P_Monto = Txt_Monto.Text.Trim();
                Rs_Ope_Con_Cheques.P_Monto_Transferencia = Convert.ToString(Convert.ToDecimal(Txt_Monto.Text.Trim()) - (Monto_Cedular + Monto_ISR) + Total_Iva_por_Acreditar);
                Rs_Ope_Con_Cheques.P_No_Partidas = "4";
                Rs_Ope_Con_Cheques.P_Estatus = Cmb_Estatus.SelectedItem.ToString();
                Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Txt_Fecha_No_Pago.Text)).ToString();
                Rs_Ope_Con_Cheques.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now).ToString();
                Rs_Ope_Con_Cheques.P_Tipo_Pago = Cmb_Tipo_Pago.SelectedItem.ToString();
                Rs_Ope_Con_Cheques.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                Rs_Ope_Con_Cheques.P_No_Poliza = Txt_No_Poliza.Text;
                Rs_Ope_Con_Cheques.P_Referencia = Txt_Referencia_Pago.Text.ToString();
                if (Txt_Beneficiario_Pago.Text.Trim().Substring(1, 1) == "-")
                {
                    Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Txt_Beneficiario_Pago.Text.Trim().Substring(2);
                }
                else
                {
                    Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Txt_Beneficiario_Pago.Text.Trim();
                }
                Rs_Ope_Con_Cheques.P_Tipo_Solicitud_Pago_ID = Txt_Tipo_Solicitud.Value;
                if (Cmb_Tipo_Pago.SelectedValue == "CHEQUE")
                {
                    Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                    Rs_Consultar_Folio.P_Cmmd = Cmmd;
                    Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();
                    //  obtendra el numero de folio de los cheques
                    if (Dt_Consulta.Rows.Count > 0)
                    {
                        foreach (DataRow Registro in Dt_Consulta.Rows)
                        {
                            if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                            {
                                Txt_No_Cheque.Text = Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString();
                            }
                        }
                    }
                    Rs_Ope_Con_Cheques.P_No_Cheque = Txt_No_Cheque.Text.Trim();
                }
                else
                {
                    Rs_Ope_Con_Cheques.P_Referencia = Txt_Referencia_Pago.Text.Trim();
                }
                Rs_Ope_Con_Cheques.P_Estatus_Comparacion = "PAGADO";
                Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                if (Servicios_Generales == "SI")
                {
                    Rs_Ope_Con_Cheques.P_Servicios_Generales = "SI";
                }
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                //Abono al beneficiario
                if (Chk_Abono_Beneficiario.Checked == true)
                {
                    Rs_Ope_Con_Cheques.P_Abono_Cuenta_Beneficiario = "SI";
                }
                else
                {
                    Rs_Ope_Con_Cheques.P_Abono_Cuenta_Beneficiario = "NO";
                }
                Cheque = Rs_Ope_Con_Cheques.Alta_Cheque();
            }
            else
            {
                Finiquito = true;
                ///Se consultan los detalles de las deducciones y persepciones del finiquito
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud.Text.Trim();
                Dt_Deducciones_Persepciones = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consultar_Datos_Solicitud_Finiquito();
                if (Dt_Deducciones_Persepciones.Rows.Count > 0)
                {
                    foreach (DataRow Fila_D_P in Dt_Deducciones_Persepciones.Rows)
                    {
                        if (Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString()) != 0)
                        {
                            Total_Debe = Total_Debe + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString());
                        }
                        else
                        {
                            Total_Haber = Total_Haber + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString());
                        }
                    }
                }
                Total_Monto = Total_Debe - Total_Haber;
                //Agrega el abono del registro de la póliza
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Banco.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Monto;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                Dt_Cuenta_Empleado = Rs_Parametros_Iva.Obtener_Parametros_Poliza();//se optiene la cuenta del empleado
                if (Dt_Cuenta_Empleado.Rows.Count > 0)
                {
                    Txt_Cuenta_Contable_Proveedor.Value = Dt_Cuenta_Empleado.Rows[0][Cat_Nom_Parametros_Poliza.Campo_Cuenta_Servicios_Profecionales_Pagar_A_CP].ToString();
                }
                //Agrega el cargo del registro de la póliza
                row = Dt_Partidas_Polizas.NewRow();
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_Proveedor.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Monto;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();

                Rs_Ope_Con_Cheques.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva_Solicitud.Text.Trim());
                Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Ope_Con_Cheques.P_No_Solicitud_Pago = Txt_No_Solicitud.Text.Trim();
                Rs_Ope_Con_Cheques.P_Comentario = Txt_Comentario_Pago.Text.Trim();
                Rs_Ope_Con_Cheques.P_Monto = Convert.ToString(Total_Monto);
                Rs_Ope_Con_Cheques.P_Monto_Transferencia = Convert.ToString(Total_Monto);
                Rs_Ope_Con_Cheques.P_No_Partidas = "2";
                Rs_Ope_Con_Cheques.P_Estatus = Cmb_Estatus.SelectedItem.ToString();
                Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Txt_Fecha_No_Pago.Text)).ToString();
                Rs_Ope_Con_Cheques.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now).ToString();
                Rs_Ope_Con_Cheques.P_Tipo_Pago = Cmb_Tipo_Pago.SelectedItem.ToString();
                Rs_Ope_Con_Cheques.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                Rs_Ope_Con_Cheques.P_No_Poliza = Txt_No_Poliza.Text;
                Rs_Ope_Con_Cheques.P_Referencia = Txt_Referencia_Pago.Text.ToString();
                Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Txt_Beneficiario_Pago.Text.Trim();
                Rs_Ope_Con_Cheques.P_Tipo_Solicitud_Pago_ID = Txt_Tipo_Solicitud.Value;
                if (Cmb_Tipo_Pago.SelectedValue == "CHEQUE")
                {
                    Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                    Rs_Consultar_Folio.P_Cmmd = Cmmd;
                    Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();
                    //  obtendra el numero de folio de los cheques
                    if (Dt_Consulta.Rows.Count > 0)
                    {
                        foreach (DataRow Registro in Dt_Consulta.Rows)
                        {
                            if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                            {
                                Txt_No_Cheque.Text = Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString();
                            }
                        }
                    }
                    Rs_Ope_Con_Cheques.P_No_Cheque = Txt_No_Cheque.Text.Trim();
                }
                else
                {
                    Rs_Ope_Con_Cheques.P_Referencia = Txt_Referencia_Pago.Text.Trim();
                }
                Rs_Ope_Con_Cheques.P_Estatus_Comparacion = "PAGADO";
                Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                //Abono al beneficiario
                if (Chk_Abono_Beneficiario.Checked == true)
                {
                    Rs_Ope_Con_Cheques.P_Abono_Cuenta_Beneficiario = "SI";
                }
                else
                {
                    Rs_Ope_Con_Cheques.P_Abono_Cuenta_Beneficiario = "NO";
                }
                Cheque = Rs_Ope_Con_Cheques.Alta_Cheque_Finiquito();
            }
            Resultado = Cheque.Substring(6, 2);
            if (Resultado == "SI")
            {
                Cheque = Cheque.Substring(0, 5);
                if (Cmb_Tipo_Pago.SelectedValue == "CHEQUE")
                {
                    if (Finiquito)
                    {
                        Imprimir(Cheque, Total_Monto, Cmmd);
                    }
                    else
                    {
                        Imprimir(Cheque, Convert.ToDecimal(Txt_Monto.Text.Trim()) - (Monto_Cedular + Monto_ISR), Cmmd);
                    }
                    // cambiamos el numero de folio de los cheques
                    Modificar_Numero_Folio(Cmmd, null);
                }
                Trans.Commit();
                Limpia_Controles();
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            }
            else
            {
                Trans.Rollback();
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para realizar el pago ";
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_CHEQUE " + ex.Message.ToString(), ex);
        }
    }
    private String Alta_Cheque_Varias_Solicitudes(DataTable Dt_Solicitudes, SqlCommand P_Cmmd)
    {
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de coneción hacia la capa de datos
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        DataTable Dt_Deducciones_Persepciones = new DataTable();
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Consulta_Folio = new DataTable();
        DataTable Dt_Banco_Cuenta_Contable = new DataTable(); 
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Decimal Monto_Cedular = 0;
        Decimal Monto_ISR = 0;
        Decimal Monto_Total = 0;
        Decimal Total_Monto = 0;
        String Reserva = "";
        Decimal Total_Debe = 0;
        Decimal Total_Haber = 0;
        String No_Cheque = "";
        String Cheque = "";
        String Servicios_Generales = "";
        Boolean Finiquito = false;
        DataTable Dt_Cuenta_Empleado = new DataTable();
        String CTA_IVA_Acreditable = "";
        String CTA_IVA_Pendiente_Acreditar = "";
        Decimal Total_Iva_por_Acreditar = 0;
        DataTable Dt_Solicitud_Pagos = new DataTable();
        String Cuenta_Contable_Banco_ID = "";
        String Banco_ID = "";
        String Beneficiario = "";
        String Resultado = "SI";
        String Cuenta_Contable_Proveedor_ID = "";
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            //se optiene la cuenta contable del banco con la que se va a realizar el pago
            Banco_ID = Dt_Solicitudes.Rows[0]["Banco_pago"].ToString();
            Rs_Ope_Con_Cheques.P_Banco_ID = Dt_Solicitudes.Rows[0]["Banco_pago"].ToString();
            Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
            Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
            foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
            {
                Cuenta_Contable_Banco_ID = Renglon[Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
            }
            foreach (DataRow Renglon in Dt_Solicitudes.Rows)
            {
                Dt_Solicitud_Pagos = new DataTable();
                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                if (!String.IsNullOrEmpty(Dt_Solicitud_Pagos.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString()))
                {
                    if(Dt_Solicitud_Pagos.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString()=="SI"){
                        Servicios_Generales = "SI";
                    }else{
                        Servicios_Generales = "NO";
                    }
                }
                else
                {
                    Servicios_Generales = "NO";
                }
                Reserva = Dt_Solicitud_Pagos.Rows[0]["NO_RESERVA"].ToString();
                DataTable Dt_Tipo = new DataTable();
                Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_Solicitud_Pagos.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                Rs_Consulta.P_Cmmd = Cmmd;
                Dt_Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos();
                if (Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().ToUpper() != "FINIQUITO" || Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().Substring(0, 4).ToUpper() != "FINI")
                {
                    Finiquito = false;
                    if (Servicios_Generales == "NO")
                    {
                        if (Dt_Solicitud_Pagos.Rows.Count > 0)
                        {
                            Cuenta_Contable_Proveedor_ID = Dt_Solicitud_Pagos.Rows[0]["Pro_Cuenta_Proveedor"].ToString();
                            Beneficiario = Dt_Solicitud_Pagos.Rows[0][Ope_Psp_Reservas.Campo_Beneficiario].ToString();
                            foreach (DataRow Registro in Dt_Solicitud_Pagos.Rows)
                            {
                                Monto_Cedular = Monto_Cedular + Convert.ToDecimal(Registro["MONTO_CEDULAR"].ToString());
                                Monto_ISR = Monto_ISR + Convert.ToDecimal(Registro["MONTO_ISR"].ToString());
                            }
                        }
                    }
                    if (Dt_Solicitud_Pagos.Rows.Count > 0)
                    {
                        Cuenta_Contable_Proveedor_ID = Dt_Solicitud_Pagos.Rows[0]["Pro_Cuenta_Proveedor"].ToString();
                        Beneficiario = Dt_Solicitud_Pagos.Rows[0][Ope_Psp_Reservas.Campo_Beneficiario].ToString();
                    }
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                    if (Servicios_Generales == "NO")
                    {
                        Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                        if (Dt_Datos_Solicitud.Rows.Count > 0)
                        {
                            foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                            {
                                if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                                {
                                    Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                                }
                            }
                        }
                    }
                    else
                    {
                        Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Datos_Solicitud_Pago_Servicios_Generales();
                        if (Dt_Datos_Solicitud.Rows.Count > 0)
                        {
                            foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                            {
                                if (!String.IsNullOrEmpty(Fila_Iva["IVA_MONTO"].ToString()))
                                {
                                    Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva["IVA_MONTO"].ToString());
                                }
                            }
                        }
                    }
                    Monto_Total = Monto_Total + Convert.ToDecimal(Dt_Solicitud_Pagos.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                }
                else
                {
                    Finiquito = true;
                    Dt_Cuenta_Empleado = Rs_Parametros_Iva.Obtener_Parametros_Poliza();//se optiene la cuenta del empleado
                    if (Dt_Cuenta_Empleado.Rows.Count > 0)
                    {
                        Cuenta_Contable_Proveedor_ID = Dt_Cuenta_Empleado.Rows[0][Cat_Nom_Parametros_Poliza.Campo_Cuenta_Servicios_Profecionales_Pagar_A_CP].ToString();
                        Beneficiario = Dt_Solicitud_Pagos.Rows[0]["EMPLEADO"].ToString();
                    }
                }
            }
            if (!Finiquito)
            {
                #region "Partidas Poliza
                //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Banco_ID;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Monto_Total - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";

                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                row = Dt_Partidas_Polizas.NewRow();
                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Proveedor_ID;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Monto_Total - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();

                //Se agrega la partida de iva pendiente de acreditar
                Rs_Parametros_Iva.P_Cmmd = Cmmd;
                Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                if (Dt_Cuentas_Iva.Rows.Count > 0)
                {
                    if (Total_Iva_por_Acreditar > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Acreditable = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Acreditable))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 3;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Acreditable;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Iva_por_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = "";
                            row["Tipo_Beneficiario"] = "";
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Pendiente_Acreditar = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Pendiente_Acreditar))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 4;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Pendiente_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                            row["Beneficiario_ID"] = "";
                            row["Tipo_Beneficiario"] = "";
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
                #endregion
                if (Cuenta_Contable_Banco_ID != "" && Cuenta_Contable_Proveedor_ID != "")
                {
                    if (Total_Iva_por_Acreditar > 0)
                    {
                        if (CTA_IVA_Acreditable != "" && CTA_IVA_Pendiente_Acreditar != "")
                        {
                            Rs_Ope_Con_Cheques.P_Dt_Solicitudes_Masivas = Dt_Solicitudes;
                            Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                            Rs_Ope_Con_Cheques.P_Monto = Monto_Total.ToString();
                            Rs_Ope_Con_Cheques.P_Monto_Transferencia = Convert.ToString(Monto_Total - (Monto_Cedular + Monto_ISR) + Total_Iva_por_Acreditar);
                            Rs_Ope_Con_Cheques.P_No_Partidas = "4";
                            Rs_Ope_Con_Cheques.P_Estatus = "PAGADO";
                            Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                            Rs_Ope_Con_Cheques.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now).ToString();
                            Rs_Ope_Con_Cheques.P_Banco_ID = Banco_ID;
                            if (Beneficiario.Substring(1, 1) == "-")
                            {
                                Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Beneficiario.Substring(2);
                            }
                            else
                            {
                                Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Beneficiario;
                            }
                            Rs_Consultar_Folio.P_Banco_ID = Banco_ID;
                            Rs_Consultar_Folio.P_Cmmd = Cmmd;
                            Dt_Consulta_Folio = Rs_Consultar_Folio.Consultar_Folio_Actual();
                            //  obtendra el numero de folio de los cheques
                            if (Dt_Consulta_Folio.Rows.Count > 0)
                            {
                                foreach (DataRow Registro in Dt_Consulta_Folio.Rows)
                                {
                                    if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                                    {
                                        No_Cheque = Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString();
                                    }
                                }
                            }
                            if (Chk_Leyeda.Checked == true)
                            {
                                Rs_Ope_Con_Cheques.P_Abono_Cuenta_Beneficiario = "SI";
                            }
                            else
                            {
                                Rs_Ope_Con_Cheques.P_Abono_Cuenta_Beneficiario = "NO";
                            }
                            Rs_Ope_Con_Cheques.P_No_Cheque = No_Cheque;
                            Rs_Ope_Con_Cheques.P_Estatus_Comparacion = "PAGADO";
                            Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                            Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                            Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                            Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                            if (Servicios_Generales == "SI")
                            {
                                Rs_Ope_Con_Cheques.P_Servicios_Generales = "SI";
                            }
                            Cheque = Rs_Ope_Con_Cheques.Alta_Cheque_Masivo();
                            Resultado = Cheque.Substring(6, 2);
                            Txt_No_Cheque.Text = No_Cheque;
                            if (Resultado == "SI")
                            {
                                Cheque = Cheque.Substring(0, 5);
                                if (Cmb_Tipo_Pago.SelectedValue == "CHEQUE")
                                {
                                    Imprimir(Cheque, Convert.ToDecimal(Monto_Total) - (Monto_Cedular + Monto_ISR), Cmmd);
                                    // cambiamos el numero de folio de los cheques

                                    Modificar_Numero_Folio(Cmmd, Banco_ID);
                                }
                                if (P_Cmmd == null)
                                {
                                    Trans.Commit();
                                }
                                //Limpia_Controles();
                                //Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                            }
                            else
                            {
                                if (P_Cmmd == null)
                                {
                                    Trans.Rollback();
                                }
                                Lbl_Mensaje_Error.Text = "Error:";
                                Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para realizar el pago ";
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                            }
                        }
                        else
                        {
                            if (CTA_IVA_Acreditable == "")
                            {
                                if (P_Cmmd == null)
                                {
                                    Trans.Rollback();
                                }
                                Lbl_Mensaje_Error.Visible = true;
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                            }
                            if (CTA_IVA_Pendiente_Acreditar == "")
                            {
                                if (P_Cmmd == null)
                                {
                                    Trans.Rollback();
                                }
                                Lbl_Mensaje_Error.Visible = true;
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                            }
                            Resultado = "NO";
                        }
                    }
                    else
                    {
                        Rs_Ope_Con_Cheques.P_Dt_Solicitudes_Masivas = Dt_Solicitudes;
                        Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                        Rs_Ope_Con_Cheques.P_Monto = Monto_Total.ToString();
                        Rs_Ope_Con_Cheques.P_Monto_Transferencia = Convert.ToString(Monto_Total - (Monto_Cedular + Monto_ISR) + Total_Iva_por_Acreditar);
                        Rs_Ope_Con_Cheques.P_No_Partidas = "2";
                        Rs_Ope_Con_Cheques.P_Estatus = "PAGADO";
                        Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                        Rs_Ope_Con_Cheques.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now).ToString();
                        Rs_Ope_Con_Cheques.P_Banco_ID = Banco_ID;
                        if (Beneficiario.Substring(1, 1) == "-")
                        {
                            Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Beneficiario.Substring(2);
                        }
                        else
                        {
                            Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Beneficiario;
                        }
                        Rs_Consultar_Folio.P_Banco_ID = Banco_ID;
                        Rs_Consultar_Folio.P_Cmmd = Cmmd;
                        Dt_Consulta_Folio = Rs_Consultar_Folio.Consultar_Folio_Actual();
                        //  obtendra el numero de folio de los cheques
                        if (Dt_Consulta_Folio.Rows.Count > 0)
                        {
                            foreach (DataRow Registro in Dt_Consulta_Folio.Rows)
                            {
                                if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                                {
                                    No_Cheque = Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString();
                                }
                            }
                        }
                        if (Chk_Leyeda.Checked == true)
                        {
                            Rs_Ope_Con_Cheques.P_Abono_Cuenta_Beneficiario = "SI";
                        }
                        else
                        {
                            Rs_Ope_Con_Cheques.P_Abono_Cuenta_Beneficiario = "NO";
                        }
                        Rs_Ope_Con_Cheques.P_No_Cheque = No_Cheque;
                        Rs_Ope_Con_Cheques.P_Estatus_Comparacion = "PAGADO";
                        Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                        Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                        Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                        Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                        Cheque = Rs_Ope_Con_Cheques.Alta_Cheque_Masivo();
                        Resultado = Cheque.Substring(6, 2);
                        Txt_No_Cheque.Text = No_Cheque;
                        if (Resultado == "SI")
                        {
                            Cheque = Cheque.Substring(0, 5);
                            if (Cmb_Tipo_Pago.SelectedValue == "CHEQUE")
                            {
                                Imprimir(Cheque, Convert.ToDecimal(Monto_Total) - (Monto_Cedular + Monto_ISR), Cmmd);
                                // cambiamos el numero de folio de los cheques

                                Modificar_Numero_Folio(Cmmd, Banco_ID);
                            }
                            if (P_Cmmd == null)
                            {
                                Trans.Commit();
                            }
                            //Limpia_Controles();
                            //Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                        }
                        else
                        {
                            if (P_Cmmd == null)
                            {
                                Trans.Rollback();
                            }
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para realizar el pago ";
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Visible = true;
                        }
                    }
                }
                else
                {
                    if (Cuenta_Contable_Banco_ID == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (Cuenta_Contable_Proveedor_ID == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    Resultado = "NO";
                }
            }
            else
            {
                ///Se consultan los detalles de las deducciones y persepciones del finiquito
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Dt_Solicitudes.Rows[0]["No_Solicitud"].ToString(); ;
                Dt_Deducciones_Persepciones = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consultar_Datos_Solicitud_Finiquito();
                if (Dt_Deducciones_Persepciones.Rows.Count > 0)
                {
                    foreach (DataRow Fila_D_P in Dt_Deducciones_Persepciones.Rows)
                    {
                        if (Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString()) != 0)
                        {
                            Total_Debe = Total_Debe + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString());
                        }
                        else
                        {
                            Total_Haber = Total_Haber + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString());
                        }
                    }
                }
                Total_Monto = Total_Debe - Total_Haber;
                #region "Partidas Poliza
                //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Banco_ID;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] =Total_Monto;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";

                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                row = Dt_Partidas_Polizas.NewRow();
                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Proveedor_ID;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Monto;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                #endregion
                if (Cuenta_Contable_Banco_ID != "" && Cuenta_Contable_Proveedor_ID != "")
                {
                        Rs_Ope_Con_Cheques.P_Dt_Solicitudes_Masivas = Dt_Solicitudes;
                        Rs_Ope_Con_Cheques.P_No_Reserva = Convert.ToDouble(Reserva);
                        Rs_Ope_Con_Cheques.P_No_Solicitud_Pago = Dt_Solicitudes.Rows[0]["No_Solicitud"].ToString();
                        Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                        Rs_Ope_Con_Cheques.P_Monto = Convert.ToString(Total_Monto);
                        Rs_Ope_Con_Cheques.P_Monto_Transferencia = Convert.ToString(Total_Monto);
                        Rs_Ope_Con_Cheques.P_No_Partidas = "2";
                        Rs_Ope_Con_Cheques.P_Estatus = "PAGADO";
                        Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                        Rs_Ope_Con_Cheques.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now).ToString();
                        Rs_Ope_Con_Cheques.P_Banco_ID = Banco_ID;
                        Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Beneficiario;
                        Rs_Consultar_Folio.P_Banco_ID = Banco_ID;
                        Rs_Consultar_Folio.P_Cmmd = Cmmd;
                        Dt_Consulta_Folio = Rs_Consultar_Folio.Consultar_Folio_Actual();
                        //  obtendra el numero de folio de los cheques
                        if (Dt_Consulta_Folio.Rows.Count > 0)
                        {
                            foreach (DataRow Registro in Dt_Consulta_Folio.Rows)
                            {
                                if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                                {
                                    No_Cheque = Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString();
                                }
                            }
                        }
                        if (Chk_Leyeda.Checked == true)
                        {
                            Rs_Ope_Con_Cheques.P_Abono_Cuenta_Beneficiario = "SI";
                        }
                        else
                        {
                            Rs_Ope_Con_Cheques.P_Abono_Cuenta_Beneficiario = "NO";
                        }
                        Rs_Ope_Con_Cheques.P_No_Cheque = No_Cheque;
                        Rs_Ope_Con_Cheques.P_Estatus_Comparacion = "PAGADO";
                        Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                        Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                        Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                        Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                        Rs_Ope_Con_Cheques.P_Tipo_Pago = "CHEQUE";
                        Cheque = Rs_Ope_Con_Cheques.Alta_Cheque_Finiquito();
                        Resultado = Cheque.Substring(6, 2);
                        Txt_No_Cheque.Text = No_Cheque;
                        if (Resultado == "SI")
                        {
                            Cheque = Cheque.Substring(0, 5);
                            if (Cmb_Tipo_Pago.SelectedValue == "CHEQUE")
                            {
                                Imprimir(Cheque, Total_Monto, Cmmd);
                                // cambiamos el numero de folio de los cheques

                                Modificar_Numero_Folio(Cmmd, Banco_ID);
                            }
                            if (P_Cmmd == null)
                            {
                                Trans.Commit();
                            }
                            //Limpia_Controles();
                            //Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                        }
                        else
                        {
                            if (P_Cmmd == null)
                            {
                                Trans.Rollback();
                            }
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para realizar el pago ";
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Visible = true;
                        }
                }
                else
                {
                    if (Cuenta_Contable_Banco_ID == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (Cuenta_Contable_Proveedor_ID == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    Resultado = "NO";
                }
            }
        }
        catch (Exception ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
            Resultado = "NO";
        }
        return Resultado;
    }
    private void Realizar_Cheque_De_Varias_Solicitudes(DataTable Dt_Solicitudes)
    {
        String Resultado = "";
        DataTable Dt_Temporal = new DataTable();
        String No_Solicitud = "";
        String Proveedor = "";
        Int32 Contador = 0;
        Boolean Insertar = false;
        Cls_Ope_Con_Cheques_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Cheques_Negocio(); //Variable de coneción hacia la capa de datos
        DataTable Dt_Solicitud_Pagos = new DataTable();
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
        Cn.Open();
        Trans = Cn.BeginTransaction();
        Cmmd.Connection = Trans.Connection;
        Cmmd.Transaction = Trans;
        try
        {
            if (Dt_Temporal.Rows.Count <= 0 && Dt_Temporal.Columns.Count <= 0)
            {
                Dt_Temporal.Columns.Add("No_Solicitud", typeof(System.String));
                Dt_Temporal.Columns.Add("Proveedor", typeof(System.String));
                Dt_Temporal.Columns.Add("Banco_Pago", typeof(System.String));
                Dt_Temporal.Columns.Add("Monto", typeof(System.Decimal));
            }
            foreach (DataRow Registro_Solicitud in Dt_Solicitudes.Rows)
            {
                No_Solicitud = Registro_Solicitud["No_Solicitud"].ToString();
                Proveedor=Registro_Solicitud["Proveedor"].ToString();
                Insertar = false;
                if (Contador == 0)
                {
                    Contador++;
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud;
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Solicitudes_Autorizadas();
                    if (Dt_Solicitud_Pagos.Rows.Count > 0)
                    {
                        DataRow row = Dt_Temporal.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        row["No_Solicitud"] = Registro_Solicitud["No_Solicitud"].ToString();
                        row["Proveedor"] = Registro_Solicitud["Proveedor"].ToString();
                        row["Banco_Pago"] = Dt_Solicitud_Pagos.Rows[0]["Banco_Pago"].ToString();
                        row["Monto"] = Registro_Solicitud["Monto"].ToString();
                        Dt_Temporal.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Temporal.AcceptChanges();
                    }
                }
                else
                {
                    Contador++;
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud;
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Solicitudes_Autorizadas();
                    if (Dt_Solicitud_Pagos.Rows.Count > 0)
                    {
                        foreach (DataRow Fila_Solicitud in Dt_Temporal.Rows)
                        {
                            if (Fila_Solicitud["Proveedor"].ToString() == Proveedor && Fila_Solicitud["Banco_Pago"].ToString() == Dt_Solicitud_Pagos.Rows[0]["Banco_Pago"].ToString())
                            {
                                Insertar=true;
                            }
                        }
                        if (Insertar)
                        {
                            DataRow row = Dt_Temporal.NewRow(); //Crea un nuevo registro a la tabla
                            //Asigna los valores al nuevo registro creado a la tabla
                            row["No_Solicitud"] = Registro_Solicitud["No_Solicitud"].ToString();
                            row["Proveedor"] = Registro_Solicitud["Proveedor"].ToString();
                            row["Banco_Pago"] = Dt_Solicitud_Pagos.Rows[0]["Banco_Pago"].ToString();
                            row["Monto"] = Registro_Solicitud["Monto"].ToString();
                            Dt_Temporal.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Temporal.AcceptChanges();
                        }
                    }
                }
            }
            if (Dt_Temporal.Rows.Count > 0)
            {
                Resultado = Alta_Cheque_Varias_Solicitudes(Dt_Temporal, Cmmd);
                if (Resultado == "SI")
                {
                    Trans.Commit();
                }
                else
                {
                    Trans.Rollback();
                }
            }
            else
            {
                Trans.Commit();
            }
            Limpia_Controles();
            Inicializa_Controles();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Transferir_Click
    /// DESCRIPCION : Da de Alta la transferencia del cheque
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 21/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Transferir_Click(object sender, EventArgs e)
    {
        Cls_Ope_Con_Cheques_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Consulta = new DataTable();
        Cls_Ope_Con_Cheques_Negocio Rs_Datos_Solicitud = new Cls_Ope_Con_Cheques_Negocio();
        DataTable Dt_Transferencia = new DataTable();
        DataTable Dt_Datos_Polizas = new DataTable();
        DataTable Dt_Tipo_Solicitud = new DataTable();
        DataTable Dt_Solicitudes_Datos = new DataTable();
        DataTable Dt_Reporte_Agrepado = new DataTable();
        Boolean Autorizado;
         Int32 Indice =0;
         Boolean Agregar;
         Boolean Transferir;
         String Orden = "";
        String CUENTA_ORIGEN;
        String FECHA;
        String BENEFICIARIO;
        String Concepto;
        Double Importe;
        Double  Total_Transferencia = 0;
        Int16 Encabezado_y_Pie = 0;
        Double Tamaño_Alias=0;
        String Formato_Imp = "";
        DataTable Dt_Reporte = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataTable Dt_Reporte_Encabezado = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataTable Dt_Reporte_Pie = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataSet Ds_final = new DataSet();
        Double NO_SECUENCIA = 2; //siempre es dos
        int Cant = 0;
        try
        {
                if (Validacion_Transferencia(true, "", "1"))
                {
                    if (Dt_Solicitudes_Datos.Rows.Count <= 0)
                    {
                        Dt_Solicitudes_Datos.Columns.Add("No_Solicitud", typeof(System.String));
                        Dt_Solicitudes_Datos.Columns.Add("Fecha_Pago", typeof(System.String));
                        Dt_Solicitudes_Datos.Columns.Add("Banco", typeof(System.String));
                        Dt_Solicitudes_Datos.Columns.Add("Proveedor", typeof(System.String));
                    }
                    foreach (GridViewRow Renglon_Grid in Grid_Pagos.Rows)
                    {
                        GridView Grid_Proveedores = (GridView)Renglon_Grid.FindControl("Grid_Proveedores");
                        foreach (GridViewRow Renglon in Grid_Proveedores.Rows)
                        {
                            GridView Grid_Datos_Solicitud = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                            foreach (GridViewRow fila in Grid_Datos_Solicitud.Rows)
                            {
                                Indice++;
                                Grid_Datos_Solicitud.SelectedIndex = Indice;
                                Autorizado = ((CheckBox)fila.FindControl("Chk_Transferencia")).Checked;
                                if (Autorizado)
                                {
                                    DataRow row = Dt_Solicitudes_Datos.NewRow(); //Crea un nuevo registro a la tabla
                                    //Asigna los valores al nuevo registro creado a la tabla
                                    row["No_Solicitud"] = ((CheckBox)fila.FindControl("Chk_Transferencia")).CssClass;
                                    row["Fecha_Pago"] = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(((TextBox)fila.FindControl("Txt_Fecha_Transferencia")).Text.ToString()));
                                    row["Banco"] = fila.Cells[7].Text;
                                    row["Proveedor"] = Renglon.Cells[1].Text;
                                    Dt_Solicitudes_Datos.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Solicitudes_Datos.AcceptChanges();
                                }
                            }
                        }
                    }
                    if (Dt_Solicitudes_Datos.Rows.Count > 0)
                    {
                        Dt_Solicitudes_Datos.DefaultView.RowFilter = "Banco='BANORTE'";
                        #region "Layout BANORTE"
                        if (Dt_Solicitudes_Datos.DefaultView.ToTable().Rows.Count > 0)
                        {
                            Transferir = true;
                            foreach (DataRow Validar_Fila in Dt_Solicitudes_Datos.DefaultView.ToTable().Rows)
                            {
                                if (!Validacion_Transferencia(true, Validar_Fila["No_Solicitud"].ToString(), "2"))
                                {
                                    Transferir = false;
                                }
                            }
                            if (Transferir)
                            {
                                if (Dt_Reporte.Rows.Count <= 0)
                                {
                                    Dt_Reporte.Columns.Add("TIPO", typeof(System.String));
                                    Dt_Reporte.Columns.Add("ID", typeof(System.String));
                                    Dt_Reporte.Columns.Add("CUENTA ORIGEN", typeof(System.String));
                                    Dt_Reporte.Columns.Add("CUENTA DESTINO", typeof(System.String));
                                    Dt_Reporte.Columns.Add("IMPORTE", typeof(System.String));
                                    Dt_Reporte.Columns.Add("ORDEN", typeof(System.String));
                                    Dt_Reporte.Columns.Add("CONCEPTO", typeof(System.String));
                                    Dt_Reporte.Columns.Add("TM", typeof(System.String));
                                    Dt_Reporte.Columns.Add("RFC MUNICIPIO", typeof(System.String));
                                    Dt_Reporte.Columns.Add("CEROS", typeof(System.String));
                                    Dt_Reporte.Columns.Add("EMAIL", typeof(System.String));
                                    Dt_Reporte.Columns.Add("FECHA", typeof(System.String));
                                    Dt_Reporte.Columns.Add("BENEFICIARIO", typeof(System.String));
                                    Dt_Reporte.Columns.Add("PESTANIA", typeof(System.String));
                                }
                                foreach (DataRow Fila in Dt_Solicitudes_Datos.DefaultView.ToTable().Rows)
                                {
                                    //Rs_Datos_Solicitud.P_No_Solicitud_Pago = Fila["No_Solicitud"].ToString();
                                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Fila["No_Solicitud"].ToString();
                                    Dt_Consulta = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_transferencia();
                                    if (Dt_Consulta.Rows.Count > 0)
                                    {
                                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                                        {
                                            DataRow Dt_Row = Dt_Reporte.NewRow();
                                            if (Fila["Banco"].ToString() == Renglon["BANCO"].ToString() && Fila["Fecha_Pago"].ToString() == String.Format("{0:dd/MMM/yyyy}", DateTime.Now))
                                            {
                                                Dt_Row["PESTANIA"] = "BB";
                                                Dt_Row["TIPO"] = "02";
                                            }
                                            else
                                            {
                                                if (Fila["Banco"].ToString() == Renglon["BANCO"].ToString() && Fila["Fecha_Pago"].ToString() != String.Format("{0:dd/MMM/yyyy}", DateTime.Now))
                                                {
                                                    Dt_Row["PESTANIA"] = "BB";
                                                    Dt_Row["TIPO"] = "05";
                                                }
                                                else
                                                {
                                                    if (Fila["Banco"].ToString() != Renglon["BANCO"].ToString() && Fila["Fecha_Pago"].ToString() == String.Format("{0:dd/MMM/yyyy}", DateTime.Now))
                                                    {
                                                        Dt_Row["PESTANIA"] = "BP";
                                                        Dt_Row["TIPO"] = "04";
                                                    }
                                                    else
                                                    {
                                                        Dt_Row["PESTANIA"] = "BP";
                                                        Dt_Row["TIPO"] = "05";
                                                    }
                                                }
                                            }
                                            Dt_Row["CUENTA ORIGEN"] = String.Format("{0:00000000000000000000}", Convert.ToInt64(Renglon["NO_CUENTA"].ToString()));
                                            Dt_Row["CEROS"] = "00000000000000";
                                            Dt_Row["RFC MUNICIPIO"] = "";
                                            if (Txt_Orden.Text.ToString().Contains('-'))
                                            {
                                                Dt_Row["ORDEN"] = Txt_Orden.Text.ToString().Substring(Txt_Orden.Text.ToString().IndexOf('-') + 1);
                                            }
                                            else
                                            {
                                                Dt_Row["ORDEN"] = Txt_Orden.Text.ToString();
                                            }
                                            Cant = 10 - Dt_Row["ORDEN"].ToString().Length;
                                            for (int I = 0; I < Cant; I++)
                                            {
                                                Dt_Row["ORDEN"] = "0" + Dt_Row["ORDEN"].ToString();
                                            }
                                            Dt_Row["TM"] = 11;
                                            if (Renglon["CUENTA_DESTINO_EMPLEADO"].ToString() != "")
                                            {
                                                Dt_Row["CUENTA DESTINO"] = String.Format("{0:00000000000000000000}", Convert.ToInt64(Renglon["CUENTA_DESTINO_EMPLEADO"].ToString()));
                                                Dt_Row["EMAIL"] = Renglon["EMAIL_EMPLEADO"].ToString();
                                            }
                                            else
                                            {
                                                if (Dt_Row["PESTANIA"].ToString() == "BB")
                                                {
                                                    Dt_Row["CUENTA DESTINO"] = String.Format("{0:00000000000000000000}", Convert.ToInt64(Renglon["CUENTA_DESTINO_PROVEEDOR"].ToString()));
                                                }
                                                else
                                                {
                                                    Dt_Row["CUENTA DESTINO"] = String.Format("{0:00000000000000000000}", Convert.ToInt64(Renglon["CLABE"].ToString()));
                                                }
                                                Dt_Row["EMAIL"] = Renglon["EMAIL_PROVEEDOR"].ToString();
                                                Dt_Row["ID"] = Renglon["ID_BANCO_PROVEEDOR"].ToString().Replace(" ", "");
                                            }
                                            Formato_Imp = String.Format("{0:n}", Convert.ToDouble(Renglon["IMPORTE"].ToString()));
                                            Formato_Imp = Formato_Imp.Replace(",", "").Replace(".", "");
                                            Dt_Row["IMPORTE"] = String.Format("{0:00000000000000}", (Convert.ToDouble(Formato_Imp)));

                                            Dt_Row["CONCEPTO"] = Renglon["CONCEPTO"].ToString();
                                            Cant = 30 - Dt_Row["CONCEPTO"].ToString().Length;
                                            for (int I = 0; I < Cant; I++)
                                            {
                                                Dt_Row["CONCEPTO"] = Dt_Row["CONCEPTO"].ToString() + " ";
                                            }
                                            Dt_Row["FECHA"] = String.Format("{0:ddMMyyyy}", Convert.ToDateTime(Fila["Fecha_Pago"].ToString()));
                                            if (Renglon["BENEFICIARIO"].ToString().Contains('-'))
                                            {
                                                Dt_Row["BENEFICIARIO"] = Renglon["BENEFICIARIO"].ToString().Substring(Renglon["BENEFICIARIO"].ToString().IndexOf("-") + 1).Trim();
                                            }
                                            else
                                            {
                                                Dt_Row["BENEFICIARIO"] = Renglon["BENEFICIARIO"].ToString().Trim();
                                            }
                                            Dt_Reporte.Rows.Add(Dt_Row); //Agrega el registro creado con todos sus valores a la tabla
                                            Dt_Reporte.AcceptChanges();
                                        }
                                        //Pasamos el datattable al dataset
                                    }
                                }
                                if (Dt_Reporte.Rows.Count > 0)
                                {
                                    if (Dt_Reporte_Agrepado.Rows.Count <= 0)
                                    {
                                        Dt_Reporte_Agrepado.Columns.Add("TIPO", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("ID", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("CUENTA ORIGEN", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("CUENTA DESTINO", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("IMPORTE", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("ORDEN", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("CONCEPTO", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("TM", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("RFC MUNICIPIO", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("CEROS", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("EMAIL", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("FECHA", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("BENEFICIARIO", typeof(System.String));
                                        Dt_Reporte_Agrepado.Columns.Add("PESTANIA", typeof(System.String));
                                    }
                                    foreach (DataRow Renglon in Dt_Reporte.Rows)
                                    {
                                        DataRow row = Dt_Reporte_Agrepado.NewRow(); //Crea un nuevo registro a la tabla
                                        Agregar = true;
                                        Importe = 0;
                                        Concepto = "";
                                        CUENTA_ORIGEN = Renglon["CUENTA ORIGEN"].ToString();
                                        FECHA = Renglon["FECHA"].ToString();
                                        BENEFICIARIO = Renglon["ID"].ToString();
                                        //insertar los que no se repiten
                                        foreach (DataRow posicion in Dt_Reporte_Agrepado.Rows)
                                        {
                                            if (CUENTA_ORIGEN.Equals(posicion["CUENTA ORIGEN"].ToString()) && FECHA.Equals(posicion["FECHA"].ToString()) && BENEFICIARIO.Equals(posicion["ID"].ToString()))
                                            {
                                                Agregar = false;
                                            }
                                        }
                                        //se calcula el monto por tipo
                                        foreach (DataRow Registro in Dt_Reporte.Rows)
                                        {
                                            if (CUENTA_ORIGEN.Equals(Registro["CUENTA ORIGEN"].ToString()) && FECHA.Equals(Registro["FECHA"].ToString()) && BENEFICIARIO.Equals(Registro["ID"].ToString()))
                                            {
                                                Importe = Importe + Convert.ToDouble(Registro["IMPORTE"].ToString());
                                                Concepto = Concepto + Registro["CONCEPTO"].ToString().Replace('X', ' ').Trim() + " ";
                                            }
                                        }
                                        if (Agregar && Importe > 0)
                                        {
                                            row["TIPO"] = Renglon["TIPO"].ToString();
                                            row["ID"] = BENEFICIARIO;
                                            row["CUENTA ORIGEN"] = CUENTA_ORIGEN;
                                            row["CUENTA DESTINO"] = Renglon["CUENTA DESTINO"].ToString();
                                            row["IMPORTE"] = String.Format("{0:00000000000000}", Convert.ToDouble(Importe));
                                            row["ORDEN"] = Renglon["ORDEN"].ToString();
                                            row["CONCEPTO"] = Concepto;
                                            Cant = 30 - row["CONCEPTO"].ToString().Length;
                                            for (int I = 0; I < Cant; I++)
                                            {
                                                row["CONCEPTO"] = row["CONCEPTO"].ToString() + " ";
                                            }
                                            row["TM"] = Renglon["TM"].ToString();
                                            row["RFC MUNICIPIO"] = Renglon["RFC MUNICIPIO"].ToString();
                                            row["CEROS"] = Renglon["CEROS"].ToString();
                                            row["EMAIL"] = Renglon["EMAIL"].ToString();
                                            row["FECHA"] = Renglon["FECHA"].ToString();
                                            row["BENEFICIARIO"] = Renglon["BENEFICIARIO"].ToString();
                                            row["PESTANIA"] = Renglon["PESTANIA"].ToString();
                                            Dt_Reporte_Agrepado.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                            Dt_Reporte_Agrepado.AcceptChanges();
                                        }
                                    }
                                }
                                if (Dt_Reporte_Agrepado.Rows.Count > 0)
                                {
                                    foreach (DataRow Fil in Dt_Solicitudes_Datos.Rows)
                                    {
                                        Dt_Consulta = new DataTable();
                                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Fil["No_Solicitud"].ToString();
                                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Transferencia = Txt_Orden.Text.Trim();
                                        // Concer la cuenta destino
                                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Fil["No_Solicitud"].ToString();
                                        Dt_Consulta = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_transferencia();
                                        if (Dt_Consulta.Rows.Count > 0)
                                        {
                                            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cuenta_Transferencia_ID = Dt_Consulta.Rows[0]["CUENTA_ORIGEN"].ToString();
                                        }
                                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                                        Rs_Modificar_Ope_Con_Solicitud_Pagos.Alta_Transferencia();
                                    }
                                    Orden = Txt_Orden.Text;
                                    Generar_Archivo_Banorte(Dt_Reporte_Agrepado, Txt_Orden.Text);
                                    Inicializa_Controles();
                                }
                            }
                        }
                        #endregion
                        #region "Layout Banco del Bajio"
                        Dt_Solicitudes_Datos.DefaultView.RowFilter = "Banco='BANCO DEL BAJIO'";
                        if (Dt_Solicitudes_Datos.DefaultView.ToTable().Rows.Count > 0)
                        {
                            Transferir = true;
                            foreach (DataRow Validar_Fila in Dt_Solicitudes_Datos.DefaultView.ToTable().Rows)
                            {
                                if (!Validacion_Transferencia(true, Validar_Fila["No_Solicitud"].ToString(), "3"))
                                {
                                    Transferir = false;
                                }
                            }
                            if (Transferir)
                            {
                                //Encabezado del Archivo
                                if (Dt_Reporte_Encabezado.Rows.Count <= 0)
                                {
                                    Dt_Reporte_Encabezado.Columns.Add("ENCABEZADO", typeof(System.String));
                                    Dt_Reporte_Encabezado.Columns.Add("ID", typeof(System.String));
                                    Dt_Reporte_Encabezado.Columns.Add("FECHA", typeof(System.String));
                                    Dt_Reporte_Encabezado.Columns.Add("ARCHIVO", typeof(System.String));
                                }
                                //Escribe detalle en archivo
                                if (Dt_Reporte.Rows.Count <= 0)
                                {
                                    Dt_Reporte.Columns.Add("DETALLE", typeof(System.String));
                                    Dt_Reporte.Columns.Add("NO_SECUENCIA", typeof(System.String));
                                    Dt_Reporte.Columns.Add("TIPO_CUENTA_EMISOR", typeof(System.String));
                                    Dt_Reporte.Columns.Add("NO_CUENTA_EMISOR", typeof(System.String));
                                    Dt_Reporte.Columns.Add("DATO_01", typeof(System.String));
                                    Dt_Reporte.Columns.Add("BANCO_RECEPTOR", typeof(System.String));
                                    Dt_Reporte.Columns.Add("MONTO_OPERACION", typeof(System.String));
                                    Dt_Reporte.Columns.Add("FECHA_APLICA", typeof(System.String));
                                    Dt_Reporte.Columns.Add("MEDIO_PAGO", typeof(System.String));
                                    Dt_Reporte.Columns.Add("TIPO_CUENTA_RECEPTOR", typeof(System.String));
                                    Dt_Reporte.Columns.Add("NO_CUENTA_RECEPTOR", typeof(System.String));
                                    Dt_Reporte.Columns.Add("CEROS", typeof(System.String));
                                    Dt_Reporte.Columns.Add("ALIAS", typeof(System.String));
                                    Dt_Reporte.Columns.Add("IVA", typeof(System.String));
                                    Dt_Reporte.Columns.Add("REFERENCIA", typeof(System.String));
                                }
                                //Encabezado del Archivo
                                if (Dt_Reporte_Pie.Rows.Count <= 0)
                                {
                                    Dt_Reporte_Pie.Columns.Add("SUMARIO", typeof(System.String));
                                    Dt_Reporte_Pie.Columns.Add("NO_SECUENCIA", typeof(System.String));
                                    Dt_Reporte_Pie.Columns.Add("NO_OPERACIONES", typeof(System.String));
                                    Dt_Reporte_Pie.Columns.Add("MONTO_TOTAL", typeof(System.String));
                                }
                                foreach (DataRow Fila in Dt_Solicitudes_Datos.DefaultView.ToTable().Rows)
                                {
                                    if (Encabezado_y_Pie == 0)
                                    {
                                        //se mandan los encabezados
                                        DataRow Dt_Row_Encabezado = Dt_Reporte_Encabezado.NewRow();
                                        Dt_Row_Encabezado["ENCABEZADO"] = "01";
                                        Dt_Row_Encabezado["ID"] = "0000001";
                                        Dt_Row_Encabezado["FECHA"] = String.Format("{0:yyyyMMdd}", DateTime.Now);
                                        Dt_Row_Encabezado["ARCHIVO"] = String.Format("{0:000}", Convert.ToInt32(Txt_Orden.Text));
                                        Dt_Reporte_Encabezado.Rows.Add(Dt_Row_Encabezado); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Reporte_Encabezado.AcceptChanges();
                                    }
                                    //Rs_Datos_Solicitud.P_No_Solicitud_Pago = Fila["No_Solicitud"].ToString();
                                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Fila["No_Solicitud"].ToString();
                                    Dt_Consulta = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_transferencia();
                                    if (Dt_Consulta.Rows.Count > 0)
                                    {
                                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                                        {
                                            DataRow Dt_Row = Dt_Reporte.NewRow();
                                            Dt_Row["DETALLE"] = "02";
                                            Dt_Row["NO_SECUENCIA"] = String.Format("{0:0000000}", NO_SECUENCIA);
                                            NO_SECUENCIA = NO_SECUENCIA + 1;
                                            Dt_Row["TIPO_CUENTA_EMISOR"] = String.Format("{0:00}", Convert.ToDouble(Renglon[Cat_Com_Proveedores.Campo_Tipo_Cuenta].ToString()));//consulta;
                                            Dt_Row["NO_CUENTA_EMISOR"] = String.Format("{0:00000000000000000000}", Convert.ToDouble(Renglon[Cat_Nom_Bancos.Campo_No_Cuenta].ToString()));
                                            Dt_Row["DATO_01"] = "01";
                                            Dt_Row["BANCO_RECEPTOR"] = String.Format("{0:00000}", Convert.ToDouble("40" + Renglon["ID_BANCO_PROVEEDOR"].ToString()));
                                            Dt_Row["MONTO_OPERACION"] = String.Format("{0:000000000000000}", Convert.ToDouble(Renglon["IMPORTE"].ToString()) * 100);
                                            Total_Transferencia = Total_Transferencia + (Convert.ToDouble(Renglon["IMPORTE"].ToString()) * 100);
                                            Dt_Row["FECHA_APLICA"] = String.Format("{0:yyyyMMdd}", Convert.ToDateTime(Fila["Fecha_Pago"].ToString()));
                                            if (Fila["BANCO"].ToString() == Renglon["BANCO"].ToString())//si la transaccion sera de bajio a bajio se coloca BCO
                                            {
                                                Dt_Row["MEDIO_PAGO"] = "BCO";
                                            }
                                            else
                                            {
                                                if (Fila["BANCO"].ToString() != Renglon["BANCO"].ToString() && Fila["Fecha_Pago"].ToString() == String.Format("{0:dd/MMM/yyyy}", DateTime.Now))//si la transaccion es de bajio a otro banco y se aplicara el mismo dia
                                                {
                                                    Dt_Row["MEDIO_PAGO"] = "SPI";
                                                }
                                                else
                                                {
                                                    if (Fila["BANCO"].ToString() != Renglon["BANCO"].ToString() && Fila["Fecha_Pago"].ToString() != String.Format("{0:dd/MMM/yyyy}", DateTime.Now))//si la transaccion es de bajio a otro banco y se aplica en otra fecha el pago es TEF
                                                    {
                                                        Dt_Row["PESTANIA"] = "TEF";
                                                    }
                                                }
                                            }
                                            Dt_Row["TIPO_CUENTA_RECEPTOR"] = String.Format("{0:00}", Convert.ToDouble(Renglon["TIPO_CUENTA"].ToString()));
                                            Dt_Row["NO_CUENTA_RECEPTOR"] = String.Format("{0:00000000000000000000}", Convert.ToDouble(Renglon["CUENTA_DESTINO_PROVEEDOR"].ToString()));
                                            Dt_Row["CEROS"] = "000000000";
                                            if (Renglon["BENEFICIARIO"].ToString().Contains("-"))
                                            {
                                                if (Renglon["BENEFICIARIO"].ToString().Length > 17)
                                                {
                                                    Dt_Row["ALIAS"] = Renglon["BENEFICIARIO"].ToString().Substring(Renglon["BENEFICIARIO"].ToString().IndexOf("-") + 1, 15);
                                                }
                                                else
                                                {
                                                    Dt_Row["ALIAS"] = Renglon["BENEFICIARIO"].ToString().Substring(Renglon["BENEFICIARIO"].ToString().IndexOf("-") + 1);
                                                    if (Dt_Row["ALIAS"].ToString().Length < 15)
                                                    {
                                                        Tamaño_Alias = Dt_Row["ALIAS"].ToString().Length;
                                                        Tamaño_Alias = 15 - Tamaño_Alias;
                                                        for (int Agregar_Caracteres = 1; Tamaño_Alias >= Agregar_Caracteres; Agregar_Caracteres++)
                                                        {
                                                            Dt_Row["ALIAS"] = Dt_Row["ALIAS"].ToString() + " ";
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (Renglon["BENEFICIARIO"].ToString().Length > 15)
                                                {
                                                    Dt_Row["ALIAS"] = Renglon["BENEFICIARIO"].ToString().Substring(0, 15);
                                                }
                                                else
                                                {
                                                    Dt_Row["ALIAS"] = Renglon["BENEFICIARIO"].ToString();
                                                    if (Dt_Row["ALIAS"].ToString().Length < 15)
                                                    {
                                                        Tamaño_Alias = Dt_Row["ALIAS"].ToString().Length;
                                                        Tamaño_Alias = 15 - Tamaño_Alias;
                                                        for (int Agregar_Caracteres = 1; Tamaño_Alias >= Agregar_Caracteres; Agregar_Caracteres++)
                                                        {
                                                            Dt_Row["ALIAS"] = Dt_Row["ALIAS"].ToString() + " ";
                                                        }
                                                    }
                                                }
                                            }
                                            Dt_Row["IVA"] = String.Format("{0:000000000000000}", Convert.ToDecimal(Renglon["IVA"].ToString()) * 100);
                                            Dt_Row["REFERENCIA"] = "Pago de solicitud No." + Fila["No_Solicitud"].ToString() + "         ";
                                            Dt_Reporte.Rows.Add(Dt_Row); //Agrega el registro creado con todos sus valores a la tabla
                                            Dt_Reporte.AcceptChanges();
                                        }
                                        //Pasamos el datattable al dataset
                                    }
                                    if (Encabezado_y_Pie == 0)
                                    {
                                        DataRow Dt_Row_Pie = Dt_Reporte_Pie.NewRow();
                                        Dt_Row_Pie["SUMARIO"] = "09";
                                        Dt_Row_Pie["NO_SECUENCIA"] = String.Format("{0:0000000}", NO_SECUENCIA + 1);
                                        Dt_Row_Pie["NO_OPERACIONES"] = String.Format("{0:0000000}", NO_SECUENCIA - 1);
                                        Dt_Row_Pie["MONTO_TOTAL"] = String.Format("{0:000000000000000000}", Total_Transferencia);
                                        Dt_Reporte_Pie.Rows.Add(Dt_Row_Pie); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Reporte_Pie.AcceptChanges();
                                    }
                                    Encabezado_y_Pie = 1;
                                }
                                #region "Agrupar"
                                ////if (Dt_Reporte.Rows.Count > 0)
                                ////{
                                ////    if (Dt_Reporte_Agrepado.Rows.Count <= 0)
                                ////    {
                                ////        Dt_Reporte_Agrepado.Columns.Add("TIPO", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("ID", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("CUENTA ORIGEN", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("CUENTA DESTINO", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("IMPORTE", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("ORDEN", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("CONCEPTO", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("TM", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("RFC MUNICIPIO", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("CEROS", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("EMAIL", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("FECHA", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("BENEFICIARIO", typeof(System.String));
                                ////        Dt_Reporte_Agrepado.Columns.Add("PESTANIA", typeof(System.String));
                                ////    }
                                ////    foreach (DataRow Renglon in Dt_Reporte.Rows)
                                ////    {
                                ////        DataRow row = Dt_Reporte_Agrepado.NewRow(); //Crea un nuevo registro a la tabla
                                ////        Agregar = true;
                                ////        Importe = 0;
                                ////        Concepto = "";
                                ////        CUENTA_ORIGEN = Renglon["CUENTA ORIGEN"].ToString();
                                ////        FECHA = Renglon["FECHA"].ToString();
                                ////        BENEFICIARIO = Renglon["ID"].ToString();
                                ////        //insertar los que no se repiten
                                ////        foreach (DataRow posicion in Dt_Reporte_Agrepado.Rows)
                                ////        {
                                ////            if (CUENTA_ORIGEN.Equals(posicion["CUENTA ORIGEN"].ToString()) && FECHA.Equals(posicion["FECHA"].ToString()) && BENEFICIARIO.Equals(posicion["ID"].ToString()))
                                ////            {
                                ////                Agregar = false;
                                ////            }
                                ////        }
                                ////        //se calcula el monto por tipo
                                ////        foreach (DataRow Registro in Dt_Reporte.Rows)
                                ////        {
                                ////            if (CUENTA_ORIGEN.Equals(Registro["CUENTA ORIGEN"].ToString()) && FECHA.Equals(Registro["FECHA"].ToString()) && BENEFICIARIO.Equals(Registro["ID"].ToString()))
                                ////            {
                                ////                Importe = Importe + Convert.ToDouble(Registro["IMPORTE"].ToString());
                                ////                Concepto = Concepto + Registro["CONCEPTO"].ToString().Replace('X', ' ').Trim() + " ";
                                ////            }
                                ////        }
                                ////        if (Agregar && Importe > 0)
                                ////        {
                                ////            row["TIPO"] = Renglon["TIPO"].ToString();
                                ////            row["ID"] = BENEFICIARIO;
                                ////            row["CUENTA ORIGEN"] = CUENTA_ORIGEN;
                                ////            row["CUENTA DESTINO"] = Renglon["CUENTA DESTINO"].ToString();
                                ////            row["IMPORTE"] = String.Format("{0:00000000000000}", Convert.ToDouble(Importe));
                                ////            row["ORDEN"] = Renglon["ORDEN"].ToString();
                                ////            row["CONCEPTO"] = Concepto;
                                ////            Cant = 30 - row["CONCEPTO"].ToString().Length;
                                ////            for (int I = 0; I < Cant; I++)
                                ////            {
                                ////                row["CONCEPTO"] = row["CONCEPTO"].ToString() + " ";
                                ////            }
                                ////            row["TM"] = Renglon["TM"].ToString();
                                ////            row["RFC MUNICIPIO"] = Renglon["RFC MUNICIPIO"].ToString();
                                ////            row["CEROS"] = Renglon["CEROS"].ToString();
                                ////            row["EMAIL"] = Renglon["EMAIL"].ToString();
                                ////            row["FECHA"] = Renglon["FECHA"].ToString();
                                ////            row["BENEFICIARIO"] = Renglon["BENEFICIARIO"].ToString();
                                ////            row["PESTANIA"] = Renglon["PESTANIA"].ToString();
                                ////            Dt_Reporte_Agrepado.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                ////            Dt_Reporte_Agrepado.AcceptChanges();
                                ////        }
                                ////    }
                                ////}

                                #endregion
                                if (Dt_Reporte.Rows.Count > 0)
                                {
                                    foreach (DataRow Fil in Dt_Solicitudes_Datos.Rows)
                                    {
                                        Dt_Consulta = new DataTable();
                                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Fil["No_Solicitud"].ToString();
                                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Transferencia = Txt_Orden.Text.Trim();
                                        // Concer la cuenta destino
                                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Fil["No_Solicitud"].ToString();
                                        Dt_Consulta = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_transferencia();
                                        if (Dt_Consulta.Rows.Count > 0)
                                        {
                                            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cuenta_Transferencia_ID = Dt_Consulta.Rows[0]["CUENTA_ORIGEN"].ToString();
                                        }
                                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                                        Rs_Modificar_Ope_Con_Solicitud_Pagos.Alta_Transferencia();
                                    }
                                    Orden = Txt_Orden.Text;
                                    Generar_Archivo_Bajio(Dt_Reporte_Encabezado, Dt_Reporte, Dt_Reporte_Pie);
                                    Inicializa_Controles();
                                }
                            }
                        }
                        #endregion
                    }
                }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Transferir_Pago_Click
    /// DESCRIPCION : Da de Alta la transferencia del cheque
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 21/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Transferir_Pago_Click(object sender, EventArgs e)
    {
        Cls_Ope_Con_Cheques_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Consulta = new DataTable();
        Cls_Ope_Con_Cheques_Negocio Rs_Datos_Solicitud = new Cls_Ope_Con_Cheques_Negocio();
        DataTable Dt_Transferencia = new DataTable();
        DataTable Dt_Datos_Polizas = new DataTable();
        DataTable Dt_Tipo_Solicitud = new DataTable();
        DataTable Dt_Solicitudes_Datos = new DataTable();
        DataTable Dt_Reporte_Agrepado = new DataTable();
        Boolean Autorizado;
         Int32 Indice =0;
        String Resultado = "";
        DataTable Dt_Reporte = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataTable Dt_Reporte_Encabezado = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataTable Dt_Reporte_Pie = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataSet Ds_final = new DataSet();
        try
        {
                //identificar que solicitudes se seleccionaron
                if (Dt_Solicitudes_Datos.Rows.Count <= 0)
                {
                    Dt_Solicitudes_Datos.Columns.Add("No_Solicitud", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Fecha_Pago", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Banco", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Proveedor", typeof(System.String));
                }
                foreach (GridViewRow Renglon_Grid in Grid_Pagos.Rows)
                {
                    GridView Grid_Proveedores = (GridView)Renglon_Grid.FindControl("Grid_Proveedores");
                    foreach (GridViewRow Renglon in Grid_Proveedores.Rows)
                    {
                        GridView Grid_Datos_Solicitud = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                        foreach (GridViewRow fila in Grid_Datos_Solicitud.Rows)
                        {
                            Indice++;
                            Grid_Datos_Solicitud.SelectedIndex = Indice;
                            Autorizado = ((CheckBox)fila.FindControl("Chk_Transferencia")).Checked;
                            if (Autorizado)
                            {
                                DataRow row = Dt_Solicitudes_Datos.NewRow(); //Crea un nuevo registro a la tabla
                                //Asigna los valores al nuevo registro creado a la tabla
                                row["No_Solicitud"] = ((CheckBox)fila.FindControl("Chk_Transferencia")).CssClass;
                                row["Fecha_Pago"] = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(((TextBox)fila.FindControl("Txt_Fecha_Transferencia")).Text.ToString()));
                                row["Banco"] = fila.Cells[7].Text;
                                row["Proveedor"] = Renglon.Cells[1].Text;
                                Dt_Solicitudes_Datos.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Solicitudes_Datos.AcceptChanges();
                            }
                        }
                    }
                }
                if (Dt_Solicitudes_Datos.Rows.Count > 0)
                {
                    SqlConnection Cn = new SqlConnection();
                    SqlCommand Cmmd = new SqlCommand();
                    SqlTransaction Trans = null;
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                    Resultado = Alta_Transferencia(Dt_Solicitudes_Datos, Cmmd);
                    if (Resultado == "SI")
                    {
                        Trans.Commit();
                        Inicializa_Controles();
                    }
                    else
                    {
                        Trans.Rollback();
                    }
                }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Transferencia
    /// DESCRIPCION : Da de Alta el Cheque con los datos proporcionados
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 21/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Alta_Transferencia(DataTable Dt_Solicitudes, SqlCommand P_Cmmd)
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Reserva = new DataTable();
        DataTable Dt_Solicitudes_Modificada = new DataTable();
        DataTable Dt_Solicitudes_Completa = new DataTable();
        DataTable Dt_Solicitudes_Partidas = new DataTable();
        DataTable Dt_Datos_Proveedor = new DataTable();
        DataTable Dt_Tipo_Operacion = new DataTable();
        DataTable Dt_Banco_Cuenta_Contable = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Solicitud_Pagos = new DataTable(); //Variable a contener los datos de la consulta de la solicitud de pago
        Cls_Ope_Con_Reservas_Negocio Rs_Reserva = new Cls_Ope_Con_Reservas_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad_Negocio();
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de coneción hacia la capa de datos
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consultar_Solicitud_Anticipo = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
        Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos = new Cls_Ope_Con_Transferencia_Negocio();
        Cls_Cat_Com_Proveedores_Negocio Rs_Consulta_Proveedor = new Cls_Cat_Com_Proveedores_Negocio();
        Cls_Ope_Con_Cheques_Negocio Rs_constulta_Empleado = new Cls_Ope_Con_Cheques_Negocio();
        Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
        DataTable Dt_Consulta_Solicitud_Pago = new DataTable();
        //String Estatus_Reserva = "";
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        Decimal Total_Iva_por_Acreditar = 0;
        String Pago = "";
        String Cuenta_Contable_Proveedor = "";
        String Cuenta = "";
        String Proveedor = "";
        String CTA_IVA_Acreditable = "";
        String CTA_IVA_Pendiente_Acreditar = "";
        String Banco = "";
        String Cuenta_Contable_ID_Banco = "";
        String Cuenta_Contable_Proveedor_Banco_ID = "";
        String Tipo_Beneficiario;
        Int32 partida = 0;
        Boolean Agregar;
        Decimal Total;
        String Nombre_Proveedor = "";
        Decimal Total_Cedular;
        Decimal Total_ISR;
        String Resultados = "";
        String Fecha_Pago = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            if (Dt_Solicitudes_Modificada.Rows.Count <= 0)
            {
                Dt_Solicitudes_Modificada.Columns.Add("No_Solicitud", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Referencia", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Banco", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Proveedor", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Cuenta", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Monto", typeof(System.Decimal));
                Dt_Solicitudes_Modificada.Columns.Add("Reserva", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Tipo_Solicitud_Pago", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Banco_Proveedor", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Fecha_Poliza", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Ref_Comision", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Ref_IVA", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Tipo_Beneficiario", typeof(System.String));
            }
            foreach (DataRow Fila in Dt_Solicitudes.Rows)
            {
                    Rs_Consulta_Proveedor.P_Proveedor_ID = Fila["PROVEEDOR"].ToString();
                    Rs_Consulta_Proveedor.P_Cmmd = Cmmd;
                    Dt_Datos_Proveedor = Rs_Consulta_Proveedor.Consulta_Datos_Proveedores();
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Fila["No_Solicitud"].ToString();
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Dt_Datos_Solicitud = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                    if (Dt_Datos_Proveedor.Rows.Count > 0)
                    {
                        Nombre_Proveedor = Dt_Datos_Proveedor.Rows[0]["NOMBRE"].ToString();
                        DataRow Rows = Dt_Solicitudes_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        Rows["No_Solicitud"] = Fila["No_Solicitud"].ToString();
                        Rows["Referencia"] ="";
                        Rows["Banco"] = Fila["BANCO"].ToString();
                        Rows["cuenta"] = Dt_Datos_Solicitud.Rows[0]["CUENTA_BANCO_PAGO_ID"].ToString();
                        Rows["Proveedor"] = Fila["Proveedor"].ToString();
                        Rows["Monto"] = Dt_Datos_Solicitud.Rows[0]["Monto"].ToString();
                        Rows["Reserva"] = Dt_Datos_Solicitud.Rows[0]["NO_RESERVA"].ToString();
                        Rows["Tipo_Solicitud_Pago"] = Dt_Datos_Solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString();
                        Rows["Banco_Proveedor"] = "";
                        Rows["Fecha_Poliza"] = Fila["Fecha_Pago"].ToString();
                        Rows["Ref_Comision"] = "";
                        Rows["Ref_IVA"] = "";
                        Rows["Tipo_Beneficiario"] = "";
                        Dt_Solicitudes_Modificada.Rows.Add(Rows); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Solicitudes_Modificada.AcceptChanges();
                    }
            }
            if (Dt_Solicitudes_Completa.Rows.Count <= 0)
            {
                Dt_Solicitudes_Completa.Columns.Add("No_Solicitud", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Referencia", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Banco", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Proveedor", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Monto", typeof(System.Decimal));
                Dt_Solicitudes_Completa.Columns.Add("Reserva", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Tipo_Solicitud_Pago", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Banco_Proveedor", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta_Contable_ID_Banco", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta_Contable_Proveedor", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Monto_Cedular", typeof(System.Decimal));
                Dt_Solicitudes_Completa.Columns.Add("Monto_ISR", typeof(System.Decimal));
                Dt_Solicitudes_Completa.Columns.Add("Monto_Transferencia", typeof(System.Decimal));
                Dt_Solicitudes_Completa.Columns.Add("Fecha_Poliza", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Ref_Comision", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Ref_IVA", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Anticipo", typeof(System.Decimal));
                Dt_Solicitudes_Completa.Columns.Add("Beneficiario_ID", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Tipo_Beneficiario_Poliza", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta_Proveedor_Bancario_ID", typeof(System.String));
            }
            foreach (DataRow Renglon in Dt_Solicitudes_Modificada.Rows)
            {
                DataRow ROW = Dt_Solicitudes_Completa.NewRow(); //Crea un nuevo registro a la tabla
                ROW["No_Solicitud"] = Renglon["No_Solicitud"].ToString();
                ROW["Referencia"] = Renglon["Referencia"].ToString();
                ROW["Banco"] = Renglon["Banco"].ToString();
                ROW["cuenta"] = Renglon["cuenta"].ToString();
                ROW["Proveedor"] = Renglon["Proveedor"].ToString();
                ROW["Monto"] = Renglon["Monto"].ToString();
                ROW["Reserva"] = Renglon["Reserva"].ToString();
                ROW["Tipo_Solicitud_Pago"] = Renglon["Tipo_Solicitud_Pago"].ToString();
                ROW["Banco_Proveedor"] = Renglon["BANCO_PROVEEDOR"].ToString();
                ROW["Fecha_Poliza"] = Renglon["Fecha_Poliza"].ToString();
                Rs_Ope_Con_Cheques.P_Banco_ID = Renglon["Cuenta"].ToString();
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
                if (Dt_Banco_Cuenta_Contable.Rows.Count > 0)// foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
                {
                    ROW["Cuenta_Contable_ID_Banco"] = Cuenta_Contable_ID_Banco = Dt_Banco_Cuenta_Contable.Rows[0][Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
                }
                Dt_Banco_Cuenta_Contable = new DataTable();
                Rs_Ope_Con_Cheques.P_Banco_ID = Renglon["Cuenta"].ToString();
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Proveedor_Bancario();
                if (Dt_Banco_Cuenta_Contable.Rows.Count > 0)// foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
                {
                    ROW["Cuenta_Proveedor_Bancario_ID"] = Cuenta_Contable_Proveedor_Banco_ID = Dt_Banco_Cuenta_Contable.Rows[0][Cat_Nom_Bancos.Campo_Cuenta_Contable_Proveedor].ToString();
                }
                Total_Cedular = 0;
                Total_ISR = 0;
                //  para Obtener la cuenta Contable del proveedor la cuenta contable
                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                foreach (DataRow Registro in Dt_Solicitud_Pagos.Rows)
                {
                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Proveedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                    {
                        ROW["Cuenta_Contable_Proveedor"] = Registro["Pro_Cuenta_Proveedor"].ToString();
                        ROW["Beneficiario_ID"] = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                        ROW["Tipo_Beneficiario_Poliza"] = "PROVEEDOR";
                    }
                    if (!String.IsNullOrEmpty(Registro["MONTO_CEDULAR"].ToString()))
                    {
                        Total_Cedular = Total_Cedular + Convert.ToDecimal(Registro["MONTO_CEDULAR"].ToString()); ;
                        Total_ISR = Total_Cedular + Convert.ToDecimal(Registro["MONTO_ISR"].ToString());
                    }
                    else
                    {
                        Total_Cedular = Total_Cedular;
                        Total_ISR = Total_ISR;
                    }
                }
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                if (Dt_Datos_Solicitud.Rows.Count > 0)
                {
                    foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                    {
                        if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                        {
                            Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                        }
                    }
                }
                ROW["Monto_Cedular"] = Total_Cedular;
                ROW["Monto_ISR"] = Total_ISR;
                ROW["Monto_Transferencia"] = Convert.ToString(Convert.ToDecimal(ROW["Monto"]) - ((Convert.ToDecimal(ROW["Monto_ISR"].ToString())) + Convert.ToDecimal(ROW["Monto_Cedular"].ToString())));
                ROW["Ref_Comision"] = Renglon["Ref_Comision"].ToString();
                ROW["Ref_IVA"] = Renglon["Ref_IVA"].ToString();
                ROW["Tipo_Beneficiario"] = Renglon["Tipo_Beneficiario"].ToString();
                Dt_Solicitudes_Completa.Rows.Add(ROW); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Solicitudes_Completa.AcceptChanges();
            }
            Proveedor = "";
            Banco = "";
            Cuenta = "";
            Cuenta_Contable_ID_Banco = "";
            Cuenta_Contable_Proveedor = "";
            Cuenta_Contable_Proveedor_Banco_ID = "";
            Tipo_Beneficiario = "";
            // Se agrupan los pagos por proveedor y cuenta 
            foreach (DataRow Fila in Dt_Solicitudes_Completa.Rows)
            {
                if (Dt_Solicitudes_Partidas.Rows.Count <= 0 && Dt_Solicitudes_Partidas.Columns.Count <= 0)
                {
                    Dt_Solicitudes_Partidas.Columns.Add("Referencia", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Banco", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Proveedor", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Monto", typeof(System.Decimal));
                    Dt_Solicitudes_Partidas.Columns.Add("Banco_Proveedor", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta_Contable_ID_Banco", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta_Contable_Proveedor", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta_Proveedor_Bancario_ID", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Monto_Cedular", typeof(System.Decimal));
                    Dt_Solicitudes_Partidas.Columns.Add("Monto_ISR", typeof(System.Decimal));
                    Dt_Solicitudes_Partidas.Columns.Add("Fecha_Poliza", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Ref_Comision", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Ref_IVA", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Anticipo", typeof(System.Decimal));
                    Dt_Solicitudes_Partidas.Columns.Add("Beneficiario_ID", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Tipo_Beneficiario_Poliza", typeof(System.String));
                }
                DataRow Filas = Dt_Solicitudes_Partidas.NewRow(); //Crea un nuevo registro a la tabla
                Agregar = true;
                Total = 0;
                Total_Cedular = 0;
                Total_ISR = 0;
                Proveedor = Fila["Proveedor"].ToString();
                Banco = Fila["Banco"].ToString();
                Cuenta = Fila["Cuenta"].ToString();
                Cuenta_Contable_ID_Banco = Fila["Cuenta_Contable_ID_Banco"].ToString();
                Cuenta_Contable_Proveedor_Banco_ID = Fila["Cuenta_Proveedor_Bancario_ID"].ToString();
                Cuenta_Contable_Proveedor = Fila["Cuenta_Contable_Proveedor"].ToString();
                Tipo_Beneficiario = Fila["Tipo_Beneficiario"].ToString();
                //insertar los que no se repiten
                foreach (DataRow Fila2 in Dt_Solicitudes_Partidas.Rows)
                {
                    if (Tipo_Beneficiario == "PROVEEDOR")
                    {
                        if (Proveedor.Equals(Fila2["PROVEEDOR"].ToString()) && Cuenta.Equals(Fila2["CUENTA"].ToString()) && Banco.Equals(Fila2["Banco"].ToString()) && Cuenta_Contable_ID_Banco.Equals(Fila2["Cuenta_Contable_ID_Banco"].ToString()) && Cuenta_Contable_Proveedor.Equals(Fila2["Cuenta_Contable_Proveedor"].ToString()) && Tipo_Beneficiario.Equals(Fila2["Tipo_Beneficiario"].ToString()))
                        {
                            Agregar = false;
                        }
                    }
                    else
                    {
                        if (Proveedor.Equals(Fila2["PROVEEDOR"].ToString()) && Cuenta.Equals(Fila2["CUENTA"].ToString()) && Banco.Equals(Fila2["Banco"].ToString()) && Cuenta_Contable_ID_Banco.Equals(Fila2["Cuenta_Contable_ID_Banco"].ToString()) && Cuenta_Contable_Proveedor.Equals(Fila2["Cuenta_Contable_Proveedor"].ToString()) && Tipo_Beneficiario.Equals(Fila2["Tipo_Beneficiario"].ToString()))
                        {
                            Agregar = false;
                        }
                    }

                }
                //se calcula el monto por tipo
                foreach (DataRow Renglon in Dt_Solicitudes_Completa.DefaultView.ToTable().Rows)
                {
                    if (Tipo_Beneficiario == "PROVEEDOR")
                    {
                        if (Proveedor.Equals(Renglon["PROVEEDOR"].ToString()) && Cuenta.Equals(Renglon["CUENTA"].ToString()) && Banco.Equals(Renglon["Banco"].ToString()) && Cuenta_Contable_ID_Banco.Equals(Renglon["Cuenta_Contable_ID_Banco"].ToString()) && Cuenta_Contable_Proveedor.Equals(Renglon["Cuenta_Contable_Proveedor"].ToString()) && Tipo_Beneficiario.Equals(Renglon["Tipo_Beneficiario"].ToString()))
                        {
                            Total = Total + Convert.ToDecimal(Renglon["Monto"].ToString());
                            if (!String.IsNullOrEmpty(Renglon["Monto_Cedular"].ToString()))
                            {
                                Total_Cedular = Total_Cedular + Convert.ToDecimal(Renglon["Monto_Cedular"].ToString());
                                Total_ISR = Total_ISR + Convert.ToDecimal(Renglon["Monto_ISR"].ToString());
                            }
                        }
                    }
                    else
                    {
                        if (Proveedor.Equals(Renglon["PROVEEDOR"].ToString()) && Cuenta.Equals(Renglon["CUENTA"].ToString()) && Banco.Equals(Renglon["Banco"].ToString()) && Cuenta_Contable_ID_Banco.Equals(Renglon["Cuenta_Contable_ID_Banco"].ToString()) && Cuenta_Contable_Proveedor.Equals(Renglon["Cuenta_Contable_Proveedor"].ToString()) && Tipo_Beneficiario.Equals(Renglon["Tipo_Beneficiario"].ToString()))
                        {
                            Total = Total + Convert.ToDecimal(Renglon["Monto"].ToString());
                            if (!String.IsNullOrEmpty(Renglon["Monto_Cedular"].ToString()))
                            {
                                Total_Cedular = Total_Cedular + Convert.ToDecimal(Renglon["Monto_Cedular"].ToString());
                                Total_ISR = Total_ISR + Convert.ToDecimal(Renglon["Monto_ISR"].ToString());
                            }
                        }
                    }

                }
                if (Agregar && Total > 0)
                {
                    Filas["Banco"] = Fila["Banco"].ToString();
                    Filas["cuenta"] = Fila["cuenta"].ToString();
                    Filas["Proveedor"] = Fila["Proveedor"].ToString();
                    Filas["Monto"] = Total;
                    Filas["Banco_Proveedor"] = Fila["Banco_Proveedor"].ToString();
                    Filas["Referencia"] = Fila["Referencia"].ToString();
                    Filas["Cuenta_Contable_ID_Banco"] = Fila["Cuenta_Contable_ID_Banco"].ToString();
                    Filas["Cuenta_Proveedor_Bancario_ID"] = Fila["Cuenta_Proveedor_Bancario_ID"].ToString();
                    Filas["Cuenta_Contable_Proveedor"] = Fila["Cuenta_Contable_Proveedor"].ToString().Trim();
                    Filas["Monto_Cedular"] = Total_Cedular;
                    Filas["Monto_ISR"] = Total_ISR;
                    Filas["Fecha_Poliza"] = Fila["Fecha_Poliza"].ToString().Trim();
                    Filas["Ref_Comision"] = Fila["Ref_Comision"].ToString().Trim();
                    Filas["Ref_IVA"] = Fila["Ref_IVA"].ToString().Trim();
                    Filas["Tipo_Beneficiario"] = Fila["Tipo_Beneficiario"].ToString().Trim();
                    Filas["Beneficiario_ID"] = Fila["Beneficiario_ID"].ToString().Trim();
                    Filas["Tipo_Beneficiario_Poliza"] = Fila["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Solicitudes_Partidas.Rows.Add(Filas); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Solicitudes_Partidas.AcceptChanges();
                    Total_Cedular = 0;
                    Total_ISR = 0;
                }
            }
            // Se realiza el registro del pago y de la poliza contable
            foreach (DataRow Registro in Dt_Solicitudes_Partidas.Rows)
            {
                partida = 0;
                Dt_Partidas_Polizas = null;
                Dt_Partidas_Polizas = new DataTable();
                if (Dt_Partidas_Polizas.Rows.Count <= 0 && Dt_Partidas_Polizas.Columns.Count <= 0)
                {
                    //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                }
                partida = partida + 1;
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Contable_ID_Banco"].ToString().Trim(); //Cuenta_Contable_ID_Banco;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Registro["Monto"].ToString().Trim()) - (Convert.ToDecimal(Registro["Monto_Cedular"].ToString().Trim()) + Convert.ToDecimal(Registro["Monto_ISR"].ToString().Trim()));//Convert.ToDouble(Monto) - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Referencia"].ToString().Trim();
                row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                partida = partida + 1;
                row = Dt_Partidas_Polizas.NewRow();
                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Contable_Proveedor"].ToString().Trim();//Cuenta_Contable_Proveedor;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Registro["Monto"].ToString().Trim()) - (Convert.ToDecimal(Registro["Monto_Cedular"].ToString().Trim()) + Convert.ToDecimal(Registro["Monto_ISR"].ToString().Trim()));//Convert.ToDouble(Monto) - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Referencia"].ToString().Trim();
                row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                //Se agrega la partida de iva pendiente de acreditar
                Dt_Cuentas_Iva = new DataTable();
                Rs_Parametros_Iva.P_Cmmd = Cmmd;
                Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                if (Dt_Cuentas_Iva.Rows.Count > 0)
                {
                    if (Total_Iva_por_Acreditar > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Acreditable = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Acreditable))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Acreditable;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Iva_por_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                            row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();

                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            partida = partida + 1;
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Pendiente_Acreditar = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Pendiente_Acreditar))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Pendiente_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                            row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                            row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();

                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            partida = partida + 1;
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
                Dt_Solicitudes_Partidas.DefaultView.RowFilter = "Proveedor='" + Registro["Proveedor"].ToString().Trim() + "' AND Cuenta='" + Registro["Cuenta"].ToString().Trim() + "' AND Tipo_Beneficiario='" + Registro["Tipo_Beneficiario"].ToString().Trim() + "'";
                Dt_Partidas_Polizas = Comprimir_Poliza(Dt_Partidas_Polizas);
                Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Ope_Con_Cheques.P_Dt_Datos_Completos = Dt_Solicitudes_Completa;
                Rs_Ope_Con_Cheques.P_Dt_Datos_Agrupados = Dt_Solicitudes_Partidas.DefaultView.ToTable();
                Rs_Ope_Con_Cheques.P_No_Partidas = Dt_Partidas_Polizas.Rows.Count.ToString();
                Rs_Ope_Con_Cheques.P_Comentario = "";
                    Rs_Ope_Con_Cheques.P_Monto_Comision = "0";
                    Rs_Ope_Con_Cheques.P_Monto_Iva = "0";
                Rs_Ope_Con_Cheques.P_Estatus = "PAGADO";
                Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MM/yy}", Convert.ToDateTime(Fecha_Pago)).ToString();
                Rs_Ope_Con_Cheques.P_Tipo_Pago = "TRANSFERENCIA";
                Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Nombre_Proveedor;//Dt_Solicitudes_Partidas.Rows[0]["PROVEEDOR"].ToString().Trim();
                Rs_Ope_Con_Cheques.P_Estatus_Comparacion = "PAGADO";
                Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MM/yy}", DateTime.Now).ToString();
                Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                Pago = Rs_Ope_Con_Cheques.Alta_Pago();
                Resultados = Pago.Substring(6, 2);
                Pago = Pago.Substring(0, 5);
                if (Resultados == "SI")
                {
                    //Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                }
                else
                {
                    break;
                }

            }
            if (P_Cmmd == null)
            {
                Trans.Commit();
            }
            return Resultados;
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }

        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO  : 21-Febrero-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    private DataTable Comprimir_Poliza(DataTable Dt_partidas)
    {
        DataTable Dt_Temporal = new DataTable();
        Int16 Cont_Partidas = 0;
        DataRow Fila;
        Int16 Partida = 1;
        Boolean Insertar_Registro = true;
        if (Dt_Temporal.Rows.Count <= 0 && Dt_Temporal.Columns.Count <= 0)
        {
            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
            Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
            Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
            Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
            Dt_Temporal.Columns.Add("Beneficiario_ID", typeof(System.String));
            Dt_Temporal.Columns.Add("Tipo_Beneficiario", typeof(System.String));
        }
        foreach (DataRow Row in Dt_partidas.Rows)
        {
            if (Cont_Partidas == 0)
            {
                Fila = Dt_Temporal.NewRow();
                //Agrega el cargo del registro de la póliza
                Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = Row[Ope_Con_Polizas_Detalles.Campo_Debe].ToString();
                Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = Row[Ope_Con_Polizas_Detalles.Campo_Haber].ToString();
                Fila[Ope_Con_Polizas_Detalles.Campo_Referencia] = Row[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString();
                Fila["Beneficiario_ID"] = Row["Beneficiario_ID"].ToString();
                Fila["Tipo_Beneficiario"] = Row["Tipo_Beneficiario"].ToString();
                Dt_Temporal.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Temporal.AcceptChanges();
                Partida++;
                Cont_Partidas++;
            }
            else
            {
                Insertar_Registro = true;
                foreach (DataRow Fila_Temporal in Dt_Temporal.Rows)
                {
                    if (Row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() == Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString())
                    {
                        if (Convert.ToDecimal(Row[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0 && Convert.ToDecimal(Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                        {
                            Fila_Temporal.BeginEdit();
                            Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + Convert.ToDecimal(Row[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                            Fila_Temporal.EndEdit();
                            Fila_Temporal.AcceptChanges();
                            Insertar_Registro = false;
                        }
                        else
                        {
                            if (Convert.ToDecimal(Row[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0 && Convert.ToDecimal(Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                            {
                                Fila_Temporal.BeginEdit();
                                Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + Convert.ToDecimal(Row[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                                Fila_Temporal.EndEdit();
                                Fila_Temporal.AcceptChanges();
                                Insertar_Registro = false;
                            }
                        }
                    }
                }
                if (Insertar_Registro)
                {
                    Fila = Dt_Temporal.NewRow();
                    //Agrega el cargo del registro de la póliza
                    Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                    Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = Row[Ope_Con_Polizas_Detalles.Campo_Debe].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = Row[Ope_Con_Polizas_Detalles.Campo_Haber].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Referencia] = Row[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString();
                    Fila["Beneficiario_ID"] = Row["Beneficiario_ID"].ToString();
                    Fila["Tipo_Beneficiario"] = Row["Tipo_Beneficiario"].ToString();
                    Dt_Temporal.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Temporal.AcceptChanges();
                    Partida++;
                }
            }
        }
        return Dt_Temporal;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Cheque_Click
    /// DESCRIPCION : Da de Alta el cheque
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 21/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Cheque_Click(object sender, EventArgs e)
    {
        Int32 Indice = 0;
        Int32 Indice_Solicitudes = 0; 
        Boolean Autorizado;
        Boolean Solicitud_por_Pagar;
        DataTable Dt_Solicitudes_Datos= new DataTable();
        try
        {
            foreach(GridViewRow Renglon_Grid in Grid_Pagos.Rows){
                GridView Grid_Proveedores = (GridView)Renglon_Grid.FindControl("Grid_Proveedores");
                foreach (GridViewRow Renglon_Grid_Proveedores in Grid_Proveedores.Rows)
                {
                    Indice++;
                    Grid_Proveedores.SelectedIndex = Indice;
                    Autorizado = ((CheckBox)Renglon_Grid_Proveedores.FindControl("Chk_Autorizado_Pago")).Checked;
                    if (Autorizado)
                    {
                        GridView Grid_Datos_Solicitud = (GridView)Renglon_Grid_Proveedores.FindControl("Grid_Datos_Solicitud");
                        foreach (GridViewRow Fila_Solicitudes in Grid_Datos_Solicitud.Rows)
                        {
                            Indice_Solicitudes++;
                            Grid_Datos_Solicitud.SelectedIndex = Indice_Solicitudes;
                            Solicitud_por_Pagar = ((CheckBox)Fila_Solicitudes.FindControl("Chk_Cheque")).Checked;
                            if (Solicitud_por_Pagar)
                            {
                                if (Dt_Solicitudes_Datos.Rows.Count <= 0 && Dt_Solicitudes_Datos.Columns.Count<=0)
                                {
                                    Dt_Solicitudes_Datos.Columns.Add("No_Solicitud", typeof(System.String));
                                    Dt_Solicitudes_Datos.Columns.Add("Proveedor", typeof(System.String));
                                    Dt_Solicitudes_Datos.Columns.Add("Monto",typeof(System.Decimal));
                                }
                                DataRow row = Dt_Solicitudes_Datos.NewRow(); //Crea un nuevo registro a la tabla
                                //Asigna los valores al nuevo registro creado a la tabla
                                row["No_Solicitud"] = ((CheckBox)Fila_Solicitudes.FindControl("Chk_Cheque")).CssClass;
                                row["Proveedor"] = Renglon_Grid_Proveedores.Cells[1].Text;
                                row["Monto"] = Convert.ToDecimal(Fila_Solicitudes.Cells[6].Text.Replace(",","").Replace("$",""));
                                Dt_Solicitudes_Datos.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Solicitudes_Datos.AcceptChanges();
                            }
                        }
                        Realizar_Cheque_De_Varias_Solicitudes(Dt_Solicitudes_Datos);
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #region (Layout Banorte)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Archivo_Banorte
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Archivo_Banorte( DataTable Dt_Datos, String Orden)
    {
        StringBuilder Archivo_Banorte_BB;//Variable que almacena todos registros de banorte.
        StringBuilder Archivo_Banorte_BP;//Variable que almacena todos registros de banorte.
        try
        {
            Dt_Datos.DefaultView.RowFilter = "PESTANIA='BB'";
            if (Dt_Datos.DefaultView.ToTable().Rows.Count > 0)
            {
                Archivo_Banorte_BB = Obtener_Encabezado_Archivo_Banorte_BB(Dt_Datos);
                Guardar_Archivo(Archivo_Banorte_BB, "LAYOUT_BANORTE", Orden + "_BB", "1");
            }
            Dt_Datos.DefaultView.RowFilter = "PESTANIA='BP'";
            if (Dt_Datos.DefaultView.ToTable().Rows.Count > 0)
            {
                Archivo_Banorte_BP = Obtener_Encabezado_Archivo_Banorte_BP(Dt_Datos);
                Guardar_Archivo(Archivo_Banorte_BP, "../Contabilidad/LAYOUT_BANORTE", Orden + "_BP", "2");
            }
            Inicializa_Controles();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el archivo de dispersion a banorte. Error: [" + Ex.Message + "");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Encabezado_Archivo_Banorte
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private StringBuilder Obtener_Encabezado_Archivo_Banorte_BB( DataTable Dt_Datos)
    {
        StringBuilder Cadena_Padre = null;
        String Fecha_Proceso = String.Empty;
        String Orden = String.Empty;
        String Tipo = String.Empty;
        StringBuilder Header = new StringBuilder();

        try
        {
            Fecha_Proceso = String.Format("{0:yyyyMMdd}",DateTime.Now);
            Orden = Txt_Orden.Text.ToString();
            if (Orden.Substring(0) == "p" || Orden.Substring(0) == "P")
            {
                Tipo="Bte - Psrvc";
            }
            else
            {
                if (Orden.Substring(0) == "G" || Orden.Substring(0) == "g")
                {
                    Tipo = "Bte - Gxpagar";
                }
                else
                {
                    if (Orden.Substring(0) == "r" || Orden.Substring(0) == "R")
                    {
                        Tipo = "Bte - Rgasto";
                    }
                    else
                    {
                        if (Orden.Substring(0) == "f" || Orden.Substring(0) == "F")
                        {
                            Tipo = "Bte - Ffijo";
                        }
                        else
                        {
                            Tipo = "Bte - Pprov";
                        }
                    }
                }
            }
            Header.Append(Fecha_Proceso);
            Header.Append(" ");
            Header.Append(Orden);
            Header.Append(" ");
            Header.Append(Tipo);
            Header.Append("\r\n");
            Cadena_Padre = new StringBuilder(Header.ToString() + Obtener_Detalle_Archivo_Banorte_BB(Dt_Datos));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el encabezado del archivo de BANORTE. Error: [" + Ex.Message + "]");
        }
        return Cadena_Padre;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Detalle_Archivo_Banorte
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private StringBuilder Obtener_Detalle_Archivo_Banorte_BB(DataTable Dt_Datos)
    {
        StringBuilder Registros = new StringBuilder();//Registros que contendra el archivo .
        StringBuilder Cadena_BB = null;
        try
        {
            Dt_Datos.DefaultView.RowFilter = "PESTANIA ='BB'";
            if (Dt_Datos.DefaultView.ToTable().Rows.Count > 0)
                {
                    foreach (DataRow Proveedor in Dt_Datos.DefaultView.ToTable().Rows)
                    {
                        Registros.Append(Proveedor["TIPO"].ToString());
                        Registros.Append(Proveedor["ID"].ToString());
                        Registros.Append("\r\n");
                        Registros.Append(Proveedor["CUENTA ORIGEN"].ToString());
                        Registros.Append(Proveedor["CUENTA DESTINO"].ToString());
                        Registros.Append(Proveedor["IMPORTE"].ToString());
                        Registros.Append(Proveedor["ORDEN"].ToString());
                        Registros.Append(Proveedor["CONCEPTO"].ToString());
                        Registros.Append("\r\n");
                        Registros.Append(Proveedor["TM"].ToString());
                        Registros.Append(Proveedor["RFC MUNICIPIO"].ToString());
                        Registros.Append(Proveedor["CEROS"].ToString());
                        Registros.Append(Proveedor["EMAIL"].ToString());
                        Registros.Append("\r\n");
                        Registros.Append(Proveedor["FECHA"].ToString());
                        Registros.Append(Proveedor["BENEFICIARIO"].ToString());
                        Registros.Append("\r\n");
                        Registros.Append("\r\n");
                    }
                }
            Cadena_BB = new StringBuilder(Registros.ToString());
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el encabezado del archivo de BANORTE. Error: [" + Ex.Message + "]");
        }
        return Cadena_BB;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Encabezado_Archivo_Banorte_BP
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private StringBuilder Obtener_Encabezado_Archivo_Banorte_BP(DataTable Dt_Datos)
    {
        StringBuilder Cadena_Padre = null;
        String Fecha_Proceso = String.Empty;
        String Orden = String.Empty;
        String Tipo = String.Empty;
        StringBuilder Header = new StringBuilder();

        try
        {
            Fecha_Proceso = String.Format("{0:yyyyMMdd}", DateTime.Now);
            Orden = Txt_Orden.Text.ToString();
            if (Orden.Substring(0) == "p" || Orden.Substring(0) == "P")
            {
                Tipo = "Bte - Psrvc";
            }
            else
            {
                if (Orden.Substring(0) == "G" || Orden.Substring(0) == "g")
                {
                    Tipo = "Bte - Gxpagar";
                }
                else
                {
                    if (Orden.Substring(0) == "r" || Orden.Substring(0) == "R")
                    {
                        Tipo = "Bte - Rgasto";
                    }
                    else
                    {
                        if (Orden.Substring(0) == "f" || Orden.Substring(0) == "F")
                        {
                            Tipo = "Bte - Ffijo";
                        }
                        else
                        {
                            Tipo = "Bte - Pprov";
                        }
                    }
                }
            }
            Header.Append(Fecha_Proceso);
            Header.Append(" ");
            Header.Append(Orden);
            Header.Append(" ");
            Header.Append(Tipo);
            Header.Append("\r\n");
            Cadena_Padre = new StringBuilder(Header.ToString() + Obtener_Detalle_Archivo_Banorte_BP(Dt_Datos));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el encabezado del archivo de BANORTE. Error: [" + Ex.Message + "]");
        }
        return Cadena_Padre;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Detalle_Archivo_Banorte_BP
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private StringBuilder Obtener_Detalle_Archivo_Banorte_BP(DataTable Dt_Datos)
    {
        StringBuilder Registros = new StringBuilder();//Registros que contendra el archivo .
        StringBuilder Cadena_BP = null;
        try
        {
            Dt_Datos.DefaultView.RowFilter = "PESTANIA ='BP'";
            if (Dt_Datos.DefaultView.ToTable().Rows.Count > 0)
            {
                foreach (DataRow Proveedor in Dt_Datos.DefaultView.ToTable().Rows)
                {
                    Registros.Append(Proveedor["TIPO"].ToString());
                    Registros.Append(Proveedor["ID"].ToString());
                    Registros.Append("\r\n");
                    Registros.Append(Proveedor["CUENTA ORIGEN"].ToString());
                    Registros.Append(Proveedor["CUENTA DESTINO"].ToString());
                    Registros.Append(Proveedor["IMPORTE"].ToString());
                    Registros.Append(Proveedor["ORDEN"].ToString());
                    Registros.Append(Proveedor["CONCEPTO"].ToString());
                    Registros.Append(Proveedor["TM"].ToString());
                    Registros.Append(Proveedor["RFC MUNICIPIO"].ToString());
                    Registros.Append(Proveedor["CEROS"].ToString());
                    Registros.Append(Proveedor["EMAIL"].ToString());
                    Registros.Append("\r\n");
                    Registros.Append(Proveedor["FECHA"].ToString());
                    Registros.Append(Proveedor["BENEFICIARIO"].ToString());
                    Registros.Append("\r\n");
                }
            }
            Cadena_BP = new StringBuilder(Registros.ToString());
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el encabezado del archivo de BANORTE. Error: [" + Ex.Message + "]");
        }
        return Cadena_BP;
    }
    private void Guardar_Archivo(StringBuilder Mensaje, String Nombre_Carpeta, String Nombre_Archivo,String Tipo)
    {
        String Ruta_Guardar_Archivo = "";//Variable que almacenará la ruta completa donde se guardara el log de la generacion de la nómina.
        String[] Archivos;

        try
        {
            //Obtenemos la ruta donde se guardara el log de layout.
            Ruta_Guardar_Archivo = Server.MapPath(Nombre_Carpeta);
            //Verificamos si el directorio del log existe, en caso contrario se crea. 
            if (!Directory.Exists(Ruta_Guardar_Archivo))
                Directory.CreateDirectory(Ruta_Guardar_Archivo);

            Archivos = Directory.GetFiles(Ruta_Guardar_Archivo);

            //Validamos que exista el archivo.
            if (Archivos.Length >= 1)
            {
                foreach (String Archivo in Archivos)
                {
                    //Eliminamos el archivo.
                    if (Archivo.Equals(Ruta_Guardar_Archivo+"\\" + Nombre_Archivo+".txt"))
                    {
                        File.Delete(Archivo);
                    }
                    //File.Delete(Archivo);
                }
            }

            Escribir_Archivo(Ruta_Guardar_Archivo, "/" + Nombre_Archivo, ".txt", Mensaje);

            if (File.Exists(@Ruta_Guardar_Archivo + "/" + Nombre_Archivo + ".txt"))
            {
                if (Tipo == "1")
                {
                    Mostrar_Archivo(Nombre_Carpeta + "/" + Nombre_Archivo + ".txt");
                }
                else
                {
                    Mostrar_Archivo_2(Nombre_Carpeta + "/" + Nombre_Archivo + ".txt");
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al guardar el archivo de dispersion. Error: [" + Ex.Message + "]");
        }
    }
    public static void Escribir_Archivo(String Ruta, String Nombre_Archivo, String Extencion, StringBuilder Cadena)
    {
        StreamWriter Escribir_Archivo = null;//Escritor, variable encargada de escribir el archivo que almacenará el historial de la nómina generada.

        try
        {
            Escribir_Archivo = new StreamWriter(@"" + (Ruta + Nombre_Archivo + Extencion), true, Encoding.UTF8);
            Escribir_Archivo.WriteLine(Cadena.ToString());
            Escribir_Archivo.Close();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al escribir el Archivo " + Nombre_Archivo + ". Error: [" + Ex.Message + "]");
        }
    }
    private void Mostrar_Archivo(String URL)
    {
        String Pagina = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL;
        try
        {

            ScriptManager.RegisterStartupScript(this, this.GetType(), "B1",
                "window.open('" + Pagina + "', 'A','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1,height=1');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "B", "window.location='Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL + "';", true);
           //Response.Redirect("Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL, false);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el archivo de dispersion generado generado. Error: [" + Ex.Message + "]");
        }
    }
    private void Mostrar_Archivo_2(String URL)
    {
        try
        {
            String Pagina = "../Nomina/Frm_Mostrar_Archivos.aspx?Documento="+URL;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "B2",
                "window.open('" + Pagina + "', 'B','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1,height=1');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "A", "window.location='../Nomina/Frm_Mostrar_Archivos.aspx?Documento=" + URL + "';", true);
           // Response.Redirect("../nomina/Frm_Mostrar_Archivos.aspx?Documento=" + URL, false);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el archivo de dispersion generado generado. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
    #region (Layout Bajio)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Archivo_Bajio
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  21/Mayo/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Archivo_Bajio(DataTable Dt_Datos_Encabezado, DataTable Dt_Datos_Detalles, DataTable Dt_Datos_Pie)
    {
        StringBuilder Archivo_BB= new StringBuilder();//Variable que almacena todos registros de bajio.
        try
        {
            if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
            {
                Archivo_BB = Obtener_Encabezado_Archivo_BB(Dt_Datos_Encabezado, Dt_Datos_Detalles, Dt_Datos_Pie);
                Guardar_Archivo(Archivo_BB, "LAYOUT_BAJIO", "pagos_" + String.Format("{0:ddMMyyyy}", DateTime.Now), "1");
            }
            Inicializa_Controles();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el archivo de dispersion a banorte. Error: [" + Ex.Message + "");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Encabezado_Archivo_BB
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  21/Mayo/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private StringBuilder Obtener_Encabezado_Archivo_BB(DataTable Dt_Datos_Encabezado, DataTable Dt_Datos_Detalles, DataTable Dt_Datos_Pie)
    {
        StringBuilder Cadena_Padre = new StringBuilder();
        String Fecha_Proceso = String.Empty;
        String Orden = String.Empty;
        String Tipo = String.Empty;
        StringBuilder Header = new StringBuilder();
        try
        {
            Header.Append(Dt_Datos_Encabezado.Rows[0]["ENCABEZADO"].ToString());
            Header.Append(Dt_Datos_Encabezado.Rows[0]["ID"].ToString());
            Header.Append(Dt_Datos_Encabezado.Rows[0]["FECHA"].ToString());
            Header.Append(Dt_Datos_Encabezado.Rows[0]["ARCHIVO"].ToString());
            Header.Append("\r\n");
            Cadena_Padre = new StringBuilder(Header.ToString() + Obtener_Detalle_Archivo_BB(Dt_Datos_Detalles,Dt_Datos_Pie));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el encabezado del archivo de BANORTE. Error: [" + Ex.Message + "]");
        }
        return Cadena_Padre;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Detalle_Archivo_BB
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private StringBuilder Obtener_Detalle_Archivo_BB(DataTable Dt_Datos_Detalles, DataTable Dt_Datos_Pie)
    {
        StringBuilder Registros = new StringBuilder();//Registros que contendra el archivo .
        StringBuilder Cadena_BB = null;
        try
        {
            if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
            {
                foreach (DataRow Proveedor in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                {
                    Registros.Append(Proveedor["DETALLE"].ToString());
                    Registros.Append(Proveedor["NO_SECUENCIA"].ToString());
                    Registros.Append(Proveedor["TIPO_CUENTA_EMISOR"].ToString());
                    Registros.Append(Proveedor["NO_CUENTA_EMISOR"].ToString());
                    Registros.Append(Proveedor["DATO_01"].ToString());
                    Registros.Append(Proveedor["BANCO_RECEPTOR"].ToString());
                    Registros.Append(Proveedor["MONTO_OPERACION"].ToString());
                    Registros.Append(Proveedor["FECHA_APLICA"].ToString());
                    Registros.Append(Proveedor["MEDIO_PAGO"].ToString());
                    Registros.Append(Proveedor["TIPO_CUENTA_RECEPTOR"].ToString());
                    Registros.Append(Proveedor["NO_CUENTA_RECEPTOR"].ToString());
                    Registros.Append(Proveedor["CEROS"].ToString());
                    Registros.Append(Proveedor["ALIAS"].ToString());
                    Registros.Append(Proveedor["IVA"].ToString());
                    Registros.Append(Proveedor["REFERENCIA"].ToString());
                    Registros.Append("\r\n");
                }
                Registros.Append(Dt_Datos_Pie.Rows[0]["SUMARIO"].ToString());
                Registros.Append(Dt_Datos_Pie.Rows[0]["NO_SECUENCIA"].ToString());
                Registros.Append(Dt_Datos_Pie.Rows[0]["NO_OPERACIONES"].ToString());
                Registros.Append(Dt_Datos_Pie.Rows[0]["MONTO_TOTAL"].ToString());
            }
            Cadena_BB = new StringBuilder(Registros.ToString());
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el encabezado del archivo de BAJIO. Error: [" + Ex.Message + "]");
        }
        return Cadena_BB;
    }
    
    #endregion
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Modificar_Numero_Folio
    ///DESCRIPCIÓN: Resibe la informacion para poder modificar el folio Actual
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  31/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Numero_Folio(SqlCommand P_Cmmd, String Banco_ID)
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Modificar_Folio_Actual = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Int32 Folio = 0;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            Folio = Convert.ToInt32(Txt_No_Cheque.Text);
            Folio++;
            if (Cmb_Cuenta_Banco.SelectedValue != "")
            {
                Rs_Modificar_Folio_Actual.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            }
            else
            {
                Rs_Modificar_Folio_Actual.P_Banco_ID = Banco_ID;
            }
            Rs_Modificar_Folio_Actual.P_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Folio_Actual.P_Folio_Actual = "" + Folio;
            Rs_Modificar_Folio_Actual.P_Cmmd = Cmmd;
            Rs_Modificar_Folio_Actual.Modificar_Folio_Actual();
            if (P_Cmmd == null)
            {
                Trans.Commit();
            }
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (Exception ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
            throw new Exception(ex.Message, ex);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                if (Txt_No_Pago.Text == "")
                {
                    Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                    Llenar_Combos_Generales();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pagos", "alert('EL Cheque solo se puede modificar');", true);
                }
            }
            else
            {
                //Valida los datos ingresados por el usuario.
                if (Validaciones(true))
                {
                    Alta_Cheque();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Cheques", "alert('El alta del Pago fue exitoso');", true);
                    Inicializa_Controles();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Metodo que permite modificar la reserva
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/Noviembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
            }
            else
            {
                if (Validar_Modificacion(true))
                {
                    Modificar_Cheque(); //Modifica los datos de la Cuenta Contable con los datos proporcionados por el usuario
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Text = "";
            Img_Error.Visible = false;

            Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            Limpia_Controles();//Limpia los controles de la forma
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Comentar_Click
    ///DESCRIPCIÓN: Se cancela la solicitud de pago 
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO  : 21-mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Comentar_Click(object sender, EventArgs e)
    {
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Tipo_Solicitud = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        DataTable Dt_Datos_Polizas = new DataTable();
        DataTable Dt_Tipo_Solicitud = new DataTable();
        Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
        ReportDocument Reporte = new ReportDocument();
        String Tipo_Solicitud = "";
        String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
        String Nombre_Archivo = "Rechazado";// +Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
        String Usuario = "";
        String Servicios_Generales = "";
        DataTable Dt_solicitud = new DataTable();
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        String Resultado = "";
        // crear transaccion para crear el convenio 
        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
        Cn.Open();
        Trans = Cn.BeginTransaction();
        Cmmd.Connection = Cn;
        Cmmd.Transaction = Trans;
        try
        {
            DataRow Row;
            DataTable Dt_Reporte = new DataTable();
            if (Txt_Comentario.Text != "")
            {
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud_Autorizar.Value;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Dt_solicitud = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                //revisar si la solicitud es de servicios masivos generales 
                if (!String.IsNullOrEmpty(Dt_solicitud.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString().Trim()))
                {
                    Servicios_Generales = "SI";
                }
                else
                {
                    Servicios_Generales = "NO";
                } 
                DataTable Dt_Tipo = new DataTable();
                Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                Rs_Consulta.P_Cmmd = Cmmd;
                Dt_Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos();
                if (Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().ToUpper() != "FINIQUITO" || Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().Substring(0, 4).ToUpper() != "FINI")
                {
                    if (Servicios_Generales == "NO")
                    {
                        Resultado = Cancela_Solicitud_Pago(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(), Cmmd);
                    }
                    else
                    {
                        Resultado = Cancela_Solicitud_Pago_Servicios_Generales(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(), Cmmd);
                    }
                }
                else
                {
                    Resultado = Cancela_Solicitud_Pago_Finiquito(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(), Cmmd);
                }
                if (Resultado == "SI")
                {
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud_Autorizar.Value;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                    Trans.Commit();
                    //  se crea la tabla para el reporte de la cancelacion
                    Dt_Reporte.Columns.Add("NO_SOLICITUD", typeof(System.String));
                    Dt_Reporte.Columns.Add("NO_RESERVA", typeof(System.String));
                    Dt_Reporte.Columns.Add("PROVEEDOR", typeof(System.String));
                    Dt_Reporte.Columns.Add("MONTO", typeof(System.Double));
                    Dt_Reporte.Columns.Add("FECHA_CREO", typeof(System.DateTime));
                    Dt_Reporte.Columns.Add("FECHA_RECHAZO", typeof(System.DateTime));
                    Dt_Reporte.Columns.Add("TIPO_SOLICITUD_PAGO_ID", typeof(System.String));
                    Dt_Reporte.Columns.Add("CONCEPTO_SOLICITUD", typeof(System.String));
                    Dt_Reporte.Columns.Add("COMENTARIO", typeof(System.String));
                    Dt_Reporte.Columns.Add("USUARIO_CREO", typeof(System.String));
                    Dt_Reporte.Columns.Add("USUARIO_RECHAZO", typeof(System.String));
                    Dt_Reporte.TableName = "Dt_Cancelacion";
                    foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
                    {
                        Row = Dt_Reporte.NewRow();

                        Row["NO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString());
                        Row["NO_RESERVA"] = (Registro["Reserva"].ToString());
                        if (!String.IsNullOrEmpty(Registro["Proveedor"].ToString()))
                        {
                            Row["PROVEEDOR"] = (Registro["Empleado"].ToString());
                        }
                        else
                        {
                            Row["PROVEEDOR"] = (Registro["Proveedor"].ToString());
                        }                        
                        Row["MONTO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                        Row["FECHA_CREO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo].ToString());
                        Row["FECHA_RECHAZO"] = "" + DateTime.Now;

                        //  para el tipo de solicitud
                        Rs_Tipo_Solicitud.P_Tipo_Solicitud = (Registro[Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString());
                        Dt_Tipo_Solicitud = Rs_Tipo_Solicitud.Consulta_Tipo_Solicitud();

                        foreach (DataRow Tipo in Dt_Tipo_Solicitud.Rows)
                        {
                            Tipo_Solicitud = (Tipo[Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion].ToString());
                        }
                        Row["TIPO_SOLICITUD_PAGO_ID"] = Tipo_Solicitud;
                        Row["CONCEPTO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString());
                        Row["COMENTARIO"] = Txt_Comentario.Text;
                        Usuario = (Registro[Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo].ToString());
                        Usuario = Usuario.Replace(".", "");
                        Row["USUARIO_CREO"] = Usuario;
                        Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                        Usuario = Usuario.Replace(".", "");
                        Row["USUARIO_RECHAZO"] = Usuario;
                        Dt_Reporte.Rows.Add(Row);
                        Dt_Reporte.AcceptChanges();
                    }
                    Ds_Reporte.Clear();
                    Ds_Reporte.Tables.Clear();
                    Ds_Reporte.Tables.Add(Dt_Reporte.Copy());
                    Reporte.Load(Ruta_Archivo + "Rpt_Con_Cancelacion_Recepcion.rpt");
                    Reporte.SetDataSource(Ds_Reporte);
                    DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                    Nombre_Archivo += ".pdf";
                    Ruta_Archivo = @Server.MapPath("../../Reporte/");
                    m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                    ExportOptions Opciones_Exportacion = new ExportOptions();
                    Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);

                    Abrir_Ventana(Nombre_Archivo);
                    Recepcion_Documentos_Inicio();
                }
                
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);   
            }
            else
            {
                Trans.Rollback();
                Recepcion_Documentos_Inicio();
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";

            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
        finally
        {
            Cn.Close();
        }
    }
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Recepcion_Documentos_Inicio
    ///DESCRIPCIÓN          : Inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Diciembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Recepcion_Documentos_Inicio()
    {
        try
        {
            Limpia_Controles();
            Llenar_Grid_Solicitudes_Pendientes();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO  : 21-Febrero-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    private void Abrir_Ventana(String Nombre_Archivo)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
        try
        {
            Pagina = Pagina + Nombre_Archivo;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 24/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Cancela_Solicitud_Pago(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand P_Cmmd)
    {
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Honorarios = new DataTable();
        DataTable Dt_Arrendamientos = new DataTable();
        DataTable Dt_Hon_Asimilables = new DataTable();
        DataRow Registro_Honorarios;
        DataRow Registro_Arrendamiento;
        DataRow Registro_Hon_Asimila;
        Decimal Total_Iva_por_Acreditar = 0;
        Decimal Total_ISR_Honorarios = 0;
        Decimal Total_Cedular_Honorarios = 0;
        Decimal Total_ISR_Arrendamientos = 0;
        Decimal Total_Cedular_Arrendamientos = 0;
        Decimal Total_Hono_asimi = 0;
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        String Cuenta_ISR = "0";
        String Cuenta_Cedular = "0";
        String CTA_IVA_PENDIENTE = "";
        Int32 partida = 0;
        DataRow row;
        String Resultado = "";
        String Beneficiario_ID = "";
        String Tipo_Beneficiario = "";
        String Tipo_Comprobacion = "";
        Double Monto_total = 0;
        Decimal Iva_Por_Partida = 0;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        //int Partida = 0;
        try
        {
            //Agregar las partidas de los impuestos de isr y de retencion de iva si tiene honorarios la solicitud de pago
            //se crean las columnas de los datatable de honorarios y de arrendamientos
            if (Dt_Honorarios.Rows.Count == 0)
            {
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Decimal));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Decimal));
            }
            if (Dt_Arrendamientos.Rows.Count == 0)
            {
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Decimal));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Decimal));
            }
            if (Dt_Hon_Asimilables.Rows.Count == 0)
            {
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Decimal));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Decimal));
            }

            Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
            Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
            if (Dt_Datos_Solicitud.Rows.Count > 0)
            {
                foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                {
                    if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                    {
                        Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                    }
                }
                //se recorre el dataset de los detalles para dividir los detalles por tipo de operacion 
                foreach (DataRow fila in Dt_Datos_Solicitud.Rows)
                {
                    if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "HONORARIOS")
                    {
                        Registro_Honorarios = Dt_Honorarios.NewRow();
                        Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                        Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                        Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                        Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                        Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                        Dt_Honorarios.Rows.Add(Registro_Honorarios); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Honorarios.AcceptChanges();
                    }
                    if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "ARRENDAMIENTO")
                    {
                        Registro_Arrendamiento = Dt_Arrendamientos.NewRow();
                        Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                        Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                        Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                        Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                        Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                        Dt_Arrendamientos.Rows.Add(Registro_Arrendamiento); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Arrendamientos.AcceptChanges();
                    }
                    if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "HON. ASIMILABLE")
                    {
                        Registro_Hon_Asimila = Dt_Hon_Asimilables.NewRow();
                        Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                        Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                        Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                        Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                        Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                        Dt_Hon_Asimilables.Rows.Add(Registro_Hon_Asimila); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Hon_Asimilables.AcceptChanges();
                    }
                }
                Total_ISR_Honorarios = 0;
                Total_Cedular_Honorarios = 0;
                if (Dt_Honorarios.Rows.Count > 0)
                {
                    foreach (DataRow fila in Dt_Honorarios.Rows)
                    {
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                        {
                            Total_ISR_Honorarios = Total_ISR_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                        }
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                        {
                            Total_Cedular_Honorarios = Total_Cedular_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                        }

                    }
                }
                Total_ISR_Arrendamientos = 0;
                Total_Cedular_Arrendamientos = 0;
                if (Dt_Arrendamientos.Rows.Count > 0)
                {
                    foreach (DataRow fila in Dt_Arrendamientos.Rows)
                    {
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                        {
                            Total_ISR_Arrendamientos = Total_ISR_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                        }
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                        {
                            Total_Cedular_Arrendamientos = Total_Cedular_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                        }
                    }
                }
                Total_Hono_asimi = 0;
                if (Dt_Hon_Asimilables.Rows.Count > 0)
                {
                    foreach (DataRow fila in Dt_Hon_Asimilables.Rows)
                    {
                        if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                        {
                            Total_Hono_asimi = Total_Hono_asimi + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                        }
                    }
                }
            }
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
            String Tipo_Poliza_ID = "";
            foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
            {
                if (!String.IsNullOrEmpty(Registro["Monto_Partida"].ToString()))
                {
                    if (!String.IsNullOrEmpty(Registro["IVA_MONTO"].ToString()))
                    {
                        Iva_Por_Partida = Convert.ToDecimal(Registro["IVA_MONTO"].ToString());
                    }
                    else
                    {
                        Iva_Por_Partida = 0;
                    }
                    partida = partida + 1;
                    Txt_Monto_Solicitud.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                    if (!String.IsNullOrEmpty(Registro["Empleado_Cuenta"].ToString()))
                    {
                        Txt_Cuenta_Contable_ID_Empleado.Value = Registro["Empleado_Cuenta"].ToString();
                        Beneficiario_ID = Registro[Cat_Empleados.Campo_Empleado_ID].ToString();
                        Tipo_Beneficiario = "EMPLEADO";
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Acreedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "A")
                        {
                            Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Acreedor"].ToString();
                            Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                            Tipo_Beneficiario = "PROVEEDOR";
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Contratista"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "C")
                            {
                                Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Contratista"].ToString();
                                Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                Tipo_Beneficiario = "PROVEEDOR";
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Deudor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "D")
                                {
                                    Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Deudor"].ToString();
                                    Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                    Tipo_Beneficiario = "PROVEEDOR";
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Judicial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "J")
                                    {
                                        Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Judicial"].ToString();
                                        Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                        Tipo_Beneficiario = "PROVEEDOR";
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Nomina"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "N")
                                        {
                                            Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Nomina"].ToString();
                                            Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                            Tipo_Beneficiario = "PROVEEDOR";
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Predial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "Z")
                                            {
                                                Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Predial"].ToString();
                                                Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                                Tipo_Beneficiario = "PROVEEDOR";
                                            }
                                            else
                                            {
                                                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Proveedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                                                {
                                                    Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Proveedor"].ToString();
                                                    Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                                    Tipo_Beneficiario = "PROVEEDOR";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!String.IsNullOrEmpty(Registro["Tipo_Solicitud_Pago_ID"].ToString()))
                    {
                        Tipo_Poliza_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                        Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                        Rs_Consulta.P_Cmmd = Cmmd;
                        Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                    }
                    //Txt_Monto_Solicitud.Value = Registro["Monto"].ToString();
                    //Txt_Concepto_Solicitud.Value = Registro["Concepto"].ToString();
                    Txt_Cuenta_Contable_reserva.Value = Registro["Cuenta_Contable_Reserva"].ToString();
                    Txt_No_Reserva.Value = Registro["NO_RESERVA"].ToString();
                    //if (Tipo_Poliza_ID != "00001" && Tipo_Poliza_ID != "00003")
                    //{
                    if (Tipo_Comprobacion == "NO")
                    {
                        //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                        if (Dt_Partidas_Polizas.Rows.Count == 0)
                        {
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
                            Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                            Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                        }
                        row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                        //Agrega el cargo del registro de la póliza
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_reserva.Value;
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Registro["Monto_Partida"].ToString()) + Convert.ToDecimal(Registro["Monto_ISR"].ToString()) + Convert.ToDecimal(Registro["Monto_Cedular"].ToString()) - Iva_Por_Partida;
                        row["Beneficiario_ID"] = Beneficiario_ID;
                        row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                        Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Partidas_Polizas.AcceptChanges();
                        Monto_total = Monto_total + (Convert.ToDouble(Registro["Monto_Partida"].ToString()) + Convert.ToDouble(Registro["Monto_ISR"].ToString()) + Convert.ToDouble(Registro["Monto_Cedular"].ToString()) - Convert.ToDouble(Iva_Por_Partida));
                    }
                }
            }
            if (Tipo_Comprobacion == "NO")
            {
                //se consultan los detalles de la solicitud de pago
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                if (Dt_Datos_Solicitud.Rows.Count > 0)
                {
                    Total_ISR_Honorarios = 0;
                    Total_Cedular_Honorarios = 0;
                    if (Dt_Honorarios.Rows.Count > 0)
                    {
                        foreach (DataRow fila in Dt_Honorarios.Rows)
                        {
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                            {
                                Total_ISR_Honorarios = Total_ISR_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                            }
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                            {
                                Total_Cedular_Honorarios = Total_Cedular_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                            }
                        }
                    }
                    Total_ISR_Arrendamientos = 0;
                    Total_Cedular_Arrendamientos = 0;
                    if (Dt_Arrendamientos.Rows.Count > 0)
                    {
                        foreach (DataRow fila in Dt_Arrendamientos.Rows)
                        {
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                            {
                                Total_ISR_Arrendamientos = Total_ISR_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                            }
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                            {
                                Total_Cedular_Arrendamientos = Total_Cedular_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                            }
                        }
                    }
                    row = Dt_Partidas_Polizas.NewRow();
                    partida = partida + 1;
                    //Agrega el abono del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Proveedor.Value.ToString()))
                    {
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Proveedor.Value;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Empleado.Value.ToString()))
                        {
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Empleado.Value;
                        }
                    }
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Txt_Monto_Solicitud.Value.ToString()) - (Total_Cedular_Arrendamientos + Total_Cedular_Honorarios + Total_ISR_Arrendamientos + Total_ISR_Honorarios);
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row["Beneficiario_ID"] = Beneficiario_ID;
                    row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                    // se recorre el datatable de honorarios para realizar la sumatoria de los impuestos en las variables correspondientes 
                    //y asi obtener el total de los impuestos de cada tipo
                    Rs_Parametros.P_Cmmd = Cmmd;
                    Dt_Cuentas = Rs_Parametros.Consulta_Datos_Parametros();
                    if (Dt_Honorarios.Rows.Count > 0)
                    {
                        if (Total_ISR_Honorarios > 0)
                        {
                            // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                            
                            if (Dt_Cuentas.Rows.Count > 0)
                            {
                                Cuenta_ISR = Dt_Cuentas.Rows[0]["CTA_ISR"].ToString().Trim();
                            }
                            if (!String.IsNullOrEmpty(Cuenta_ISR))
                            {
                                partida = partida + 1;
                                row = Dt_Partidas_Polizas.NewRow();
                                //Agrega el cargo del registro de la póliza
                                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_ISR_Honorarios;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                row["Beneficiario_ID"] = Beneficiario_ID;
                                row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Partidas_Polizas.AcceptChanges();
                            }                            
                        }
                        if (Total_Cedular_Honorarios > 0)
                        {
                            // CUENTA IMPUESTO CEDULAR 1% HONORARIOS RET consultamos el id que tiene la cuenta en el sistema
                            if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Cedular = Dt_Cuentas.Rows[0]["CTA_Cedular"].ToString().Trim();
                                }
                            if (!String.IsNullOrEmpty(Cuenta_Cedular))
                            {
                                partida = partida + 1;
                                row = Dt_Partidas_Polizas.NewRow();
                                //Agrega el cargo del registro de la póliza
                                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cedular;
                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Cedular_Honorarios;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                row["Beneficiario_ID"] = Beneficiario_ID;
                                row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Partidas_Polizas.AcceptChanges();
                            }
                        }
                    }
                    // se recorre el datatable de honorarios para realizar la sumatoria de los impuestos en las variables correspondientes 
                    //y asi obtener el total de los impuestos de cada tipo
                    if (Dt_Arrendamientos.Rows.Count > 0)
                    {
                        Cuenta_Cedular = "";
                        Cuenta_ISR = "";
                        if (Total_ISR_Arrendamientos > 0)
                        {
                            // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                             if (Dt_Cuentas.Rows.Count > 0)
                               {
                                    Cuenta_ISR = Dt_Cuentas.Rows[0]["ISR_Arrendamiento"].ToString().Trim();
                                }
                             if (!String.IsNullOrEmpty(Cuenta_ISR))
                             {
                                 partida = partida + 1;
                                 row = Dt_Partidas_Polizas.NewRow();
                                 //Agrega el cargo del registro de la póliza
                                 row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                 row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                                 row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                 row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_ISR_Arrendamientos;
                                 row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                 row["Beneficiario_ID"] = Beneficiario_ID;
                                 row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                 Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                 Dt_Partidas_Polizas.AcceptChanges();
                             }
                        }
                        if (Total_Cedular_Arrendamientos > 0)
                        {
                            // CUENTA IMPUESTO CEDULAR 1% HONORARIOS RET consultamos el id que tiene la cuenta en el sistema
                            if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Cedular = Dt_Cuentas.Rows[0]["Cedular_Arrendamiento"].ToString().Trim();
                                }
                            if (!String.IsNullOrEmpty(Cuenta_Cedular))
                            {
                                partida = partida + 1;
                                row = Dt_Partidas_Polizas.NewRow();
                                //Agrega el cargo del registro de la póliza
                                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cedular;
                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Cedular_Arrendamientos;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                row["Beneficiario_ID"] = Beneficiario_ID;
                                row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Partidas_Polizas.AcceptChanges();
                            }
                            
                        }
                    }

                    //Se agrega la partida de iva pendiente de acreditar
                    Rs_Parametros_Iva.P_Cmmd = Cmmd;
                    Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                    if (Dt_Cuentas_Iva.Rows.Count > 0)
                    {
                        if (Total_Iva_por_Acreditar > 0)
                        {
                            // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                            if (Dt_Cuentas_Iva.Rows.Count > 0)
                            {
                                CTA_IVA_PENDIENTE = Dt_Cuentas_Iva.Rows[0]["CTA_IVA_PENDIENTE"].ToString().Trim();
                            }
                            if (!String.IsNullOrEmpty(CTA_IVA_PENDIENTE))
                            {
                                partida = partida + 1;
                                row = Dt_Partidas_Polizas.NewRow();
                                //Agrega el cargo del registro de la póliza
                                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_PENDIENTE;
                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                                row["Beneficiario_ID"] = Beneficiario_ID;
                                row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Partidas_Polizas.AcceptChanges();
                            }
                        }
                    }
                }
                //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(partida);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PRE-DOCUMENTADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Contabi = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Contabilidad = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Contabilidad = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Monto_total + Convert.ToDouble(Total_Iva_por_Acreditar);// Convert.ToDouble(Txt_Monto_Solicitud.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Resultado = Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
            }
            else
            {
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PRE-DOCUMENTADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd; 
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago_Sin_Poliza();
                Resultado = "SI";
                
            }
            if (P_Cmmd == null)
            {
                Trans.Commit();
            }
            return Resultado;
        }
        catch (Exception ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }

        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago_Servicios_Generales
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 24/Septiembre/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Cancela_Solicitud_Pago_Servicios_Generales(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand P_Cmmd)
    {
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Honorarios = new DataTable();
        DataTable Dt_Arrendamientos = new DataTable();
        DataTable Dt_Hon_Asimilables = new DataTable();
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        Double Monto_total = 0;
        String CTA_IVA_PENDIENTE = "";
        String Beneficiario_ID = "";
        String Tipo_Beneficiario = "";
        Int32 partida = 0;
        Decimal Total_Iva_por_Acreditar = 0;
        String Resultado = "";
        String Tipo_Comprobacion = "";
        DataRow row;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        //int Partida = 0;
        try
        {
            Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
            Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
            if (Dt_Datos_Solicitud.Rows.Count > 0)
            {
                foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                {
                    if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                    {
                        Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                    }
                }
            }
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago_Servicios_Generales();
            String Tipo_Poliza_ID = "";
            foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
            {
                if (!String.IsNullOrEmpty(Registro["Monto_Partida"].ToString()))
                {
                    partida = partida + 1;
                    Txt_Monto_Solicitud.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                    if (!String.IsNullOrEmpty(Registro["Empleado_Cuenta"].ToString()))
                    {
                        Txt_Cuenta_Contable_ID_Empleado.Value = Registro["Empleado_Cuenta"].ToString();
                        Beneficiario_ID = Registro[Cat_Empleados.Campo_Empleado_ID].ToString();
                        Tipo_Beneficiario = "EMPLEADO";
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Proveedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                        {
                            Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Proveedor"].ToString();
                            Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                            Tipo_Beneficiario = "PROVEEDOR";
                        }
                    }
                    if (!String.IsNullOrEmpty(Registro["Tipo_Solicitud_Pago_ID"].ToString()))
                    {
                        Tipo_Poliza_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                        Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                        Rs_Consulta.P_Cmmd = Cmmd;
                        Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                    }
                    Txt_Cuenta_Contable_reserva.Value = Registro["Cuenta_Contable_Reserva"].ToString();
                    Txt_No_Reserva.Value = Registro["NO_RESERVA"].ToString();
                    if (Tipo_Comprobacion == "NO")
                    {
                        //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                        if (Dt_Partidas_Polizas.Rows.Count == 0)
                        {
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
                            Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                            Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                        }
                        row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                        //Agrega el cargo del registro de la póliza
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_reserva.Value;
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Registro["Monto_Partida"].ToString());
                        row["Beneficiario_ID"] = Beneficiario_ID;
                        row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                        Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Partidas_Polizas.AcceptChanges();
                        Monto_total = Monto_total + (Convert.ToDouble(Registro["Monto_Partida"].ToString()));
                    }
                }
            }
            if (Tipo_Comprobacion == "NO")
            {
                //se consultan los detalles de la solicitud de pago
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                if (Dt_Datos_Solicitud.Rows.Count > 0)
                {
                    row = Dt_Partidas_Polizas.NewRow();
                    partida = partida + 1;
                    //Agrega el abono del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Proveedor.Value.ToString()))
                    {
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Proveedor.Value;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Empleado.Value.ToString()))
                        {
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Empleado.Value;
                        }
                    }
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Txt_Monto_Solicitud.Value.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row["Beneficiario_ID"] = Beneficiario_ID;
                    row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                    //Se agrega la partida de iva pendiente de acreditar
                    Rs_Parametros_Iva.P_Cmmd = Cmmd;
                    Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                    if (Dt_Cuentas_Iva.Rows.Count > 0)
                    {
                        if (Total_Iva_por_Acreditar > 0)
                        {
                            // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                            if (Dt_Cuentas_Iva.Rows.Count > 0)
                            {
                                CTA_IVA_PENDIENTE = Dt_Cuentas_Iva.Rows[0]["CTA_IVA_PENDIENTE"].ToString().Trim();
                            }
                            if (!String.IsNullOrEmpty(CTA_IVA_PENDIENTE))
                            {
                                partida = partida + 1;
                                row = Dt_Partidas_Polizas.NewRow();
                                //Agrega el cargo del registro de la póliza
                                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_PENDIENTE;
                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                                row["Beneficiario_ID"] = Beneficiario_ID;
                                row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Partidas_Polizas.AcceptChanges();
                            }
                        }
                    }
                }
                //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(partida);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PRE-DOCUMENTADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Servicios_Generales = "SI";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Contabi = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Contabilidad = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Contabilidad = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_recepcion_Documentos = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Jefe = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Monto_total + Convert.ToDouble(Total_Iva_por_Acreditar);// Convert.ToDouble(Txt_Monto_Solicitud.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Resultado = Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
            }
            else
            {
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago_Sin_Poliza();
                Resultado = "SI";
            }
            return Resultado;
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }

        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago_Finiquito
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Cancela_Solicitud_Pago_Finiquito(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand P_Cmmd)
    {
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Honorarios = new DataTable();
        DataTable Dt_Arrendamientos = new DataTable();
        DataTable Dt_Hon_Asimilables = new DataTable();
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        String Beneficiario_ID = "";
        String Tipo_Beneficiario = "";
        Int32 partida = 0;
        String Resultado = "";
        String Concepto_Solicitud = "";
        DataRow row;
        DataTable Dt_Deducciones = new DataTable();
        DataTable Dt_Datos_Deduccones = new DataTable();
        DataRow Registro_Deduccion;
        DataTable Dt_Cuenta_Empleado = new DataTable();
        DataTable Dt_Deducciones_Persepciones = new DataTable();
        Decimal Total_Debe = 0;
        Decimal Total_Monto = 0;
        Decimal Total_Haber = 0;
        Decimal Total_Partidas = 0;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        //int Partida = 0;
        try
        {
            if (Dt_Deducciones.Rows.Count == 0)
            {
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Partida_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Fte_Financiamiento_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Area_Funcional_ID, typeof(System.String));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Debe, typeof(System.Double));
                Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Haber, typeof(System.Double));
            }
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
            if (Dt_Datos_Polizas.Rows.Count > 0)
            {
                //Se consultan las deducciones y persepciones de la solicitud de pago
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                Dt_Datos_Deduccones = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud_Finiquito();
                if (Dt_Datos_Deduccones.Rows.Count > 0)
                {
                    foreach (DataRow Fila_Deduccion in Dt_Datos_Deduccones.Rows)
                    {
                        Registro_Deduccion = Dt_Deducciones.NewRow();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Partida_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Partida_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Fte_Financiamiento_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Fte_Financiamiento_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Proyecto_Programa_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Proyecto_Programa_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Area_Funcional_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Area_Funcional_ID].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString().Trim();
                        Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString().Trim();
                        Dt_Deducciones.Rows.Add(Registro_Deduccion); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Deducciones.AcceptChanges();
                    }
                }
            }
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
            if (Dt_Datos_Polizas.Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Dt_Datos_Polizas.Rows[0]["Monto"].ToString()))
                {
                    Total_Partidas = Convert.ToDecimal(Dt_Datos_Polizas.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                    Txt_Monto_Solicitud.Value = Dt_Datos_Polizas.Rows[0]["Monto"].ToString();
                    Dt_Cuenta_Empleado = Rs_Parametros_Iva.Obtener_Parametros_Poliza();//se optiene la cuenta del empleado
                    if (Dt_Cuenta_Empleado.Rows.Count > 0)
                    {
                        Txt_Cuenta_Contable_ID_Empleado.Value = Dt_Cuenta_Empleado.Rows[0][Cat_Nom_Parametros_Poliza.Campo_Cuenta_Servicios_Profecionales_Pagar_A_CP].ToString();
                    }
                    else
                    {
                        Txt_Cuenta_Contable_ID_Empleado.Value = "";
                    }
                    Beneficiario_ID = Dt_Datos_Polizas.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString();
                    Tipo_Beneficiario = "EMPLEADO";
                    Concepto_Solicitud = Dt_Datos_Polizas.Rows[0]["Concepto"].ToString();
                    Txt_No_Reserva.Value = Dt_Datos_Polizas.Rows[0]["NO_RESERVA"].ToString();
                }
            }
            if (Txt_Cuenta_Contable_ID_Empleado.Value != "")
            {
                //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                if (Dt_Partidas_Polizas.Rows.Count == 0)
                {
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
                    Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                }
                ///Se consultan los detalles de las deducciones y persepciones del finiquito
                Dt_Deducciones_Persepciones = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consultar_Datos_Solicitud_Finiquito();
                if (Dt_Deducciones_Persepciones.Rows.Count > 0)
                {
                    foreach (DataRow Fila_D_P in Dt_Deducciones_Persepciones.Rows)
                    {
                        if (Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString()) != 0)
                        {
                            partida = partida + 1;
                            Total_Debe = Total_Debe + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString());
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID].ToString();
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString());
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        else
                        {
                            partida = partida + 1;
                            Total_Haber = Total_Haber + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString());
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID].ToString();
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString());
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
                Total_Monto = Total_Debe - Total_Haber;
                row = Dt_Partidas_Polizas.NewRow();
                partida = partida + 1;
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Empleado.Value.ToString()))
                {
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Empleado.Value;
                }
                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = //Txt_Concepto_Solicitud.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Monto;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row["Beneficiario_ID"] = Beneficiario_ID;
                row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();

                //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(partida);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PRE-DOCUMENTADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Contabi = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Contabilidad = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Contabilidad = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_recepcion_Documentos = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Jefe = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Convert.ToDouble(Txt_Monto_Solicitud.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Resultado = Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Finiquito(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
            }
            else
            {
                Resultado = "NO,PE";
            }
            return Resultado;
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }

        }
    }
    protected void Btn_Cancelar_Click(object sender, EventArgs e)
    {
        try
        {
            Recepcion_Documentos_Inicio();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Banco_OnSelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  31/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Banco_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
            Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Cuenta_Banco, Dt_Consulta, "CUENTA", "BANCO_ID");
            Txt_No_Cheque.Text = "";
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Cuenta_Banco_OnSelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  10/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Cuenta_Banco_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        Int32 Folio_Actual = 0;
        Int32 Folio_Final = 0;
        Boolean Estado_Folio = false;
        try
        {
            if (Cmb_Banco.SelectedIndex > 0)
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;

                Txt_No_Cheque.Text = "";
                Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();

                //  obtendra el numero de folio de los cheques
                if (Dt_Consulta.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                            Folio_Actual = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()));

                        else
                            Estado_Folio = true;

                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()))
                            Folio_Final = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()));

                        else
                            Estado_Folio = true;
                    }
                }
                //  si contiene informacion
                if (Estado_Folio == false)
                {
                    if (Folio_Actual <= Folio_Final)
                        Txt_No_Cheque.Text = "" + Folio_Actual;

                    //  si el folio se pasa del final manda un mensaje
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Se terminaron los folios del Talonario de cheques actual por favor ingrese nuevos folios";
                    }
                }
                //  no tiene asignado los numeros de folio de los chuques
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Por favor defina el rango de los folio del Talonario de cheques para poder utilizarlos en esta ventana";
                }
            }
            else
            {
                Txt_No_Cheque.Text = "";
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Solicitud_Pagos_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
    ///PARAMETROS           :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 22/mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Proveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
       //Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
       //Cls_Ope_Con_Cheques_Negocio Solicitud_Proveedor = new Cls_Ope_Con_Cheques_Negocio();
       Cls_Cat_Con_Proveedores_Negocio Datos_Proveedor = new Cls_Cat_Con_Proveedores_Negocio();
                DataTable Dt_Detalles = new DataTable();
                DataTable Dt_Documentos = new DataTable();
                DataTable Dt_Proveedor = new DataTable();
                DataTable Dt_Datos_Proveedor = new DataTable();
                try
                {
                    GridView grid_proveedor = sender as GridView;
                    if (grid_proveedor.SelectedIndex > (-1))
                    {
                        Datos_Proveedor.P_Proveedor_ID = grid_proveedor.SelectedRow.Cells[1].Text.Trim();
                                Dt_Datos_Proveedor=Datos_Proveedor.Consulta_Datos_Proveedores();
                                if (Dt_Datos_Proveedor.Rows.Count >0)
                                {
                                    Txt_Banco_Proveedor.Text = Dt_Datos_Proveedor.Rows[0]["BANCO_PROVEEDOR"].ToString().Trim();
                                    Txt_Clabe.Text = Dt_Datos_Proveedor.Rows[0]["CLABE"].ToString().Trim();
                                    Txt_Numero_Cuenta.Text = Dt_Datos_Proveedor.Rows[0]["CUENTA"].ToString().Trim();
                                    Txt_Padron_ID.Text = Dt_Datos_Proveedor.Rows[0]["PROVEEDOR_ID"].ToString().Trim();
                                }
                                Mpe_Proveedor.Show();
                    }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Solicitud_Detalles_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
    ///PARAMETROS           :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 22/mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Datos_Solicitud_Detalles_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
        Cls_Ope_Con_Cheques_Negocio Solicitud_Proveedor = new Cls_Ope_Con_Cheques_Negocio();
        Cls_Cat_Con_Proveedores_Negocio Datos_Proveedor = new Cls_Cat_Con_Proveedores_Negocio();
        DataTable Dt_Detalles = new DataTable();
        DataTable Dt_Documentos = new DataTable();
        DataTable Dt_Proveedor = new DataTable();
        DataTable Dt_Datos_Proveedor = new DataTable();
        try
        {
            GridView Grid_Solicitudes_Datos = sender as GridView;
            if (Grid_Solicitudes_Datos.SelectedIndex > (-1))
            {
                Solicitud_Negocio.P_No_Solicitud_Pago = Grid_Solicitudes_Datos.SelectedRow.Cells[2].Text.Trim();
                Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
                Dt_Documentos = Solicitud_Negocio.Consulta_Documentos();
                if (Dt_Detalles != null)
                {
                    if (Dt_Detalles.Rows.Count > 0)
                    {

                        Txt_No_Pago_Det.Text = Dt_Detalles.Rows[0]["NO_SOLICITUD_PAGO"].ToString().Trim();
                        Txt_No_Reserva_Det.Text = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString().Trim();
                        Txt_Concepto_Reserva_Det.Text = Dt_Detalles.Rows[0]["CONCEPTO_RESERVA"].ToString().Trim();
                        Txt_Beneficiario_Det.Text = Dt_Detalles.Rows[0]["BENEFICIARIO"].ToString().Trim();
                        Txt_Fecha_Solicitud_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_SOLICITUD"]);
                        Txt_Monto_Solicitud_Det.Text = String.Format("{0:c}", Dt_Detalles.Rows[0]["MONTO"]);
                        Txt_No_poliza_Det.Text = Dt_Detalles.Rows[0]["NO_POLIZA"].ToString().Trim();
                        if (String.IsNullOrEmpty(Txt_No_poliza_Det.Text))
                        {
                            Tr_Poliza.Style.Add("Display", "none");
                        }
                        else
                        {
                            Tr_Poliza.Style.Add("Display", "block");
                        }
                        Grid_Documentos.Columns[3].Visible = true;
                        Grid_Documentos.Columns[4].Visible = true;
                        Grid_Documentos.DataSource = Dt_Documentos;
                        Grid_Documentos.DataBind();
                        Grid_Documentos.Columns[3].Visible = false;
                        Grid_Documentos.Columns[4].Visible = false;
                        Mpe_Detalles.Show();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : RBL_Orden_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
    ///PARAMETROS           :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 22/mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void RBL_Orden_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Llenar_Grid_Solicitudes_Pendientes();
            if (Cmb_Forma_Pago.SelectedItem.Text == "Cheque")
            {
                 Tr_Btn_Transferir.Style.Add("display","none");// = false;
                Txt_Orden.Enabled = false;
                Btn_Cheque.Visible = true;
                Lbl_Seleccion_Masiva.Visible = false;
                chkAll.Visible = false;
                Chk_Leyeda.Visible = true;
                Chk_Leyeda.Checked = false;
                Lbl_Leyenda.Visible = true;
                Lbl_Total_Cheque.Visible = true;
                Txt_Total_Cheque.Text = "0";
                Txt_Total_Cheque.Visible = true;
                Chk_Generar_Layout.Visible = false;
                Lbl_Generar_Layaout.Visible = false;
                Tr_Btn_Transferir_Sin_Layout.Style.Add("display", "none");// = false;
                
            }
            else
            {
                Tr_Btn_Transferir.Style.Add("display", "block");// = false;
                Txt_Orden.Enabled = true;
                Btn_Cheque.Visible = false;
                Lbl_Seleccion_Masiva.Visible = true;
                chkAll.Visible = true;
                Chk_Leyeda.Visible = false;
                Chk_Leyeda.Checked = false;
                Lbl_Leyenda.Visible = false;
                Lbl_Total_Cheque.Visible = true;
                Txt_Total_Cheque.Text = "0";
                Txt_Total_Cheque.Visible = true;
                Chk_Generar_Layout.Visible = true;
                Lbl_Generar_Layaout.Visible = true;
                Chk_Generar_Layout.Checked = true;
               Tr_Btn_Transferir_Sin_Layout.Style.Add("display", "none");// = false;
                
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Documentos_RowDataBound
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  05-Marzo-2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        HyperLink Hyp_Lnk_Ruta;
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                if (!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) && e.Row.Cells[4].Text.Trim() != "&nbsp;")
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[4].Text.Trim();
                    Hyp_Lnk_Ruta.Enabled = true;
                }
                else
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "";
                    Hyp_Lnk_Ruta.Enabled = false;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #region (Cheque impreso)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Convertir_Cantidad_Letras
    ///DESCRIPCIÓN: Convierte una cantidad en letra
    ///PARAMETROS: 
    ///CREO:        
    ///FECHA_CREO:  15-Marzo-2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected String Convertir_Cantidad_Letras(Decimal Cantidad_Numero)
    {
        Numalet Obj_Numale = new Numalet();
        String Cantidad_Letra = String.Empty;

        try
        {
            Obj_Numale.MascaraSalidaDecimal = "00/100 M.N.";
            Obj_Numale.SeparadorDecimalSalida = "pesos";
            Obj_Numale.ApocoparUnoParteEntera = true;
            Cantidad_Letra = Obj_Numale.ToCustomCardinal(Cantidad_Numero).Trim().ToUpper();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al convertir la cantidad a letras. Error:[" + Ex.Message + "]");
        }
        return Cantidad_Letra;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String Cheque, Decimal Monto, SqlCommand P_Cmmd)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Cabecera = new DataTable();
        String Letra = "";
        String Banco_ID = "";
        String No_Solicitud_Pago = "";

        Cls_Ope_Con_Cheques_Negocio Rs_Cheques = new Cls_Ope_Con_Cheques_Negocio();
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            Cls_Ope_Con_Cheques_Negocio Datos_Cheque = new Cls_Ope_Con_Cheques_Negocio();
            Ds_Reporte = new DataSet();
            Datos_Cheque.P_No_Cheque = Cheque;
            Datos_Cheque.P_Cmmd = Cmmd;
            Dt_Pagos = Datos_Cheque.Consulta_Pago_Cheque();
            Letra = Convertir_Cantidad_Letras(Monto);
            if (Dt_Temporal.Rows.Count <= 0)
            {
                Dt_Temporal.Columns.Add("NO_PAGO", typeof(System.String));
                Dt_Temporal.Columns.Add("MONTO", typeof(System.Decimal));
                Dt_Temporal.Columns.Add("BENEFICIARIO", typeof(System.String));
                Dt_Temporal.Columns.Add("MONTO_LETRA", typeof(System.String));
                Dt_Temporal.Columns.Add("NO_CHEQUE", typeof(System.String));
                Dt_Temporal.Columns.Add("ABONO_BENEFICIARIO", typeof(System.String));

                Dt_Temporal.TableName = "Dt_Cheque";
            }
            foreach (DataRow Registro in Dt_Pagos.Rows)
            {
                DataRow row = Dt_Temporal.NewRow(); //Crea un nuevo registro a la tabla
                row["NO_PAGO"] = Registro["NO_PAGO"].ToString();
                row["MONTO"] = Monto;
                if (Registro["BENEFICIARIO_PAGO"].ToString().Contains('-'))
                {
                    row["BENEFICIARIO"] = Registro["BENEFICIARIO_PAGO"].ToString().Substring(2);
                }
                else
                {
                    row["BENEFICIARIO"] = Registro["BENEFICIARIO_PAGO"].ToString();
                }
                Banco_ID = Registro["BANCO_ID"].ToString();
                No_Solicitud_Pago = Registro["NO_SOLICITUD_PAGO"].ToString();
                row["MONTO_LETRA"] = Letra;
                row["NO_CHEQUE"] = Registro["NO_CHEQUE"].ToString();
                if (!String.IsNullOrEmpty(Registro[Ope_Con_Pagos.Campo_Abono_Cuenta_Beneficiario].ToString()))
                {
                    if (Registro[Ope_Con_Pagos.Campo_Abono_Cuenta_Beneficiario].ToString() == "SI")
                    {
                        row["ABONO_BENEFICIARIO"] = "PARA ABONO EN CUENTA DEL BENEFICIARIO";
                    }
                    else
                    {
                        row["ABONO_BENEFICIARIO"] = "";
                    }
                }
                else
                {
                    row["ABONO_BENEFICIARIO"] = "";
                }
                Dt_Temporal.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Temporal.AcceptChanges();
            }
            if (Dt_Pagos.Rows.Count > 0)
            {
                //Ds_Reporte.Tables.Add(Dt_Cabecera);
                Ds_Reporte.Tables.Add(Dt_Temporal.Copy());
                Ds_Rpt_Con_Cheque Obj_Data_SET = new Ds_Rpt_Con_Cheque();

                //Se llama al método que ejecuta la operación de generar el reporte.
                string nombre_pdf = "Reporte_Cheques" + Cheque + ".pdf";
                Generar_Reporte(Ds_Reporte, Obj_Data_SET, "Rpt_Con_Cheque.rpt", nombre_pdf);
            }
            //Insertar en la tabla de ope_con_cheques 
            Rs_Cheques.P_No_Cheque = Dt_Temporal.Rows[0]["NO_CHEQUE"].ToString();
            Rs_Cheques.P_No_Pago = Dt_Temporal.Rows[0]["NO_PAGO"].ToString();
            Rs_Cheques.P_Banco_ID = Banco_ID;
            Rs_Cheques.P_Estatus = "PENDIENTE";
            Rs_Cheques.P_Concepto = "CHEQUE PARA EL PAGO DE LA SOLICITUD" + No_Solicitud_Pago;
            Rs_Cheques.P_Monto = Dt_Temporal.Rows[0]["MONTO"].ToString();
            Rs_Cheques.P_Monto_Letra = Letra;
            Rs_Cheques.P_Beneficiario_Pago = Dt_Temporal.Rows[0]["BENEFICIARIO"].ToString();
            Rs_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.ToString();
            Rs_Cheques.P_Cmmd = Cmmd;
            Rs_Cheques.Alta_Seguimiento_Cheque();
            if (P_Cmmd == null)
            {
                Trans.Commit();
            }
        }
        //}
        catch (Exception Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            Lbl_Mensaje_Error.Visible = true;
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    #endregion
    #region Metodos Reportes
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Nombre_PDF)
    {
       try
        {

            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Contabilidad/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            String Ruta = "../../Reporte/" + Nombre_PDF;
            Mostrar_Reporte(Nombre_PDF, "PDF");
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte_Generar;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pagos", "alert('Imprimiendo cheque No. " + Txt_No_Cheque.Text.Trim() + ".');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Rpt_Excel
    /// DESCRIPCION :   Se encarga de generar el archivo de excel pasandole los paramentros
    ///                 al documento
    /// PARAMETROS  :   Dt_situacion.- Es la consulta que contiene las notas esf
    ///                 Ds_Actividades.- Es la consulta que contiene las notas era
    ///                 Ds_Variaciones.- Es la consulta que contiene las notas vhp
    ///                 Ds_Flujo.- Es la consulta que contiene las notas efe
    /// CREO        :   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   07/Marzo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Generar_Rpt_Excel(DataTable Dt_Reporte)
    {
        WorksheetCell Celda = new WorksheetCell();
        WorksheetCell Celda2 = new WorksheetCell();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Documentos = new DataTable();
        String Ruta_Archivo = "";
        String Nombre_Archivo = "";

        try
        {

            Nombre_Archivo = "Transferencias_banco";
            //  Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            //  propiedades del libro
            Libro.Properties.Title = "LATOUT TB-"+Txt_Orden.Text+"-11";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI_Irapuato";

            //  Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("Encabezado");
            //  Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera_Izquierda = Libro.Styles.Add("Encabezado_Izquierda");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Indice = Libro.Styles.Add("Contenido_Indice");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Nombre = Libro.Styles.Add("Contenido_Nombre");
            ////  Creamos el estilo cabecera 2 para la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Nombre_Negritas = Libro.Styles.Add("Contenido_Nombre_Negritas");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Firma = Libro.Styles.Add("Contenido_Firma");

            #region Estilos
            //***************************************inicio de los estilos***********************************************************
            //  estilo para la cabecera    Encabezado
            Estilo_Cabecera.Font.FontName = "Arial";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#000000";
            Estilo_Cabecera.Interior.Color = "white";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.None;
            //  estilo para la cabecera    Encabezado_Izquierda
            Estilo_Cabecera_Izquierda.Font.FontName = "Arial";
            Estilo_Cabecera_Izquierda.Font.Size = 10;
            Estilo_Cabecera_Izquierda.Font.Bold = true;
            Estilo_Cabecera_Izquierda.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera_Izquierda.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera_Izquierda.Alignment.Rotate = 0;
            Estilo_Cabecera_Izquierda.Font.Color = "#000000";
            Estilo_Cabecera_Izquierda.Interior.Color = "white";
            Estilo_Cabecera_Izquierda.Interior.Pattern = StyleInteriorPattern.None;
            //estilo para el contenido   contenido_indice
            //estilo para el contenido   contenido_indice
            Estilo_Contenido_Indice.Font.FontName = "Arial";
            Estilo_Contenido_Indice.Font.Size = 10;
            Estilo_Contenido_Indice.Font.Bold = false;
            Estilo_Contenido_Indice.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Indice.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Contenido_Indice.Alignment.Rotate = 0;
            Estilo_Contenido_Indice.Font.Color = "#000000";
            Estilo_Contenido_Indice.Interior.Color = "White";
            Estilo_Contenido_Indice.Interior.Pattern = StyleInteriorPattern.None;
            //estilo para el    Contenido_Nombre
            Estilo_Contenido_Nombre.Font.FontName = "Arial";
            Estilo_Contenido_Nombre.Font.Size = 10;
            Estilo_Contenido_Nombre.Font.Bold = true;
            Estilo_Contenido_Nombre.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido_Nombre.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre.Font.Color = "#000000";
            Estilo_Contenido_Nombre.Interior.Color = "white";
            Estilo_Contenido_Nombre.Interior.Pattern = StyleInteriorPattern.None;
            //estilo para el    Contenido_Firma
            Estilo_Contenido_Nombre_Negritas.Font.FontName = "Arial";
            Estilo_Contenido_Nombre_Negritas.Font.Size = 10;
            Estilo_Contenido_Nombre_Negritas.Font.Bold = true;
            Estilo_Contenido_Nombre_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido_Nombre_Negritas.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre_Negritas.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre_Negritas.Font.Color = "#000000";
            Estilo_Contenido_Nombre_Negritas.Interior.Color = "white";
            Estilo_Contenido_Nombre_Negritas.Interior.Pattern = StyleInteriorPattern.None;
            //estilo para el    Contenido_Nombre Negritas
            Estilo_Contenido_Firma.Font.FontName = "Arial";
            Estilo_Contenido_Firma.Font.Size = 10;
            Estilo_Contenido_Firma.Font.Bold = true;
            Estilo_Contenido_Firma.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido_Firma.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Firma.Alignment.Rotate = 0;
            Estilo_Contenido_Firma.Font.Color = "#000000";
            Estilo_Contenido_Firma.Interior.Color = "white";
            Estilo_Contenido_Firma.Interior.Pattern = StyleInteriorPattern.None;
            //*************************************** fin de los estilos***********************************************************
            #endregion

            #region Hojas
            //  Creamos una hoja que tendrá la BB
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("BB");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //  para la hoja de BP
            CarlosAg.ExcelXmlWriter.Worksheet Hoja_Ea = Libro.Worksheets.Add("BP");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            #endregion
            #region
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(20));//   1 tipo.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  2 id.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  3 cuenta origen.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  4 cuenta destino.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  5 importe.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//  6 orden.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//  7 concepto.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(20));//   8 tm.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//  9 rfc municipio.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  10 ceros.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//  11 email.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  12 fecha.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(400));//  13 beneficiario.
            //  Agregamos las columnas que tendrá la hoja de excel.

            #region ENCABEZADOS
            Celda = Renglon.Cells.Add("TIPO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("ID");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("CUENTA ORIGEN");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("CUENTA DESTINO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("IMPORTE");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("ORDEN");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("CONCEPTO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("TM");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("RFC MUNICIPIO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("CEROS");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("EMAIL");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("FECHA");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("BENEFICIARIO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Renglon = Hoja.Table.Rows.Add();

            #endregion

            Dt_Reporte.DefaultView.RowFilter = "PESTANIA='BB'";
            foreach (DataRow Dr in Dt_Reporte.DefaultView.ToTable().Rows )
            {
                Renglon = Hoja.Table.Rows.Add();

                foreach (DataColumn Columna in Dt_Reporte.DefaultView.ToTable().Columns)
                {
                        if (Columna is DataColumn)
                        {
                            if (Columna.ColumnName.ToString() != "PESTANIA")
                            {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Contenido_Indice"));
                            }
                          }
                }
            }
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//   1 tipo.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  2 id.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//  3 cuenta origen.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//  4 cuenta destino.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  5 importe.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  6 orden.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//  7 concepto.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//   8 tm.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  9 rfc municipio.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  10 ceros.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//  11 email.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  12 fecha.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(400));//  13 beneficiario.
            #region ENCABEZADOS Hoja_Ea
            Celda2 = Renglon_Ea.Cells.Add("TIPO");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("ID");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("CUENTA ORIGEN");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("CUENTA DESTINO");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("IMPORTE");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("ORDEN");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("CONCEPTO");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("TM");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("RFC MUNICIPIO");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("CEROS");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("EMAIL");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("FECHA");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("BENEFICIARIO");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Renglon_Ea = Hoja_Ea.Table.Rows.Add();

            #endregion

            Dt_Reporte.DefaultView.RowFilter = "PESTANIA='BP'";

            foreach (DataRow Dr in Dt_Reporte.DefaultView.ToTable().Rows)
            {
                Renglon_Ea = Hoja_Ea.Table.Rows.Add();

                foreach (DataColumn Columna in Dt_Reporte.DefaultView.ToTable().Columns)
                {
                    if (Columna is DataColumn)
                    {
                        if (Columna.ColumnName.ToString() != "PESTANIA")
                        {
                                Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Contenido_Indice"));
                        }
                    }
                }
            }
            #endregion
            //Abre el archivo de excel
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Nombre_Archivo);
                Response.WriteFile(Ruta_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
        }// fin try

        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
        }
    }
}