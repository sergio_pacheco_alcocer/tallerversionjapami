<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true"  enableEventValidation="false" CodeFile="Frm_Ope_Con_Gastos_Comprobar.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Gastos_Comprobar" Title="Gastos por comprobar"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
<script src="../../easyui/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.formatCurrency-1.4.0.min.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.formatCurrency.all.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        // manejo de ocultar tr dependiendo del tipo de operacion que realicen
        $(document).ready(function() {
            $("select[id$=Cmb_Operacion]").change(function() {
                $(".Cmb_Operacion option:selected").each(function() {
                    var Tipo = $(this).val();
                    if (Tipo == 3 || Tipo == 2) {
                        $("#Td_Cedular").show();
                        $("#Td_Txt_Reten_Cedular").show();
                        $("#Td_encabezado1").show();
                        $("#Td_encabezado2").hide();
                        $('#Txt_Fectura_ISR').show();
                        $('#Txt_Fectura_Reten_IVA').show();
                        $('#Txt_Fectura_ISH').hide();
                        $('#Txt_Fectura_IEPS').hide();
                        $('#Td_Encab_Iva').show();
                        $('#Td_Encab_Mes').hide();
                        $('#Td_Iva').show();
                        $('#Td_Mes').hide();
                        $('#Td_ISH').hide();
                        $("#Td_ISR").show();
                        $('#Td_IEPS').hide();
                        $("#Td_Reten_Iva").show();
                        if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                            document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                            document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
                            document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                            document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                            document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value))) * 100) / 100; ;
                            document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                        }
                    }
                    if (Tipo == 1) {
                        $("#Td_Cedular").hide();
                        $("#Td_Txt_Reten_Cedular").hide();
                        $("#Td_encabezado1").hide();
                        $("#Td_encabezado2").show();
                        $('#Txt_Fectura_ISR').hide();
                        $('#Txt_Fectura_Reten_IVA').hide();
                        $('#Td_ISH').show();
                        $("#Td_ISR").hide();
                        $('#Td_IEPS').show();
                        $('#Txt_Fectura_ISH').show();
                        $('#Txt_Fectura_IEPS').show();
                        $('#Td_Encab_Iva').show();
                        $('#Td_Encab_Mes').hide();
                        $('#Td_Iva').show();
                        $("#Td_Reten_Iva").hide();
                        $('#Td_Mes').hide();
                        if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                            document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value / 100)) * 100) / 100;
                            document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value))) * 100) / 100;
                            document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                            document.getElementById("<%=Txt_ISR.ClientID%>").value = 0;
                            document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                            document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;

                        }
                    }
                    if (Tipo == 4) {
                        $("#Td_Cedular").hide();
                        $("#Td_Txt_Reten_Cedular").hide();
                        $('#Txt_Fectura_ISR').show();
                        $('#Txt_Fectura_Reten_IVA').hide();
                        $("#Td_Reten_Iva").hide();
                        $("#Td_ISR").show();
                        $("#Td_ISH").hide();
                        $("#Td_IEPS").hide();
                        $('#Txt_Fectura_ISH').hide();
                        $('#Txt_Fectura_IEPS').hide();
                        $('#Td_Encab_Iva').hide();
                        $('#Td_Encab_Mes').show();
                        $('#Td_Iva').hide();
                        $('#Td_Mes').show();
                        if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                            try {
                                var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                                $.ajax({
                                    url: "Controlador_Contabilidad.aspx?" + cadena,
                                    type: 'POST',
                                    async: false,
                                    cache: false,
                                    success: function(data) {
                                        if (data != null) {
                                            document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                        }
                                        else {
                                            document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                        }
                                    }
                                });
                            } catch (Ex) {
                                $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                            }
                            document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                            document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                            document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                            document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                            document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;
                        }
                    }
                });
            }).trigger('change');
        });
        function calculo() {
            if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 2 || document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 3) {
                document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value))) * 100) / 100; ;
                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
            } else {
                if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 1) {
                    document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value / 100)) * 100) / 100;
                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value))) * 100) / 100;
                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                } else {
                    if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 4) {
                        if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0 && document.getElementById("<%=Txt_Meses.ClientID%>").value > 0) {
                            try {
                                var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                                $.ajax({
                                    url: "Controlador_Contabilidad.aspx?" + cadena,
                                    type: 'POST',
                                    async: false,
                                    cache: false,
                                    success: function(data) {
                                        if (data != null) {
                                            document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                        }
                                        else {
                                            document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                        }
                                    }
                                });
                            } catch (Ex) {
                                $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                            }
                            document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                            document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                            document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                        }
                    }
                }
            }
            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value <= 0) {
                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = "0";
                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
            }
        }
        function calculo2() {
            if (document.getElementById("<%=Txt_IVA.ClientID%>").value < 0) {
                document.getElementById("<%=Txt_IVA.ClientID%>").value = "0";
            }
            if (document.getElementById("<%=Txt_ISR.ClientID%>").value < 0) {
                document.getElementById("<%=Txt_ISR.ClientID%>").value = "0";
            }
            if (document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value < 0) {
                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = "0";
            }
            if (document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value < 0) {
                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = "0";
            }
            if (document.getElementById("<%=Txt_ISH.ClientID%>").value < 0) {
                document.getElementById("<%=Txt_ISH.ClientID%>").value = "0";
            }
            if (document.getElementById("<%=Txt_IEPS.ClientID%>").value < 0) {
                document.getElementById("<%=Txt_IEPS.ClientID%>").value = "0";
            }
            if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 2 || document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 3) {
                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = (parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value));
                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
            } else {
                if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 1) {
                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = (parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value));
                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                } else {
                    if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 4) {
                        document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                        document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                    }
                }
            }

        }
        function StartUpload(sender, args) {
            try {
                var filename = args.get_fileName();

                if (filename != "") {
                    // code to get File Extension..   
                    var arr1 = new Array;
                    arr1 = filename.split("\\");
                    var len = arr1.length;
                    var img1 = arr1[len - 1];
                    var filext = img1.substring(img1.lastIndexOf(".") + 1);


                    if (filext == "txt" || filext == "doc" || filext == "pdf" || filext == "docx" || filext == "jpg" || filext == "JPG" || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" || filext == "GIF" || filext == "xlsx") {
                        if (args.get_length() > 2621440) {
                            var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                            alert("El Archivo " + filename + ". Excedio el Tama�o Permitido:\n\nTama�o del Archivo: [" + args.get_length() + " Bytes]\nTama�o Permitido: [2621440 Bytes o 2.5 Mb]" + mensaje);
                            return false;

                        }
                        return true;
                    } else {
                        var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                        alert("Tipo de archivo inv�lido " + filename + "\n\nFormatos Validos [.txt, .doc, .docx, .zip, .rar, .pdf, .jpg, .jpeg, .png, .gif, .xlsx]" + mensaje);
                        return false;
                    }
                }
            } catch (e) {

            }
        }

        function uploadError(sender, args) {
            try {
                var mensaje = "\n\nLa Tabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                alert("Error al Intentar cargar el archivo. [ " + args.get_fileName() + " ]" +
                        "\n\nTama�o V�lido de los Archivos:\n" +
                        " + El Archivo deb� ser mayor a 1Kb.\n + El Archivo deb� ser Menor a 2.5 Mb" + mensaje);
                //refreshGridView();
            } catch (e) {

            }
        }

        //Metodo para mantener los calendarios en una capa mas alat.
        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10000005;
        }

        function Abrir_Modal_Popup() {
            $find('Busqueda_Solicitud_Pago').show();
            return false;
        }
        function Mostrar_Tabla(Renglon, Imagen) {
            object = document.getElementById(Renglon);
            if (object.style.display == "none") {
                object.style.display = "";
                document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
            } else {
                object.style.display = "none";
                document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Parametros_Contabilidad" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"></cc1:ToolkitScriptManager>
       <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
             <ContentTemplate>
               <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                    <ProgressTemplate>
                       <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div  class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            
            <div id="Div_General" runat="server"  style="background-color:#ffffff; width:98%; height:100%;"> <%--Fin del div General--%>
                    <table  border="0" cellspacing="0" class="estilo_fuente" frame="border" width="100%">
                        <tr align="center">
                            <td  colspan="2" class="label_titulo">Comprobacion de Gastos 
                            </td>
                       </tr>
                        <tr> <!--Bloque del mensaje de error-->
                            <td colspan="2" >
                                <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                            </td>      
                        </tr>
                        <tr class="barra_busqueda">
                            <td  style="width:99%;">
                                <asp:ImageButton ID="Btn_Modificar" runat="server"  ToolTip="Modificar" CssClass="Img_Button" TabIndex="2"
                                    ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" />
                                                
                                <asp:ImageButton ID="Btn_Salir" runat="server" 
                                    CssClass="Img_Button" 
                                    ToolTip="Salir"
                                    ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                    onclick="Btn_Salir_Click"/>
                             </td>
                        </tr>
                        </table>
                        <table class="estilo_fuente" width="100%">
                            <tr>
                                <td colspan ="4">
                                &nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Lbl_Monto" runat="server" Text="Monto a Comprobar"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Total_Comprobacion" runat="server" style="text-align:right;"></asp:TextBox>
                                </td>
                                <td colspan ="2" style="width:50%;" align="right">
                                    <asp:Button ID="Btn_Comprobar" runat="server" Text="Comprobar" OnClick="Btn_Comprobar_Click" CssClass="button"  
                                    CausesValidation="false"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan ="4">
                                &nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                            <tr>
                                <td style="width:100%;text-align:center;vertical-align:top;"  colspan ="4">
                                    <center>
                                        <div id="Div_Presentacion" runat="server" style="height:600px;width:99%;border-color:Silver;display:block">
                                            <asp:GridView ID="Grid_Gastos_Comprobacion" runat="server" AllowPaging="False"  ShowHeader="false"
                                                AutoGenerateColumns="False" CssClass="GridView_1"  EmptyDataText="No se encontraron gastos por comprobar" 
                                                DataKeyNames="Tipo" GridLines="None" Width="99%" OnRowDataBound="Grid_Pagos_RowDataBound" >
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Image ID="Img_Btn_Expandir" runat="server"
                                                                ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                                        <ItemStyle HorizontalAlign="Center" Width="2%" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="TIPO" HeaderText="Filtro">
                                                        <HeaderStyle HorizontalAlign="Left" Width="75%" />
                                                        <ItemStyle HorizontalAlign="Left" Width="75%" />
                                                    </asp:BoundField>
			                                        <asp:BoundField DataField="Monto_Tipo" HeaderText="Monto" DataFormatString="{0:c}">
                                                        <HeaderStyle HorizontalAlign="right" Width="23%" />
                                                        <ItemStyle HorizontalAlign="right" Width="23%"  />
                                                    </asp:BoundField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Lbl_Movimientos" runat="server" Text='<%# Bind("IDENTIFICADOR_TIPO") %>' Visible="false"></asp:Label>
                                                            <asp:Literal ID="Ltr_Inicio" runat="server" Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='position:static'&gt;&lt;td colspan='3'; align='right'&gt;" />
                                                            <asp:GridView ID="Grid_Deudores" runat="server" AllowPaging="False" DataKeyNames="DEUDOR_ID" OnRowDataBound="Grid_Proveedores_RowDataBound" 
                                                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="98%" > <%--OnSelectedIndexChanged="Grid_Solicitud_Pagos_SelectedIndexChanged" --%>
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:Image ID="Img_Btn_Expandir_Deudor" runat="server"
                                                                                ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="2%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="DEUDOR_ID" HeaderText="Deudor">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                                                    </asp:BoundField>
                                                                     <asp:ButtonField CommandName="Select" DataTextField="NOMBRE" ControlStyle-Font-Size="X-Small">
                                                                         <HeaderStyle Font-Size="XX-Small" HorizontalAlign="Left" Width="50%" />
                                                                        <ItemStyle Font-Size="XX-Small" Width="50%" ForeColor="Blue" />
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="IMPORTE" HeaderText="Monto" DataFormatString="{0:c}">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField  HeaderText= "Pagar">
                                                                            <HeaderStyle HorizontalAlign="center" Width="5%" Font-Size="X-Small"/>
                                                                            <ItemStyle  HorizontalAlign="Center" Width="5%"  Font-Size="X-Small"/>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox id="Chk_Autorizado_Pago" runat="server" CssClass='<%# Eval("DEUDOR_ID") %>' OnCheckedChanged="Chk_Autorizado_OnCheckedChanged" AutoPostBack="True" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
					                                                <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Lbl_Solicitud" runat="server" 
                                                                                Text='<%# Bind("IDENTIFICADOR") %>' Visible="false"></asp:Label>
                                                                                <asp:Literal ID="Ltr_Inicio2" runat="server" Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='position:static'&gt;&lt;td colspan='8'; align='right';&gt;" />
                                                                            <asp:GridView ID="Grid_Datos_Solicitud" runat="server" AllowPaging="False"  
                                                                                  AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="98%"><%--OnSelectedIndexChanged="Grid_Solicitud_Detalles_SelectedIndexChanged" OnRowDataBound="Grid_Solicitudes_RowDataBound"--%>
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="Btn_Seleccionar_Solicitudes" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                                                 CommandArgument='<%# Eval("NO_DEUDA") %>'  /> <%--OnClick="Btn_Seleccionar_Solicitud_Click"--%>
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="NO_DEUDA" HeaderText="Solicitud">
                                                                                        <HeaderStyle HorizontalAlign="center" Width="10%" Font-Size="X-Small" />
                                                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="center" Width="10%" />
                                                                                    </asp:BoundField>
                                                                                     <asp:BoundField DataField="TIPO_MOVIMIENTO" HeaderText="Tipo Deuda">
                                                                                        <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="15%" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto">
                                                                                        <HeaderStyle HorizontalAlign="Left" Width="30%" Font-Size="X-Small"/>
                                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="30%" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="IMPORTE" HeaderText="Monto" DataFormatString="{0:c}">
                                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                                    </asp:BoundField>
                                                                                    <asp:TemplateField  HeaderText= "Comprobar">
                                                                                        <HeaderStyle  Font-Size="X-Small" HorizontalAlign="center" Width="5%" />
                                                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="center" Width="5%" />
                                                                                        <ItemTemplate >
                                                                                            <asp:CheckBox ID="Chk_Cheque" runat="server" CssClass='<%# Eval("NO_DEUDA") %>' ToolTip='<%# Eval("DEUDOR_ID") %>' runat="server" AutoPostBack="true" OnCheckedChanged="Chk_Cheque_Solicitud_OnCheckedChanged" /><%--AutoPostBack="true" OnCheckedChanged="Chk_Cheque_Solicitud_OnCheckedChanged"--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                                                <FooterStyle CssClass="GridPager" />
                                                                                <HeaderStyle CssClass="GridHeader_Nested" />
                                                                                <PagerStyle CssClass="GridPager" />
                                                                                <RowStyle CssClass="GridItem" />
                                                                                <SelectedRowStyle CssClass="GridSelected" />
                                                                            </asp:GridView>
                                                                            <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TIPO_DEUDOR" HeaderText="bene">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="0.5%" Font-Size="X-Small"/>
                                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="0.5%" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                                <FooterStyle CssClass="GridPager" />
                                                                <HeaderStyle CssClass="GridHeader_Nested" />
                                                                <PagerStyle CssClass="GridPager" />
                                                                <RowStyle CssClass="GridItem" />
                                                                <SelectedRowStyle CssClass="GridSelected" />
                                                            </asp:GridView>
                                                            <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                <FooterStyle CssClass="GridPager" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <PagerStyle CssClass="GridPager" />
                                                <RowStyle CssClass="GridItem" />
                                                <SelectedRowStyle CssClass="GridSelected" />
                                            </asp:GridView>
                                        </div>
                                    </center>
                                </td>
                            </tr>
                            <asp:HiddenField ID="Txt_No_Reserva_Anterior" runat="server" />
                                <asp:HiddenField ID="Txt_Monto_Solicitud_Anterior" runat="server" />
                                <asp:HiddenField ID="Txt_ID_Proveedor" runat="server" />
                                <asp:HiddenField ID="Txt_ID_Empleado" runat="server" />
                                <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Proveedor" runat="server" />
                                <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Empleado" runat="server" />
                                <asp:HiddenField ID="Txt_Monto_Solicitud" runat="server" />
                                <asp:HiddenField ID="Txt_Concepto_Solicitud" runat="server" />
                                <asp:HiddenField ID="Txt_Cuenta_Contable_reserva" runat="server" />
                                <asp:HiddenField ID="Txt_No_Reservah" runat="server" />
                                <asp:HiddenField ID="Txt_porcentaje_ISR_HA" runat="server" />
                        </table>
                        <div id="Div_Detalles" runat="server" style="overflow:auto;width:99%;vertical-align:top;border-style:outset;border-color:Silver;display:block">
                            <table class="estilo_fuente" width="100%">
                                <tr >
                                    <td style="width:15%" >
                                        <asp:Label ID="Lbl_No_Solicitud" runat="server" Text="N�mero de solicitud"></asp:Label>
                                    </td>
                                    <td style="width:30%" >
                                        <asp:TextBox ID="Txt_No_Solicitud" runat="server" Width="98%" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td style="width:15%" >
                                        <asp:Label ID="Lbl_No_Reserva" runat="server" Text="&nbsp;&nbsp;N�mero de reserva" ></asp:Label>
                                    </td>
                                    <td style="width:30%" >
                                        <asp:TextBox ID="Txt_No_Reserva" runat="server" Width="98%" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td style="width:10%" ></td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <asp:Label ID="Lbl_Beneficiario" runat="server" Text="Beneficiario"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Beneficiario" runat="server" Width="98%" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="Lbl_Fecha_Solicitud" runat="server" Text="&nbsp;&nbsp;Fecha de solicitud"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Fecha_Solicitud" runat="server" Width="98%" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td>
                                        <asp:Label ID="Lbl_Total" runat="server" Text="Total"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Total" runat="server" Width="98%" ReadOnly="true" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=5>
                                        <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                                            <tr>  
                                                <td>
                                                    <div style="overflow:auto;width:99%;vertical-align:top; max-height:150px">
                                                <asp:GridView ID="Grid_Partidas" runat="server" Width="97%"
                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None">
                                                    <Columns>                                               
                                                           <asp:BoundField DataField="Dependencia" HeaderText="Dependencia" ItemStyle-Font-Size="XX-Small">
                                                             <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"  />
                                                             <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Fuente_Financiamiento" HeaderText="Fuente" ItemStyle-Font-Size="XX-Small">
                                                             <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                             <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="PROYECTOS_PROGRAMAS" HeaderText="Programas" ItemStyle-Font-Size="XX-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                           <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="AREA_FUNCIONAL" HeaderText="Area funcional" ItemStyle-Font-Size="XX-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                           <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="PARTIDA" HeaderText="Partida" ItemStyle-Font-Size="XX-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                           <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="CODIGO_PROGRAMATICO" HeaderText="Codigo_Programatico" ItemStyle-Font-Size="XX-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                           <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Importe_Inicial" HeaderText="Monto Solicitado" DataFormatString="{0:c}" ItemStyle-Font-Size="XX-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small" />
                                                           <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                           </asp:BoundField>
                                                    </Columns>                                                    
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="tblHead" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView> 
                                           </div>                            
                                                    </td>  
                                                </tr>                                
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5"></td>
                                </tr>
                                </table>
                                <table class="estilo_fuente" width="99%">
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="Upnl_Datos_Solicutd_Pago" runat="server" UpdateMode="Conditional">
                    <ContentTemplate> 
                        <asp:Panel ID="Pnl_Datos_Solicutd_Pago" runat="server" GroupingText="Solicitud de Pago" Width="98%" BackColor="White">
                            <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td >
                                    <asp:HiddenField ID="Txt_iva_parametro" runat="server" />
                                    <asp:HiddenField ID="Txt_Reten_Cedular_parametro" runat="server" />
                                    <asp:HiddenField ID="Txt_Monto_partida" runat="server" />
                                <%--<asp:TextBox ID="Txt_iva_parametro" runat="server" Visible= "False"></asp:TextBox>
                                <asp:TextBox ID="Txt_Reten_Cedular_parametro" runat="server" Visible= "False"></asp:TextBox>--%>
                                </td>
                                <td >
                                <asp:HiddenField ID="Txt_reten_iva_parametro" runat="server" />
                                <asp:HiddenField ID="Txt_ISR_parametro" runat="server" />
                               <%-- <asp:TextBox ID="Txt_reten_iva_parametro" runat="server" Visible= "False"></asp:TextBox>
                                <asp:TextBox ID="Txt_ISR_parametro" runat="server" Visible= "False"></asp:TextBox>--%>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                <asp:TextBox ID="Txt_Ruta" runat="server" Visible= "False"></asp:TextBox>

                                </td>
                                <td >
                                <asp:TextBox ID="Txt_Nombre_Archivo" runat="server" Visible= "False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                 <td style="width:80%" colspan="3">
                                        <asp:Label ID="Lbl_nom_Proveedor" runat="server" Text="*Nombre Proveedor"></asp:Label>
                                    </td>                                 
                                </tr>
                            <tr>        
                                    <td style="width:99%" colspan="4">
                                        <asp:TextBox ID="Txt_Nom_Proveedor" runat="server" Width="97%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Nom_Proveedor" runat="server" TargetControlID="Txt_Nom_Proveedor"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="������������. ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>                            
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                            <tr>
                                                   <td style="width:25%">*RFC</td>                                   
                                                    <td style="width:25%" >*CURP</td> 
                                                   <td style="width:25%">*Numero Documento</td>                                   
                                                   <td style="width:25%">*Fecha Documento</td>    
                                            </tr>
                                            <tr>
                                                    <td style="width:25%">
                                                        <asp:TextBox ID="Txt_RFC" runat="server" MaxLength="13" Width="90%"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="Txt_RFC"
                                                            FilterType="Numbers,Custom, UppercaseLetters, LowercaseLetters" ValidChars="������������. ">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </td>
                                                    <td style="width:25%">                                        
                                                        <asp:TextBox ID="Txt_CURP" runat="server" Width="90%" MaxLength="19"></asp:TextBox> 
                                                        <cc1:FilteredTextBoxExtender ID="Filt_CURP" runat="server" TargetControlID="Txt_CURP"
                                                            FilterType="Numbers,Custom, UppercaseLetters, LowercaseLetters">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </td>
                                                     <td style="width:25%">
                                                      <asp:TextBox ID="Txt_No_Factura_Solicitud_Pago" runat="server" Width="90%" MaxLength="15"></asp:TextBox>   
                                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_No_Factura_Solicitud_Pago" runat="server" TargetControlID="Txt_No_Factura_Solicitud_Pago"
                                                            FilterType="Numbers,Custom, UppercaseLetters, LowercaseLetters" >
                                                        </cc1:FilteredTextBoxExtender>
                                                    </td>
                                                    <td style="width:25%">
                                                          <asp:TextBox ID="Txt_Fecha_Factura_Solicitud_Pago" runat="server" MaxLength="100" Width="85%" Enable="false"/>
                                                        <cc1:CalendarExtender ID="DTP_Fecha_Factura_Solicitud_Pago" runat="server" 
                                                            TargetControlID="Txt_Fecha_Factura_Solicitud_Pago" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Factura_Solicitud_Pago"/>
                                                         <asp:ImageButton ID="Btn_Fecha_Factura_Solicitud_Pago" runat="server" TabIndex="9" 
                                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                                            Height="18px" CausesValidation="false" Enabled="true"/> 
                                                    </td>
                                             </tr>
                                             <tr>
                                                 <td style="width:75%" colspan="3">
                                                        <asp:Label ID="Lbl_Partida" runat="server" Text="Partidas"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Lbl_Subtotal1" runat="server" Text="SubTotal" Width="55px"></asp:Label>
                                                    </td>
                                            </tr>
                                            <tr>        
                                                    <td style="width:75%" colspan="3">
                                                        <asp:DropDownList ID="Cmb_Partida" runat="server" Width="98%">
                                                        </asp:DropDownList>
                                                    </td>   
                                                    <td style=" width:40%;">
                                                        <asp:TextBox ID="Txt_Subtotal_factura" runat="server" Width="85%" OnKeyUp="javascript:calculo();" OnKeyPress="this.value=(this.value.match(/^[0-9]*(\.[0-9]{0,2})?$/))?this.value :'';" ></asp:TextBox>
                                                    </td>   
                                             </tr>
                                        </table>
                                    </td>
                                </tr>
                             <tr>
                                <td style="width:25%">
                                    <asp:Label ID="Lbl_Operacion" runat="server" Text="Operacion"></asp:Label>
                                </td>
                                <td  style="width:47%">
                                        Documento Electr�nico
                                </td>
                                <td id="Td_Encab_Iva" style="width:14%">
                                    &nbsp;&nbsp;&nbsp;
                                     <asp:Label ID="Lbl_IVA_Factura" runat="server" Text="IVA" ></asp:Label>
                                </td>
                                <td id="Td_Encab_Mes" style="width:14%;display:none">
                                     <asp:Label ID="Lbl_Meses" runat="server" Text="Meses"></asp:Label>
                                </td>
                                <td id="Td_Cedular" style="width:14%">
                                     <asp:Label ID="Lbl_Cedular" runat="server" Text="Cedular"></asp:Label>
                                </td>
                             </tr>
                            <tr> 
                                    <td  style="width:25%">
                                         <asp:DropDownList ID="Cmb_Operacion"   CssClass="Cmb_Operacion" runat="server" Width="195px">
                                             <asp:ListItem value="1">OTROS</asp:ListItem>
                                             <asp:ListItem Value="2">ARRENDAMIENTO</asp:ListItem>
                                             <asp:ListItem Value="3">HONORARIOS</asp:ListItem>
                                             <asp:ListItem Value="4">HON. ASIMILABLE</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td  style="width:47%">                                  
                                          <cc1:AsyncFileUpload ID="Asy_Cargar_Archivo" runat="server"  Width="380px" ThrobberID="Throbber" 
                                            ForeColor="Black" Font-Bold="True" CompleteBackColor="LightGreen" 
                                            UploadingBackColor="LightGray" OnClientUploadComplete="StartUpload"  OnClientUploadError="uploadError"
                                            OnUploadedComplete="Asy_Cargar_Archivo_Complete" FailedValidation="False" />  
                                    </td>
                                    <td  id="Td_Iva" style="width:14%">
                                        &nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="Txt_IVA" runat="server" Width="70%" OnKeyUp="javascript:calculo2();" ToolTip="IVA" ></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_IVA" runat="server" 
                                            TargetControlID="Txt_IVA" FilterType="Custom, Numbers" ValidChars="-,."/>                                          
                                    </td>
                                    <td id="Td_Mes" style="width:14%;display:none">
                                            &nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="Txt_Meses" runat="server" Width="70%" OnKeyUp="javascript:Calculo3();" Text="1" ></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Meses" runat="server" 
                                            TargetControlID="Txt_Meses" FilterType="Numbers" />
                                    </td> 
                                    <td style="width:14%" id="Td_Txt_Reten_Cedular">
                                        <asp:TextBox ID="Txt_Reten_Cedular" runat="server" Width="60%" OnKeyUp="javascript:calculo2();" ToolTip="Retencion Cedular"></asp:TextBox>                                        
                                        <cc1:FilteredTextBoxExtender ID="Fil_Txt_Reten_Cedular" runat="server" 
                                            TargetControlID="Txt_Reten_Cedular" FilterType="Custom, Numbers" ValidChars="-,."/>  
                                    </td>
                                </tr>
                                <tr>
                                <td style="width:75%" colspan="2">
                                </td>
                                <td id="Td_ISR" style="width:14%">
                                    &nbsp;&nbsp;&nbsp;
                                     <asp:Label ID="Lbl_ISR" runat="server" Text="ISR" Width="55px" ToolTip="Impuesto Sobre la Renta"></asp:Label>
                                </td>                                
                                <td id="Td_Reten_Iva" style="width:14%">
                                     <asp:Label ID="Lbl_Reten_IVA" runat="server" Text="Ret.IVA" Width="45px" ToolTip="Retenci�n Iva"></asp:Label>
                                </td>
                                <td id="Td_ISH" style="width:14%; display:none;">
                                    &nbsp;&nbsp;&nbsp;
                                     <asp:Label ID="Lbl_ISH" runat="server" Text="ISH" Width="55px"  ToolTip="Impuesto Sobre Hospedaje"></asp:Label>  
                                </td>                                
                                <td id="Td_IEPS" style="width:14%; display:none;">
                                     <asp:Label ID="Lbl_IEPS" runat="server" Text="IEPS" Width="45px"  ToolTip="Impuesto Especial SObre Producci�n y Servicios"></asp:Label>
                                </td>
                                <%--<td  colspan="2" id="Td_encabezado2" style="display:none; width:25%;">
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Lbl_ISH" runat="server" Text="ISH" Width="55px" ></asp:Label>  
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                  
                                    <asp:Label ID="Lbl_IEPS" runat="server" Text="IEPS" Width="45px" ></asp:Label>
                                </td>--%>
                                </tr>
                                <tr>
                                    <td style="width:75%" colspan="2">
                                     &nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td style="width:14%;display:block;" id="Txt_Fectura_ISR">
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:TextBox ID="Txt_ISR" runat="server" Width="70%" OnKeyUp="javascript:calculo2();" ToolTip="Impuesto Sobre la Renta"></asp:TextBox>                                            
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_ISR" runat="server" 
                                            TargetControlID="Txt_ISR" FilterType="Custom, Numbers" ValidChars="-,."/>  
                                     </td>
                                     <td style="width:14%;display:block;" id="Txt_Fectura_Reten_IVA">
                                            <asp:TextBox ID="Txt_Reten_IVA" runat="server" Width="60%" OnKeyUp="javascript:calculo2();" ToolTip="Retenci�n de Iva"></asp:TextBox>                                            
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Reten_IVA" runat="server" 
                                            TargetControlID="Txt_Reten_IVA" FilterType="Custom, Numbers" ValidChars="-,."/>  
                                     </td>     
                                     <td style="width:14%;display:none;" id="Txt_Fectura_ISH">
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:TextBox ID="Txt_ISH" runat="server"  Width="70%" OnKeyUp="javascript:calculo2();" ToolTip="Impuesto Sobre Hospedaje"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_ISH" runat="server" 
                                            TargetControlID="Txt_ISH" FilterType="Custom, Numbers" ValidChars="-,."/>  
                                     </td>
                                     <td style="width:14%;display:none;" id="Txt_Fectura_IEPS">
                                             <asp:TextBox ID="Txt_IEPS" runat="server" Width="60%" OnKeyUp="javascript:calculo2();" ToolTip="Impuesto Especial SObre Producci�n y Servicios"></asp:TextBox> 
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_IEPS" runat="server" 
                                            TargetControlID="Txt_IEPS" FilterType="Custom, Numbers" ValidChars="-,."/>  
                                     </td>     
                                </tr>
                                <tr>
                                    <td style="width:75%" colspan="2">
                                    </td>
                                    <td colspan="2" style="width:25%">
                                        &nbsp;&nbsp;&nbsp;
                                     <asp:Label ID="Lbl_total_Factura" runat="server" Text="Total" Width="55px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                <td style="width:75%" colspan="2">
                                </td>
                                     <td colspan="2" style="width:25%">
                                        &nbsp;&nbsp;&nbsp;
                                         <asp:TextBox ID="Txt_Monto_Solicitud_Pago" runat="server" CssClass="text_cantidades_grid" TabIndex="10" width="65%" ReadOnly="true"/><%--  onblur="$('input[id$=Txt_Monto_Solicitud_Pago]').formatCurrency({colorize:true, region: 'es-MX'});"/>--%>
                                            <%--<cc1:FilteredTextBoxExtender ID="FTE_Txt_Monto_Solicitud_Pago" runat="server" 
                                            TargetControlID="Txt_Monto_Solicitud_Pago" FilterType="Custom, Numbers" ValidChars="-,."/>  --%>
                                        <asp:ImageButton ID="Btn_Agregar_Documento" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_add.png"
                                            CssClass="Img_Button" ToolTip="Agregar"  Height="15px" OnClick="Btn_Agregar_Documento_Click" AutoPostBack="true" /> 
                                    
                                    </td>
                                </tr>
                                </table>                                
                                    </asp:Panel>
                                    <script type="text/javascript" language="javascript">
                                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(fin_peticion);

                                        function fin_peticion() {
                                            $(document).ready(function() {
                                                $("select[id$=Cmb_Operacion]").change(function() {
                                                    $(".Cmb_Operacion option:selected").each(function() {
                                                        var Tipo = $(this).val();
                                                        if (Tipo == 3 || Tipo == 2) {
                                                            $("#Td_Cedular").show();
                                                            $("#Td_Txt_Reten_Cedular").show();
                                                            $("#Td_encabezado1").show();
                                                            $("#Td_encabezado2").hide();
                                                            $('#Txt_Fectura_ISR').show();
                                                            $('#Txt_Fectura_Reten_IVA').show();
                                                            $('#Txt_Fectura_ISH').hide();
                                                            $('#Txt_Fectura_IEPS').hide();
                                                            $("#Td_Reten_Iva").show();
                                                            $('#Td_Encab_Iva').show();
                                                            $('#Td_Encab_Mes').hide();
                                                            $('#Td_Iva').show();
                                                            $('#Td_Mes').hide();
                                                            $('#Td_ISH').hide();
                                                            $("#Td_ISR").show();
                                                            $('#Td_IEPS').hide();
                                                            $("#Td_Reten_Iva").show();
                                                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                                                document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value))) * 100) / 100; ;
                                                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                            }
                                                        }
                                                        if (Tipo == 1) {

                                                            $("#Td_Cedular").hide();
                                                            $("#Td_Txt_Reten_Cedular").hide();
                                                            $("#Td_encabezado1").hide();
                                                            $("#Td_encabezado2").show();
                                                            $('#Txt_Fectura_ISR').hide();
                                                            $("#Td_Reten_Iva").hide();
                                                            $('#Txt_Fectura_Reten_IVA').hide();
                                                            $('#Txt_Fectura_ISH').show();
                                                            $('#Txt_Fectura_IEPS').show();
                                                            $('#Td_Encab_Iva').show();
                                                            $('#Td_Encab_Mes').hide();
                                                            $('#Td_ISH').show();
                                                            $("#Td_ISR").hide();
                                                            $('#Td_IEPS').show();
                                                            $('#Td_Iva').show();
                                                            $('#Td_Mes').hide();
                                                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                                                document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value))) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                                document.getElementById("<%=Txt_ISR.ClientID%>").value = 0;
                                                                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                                                                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;
                                                            }
                                                        }
                                                        if (Tipo == 4) {
                                                            $("#Td_Cedular").hide();
                                                            $("#Td_Txt_Reten_Cedular").hide();
                                                            $('#Txt_Fectura_ISR').show();
                                                            $('#Txt_Fectura_Reten_IVA').hide();
                                                            $("#Td_Reten_Iva").hide();
                                                            $("#Td_ISR").show();
                                                            $("#Td_ISH").hide();
                                                            $("#Td_IEPS").hide();
                                                            $('#Txt_Fectura_ISH').hide();
                                                            $('#Txt_Fectura_IEPS').hide();
                                                            $('#Td_Encab_Iva').hide();
                                                            $('#Td_Encab_Mes').show();
                                                            $('#Td_Iva').hide();
                                                            $('#Td_Mes').show();
                                                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                                                try {
                                                                    var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                                                                    $.ajax({
                                                                        url: "Controlador_Contabilidad.aspx?" + cadena,
                                                                        type: 'POST',
                                                                        async: false,
                                                                        cache: false,
                                                                        success: function(data) {
                                                                            if (data != null) {
                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                                                            }
                                                                            else {
                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                                                            }
                                                                        }
                                                                    });
                                                                } catch (Ex) {
                                                                    $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                                                                }
                                                                document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                                                                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;
                                                            }
                                                        }
                                                    });
                                                }).trigger('change');
                                            });
                                            function calculo2() {
                                                if (document.getElementById("<%=Txt_IVA.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_IVA.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Txt_ISR.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_ISR.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Txt_ISH.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_ISH.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Txt_IEPS.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_IEPS.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 2 || document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 3) {
                                                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = (parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value));
                                                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                } else {
                                                    if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 1) {
                                                        document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = (parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value));
                                                        document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                    } else {
                                                        if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 4) {
                                                            document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                                            document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                            //                                                        if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                                            //                                                            try{
                                                            //                                                                        var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses="+document.getElementById("<%=Txt_Meses.ClientID%>").value +"&";
                                                            //                                                                        $.ajax({
                                                            //                                                                        url: "Controlador_Contabilidad.aspx?"+ cadena,
                                                            //                                                                        type: 'POST',
                                                            //                                                                        async: false,
                                                            //                                                                        cache: false,
                                                            //                                                                        success: function(data) {
                                                            //                                                                            if (data != null) {
                                                            //                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                                            //                                                                            }
                                                            //                                                                            else {
                                                            //                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                                            //                                                                            }
                                                            //                                                                        }
                                                            //                                                                    });
                                                            //                                                                } catch (Ex) {
                                                            //                                                                    $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                                                            //                                                                }
                                                            //                                                                document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                                            //                                                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                                            //                                                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                            //                                                        }
                                                        }
                                                    }
                                                }
                                            }
                                            function calculo() {
                                                if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 2 || document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 3) {
                                                    document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value))) * 100) / 100; ;
                                                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                } else {
                                                    if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 1) {
                                                        document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value / 100)) * 100) / 100;
                                                        document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value))) * 100) / 100;
                                                        document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                    } else {
                                                        if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 4) {
                                                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0 && document.getElementById("<%=Txt_Meses.ClientID%>").value > 0) {
                                                                try {
                                                                    var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                                                                    $.ajax({
                                                                        url: "Controlador_Contabilidad.aspx?" + cadena,
                                                                        type: 'POST',
                                                                        async: false,
                                                                        cache: false,
                                                                        success: function(data) {
                                                                            if (data != null) {
                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                                                            }
                                                                            else {
                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                                                            }
                                                                        }
                                                                    });
                                                                } catch (Ex) {
                                                                    $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                                                                }
                                                                document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                            }
                                                        }
                                                    }
                                                }
                                                if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value <= 0) {
                                                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = "0";
                                                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                }
                                            }
                                            function Calculo3() {
                                                if (document.getElementById("<%=Txt_Meses.ClientID%>").value != "" && document.getElementById("<%=Txt_Meses.ClientID%>").value != 0) {
                                                    if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                                        try {
                                                            var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                                                            $.ajax({
                                                                url: "Controlador_Contabilidad.aspx?" + cadena,
                                                                type: 'POST',
                                                                async: false,
                                                                cache: false,
                                                                success: function(data) {
                                                                    if (data != null) {
                                                                        document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                                                    }
                                                                    else {
                                                                        document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                                                    }
                                                                }
                                                            });
                                                        } catch (Ex) {
                                                            $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                                                        }
                                                        document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                                        document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                                        document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                    }
                                                }
                                            }
                                        }
                                   </script> 
                                   </ContentTemplate>
                              </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table  width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                                <tr align="center">
                                                        <td>
                                                        <div style="overflow:auto;width:98%;height:80px;vertical-align:top;">
                                                                <asp:GridView ID="Grid_Documentos" runat="server" Width="97%"
                                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                                                     OnRowDataBound="Grid_Documentos_RowDataBound">
                                                                    <Columns>
                                                                            <asp:TemplateField HeaderText="Link">
                                                                                <ItemTemplate>
                                                                                    <asp:HyperLink ID="Hyp_Lnk_Ruta" ForeColor="Blue" runat="server" >Archivo</asp:HyperLink></ItemTemplate><HeaderStyle HorizontalAlign ="Left" width ="5%" />
                                                                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                                                                            </asp:TemplateField> 
                                                                            <asp:BoundField DataField="Partida" HeaderText="Partida" ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                                           </asp:BoundField>                                                                                                         
                                                                           <asp:BoundField DataField="Fecha_Documento" HeaderText="Fecha" DataFormatString="{0:dd/MMMM/yyyy}" ItemStyle-Font-Size="X-Small">
                                                                             <HeaderStyle HorizontalAlign="Center" Width="0%" />
                                                                             <ItemStyle HorizontalAlign="Center" Width="0%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Iva" HeaderText="Iva" ItemStyle-Font-Size="X-Small">
                                                                             <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                             <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Operacion" HeaderText="Operaci�n" ItemStyle-Font-Size="X-Small">
                                                                             <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                             <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Monto" HeaderText="Monto" DataFormatString="{0:c}" ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                                           <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="No_Documento" HeaderText="Documento" ItemStyle-Font-Size="X-Small">
                                                                             <HeaderStyle HorizontalAlign="Center" Width="0%" />
                                                                             <ItemStyle HorizontalAlign="Center" Width="0%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Partida_Id" HeaderText="Partida_Id" DataFormatString="{0:c}" ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                           </asp:BoundField>                                                           
                                                                           <asp:BoundField DataField="Ruta" HeaderText="Ruta" ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Archivo" HeaderText="Archivo" ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Proyecto_Programa_Id" HeaderText="Proyecto_Programa_Id" ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Fte_Financiamiento_Id" HeaderText="Fte_Financiamineto_Id"  ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Nombre_Proveedor_Fact" HeaderText="Nombre_Proveedor_Fact"  ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="RFC" HeaderText="RFC"  ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="CURP" HeaderText="CURP"  ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Retencion_ISR" HeaderText="ISR"  ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Retencion_IVA" HeaderText="Retencion_IVA"  ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Retencion_Celula" HeaderText="Celula"  ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                           </asp:BoundField>
                                                                           <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <center>
                                                                                    <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                                                        CausesValidation="false" 
                                                                                        ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                                                        OnClick="Btn_Eliminar_Partida" 
                                                                                        OnClientClick="return confirm('�Est� seguro de eliminar de la tabla la cuneta seleccionada?');" />
                                                                                </center>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                                            <ItemStyle HorizontalAlign="Left" Width="5%" />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="ISH" HeaderText="ISH"  ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                           </asp:BoundField>
                                                                        <asp:BoundField DataField="IEPS" HeaderText="IEPS"  ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                           </asp:BoundField>
                                                                    </Columns>                                                    
                                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                                    <PagerStyle CssClass="GridHeader" />
                                                                    <HeaderStyle CssClass="tblHead" />
                                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                                </asp:GridView> 
                                                           </div>                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    <td>&nbsp;</td></tr></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="estilo_fuente" width="100%">
                                                <tr>
                                                        <td style="width:70%" colspan="2">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width:30%">
                                                            <asp:Label ID="Lbl_Subtotal" runat="server" Text="SubTotal:"></asp:Label>
                                                            &nbsp;
                                                            <asp:TextBox ID="Txt_Subtotal" runat="server" Width="50%" CssClass="text_cantidades_grid" TabIndex="10" ReadOnly="True"
                                                                onblur="$('input[id$=Txt_Subtotal]').formatCurrency({colorize:true, region: 'es-MX'});" />
                                                                <cc1:FilteredTextBoxExtender ID="Filt_Txt_Subtotal" runat="server" 
                                                                TargetControlID="Txt_Total" FilterType="Custom, Numbers" ValidChars="-,."/>
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:70%" colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    <td style="width:30%">
                                                        <asp:Label ID="Label1" runat="server" Text="Total:"></asp:Label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:TextBox ID="Txt_Total_Comprobar" runat="server" Width="50%" CssClass="text_cantidades_grid" TabIndex="10" ReadOnly="True"
                                                            onblur="$('input[id$=Txt_Total_Comprobar]').formatCurrency({colorize:true, region: 'es-MX'});" />
                                                            <cc1:FilteredTextBoxExtender ID="Filt_Txt_Total_Comprobar" runat="server" 
                                                            TargetControlID="Txt_Total_Comprobar" FilterType="Custom, Numbers" ValidChars="-,.$"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                        </div>
                      <div id="Div_Cheque" runat="server" style="overflow:auto;height:200px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;display:block">
                         <asp:Panel ID="Pnl_Datos_Cheque" runat="server" GroupingText="Datos de Cheque" 
                            Width="98%">
                            <table class="estilo_fuente" width="100%">
                                    
                                <tr>
                                    <td style=" width:25%">
                                        &nbsp; &nbsp;
                                        <asp:Label ID="Lbl_banco" runat="server" Text="Banco"></asp:Label>
                                    </td>
                                    <td style=" width:30%">
                                        <asp:DropDownList ID="Cmb_Banco" runat="server" Width="92%" AutoPostBack="true"
                                            OnSelectedIndexChanged="Cmb_Banco_OnSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td style=" width:10%;display:none" id="Td_Cuenta">Cuenta
                                    </td>
                                    <td style=" width:35%;display:none" id="Td_Numero_Cuenta">
                                        <asp:TextBox ID="Txt_Cuenta" runat="server" ReadOnly="true" Width="56%"></asp:TextBox>
                                    </td>
                                </tr>
                                
                                  <tr>
                                        <td>
                                            &nbsp; &nbsp; 
                                            <asp:Label ID="Lbl_Cuenta_Banco" runat="server" Text="Cuenta"></asp:Label>
                                        </td>
                                         <td style=" width:32%">
                                            <asp:DropDownList ID="Cmb_Cuenta_Banco" runat="server" CssClass="Estatus" width="92%" 
                                                AutoPostBack="true" OnSelectedIndexChanged="Cmb_Cuenta_Banco_OnSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    
                                    </tr>
                                    
                                <tr>
                                    <td>
                                        &nbsp; &nbsp;   
                                        <asp:Label ID="Lbl_No_Cheque" runat="server" Text="*No_Cheque" ></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_No_Cheque" runat="server"  Width="90%" MaxLength ="20" ReadOnly="true" ></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender  ID="FTE_Txt_Cheque" runat="server" FilterType ="Numbers" TargetControlID="Txt_No_Cheque" ></cc1:FilteredTextBoxExtender>
                                    </td>
                                  <td style=" width:13%">
                                        &nbsp;
                                        <asp:Label ID="Lbl_Fecha_No_Pago" runat="server" Text="Fecha Pago" ></asp:Label>
                                    </td>
                                    <td style=" width:32%">
                                        <asp:TextBox ID="Txt_Fecha_No_Pago" runat="server" ReadOnly="true" Width="56%"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                        <td>
                                            &nbsp; &nbsp;   
                                            <asp:Label ID="Lbl_comentario" runat="server" Text="*Comentario" ></asp:Label>
                                        </td>
                                        <td colspan="3" style=" width:60%">
                                            <asp:TextBox ID="Txt_Comentario_Pago" runat="server"  Width="82%" MaxLength ="250"  ></asp:TextBox>
                                        </td>
                                    </tr>
                               
                            </table>
                         </asp:Panel> 
                      </div>
                </div>
            </ContentTemplate>
      </asp:UpdatePanel>
</asp:Content>