﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Parametros_Cuentas_Orden_Egresos.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;

public partial class paginas_Contabilidad_Frm_Cat_Con_Parametros_Cuentas_Orden_Egresoso : System.Web.UI.Page
{
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Metodo que se carga cada que ocurre un PostBack de la Página
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Configuracion_Formulario(true);
                Llenar_Grid_Listado(0);
                Llenar_Combos();
            }
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Configuracion_Formulario
        ///DESCRIPCIÓN          : Carga una configuracion de los controles del Formulario
        ///PARAMETROS           : 1. Estatus. Estatus en el que se cargara la configuración
        ///                                   de los controles.
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Configuracion_Formulario(Boolean Estatus)
        {
            Btn_Modificar.Visible = true;
            Btn_Modificar.AlternateText = "Modificar";
            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
            Div_Contenedor_Msj_Error.Visible = false;
            Div_Datos_Generales.Visible = !Estatus;
            Div_Campos.Visible = Estatus;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Listado
        ///DESCRIPCIÓN          : Llena el Listado con una consulta que puede o no
        ///                       tener Filtros.
        ///PARAMETROS           : 1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Listado(int Pagina)
        {
            try
            {
                Grid_Listado.SelectedIndex = (-1);
                Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio Negocio = new Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio();
                Grid_Listado.Columns[1].Visible = true;
                Grid_Listado.DataSource = Negocio.Consultar_Cuentas_Orden();
                Grid_Listado.PageIndex = Pagina;
                Grid_Listado.DataBind();
                Grid_Listado.Columns[1].Visible = false;
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Catalogo
        ///DESCRIPCIÓN          : Limpia los controles del Formulario
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Limpiar_Catalogo()
        {
            Txt_Descripcion.Text = String.Empty;
            Cmb_Cuenta_Contable.SelectedIndex = 0;
            Cmb_Momento_Presupuestal.SelectedIndex = 0;
            Hdf_Cuenta_Orden_ID.Value = String.Empty;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combos
        ///DESCRIPCIÓN          : Llena los controles de seleccion.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Combos()
        {
            Cmb_Momento_Presupuestal.Items.Add(new ListItem("<< SELECCIONE >>", ""));
            Cmb_Momento_Presupuestal.Items.Add(new ListItem("Aprobado", "APROBADO"));
            Cmb_Momento_Presupuestal.Items.Add(new ListItem("Por Ejercer", "POR_EJERCER"));
            Cmb_Momento_Presupuestal.Items.Add(new ListItem("Modificado", "MODIFICADO"));
            Cmb_Momento_Presupuestal.Items.Add(new ListItem("Comprometido", "COMPROMETIDO"));
            Cmb_Momento_Presupuestal.Items.Add(new ListItem("Devengado", "DEVENGADO"));
            Cmb_Momento_Presupuestal.Items.Add(new ListItem("Ejercido", "EJERCIDO"));
            Cmb_Momento_Presupuestal.Items.Add(new ListItem("Pagado", "PAGADO"));

            Cls_Cat_Con_Cuentas_Contables_Negocio Cuentas = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            Cuentas.P_Cuenta = "8";
            Cuentas.P_Afectable = "SI";
            Cmb_Cuenta_Contable.DataSource = Cuentas.Consulta_Cuentas_Contables();
            Cmb_Cuenta_Contable.DataValueField = "CUENTA_CONTABLE_ID";
            Cmb_Cuenta_Contable.DataTextField = "Cuenta_Descripcion";
            Cmb_Cuenta_Contable.DataBind();
            Cmb_Cuenta_Contable.Items.Insert(0, new ListItem("<< SELECIONE >>", ""));
        }

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Componentes
            ///DESCRIPCIÓN          : Hace una validacion de que haya datos en los componentes
            ///                       antes de hacer una operación.
            ///PROPIEDADES          :
            ///CREO                 : Salvador Vázquez Camacho.
            ///FECHA_CREO           : 20/Marzo/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private Boolean Validar_Componentes()
            {
                Lbl_Mensaje_Error.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;

                if (Cmb_Cuenta_Contable.SelectedIndex < 1)
                {
                    Mensaje_Error = Mensaje_Error + "<br/>+ Seleccionar un valor para la Cuenta Contable.";
                    Validacion = false;
                }
                if (Cmb_Momento_Presupuestal.SelectedIndex < 1)
                {
                    Mensaje_Error = Mensaje_Error + "<br/>+ Seleccionar un valor para el Momento Presupuestal.";
                    Validacion = false;
                }
                if (String.IsNullOrEmpty(Txt_Descripcion.Text))
                {
                    Mensaje_Error = Mensaje_Error + "<br/>+ Ingresar un comentario.";
                    Validacion = false;
                }
                else
                {
                    if (Txt_Descripcion.Text.Length > 400)
                    {
                        Mensaje_Error = Mensaje_Error + "<br/>+ Ingresar un Maximo de 400 caracteres.";
                        Validacion = false;
                    }
                }
                if (!Validacion)
                {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

        #endregion

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Evento del Boton Nuevo
        ///PARAMETROS:   
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = false;
            if (Btn_Nuevo.AlternateText == "Nuevo")
            {
                Configuracion_Formulario(false);
                Cmb_Cuenta_Contable.Enabled = true;
                Cmb_Momento_Presupuestal.Enabled = true;
                Txt_Descripcion.Enabled = true;
                Btn_Modificar.Visible = false;
                Btn_Nuevo.AlternateText = "Guardar";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Salir.AlternateText = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
            }
            else
            {
                if (Validar_Componentes())
                {
                    Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio Negocio = new Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio();
                    Negocio.P_Cuenta_Contable_ID = Cmb_Cuenta_Contable.SelectedValue;
                    Negocio.P_Descripcion = Txt_Descripcion.Text;
                    Negocio.P_Momento_Presupuestal = Cmb_Momento_Presupuestal.SelectedValue;
                    Negocio.Crear_Cuenta_Orden();
                    Configuracion_Formulario(true);
                    Limpiar_Catalogo();
                    Llenar_Grid_Listado(Grid_Listado.PageIndex);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Alta Exitosa');", true);
                    Cmb_Cuenta_Contable.Enabled = false;
                    Cmb_Momento_Presupuestal.Enabled = false;
                    Txt_Descripcion.Enabled = false;
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
        ///DESCRIPCIÓN          : Deja los componentes listos para hacer la modificacion.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Btn_Modificar.AlternateText.Equals("Modificar"))
                {
                    if (Grid_Listado.Rows.Count > 0 && Grid_Listado.SelectedIndex > (-1))
                    {
                        Configuracion_Formulario(false);
                        Cmb_Cuenta_Contable.Enabled = true;
                        Cmb_Momento_Presupuestal.Enabled = true;
                        Txt_Descripcion.Enabled = true;
                        Btn_Modificar.AlternateText = "Actualizar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        Btn_Salir.AlternateText = "Cancelar";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "Debe seleccionar el Registro que se desea Modificar.";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
                else
                {
                    if (Validar_Componentes())
                    {
                        Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio Negocio = new Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio();
                        Negocio.P_ID = Hdf_Cuenta_Orden_ID.Value;
                        Negocio.P_Cuenta_Contable_ID = Cmb_Cuenta_Contable.SelectedValue;
                        Negocio.P_Descripcion = Txt_Descripcion.Text;
                        Negocio.P_Momento_Presupuestal = Cmb_Momento_Presupuestal.SelectedValue;
                        Negocio.Modificar_Cuenta_Orden();
                        Configuracion_Formulario(true);
                        Limpiar_Catalogo();
                        Llenar_Grid_Listado(Grid_Listado.PageIndex);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Actualización Exitosa');", true);
                        Cmb_Cuenta_Contable.Enabled = false;
                        Cmb_Momento_Presupuestal.Enabled = false;
                        Txt_Descripcion.Enabled = false;
                        Btn_Modificar.AlternateText = "Modificar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Cancela la operación que esta en proceso 
        ///                       (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Div_Datos_Generales.Visible)
            {
                if (Btn_Modificar.AlternateText == "Actualizar")
                {
                    Cmb_Cuenta_Contable.Enabled = false;
                    Cmb_Momento_Presupuestal.Enabled = false;
                    Txt_Descripcion.Enabled = false;
                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                }
                else
                    Response.Redirect("../Contabilidad/Frm_Cat_Con_Parametros_Cuentas_Orden_Egresoso.aspx");
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Listado_PageIndexChanging
        ///DESCRIPCIÓN          : Maneja la pagiación del Grid.
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Listado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Listado.SelectedIndex = (-1);
                Llenar_Grid_Listado(e.NewPageIndex);
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Listado_SelectedIndexChanged
        ///DESCRIPCIÓN          : Metodo para cargar los datos del elemento seleccionado
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Listado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Listado.SelectedIndex > (-1))
                {
                    Limpiar_Catalogo();
                    Configuracion_Formulario(false);
                    Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio Negocio = new Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio();
                    Hdf_Cuenta_Orden_ID.Value = HttpUtility.HtmlDecode(Grid_Listado.SelectedRow.Cells[1].Text.Trim());
                    Negocio.P_ID = Hdf_Cuenta_Orden_ID.Value;
                    DataTable Dt_Cuenta_Orden = Negocio.Consultar_Cuentas_Orden();

                    if (Dt_Cuenta_Orden.Rows.Count > 0)
                    {
                        Cmb_Cuenta_Contable.SelectedIndex = Cmb_Cuenta_Contable.Items.IndexOf(
                            Cmb_Cuenta_Contable.Items.FindByValue(Dt_Cuenta_Orden.Rows[0][Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Cuenta_Contable_ID].ToString().Trim()));
                        Cmb_Momento_Presupuestal.SelectedIndex = Cmb_Momento_Presupuestal.Items.IndexOf(
                            Cmb_Momento_Presupuestal.Items.FindByValue(Dt_Cuenta_Orden.Rows[0][Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Momento_Presupuestal].ToString().Trim()));
                        Txt_Descripcion.Text = Dt_Cuenta_Orden.Rows[0][Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Descripcion].ToString();
                    }
                    Btn_Nuevo.Visible = false;
                    Cmb_Cuenta_Contable.Enabled = false;
                    Cmb_Momento_Presupuestal.Enabled = false;
                    Txt_Descripcion.Enabled = false;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion
}
