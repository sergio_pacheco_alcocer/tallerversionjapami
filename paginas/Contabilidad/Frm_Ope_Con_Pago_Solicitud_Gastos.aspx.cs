﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
//using System.Windows.Forms;
using System.Web;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sindicatos.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using AjaxControlToolkit;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Area_Funcional.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Compromisos_Contabilidad.Negocios;
using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Cheque.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago_Contabilidad.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Numalet;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Contabilidad_Transferencia.Negocio;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Catalogo_Con_Proveedores.Negocio;
//using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;
using JAPAMI.Autoriza_Solicitud_Deudores.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Pago_Solicitud_Gastos : System.Web.UI.Page
{
    private static String P_Dt_Solicitud = "P_Dt_Solicitud";
    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        try
        {
            if (!IsPostBack)
            {
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);
                Txt_Fecha.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Inicio.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_No_Pago.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                Cls_Ope_Con_Cheques_Negocio Tipos_Negocio = new Cls_Ope_Con_Cheques_Negocio();
                DataTable Dt_Tipos = Tipos_Negocio.Consulta_Tipos_Solicitudes();
                //RBL_Orden.SelectedIndex = 0;
                Llenar_Combos_Generales();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// 
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            //Botones.Add(Btn_Eliminar);
            //Botones.Add(Btn_Mostrar_Popup_Busqueda);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Solicitudes_Pendientes
    // DESCRIPCIÓN: Llena el grid principal de las solicitudes de pago que estan autorizadas
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 18/noviembre/2011 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Grid_Solicitudes_Pendientes()
    {
        DataTable Temporal = null;
        String Tipo;
        String Tipo_Deudor = "";
        String Forma_Pago;
        DataTable Dt_Modificada = new DataTable();
        Boolean Insertar;
        Double Monto;
        DataTable Dt_Datos_Detalles = new DataTable();
        Session["Dt_Datos_Detalles"] = Dt_Datos_Detalles;
        DataTable Dt_Datos = new DataTable();
        Session["Dt_Datos"] = Dt_Datos;
        Int32 Contador = 0;
        Session["Contador"] = Contador;
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        //Cls_Ope_Con_Cheques_Negocio Solicitudes = new Cls_Ope_Con_Cheques_Negocio();
        Rs_Solicitudes.P_Estatus = "PORPAGAR";
        Session[P_Dt_Solicitud] = Rs_Solicitudes.Consulta_Solicitudes_PorPagar();
         if (Session[P_Dt_Solicitud] != null && ((DataTable)Session[P_Dt_Solicitud]).Rows.Count > 0)
        {
            Temporal = Session[P_Dt_Solicitud] as DataTable;
            foreach (DataRow Fila in Temporal.Rows)
            {
                if (Dt_Modificada.Rows.Count <=0 && Dt_Modificada.Columns.Count<=0)
                {
                    Dt_Modificada.Columns.Add("TIPO", typeof(System.String));
                    Dt_Modificada.Columns.Add("MONTO_TIPO", typeof(System.Double));
                    Dt_Modificada.Columns.Add("FORMA_PAGO", typeof(System.String));
                    Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                    Dt_Modificada.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                }
                DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                Tipo = Fila["BANCO"].ToString();
                Tipo_Deudor = Fila["TIPO_DEUDOR"].ToString();
                Forma_Pago = Cmb_Forma_Pago.SelectedItem.Text.ToString();
                Insertar = true;
                Monto = 0;
                //insertar los que no se repiten
                foreach (DataRow Fila2 in Dt_Modificada.Rows)
                {
                    //se verifica si selecciono un tipo de pago para solo mostrar las solicitudes de ese pago
                        if (Tipo.Equals(Fila2["TIPO"].ToString()))
                        {
                            Insertar = false;
                        }
                }
                //se calcula el monto por tipo
                foreach (DataRow Renglon in Temporal.Rows)
                {

                    if (Tipo.Equals(Renglon["BANCO"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()))
                    {
                        Monto = Monto + Convert.ToDouble(Renglon["IMPORTE"].ToString());
                    }
                }
                if (Insertar && Monto > 0)
                {
                    row["TIPO"] = Tipo;
                    row["MONTO_TIPO"] = Monto;
                    row["FORMA_PAGO"] = Forma_Pago;
                    row["IDENTIFICADOR_TIPO"] = Forma_Pago+Tipo+Monto;
                    row["TIPO_DEUDOR"] = Tipo_Deudor;
                    Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Modificada.AcceptChanges();
                }
            }
            Session["Dt_Datos_Detalles"] = Temporal;
            Session["Dt_Datos"] = Dt_Modificada;
            Grid_Pagos.DataSource = Dt_Modificada;
            Grid_Pagos.DataBind();
            //Grid_Pagos.Columns[3].Visible = false;
        }
        else
        {
            Session[P_Dt_Solicitud] = null;
            Grid_Pagos.DataSource = null;
            Grid_Pagos.DataBind();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 18/Noviembre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpia_Controles();
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Llenar_Grid_Solicitudes_Pendientes();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 18/Noviembre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Txt_No_Solicitud_Busqueda.Text = "";
            // text de solicitud de pago
            Txt_No_Solicitud.Text = "";
            Txt_Fecha.Text = "";
            Txt_Motivo_Cancelacion.Text = "";
            Txt_Tipo_Solicitud_Pago.Text = "";
            Txt_Estatus_Solicitud.Text = "";
            Txt_Concepto.Text = "";
            Txt_Monto.Text = "";
            // Text de Datos de pago
            Txt_No_Pago.Text = "";
            Txt_Fecha_No_Pago.Text = "";
            Cmb_Tipo_Pago.SelectedIndex = 0;
            Cmb_Estatus.SelectedIndex = 0;
            Cmb_Banco.SelectedIndex = 0;
            if (Cmb_Cuenta_Banco.SelectedIndex > 0)
            {
                Cmb_Cuenta_Banco.SelectedIndex = 0;
            }
            Txt_No_Cheque.Text = "";
            Txt_Referencia_Pago.Text = "";
            Txt_Comentario_Pago.Text = "";
            Txt_Beneficiario_Pago.Text = "";
            Txt_Cuenta_Contable_ID_Banco.Value = "";
            Txt_Cuenta_Contable_Proveedor.Value = "";
            Txt_TIpo_Benenficiario.Value = "";
            Txt_Beneficiario_ID.Value = "";
            Txt_Tipo_Solicitud.Value = "";
            //Txt_Orden.Text = "";
            Txt_No_Solicitud_Autorizar.Value = "";
            Txt_Rechazo.Value = "";
            Txt_Comentario.Text = "";
            Session["Dt_Datos_Detalles_Proveedor"] = null;
            Session["Dt_Datos_Proveedor"] = null;
            Session["Dt_Datos_Detalles"] = null;
            Session["Dt_Datos"] = null;
            Session["Contador"] = null;
            Session["Contador_Proveedor"] = null;
            Session["Dt_Forma_Pago"] = null;
            Hd_TIpo_Solicitud_Pago.Value = "";
        }
        catch (Exception ex)
        {
            throw new Exception("Limpiar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                          si es una alta, modificacion
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 18/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    //Btn_Eliminar.Visible = true;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Modificar.CausesValidation = false;
                    //Btn_Imprimir_Excel.Visible = true;
                    //Btn_Imprimir_Excel.ToolTip = "Generar Layout traspasos";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Div_Solicitudes_Pendientes.Style.Add("display", "block");
                    Div_Datos_Solicitud.Style.Add("display", "none");
                    Configuracion_Acceso("Frm_Ope_Con_Pago_Solicitud_Gastos.aspx");
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = false;
                    Cmb_Estatus.Enabled = false;
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    //Btn_Eliminar.Visible = false;  
                    //Btn_Imprimir_Excel.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Div_Solicitudes_Pendientes.Style.Add("display", "none");
                    Div_Datos_Solicitud.Style.Add("display", "block");
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Cmb_Estatus.Enabled = false;
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = false;
                    //Btn_Imprimir_Excel.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    Cmb_Estatus.Enabled = true;
                    break;
            }

            Txt_No_Solicitud.Enabled = false;
            Txt_Fecha.Enabled = false;
            Txt_Tipo_Solicitud_Pago.Enabled = false;
            //Txt_MesAnio.Enabled = false;
           // Txt_No_Reserva_Solicitud.Enabled = false;
            Txt_Estatus_Solicitud.Enabled = false;
            Txt_Concepto.Enabled = false;
            Txt_Monto.Enabled = false;
           // Txt_No_Poliza.Enabled = false;
            Txt_No_Pago.Enabled = Habilitado;
            Txt_Fecha_No_Pago.Enabled = Habilitado;
            Cmb_Tipo_Pago.Enabled = Habilitado;
            Cmb_Banco.Enabled = Habilitado;
            Txt_No_Cheque.Enabled = Habilitado;
            Txt_Referencia_Pago.Enabled = Habilitado;
            Txt_Comentario_Pago.Enabled = Habilitado;
            Txt_Beneficiario_Pago.Enabled = Habilitado;
            Txt_Fecha_Inicio.Enabled = false;
            Txt_Fecha_Final.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String Numero_Solicitud)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        try
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            Ds_Reporte = new DataSet();
            Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
            Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            Lbl_Mensaje_Error.Visible = true;
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Requisicion_Click
    ///DESCRIPCIÓN: Metodo para consultar la reserva
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Solicitud_Click(object sender, ImageClickEventArgs e)
    {
        String No_Solicitud = ((ImageButton)sender).CommandArgument;
        Evento_Grid_Solicitudes_Seleccionar(No_Solicitud);
        if (Txt_Estatus_Solicitud.Text != "PAGADO" && Txt_Estatus_Solicitud.Text != "CANCELADO")
        {
            Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Solicitud_Click
    ///DESCRIPCIÓN: manda llamar el metodo de la impresion de la caratula 
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO  : 21-mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Solicitud_Click(object sender, EventArgs e)
    {
        String No_Solicitud = ((LinkButton)sender).Text;
        Imprimir(No_Solicitud);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Btn_Buscar_Solicitud_Click
    ///DESCRIPCIÓN: Metodo para consultar la reserva
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Solicitud_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Temporal = null;
        String Tipo;
        String Tipo_Deudor = "";
        DataTable Dt_Modificada = new DataTable();
        Boolean Insertar;
        Double Monto;
        String Forma_Pago;
        DataTable Dt_Datos_Detalles = new DataTable();
        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
        DataTable Dt_Datos = new DataTable();
        Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
        int Contador = 0;
        Session["Contador"] = Contador;
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Consulta_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        //Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Consulta_Solicitudes.P_Fecha_Inicio = String.Format("{0:dd-MM-yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text));
        Consulta_Solicitudes.P_Fecha_Final = String.Format("{0:dd-MM-yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text));
        if (Txt_No_Solicitud_Busqueda.Text != "")
        {
            Consulta_Solicitudes.P_No_Deuda = String.Format("{0:0000000000}", Convert.ToInt16(Txt_No_Solicitud_Busqueda.Text.ToString()));
        }
        if (Cmb_Estatus_filtro.SelectedIndex > 0)
        {
            Consulta_Solicitudes.P_Estatus = Cmb_Estatus_filtro.SelectedItem.Text.ToString();
        }
        Session[P_Dt_Solicitud] = Consulta_Solicitudes.Consulta_Solicitudes_Pagadas_o_Porpagar();
        if (((DataTable)Session[P_Dt_Solicitud]).Rows.Count == 1)
        {
            Temporal = Session[P_Dt_Solicitud] as DataTable;
            String No_Solicitud = Temporal.Rows[0]["NO_DEUDA"].ToString().Trim();
            Evento_Grid_Solicitudes_Seleccionar(No_Solicitud);
            if (Txt_Estatus_Solicitud.Text != "PAGADO" && Txt_Estatus_Solicitud.Text != "CANCELADO")
            {
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                Llenar_Combos_Generales();
            }
        }
        else
        {
            if (Session[P_Dt_Solicitud] != null && ((DataTable)Session[P_Dt_Solicitud]).Rows.Count > 0)
            {
                Temporal=Session[P_Dt_Solicitud] as DataTable;
                foreach (DataRow Fila in Temporal.Rows)
                {
                    if (Dt_Modificada.Rows.Count <= 0 && Dt_Modificada.Columns.Count <= 0)
                    {
                        Dt_Modificada.Columns.Add("TIPO", typeof(System.String));
                        Dt_Modificada.Columns.Add("MONTO_TIPO", typeof(System.Double));
                        Dt_Modificada.Columns.Add("FORMA_PAGO", typeof(System.String));
                        Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                        Dt_Modificada.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                    }
                    DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                    Tipo = Fila["BANCO"].ToString();
                    Tipo_Deudor = Fila["TIPO_DEUDOR"].ToString();
                    Cmb_Forma_Pago.SelectedIndex = Cmb_Forma_Pago_Filtro.SelectedIndex;
                    Forma_Pago = Cmb_Forma_Pago.SelectedItem.Text.ToString();
                    Insertar = true;
                    Monto = 0;
                    //insertar los que no se repiten
                    foreach (DataRow Fila2 in Dt_Modificada.Rows)
                    {
                        //se verifica si selecciono un tipo de pago para solo mostrar las solicitudes de ese pago
                        if (Tipo.Equals(Fila2["TIPO"].ToString()))
                        {
                            Insertar = false;
                        }
                    }
                    //se calcula el monto por tipo
                    foreach (DataRow Renglon in Temporal.Rows)
                    {

                        if (Tipo.Equals(Renglon["BANCO"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()))
                        {
                            Monto = Monto + Convert.ToDouble(Renglon["IMPORTE"].ToString());
                        }
                    }
                    if (Insertar && Monto > 0)
                    {
                        row["TIPO"] = Tipo;
                        row["MONTO_TIPO"] = Monto;
                        row["FORMA_PAGO"] = Forma_Pago;
                        row["IDENTIFICADOR_TIPO"] = Forma_Pago + Tipo + Monto;
                        row["TIPO_DEUDOR"] = Tipo_Deudor;
                        Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Modificada.AcceptChanges();
                    }
                }
                Session["Dt_Datos_Detalles"] = Temporal;
                Session["Dt_Datos"] = Dt_Modificada;
                Grid_Pagos.DataSource = Dt_Modificada;
                Grid_Pagos.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "No. Pago", "alert('El numero de Pago que busca No se pago con cheque ');", true);
                Session[P_Dt_Solicitud] = null;
                Grid_Pagos.DataSource = null;
                Grid_Pagos.DataBind();
            }
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Pagos_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView Gv_Detalles = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Dt_Datos = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Modificada = new DataTable();
        DataTable Dt_Datos_Detalles_Proveedor = new DataTable();
        Dt_Datos_Detalles_Proveedor = ((DataTable)(Session["Dt_Datos_Detalles_Proveedor"]));
        DataTable Dt_Datos_Proveedor = new DataTable();
        Dt_Datos_Proveedor = ((DataTable)(Session["Dt_Datos_Proveedor"]));
        Int32 Contador_Proveedor = 0;
        Session["Contador_Proveedor"] = Contador_Proveedor;
        String DEUDOR_ID = "";
        String TIPO_DEUDOR = "";
        String Tipo = "";
        String Forma_Pago = "";
        Boolean Agregar;
        Double Total;
        Int32 Contador;
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Image Img = new Image();
        Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
        Literal Lit = new Literal();
        Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
        Label Lbl_facturas = new Label();
        Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos");
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador = (Int32)(Session["Contador"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                foreach(DataRow Filas in Dt_Datos_Detalles.Rows)
                {
                    DEUDOR_ID = "";
                    TIPO_DEUDOR = "";
                    DEUDOR_ID = Filas["DEUDOR_ID"].ToString();
                    TIPO_DEUDOR = Filas["TIPO_DEUDOR"].ToString();
                    Tipo = Convert.ToString(Dt_Datos.Rows[Contador]["Tipo"].ToString());
                    Forma_Pago = Convert.ToString(Dt_Datos.Rows[Contador]["Forma_Pago"].ToString());
                    if (!String.IsNullOrEmpty(DEUDOR_ID))
                    {
                        Dt_Datos_Detalles.DefaultView.RowFilter = "Banco='" + Tipo + "' and DEUDOR_ID='" + DEUDOR_ID + "' and Forma_Pago='" + Forma_Pago + "' and TIPO_DEUDOR='" + TIPO_DEUDOR + "'";
                    }
                    if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Deudores");
                        foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                        {
                            if (Dt_Modificada.Rows.Count <= 0 && Dt_Modificada.Columns.Count <= 0)
                            {
                                Dt_Modificada.Columns.Add("DEUDOR_ID", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                                Dt_Modificada.Columns.Add("NOMBRE", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO_MOVIMIENTO", typeof(System.String));
                                Dt_Modificada.Columns.Add("IMPORTE", typeof(System.Double));
                                Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO", typeof(System.String));
                                Dt_Modificada.Columns.Add("FORMA_PAGO", typeof(System.String));
                                Dt_Modificada.Columns.Add("IDENTIFICADOR", typeof(System.String));
                            }
                            DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            Agregar = true;
                            Total = 0;
                            //insertar los que no se repiten
                            foreach (DataRow Fila2 in Dt_Modificada.Rows)
                            {
                                if (!String.IsNullOrEmpty(DEUDOR_ID))
                                {
                                    if (DEUDOR_ID.Equals(Fila2["DEUDOR_ID"].ToString()) && Forma_Pago.Equals(Fila2["Forma_Pago"].ToString()) && TIPO_DEUDOR.Equals(Fila2["TIPO_DEUDOR"].ToString()))
                                    {
                                        Agregar = false;
                                    }
                                }
                            }
                            //se calcula el monto por tipo
                            foreach (DataRow Renglon in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                            {
                                if (!String.IsNullOrEmpty(DEUDOR_ID))
                                {
                                    if (DEUDOR_ID.Equals(Renglon["DEUDOR_ID"].ToString()) && Forma_Pago.Equals(Renglon["Forma_Pago"].ToString()) && TIPO_DEUDOR.Equals(Renglon["TIPO_DEUDOR"].ToString()))
                                    {
                                        Total = Total + Convert.ToDouble(Renglon["IMPORTE"].ToString());
                                    }
                                }
                            }
                            if (Agregar && Total>0)
                            {
                                if (!String.IsNullOrEmpty(DEUDOR_ID))
                                {
                                    row["DEUDOR_ID"] = DEUDOR_ID;
                                    row["NOMBRE"] = Fila["NOMBRE"].ToString();
                                    row["TIPO_MOVIMIENTO"] = Fila["TIPO_MOVIMIENTO"].ToString();
                                    row["TIPO_DEUDOR"] = TIPO_DEUDOR;
                                }
                                row["IMPORTE"] = Total;
                                row["TIPO"] = Tipo;
                                row["FORMA_PAGO"] = Forma_Pago;
                                row["ESTATUS"] = Fila["Estatus"].ToString();
                                if (TIPO_DEUDOR=="PROVEEDOR")
                                {
                                    row["IDENTIFICADOR"] = DEUDOR_ID + "P" + Tipo + Forma_Pago;
                                }
                                else
                                {
                                    row["IDENTIFICADOR"] = DEUDOR_ID + "E" + Tipo + Forma_Pago;
                                }
                                Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Modificada.AcceptChanges();
                            }
                        }
                    }
                }
                Session["Dt_Datos_Detalles_Proveedor"] = Dt_Datos_Detalles;
                Session["Dt_Datos_Proveedor"] = Dt_Modificada;
                Gv_Detalles.Columns[6].Visible = true;
                Gv_Detalles.DataSource = Dt_Modificada;
                Gv_Detalles.DataBind();
                Gv_Detalles.Columns[6].Visible = false;
                Session["Contador"] = Contador + 1;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Proveedores_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Proveedores_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView Gv_Detalles = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Dt_Datos = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Modificada = new DataTable();
        String Deudor_ID = "";
        String Tipo_Beneficiario = "";
        String Tipo = "";
        String NO_DEUDA = "";
        Int32 Contador_Proveedor;
        String Forma_Pago = "";
        DataTable Dt_Forma_Pago = new DataTable();
        Dt_Forma_Pago = ((DataTable)(Session["Dt_Forma_Pago"]));
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Image Img = new Image();
        Img = (Image)e.Row.FindControl("Img_Btn_Expandir_Deudor");
        Literal Lit = new Literal();
        Lit = (Literal)e.Row.FindControl("Ltr_Inicio2");
        Label Lbl_facturas = new Label();
        Lbl_facturas = (Label)e.Row.FindControl("Lbl_Solicitud");
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador_Proveedor = (Int32)(Session["Contador_Proveedor"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos = ((DataTable)(Session["Dt_Datos_Proveedor"]));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles_Proveedor"]));
                foreach (DataRow Filas in Dt_Datos_Detalles.Rows)
                {
                    Deudor_ID = "";
                    NO_DEUDA = Filas["NO_DEUDA"].ToString();
                    Deudor_ID = Dt_Datos.Rows[Contador_Proveedor]["DEUDOR_ID"].ToString();
                    Tipo = Convert.ToString(Dt_Datos.Rows[Contador_Proveedor]["Tipo"].ToString());
                    Tipo_Beneficiario = Dt_Datos.Rows[Contador_Proveedor]["TIPO_DEUDOR"].ToString();
                    Forma_Pago = Convert.ToString(Dt_Datos.Rows[Contador_Proveedor]["Forma_Pago"].ToString());
                    Dt_Datos_Detalles.DefaultView.RowFilter = "Banco='" + Tipo + "' and deudor_id='" + Deudor_ID + "' and No_Deuda='" + NO_DEUDA + "' and Forma_Pago='" + Forma_Pago + "'";
                    if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Datos_Solicitud");
                        foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                        {
                            if (Dt_Modificada.Rows.Count <= 0)
                            {
                                Dt_Modificada.Columns.Add("NO_DEUDA", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO_MOVIMIENTO", typeof(System.String));
                                Dt_Modificada.Columns.Add("BANCO", typeof(System.String));
                                Dt_Modificada.Columns.Add("CONCEPTO", typeof(System.String));
                                Dt_Modificada.Columns.Add("IMPORTE", typeof(System.Double));
                                Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                                Dt_Modificada.Columns.Add("FORMA_PAGO", typeof(System.String));
                            }
                            DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            row["NO_DEUDA"] = Fila["NO_DEUDA"].ToString();
                            row["TIPO_MOVIMIENTO"] = Fila["TIPO_MOVIMIENTO"].ToString();
                            row["IMPORTE"] = Fila["IMPORTE"].ToString();
                            row["BANCO"] = Fila["BANCO"].ToString();
                            row["CONCEPTO"] = Fila["CONCEPTO"].ToString();
                            row["ESTATUS"] = Fila["ESTATUS"].ToString();
                            row["FORMA_PAGO"] = Forma_Pago;
                            Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificada.AcceptChanges();
                        }
                        Session["Dt_Forma_Pago"] = Dt_Modificada;
                        //Gv_Detalles.Columns[2].Visible = true;
                        //Gv_Detalles.Columns[3].Visible = true;
                        Gv_Detalles.DataSource = Dt_Modificada;
                        Gv_Detalles.DataBind();
                        //Gv_Detalles.Columns[2].Visible = false;
                        //Gv_Detalles.Columns[3].Visible = false;
                    }
                }
                Session["Contador_Proveedor"] = Contador_Proveedor + 1;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Solicitudes_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Solicitudes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        TextBox Txt_Fecha;
        DataTable Dt_Datos = new DataTable();
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Dt_Datos = ((DataTable)(Session["Dt_Forma_Pago"]));
                if (Dt_Datos.Rows[0]["Forma_Pago"].ToString() == "Transferencia")
                {
                    e.Row.Cells[0].Enabled = false;
                    e.Row.Cells[6].Enabled = true;
                    e.Row.Cells[7].Enabled = true;
                    Txt_Fecha = (TextBox)e.Row.Cells[6].FindControl("Txt_Fecha_Transferencia");
                    Txt_Fecha.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                }
                else
                {
                    if (Dt_Datos.Rows[0]["Forma_Pago"].ToString() == "Cheque")
                    {
                        e.Row.Cells[0].Enabled = true;
                        e.Row.Cells[6].Enabled = false;
                        e.Row.Cells[7].Enabled = false;
                    }
                }
                
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Evento_Grid_Solicitudes_Seleccionar
    ///DESCRIPCIÓN:
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Evento_Grid_Solicitudes_Seleccionar(String Dato)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        //Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de coneción hacia la capa de datos
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();        
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Cat_Nom_Bancos_Negocio Rs_Banco_Pago = new Cls_Cat_Nom_Bancos_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Banco_Pago = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
         DataTable Dt_Consulta = new DataTable();
         DataTable Dt_Consulta_Cuentas = new DataTable();
        DataTable Dt_Solicitud_Pagos = new DataTable(); //Variable a contener los datos de la consulta de la solicitud de pago
        DataTable Dt_Cuenta = new DataTable(); //Variable a contener los datos de la consulta
        DataTable Dt_Solicitud_Detalles = new DataTable();
        Habilitar_Controles("Inicial");
        Llenar_Combos_Generales();
        //Btn_Imprimir_Excel.Visible = false;
        Btn_Modificar.ToolTip = "Cancelar Pago";
        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
        Btn_Modificar.Visible = false;
        Div_Datos_Solicitud.Style.Add("display", "block");
        Div_Solicitudes_Pendientes.Style.Add("display", "none");
        Int32 Folio_Actual = 0;
        Int32 Folio_Final = 0;
        Boolean Estado_Folio = false;
        Txt_Fecha_No_Pago.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
        Cls_Ope_Con_Cheques_Negocio Cheques = new Cls_Ope_Con_Cheques_Negocio();
        Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
        Cls_Ope_Con_Reservas_Negocio Reserva_Negocio = new Cls_Ope_Con_Reservas_Negocio();
        String No_Solicitud = Dato;//Grid_Requisiciones.SelectedDataKey["No_Requisicion"].ToString();
        DataRow[] Solicitud = ((DataTable)Session[P_Dt_Solicitud]).Select("NO_DEUDA = '" + No_Solicitud + "'");
        Txt_No_Solicitud.Text = Solicitud[0]["NO_DEUDA"].ToString();
        String Fecha = Solicitud[0]["FECHA_CREO"].ToString();
        Fecha = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Fecha));
        Txt_Fecha.Text = Fecha.ToUpper();
        //Seleccionar combo dependencia
        Txt_Tipo_Solicitud_Pago.Text =
            Solicitud[0]["TIPO_MOVIMIENTO"].ToString().Trim();
        //Total de la requisición
        //Se llena el combo partidas
        Txt_Estatus_Solicitud.Text =
            Solicitud[0]["ESTATUS"].ToString();
        Txt_Concepto.Text =
            Solicitud[0]["CONCEPTO"].ToString();
        String Monto = Solicitud[0]["IMPORTE"].ToString();
        Monto = String.Format("{0:c}", Convert.ToDouble(Monto));
        Txt_Monto.Text = Monto;
        Txt_Beneficiario_Pago.Text =
            Solicitud[0]["NOMBRE"].ToString();
        Btn_Nuevo.Visible = true;
        Rs_Banco_Pago.P_Banco_ID = Solicitud[0]["CUENTA_BANCO_PAGO"].ToString();
        Dt_Banco_Pago = Rs_Banco_Pago.Consulta_Bancos();
        if (Dt_Banco_Pago.Rows.Count > 0)
        {
            //se selecciona el banco que tiene asignada la fuente de financiamiento para pagar
            Cmb_Banco.SelectedValue = Dt_Banco_Pago.DefaultView.ToTable().Rows[0]["NOMBRE"].ToString();
            // se llena el combo de cuentas con las cuentas del banco que se selecciono anteriormente
            Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
            Dt_Consulta_Cuentas = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Cuenta_Banco, Dt_Consulta_Cuentas, "CUENTA", "BANCO_ID");
            Dt_Consulta_Cuentas.DefaultView.RowFilter = "BANCO_ID=" + Solicitud[0]["CUENTA_BANCO_PAGO"].ToString();
            if (Dt_Consulta_Cuentas.DefaultView.ToTable().Rows.Count > 0)
            {
                //Se selecciona la cuenta del banco que tiene la fuente de financiamiento para pagar
                Cmb_Cuenta_Banco.SelectedValue = Solicitud[0]["CUENTA_BANCO_PAGO"].ToString();
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Txt_No_Cheque.Text = "";
                Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();
                //  obtendra el numero de folio de los cheques
                if (Dt_Consulta.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                            Folio_Actual = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()));

                        else
                            Estado_Folio = true;

                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()))
                            Folio_Final = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()));

                        else
                            Estado_Folio = true;
                    }
                }
                //  si contiene informacion
                if (Estado_Folio == false)
                {
                    if (Folio_Actual <= Folio_Final)
                        Txt_No_Cheque.Text = "" + Folio_Actual;
                    //  si el folio se pasa del final manda un mensaje
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Se terminaron los folios del Talonario de cheques actual por favor ingrese nuevos folios";
                    }
                }
                //  no tiene asignado los numeros de folio de los chuques
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Por favor defina el rango de los folio del Talonario de cheques para poder utilizarlos en esta ventana";
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "La cuenta bancaria no tiene asignada cuenta contable favor de agregarla en el catalogo de bancos";
            }
        }
        else
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "La cuenta bancaria no tiene asignada cuenta contable favor de agregarla en el catalogo de bancos";
        }
        Rs_Solicitud.P_No_Deuda = Solicitud[0]["NO_DEUDA"].ToString();
        Dt_Solicitud_Pagos = Rs_Solicitud.Consulta_Datos_Solicitud_Pago();
        foreach (DataRow Registro in Dt_Solicitud_Pagos.Rows)
        {
            if (!String.IsNullOrEmpty(Registro["CUENTA_DEUDOR"].ToString()))
            {
                Rs_Cuentas_Proveedor.P_Cuenta_Contable_ID = Registro["CUENTA_DEUDOR"].ToString();
                    Dt_C_Proveedor = Rs_Cuentas_Proveedor.Consulta_Existencia_Cuenta_Contable();
                    if (Dt_C_Proveedor.Rows.Count > 0)
                    {
                        Txt_Cuenta_Contable_Proveedor.Value = Dt_C_Proveedor.Rows[0]["Cuenta_Contable_ID"].ToString();
                        Txt_Cuenta.Text = Dt_C_Proveedor.Rows[0]["Cuenta"].ToString();
                        Txt_Beneficiario_ID.Value = Registro[OPE_CON_DEUDORES.Campo_Deudor_ID].ToString();
                        if (Registro[OPE_CON_DEUDORES.Campo_Tipo_Deudor].ToString() == "PROVEEDOR")
                        { Txt_TIpo_Benenficiario.Value = "PROVEEDOR"; }
                        else
                        {
                            Txt_TIpo_Benenficiario.Value = "EMPLEADO";
                        }
                    }
            }
            else
            {
                Rs_Cuentas_Proveedor.P_Cuenta = "112300001";
                Dt_C_Proveedor = Rs_Cuentas_Proveedor.Consulta_Existencia_Cuenta_Contable();
                if (Dt_C_Proveedor.Rows.Count > 0)
                {
                    Txt_Cuenta_Contable_Proveedor.Value = Dt_C_Proveedor.Rows[0]["Cuenta_Contable_ID"].ToString();
                    Txt_Cuenta.Text = Dt_C_Proveedor.Rows[0]["Cuenta"].ToString();
                    Txt_Beneficiario_ID.Value = Registro[OPE_CON_DEUDORES.Campo_Deudor_ID].ToString();
                    if (Registro[OPE_CON_DEUDORES.Campo_Tipo_Deudor].ToString() == "PROVEEDOR")
                    { Txt_TIpo_Benenficiario.Value = "PROVEEDOR"; }
                    else
                    {
                        Txt_TIpo_Benenficiario.Value = "EMPLEADO";
                    }
                }
            }
            if (!String.IsNullOrEmpty(Registro["TIPO_MOVIMIENTO"].ToString()))
            {
                Txt_Tipo_Solicitud.Value = Registro["TIPO_MOVIMIENTO"].ToString();
                Hd_TIpo_Solicitud_Pago.Value = Registro[OPE_CON_DEUDORES.Campo_Tipo_Solicitud_ID].ToString();
            }
            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Txt_Cuenta.Text;
            Dt_Cuenta = Rs_Consulta_Con_Cuentas_Contables.Consulta_Cuentas_Contables();
            foreach (DataRow fila in Dt_Cuenta.Rows)
            {
                Txt_Cuenta.Text = fila["CUENTA"].ToString();
            }
        }
        if (Txt_Estatus_Solicitud.Text == "PAGADO" )
        {
            Cheques.P_No_Deuda = No_Solicitud;
            Cheques.P_Estatus = "PAGADO";
            DataTable Pago = Cheques.Consulta_Datos_Pago_Deudores();
            if (Pago.Rows.Count > 0)
            {
                Txt_No_Pago.Text =
                Pago.Rows[0]["NO_PAGO"].ToString().Trim();
                Txt_Fecha.Text =
                 string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Pago.Rows[0]["FECHA_PAGO"].ToString()));
                Cmb_Tipo_Pago.SelectedValue = Pago.Rows[0]["FORMA_PAGO"].ToString().Trim();
                Cmb_Estatus.SelectedValue = Pago.Rows[0]["ESTATUS"].ToString().Trim();
                if (Pago.Rows[0]["ESTATUS"].ToString().Trim() == "CANCELADO")
                {
                    Txt_Motivo_Cancelacion.Text =
                   Pago.Rows[0]["MOTIVO_CANCELACION"].ToString();
                }
                Txt_No_Cheque.Text =
                    Pago.Rows[0]["NO_CHEQUE"].ToString();
                Txt_Referencia_Pago.Text =
                    Pago.Rows[0]["REFERENCIA_TRANSFERENCIA_BANCA"].ToString();
                Txt_Comentario_Pago.Text =
                    Pago.Rows[0]["COMENTARIOS"].ToString();
                Txt_Beneficiario_Pago.Text =
                    Pago.Rows[0]["BENEFICIARIO_PAGO"].ToString();
                Cmb_Banco.SelectedValue = Pago.Rows[0]["NOMBRE"].ToString();
                Txt_Cuenta_Contable_ID_Banco.Value = Pago.Rows[0]["CUENTA_CONTABLE_ID"].ToString();
                Cmb_Cuenta_Banco.Enabled = false;
            }
            Btn_Modificar.Visible = true;
            Cmb_Estatus.Enabled = true;
        }
        //se comento porque se cambio la jugada
        //Consultar_Fuente_Banco(Txt_No_Reserva_Solicitud.Text);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Fuente_Banco
    ///DESCRIPCIÓN:Se consulta la reserva para ver que fuente de financiamiento esta asignada
    ///             y asi saber de que banco se puede pagar esta solicitud
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consultar_Fuente_Banco(String  Reserva)
    {
        Cls_Ope_Con_Reservas_Negocio Rs_Reservas = new Cls_Ope_Con_Reservas_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Datos_Reserva = new DataTable();
        DataTable Dt_Bancos = new DataTable();
        DataTable Dt_Consulta_Cuentas = new DataTable();
        String Fuente_Financiamiento="";
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        Int32 Folio_Actual = 0;
        Int32 Folio_Final = 0;
        Boolean Estado_Folio = false;
        try
        {
            Rs_Reservas.P_No_Reserva = Reserva;
            Dt_Datos_Reserva = Rs_Reservas.Consultar_Reservas_Detalladas();
            if (Dt_Datos_Reserva.Rows.Count > 0)
            {
                Fuente_Financiamiento = Dt_Datos_Reserva.Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString();
                if (!String.IsNullOrEmpty(Fuente_Financiamiento))
                {
                    Dt_Bancos=Rs_Bancos.Consultar_Bancos();
                    if (Dt_Bancos.Rows.Count > 0)
                    {
                        Dt_Bancos.DefaultView.RowFilter = "FUENTE_FINANCIAMIENTO_ID=" + Fuente_Financiamiento;
                        if (Dt_Bancos.DefaultView.ToTable().Rows.Count > 0)
                        {
                            //se selecciona el banco que tiene asignada la fuente de financiamiento para pagar
                            Cmb_Banco.SelectedValue = Dt_Bancos.DefaultView.ToTable().Rows[0]["NOMBRE"].ToString();
                            // se llena el combo de cuentas con las cuentas del banco que se selecciono anteriormente
                            Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
                            Dt_Consulta_Cuentas = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Cuenta_Banco, Dt_Consulta_Cuentas, "CUENTA", "BANCO_ID");
                            Dt_Consulta_Cuentas.DefaultView.RowFilter = "BANCO_ID=" + Dt_Bancos.DefaultView.ToTable().Rows[0]["BANCO_ID"].ToString();
                            if (Dt_Consulta_Cuentas.DefaultView.ToTable().Rows.Count > 0)
                            {
                                //Se selecciona la cuenta del banco que tiene la fuente de financiamiento para pagar
                                Cmb_Cuenta_Banco.SelectedValue = Dt_Bancos.DefaultView.ToTable().Rows[0]["BANCO_ID"].ToString();
                                Lbl_Mensaje_Error.Visible = false;
                                Img_Error.Visible = false;
                                Txt_No_Cheque.Text = "";
                                Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                                Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();
                                //  obtendra el numero de folio de los cheques
                                if (Dt_Consulta.Rows.Count > 0)
                                {
                                    foreach (DataRow Registro in Dt_Consulta.Rows)
                                    {
                                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                                            Folio_Actual = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()));

                                        else
                                            Estado_Folio = true;

                                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()))
                                            Folio_Final = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()));

                                        else
                                            Estado_Folio = true;
                                    }
                                }
                                //  si contiene informacion
                                if (Estado_Folio == false)
                                {
                                    if (Folio_Actual <= Folio_Final)
                                        Txt_No_Cheque.Text = "" + Folio_Actual;
                                    //  si el folio se pasa del final manda un mensaje
                                    else
                                    {
                                        Lbl_Mensaje_Error.Visible = true;
                                        Img_Error.Visible = true;
                                        Lbl_Mensaje_Error.Text = "Se terminaron los folios del Talonario de cheques actual por favor ingrese nuevos folios";
                                    }
                                }
                                //  no tiene asignado los numeros de folio de los chuques
                                else
                                {
                                    Lbl_Mensaje_Error.Visible = true;
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Text = "Por favor defina el rango de los folio del Talonario de cheques para poder utilizarlos en esta ventana";
                                }
                            }
                            else
                            {
                                Lbl_Mensaje_Error.Visible = true;
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Text = "La cuenta bancaria no tiene asignada cuenta contable favor de agregarla en el catalogo de bancos";
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Consultar la fuente de financiamiento para conocer el banco " + ex.Message.ToString(), ex);
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Combos_Generales()
    // DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 17/Noviembre/2011 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Combos_Generales()
    {
        Cls_Cat_Nom_Bancos_Negocio Bancos_Negocio = new Cls_Cat_Nom_Bancos_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        //DataTable Dt_Bancos = Bancos_Negocio.Consulta_Bancos();
        DataTable Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
        Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Banco, Dt_Existencia, "NOMBRE", "NOMBRE");
        //Consultar_Tipos_Solicitud();
    }
 
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Validaciones
    // DESCRIPCIÓN: Genera el String con la informacion que falta y ejecuta la 
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private Boolean Validaciones(bool Validar_Completo)
    {
        Boolean Bln_Bandera;
        Bln_Bandera = true;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br />";
        //Verifica que campos esten seleccionados o tengan valor valor
        if (Cmb_Banco.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += " Seleccionar un Banco. <br />";
            Bln_Bandera = false;
        }
        if (Cmb_Tipo_Pago.SelectedValue == "CHEQUE")
        {
            if (Cmb_Cuenta_Banco.SelectedIndex == 0)
            {
                Lbl_Mensaje_Error.Text += " Seleccione la cuenta del banco <br />";
                Bln_Bandera = false;
            }
            if (Txt_No_Cheque.Text == "")
            {
                Lbl_Mensaje_Error.Text += " Introducir el número de cheque <br />";
                Bln_Bandera = false;
            }
        }
        else
        {
            if (Txt_Referencia_Pago.Text == "")
            {
                Lbl_Mensaje_Error.Text += " Introducir la Referencia de la transferencia<br />";
                Bln_Bandera = false;
            }
        }
        if (Txt_Beneficiario_Pago.Text == "")
        {
            Lbl_Mensaje_Error.Text += " Introducir un beneficiario <br />";
            Bln_Bandera = false;
        }
        if (Cmb_Estatus.SelectedValue == "CANCELADO")
        {
            if (Txt_Motivo_Cancelacion.Text == "")
            {
                Lbl_Mensaje_Error.Text += " Introducir el motivo de la cancelación  <br />";
                Bln_Bandera = false;
            }
        }
        if (!Bln_Bandera)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
        return Bln_Bandera;
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Validaciones
    // DESCRIPCIÓN: Genera el String con la informacion que falta y ejecuta la 
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private Boolean Validacion_Transferencia(bool Validar_Completo, String No_Solicitud, String Tipo)
    {
        Boolean Bln_Bandera;
        Bln_Bandera = true;
        if (Tipo.Equals("1"))
        {
            //if (Txt_Orden.Text == "")
            //{
            //    Lbl_Mensaje_Error.Text += " Introducir el número de orden de la transferencia <br>";
            //    Bln_Bandera = false;
            //}
        }
        else
        {
            try
            {
                DataTable Dt_Consulta = new DataTable();
                DataTable Dt_Solicitud = new DataTable();
                Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Datos_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
                    Rs_Datos_Solicitud.P_No_Deuda  = No_Solicitud;
                    Dt_Solicitud=Rs_Datos_Solicitud.Consulta_Solicitud_Pago();
                    if (Dt_Solicitud.Rows.Count > 0)
                    {
                        if (Dt_Solicitud.Rows[0]["TIPO_DEUDOR"].ToString()=="EMPLEADO")
                        {
                            Rs_Datos_Solicitud.P_No_Deuda = No_Solicitud;
                            Dt_Consulta = Rs_Datos_Solicitud.Consultar_Datos_Bancarios();
                            if (Dt_Consulta.Rows.Count > 0)
                            {
                                foreach (DataRow Registro in Dt_Consulta.Rows)
                                {
                                    if (Registro["NOMBRE_BANCO"].ToString() == "" || Registro[Cat_Empleados.Campo_No_Tarjeta].ToString() == "")
                                    {
                                        Lbl_Mensaje_Error.Text += "**El Empleado " + Registro["Nombre_Beneficiario"].ToString() + " Con numero de solicitud de pago " +
                                                Registro[OPE_CON_DEUDORES.Campo_No_Deuda].ToString() + " no tiene informacion bancaria asignada, favor de ingresar los datos" + "<br>";
                                        Bln_Bandera = false;
                                    }

                                }// fin del foreach

                            }// fin del if Dt_Consulta
                        }
                        else
                        {
                            Rs_Datos_Solicitud.P_No_Deuda = No_Solicitud;
                            Dt_Consulta = Rs_Datos_Solicitud.Consultar_Datos_Bancarios_Proveedor();
                            if (Dt_Consulta.Rows.Count > 0)
                            {
                                foreach (DataRow Registro in Dt_Consulta.Rows)
                                {
                                    if (Registro["NOMBRE_BANCO"].ToString() == "" ||
                                                Registro[Cat_Com_Proveedores.Campo_Banco_Proveedor_ID].ToString() == "" ||
                                                Registro[Cat_Com_Proveedores.Campo_Clabe].ToString() == "")
                                    {
                                        Lbl_Mensaje_Error.Text += "**El Proveedor " + Registro["Nombre_Beneficiario"].ToString() + " Con numero de solicitud de pago " +
                                                Registro[OPE_CON_DEUDORES.Campo_No_Deuda].ToString() + " no tiene informacion bancaria asignada, favor de ingresar los datos" + "<br>";
                                        Bln_Bandera = false;
                                    }

                                }// fin del foreach

                            }// fin del if Dt_Consulta
                        }
                    }
            }// fin del try
            catch(Exception ex)
            {
                throw new Exception("Validacion bancaria " + ex.Message.ToString(), ex);
            }
        }
        if (!Bln_Bandera)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
        return Bln_Bandera;
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Validaciones
    // DESCRIPCIÓN: Genera el String con la informacion que falta y ejecuta la 
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private Boolean Validar_Modificacion(bool Validar_Completo)
    {
        Boolean Bln_Bandera;
        Bln_Bandera = true;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
        //Verifica que campos esten seleccionados o tengan valor valor
        if (Txt_Motivo_Cancelacion.Text == "" || Txt_Motivo_Cancelacion.Text == null)
        {
            Lbl_Mensaje_Error.Text += " El Motivo de la cancelacion del Pago <br>";
            Bln_Bandera = false;
        }
        if (!Bln_Bandera)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
        return Bln_Bandera;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Cheque
    /// DESCRIPCION : Modifica los datos del pago  con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 25/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Modificar_Cheque()
    {
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Modificar_Ope_Con_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        //        Cls_Ope_Con_Cheques_Negocio Rs_Modificar_Ope_Con_Pagos = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexión hacia la capa de Negocios
        try
        {
            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario, typeof(System.String));

            DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
            //Agrega el cargo del registro de la póliza
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_Proveedor.Value;
            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION- DEL PAGO DE LA SOLICITUD DEL DEUDOR No." + Txt_No_Solicitud.Text.ToString();
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Txt_Monto.Text.ToString().Replace("$", "").Replace(",", ""));
            row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Txt_Beneficiario_ID.Value;
            row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Txt_TIpo_Benenficiario.Value;
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();

            row = Dt_Partidas_Polizas.NewRow();
            //Agrega el abono del registro de la póliza
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Banco.Value;
            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CANCELACION- DEL PAGO DE LA SOLICITUD DEL DEUDOR No." + Txt_No_Solicitud.Text.ToString();
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Monto.Text.ToString().Replace("$", "").Replace(",", ""));
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Txt_Beneficiario_ID.Value;
            row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Txt_TIpo_Benenficiario.Value;
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();

            //Agrega los valores a pasar a la capa de negocios para ser dados de alta
            Rs_Modificar_Ope_Con_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
            Rs_Modificar_Ope_Con_Pagos.P_No_Deuda = Txt_No_Solicitud.Text;
            Rs_Modificar_Ope_Con_Pagos.P_Estatus = "CANCELADO";
            Rs_Modificar_Ope_Con_Pagos.P_Importe = Txt_Monto.Text.Replace("$","").Replace(",","");
            Rs_Modificar_Ope_Con_Pagos.P_Comentario = "CANCELACION- DEL PAGO DE LA SOLICITUD DEL DEUDOR No." + Txt_No_Solicitud.Text.ToString();
            Rs_Modificar_Ope_Con_Pagos.P_Motivo_Cancelacion = Txt_Motivo_Cancelacion.Text;
            Rs_Modificar_Ope_Con_Pagos.P_Usuario_Autorizo = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Ope_Con_Pagos.P_No_Pago = Txt_No_Pago.Text.ToString();
            Rs_Modificar_Ope_Con_Pagos.Modificar_Pago(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Cheque
    /// DESCRIPCION : Da de Alta el Cheque con los datos proporcionados
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 21/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Cheque()
    {
        try
        {
            String Resultado="";
            String Cuentas_Por_Pagar = "";
            String Bancos_Otros = "";
            Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
            DataTable Dt_Consulta = new DataTable();
            DataTable Dt_Reserva = new DataTable();
            String Cheque = "";
            String Fondo_Revolvente = "";
            DataTable Dt_Banco_Cuenta_Contable = new DataTable();
            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Parametros = new DataTable();
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
            Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos            
            Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
            Rs_Ope_Con_Cheques.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
            foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
            {
                Txt_Cuenta_Contable_ID_Banco.Value = Renglon[Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
            }
            //consultar el tipo de solicitud de pago
            Dt_Parametros=Rs_Parametros.Consulta_Datos_Parametros_2();
            if (Dt_Parametros.Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Solicitud_Fondo_Revolvente].ToString()))
                {
                     Fondo_Revolvente = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Solicitud_Fondo_Revolvente].ToString();
                }
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Banco_Otros_Deudor].ToString()))
                {
                    Bancos_Otros = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Banco_Otros_Deudor].ToString();
                }
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Por_Pagar_Deudor].ToString()))
                {
                    Cuentas_Por_Pagar = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Por_Pagar_Deudor].ToString();
                }
            }
            if (Hd_TIpo_Solicitud_Pago.Value == Fondo_Revolvente)
            {
                if (!String.IsNullOrEmpty(Bancos_Otros) && !String.IsNullOrEmpty(Cuentas_Por_Pagar))
                {
                    Alta_Cheque_Fondo_Revolvente(Bancos_Otros, Cuentas_Por_Pagar);
                }
                else
                {
                    if (String.IsNullOrEmpty(Bancos_Otros) && String.IsNullOrEmpty(Cuentas_Por_Pagar))
                    {
                        Lbl_Mensaje_Error.Text = "Error:";
                        Lbl_Mensaje_Error.Text = "No se han cargado las cuentas Deudor de los parametros, Favor de validarlo con Contabilidad";
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Visible = true;
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(Cuentas_Por_Pagar))
                        {
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = "No se han cargado las cuentas por pagar del deudor, Favor de validarlo con Contabilidad";
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Visible = true;
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = "No se han cargado las cuentas otros pagos, Favor de validarlo con Contabilidad";
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Visible = true;
                        }
                    }
                }
            }
            else
            {
                //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario, typeof(System.String));
                
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla


                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Banco.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Txt_Monto.Text.ToString().Replace("$","").Replace(",",""));// -(Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
                row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Txt_Beneficiario_ID.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Txt_TIpo_Benenficiario.Value;

                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                row = Dt_Partidas_Polizas.NewRow();
                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_Proveedor.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Monto.Text.ToString().Replace("$", "").Replace(",", ""));// - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
                row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Txt_Beneficiario_ID.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Txt_TIpo_Benenficiario.Value;
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();

                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
                
                
                Rs_Solicitudes.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Solicitudes.P_No_Deuda = Txt_No_Solicitud.Text.Trim();
                Rs_Solicitudes.P_Comentario = Txt_Comentario_Pago.Text.Trim();
                Rs_Solicitudes.P_Importe = Txt_Monto.Text.Trim().Replace("$","").Replace(",","");
                Rs_Solicitudes.P_No_Partidas = "2";
                Rs_Solicitudes.P_Estatus = Cmb_Estatus.SelectedItem.ToString();
                Rs_Solicitudes.P_Fecha_Pago = String.Format("{0:dd/MM/yy}", Convert.ToDateTime(Txt_Fecha_No_Pago.Text)).ToString();
                Rs_Solicitudes.P_Forma_Pago = Cmb_Tipo_Pago.SelectedItem.ToString();
                Rs_Solicitudes.P_Cuenta_Banco_Pago = Cmb_Cuenta_Banco.SelectedValue;
                Rs_Solicitudes.P_Referencia = Txt_Referencia_Pago.Text.ToString();
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                if (Txt_Beneficiario_Pago.Text.Trim().Substring(1,1) == "-")
                {
                    Rs_Solicitudes.P_Beneficiario_Pago = Txt_Beneficiario_Pago.Text.Trim().Substring(2);
                }
                else
                {
                    Rs_Solicitudes.P_Beneficiario_Pago = Txt_Beneficiario_Pago.Text.Trim();
                }
                Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();
                //  obtendra el numero de folio de los cheques
                if (Dt_Consulta.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                        {
                            Txt_No_Cheque.Text = Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString();
                        }
                    }
                }
                Rs_Solicitudes.P_No_Cheque = Txt_No_Cheque.Text.Trim();
                Rs_Solicitudes.P_Usuario_Autorizo = Cls_Sessiones.Nombre_Empleado;
                Rs_Solicitudes.P_Cmmd = Cmmd;
                Cheque = Rs_Solicitudes.Alta_Cheque();
                Resultado = Cheque.Substring(6, 2);
                if (Resultado == "SI")
                {
                    Cheque = Cheque.Substring(0, 5);
                    if (Cmb_Tipo_Pago.SelectedValue == "CHEQUE")
                    {
                      Imprimir(Cheque, Convert.ToDecimal(Txt_Monto.Text.Trim().Replace("$","").Replace(",","")), Cmmd);
                        // cambiamos el numero de folio de los cheques
                      Modificar_Numero_Folio(Cmmd, null);
                    }
                    Trans.Commit();
                    Limpia_Controles();
                    Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                }
                else
                {
                    Trans.Rollback();
                    Lbl_Mensaje_Error.Text = "Error:";
                    Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para realizar el pago ";
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Visible = true;
                }
             }
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_CHEQUE " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String Cheque, Decimal Monto, SqlCommand P_Cmmd)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Cabecera = new DataTable();
        String Letra = "";
        String Banco_ID = "";
        String No_Solicitud_Pago = "";

        Cls_Ope_Con_Cheques_Negocio Rs_Cheques = new Cls_Ope_Con_Cheques_Negocio();
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            Cls_Ope_Con_Cheques_Negocio Datos_Cheque = new Cls_Ope_Con_Cheques_Negocio();
            Ds_Reporte = new DataSet();
            Datos_Cheque.P_No_Cheque = Cheque;
            Datos_Cheque.P_Cmmd = Cmmd;
            Dt_Pagos = Datos_Cheque.Consulta_Pago_Cheque();
            Letra = Convertir_Cantidad_Letras(Monto);
            if (Dt_Temporal.Rows.Count <= 0)
            {
                Dt_Temporal.Columns.Add("NO_PAGO", typeof(System.String));
                Dt_Temporal.Columns.Add("MONTO", typeof(System.Decimal));
                Dt_Temporal.Columns.Add("BENEFICIARIO", typeof(System.String));
                Dt_Temporal.Columns.Add("MONTO_LETRA", typeof(System.String));
                Dt_Temporal.Columns.Add("NO_CHEQUE", typeof(System.String));
                Dt_Temporal.Columns.Add("ABONO_BENEFICIARIO", typeof(System.String));

                Dt_Temporal.TableName = "Dt_Cheque";
            }
            foreach (DataRow Registro in Dt_Pagos.Rows)
            {
                DataRow row = Dt_Temporal.NewRow(); //Crea un nuevo registro a la tabla
                row["NO_PAGO"] = Registro["NO_PAGO"].ToString();
                row["MONTO"] = Monto;
                if (Registro["BENEFICIARIO_PAGO"].ToString().Contains('-'))
                {
                    row["BENEFICIARIO"] = Registro["BENEFICIARIO_PAGO"].ToString().Substring(2);
                }
                else
                {
                    row["BENEFICIARIO"] = Registro["BENEFICIARIO_PAGO"].ToString();
                }
                Banco_ID = Registro["BANCO_ID"].ToString();
                No_Solicitud_Pago = Registro["NO_SOLICITUD_PAGO"].ToString();
                row["MONTO_LETRA"] = Letra;
                row["NO_CHEQUE"] = Registro["NO_CHEQUE"].ToString();
                if (!String.IsNullOrEmpty(Registro[Ope_Con_Pagos.Campo_Abono_Cuenta_Beneficiario].ToString()))
                {
                    if (Registro[Ope_Con_Pagos.Campo_Abono_Cuenta_Beneficiario].ToString() == "SI")
                    {
                        row["ABONO_BENEFICIARIO"] = "PARA ABONO EN CUENTA DEL BENEFICIARIO";
                    }
                    else
                    {
                        row["ABONO_BENEFICIARIO"] = "";
                    }
                }
                else
                {
                    row["ABONO_BENEFICIARIO"] = "";
                }
                Dt_Temporal.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Temporal.AcceptChanges();
            }
            if (Dt_Pagos.Rows.Count > 0)
            {
                //Ds_Reporte.Tables.Add(Dt_Cabecera);
                Ds_Reporte.Tables.Add(Dt_Temporal.Copy());
                Ds_Rpt_Con_Cheque Obj_Data_SET = new Ds_Rpt_Con_Cheque();

                //Se llama al método que ejecuta la operación de generar el reporte.
                string nombre_pdf = "Reporte_Cheques" + Cheque + ".pdf";
                Generar_Reporte(Ds_Reporte, Obj_Data_SET, "Rpt_Con_Cheque.rpt", nombre_pdf);
            }
            //Insertar en la tabla de ope_con_cheques 
            Rs_Cheques.P_No_Cheque = Dt_Temporal.Rows[0]["NO_CHEQUE"].ToString();
            Rs_Cheques.P_No_Pago = Dt_Temporal.Rows[0]["NO_PAGO"].ToString();
            Rs_Cheques.P_Banco_ID = Banco_ID;
            Rs_Cheques.P_Estatus = "PENDIENTE";
            Rs_Cheques.P_Concepto = "CHEQUE PARA EL PAGO DE LA SOLICITUD" + No_Solicitud_Pago;
            Rs_Cheques.P_Monto = Dt_Temporal.Rows[0]["MONTO"].ToString();
            Rs_Cheques.P_Monto_Letra = Letra;
            Rs_Cheques.P_Beneficiario_Pago = Dt_Temporal.Rows[0]["BENEFICIARIO"].ToString();
            Rs_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.ToString();
            Rs_Cheques.P_Cmmd = Cmmd;
            Rs_Cheques.Alta_Seguimiento_Cheque();
            if (P_Cmmd == null)
            {
                Trans.Commit();
            }
        }
        //}
        catch (Exception Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            Lbl_Mensaje_Error.Visible = true;
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Convertir_Cantidad_Letras
    ///DESCRIPCIÓN: Convierte una cantidad en letra
    ///PARAMETROS: 
    ///CREO:        
    ///FECHA_CREO:  15-Marzo-2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected String Convertir_Cantidad_Letras(Decimal Cantidad_Numero)
    {
        Numalet Obj_Numale = new Numalet();
        String Cantidad_Letra = String.Empty;

        try
        {
            Obj_Numale.MascaraSalidaDecimal = "00/100 M.N.";
            Obj_Numale.SeparadorDecimalSalida = "pesos";
            Obj_Numale.ApocoparUnoParteEntera = true;
            Cantidad_Letra = Obj_Numale.ToCustomCardinal(Cantidad_Numero).Trim().ToUpper();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al convertir la cantidad a letras. Error:[" + Ex.Message + "]");
        }
        return Cantidad_Letra;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Modificar_Numero_Folio
    ///DESCRIPCIÓN: Resibe la informacion para poder modificar el folio Actual
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  31/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Numero_Folio(SqlCommand P_Cmmd, String Banco_ID)
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Modificar_Folio_Actual = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Int32 Folio = 0;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            Folio = Convert.ToInt32(Txt_No_Cheque.Text);
            Folio++;
            if (Cmb_Cuenta_Banco.SelectedValue != "")
            {
                Rs_Modificar_Folio_Actual.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            }
            else
            {
                Rs_Modificar_Folio_Actual.P_Banco_ID = Banco_ID;
            }
            Rs_Modificar_Folio_Actual.P_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Folio_Actual.P_Folio_Actual = "" + Folio;
            Rs_Modificar_Folio_Actual.P_Cmmd = Cmmd;
            Rs_Modificar_Folio_Actual.Modificar_Folio_Actual();
            if (P_Cmmd == null)
            {
                Trans.Commit();
            }
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (Exception ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
            throw new Exception(ex.Message, ex);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    
    protected void Btn_Transferir_Click(object sender, EventArgs e)
    {
        DataTable Dt_Consulta = new DataTable();
        Cls_Ope_Con_Cheques_Negocio Rs_Datos_Solicitud = new Cls_Ope_Con_Cheques_Negocio();
        //Cls_Ope_Con_Cheques_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Cheques_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Modificar_Ope_con_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        DataTable Dt_Transferencia = new DataTable();
        DataTable Dt_Datos_Polizas = new DataTable();
        DataTable Dt_Tipo_Solicitud = new DataTable();
        DataTable Dt_Solicitudes_Datos = new DataTable();
        DataTable Dt_Reporte_Agrepado = new DataTable();
        Boolean Autorizado;
         Int32 Indice =0;
         Boolean Agregar;
         Boolean Transferir;
         String Orden = "";
        String CUENTA_ORIGEN;
        String CUENTA_DESTINO;
        String FECHA;
        String BENEFICIARIO;
        String PESTANIA;
        String Concepto;
        Double Importe;
        String Formato_Imp = "";
        DataTable Dt_Reporte = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataSet Ds_final = new DataSet();
        int Cant = 0;
        //int Cont = 0;
        //int contador = 0; 
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
        try
        {
            if (Validacion_Transferencia(true,"", "1"))
            {
                if (Dt_Solicitudes_Datos.Rows.Count <= 0)
                {
                    Dt_Solicitudes_Datos.Columns.Add("No_Solicitud", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Fecha_Pago", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Banco", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Deudor", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Tipo_Deudor", typeof(System.String));
                }
                foreach (GridViewRow Renglon_Grid in Grid_Pagos.Rows)
                {
                    GridView Grid_Deudores = (GridView)Renglon_Grid.FindControl("Grid_Deudores");
                    foreach (GridViewRow Renglon in Grid_Deudores.Rows)
                    {
                        GridView Grid_Datos_Solicitud = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                        foreach (GridViewRow fila in Grid_Datos_Solicitud.Rows)
                        {
                            Indice++;
                            Grid_Datos_Solicitud.SelectedIndex = Indice;
                            Autorizado = ((CheckBox)fila.FindControl("Chk_Transferencia")).Checked;
                            if (Autorizado)
                            {
                                DataRow row = Dt_Solicitudes_Datos.NewRow(); //Crea un nuevo registro a la tabla
                                //Asigna los valores al nuevo registro creado a la tabla
                                row["No_Solicitud"] = ((CheckBox)fila.FindControl("Chk_Transferencia")).CssClass;
                                row["Fecha_Pago"] = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(((TextBox)fila.FindControl("Txt_Fecha_Transferencia")).Text.ToString()));
                                row["Banco"] = fila.Cells[5].Text;
                                row["Deudor"] = Renglon.Cells[1].Text;
                                row["Tipo_Deudor"] = Renglon.Cells[6].Text;
                                Dt_Solicitudes_Datos.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Solicitudes_Datos.AcceptChanges();
                            }
                        }
                    }
                }
                if (Dt_Solicitudes_Datos.Rows.Count > 0)
                {
                    Dt_Solicitudes_Datos.DefaultView.RowFilter = "Banco='BANORTE'";
                    if (Dt_Solicitudes_Datos.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Transferir = true;
                        Lbl_Mensaje_Error.Visible = false;
                        Img_Error.Visible = false;
                        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                        foreach (DataRow Validar_Fila in Dt_Solicitudes_Datos.DefaultView.ToTable().Rows)
                        {
                            if (!Validacion_Transferencia(true, Validar_Fila["No_Solicitud"].ToString(), "2"))
                            {
                                Transferir = false;
                            }
                        }
                        if (Transferir)
                        {
                            if (Dt_Reporte.Rows.Count <= 0)
                            {
                                Dt_Reporte.Columns.Add("TIPO", typeof(System.String));
                                Dt_Reporte.Columns.Add("ID", typeof(System.String));
                                Dt_Reporte.Columns.Add("CUENTA ORIGEN", typeof(System.String));
                                Dt_Reporte.Columns.Add("CUENTA DESTINO", typeof(System.String));
                                Dt_Reporte.Columns.Add("IMPORTE", typeof(System.String));
                                Dt_Reporte.Columns.Add("ORDEN", typeof(System.String));
                                Dt_Reporte.Columns.Add("CONCEPTO", typeof(System.String));
                                Dt_Reporte.Columns.Add("TM", typeof(System.String));
                                Dt_Reporte.Columns.Add("RFC MUNICIPIO", typeof(System.String));
                                Dt_Reporte.Columns.Add("CEROS", typeof(System.String));
                                Dt_Reporte.Columns.Add("EMAIL", typeof(System.String));
                                Dt_Reporte.Columns.Add("FECHA", typeof(System.String));
                                Dt_Reporte.Columns.Add("BENEFICIARIO", typeof(System.String));
                                Dt_Reporte.Columns.Add("PESTANIA", typeof(System.String));
                            }
                            foreach (DataRow Fila in Dt_Solicitudes_Datos.DefaultView.ToTable().Rows)
                            {
                                //Rs_Datos_Solicitud.P_No_Solicitud_Pago = Fila["No_Solicitud"].ToString();
                                Rs_Modificar_Ope_con_Solicitud.P_No_Deuda = Fila["No_Solicitud"].ToString();
                                Dt_Consulta = Rs_Modificar_Ope_con_Solicitud.Consulta_Datos_transferencia();
                                if (Dt_Consulta.Rows.Count > 0)
                                {
                                    foreach (DataRow Renglon in Dt_Consulta.Rows)
                                    {
                                        DataRow Dt_Row = Dt_Reporte.NewRow();
                                        if (Fila["Banco"].ToString() == Renglon["BANCO_DEUDOR"].ToString() && Fila["Fecha_Pago"].ToString() == String.Format("{0:dd/MMM/yyyy}", DateTime.Now))
                                        {
                                            Dt_Row["PESTANIA"] = "BB";
                                            Dt_Row["TIPO"] = "02";
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(Renglon["BANCO_DEUDOR"].ToString()))
                                            {
                                                if (Fila["Banco"].ToString() == Renglon["BANCO_DEUDOR"].ToString() && Fila["Fecha_Pago"].ToString() != String.Format("{0:dd/MMM/yyyy}", DateTime.Now))
                                                {
                                                    Dt_Row["PESTANIA"] = "BB";
                                                    Dt_Row["TIPO"] = "05";
                                                }
                                                else
                                                {
                                                    if (Fila["Banco"].ToString() != Renglon["BANCO_DEUDOR"].ToString() && Fila["Fecha_Pago"].ToString() == String.Format("{0:dd/MMM/yyyy}", DateTime.Now))
                                                    {
                                                        Dt_Row["PESTANIA"] = "BP";
                                                        Dt_Row["TIPO"] = "04";
                                                    }
                                                    else
                                                    {
                                                        Dt_Row["PESTANIA"] = "BP";
                                                        Dt_Row["TIPO"] = "05";
                                                    }
                                                }
                                            }
                                        }
                                        Dt_Row["CUENTA ORIGEN"] = String.Format("{0:00000000000000000000}", Convert.ToInt64(Renglon["CUENTA_ORIGEN"].ToString()));
                                        Dt_Row["CEROS"] = "00000000000000";
                                        Dt_Row["RFC MUNICIPIO"] = "MIG850101IZ2 ";
                                        //if (Txt_Orden.Text.ToString().Contains('-'))
                                        //{
                                        //    Dt_Row["ORDEN"] = Txt_Orden.Text.ToString().Substring(Txt_Orden.Text.ToString().IndexOf('-') + 1);
                                        //}
                                        //else
                                        //{
                                        //    Dt_Row["ORDEN"] = Txt_Orden.Text.ToString();
                                        //}
                                        Cant = 10 - Dt_Row["ORDEN"].ToString().Length;
                                        for (int I = 0; I < Cant; I++)
                                        {
                                            Dt_Row["ORDEN"] = "0" + Dt_Row["ORDEN"].ToString();
                                        }
                                        Dt_Row["TM"] = 11;
                                        if (Renglon["CUENTA_DESTINO"].ToString() != "")
                                        {
                                            if (Dt_Row["PESTANIA"].ToString() == "BB")
                                            {
                                                Dt_Row["CUENTA DESTINO"] = String.Format("{0:00000000000000000000}", Convert.ToInt64(Renglon["CUENTA_DESTINO"].ToString()));
                                                //Dt_Row["CUENTA DESTINO"] = String.Format("{0:00000000000000000000}", Convert.ToInt64(Renglon["CUENTA_DESTINO_PROVEEDOR"].ToString()));
                                            }
                                            else
                                            {
                                                Dt_Row["CUENTA DESTINO"] = String.Format("{0:00000000000000000000}", Convert.ToDouble(Renglon["CLABE"].ToString()));
                                                //Dt_Row["CUENTA DESTINO"] = String.Format("{0:00000000000000000000}", Convert.ToInt64(Renglon["CLABE"].ToString()));
                                            }
                                            Dt_Row["EMAIL"] = Renglon["CORREO"].ToString();
                                            Dt_Row["ID"] = Renglon["ID"].ToString();
                                        }
                                        Formato_Imp = String.Format("{0:n}", Convert.ToDouble(Renglon["IMPORTE"].ToString()));
                                        Formato_Imp = Formato_Imp.Replace(",", "").Replace(".", "");
                                        Dt_Row["IMPORTE"] = String.Format("{0:00000000000000}", (Convert.ToDouble(Formato_Imp)));

                                        Dt_Row["CONCEPTO"] = Renglon["CONCEPTO"].ToString();
                                        Cant = 30 - Dt_Row["CONCEPTO"].ToString().Length;
                                        for (int I = 0; I < Cant; I++)
                                        {
                                            Dt_Row["CONCEPTO"] = Dt_Row["CONCEPTO"].ToString() + " ";
                                        }
                                        Dt_Row["FECHA"] = String.Format("{0:ddMMyyyy}", Convert.ToDateTime(Fila["Fecha_Pago"].ToString()));
                                        if (Renglon["BENEFICIARIO"].ToString().Contains('-'))
                                        {
                                            Dt_Row["BENEFICIARIO"] = Renglon["BENEFICIARIO"].ToString().Substring(Renglon["BENEFICIARIO"].ToString().IndexOf("-") + 1).Trim();
                                        }
                                        else
                                        {
                                            Dt_Row["BENEFICIARIO"] = Renglon["BENEFICIARIO"].ToString().Trim();
                                        }
                                        Dt_Reporte.Rows.Add(Dt_Row); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Reporte.AcceptChanges();
                                    }
                                    //Pasamos el datattable al dataset
                                }
                            }
                            if (Dt_Reporte.Rows.Count > 0)
                            {
                                if (Dt_Reporte_Agrepado.Rows.Count <= 0)
                                {
                                    Dt_Reporte_Agrepado.Columns.Add("TIPO", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("ID", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("CUENTA ORIGEN", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("CUENTA DESTINO", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("IMPORTE", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("ORDEN", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("CONCEPTO", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("TM", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("RFC MUNICIPIO", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("CEROS", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("EMAIL", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("FECHA", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("BENEFICIARIO", typeof(System.String));
                                    Dt_Reporte_Agrepado.Columns.Add("PESTANIA", typeof(System.String));
                                }
                                foreach (DataRow Renglon in Dt_Reporte.Rows)
                                {
                                    DataRow row = Dt_Reporte_Agrepado.NewRow(); //Crea un nuevo registro a la tabla
                                    Agregar = true;
                                    Importe = 0;
                                    Concepto = "";
                                    CUENTA_ORIGEN = Renglon["CUENTA ORIGEN"].ToString();
                                    CUENTA_DESTINO = Renglon["CUENTA DESTINO"].ToString();
                                    FECHA = Renglon["FECHA"].ToString();
                                    BENEFICIARIO = Renglon["ID"].ToString();
                                    PESTANIA = Renglon["PESTANIA"].ToString();
                                    //insertar los que no se repiten
                                    foreach (DataRow posicion in Dt_Reporte_Agrepado.Rows)
                                    {
                                        if (CUENTA_ORIGEN.Equals(posicion["CUENTA ORIGEN"].ToString()) && FECHA.Equals(posicion["FECHA"].ToString()) && BENEFICIARIO.Equals(posicion["ID"].ToString()) && PESTANIA.Equals(posicion["PESTANIA"].ToString()) && CUENTA_DESTINO.Equals(posicion["CUENTA DESTINO"].ToString()))
                                        {
                                            Agregar = false;
                                        }
                                    }
                                    //se calcula el monto por tipo
                                    foreach (DataRow Registro in Dt_Reporte.Rows)
                                    {
                                        if (CUENTA_ORIGEN.Equals(Registro["CUENTA ORIGEN"].ToString()) && FECHA.Equals(Registro["FECHA"].ToString()) && BENEFICIARIO.Equals(Registro["ID"].ToString()) && PESTANIA.Equals(Registro["PESTANIA"].ToString()) && CUENTA_DESTINO.Equals(Registro["CUENTA DESTINO"].ToString()))
                                        {
                                            Importe = Importe + Convert.ToDouble(Registro["IMPORTE"].ToString());
                                            Concepto = Concepto + Registro["CONCEPTO"].ToString().Replace('X', ' ').Trim() + " ";
                                        }
                                    }
                                    if (Agregar && Importe > 0)
                                    {
                                        row["TIPO"] = Renglon["TIPO"].ToString();
                                        row["ID"] = BENEFICIARIO;
                                        row["CUENTA ORIGEN"] = CUENTA_ORIGEN;
                                        row["CUENTA DESTINO"] = Renglon["CUENTA DESTINO"].ToString();
                                        row["IMPORTE"] = String.Format("{0:00000000000000}", Convert.ToDouble(Importe));
                                        row["ORDEN"] = Renglon["ORDEN"].ToString();
                                        row["CONCEPTO"] = Concepto;
                                        Cant = 30 - row["CONCEPTO"].ToString().Length;
                                        for (int I = 0; I < Cant; I++)
                                        {
                                            row["CONCEPTO"] = row["CONCEPTO"].ToString() + " ";
                                        }
                                        row["TM"] = Renglon["TM"].ToString();
                                        row["RFC MUNICIPIO"] = Renglon["RFC MUNICIPIO"].ToString();
                                        row["CEROS"] = Renglon["CEROS"].ToString();
                                        row["EMAIL"] = Renglon["EMAIL"].ToString();
                                        row["FECHA"] = Renglon["FECHA"].ToString();
                                        row["BENEFICIARIO"] = Renglon["BENEFICIARIO"].ToString();
                                        row["PESTANIA"] = Renglon["PESTANIA"].ToString();
                                        Dt_Reporte_Agrepado.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Reporte_Agrepado.AcceptChanges();
                                    }
                                }
                            }
                            if (Dt_Reporte_Agrepado.Rows.Count > 0)
                            {
                                foreach (DataRow Fil in Dt_Solicitudes_Datos.Rows)
                                {
                                    Dt_Consulta = new DataTable();
                                    Rs_Modificar_Ope_con_Solicitud.P_No_Deuda = Fil["No_Solicitud"].ToString();
                                    //Rs_Modificar_Ope_con_Solicitud.P_Transferencia = Txt_Orden.Text.Trim();
                                    // Concer la cuenta destino
                                    Dt_Consulta = Rs_Modificar_Ope_con_Solicitud.Consulta_Datos_transferencia();
                                    if (Dt_Consulta.Rows.Count > 0)
                                    {
                                        Rs_Modificar_Ope_con_Solicitud.P_Cuenta_Transferencia = Dt_Consulta.Rows[0]["CUENTA_BANCO_PAGO"].ToString();
                                    }
                                    Rs_Modificar_Ope_con_Solicitud.P_Usuario_Autorizo  = Cls_Sessiones.Nombre_Empleado;
                                    Rs_Modificar_Ope_con_Solicitud.Alta_Transferencia();
                                }
                               // Orden = Txt_Orden.Text;
                                Generar_Archivo_Banorte(Dt_Reporte_Agrepado,"");// Txt_Orden.Text);
                                Inicializa_Controles();
                            }
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #region (Layout Banorte)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Archivo_Banorte
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Archivo_Banorte( DataTable Dt_Datos, String Orden)
    {
        StringBuilder Archivo_Banorte_BB;//Variable que almacena todos registros de banorte.
        StringBuilder Archivo_Banorte_BP;//Variable que almacena todos registros de banorte.
        try
        {
            Dt_Datos.DefaultView.RowFilter = "PESTANIA='BB'";
            if (Dt_Datos.DefaultView.ToTable().Rows.Count > 0)
            {
                Archivo_Banorte_BB = Obtener_Encabezado_Archivo_Banorte_BB(Dt_Datos);
                Guardar_Archivo(Archivo_Banorte_BB, "LAYOUT_BANORTE", Orden + "_BB"+DateTime.Now.ToString("dd-MMM-yyyy").ToUpper(), "1");
            }
            Dt_Datos.DefaultView.RowFilter = "PESTANIA='BP'";
            if (Dt_Datos.DefaultView.ToTable().Rows.Count > 0)
            {
                Archivo_Banorte_BP = Obtener_Encabezado_Archivo_Banorte_BP(Dt_Datos);
                Guardar_Archivo(Archivo_Banorte_BP, "../Contabilidad/LAYOUT_BANORTE", Orden + "_BP"+DateTime.Now.ToString("dd-MMM-yyyy").ToUpper(), "2");
            }
            //Inicializa_Controles();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el archivo de dispersion a banorte. Error: [" + Ex.Message + "");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Encabezado_Archivo_Banorte
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private StringBuilder Obtener_Encabezado_Archivo_Banorte_BB( DataTable Dt_Datos)
    {
        StringBuilder Cadena_Padre = null;
        String Fecha_Proceso = String.Empty;
        String Orden = String.Empty;
        String Tipo = String.Empty;
        StringBuilder Header = new StringBuilder();

        try
        {
            Fecha_Proceso = String.Format("{0:yyyyMMdd}",DateTime.Now);
            Orden = "";//Txt_Orden.Text.ToString();
            if (Orden.Substring(0) == "p" || Orden.Substring(0) == "P")
            {
                Tipo="Bte - Psrvc";
            }
            else
            {
                if (Orden.Substring(0) == "G" || Orden.Substring(0) == "g")
                {
                    Tipo = "Bte - Gxpagar";
                }
                else
                {
                    if (Orden.Substring(0) == "r" || Orden.Substring(0) == "R")
                    {
                        Tipo = "Bte - Rgasto";
                    }
                    else
                    {
                        if (Orden.Substring(0) == "f" || Orden.Substring(0) == "F")
                        {
                            Tipo = "Bte - Ffijo";
                        }
                        else
                        {
                            Tipo = "Bte - Pprov";
                        }
                    }
                }
            }
            Header.Append(Fecha_Proceso);
            Header.Append(" ");
            Header.Append(Orden);
            Header.Append(" ");
            Header.Append(Tipo);
            Header.Append("\r\n");
            Cadena_Padre = new StringBuilder(Header.ToString() + Obtener_Detalle_Archivo_Banorte_BB(Dt_Datos));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el encabezado del archivo de BANORTE. Error: [" + Ex.Message + "]");
        }
        return Cadena_Padre;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Detalle_Archivo_Banorte
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private StringBuilder Obtener_Detalle_Archivo_Banorte_BB(DataTable Dt_Datos)
    {
        StringBuilder Registros = new StringBuilder();//Registros que contendra el archivo .
        StringBuilder Cadena_BB = null;
        try
        {
            Dt_Datos.DefaultView.RowFilter = "PESTANIA ='BB'";
            if (Dt_Datos.DefaultView.ToTable().Rows.Count > 0)
                {
                    foreach (DataRow Proveedor in Dt_Datos.DefaultView.ToTable().Rows)
                    {
                        Registros.Append(Proveedor["TIPO"].ToString());
                        Registros.Append(Proveedor["ID"].ToString());
                        Registros.Append("\r\n");
                        Registros.Append(Proveedor["CUENTA ORIGEN"].ToString());
                        Registros.Append(Proveedor["CUENTA DESTINO"].ToString());
                        Registros.Append(Proveedor["IMPORTE"].ToString());
                        Registros.Append(Proveedor["ORDEN"].ToString());
                        Registros.Append(Proveedor["CONCEPTO"].ToString());
                        Registros.Append("\r\n");
                        Registros.Append(Proveedor["TM"].ToString());
                        Registros.Append(Proveedor["RFC MUNICIPIO"].ToString());
                        Registros.Append(Proveedor["CEROS"].ToString());
                        Registros.Append(Proveedor["EMAIL"].ToString());
                        Registros.Append("\r\n");
                        Registros.Append(Proveedor["FECHA"].ToString());
                        Registros.Append(Proveedor["BENEFICIARIO"].ToString());
                        Registros.Append("\r\n");
                        Registros.Append("\r\n");
                    }
                }
            Cadena_BB = new StringBuilder(Registros.ToString());
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el encabezado del archivo de BANORTE. Error: [" + Ex.Message + "]");
        }
        return Cadena_BB;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Encabezado_Archivo_Banorte_BP
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private StringBuilder Obtener_Encabezado_Archivo_Banorte_BP(DataTable Dt_Datos)
    {
        StringBuilder Cadena_Padre = null;
        String Fecha_Proceso = String.Empty;
        String Orden = String.Empty;
        String Tipo = String.Empty;
        StringBuilder Header = new StringBuilder();

        try
        {
            Fecha_Proceso = String.Format("{0:yyyyMMdd}", DateTime.Now);
            Orden = "";//Txt_Orden.Text.ToString();
            if (Orden.Substring(0) == "p" || Orden.Substring(0) == "P")
            {
                Tipo = "Bte - Psrvc";
            }
            else
            {
                if (Orden.Substring(0) == "G" || Orden.Substring(0) == "g")
                {
                    Tipo = "Bte - Gxpagar";
                }
                else
                {
                    if (Orden.Substring(0) == "r" || Orden.Substring(0) == "R")
                    {
                        Tipo = "Bte - Rgasto";
                    }
                    else
                    {
                        if (Orden.Substring(0) == "f" || Orden.Substring(0) == "F")
                        {
                            Tipo = "Bte - Ffijo";
                        }
                        else
                        {
                            Tipo = "Bte - Pprov";
                        }
                    }
                }
            }
            Header.Append(Fecha_Proceso);
            Header.Append(" ");
            Header.Append(Orden);
            Header.Append(" ");
            Header.Append(Tipo);
            Header.Append("\r\n");
            Cadena_Padre = new StringBuilder(Header.ToString() + Obtener_Detalle_Archivo_Banorte_BP(Dt_Datos));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el encabezado del archivo de BANORTE. Error: [" + Ex.Message + "]");
        }
        return Cadena_Padre;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Detalle_Archivo_Banorte_BP
    ///DESCRIPCIÓN: Resibe la informacion para poder generar el archivo del banco
    ///PARAMETROS:  
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  03/Julio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private StringBuilder Obtener_Detalle_Archivo_Banorte_BP(DataTable Dt_Datos)
    {
        StringBuilder Registros = new StringBuilder();//Registros que contendra el archivo .
        StringBuilder Cadena_BP = null;
        try
        {
            Dt_Datos.DefaultView.RowFilter = "PESTANIA ='BP'";
            if (Dt_Datos.DefaultView.ToTable().Rows.Count > 0)
            {
                foreach (DataRow Proveedor in Dt_Datos.DefaultView.ToTable().Rows)
                {
                    Registros.Append(Proveedor["TIPO"].ToString());
                    Registros.Append(Proveedor["ID"].ToString());
                    Registros.Append("\r\n");
                    Registros.Append(Proveedor["CUENTA ORIGEN"].ToString());
                    Registros.Append(Proveedor["CUENTA DESTINO"].ToString());
                    Registros.Append(Proveedor["IMPORTE"].ToString());
                    Registros.Append(Proveedor["ORDEN"].ToString());
                    Registros.Append(Proveedor["CONCEPTO"].ToString());
                    Registros.Append(Proveedor["TM"].ToString());
                    Registros.Append(Proveedor["RFC MUNICIPIO"].ToString());
                    Registros.Append(Proveedor["CEROS"].ToString());
                    Registros.Append(Proveedor["EMAIL"].ToString());
                    Registros.Append("\r\n");
                    Registros.Append(Proveedor["FECHA"].ToString());
                    Registros.Append(Proveedor["BENEFICIARIO"].ToString());
                    Registros.Append("\r\n");
                }
            }
            Cadena_BP = new StringBuilder(Registros.ToString());
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el encabezado del archivo de BANORTE. Error: [" + Ex.Message + "]");
        }
        return Cadena_BP;
    }
    private void Guardar_Archivo(StringBuilder Mensaje, String Nombre_Carpeta, String Nombre_Archivo,String Tipo)
    {
        String Ruta_Guardar_Archivo = "";//Variable que almacenará la ruta completa donde se guardara el log de la generacion de la nómina.
        String[] Archivos;

        try
        {
            //Obtenemos la ruta donde se guardara el log de layout.
            Ruta_Guardar_Archivo = Server.MapPath(Nombre_Carpeta);
            //Verificamos si el directorio del log existe, en caso contrario se crea. 
            if (!Directory.Exists(Ruta_Guardar_Archivo))
                Directory.CreateDirectory(Ruta_Guardar_Archivo);

            Archivos = Directory.GetFiles(Ruta_Guardar_Archivo);

            //Validamos que exista el archivo.
            if (Archivos.Length > 1)
            {
                foreach (String Archivo in Archivos)
                {
                    //Eliminamos el archivo.
                    File.Delete(Archivo);
                }
            }

            Escribir_Archivo(Ruta_Guardar_Archivo, "/" + Nombre_Archivo, ".txt", Mensaje);

            if (File.Exists(@Ruta_Guardar_Archivo + "/" + Nombre_Archivo + ".txt"))
            {
                if (Tipo == "1")
                {
                    Mostrar_Archivo(Nombre_Carpeta + "/" + Nombre_Archivo + ".txt");
                }
                else
                {
                    Mostrar_Archivo_2(Nombre_Carpeta + "/" + Nombre_Archivo + ".txt");
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al guardar el archivo de dispersion. Error: [" + Ex.Message + "]");
        }
    }
    public static void Escribir_Archivo(String Ruta, String Nombre_Archivo, String Extencion, StringBuilder Cadena)
    {
        StreamWriter Escribir_Archivo = null;//Escritor, variable encargada de escribir el archivo que almacenará el historial de la nómina generada.

        try
        {
            Escribir_Archivo = new StreamWriter(@"" + (Ruta + Nombre_Archivo + Extencion), true, Encoding.UTF8);
            Escribir_Archivo.WriteLine(Cadena.ToString());
            Escribir_Archivo.Close();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al escribir el Archivo " + Nombre_Archivo + ". Error: [" + Ex.Message + "]");
        }
    }
    //////private void Mostrar_Archivo(String URL)
    //////{
    //////    String Pagina = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL;
    //////    try
    //////    {

    //////        ScriptManager.RegisterStartupScript(this, this.GetType(), "B1",
    //////            "window.open('" + Pagina + "', 'A','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1,height=1');", true);
    //////        //ScriptManager.RegisterStartupScript(this, this.GetType(), "B", "window.location='Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL + "';", true);
    //////       //Response.Redirect("Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL, false);
    //////    }
    //////    catch (Exception Ex)
    //////    {
    //////        throw new Exception("Error al mostrar el archivo de dispersion generado generado. Error: [" + Ex.Message + "]");
    //////    }
    //////}
    //////private void Mostrar_Archivo_2(String URL)
    //////{
    //////    try
    //////    {
    //////        String Pagina = "../Nomina/Frm_Mostrar_Archivos.aspx?Documento="+URL;
    //////        ScriptManager.RegisterStartupScript(this, this.GetType(), "B2",
    //////            "window.open('" + Pagina + "', 'B','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1,height=1');", true);
    //////        //ScriptManager.RegisterStartupScript(this, this.GetType(), "A", "window.location='../Nomina/Frm_Mostrar_Archivos.aspx?Documento=" + URL + "';", true);
    //////       // Response.Redirect("../nomina/Frm_Mostrar_Archivos.aspx?Documento=" + URL, false);
    //////    }
    //////    catch (Exception Ex)
    //////    {
    //////        throw new Exception("Error al mostrar el archivo de dispersion generado generado. Error: [" + Ex.Message + "]");
    //////    }
    //////}

    private void Mostrar_Archivo(String URL)
    {
        String Pagina = "../../Reporte/";
        try
        {
            Pagina += URL;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "B1",
                "window.open('" + Pagina + "', 'A','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "B", "window.location='Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL + "';", true);
            //Response.Redirect("Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL, false);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el archivo de dispersion generado generado. Error: [" + Ex.Message + "]");
        }
    }

    private void Mostrar_Archivo_2(String URL)
    {
        String Pagina = "../../Reporte/";
        try
        {
            Pagina += URL;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "B2",
                "window.open('" + Pagina + "', 'B','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "A", "window.location='../Nomina/Frm_Mostrar_Archivos.aspx?Documento=" + URL + "';", true);
            // Response.Redirect("../nomina/Frm_Mostrar_Archivos.aspx?Documento=" + URL, false);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el archivo de dispersion generado generado. Error: [" + Ex.Message + "]");
        }
    }

    #endregion
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Modificar_Numero_Folio
    ///DESCRIPCIÓN: Resibe la informacion para poder modificar el folio Actual
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  31/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Numero_Folio()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Modificar_Folio_Actual = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Int32 Folio = 0;
        try
        {
            Folio = Convert.ToInt32(Txt_No_Cheque.Text);
            Folio++;
            Rs_Modificar_Folio_Actual.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            Rs_Modificar_Folio_Actual.P_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Folio_Actual.P_Folio_Actual = "" + Folio;
            Rs_Modificar_Folio_Actual.Modificar_Folio_Actual();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
            throw new Exception(ex.Message, ex);
        }
    }
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                if (Txt_No_Pago.Text == "")
                {
                    Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                    Llenar_Combos_Generales();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pagos", "alert('EL Cheque solo se puede modificar');", true);
                }

            }
            else
            {
                //Valida los datos ingresados por el usuario.
                if (Validaciones(true))
                {
                    Alta_Cheque();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Cheques", "alert('El alta del Pago fue exitoso');", true);
                    Inicializa_Controles();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Metodo que permite modificar la reserva
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/Noviembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
            }
            else
            {
                if (Validar_Modificacion(true))
                {
                    Modificar_Cheque(); //Modifica los datos de la Cuenta Contable con los datos proporcionados por el usuario
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Text = "";
            Img_Error.Visible = false;

            Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            Limpia_Controles();//Limpia los controles de la forma
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Comentar_Click(object sender, EventArgs e)
    {
         Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos= new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        try
        {
            DataTable Dt_Reporte = new DataTable();

            if (Txt_Comentario.Text != "")
            {
                Rechazar_Solicitud(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString());
                Inicializa_Controles();
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Recepcion_Documentos_Inicio
    ///DESCRIPCIÓN          : Inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Diciembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Recepcion_Documentos_Inicio()
    {
        try
        {
            Limpia_Controles();
            Llenar_Grid_Solicitudes_Pendientes();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO  : 21-Febrero-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    private void Abrir_Ventana(String Nombre_Archivo)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
        try
        {
            Pagina = Pagina + Nombre_Archivo;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
        }
    }
     ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Rechazar_Solicitud
        /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
        ///               proporcionados por el usuario
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Rechazar_Solicitud(String No_Solicitud_Pago,String Comentario, String Empleado_ID, String Nombre_Empleado)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio(); //Variable de conexión hacia la capa de Negocios
            //Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
            DataTable Dt_Detalles = new DataTable();
            DataTable Dt_Datos_Polizas = new DataTable();
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            try
            {    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Deuda = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "CANCELADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanza  = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Finanzas  = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Autorizo = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Cambiar_Estatus_Solicitud(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados              
                //Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);
            }
            catch (Exception ex)
            {
                throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
            }
        }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 24/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cancela_Solicitud_Pago(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado)
    {
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Honorarios = new DataTable();
        DataTable Dt_Arrendamientos = new DataTable();
        DataTable Dt_Hon_Asimilables = new DataTable();
        DataRow Registro_Honorarios;
        DataRow Registro_Arrendamiento;
        DataRow Registro_Hon_Asimila;
        Double Total_ISR_Honorarios = 0;
        Double Total_Cedular_Honorarios = 0;
        Double Total_ISR_Arrendamientos = 0;
        Double Total_Cedular_Arrendamientos = 0;
        Double Total_Hono_asimi = 0;
        String Cuenta_ISR = "0";
        String Cuenta_Cedular = "0";
        Int32 partida = 0;
        DataRow row;
        //int Partida = 0;
        try
        {
            //Agregar las partidas de los impuestos de isr y de retencion de iva si tiene honorarios la solicitud de pago
            //se crean las columnas de los datatable de honorarios y de arrendamientos
            if (Dt_Honorarios.Rows.Count == 0)
            {
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Double));
                Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Double));
            }
            if (Dt_Arrendamientos.Rows.Count == 0)
            {
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Double));
                Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Double));
            }
            if (Dt_Hon_Asimilables.Rows.Count == 0)
            {
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Double));
                Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Double));
            }
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
            String Tipo_Poliza_ID = "";
            foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
            {
                if (!String.IsNullOrEmpty(Registro["Monto_Partida"].ToString()))
                {
                    partida = partida + 1;
                    Txt_Monto_Solicitud.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                    if (!String.IsNullOrEmpty(Registro["Empleado_Cuenta"].ToString()))
                    {
                        Txt_Cuenta_Contable_ID_Empleado.Value = Registro["Empleado_Cuenta"].ToString();
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Acreedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "A")
                        {
                            Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Acreedor"].ToString();
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Contratista"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "C")
                            {
                                Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Contratista"].ToString();
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Deudor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "D")
                                {
                                    Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Deudor"].ToString();
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Judicial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "J")
                                    {
                                        Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Judicial"].ToString();
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Nomina"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "N")
                                        {
                                            Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Nomina"].ToString();
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Predial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "Z")
                                            {
                                                Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Predial"].ToString();
                                            }
                                            else
                                            {
                                                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Proveedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                                                {
                                                    Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Proveedor"].ToString();
                                                }
                                                else
                                                {
                                                    if (Registro["Tipo_Solicitud_Pago_ID"].ToString() == "00008" && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                                                    {
                                                        Rs_Cuentas_Proveedor.P_Cuenta = "211200001";
                                                        Dt_C_Proveedor = Rs_Cuentas_Proveedor.Consulta_Existencia_Cuenta_Contable();
                                                        if (Dt_C_Proveedor.Rows.Count > 0)
                                                        {
                                                            Txt_Cuenta_Contable_ID_Proveedor.Value = Dt_C_Proveedor.Rows[0]["Cuenta_Contable_ID"].ToString();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!String.IsNullOrEmpty(Registro["Tipo_Solicitud_Pago_ID"].ToString()))
                    {
                        Tipo_Poliza_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                    }
                    //Txt_Monto_Solicitud.Value = Registro["Monto"].ToString();
                    //Txt_Concepto_Solicitud.Value = Registro["Concepto"].ToString();
                    Txt_Cuenta_Contable_reserva.Value = Registro["Cuenta_Contable_Reserva"].ToString();
                    Txt_No_Reserva.Value = Registro["NO_RESERVA"].ToString();
                    if (Tipo_Poliza_ID != "00001" && Tipo_Poliza_ID != "00003")
                    {
                        //se consultan los detalles de la solicitud de pago
                        Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                        Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                        if (Dt_Datos_Solicitud.Rows.Count > 0)
                        {
                            //se recorre el dataset de los detalles para dividir los detalles por tipo de operacion 
                            foreach (DataRow fila in Dt_Datos_Solicitud.Rows)
                            {
                                if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "HONORARIOS")
                                {
                                    Registro_Honorarios = Dt_Honorarios.NewRow();
                                    Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                                    Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                                    Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                                    Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                                    Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                                    Dt_Honorarios.Rows.Add(Registro_Honorarios); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Honorarios.AcceptChanges();
                                }
                                if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "ARRENDAMIENTO")
                                {
                                    Registro_Arrendamiento = Dt_Arrendamientos.NewRow();
                                    Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                                    Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                                    Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                                    Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                                    Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                                    Dt_Arrendamientos.Rows.Add(Registro_Arrendamiento); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Arrendamientos.AcceptChanges();
                                }
                                if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "HON. ASIMILABLE")
                                {
                                    Registro_Hon_Asimila = Dt_Hon_Asimilables.NewRow();
                                    Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                                    Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                                    Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                                    Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                                    Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                                    Dt_Hon_Asimilables.Rows.Add(Registro_Hon_Asimila); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Hon_Asimilables.AcceptChanges();
                                }
                            }
                            Total_ISR_Honorarios = 0;
                            Total_Cedular_Honorarios = 0;
                            if (Dt_Honorarios.Rows.Count > 0)
                            {
                                foreach (DataRow fila in Dt_Honorarios.Rows)
                                {
                                    if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                                    {
                                        Total_ISR_Honorarios = Total_ISR_Honorarios + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                                    }
                                    if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                                    {
                                        Total_Cedular_Honorarios = Total_Cedular_Honorarios + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                                    }
                                }
                            }
                            Total_ISR_Arrendamientos = 0;
                            Total_Cedular_Arrendamientos = 0;
                            if (Dt_Arrendamientos.Rows.Count > 0)
                            {
                                foreach (DataRow fila in Dt_Arrendamientos.Rows)
                                {
                                    if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                                    {
                                        Total_ISR_Arrendamientos = Total_ISR_Arrendamientos + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                                    }
                                    if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                                    {
                                        Total_Cedular_Arrendamientos = Total_Cedular_Arrendamientos + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                                    }
                                }
                            }
                            Total_Hono_asimi = 0;
                            if (Dt_Hon_Asimilables.Rows.Count > 0)
                            {
                                foreach (DataRow fila in Dt_Hon_Asimilables.Rows)
                                {
                                    if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                                    {
                                        Total_Hono_asimi = Total_Hono_asimi + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                                    }
                                }
                            }
                        }
                        //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                        if (Dt_Partidas_Polizas.Rows.Count == 0)
                        {
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                        }
                        row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                        //Agrega el cargo del registro de la póliza
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_reserva.Value;
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Registro["Monto_Partida"].ToString()) + Convert.ToDouble(Registro["Monto_ISR"].ToString()) + Convert.ToDouble(Registro["Monto_Cedular"].ToString());
                        Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Partidas_Polizas.AcceptChanges();
                    }
                }
            }
            if (Tipo_Poliza_ID != "00001" && Tipo_Poliza_ID != "00003")
            {
                //se consultan los detalles de la solicitud de pago
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                if (Dt_Datos_Solicitud.Rows.Count > 0)
                {
                    Total_ISR_Honorarios = 0;
                    Total_Cedular_Honorarios = 0;
                    if (Dt_Honorarios.Rows.Count > 0)
                    {
                        foreach (DataRow fila in Dt_Honorarios.Rows)
                        {
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                            {
                                Total_ISR_Honorarios = Total_ISR_Honorarios + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                            }
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                            {
                                Total_Cedular_Honorarios = Total_Cedular_Honorarios + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                            }
                        }
                    }
                    Total_ISR_Arrendamientos = 0;
                    Total_Cedular_Arrendamientos = 0;
                    if (Dt_Arrendamientos.Rows.Count > 0)
                    {
                        foreach (DataRow fila in Dt_Arrendamientos.Rows)
                        {
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                            {
                                Total_ISR_Arrendamientos = Total_ISR_Arrendamientos + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                            }
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                            {
                                Total_Cedular_Arrendamientos = Total_Cedular_Arrendamientos + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                            }
                        }
                    }
                    row = Dt_Partidas_Polizas.NewRow();
                    partida = partida + 1;
                    //Agrega el abono del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Proveedor.Value.ToString()))
                    {
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Proveedor.Value;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Empleado.Value.ToString()))
                        {
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Empleado.Value;
                        }
                    }
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Monto_Solicitud.Value.ToString()) - (Total_Cedular_Arrendamientos + Total_Cedular_Honorarios + Total_ISR_Arrendamientos + Total_ISR_Honorarios);
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                    // se recorre el datatable de honorarios para realizar la sumatoria de los impuestos en las variables correspondientes 
                    //y asi obtener el total de los impuestos de cada tipo
                    if (Dt_Honorarios.Rows.Count > 0)
                    {
                        if (Total_ISR_Honorarios > 0)
                        {
                            // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                            Rs_Cuentas_Impuestos.P_Cuenta = "211700006";
                            Dt_Cuentas = Rs_Cuentas_Impuestos.Consulta_Existencia_Cuenta_Contable();
                            if (Dt_Cuentas.Rows.Count > 0)
                            {
                                Cuenta_ISR = Dt_Cuentas.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                            }
                            partida = partida + 1;
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_ISR_Honorarios;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        if (Total_Cedular_Honorarios > 0)
                        {
                            // CUENTA IMPUESTO CEDULAR 1% HONORARIOS RET consultamos el id que tiene la cuenta en el sistema
                            Rs_Cuentas_Impuestos.P_Cuenta = "211700010";
                            Dt_Cuentas = Rs_Cuentas_Impuestos.Consulta_Existencia_Cuenta_Contable();
                            if (Dt_Cuentas.Rows.Count > 0)
                            {
                                Cuenta_Cedular = Dt_Cuentas.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                            }
                            partida = partida + 1;
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cedular;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Cedular_Honorarios;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                    // se recorre el datatable de honorarios para realizar la sumatoria de los impuestos en las variables correspondientes 
                    //y asi obtener el total de los impuestos de cada tipo
                    if (Dt_Arrendamientos.Rows.Count > 0)
                    {
                        Cuenta_Cedular = "";
                        Cuenta_ISR = "";
                        if (Total_ISR_Arrendamientos > 0)
                        {
                            // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                            Rs_Cuentas_Impuestos.P_Cuenta = "211700007";
                            Dt_Cuentas = Rs_Cuentas_Impuestos.Consulta_Existencia_Cuenta_Contable();
                            if (Dt_Cuentas.Rows.Count > 0)
                            {
                                Cuenta_ISR = Dt_Cuentas.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                            }
                            partida = partida + 1;
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_ISR_Arrendamientos;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        if (Total_Cedular_Arrendamientos > 0)
                        {
                            // CUENTA IMPUESTO CEDULAR 1% HONORARIOS RET consultamos el id que tiene la cuenta en el sistema
                            Rs_Cuentas_Impuestos.P_Cuenta = "211700009";
                            Dt_Cuentas = Rs_Cuentas_Impuestos.Consulta_Existencia_Cuenta_Contable();
                            if (Dt_Cuentas.Rows.Count > 0)
                            {
                                Cuenta_Cedular = Dt_Cuentas.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                            }
                            partida = partida + 1;
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cedular;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Cedular_Arrendamientos;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }

                //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(partida);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PRE-DOCUMENTADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Contabi = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Contabilidad = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Contabilidad = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Ejercido = "";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Convert.ToDouble(Txt_Monto_Solicitud.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
            }
            else
            {
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PRE-DOCUMENTADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago_Sin_Poliza();
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Cheque_Fondo_Revolvente
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 24/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Cheque_Fondo_Revolvente(String Banco_Otros, String Cuentas_Por_Pagar)
    {

        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        String Cheque = "";
        String Resultado = "";
        DataTable Dt_Banco_Cuenta_Contable = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos            
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
        Cn.Open();
        Trans = Cn.BeginTransaction();
        Cmmd.Connection = Trans.Connection;
        Cmmd.Transaction = Trans;
        try
        {
            Rs_Ope_Con_Cheques.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
            Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
            foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
            {
                Txt_Cuenta_Contable_ID_Banco.Value = Renglon[Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
            }
            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));


            //Agrega el abono del registro de la póliza
            DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Banco.Value;
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Txt_Monto.Text.ToString().Replace("$","").Replace(",",""));
            row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
            row["BENEFICIARIO_ID"] = "";
            row["TIPO_BENEFICIARIO"] = "";
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();

            //Agrega el cargo del registro de la póliza
            row = Dt_Partidas_Polizas.NewRow();
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_Proveedor.Value;
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Txt_Monto.Text.ToString().Replace("$", "").Replace(",", ""));
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
            row["BENEFICIARIO_ID"] = "";
            row["TIPO_BENEFICIARIO"] = "";
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();

            //Agrega el abono del registro de la póliza
            row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 3;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Banco_Otros;
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Txt_Monto.Text.ToString().Replace("$", "").Replace(",", ""));
            row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
            row["BENEFICIARIO_ID"] = "";
            row["TIPO_BENEFICIARIO"] = "";
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();

            //Agrega el cargo del registro de la póliza
            row = Dt_Partidas_Polizas.NewRow();
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 4;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuentas_Por_Pagar;
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Txt_Monto.Text.ToString().Replace("$", "").Replace(",", ""));
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Txt_Referencia_Pago.Text.ToString();
            row["BENEFICIARIO_ID"] = "";
            row["TIPO_BENEFICIARIO"] = "";
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();


            Rs_Solicitudes.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
            Rs_Solicitudes.P_No_Deuda = Txt_No_Solicitud.Text.Trim();
            Rs_Solicitudes.P_Comentario = Txt_Comentario_Pago.Text.Trim();
            Rs_Solicitudes.P_Importe = Convert.ToString(Convert.ToDecimal(Txt_Monto.Text.Trim().Replace("$", "").Replace(",", ""))*2);
            Rs_Solicitudes.P_No_Partidas = "4";
            Rs_Solicitudes.P_Estatus = Cmb_Estatus.SelectedItem.ToString();
            Rs_Solicitudes.P_Fecha_Pago = String.Format("{0:dd/MM/yy}", Convert.ToDateTime(Txt_Fecha_No_Pago.Text)).ToString();
            Rs_Solicitudes.P_Forma_Pago = Cmb_Tipo_Pago.SelectedItem.ToString();
            Rs_Solicitudes.P_Cuenta_Banco_Pago = Cmb_Cuenta_Banco.SelectedValue;
            Rs_Solicitudes.P_Referencia = Txt_Referencia_Pago.Text.ToString();
            Rs_Solicitudes.P_Cmmd = Cmmd;
            if (Txt_Beneficiario_Pago.Text.Trim().Substring(1, 1) == "-")
            {
                Rs_Solicitudes.P_Beneficiario_Pago = Txt_Beneficiario_Pago.Text.Trim().Substring(2);
            }
            else
            {
                Rs_Solicitudes.P_Beneficiario_Pago = Txt_Beneficiario_Pago.Text.Trim();
            }
            Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            Rs_Consultar_Folio.P_Cmmd = Cmmd;
            Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();
            //  obtendra el numero de folio de los cheques
            if (Dt_Consulta.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                    {
                        Txt_No_Cheque.Text = Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString();
                    }
                }
            }
            Rs_Solicitudes.P_No_Cheque = Txt_No_Cheque.Text.Trim();
            Rs_Solicitudes.P_Usuario_Autorizo = Cls_Sessiones.Nombre_Empleado;
            Cheque = Rs_Solicitudes.Alta_Cheque();
            Resultado = Cheque.Substring(6, 2);
            if (Resultado == "SI")
            {
                Cheque = Cheque.Substring(0, 5);
                Imprimir(Cheque, Convert.ToDecimal(Txt_Monto.Text.Trim().Replace("$", "").Replace(",", "")), Cmmd);
                // cambiamos el numero de folio de los cheques
                Modificar_Numero_Folio(Cmmd, null);
                Trans.Commit();
                Limpia_Controles();
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            }
            else
            {
                Trans.Rollback();
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para realizar el pago ";
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_CHEQUE " + ex.Message.ToString(), ex);
        }
    }
    protected void Btn_Cancelar_Click(object sender, EventArgs e)
    {
        try
        {
            Recepcion_Documentos_Inicio();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Banco_OnSelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  31/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Banco_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
            Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Cuenta_Banco, Dt_Consulta, "CUENTA", "BANCO_ID");
            Txt_No_Cheque.Text = "";
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Cuenta_Banco_OnSelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  10/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Cuenta_Banco_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        Int32 Folio_Actual = 0;
        Int32 Folio_Final = 0;
        Boolean Estado_Folio = false;
        try
        {
            if (Cmb_Banco.SelectedIndex > 0)
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;

                Txt_No_Cheque.Text = "";
                Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();

                //  obtendra el numero de folio de los cheques
                if (Dt_Consulta.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                            Folio_Actual = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()));

                        else
                            Estado_Folio = true;

                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()))
                            Folio_Final = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()));

                        else
                            Estado_Folio = true;
                    }
                }
                //  si contiene informacion
                if (Estado_Folio == false)
                {
                    if (Folio_Actual <= Folio_Final)
                        Txt_No_Cheque.Text = "" + Folio_Actual;

                    //  si el folio se pasa del final manda un mensaje
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Se terminaron los folios del Talonario de cheques actual por favor ingrese nuevos folios";
                    }
                }
                //  no tiene asignado los numeros de folio de los chuques
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Por favor defina el rango de los folio del Talonario de cheques para poder utilizarlos en esta ventana";
                }
            }
            else
            {
                Txt_No_Cheque.Text = "";
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Solicitud_Pagos_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
    ///PARAMETROS           :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 22/mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Solicitud_Pagos_SelectedIndexChanged(object sender, EventArgs e)
    {
       //Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
       //Cls_Ope_Con_Cheques_Negocio Solicitud_Proveedor = new Cls_Ope_Con_Cheques_Negocio();
       Cls_Cat_Con_Proveedores_Negocio Datos_Proveedor = new Cls_Cat_Con_Proveedores_Negocio();
       Cls_Ope_Con_Cheques_Negocio Datos_Empleado = new Cls_Ope_Con_Cheques_Negocio();
                DataTable Dt_Detalles = new DataTable();
                DataTable Dt_Documentos = new DataTable();
                DataTable Dt_Proveedor = new DataTable();
                DataTable Dt_Datos_Proveedor = new DataTable();
                try
                {
                    GridView grid_proveedor = sender as GridView;
                    if (grid_proveedor.SelectedIndex > (-1))
                    {
                        if (grid_proveedor.SelectedRow.Cells[6].Text.Trim() == "PROVEEDOR")
                        {
                            Datos_Proveedor.P_Proveedor_ID = grid_proveedor.SelectedRow.Cells[1].Text.Trim();
                            Dt_Datos_Proveedor = Datos_Proveedor.Consulta_Datos_Proveedores();
                            if (Dt_Datos_Proveedor.Rows.Count > 0)
                            {
                                Txt_Banco_Proveedor.Text = Dt_Datos_Proveedor.Rows[0]["BANCO_PROVEEDOR"].ToString().Trim();
                                Txt_Clabe.Text = Dt_Datos_Proveedor.Rows[0]["CLABE"].ToString().Trim();
                                Txt_Numero_Cuenta.Text = Dt_Datos_Proveedor.Rows[0]["CUENTA"].ToString().Trim();
                                Txt_Padron_ID.Text = Dt_Datos_Proveedor.Rows[0]["PROVEEDOR_ID"].ToString().Trim();
                            }
                        }
                        else
                        {
                            if (grid_proveedor.SelectedRow.Cells[6].Text.Trim() == "EMPLEADO")
                            {
                                Datos_Empleado.P_Empleado_ID_Jefe= grid_proveedor.SelectedRow.Cells[1].Text.Trim();
                                Dt_Datos_Proveedor = Datos_Empleado.Consultar_Datos_Empleado();
                                if (Dt_Datos_Proveedor.Rows.Count > 0)
                                {
                                    Txt_Banco_Proveedor.Text = Dt_Datos_Proveedor.Rows[0]["BANCO"].ToString();//Dt_Datos_Proveedor.Rows[0]["BANCO_PROVEEDOR"].ToString().Trim();
                                    Txt_Clabe.Text = Dt_Datos_Proveedor.Rows[0]["NO_TARJETA"].ToString().Trim();
                                    Txt_Numero_Cuenta.Text = Dt_Datos_Proveedor.Rows[0]["NO_CUENTA_BANCARIA"].ToString().Trim();
                                    Txt_Padron_ID.Text = Dt_Datos_Proveedor.Rows[0]["EMPLEADO_ID"].ToString().Trim();
                                }
                            }
                        }
                        
                                Mpe_Proveedor.Show();
                    }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Solicitud_Detalles_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
    ///PARAMETROS           :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 22/mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Solicitud_Detalles_SelectedIndexChanged(object sender, EventArgs e)
    {
        ////Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
        ////Cls_Ope_Con_Cheques_Negocio Solicitud_Proveedor = new Cls_Ope_Con_Cheques_Negocio();
        ////Cls_Cat_Con_Proveedores_Negocio Datos_Proveedor = new Cls_Cat_Con_Proveedores_Negocio();
        ////DataTable Dt_Detalles = new DataTable();
        ////DataTable Dt_Documentos = new DataTable();
        ////DataTable Dt_Proveedor = new DataTable();
        ////DataTable Dt_Datos_Proveedor = new DataTable();
        ////try
        ////{
        ////    GridView Grid_Solicitudes_Datos = sender as GridView;
        ////    if (Grid_Solicitudes_Datos.SelectedIndex > (-1))
        ////    {
        ////        Solicitud_Negocio.P_No_Solicitud_Pago = Grid_Solicitudes_Datos.SelectedRow.Cells[1].Text.Trim();
        ////        Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
        ////        Dt_Documentos = Solicitud_Negocio.Consulta_Documentos();
        ////        if (Dt_Detalles != null)
        ////        {
        ////            if (Dt_Detalles.Rows.Count > 0)
        ////            {

        ////                Txt_No_Pago_Det.Text = Dt_Detalles.Rows[0]["NO_SOLICITUD_PAGO"].ToString().Trim();
        ////                Txt_No_Reserva_Det.Text = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString().Trim();
        ////                Txt_Concepto_Reserva_Det.Text = Dt_Detalles.Rows[0]["CONCEPTO_RESERVA"].ToString().Trim();
        ////                Txt_Beneficiario_Det.Text = Dt_Detalles.Rows[0]["BENEFICIARIO"].ToString().Trim();
        ////                Txt_Fecha_Solicitud_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_SOLICITUD"]);
        ////                Txt_Monto_Solicitud_Det.Text = String.Format("{0:c}", Dt_Detalles.Rows[0]["MONTO"]);
        ////                Txt_No_poliza_Det.Text = Dt_Detalles.Rows[0]["NO_POLIZA"].ToString().Trim();
        ////                if (String.IsNullOrEmpty(Txt_No_poliza_Det.Text))
        ////                {
        ////                    Tr_Poliza.Style.Add("Display", "none");
        ////                }
        ////                else
        ////                {
        ////                    Tr_Poliza.Style.Add("Display", "block");
        ////                }
        ////                Grid_Documentos.Columns[3].Visible = true;
        ////                Grid_Documentos.Columns[4].Visible = true;
        ////                Grid_Documentos.DataSource = Dt_Documentos;
        ////                Grid_Documentos.DataBind();
        ////                Grid_Documentos.Columns[3].Visible = false;
        ////                Grid_Documentos.Columns[4].Visible = false;
        ////                Mpe_Detalles.Show();
        ////            }
        ////        }
        ////    }
        ////}
        ////catch (Exception Ex)
        ////{
        ////    throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
        ////}
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : RBL_Orden_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
    ///PARAMETROS           :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 22/mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void RBL_Orden_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Llenar_Grid_Solicitudes_Pendientes();
            if (Cmb_Forma_Pago.SelectedItem.Text == "Cheque")
            {
                //Btn_transferir.Enabled = false;
                //Txt_Orden.Enabled = false;
            }
            else
            {
                //Btn_transferir.Enabled = true;
                //Txt_Orden.Enabled = true;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Documentos_RowDataBound
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  05-Marzo-2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        HyperLink Hyp_Lnk_Ruta;
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                if (!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) && e.Row.Cells[4].Text.Trim() != "&nbsp;")
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[4].Text.Trim();
                    Hyp_Lnk_Ruta.Enabled = true;
                }
                else
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "";
                    Hyp_Lnk_Ruta.Enabled = false;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #region (Cheque impreso)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Convertir_Cantidad_Letras
    ///DESCRIPCIÓN: Convierte una cantidad en letra
    ///PARAMETROS: 
    ///CREO:        
    ///FECHA_CREO:  15-Marzo-2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected String Convertir_Cantidad_Letras(Double Cantidad_Numero)
    {
        Numalet Obj_Numale = new Numalet();
        String Cantidad_Letra = String.Empty;

        try
        {
            Obj_Numale.MascaraSalidaDecimal = "00/100 M.N.";
            Obj_Numale.SeparadorDecimalSalida = "pesos";
            Obj_Numale.ApocoparUnoParteEntera = true;
            Cantidad_Letra = Obj_Numale.ToCustomCardinal(Cantidad_Numero).Trim().ToUpper();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al convertir la cantidad a letras. Error:[" + Ex.Message + "]");
        }
        return Cantidad_Letra;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String Cheque, Double Monto)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        DataTable Dt_Temporal = new DataTable();
        String Letra = "";
        try
        {
            Cls_Ope_Con_Cheques_Negocio Datos_Cheque = new Cls_Ope_Con_Cheques_Negocio();
            Ds_Reporte = new DataSet();
            Datos_Cheque.P_No_Cheque = Cheque;
            Dt_Pagos = Datos_Cheque.Consulta_Pago_Cheque();
            Letra = Convertir_Cantidad_Letras(Monto);
            if (Dt_Temporal.Rows.Count <= 0)
            {
                Dt_Temporal.Columns.Add("NO_PAGO", typeof(System.String));
                Dt_Temporal.Columns.Add("MONTO", typeof(System.Double));
                Dt_Temporal.Columns.Add("BENEFICIARIO", typeof(System.String));
                Dt_Temporal.Columns.Add("MONTO_LETRA", typeof(System.String));
                Dt_Temporal.Columns.Add("NO_CHEQUE", typeof(System.String));
                Dt_Temporal.TableName = "Dt_Cheque";
            }
            foreach (DataRow Registro in Dt_Pagos.Rows)
            {
                DataRow row = Dt_Temporal.NewRow(); //Crea un nuevo registro a la tabla
                row["NO_PAGO"] = Registro["NO_PAGO"].ToString();
                row["MONTO"] = Monto;
                if (Registro["BENEFICIARIO_PAGO"].ToString().Contains('-'))
                {
                    row["BENEFICIARIO"]=Registro["BENEFICIARIO_PAGO"].ToString().Substring(2);
                }
                else
                {
                    row["BENEFICIARIO"] = Registro["BENEFICIARIO_PAGO"].ToString();
                }
                row["MONTO_LETRA"] = Letra;
                row["NO_CHEQUE"] = Registro["NO_CHEQUE"].ToString();
                Dt_Temporal.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Temporal.AcceptChanges();
            }


            if (Dt_Pagos.Rows.Count > 0)
            {
                Ds_Reporte.Tables.Add(Dt_Temporal.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Cheque.rpt", "Reporte_Cheques" + Cheque, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            Lbl_Mensaje_Error.Visible = true;
        }

    }
    #endregion
    #region Metodos Reportes
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Nombre_PDF)
    {
        try
        {

            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Contabilidad/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            String Ruta = "../../Reporte/" + Nombre_PDF;
            Mostrar_Reporte(Nombre_PDF, "PDF");
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }


    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";

        try
        {
            Pagina = Pagina + Nombre_Reporte_Generar;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pagos", "alert('Imprimiendo cheque No. " + Txt_No_Cheque.Text.Trim() + ".');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion
}