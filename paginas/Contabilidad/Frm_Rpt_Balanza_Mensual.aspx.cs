﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Niveles.Negocio;

public partial class paginas_Contabilidad_Frm_Rpt_Balanza_Mensual : System.Web.UI.Page
{
    #region (Load/Init)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Refresca la session del usuario lagueado al sistema.
                Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
                //Valida que exista algun usuario logueado al sistema.
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                }
                else
                {
                    Lbl_Haber.Text = "&nbsp;";
                    Lbl_Debe.Text = "&nbsp;";
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion
    #region (Metodos)
        #region (Métodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION : Inicializa los controles de la forma para prepararla para el
            ///               reporte
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Octubre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                try
                {
                    Cargar_Cuentas_Contables_Combos("",""); //Carga los cmbos con la descripci{on de las cuentas contables que se tienen dadas de alta0
                    Limpia_Controles(); //limpia los campos de la forma
                    Llena_Combo_Dependencias();
                    Llena_Combo_Niveles();
                }
                catch (Exception ex)
                {
                    throw new Exception("Inicializa_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION : Limpia los controles que se encuentran en la forma
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Octubre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Controles()
            {
                try
                {  
                    Cmb_Anio.SelectedIndex = -1;
                    Cmb_Meses.SelectedIndex = -1;
                    Cmb_Cuenta_Inicial.SelectedIndex = -1;
                    Cmb_Cuenta_Contable_Final.SelectedIndex = -1;
                    Chk_Movimientos_Saldo_No_Cero.Checked = false;
                    Txt_Cuenta_Inicial.Text = "";
                    Txt_Cuenta_Final.Text = "";
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Validar_Datos_Reporte
            /// DESCRIPCION : Validar que se hallan proporcionado todos los datos para la
            ///               generación del reporte
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 18-Octubre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Validar_Datos_Reporte()
            {
                Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";

                if (Cmb_Tipo_Balance.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El Tipo de Balance. <br />";
                    Datos_Validos = false;
                }
                if (Cmb_Meses.SelectedIndex <= 0)
                {
                    //verificar el tipo de reporte
                    if (Cmb_Tipo_Balance.SelectedIndex != 2)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El mes a consultar. <br />";
                        Datos_Validos = false;
                    }
                }
                if (Cmb_Anio.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El Año a consultar. <br />";
                    Datos_Validos = false;
                }
                return Datos_Validos;
            }

            /// *************************************************************************************
            /// NOMBRE:              Mostrar_Reporte
            /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
            /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
            ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
            /// USUARIO CREO:        Juan Alberto Hernández Negrete.
            /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
            /// USUARIO MODIFICO:    Salvador Hernández Ramírez
            /// FECHA MODIFICO:      16-Mayo-2011
            /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
            /// *************************************************************************************
            protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
            {
                String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

                try
                {
                    if (Formato == "PDF")
                    {
                        Pagina = Pagina + Nombre_Reporte_Generar;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                        "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
                    }
                    else if (Formato == "Excel")
                    {
                        String Ruta = "../../Exportaciones/" + Nombre_Reporte_Generar;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
                }
            }
        #endregion
        #region (Consultas)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cargar_Cuentas_Contables_Combos
            /// DESCRIPCION : Consulta las cuentas contables estan dadas de alta
            /// PARAMETROS  : 1. Dependencia_ID: Cadena de texto con el ID de la dependencia
            ///               2. Nivel_ID: Cadena de texto con el ID del nivel
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Octubre-2011
            /// MODIFICO          : Noe Mosqueda Valadez
            /// FECHA_MODIFICO    : 13/Abril/2012 17:39
            /// CAUSA_MODIFICACION: Se agrego el filtro del nivel y de la dependencia
            ///*******************************************************************************
            private void Cargar_Cuentas_Contables_Combos(String Dependencia_ID, String Nivel_ID)
            {
                DataTable Dt_Cuenta_Contable = new DataTable(); //Variable a contener los valores de la consulta
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Reporte_Contabilidad_Negocio = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //variable para la capa de negocios para los reportes
                try
                {
                    //Asignar parametros
                    Reporte_Contabilidad_Negocio.P_Dependencia_ID = Dependencia_ID;
                    Reporte_Contabilidad_Negocio.P_Nivel_ID = Nivel_ID;
                    Dt_Cuenta_Contable = Reporte_Contabilidad_Negocio.Consulta_Cuentas_Contables_Concatenada();
                    Cmb_Cuenta_Inicial.DataSource = Dt_Cuenta_Contable;
                    Cmb_Cuenta_Inicial.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                    Cmb_Cuenta_Inicial.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    Cmb_Cuenta_Inicial.DataBind();

                    Cmb_Cuenta_Inicial.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Cuenta_Inicial.SelectedIndex = 0;

                    Cmb_Cuenta_Contable_Final.DataSource = Dt_Cuenta_Contable;
                    Cmb_Cuenta_Contable_Final.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                    Cmb_Cuenta_Contable_Final.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    Cmb_Cuenta_Contable_Final.DataBind();

                    Cmb_Cuenta_Contable_Final.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Cuenta_Contable_Final.SelectedIndex = 0;
                }
                catch (Exception ex)
                {
                    throw new Exception("Cargar_Cuentas_Contables_Combos " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Contable
            /// DESCRIPCION : Consulta que la cuenta contable que proporciono el usuario este
            ///               dada de alta si es así consulta el ID o Cuenta a la cual pertenece
            /// PARAMETROS  : Tipo_Busqueda: Indica si la consulta se realiza desde una caja
            ///                              de Texto o Combo y de acuerdo a esto realiza el
            ///                              filtro que debera llevar
            ///               Cuenta_Incia : Indica si se esta consultando la cuenta desde donde
            ///                              se iniciara la consulta de la cuentas contables                              
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Octubre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Cuenta_Contable(String Tipo_Busqueda, String Cuenta_Inicial)
            {
                DataTable Dt_Cuenta_Contable = new DataTable(); //Variable a contener los valores de la consulta
                Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cat_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión hacia la capa de negocios

                try
                {
                    //Si se pretende consultar la cuenta contable del combo de la cuenta contable
                    if (Tipo_Busqueda == "COMBO")
                    {
                        if (Cuenta_Inicial == "SI")
                        {
                            Rs_Consulta_Cat_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Cuenta_Inicial.SelectedValue;
                            Txt_Cuenta_Inicial.Text = "";
                        }
                        else
                        {
                            Rs_Consulta_Cat_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Cuenta_Contable_Final.SelectedValue;
                            Txt_Cuenta_Final.Text = "";
                        }
                    }
                    //Si se pretende consultar el ID de la cuenta contable proporcionada por el usuario
                    else
                    {
                        if (Cuenta_Inicial == "SI")
                        {
                            Rs_Consulta_Cat_Con_Cuentas_Contables.P_Cuenta = Txt_Cuenta_Inicial.Text.ToString();
                            Cmb_Cuenta_Inicial.SelectedIndex = -1;
                        }
                        else
                        {
                            Rs_Consulta_Cat_Con_Cuentas_Contables.P_Cuenta = Txt_Cuenta_Final.Text.ToString();
                            Cmb_Cuenta_Contable_Final.SelectedIndex = -1;
                        }
                    }
                    Dt_Cuenta_Contable = Rs_Consulta_Cat_Con_Cuentas_Contables.Consulta_Cuentas_Contables(); //Consulta el ID y número de cuenta de la cuenta que se pretende buscar

                    //Agrega los datos de la consulta en los controles correspondientes
                    foreach (DataRow Registro in Dt_Cuenta_Contable.Rows)
                    {
                        //Agrega la cuenta contable que tiene asignado ID seleccionado del combo por el usuario
                        if (Tipo_Busqueda == "COMBO")
                        {
                            if (Cuenta_Inicial == "SI")
                            {
                                Txt_Cuenta_Inicial.Text = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
                            }
                            else
                            {
                                Txt_Cuenta_Final.Text = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
                            }
                        }
                        //Selecciona el ID de la cuenta contable que fue proporcionada por el usuario
                        else
                        {
                            if (Cuenta_Inicial == "SI")
                            {
                                Cmb_Cuenta_Inicial.SelectedValue = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
                            }
                            else
                            {
                                Cmb_Cuenta_Contable_Final.SelectedValue = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Cuenta_Contable " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Balance_Mensual
            /// DESCRIPCION : Consulta los datos a Generar para poder mostrar el reporte al
            ///               usuario
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Octubre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Balance_Mensual(String Tipo_Reporte)
            {
                String Mes; //Obtiene el mes para la genración del reporte
                String Anio; //Obtiene el año para la genración del reporte
                String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
                String Nombre_Archivo = "Balance_Mensual" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyyHHmmss}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
                DataRow Registro; //Obtiene los valores de la consulta realizada para la impresión del recibo
                Ds_Rpt_Con_Balanza_Mensual Ds_Balance_Mensual = new Ds_Rpt_Con_Balanza_Mensual();
                DataTable Dt_Balance_Mensual = new DataTable(); //Va a conter los valores a pas-ar al reporte                
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Ope_Con_Cierre_Mensual = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
                DataTable Dt_Reporte_Excel = new DataTable();
                try
                {
                    Anio = Cmb_Anio.SelectedItem.Text.ToString();
                    Mes = Cmb_Meses.SelectedItem.Text.Substring(0, 2).ToString();
                    Anio = Anio.Substring(2, 2).ToString();
                    Rs_Consulta_Ope_Con_Cierre_Mensual.P_Montos_Cero = "NO";
                    if (Cmb_Cuenta_Inicial.SelectedIndex > 0) Rs_Consulta_Ope_Con_Cierre_Mensual.P_Cuenta_Inicial = Txt_Cuenta_Inicial.Text.ToString();
                    if (Cmb_Cuenta_Contable_Final.SelectedIndex > 0) Rs_Consulta_Ope_Con_Cierre_Mensual.P_Cuenta_Final = Txt_Cuenta_Final.Text.ToString();
                    if (Chk_Movimientos_Saldo_No_Cero.Checked == true) Rs_Consulta_Ope_Con_Cierre_Mensual.P_Montos_Cero = "SI";
                    Rs_Consulta_Ope_Con_Cierre_Mensual.P_Mes_Anio = Mes + Anio;
                    Dt_Balance_Mensual = Rs_Consulta_Ope_Con_Cierre_Mensual.Consulta_Balanza_Mensual(); //Consulta los valores del mes y el año que selecciono el usuario

                    if (Dt_Balance_Mensual.Rows.Count > 0)
                    {
                        // Se llena la cabecera del DataSet                
                        //Registro = Dt_Balance_Mensual.Rows[0];
                        //Ds_Balance_Mensual.Tables["Balanza_Mensual"].ImportRow(Registro);
                        Dt_Balance_Mensual.TableName = "Balanza_Mensual";

                        Ds_Balance_Mensual.Clear();
                        Ds_Balance_Mensual.Tables.Clear();
                        Ds_Balance_Mensual.Tables.Add(Dt_Balance_Mensual.Copy());
                    }


                    ReportDocument Reporte = new ReportDocument();
                    Reporte.Load(Ruta_Archivo + "Rpt_Balanza_Mensual.rpt");
                    Reporte.SetDataSource(Ds_Balance_Mensual);

                    ParameterFieldDefinitions Cr_Parametros;
                    ParameterFieldDefinition Cr_Parametro;
                    ParameterValues Cr_Valor_Parametro = new ParameterValues();
                    ParameterDiscreteValue Cr_Valor = new ParameterDiscreteValue();

                    Cr_Parametros = Reporte.DataDefinition.ParameterFields;

                    Cr_Parametro = Cr_Parametros["Mes"];
                    Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = Cmb_Meses.SelectedValue;
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);


                    Cr_Parametro = Cr_Parametros["Anio"];
                    Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = Cmb_Anio.SelectedValue;
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);


                    if (Dt_Balance_Mensual.Rows.Count > 0)
                    {
                        Dt_Balance_Mensual = new DataTable();

                        Dt_Balance_Mensual = Rs_Consulta_Ope_Con_Cierre_Mensual.Consulta_Balanza_Mensual_Debe_Haber(); //Consulta los totales del debe y haber de las cuenntas contable seleccionadas por el usuario
                        //Obtiene los valores de la consulta anterior
                        foreach (DataRow Balance in Dt_Balance_Mensual.Rows)
                        {
                            Cr_Parametro = Cr_Parametros["Total_Debe"];
                            Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                            Cr_Valor_Parametro.Clear();

                            Cr_Valor.Value = Convert.ToDecimal(Balance["Total_Debe"].ToString());
                            Cr_Valor_Parametro.Add(Cr_Valor);
                            Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);


                            Cr_Parametro = Cr_Parametros["Total_Haber"];
                            Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                            Cr_Valor_Parametro.Clear();

                            Cr_Valor.Value = Convert.ToDecimal(Balance["Total_Haber"].ToString());
                            Cr_Valor_Parametro.Add(Cr_Valor);
                            Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);
                        }
                    }
                    else
                    {
                        Cr_Parametro = Cr_Parametros["Total_Debe"];
                        Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                        Cr_Valor_Parametro.Clear();

                        Cr_Valor.Value = 0;
                        Cr_Valor_Parametro.Add(Cr_Valor);
                        Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);


                        Cr_Parametro = Cr_Parametros["Total_Haber"];
                        Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                        Cr_Valor_Parametro.Clear();

                        Cr_Valor.Value = 0;
                        Cr_Valor_Parametro.Add(Cr_Valor);
                        Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);
                    }

                    if (Tipo_Reporte == "PDF")
                    {
                        DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                        Nombre_Archivo += ".pdf";
                        Ruta_Archivo = @Server.MapPath("../../Reporte/");
                        m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                        ExportOptions Opciones_Exportacion = new ExportOptions();
                        Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                        Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                        Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                        Reporte.Export(Opciones_Exportacion);

                        Abrir_Ventana(Nombre_Archivo);
                    }

                    else if (Tipo_Reporte == "EXCEL")
                    {
                        DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                        Nombre_Archivo += ".xls";
                        Ruta_Archivo = @Server.MapPath("../../Reporte/");
                        m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                        ExportOptions Opciones_Exportacion = new ExportOptions();
                        Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                        Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                        Opciones_Exportacion.ExportFormatType = ExportFormatType.Excel;
                        Reporte.Export(Opciones_Exportacion);


                        Dt_Reporte_Excel = Ds_Balance_Mensual.Tables[0];
                        Generar_Rpt_Excel(Dt_Reporte_Excel);
                        //Mostrar_Excel(Ruta_Archivo + Nombre_Archivo, "application/vnd.ms-excel");
                    }

                    else if (Tipo_Reporte == "WORD")
                    {
                        DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                        Nombre_Archivo += ".xls";
                        Ruta_Archivo = @Server.MapPath("../../Reporte/");
                        m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                        ExportOptions Opciones_Exportacion = new ExportOptions();
                        Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                        Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                        Opciones_Exportacion.ExportFormatType = ExportFormatType.WordForWindows;
                        Reporte.Export(Opciones_Exportacion);

                        Mostrar_Excel(Ruta_Archivo + Nombre_Archivo, "application/vnd.ms-excel");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Balance_Mensual " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
            ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
            ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
            ///                             para mostrar los datos al usuario
            ///CREO       : Yazmin A Delgado Gómez
            ///FECHA_CREO : 12-Octubre-2011
            ///MODIFICO          :
            ///FECHA_MODIFICO    :
            ///CAUSA_MODIFICACIÓN:
            ///******************************************************************************
            private void Abrir_Ventana(String Nombre_Archivo)
            {
                String Pagina = "../../Reporte/";//"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
                try
                {
                    Pagina = Pagina + Nombre_Archivo;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
                }
            }
           
            /// *************************************************************************************
            /// NOMBRE: Mostrar_Excel
            /// DESCRIPCIÓN: Muestra el reporte en excel.
            /// PARÁMETROS: No Aplica
            /// USUARIO CREO: Juan Alberto Hernández Negrete.
            /// FECHA CREO: 10/Diciembre/2011.
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA MODIFICACIÓN:
            /// *************************************************************************************
            private void Mostrar_Excel(string Ruta_Archivo, string Contenido)
            {
                try
                {
                    System.IO.FileInfo ArchivoExcel = new System.IO.FileInfo(Ruta_Archivo);
                    if (ArchivoExcel.Exists)
                    {
                        Response.Clear();
                        Response.Buffer = true;
                        Response.ContentType = Contenido;
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + ArchivoExcel.Name);
                        Response.Charset = "UTF-8";
                        Response.ContentEncoding = Encoding.Default;
                        Response.WriteFile(ArchivoExcel.FullName);
                        //Response.End();
                    }
                }
                catch (Exception Ex)
                {
                    //// Response.End(); siempre genera una excepción (http://support.microsoft.com/kb/312629/EN-US/)
                    throw new Exception("Error al mostrar el reporte en excel. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Generar_Rpt_Excel
            /// DESCRIPCION :   Se encarga de generar el archivo de excel pasandole los paramentros
            ///                 al documento
            /// PARAMETROS  :   Dt_Consulta_Reporte.- Es la consulta que contiene la informacion que se reportara
            /// CREO        :   Hugo Enrique Ramírez Aguilera
            /// FECHA_CREO  :   25/Abril/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public void Generar_Rpt_Excel(DataTable Dt_Consulta_Reporte)
            {
                WorksheetCell Celda = new WorksheetCell();
                String Nombre_Archivo = "";
                String Ruta_Archivo = "";
                Double Flujo = 0.0;
                Double Total_Debe = 0.0;
                Double Total_Haber = 0.0;
                String Formato_Importe = "";
                try
                {
                    Nombre_Archivo = "05_ANE_BC_SujetoFiscalizado_" + Cmb_Meses.SelectedValue.Substring(0,2) + "_" + Cmb_Anio.SelectedValue + ".xls";
                    
                    Ruta_Archivo = @Server.MapPath("../../Reporte/" + Nombre_Archivo);

                    //  Creamos el libro de Excel.
                    CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                    //  propiedades del libro
                    Libro.Properties.Title = "BALANZA DE COMPROBACION";
                    Libro.Properties.Created = DateTime.Now;
                    Libro.Properties.Author = "JAPAMI_Irapuato";

                    //  Creamos el estilo cabecera para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("Encabezado");//1
                    //CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera_Izquierda = Libro.Styles.Add("Encabezado_Izquierda");//2
                    //CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Indice = Libro.Styles.Add("Contenido_Indice");//3
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Nombre = Libro.Styles.Add("Contenido_Nombre");//4
                    //CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Nota = Libro.Styles.Add("Contenido_Nota");//5
                    //CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Nombre_Negritas = Libro.Styles.Add("Contenido_Nombre_Negritas");//6
                    //CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Firma = Libro.Styles.Add("Contenido_Firma");//7
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Totales = Libro.Styles.Add("Totales");//8
                    //CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Totales_Negritas = Libro.Styles.Add("Totales_Negritas");//9


                    #region Estilos
                    //***************************************inicio de los estilos***********************************************************
                    //  estilo para la cabecera    Encabezado
                    Estilo_Cabecera.Font.FontName = "Arial";
                    Estilo_Cabecera.Font.Size = 10;
                    Estilo_Cabecera.Font.Bold = true;
                    Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera.Alignment.Rotate = 0;
                    Estilo_Cabecera.Font.Color = "#000000";
                    Estilo_Cabecera.Interior.Color = "#FCD8BA";
                    Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el    Contenido_Nombre
                    Estilo_Contenido_Nombre.Font.FontName = "Arial";
                    Estilo_Contenido_Nombre.Font.Size = 10;
                    Estilo_Contenido_Nombre.Font.Bold = false;
                    Estilo_Contenido_Nombre.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                    Estilo_Contenido_Nombre.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Contenido_Nombre.Alignment.Rotate = 0;
                    Estilo_Contenido_Nombre.Font.Color = "#000000";
                    Estilo_Contenido_Nombre.Interior.Color = "White";
                    Estilo_Contenido_Nombre.Interior.Pattern = StyleInteriorPattern.None;
                    Estilo_Contenido_Nombre.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Contenido_Nombre.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Contenido_Nombre.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Contenido_Nombre.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    

                    //  estilo para el presupuesto (importe)
                    Estilo_Totales.Font.FontName = "Arial";
                    Estilo_Totales.Font.Size = 10;
                    Estilo_Totales.Font.Bold = false;
                    Estilo_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Totales.Alignment.Rotate = 0;
                    Estilo_Totales.Font.Color = "#000000";
                    Estilo_Totales.Interior.Color = "White";
                    Estilo_Totales.Interior.Pattern = StyleInteriorPattern.None;
                    Estilo_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                   
                    //*************************************** fin de los estilos***********************************************************
                    #endregion


                    #region Hojas
                    //  Creamos una hoja que tendrá el libro.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("BC");
                    //  Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                    #endregion

                    #region BC
                    //  Agregamos las columnas que tendrá la hoja de excel.
                    Renglon.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  1 Cuentas con todos sus niveles.
                    Renglon.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//  2 Nombre de la cuenta
                    Renglon.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  3 Saldo inicial .
                    Renglon.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  4 debe .
                    Renglon.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  5 haber .
                    Renglon.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  6 saldo final.
                    Renglon.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  7 flujo .


                    //  se llena el encabezado principal
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("BALANZA DE COMPROBACIÓN");
                    Celda.MergeAcross = 6; // Merge 7 cells together
                    Celda.StyleID = "Encabezado";


                    //  para los titulos
                    Renglon = Hoja.Table.Rows.Add();
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Cuentas", "Encabezado"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Nombre", "Encabezado"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Saldo inicial", "Encabezado"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Debe", "Encabezado"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Haber", "Encabezado"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Saldo final", "Encabezado"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Flujo", "Encabezado"));

                    //  para la informacion
                    if (Dt_Consulta_Reporte is DataTable)
                    {
                        if (Dt_Consulta_Reporte.Rows.Count > 0)
                        {
                            foreach (DataRow Registro in Dt_Consulta_Reporte.Rows)
                            {
                                Renglon = Hoja.Table.Rows.Add();
                                //  para la cuenta
                                if (!String.IsNullOrEmpty(Registro["Cuenta"].ToString()))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Registro["Cuenta"].ToString(), "Contenido_Nombre"));
                                }
                                //  para la descripcion
                                if (!String.IsNullOrEmpty(Registro["Descripcion"].ToString()))
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Registro["Descripcion"].ToString(), "Contenido_Nombre"));
                                }
                                //  para la saldo inical
                                if (!String.IsNullOrEmpty(Registro["Saldo_Inicial"].ToString()))
                                {
                                    Formato_Importe = String.Format("{0:n}", Registro["Saldo_Inicial"]);
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                                }

                                //  para el debe
                                if (!String.IsNullOrEmpty(Registro["Total_Debe"].ToString()))
                                {
                                    Total_Debe = Convert.ToDouble(Registro["Total_Debe"].ToString());
                                    Formato_Importe = String.Format("{0:n}", Registro["Total_Debe"]);
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                                }
                                else
                                    Total_Debe = 0.0;

                                //  para el haber
                                if (!String.IsNullOrEmpty(Registro["Total_Haber"].ToString()))
                                {
                                    Total_Haber = Convert.ToDouble(Registro["Total_Haber"].ToString());
                                    Formato_Importe = String.Format("{0:n}", Registro["Total_Haber"]);
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                                }
                                else
                                    Total_Haber = 0.0;

                                //  para la saldo final
                                if (!String.IsNullOrEmpty(Registro["Saldo_Final"].ToString()))
                                {
                                    Formato_Importe = String.Format("{0:n}", Registro["Saldo_Final"]);
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                                }
                                //  para el flujo
                                Flujo = Total_Debe - Total_Haber;
                                Formato_Importe = String.Format("{0:n}", Flujo);
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                            }
                        }
                    }

                    #endregion

                    //se guarda el documento
                    //Libro.Save(Ruta_Archivo);

                    //Abre el archivo de excel
                    //Mostrar_Excel(Nombre_Archivo, "application/vnd.ms-excel");
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + Nombre_Archivo);
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;
                    Libro.Save(Response.OutputStream);
                    Response.End();
                   
                }
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Ex.Message;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION: Llena_Combo_Dependencias
            ///DESCRIPCION: Llenar el combo de las dependencias
            ///PARAMETROS : 
            ///CREO       : Noe Mosqueda Valadez
            ///FECHA_CREO : 13/Abril/2012 17:30
            ///MODIFICO          :
            ///FECHA_MODIFICO    :
            ///CAUSA_MODIFICACION:
            ///******************************************************************************
            private void Llena_Combo_Dependencias()
            {
                //Declaracion de variables
                Cls_Cat_Dependencias_Negocio Dependencias_Negocio = new Cls_Cat_Dependencias_Negocio(); //variable para la capa de negocios
                DataTable Dt_Dependencias = new DataTable(); //tabla para la consulta de las dependencias

                try
                {
                    //Realizar consulta
                    Dt_Dependencias = Dependencias_Negocio.Consulta_Dependencias();

                    //Llenar el combo
                    Cmb_Dependencias.DataSource = Dt_Dependencias;
                    Cmb_Dependencias.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Dependencias.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                    Cmb_Dependencias.DataBind();
                    Cmb_Dependencias.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Dependencias.SelectedIndex = 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString(), ex);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION: Llena_Combo_Niveles
            ///DESCRIPCION: Llenar el combo de los niveles
            ///PARAMETROS : 
            ///CREO       : Noe Mosqueda Valadez
            ///FECHA_CREO : 13/Abril/2012 17:30
            ///MODIFICO          :
            ///FECHA_MODIFICO    :
            ///CAUSA_MODIFICACION:
            ///******************************************************************************
            private void Llena_Combo_Niveles()
            {
                //Declaracion de variables
                Cls_Cat_Con_Niveles_Negocio Niveles_Negocio = new Cls_Cat_Con_Niveles_Negocio();
                DataTable Dt_Niveles = new DataTable(); //tabla para la consulta de los niveles

                try
                {
                    //Asignar la consulta
                    Dt_Niveles = Niveles_Negocio.Consulta_Niveles();

                    //Llenar el combo
                    Cmb_Niveles.DataSource = Dt_Niveles;
                    Cmb_Niveles.DataTextField = Cat_Con_Niveles.Campo_Descripcion;
                    Cmb_Niveles.DataValueField = Cat_Con_Niveles.Campo_Nivel_ID;
                    Cmb_Niveles.DataBind();
                    Cmb_Niveles.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Niveles.SelectedIndex = 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString(), ex);
                }
            }

            private void Consulta_Debe_Haber()
            {
                //Declaracion de variables
                String Mes; //Obtiene el mes para la genración del reporte
                String Anio; //Obtiene el año para la genración del reporte
                DataTable Dt_Balance_Mensual = new DataTable(); //Va a conter los valores a pas-ar al reporte                
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Ope_Con_Cierre_Mensual = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
                int Cont_Elementos = 0; //variable apra el contador
                Double Debe = 0; //Variable para el debe
                Double Haber = 0; //Variable para el haber

                try
                {
                    Anio = Cmb_Anio.SelectedItem.Text.ToString();
                    Mes = Cmb_Meses.SelectedItem.Text.Substring(0, 2).ToString();
                    Anio = Anio.Substring(2, 2).ToString();
                    Rs_Consulta_Ope_Con_Cierre_Mensual.P_Montos_Cero = "NO";
                    if (Cmb_Cuenta_Inicial.SelectedIndex > 0) Rs_Consulta_Ope_Con_Cierre_Mensual.P_Cuenta_Inicial = Txt_Cuenta_Inicial.Text.ToString();
                    if (Cmb_Cuenta_Contable_Final.SelectedIndex > 0) Rs_Consulta_Ope_Con_Cierre_Mensual.P_Cuenta_Final = Txt_Cuenta_Final.Text.ToString();
                    if (Chk_Movimientos_Saldo_No_Cero.Checked == true) Rs_Consulta_Ope_Con_Cierre_Mensual.P_Montos_Cero = "SI";
                    Rs_Consulta_Ope_Con_Cierre_Mensual.P_Mes_Anio = Mes + Anio;
                    Dt_Balance_Mensual = Rs_Consulta_Ope_Con_Cierre_Mensual.Consulta_Balanza_Mensual(); //Consulta los valores del mes y el año que selecciono el usuario

                    //Verificar si la consulta arrojo resultados
                    if (Dt_Balance_Mensual.Rows.Count > 0)
                    {
                        //Ciclo para el barrido de la tabla
                        for (Cont_Elementos = 0; Cont_Elementos < Dt_Balance_Mensual.Rows.Count; Cont_Elementos++)
                        {
                            //Calcular los totales
                            if (Dt_Balance_Mensual.Rows[Cont_Elementos][Ope_Con_Cierre_Mensual.Campo_Total_Debe] != DBNull.Value)
                            {
                                Debe += Convert.ToDouble(Dt_Balance_Mensual.Rows[Cont_Elementos][Ope_Con_Cierre_Mensual.Campo_Total_Debe]);
                            }

                            if (Dt_Balance_Mensual.Rows[Cont_Elementos][Ope_Con_Cierre_Mensual.Campo_Total_Haber] != DBNull.Value)
                            {
                                Haber += Convert.ToDouble(Dt_Balance_Mensual.Rows[Cont_Elementos][Ope_Con_Cierre_Mensual.Campo_Total_Haber]);
                            }
                        }

                        //Colocar los valores en las etiquetas
                        Lbl_Debe.Text = "Total Debe:<br />" + String.Format("{0:c}", Debe);
                        Lbl_Haber.Text = "Total Haber:<br />" + String.Format("{0:c}", Haber);
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "El mes seleccionado, no se ha cerrado, favor de cerrar primero el mes.";
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }

            private void Consulta_Cuentas_Contables_Aproximacion(String Cuenta, String Control)
            {
                //Declaracion de variables
                DataTable Dt_Cuentas = new DataTable(); //Tabla para el resultado de la consulta
                Cls_Cat_Con_Cuentas_Contables_Negocio Cuentas_Contables_Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión hacia la capa de negocios

                try
                {
                    //Asignar propiedades
                    if (Cls_Util.EsNumerico(Cuenta) == true)
                    {
                        Cuentas_Contables_Negocio.P_Tipo_Busqueda = "Cuenta";
                    }
                    else
                    {
                        Cuentas_Contables_Negocio.P_Tipo_Busqueda = "Descripcion";
                    }
                    Cuentas_Contables_Negocio.P_Descripcion = Cuenta;
                    Dt_Cuentas = Cuentas_Contables_Negocio.Consulta_Cuentas_Contables_Cuenta_Izquierda();

                    //Seleccionar el combo
                    switch (Control)
                    {
                        case "Inicial":
                            Cmb_Cuenta_Inicial.DataSource = Dt_Cuentas;
                            Cmb_Cuenta_Inicial.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                            Cmb_Cuenta_Inicial.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                            Cmb_Cuenta_Inicial.DataBind();

                            Cmb_Cuenta_Inicial.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                            Cmb_Cuenta_Inicial.SelectedIndex = 0;
                            break;

                        case "Final":
                            Cmb_Cuenta_Contable_Final.DataSource = Dt_Cuentas;
                            Cmb_Cuenta_Contable_Final.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                            Cmb_Cuenta_Contable_Final.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                            Cmb_Cuenta_Contable_Final.DataBind();

                            Cmb_Cuenta_Contable_Final.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                            Cmb_Cuenta_Contable_Final.SelectedIndex = 0;
                            break;

                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }

            private void Consulta_Nueva_Balanza_Mensual(String Formato)
            {
                //Declaracion de variables
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Reporte_Contabilidad_Negocio = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //variable para la capa de negocios
                DataSet Ds_Balanza_Mensual = new DataSet(); //Dataset para el resultado de la consulta
                String Mes; //Variable para el mes
                String Anio; //variable para el año
                String Ruta_Archivo = String.Empty;
                DataRow Renglon; //Renglon para el llenado de las tablas
                Ds_Rpt_Con_Nueva_Balanza_Mensual Ds_Rpt_Con_Nueva_Balanza_Mensual_src = new Ds_Rpt_Con_Nueva_Balanza_Mensual(); //Dataset archivo para el llenado del reporte de Crystal
                int Cont_Elementos = 0; //variable para el contador
                ReportDocument Reporte = new ReportDocument(); //variable para el reporte de crystal
                String Nombre_Archivo = String.Empty; //Variable para el nombre del archivo
                DiskFileDestinationOptions m_crDiskFileDestinationOptions; //variable para el almacenamiento en disco
                ExportOptions Opciones_Exportacion; //Variable para la exportacion
                int Total_Dias_Mes = 0; //variable para el total de dias del mes seleccionado

                try
                {
                    //Asignar la ruta del archivo
                    Ruta_Archivo = @HttpContext.Current.Server.MapPath("../Rpt/Contabilidad/"); //OBtiene la ruta del crystal

                    //Colocar los datos del mes y año
                    Anio = Cmb_Anio.SelectedItem.Text.Trim();

                    //verificar el tipo de reporte
                    if (Cmb_Tipo_Balance.SelectedIndex != 2)
                    {
                        Mes = Cmb_Meses.SelectedItem.Text.Substring(0, 2).Trim();

                    }
                    else
                    {
                        Mes = "13";
                    }

                    //Colocar los otros filtros del reporte
                    if (Cmb_Cuenta_Inicial.SelectedIndex > 0)
                    {
                        Reporte_Contabilidad_Negocio.P_Cuenta_Inicial = Txt_Cuenta_Inicial.Text.Trim();
                    }

                    if (Cmb_Cuenta_Contable_Final.SelectedIndex > 0)
                    {
                        Reporte_Contabilidad_Negocio.P_Cuenta_Final = Txt_Cuenta_Final.Text.Trim();
                    }

                    if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                    {
                        Reporte_Contabilidad_Negocio.P_Montos_Cero = "SI";
                    }
                    else
                    {
                        Reporte_Contabilidad_Negocio.P_Montos_Cero = "NO";
                    }

                    Reporte_Contabilidad_Negocio.P_Mes_Anio = Mes + Anio.Substring(2, 2);
                    if (Mes == "13")
                    {
                        Reporte_Contabilidad_Negocio.P_Mes = Mes;
                    }
                    else
                    {
                        Reporte_Contabilidad_Negocio.P_Mes = Cmb_Meses.SelectedItem.Text.Substring(3);
                    }
                    Reporte_Contabilidad_Negocio.P_Anio = Convert.ToInt32(Anio);
                    if (Mes != "13")
                    {
                        Total_Dias_Mes = DateTime.DaysInMonth(Convert.ToInt32(Cmb_Anio.SelectedItem.Value), Convert.ToInt32(Mes));
                        Reporte_Contabilidad_Negocio.P_Fecha_Final = Convert.ToString(Total_Dias_Mes).Trim() + "/" + Mes + "/" + Anio;
                    }
                    else
                    {
                        Reporte_Contabilidad_Negocio.P_Fecha_Final = "31/12/" + Anio;
                    }

                    
                    //Ejecutar la consulta
                    Ds_Balanza_Mensual = Reporte_Contabilidad_Negocio.Consulta_Nueva_Balanza_Mensual();

                    //Verificar si la consulta arrojo resultados
                    if (Ds_Balanza_Mensual.Tables.Count == 2)
                    {
                        //Verificar si la tabla de los detalles tiene elementos
                        if (Ds_Balanza_Mensual.Tables[1].Rows.Count > 0)
                        {
                            //LLenar el dataset archivo con los datos
                            for (Cont_Elementos = 0; Cont_Elementos < Ds_Balanza_Mensual.Tables[0].Rows.Count; Cont_Elementos++)
                            {
                                //Instanciar el renglon y guardarlo en la tabla de la cabecera
                                Renglon = Ds_Balanza_Mensual.Tables[0].Rows[Cont_Elementos];
                                Ds_Rpt_Con_Nueva_Balanza_Mensual_src.Tables[0].ImportRow(Renglon);
                            }

                            for (Cont_Elementos = 0; Cont_Elementos < Ds_Balanza_Mensual.Tables[1].Rows.Count; Cont_Elementos++)
                            {
                                //Instanciar el renglon y guardarlo en la tabla de los detalles
                                Renglon = Ds_Balanza_Mensual.Tables[1].Rows[Cont_Elementos];
                                Ds_Rpt_Con_Nueva_Balanza_Mensual_src.Tables[1].ImportRow(Renglon);
                            }

                            //Colocar el dataset en el reporte
                            Reporte.Load(Ruta_Archivo + "Rpt_Nueva_Balanza_Mensual_Compacta.rpt");
                            Reporte.SetDataSource(Ds_Rpt_Con_Nueva_Balanza_Mensual_src);

                            //Asignar las propiedades para generar el reporte
                            Nombre_Archivo = "Balance_Mensual" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyyHHmmss}", DateTime.Today));

                            //Verificar el formato
                            switch (Formato)
                            {
                                case "pdf":
                                    Nombre_Archivo += ".pdf";
                                    break;

                                case "xls":
                                    Nombre_Archivo += ".xls";
                                    break;

                                default:
                                    break;
                            }

                            Ruta_Archivo = @HttpContext.Current.Server.MapPath("../../Reporte/");
                            m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();
                            m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                            //Asignar propiedades para la exportacion
                            Opciones_Exportacion = new ExportOptions();
                            Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                            Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;

                            //verificar el formato
                            switch (Formato)
                            {
                                case "pdf":
                                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                                    break;

                                case "xls":
                                    Opciones_Exportacion.ExportFormatType = ExportFormatType.Excel;
                                    break;

                                default:
                                    break;
                            }

                            Reporte.Export(Opciones_Exportacion);

                            //Verificar el tipo de reporte
                            switch (Formato)
                            {
                                case "pdf":
                                    Abrir_Ventana(Nombre_Archivo);
                                    break;

                                case "xls":
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta_Archivo + Nombre_Archivo + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta_Archivo + Nombre_Archivo + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
                                    Abrir_Ventana(Nombre_Archivo);
                                    break;

                                default:
                                    break;
                            }
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "El mes seleccionado, no se ha cerrado, favor de cerrar primero el mes.";
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "El mes seleccionado, no se ha cerrado, favor de cerrar primero el mes.";
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }

            private void Nueva_Balanza_Mensual_Excel()
            {
                //Declaracion de variables
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Reporte_Contabilidad_Negocio = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //variable para la capa de negocios
                DataSet Ds_Balanza_Mensual = new DataSet(); //Dataset para el resultado de la consulta
                string Mes; //variable para el mes
                string Anio; //variable para el año
                int Cont_Elementos = 0; //variable para el contador
                int Total_Dias_Mes = 0; //variable para el total de dias del mes seleccionado
                string Nombre_Archivo = string.Empty; //variable apra el nombre del archivo
                string Ruta_Archivo = string.Empty; //variable para la ruta donde sera almacenado el reporte
                Workbook Libro; //variable para el libro de Excel
                Worksheet Hoja; //variable para la hoja del libro
                DateTime Fecha; //variabkle para la fecha que sera puesta en el reporte
                WorksheetStyle Estilo_Cabecera; //Estilo para lña cabecera
                WorksheetStyle Estilo_Tabla1; //Estilo para la tabla subrayada
                WorksheetStyle Estilo_Montos; //Estilo para los montos
                WorksheetStyle Estilo_Montos_Negrita; //Estilo para los montos totales
                WorksheetStyle Estilo_Texto; //Estilo para los textos
                WorksheetRow Renglon; //Renglon para el llenado del documento
                WorksheetCell Celda; //Celda para el llenado del documento

                try
                {
                    //Colocar los datos del mes y año
                    Anio = Cmb_Anio.SelectedItem.Text.Trim();

                    //verificar el tipo de reporte
                    if (Cmb_Tipo_Balance.SelectedIndex != 2)
                    {
                        Mes = Cmb_Meses.SelectedItem.Text.Substring(0, 2).Trim();
                    }
                    else
                    {
                        Mes = "13";
                    }

                    //Colocar los otros filtros del reporte
                    if (Cmb_Cuenta_Inicial.SelectedIndex > 0)
                    {
                        Reporte_Contabilidad_Negocio.P_Cuenta_Inicial = Txt_Cuenta_Inicial.Text.Trim();
                    }

                    if (Cmb_Cuenta_Contable_Final.SelectedIndex > 0)
                    {
                        Reporte_Contabilidad_Negocio.P_Cuenta_Final = Txt_Cuenta_Final.Text.Trim();
                    }

                    if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                    {
                        Reporte_Contabilidad_Negocio.P_Montos_Cero = "SI";
                    }
                    else
                    {
                        Reporte_Contabilidad_Negocio.P_Montos_Cero = "NO";
                    }

                    Reporte_Contabilidad_Negocio.P_Mes_Anio = Mes + Anio.Substring(2, 2);

                    if (Mes == "13")
                    {
                        Reporte_Contabilidad_Negocio.P_Mes = Mes;
                    }
                    else
                    {
                        Reporte_Contabilidad_Negocio.P_Mes = Cmb_Meses.SelectedItem.Text.Substring(3);
                    }
                    
                    Reporte_Contabilidad_Negocio.P_Anio = Convert.ToInt32(Anio);
                    if (Mes == "13")
                    {
                        Total_Dias_Mes = 31;
                    }
                    else
                    {
                        Total_Dias_Mes = DateTime.DaysInMonth(Convert.ToInt32(Cmb_Anio.SelectedItem.Value), Convert.ToInt32(Mes));
                    }

                    if (Mes == "13")
                    {
                        Reporte_Contabilidad_Negocio.P_Fecha_Final = "31/12/" + Anio;
                    }
                    else
                    {
                        Reporte_Contabilidad_Negocio.P_Fecha_Final = Convert.ToString(Total_Dias_Mes).Trim() + "/" + Mes + "/" + Anio;
                    }

                    //Ejecutar la consulta
                    Ds_Balanza_Mensual = Reporte_Contabilidad_Negocio.Consulta_Nueva_Balanza_Mensual();

                    //verificar si la consulta arrojo resultados
                    if (Ds_Balanza_Mensual.Tables.Count == 2)
                    {
                        //verificar si la tabla de los detalles tiene elementos
                        if (Ds_Balanza_Mensual.Tables[1].Rows.Count > 0)
                        {
                            //Asigar el nombre y la ruta del archivo
                            if (Mes == "13")
                            {
                                Nombre_Archivo = "Balanza_MES_13_" + Cmb_Anio.SelectedItem.Text + ".xls";
                            }
                            else
                            {
                                Nombre_Archivo = "Balanza_" + Cmb_Meses.SelectedItem.Text + "_" + Cmb_Anio.SelectedItem.Text + ".xls";
                            }
                            
                            Ruta_Archivo = HttpContext.Current.Server.MapPath("../../Reporte/" + Nombre_Archivo);

                            if (Mes == "13")
                            {
                                Fecha = new DateTime(Convert.ToInt32(Anio), 12, 31);
                            }
                            else
                            {
                                //Calcular la fecha
                                Fecha = new DateTime(Convert.ToInt32(Anio), Convert.ToInt32(Mes), 1);
                                Fecha = Fecha.AddMonths(1);
                                Fecha = Fecha.AddDays(-1);
                            }

                            //Asignar las propiedades del libro de Excel
                            Libro = new Workbook();
                            Libro.Properties.Title = "Balanza de Comprobación al " + string.Format("{0:dd/MM/yyyy}", Fecha);
                            Libro.Properties.Author = "JAPAMI";
                            Libro.Properties.Created = DateTime.Now;

                            //Crear la hoja
                            Hoja = Libro.Worksheets.Add(Cmb_Meses.SelectedItem.Text.Substring(0, 3).ToLower() + Cmb_Anio.SelectedItem.Text);

                            //Estilos del libro
                            Estilo_Cabecera = Libro.Styles.Add("Cabecera");
                            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                            Estilo_Cabecera.Font.FontName = "Times";
                            Estilo_Cabecera.Font.Bold = true;
                            Estilo_Cabecera.Font.Size = 10;

                            Estilo_Tabla1 = Libro.Styles.Add("Tabla1");
                            Estilo_Tabla1.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                            Estilo_Tabla1.Font.FontName = "Times";
                            Estilo_Tabla1.Font.Bold = true;
                            Estilo_Tabla1.Font.Size = 10;
                            Estilo_Tabla1.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");

                            Estilo_Texto = Libro.Styles.Add("Texto");
                            Estilo_Texto.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                            Estilo_Texto.Font.FontName = "Times";
                            Estilo_Texto.Font.Size = 8;

                            Estilo_Montos = Libro.Styles.Add("Montos");
                            Estilo_Montos.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                            Estilo_Montos.Font.FontName = "Times";
                            Estilo_Montos.Font.Size = 8;

                            Estilo_Montos_Negrita = Libro.Styles.Add("Montos_Negrita");
                            Estilo_Montos_Negrita.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                            Estilo_Montos_Negrita.Font.FontName = "Times";
                            Estilo_Montos_Negrita.Font.Bold = true;
                            Estilo_Montos_Negrita.Font.Size = 8;

                            //Colocar los datos de la cabecera
                            Renglon = Hoja.Table.Rows.Add();
                            Celda = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.", DataType.String, "Cabecera");
                            Celda.MergeAcross = 5;
                            Renglon = Hoja.Table.Rows.Add();
                            Celda = Renglon.Cells.Add("SIAC", DataType.String, "Cabecera");
                            Celda.MergeAcross = 5;
                            Renglon = Hoja.Table.Rows.Add();
                            Renglon = Hoja.Table.Rows.Add();
                            Celda = Renglon.Cells.Add("BALANZA DE COMPROBACIÓN AL " + string.Format("{0:dd/MM/yyyy}", Fecha), DataType.String, "Cabecera");
                            Celda.MergeAcross = 5;
                            Renglon = Hoja.Table.Rows.Add();
                            Celda = Renglon.Cells.Add("Mes: " + Mes, DataType.String, "Cabecera");
                            Celda.MergeAcross = 1;
                            Celda = Renglon.Cells.Add("Año: " + Anio, DataType.String, "Cabecera");
                            Celda.MergeAcross = 1;
                            Renglon.Cells.Add(new WorksheetCell(string.Format("{0:dd/MMM/yyyy}", DateTime.Now), DataType.String, "Cabecera"));
                            Renglon.Cells.Add(new WorksheetCell(string.Format("{0:HH:mm:ss}", DateTime.Now), DataType.String, "Cabecera"));

                            //Colocar los encabezados de las columnas
                            Renglon = Hoja.Table.Rows.Add();
                            Renglon.Cells.Add(new WorksheetCell("Cuenta", DataType.String, "Tabla1"));
                            Renglon.Cells.Add(new WorksheetCell("Descripción", DataType.String, "Tabla1"));
                            Renglon.Cells.Add(new WorksheetCell("Saldo Inicial", DataType.String, "Tabla1"));
                            Renglon.Cells.Add(new WorksheetCell("Debe", DataType.String, "Tabla1"));
                            Renglon.Cells.Add(new WorksheetCell("Haber", DataType.String, "Tabla1"));
                            Renglon.Cells.Add(new WorksheetCell("Saldo Final", DataType.String, "Tabla1"));
                            
                            //Ciclo para el barrido de los elementos
                            for (Cont_Elementos = 0; Cont_Elementos < Ds_Balanza_Mensual.Tables[1].Rows.Count; Cont_Elementos++)
                            {
                                //Agregar renglon
                                Renglon = Hoja.Table.Rows.Add();

                                //Colocar las columnas
                                Renglon.Cells.Add(new WorksheetCell(Ds_Balanza_Mensual.Tables[1].Rows[Cont_Elementos]["Cuenta"].ToString().Trim(), DataType.String, "Texto"));
                                Renglon.Cells.Add(new WorksheetCell(Ds_Balanza_Mensual.Tables[1].Rows[Cont_Elementos]["Descripcion"].ToString().Trim(), DataType.String, "Texto"));
                                Renglon.Cells.Add(new WorksheetCell("" + string.Format("{0:n}", Convert.ToDouble(Ds_Balanza_Mensual.Tables[1].Rows[Cont_Elementos]["Saldo_Inicial"])).Replace("$",""), "Montos"));
                                Renglon.Cells.Add(new WorksheetCell("" + string.Format("{0:n}", Convert.ToDouble(Ds_Balanza_Mensual.Tables[1].Rows[Cont_Elementos]["Debe"].ToString().Trim())).Replace("$", ""), "Montos"));
                                Renglon.Cells.Add(new WorksheetCell("" + string.Format("{0:n}", Convert.ToDouble(Ds_Balanza_Mensual.Tables[1].Rows[Cont_Elementos]["Haber"].ToString().Trim())).Replace("$", ""), "Montos"));
                                Renglon.Cells.Add(new WorksheetCell("" + string.Format("{0:n}", Convert.ToDouble(Ds_Balanza_Mensual.Tables[1].Rows[Cont_Elementos]["Saldo_Final"].ToString().Trim())).Replace("$", ""), "Montos"));
                            }

                            //Colocar el renglon con los totales
                            Renglon = Hoja.Table.Rows.Add();
                            Renglon.Cells.Add(new WorksheetCell("", DataType.String, "Texto"));
                            Renglon.Cells.Add(new WorksheetCell("", DataType.String, "Texto"));
                            Renglon.Cells.Add(new WorksheetCell("" + string.Format("{0:n}", Convert.ToDouble(Ds_Balanza_Mensual.Tables[0].Rows[0]["Saldo_Inicial"].ToString().Trim())).Replace("$", ""), "Montos_Negrita"));
                            Renglon.Cells.Add(new WorksheetCell("" + string.Format("{0:n}", Convert.ToDouble(Ds_Balanza_Mensual.Tables[0].Rows[0]["Total_Debe"].ToString().Trim())).Replace("$", ""), "Montos_Negrita"));
                            Renglon.Cells.Add(new WorksheetCell("" + string.Format("{0:n}", Convert.ToDouble(Ds_Balanza_Mensual.Tables[0].Rows[0]["Total_Haber"].ToString().Trim())).Replace("$", ""), "Montos_Negrita"));
                            Renglon.Cells.Add(new WorksheetCell("" + string.Format("{0:n}", Convert.ToDouble(Ds_Balanza_Mensual.Tables[0].Rows[0]["Saldo_Final"].ToString().Trim())).Replace("$", ""), "Montos_Negrita"));

                            //Hoja.Protected = true;
                            Libro.Save(HttpContext.Current.Server.MapPath("~") + "\\Exportaciones\\" + Nombre_Archivo);
                            Mostrar_Reporte(Nombre_Archivo, "Excel");
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "La balanza no contiene información.";
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "La balanza no contiene información.";
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
    
        #endregion
    #endregion
    #region (Eventos)
        protected void Cmb_Cuenta_Inicial_SelectedIndexChanged(object sender, EventArgs e)
        {
            Txt_Cuenta_Inicial.Text = "";
            if (Cmb_Cuenta_Inicial.SelectedIndex > 0) Consulta_Cuenta_Contable("COMBO", "SI"); //Consulta el No Cuenta Contable que tiene seleccionado la descripcion de la cuenta proporcionada
        }
        protected void Cmb_Cuenta_Contable_Final_SelectedIndexChanged(object sender, EventArgs e)
        {
            Txt_Cuenta_Final.Text = "";
            if (Cmb_Cuenta_Contable_Final.SelectedIndex > 0) Consulta_Cuenta_Contable("COMBO", "NO"); //Consulta el No Cuenta Contable que tiene seleccionado la descripcion de la cuenta proporcionada
        }
        protected void Txt_Cuenta_Inicial_TextChanged(object sender, EventArgs e)
        {
            //Cmb_Cuenta_Inicial.SelectedIndex = -1;
            //if (!String.IsNullOrEmpty(Txt_Cuenta_Inicial.Text)) Consulta_Cuenta_Contable("TEXTO", "SI"); //Consulta el ID de la cuenta contable que tiene asignado el n{umero de cuenta que fue proporcionada por el usuario
            
            Consulta_Cuentas_Contables_Aproximacion(Txt_Cuenta_Inicial.Text.Trim(), "Inicial");
        }
        protected void Txt_Cuenta_Final_TextChanged(object sender, EventArgs e)
        {
            //Cmb_Cuenta_Contable_Final.SelectedIndex = -1;
            //if (!String.IsNullOrEmpty(Txt_Cuenta_Final.Text)) Consulta_Cuenta_Contable("TEXTO", "NO"); //Consulta el ID de la cuenta contable que tiene asignado el n{umero de cuenta que fue proporcionada por el usuario

            Consulta_Cuentas_Contables_Aproximacion(Txt_Cuenta_Final.Text.Trim(), "Final");
        }


        protected void Btn_Reporte_Balance_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (Validar_Datos_Reporte())
                {
                    //Consulta_Balance_Mensual("PDF"); //Consulta el balance mensual
                    Consulta_Nueva_Balanza_Mensual("pdf");
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }


        /// *************************************************************************************
        /// NOMBRE:         Btn_Generar_Reporte_Excel_Click
        /// DESCRIPCIÓN:    Genera el reporte en el formato de excel
        /// PARÁMETROS:     No Aplica
        /// 
        /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREO:     23/Abril/2012
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Btn_Generar_Reporte_Excel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (Validar_Datos_Reporte())
                {
                    //Consulta_Balance_Mensual("EXCEL"); //Consulta el balance mensual
                    //Consulta_Nueva_Balanza_Mensual("xls");
                    Nueva_Balanza_Mensual_Excel();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
            catch (System.Threading.ThreadAbortException ex)
            { }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte Catálogo de empleados. Error: [" + Ex.Message + "]");
            }
        }

        /// *************************************************************************************
        /// NOMBRE:         Btn_Generar_Reporte_Word_Click
        /// DESCRIPCIÓN:    Genera el reporte en el formato de word
        /// PARÁMETROS:     No Aplica
        /// 
        /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREO:     23/Abril/2012
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Btn_Generar_Reporte_Word_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (Validar_Datos_Reporte())
                {
                    Consulta_Balance_Mensual("EXCEL"); //Consulta el balance mensual
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
            catch (System.Threading.ThreadAbortException ex)
            { }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte Catálogo de empleados. Error: [" + Ex.Message + "]");
            }
        }

        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }

        protected void Btn_Consultar_Totales_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Consulta_Debe_Haber();
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Error: (Btn_Consultar_Totales_Click)" + ex.Message.ToString();
            }
        }

        protected void Cmb_Dependencias_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Cargar_Cuentas_Contables_Combos(Cmb_Dependencias.SelectedItem.Value, Cmb_Niveles.SelectedItem.Value);
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Error: (Cmb_Dependencias_SelectedIndexChanged)" + ex.Message.ToString();
            }
        }

        protected void Cmb_Niveles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Cargar_Cuentas_Contables_Combos(Cmb_Dependencias.SelectedItem.Value, Cmb_Niveles.SelectedItem.Value);
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Error: (Cmb_Niveles_SelectedIndexChanged)" + ex.Message.ToString();
            }
        }

    #endregion
}
