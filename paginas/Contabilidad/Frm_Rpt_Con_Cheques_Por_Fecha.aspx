﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Con_Cheques_Por_Fecha.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Cheques_Por_Fecha" Title="Reporte Cheques Dia" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Mostrar_Tabla(Renglon, Imagen) {
            object = document.getElementById(Renglon);
            if (object.style.display == "none") {
                object.style.display = "block";
                document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
            } else {
                object.style.display = "none";
                document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server" >
    <asp:ScriptManager ID="ScriptManager_Areas" runat="server"  EnableScriptGlobalization="true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>        
            <asp:UpdateProgress ID="Uprg_Cheques_Realizados" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
               <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Reporte_Cuentas_Afectables" style="background-color:#ffffff; width:100%; height:100%;">    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Reporte Cheques por Fecha</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td colspan="2">                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;">
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                CssClass="Img_Button" TabIndex="1"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                onclick="Btn_Nuevo_Click" />
                                            <asp:ImageButton ID="Btn_Reporte_Cheques" runat="server" ToolTip="Reporte" 
                                                CssClass="Img_Button" TabIndex="1" Visible="false"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                                                onclick="Btn_Reporte_Cheques_Emitidos_Click"/>
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                onclick="Btn_Salir_Click"/>
                                        </td>
                                      <td align="right" style="width:41%;">&nbsp;</td>       
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>   
                <table width="99%" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td style="width:20%;text-align:left;">Banco</td>
                                    <td style="width:30%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Banco" runat="server" Width="90%" TabIndex="1"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%">
                                        <asp:Label ID="Lbl_Fecha_Inicio" runat="server" Text="*Fecha Inicio"></asp:Label>
                                    </td>
                                    <td style="width:30%">
                                        <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="18px" />
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_inicio"/>
                                         <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Inicio" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Inicio" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="Mev_Txt_Fecha_Poliza" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Inicio"
                                            ControlExtender="Mee_Txt_Fecha_Inicio" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Inicio Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                        </td>
                                        <td style="width:20%">
                                            <asp:Label ID="Lbl_Fecha_Final" runat="server" Text="*Fecha Final"></asp:Label>
                                        </td>
                                        <td style="width:30%">
                                            <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="18px" />
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Final" runat="server" 
                                            TargetControlID="Txt_Fecha_Final" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Final" runat="server" 
                                            TargetControlID="Txt_Fecha_Final" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Final"/>
                                         <asp:ImageButton ID="Btn_Fecha_Final" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Final" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Final" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="MaskedEditValidator1" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Final"
                                            ControlExtender="Mee_Txt_Fecha_Final" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Final Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                     <tr>
                        <td  runat="server" id="Tr_Grid_Bancos" colspan="4" style="display:none;">
                               <div>
                                    <table width="100%"  border="0" cellspacing="0">
                                        <tr >
                                            <td Font-Size="XX-Small" style="width:85%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Banco</td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="overflow:auto;height:500px;width:99%;vertical-align:top;border-style:outset;border-color:Silver; position:static" >
                                    <asp:GridView ID="Grid_Cuentas_Movimientos" runat="server" AllowPaging="False"  ShowHeader="false"
                                        AutoGenerateColumns="False" CssClass="GridView_1" OnRowDataBound="Grid_Cuentas_RowDataBound"
                                        DataKeyNames="NOMBRE" GridLines="None" Width="99%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="Img_Btn_Expandir" runat="server"
                                                        ImageUrl="~/paginas/imagenes/paginas/add_up.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                                <ItemStyle HorizontalAlign="Left" Width="2%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="BANCO">
                                                <HeaderStyle HorizontalAlign="Left" Width="80%" />
                                                <ItemStyle HorizontalAlign="Left" Width="80%" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbl_Movimientos" runat="server" 
                                                        Text='<%# Bind("NOMBRE") %>' Visible="false"></asp:Label>
                                                    <asp:Literal ID="Ltr_Inicio" runat="server" 
                                                        Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='display:none;position:static'&gt;&lt;td colspan='4';left-padding:10px;&gt;" />
                                                    <asp:GridView ID="Grid_Movimientos" runat="server" AllowPaging="False" 
                                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="99.9%">
                                                        <Columns>
                                                            <asp:BoundField DataField="Fecha_pago" HeaderText="Fecha Pago" HeaderStyle-Font-Size="X-Small" DataFormatString="{0:dd/MMM/yyyy}">
                                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="10%" />
                                                            </asp:BoundField>                                                            
                                                            <asp:BoundField DataField="No_Solicitud_Pago"  HeaderStyle-Font-Size="X-Small" HeaderText="Solicitud Pago">
                                                                <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="center"  Width="10%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="no_Cheque"  HeaderText="No. Cheque" HeaderStyle-Font-Size="X-Small">
                                                                <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="center"  Width="10%" />
                                                            </asp:BoundField>                                                            
                                                            <asp:BoundField DataField="Monto"  HeaderText="Monto" HeaderStyle-Font-Size="X-Small" DataFormatString="{0:C}">
                                                                <HeaderStyle HorizontalAlign="left" Width="10%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="10%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="BENEFICIARIO_PAGO"  HeaderStyle-Font-Size="X-Small" HeaderText="Beneficiario">
                                                                <HeaderStyle HorizontalAlign="Center" Width="30%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left" Width="30%" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField  Visible="True" HeaderText="Poliza" HeaderStyle-Font-Size="X-Small">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton Font-Size="X-Small"  ID="Btn_Seleccionar_Poliza" runat="server" Text= '<%# Eval("No_Poliza") %>'                                             
                                                                        OnClick="Btn_Poliza_Click" CommandArgument='<%# Eval("MES_ANO") %>' CssClass='<%# Eval("Tipo_Poliza_ID") %>' ForeColor="Blue"  />
                                                                    </ItemTemplate >
                                                                    <HeaderStyle HorizontalAlign="center" />
                                                                    <ItemStyle Font-Size="X-Small"  HorizontalAlign="Center"  Width="10%"/>
                                                                </asp:TemplateField> 
                                                             <asp:BoundField DataField="USUARIO" HeaderStyle-Font-Size="X-Small" HeaderText="Creo"  DataFormatString="{0:C}">
                                                                <HeaderStyle HorizontalAlign="left" Width="20%" />
                                                                <ItemStyle Font-Size="XX-Small"  HorizontalAlign="left"  Width="20%" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                        <FooterStyle CssClass="GridPager" />
                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                        <PagerStyle CssClass="GridPager" />
                                                        <RowStyle CssClass="GridItem" />
                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                    </asp:GridView>
                                                    <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                        <FooterStyle CssClass="GridPager" />
                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                        <PagerStyle CssClass="GridPager" />
                                        <RowStyle CssClass="GridItem" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                    </asp:GridView>
                                </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

