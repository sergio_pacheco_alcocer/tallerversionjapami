﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
using System.Windows.Forms;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sindicatos.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using AjaxControlToolkit;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Cheques_Bancos : System.Web.UI.Page
{
    #region Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

        try
        {
            if (!IsPostBack)
            {
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones   //Limpia los controles del forma
                
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE:         Configuracion_Acceso
    /// DESCRIPCIÓN:    Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS:     No Áplica.
    /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   30/Enero/2012
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Salir);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Buscar);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION :   Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS:     Cadena.- El dato a evaluar si es numerico.
    /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   30/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region Metodos
    #region(Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda
    ///               realizar diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpiar_Controles();
            //Habilitar_Controles("Nuevo");
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Cargar_Grid();
            Btn_Modificar.Visible = false;
            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 30/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            Txt_Busqueda.Text = "";
            //Cmb_Banco.SelectedIndex = 0;
            Txt_Banco_ID.Text = "";
            Txt_Nombre_Banco.Text = "";
            Txt_Folio_Inicial.Text = "";
            Txt_Folio_Final.Text = "";
            Txt_Folio_Actual.Text = "";
            Txt_No_Cuenta.Text = "";
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }
     ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION :   Habilita y Deshabilita los controles de la forma para prepara la página
    ///                 para a siguiente operación
    /// PARAMETROS:     1.- Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                 si es una alta, modificacion
     /// CREO:          Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO:     20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Modificar.CausesValidation = false;
                    Btn_Modificar.Visible = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Configuracion_Acceso("Frm_Ope_Con_Cheques_Bancos.aspx");
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    break;
            }
            //  mensajes de error
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            //Txt_Busqueda.Enabled = Habilitado;
            //Cmb_Banco.Enabled = Habilitado;
            Txt_Folio_Inicial.Enabled = Habilitado;
            Txt_Folio_Final.Enabled = Habilitado;
            Txt_Folio_Actual.Enabled = Habilitado;
            Txt_Nombre_Banco.Enabled = Habilitado;
            Txt_Banco_ID.Enabled = Habilitado;
            Txt_No_Cuenta.Enabled = Habilitado;
            Txt_Busqueda.Enabled = !Habilitado;
        }

        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }
    #endregion

    #region(Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos
    /// DESCRIPCION : Validar que se se encuentre todos los datos para continuar con el proceso
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        Int32 Folio_Inicial = -1; 
        Int32 Folio_Final = -1;
        Int32 Folio_Actual = -1;
        String Espacios_Blanco = "";
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Lbl_Mensaje_Error.Text += Espacios_Blanco + "Es necesario Introducir: <br>";
        Lbl_Mensaje_Error.Visible = true;
        Img_Error.Visible = true;
        try
        {

            //if (Cmb_Banco.SelectedIndex == 0)
            //{
            //    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun banco del combo.<br>";
            //    Datos_Validos = false;
            //}

            if (Txt_Banco_ID.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun registro de la tabla.<br>";
                Datos_Validos = false;
            }
            else
            {
                if (Txt_Folio_Inicial.Text == "")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el número del folio inicial del cheque.<br>";
                    Datos_Validos = false;
                }
                else
                    Folio_Inicial = Convert.ToInt32(Txt_Folio_Inicial.Text);


                if (Txt_Folio_Final.Text == "")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el número del folio final del cheque.<br>";
                    Datos_Validos = false;
                }
                else
                    Folio_Final = Convert.ToInt32(Txt_Folio_Final.Text);

                if ((Folio_Inicial != -1) && (Folio_Final != -1))
                {
                    if (Folio_Final < Folio_Inicial)
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + "*El folio final debe ser mayor que el inicial.<br>";
                        Datos_Validos = false;
                    }
                }

                if (Txt_Folio_Actual.Text == "")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el número del folio actual del cheque.<br>";
                    Datos_Validos = false;
                }
                else
                    Folio_Actual=Convert.ToInt32(Txt_Folio_Actual.Text);

                if ((Folio_Inicial != -1) && (Folio_Actual != -1))
                {
                    if (Folio_Actual < Folio_Inicial)
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + "*El folio Actual debe ser mayor que al inicial.<br>";
                        Datos_Validos = false;
                    }
                }
            }
        }
        catch
        {
        }
        return Datos_Validos;
    }

    #endregion

    #region (Consultas)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Grid
    /// DESCRIPCION : Carga la informacion en el combo banco
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 30/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Grid()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Cls_Ope_Con_Cheques_Bancos_Negocio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Cat_Nom_Bancos_Negocio Rs_Bancos = new Cls_Cat_Nom_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Dt_Consulta = Rs_Bancos.Consulta_Bancos();

            DataView Dv_Ordenar = new DataView(Dt_Consulta);
            Dv_Ordenar.Sort = "NOMBRE, NO_CUENTA";
            Dt_Consulta = Dv_Ordenar.ToTable();


            Session["Grid_Bancos"] = Dt_Consulta;
            Grid_Bancos.Columns[1].Visible = true;
            Grid_Bancos.DataSource = (DataTable)Session["Grid_Bancos"];
            Grid_Bancos.DataBind();
            Grid_Bancos.Columns[1].Visible = false;
            Grid_Bancos.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Gastos por Comprobar" + ex.Message.ToString(), ex);
        }
    } 

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Grid_Banco
    /// DESCRIPCION : Carga la informacion en el combo banco
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 30/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Grid_Banco()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Cls_Ope_Con_Cheques_Bancos_Negocio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.P_Nombre_Banco = Txt_Busqueda.Text.ToUpper();
            Dt_Consulta = Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.Consultar_Bancos_Like();
            Session["Grid_Bancos"] = Dt_Consulta;
            Grid_Bancos.Columns[1].Visible = true;
            Grid_Bancos.DataSource = (DataTable)Session["Grid_Bancos"];
            Grid_Bancos.DataBind();
            Grid_Bancos.Columns[1].Visible = false;
            Grid_Bancos.SelectedIndex = -1;
            //Cmb_Banco.Items.Clear();
            //Cmb_Banco.DataSource = Dt_Consulta;
            //Cmb_Banco.DataValueField = Cat_Nom_Bancos.Campo_Banco_ID;
            //Cmb_Banco.DataTextField = Cat_Nom_Bancos.Campo_Nombre;
            //Cmb_Banco.DataBind();
            //Cmb_Banco.Items.Insert(0, "< SELECCIONE BANCO >");
            //Cmb_Banco.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception("Gastos por Comprobar" + ex.Message.ToString(), ex);
        }
    } 
    #endregion

    #region (Operaciones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Modificar_Banco
    ///DESCRIPCIÓN: Pasa los elementos a la capa de negocios
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  30/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Banco()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Modificar_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        try
        {
            Rs_Modificar_Banco.P_Folio_Inicial = Txt_Folio_Inicial.Text;
            Rs_Modificar_Banco.P_Folio_Final = Txt_Folio_Final.Text;
            Rs_Modificar_Banco.P_Folio_Actual = Txt_Folio_Actual.Text;
            Rs_Modificar_Banco.P_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Banco.P_Banco_ID = Txt_Banco_ID.Text;
            Rs_Modificar_Banco.Modificar_Folio_Banco();

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion

    #endregion

    #region Eventos
    
    #region (Botones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: realizara una modificaion
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  20/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                if (Txt_Banco_ID.Text != "")
                    Habilitar_Controles("Modificar");

                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "*Seleccione algun registro de la tabla.<br>";
                }
            }
            else
            {
                if (Validar_Datos())
                {
                    Modificar_Banco();
                    Div_Grid.Style.Value = "overflow:auto;height:200px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:block";
                    Div_Detalles.Style.Value = "display:none";
                    Inicializa_Controles();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar
    ///DESCRIPCIÓN: Busca los bancos
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  15/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Cargar_Grid_Banco();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Quitar_Click
    ///DESCRIPCIÓN: quitara un registros del grid
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  31/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Quitar_Click(object sender, EventArgs e)
    {
        try
        {
            
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operacion actual que se este realizando
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  30/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        //Div_Grid.Style.Value = "overflow:auto;height:200px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:block"";
        //            Div_Detalles.Style.Value = "display:none";
        if ((Btn_Salir.ToolTip == "Cancelar") || (Div_Grid.Style.Value == "display:none"))
        {
            Div_Grid.Style.Value = "overflow:auto;height:200px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:block";
            Div_Detalles.Style.Value = "display:none";
            Inicializa_Controles();
        }
        
        else
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
    }
    #endregion

    #endregion

    #region Grids
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Bancos_SelectedIndexChanged
    /// DESCRIPCION : Consulta los datos de los movimientos seleccionada por el usuario
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 31/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Bancos_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            Grid_Bancos.Columns[1].Visible = true;
            GridViewRow selectedRow = Grid_Bancos.Rows[Grid_Bancos.SelectedIndex];
            Grid_Bancos.Columns[1].Visible = false;

            Txt_Banco_ID.Text = HttpUtility.HtmlDecode(selectedRow.Cells[1].Text).ToString();
            Txt_Nombre_Banco.Text = HttpUtility.HtmlDecode(selectedRow.Cells[2].Text).ToString();
            Txt_No_Cuenta.Text = HttpUtility.HtmlDecode(selectedRow.Cells[3].Text).ToString();
            Txt_Folio_Inicial.Text = HttpUtility.HtmlDecode(selectedRow.Cells[4].Text).ToString();
            Txt_Folio_Final.Text = HttpUtility.HtmlDecode(selectedRow.Cells[5].Text).ToString();
            Txt_Folio_Actual.Text = HttpUtility.HtmlDecode(selectedRow.Cells[6].Text).ToString();
            //Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Estatus));

            Div_Grid.Style.Value = "display:none";
            Div_Detalles.Style.Value = "display:block";

            Txt_Busqueda.Enabled = false;

            Habilitar_Controles("Modificar");
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
}
