﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Bancos_Nomina.Negocio;
using JAPAMI.Reporte_Cheques.Negocio;
using JAPAMI.Proveedores.Negocios;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;

public partial class paginas_Contabilidad_Frm_Rpt_Con_Cheques : System.Web.UI.Page
{
    #region (Variables Locales)
        private const String P_Dt_Cheques = "Dt_Cheques";
        private static Boolean buscado = false;
    #endregion

    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Estado_Inicial();
            }
            else
            {
                Mostrar_Error("", false);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Page_Load) " + ex.Message, true);
        }
    }
    #endregion

    #region (Metodos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Mostrar_Error
    ///DESCRIPCION:             Mostrar el mensaje de error
    ///PARAMETROS:              1. Mensaje: Cadena de texto con el mensaje de error a mostrar
    ///                         2. Mostrar: Booleano que indica si se va a mostrar el mensaje de error.
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              17/Abril/2012 17:39
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Mostrar_Error(String Mensaje, Boolean Mostrar)
    {
        try
        {
            Lbl_Informacion.Text = Mensaje;
            Div_Contenedor_Msj_Error.Visible = Mostrar;
        }
        catch (Exception ex)
        {
            Lbl_Informacion.Text = "Error: (Mostrar_Error)" + ex.ToString();
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Estado_Inicial
    ///DESCRIPCION:             Colocar la pagina en un estado inicial para su navegacion
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              17/Abril/2012 17:40
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Estado_Inicial()
    {
        try
        {
            Elimina_Sesiones();
            Llena_Combo_Bancos();

            Lbl_Empleado_Proveedor.Visible = false;
            Cmb_Empleado_Proveedor.Visible = false;
            Btn_Busqueda_Empleado_Proveedor.Visible = false;
            Txt_Busqueda.Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Elimina_Sesiones
    ///DESCRIPCION:             Eliminar las sesiones utilizadas en esta pagina
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              17/Abril/2012 17:42
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Elimina_Sesiones()
    {
        try
        {
            //Eliminar las sesiones
            HttpContext.Current.Session.Remove(P_Dt_Cheques);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Combo_Bancos
    ///DESCRIPCION:             Llenar el combo de los bancos
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              17/Abril/2012 17:58
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Combo_Bancos()
    {
        //Declaracion de variables
        Cls_Cat_Nom_Bancos_Negocio Bancos_Negocio = new Cls_Cat_Nom_Bancos_Negocio(); //Variable para la capa de negocios
        DataTable Dt_Bancos = new DataTable(); //Tabla para el llenado del combo

        try
        {
            //Ejecutar consulta
            Dt_Bancos = Bancos_Negocio.Consulta_Bancos();

            //Llenar el combo de los bancos
            Cmb_Bancos.Items.Clear();
            Cmb_Bancos.DataSource = Dt_Bancos;
            Cmb_Bancos.DataTextField = Cat_Nom_Bancos.Campo_Nombre;
            Cmb_Bancos.DataValueField = Cat_Nom_Bancos.Campo_Banco_ID;
            Cmb_Bancos.DataBind();
            Cmb_Bancos.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Bancos.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Combo_Empleados_Proveedores
    ///DESCRIPCION:             Llenar el combo con los datos de empleado por nombre o numero
    ///                         asi tambien como proveedores por nombre o padron
    ///PARAMETROS:              1. Tipo: Cadena de texto que contiene el tipo (Empleado o Proveedor)
    ///                         2. Busqueda: Cadena de texto que contiene la busqueda para la consulta
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              18/Abril/2012 19:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Combo_Empleados_Proveedores(String Tipo, String Busqueda)
    {
        //Declaracion de variables
        DataTable Dt_Resultado = new DataTable(); //tabla para el llenado del combo
        String Campo_Texto = String.Empty; //variable para el campo del texto
        String Campo_Valor = String.Empty; //Variable para el campo del valor
        Cls_Cat_Empleados_Negocios Empleados_Negocio = new Cls_Cat_Empleados_Negocios(); //variable para la capa de negocios del catalogo de empleados
        Cls_Cat_Com_Proveedores_Negocio Proveedores_Negocio = new Cls_Cat_Com_Proveedores_Negocio(); //variable para la capa de negocios de proveedores

        try
        {
            //Seleccionar el tipo de la consulta
            switch (Tipo)
            {
                case "Empleado":
                    //verificar si la busqueda es numerica
                    if (Cls_Util.EsNumerico(Busqueda) == true)
                    {
                        Empleados_Negocio.P_No_Empleado = String.Format("{0:000000}", int.Parse(Busqueda));
                    }
                    else
                    {
                        Empleados_Negocio.P_Nombre = Busqueda;
                    }
                    
                    //Ejecutar consulta
                    Dt_Resultado = Empleados_Negocio.Consulta_Empleados();

                    //Nombre de los campos
                    Campo_Texto = "Empleado";
                    Campo_Valor = Cat_Empleados.Campo_Empleado_ID;
                    break;

                case "Proveedor":
                    //Verificar si la busqueda es numerica
                    if (Cls_Util.EsNumerico(Busqueda) == true)
                    {
                        Proveedores_Negocio.P_Proveedor_ID = String.Format("{0:0000000000}", int.Parse(Busqueda));
                    }
                    else
                    {
                        Proveedores_Negocio.P_Razon_Social = Busqueda;
                    }

                    //Ejecutar consulta
                    Dt_Resultado = Proveedores_Negocio.Consulta_Avanzada_Proveedor();

                    //Nombres de las columnas
                    Campo_Texto = Cat_Com_Proveedores.Campo_Nombre;
                    Campo_Valor = Cat_Com_Proveedores.Campo_Proveedor_ID;
                    break;

                default:
                    break;
            }

            //Llenar el combo
            Cmb_Empleado_Proveedor.DataSource = Dt_Resultado;
            Cmb_Empleado_Proveedor.DataTextField = Campo_Texto;
            Cmb_Empleado_Proveedor.DataValueField = Campo_Valor;
            Cmb_Empleado_Proveedor.DataBind();
            Cmb_Empleado_Proveedor.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Empleado_Proveedor.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Reporte_Excel
    ///DESCRIPCION:             Llenar un reporte de Excel con el resultado de la consulta
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              19/Abril/2012 12:35
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Reporte_Excel()
    {
        //Declaracion de variables
        DataTable Dt_Cheques = new DataTable(); //tabla para el resultado de la consulta
        int Cont_Elementos = 0; //variable para el contador
        Cls_Rpt_Con_Cheques_Negocio Cheques_Negocio = new Cls_Rpt_Con_Cheques_Negocio(); //Variable para la capa de negocios
        String Ruta_Archivo = String.Empty; //variable para la ruta del archivo
        String Nombre_Archivo = "Reporte_Cheques.xls"; //variable para el nombre del archivo
        Workbook book = new Workbook(); //Variable para el libro
        WorksheetStyle style; //Variable para el estilo
        Worksheet sheet; //variable para la hoja
        WorksheetRow row; //variable para el renglon
        WorksheetCell cell; //Variable para la celda
        String Script_js = String.Empty; //variable para el Script de javascript
        String Encabezado = String.Empty; //variable para el encabezado del reporte

        try
        {
            //Asignar parametros para la consulta
            if (Cmb_Bancos.SelectedIndex > 0)
            {
                Cheques_Negocio.P_Banco_ID = Cmb_Bancos.SelectedItem.Value;
                Encabezado = "Banco: " + Cmb_Bancos.SelectedItem.Text + "\r\n";
            }

            if (Cmb_Estatus.SelectedIndex > 0)
            {
                Cheques_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                Encabezado += "Estatus: " + Cmb_Estatus.SelectedItem.Text + "\r\n";
            }

            if (String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) == false)
            {
                Cheques_Negocio.P_Fecha_Inicio = Convert.ToDateTime(Txt_Fecha_Inicial.Text);

                if (String.IsNullOrEmpty(Txt_Fecha_Final.Text) == false)
                {
                    Cheques_Negocio.P_Fecha_Fin = Convert.ToDateTime(Txt_Fecha_Final.Text);
                    Encabezado += "Del " + Txt_Fecha_Inicial.Text + " al " + Txt_Fecha_Final.Text + "\r\n";
                }
                else
                {
                    Encabezado += "A partir del " + Txt_Fecha_Inicial.Text + "\r\n";
                }
            }

            Cheques_Negocio.P_Tipo = Cmb_Tipos.SelectedItem.Value;
            Encabezado += "Tipo Consulta: " + Cmb_Tipos.SelectedItem.Value + "\r\n"; 
            //Verificar el tipo de la consulta
            if(buscado){
            switch (Cmb_Tipos.SelectedItem.Value)
            {
                case "Empleado":
                    Cheques_Negocio.P_Empleado_ID = Cmb_Empleado_Proveedor.SelectedItem.Value.ToString();
                    //Cheques_Negocio.P_Empleado_ID = Cmb_Tipos.SelectedItem.Value.ToString();
                    break;

                case "Proveedor":
                    Cheques_Negocio.P_Proveedor_ID = Cmb_Empleado_Proveedor.SelectedItem.Value.ToString();
                    //Cheques_Negocio.P_Proveedor_ID = Cmb_Tipos.SelectedItem.Value.ToString();
                    break;

                default:
                    break;
            }
            }
            //Ejecutar consulta
            Dt_Cheques = Cheques_Negocio.Consulta_Cheques();

            //Verificar si la consulta arrojo resultados
            if (Dt_Cheques.Rows.Count > 0)
            {
                //Asignar la ruta del archivo
                Ruta_Archivo = HttpContext.Current.Server.MapPath("~") + "\\Exportaciones\\" + Nombre_Archivo;

                // Especificar qué hoja debe ser abierto y el tamaño de la ventana por defecto
                book.ExcelWorkbook.ActiveSheetIndex = 0;
                book.ExcelWorkbook.WindowTopX = 100;
                book.ExcelWorkbook.WindowTopY = 200;
                book.ExcelWorkbook.WindowHeight = 7000;
                book.ExcelWorkbook.WindowWidth = 8000;

                // Propiedades del documento
                book.Properties.Author = "CONTEL";
                book.Properties.Title = "REPORTE";
                book.Properties.Created = DateTime.Now;

                // Se agrega estilo al libro
                style = book.Styles.Add("HeaderStyle");
                style.Font.FontName = "Tahoma";
                style.Font.Size = 13;
                style.Font.Bold = true;
                style.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                style.Font.Color = "White";
                style.Interior.Color = "Blue";
                style.Interior.Pattern = StyleInteriorPattern.DiagCross;

                // Se Crea el estilo a usar
                style = book.Styles.Add("Default");
                style.Font.FontName = "Tahoma";
                style.Font.Size = 10;

                //Asignar el nombre a la hoja
                sheet = book.Worksheets.Add("Hoja1");

                //Agregar renglon para el encabezado
                row = sheet.Table.Rows.Add();
                row.Index = 0;//Para saltarse Filas

                // Se agrega el encabezado
                row.Cells.Add(new WorksheetCell(Encabezado, "HeaderStyle"));

                //Agregar renglones para separar
                row = sheet.Table.Rows.Add();
                row = sheet.Table.Rows.Add();
                row = sheet.Table.Rows.Add();

                //Encabezado de la tabla
                row.Cells.Add(new WorksheetCell("No Pago", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Fecha Pago", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Concepto Pago ", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("No Solicitud Pago", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Fecha Solicitud", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Prefijo", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Mes/Año", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Fecha Poliza", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Tipo Poliza", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Banco", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("No Cheque", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Estatus", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Monto", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell(Cmb_Tipos.SelectedItem.Value, "HeaderStyle")); //Columna del empleado o del proveedor
                
                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Cheques.Rows.Count; Cont_Elementos++)
                {
                    //Agregar renglon
                    row = sheet.Table.Rows.Add();

                    //Agregar las columnas
                    if (Dt_Cheques.Rows[Cont_Elementos]["No_Pago"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("0", DataType.Number));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos]["No_Pago"].ToString().Trim(), DataType.Number));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["Fecha_Pago"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(String.Format("{0:dd/MMM/yyyy}", Dt_Cheques.Rows[Cont_Elementos]["Fecha_Pago"]), DataType.String));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["Concepto"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos]["Concepto"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["No_Solicitud_Pago"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("0", DataType.Number));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos]["No_Solicitud_Pago"].ToString().Trim(), DataType.Number));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["Fecha_Solicitud"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(String.Format("{0:dd/MMM/yyyy}", Dt_Cheques.Rows[Cont_Elementos]["Fecha_Solicitud"]), DataType.String));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["Prefijo"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos]["Prefijo"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["Mes_Ano"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos]["Mes_Ano"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["Fecha_Poliza"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(String.Format("{0:dd/MMM/yyyy}", Dt_Cheques.Rows[Cont_Elementos]["Fecha_Poliza"]), DataType.String));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["Tipo_Poliza"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos]["Tipo_Poliza"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["Banco"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos]["Banco"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["No_Cheque"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos]["No_Cheque"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["Estatus"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos]["Estatus"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos]["Monto"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("0", DataType.Number));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos]["Monto"].ToString().Trim(), DataType.Number));
                    }

                    if (Dt_Cheques.Rows[Cont_Elementos][Cmb_Tipos.SelectedItem.Value] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Cheques.Rows[Cont_Elementos][Cmb_Tipos.SelectedItem.Value].ToString().Trim(), DataType.String));
                    }
                }

                // Se Guarda el archivo                
                book.Save(Ruta_Archivo);
                string script = @"<script type='text/javascript'>alert('Registros Exportados a Excel');</script>";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);

                Mostrar_Reporte(Nombre_Archivo, "Excel");
            }
            else
            {
                Mostrar_Error("La consulta no arroj&oacute; resultados.", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Exportaciones/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Llena_Reporte_PDF
    /// DESCRIPCION:         Llenar el PDF con el resultado de la consulta
    /// PARÁMETROS:          
    /// USUARIO CREO:        Noe Mosqueda Valadez
    /// FECHA CREO:          19/Abril/2012 18:34
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACION:  
    /// *************************************************************************************
    private void Llena_Reporte_PDF()
    {
        //Declaracion de variables
        DataTable Dt_Cheques = new DataTable(); //tabla para el resultado de la consulta
        int Cont_Elementos = 0; //variable para el contador
        Cls_Rpt_Con_Cheques_Negocio Cheques_Negocio = new Cls_Rpt_Con_Cheques_Negocio(); //Variable para la capa de negocios
        String Encabezado = String.Empty; //Variable para el encabezado
        DataTable Dt_Cabecera = new DataTable(); //Tabla para la cabecera
        DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles
        DataSet Ds_Reporte = new DataSet(); //Dataset para el reporte
        Ds_Rpt_Con_Cheques Ds_Rpt_Con_Cheques_src = new Ds_Rpt_Con_Cheques(); //Dataset archivo para el llenado del reporte
        DataRow Renglon; //Renglon para el llenado de las tablas
        int Cont_Columnas = 0; //variable para el contador de las columnas
        String Valida = "";
        try
        {
            //Asignar parametros para la consulta
            if (Cmb_Bancos.SelectedIndex > 0)
            {
                Cheques_Negocio.P_Banco_ID = Cmb_Bancos.SelectedItem.Value;
                Encabezado = "Banco: " + Cmb_Bancos.SelectedItem.Text + "\r\n";
            }

            if (Cmb_Estatus.SelectedIndex > 0)
            {
                Cheques_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                Encabezado += "Estatus: " + Cmb_Estatus.SelectedItem.Text + "\r\n";
            }

            if (String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) == false)
            {
                Cheques_Negocio.P_Fecha_Inicio = Convert.ToDateTime(Txt_Fecha_Inicial.Text);

                if (String.IsNullOrEmpty(Txt_Fecha_Final.Text) == false)
                {
                    Cheques_Negocio.P_Fecha_Fin = Convert.ToDateTime(Txt_Fecha_Final.Text);
                    Encabezado += "Del " + Txt_Fecha_Inicial.Text + " al " + Txt_Fecha_Final.Text + "\r\n";
                }
                else
                {
                    Encabezado += "A partir del " + Txt_Fecha_Inicial.Text + "\r\n";
                }
            }

            Cheques_Negocio.P_Tipo = Cmb_Tipos.SelectedItem.Value;
            Encabezado += "Tipo Consulta: " + Cmb_Tipos.SelectedItem.Value + "\r\n";
            //Verificar el tipo de la consulta
            switch (Cmb_Tipos.SelectedItem.Value)
            {
                case "Empleado":
                    if (Cmb_Empleado_Proveedor.SelectedItem != null)
                        Cheques_Negocio.P_Empleado_ID = Cmb_Empleado_Proveedor.SelectedItem.Value;
                    else
                        Valida = "Selecicone al Empleado.<br/>";
                    break;

                case "Proveedor":
                    if (Cmb_Empleado_Proveedor.SelectedItem != null)
                        Cheques_Negocio.P_Proveedor_ID = Cmb_Empleado_Proveedor.SelectedItem.Value;
                    else
                        Valida = "Selecicone al Proveedor.<br/>";
                    break;

                default:
                    break;
            }

            //Ejecutar consulta
            Dt_Cheques = Cheques_Negocio.Consulta_Cheques();

            //Verifica que no falte el proovedor o el empleado si aplica
            if (String.IsNullOrEmpty(Valida))
            {
                //verificar si la consulta arrojo resultados
                if (Dt_Cheques.Rows.Count > 0)
                {
                    //Colocar las columnas a la tabla de la cabecera
                    Dt_Cabecera.Columns.Add("ID", typeof(int));
                    Dt_Cabecera.Columns.Add("Datos_Filtro", typeof(String));
                    Dt_Cabecera.Columns.Add("Tipo", typeof(String));

                    //Colocar las columnas en la tabla de los datelles
                    Dt_Detalles.Columns.Add("ID", typeof(int));
                    Dt_Detalles.Columns.Add("No_Pago", typeof(int));
                    Dt_Detalles.Columns.Add("Fecha_Pago", typeof(DateTime));
                    Dt_Detalles.Columns.Add("No_Solicitud_Pago", typeof(int));
                    Dt_Detalles.Columns.Add("Fecha_Solicitud", typeof(DateTime));
                    Dt_Detalles.Columns.Add("Prefijo", typeof(String));
                    Dt_Detalles.Columns.Add("Mes_Ano", typeof(String));
                    Dt_Detalles.Columns.Add("Fecha_Poliza", typeof(DateTime));
                    Dt_Detalles.Columns.Add("Tipo_Poliza", typeof(String));
                    Dt_Detalles.Columns.Add("Banco", typeof(String));
                    Dt_Detalles.Columns.Add("No_Cheque", typeof(String));
                    Dt_Detalles.Columns.Add("Estatus", typeof(String));
                    Dt_Detalles.Columns.Add("Monto", typeof(Double));
                    Dt_Detalles.Columns.Add("Proveedor", typeof(String));
                    Dt_Detalles.Columns.Add("Empleado", typeof(String));
                    Dt_Detalles.Columns.Add("No_Empleado", typeof(String));
                    Dt_Detalles.Columns.Add("Concepto", typeof(String));


                    //Instanciar el renglon de la cabecera
                    Renglon = Dt_Cabecera.NewRow();

                    //Llenar y colocar el renglon de la cabecera
                    Renglon["ID"] = 1;
                    Renglon["Datos_Filtro"] = Encabezado;
                    Renglon["Tipo"] = Cmb_Tipos.SelectedItem.Value;
                    Dt_Cabecera.Rows.Add(Renglon);

                    //Ciclo para el barrido de la tabla de los productos
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Cheques.Rows.Count; Cont_Elementos++)
                    {
                        //Instanciar el renglon de los detalles
                        Renglon = Dt_Detalles.NewRow();

                        //Colocar el ID
                        Renglon["ID"] = 1;

                        //Ciclo para el llenado del resto del renglon
                        for (Cont_Columnas = 0; Cont_Columnas < Dt_Cheques.Columns.Count; Cont_Columnas++)
                        {
                            Renglon[Cont_Columnas + 1] = Dt_Cheques.Rows[Cont_Elementos][Cont_Columnas];
                        }

                        //Colocar el renglon en la tabla
                        Dt_Detalles.Rows.Add(Renglon);
                    }

                    //Colocar las tablas en el dataset
                    Ds_Reporte.Tables.Add(Dt_Cabecera);
                    Ds_Reporte.Tables.Add(Dt_Detalles);

                    //Generar el reporte
                    Generar_Reporte(Ds_Reporte, Ds_Rpt_Con_Cheques_src, "Rpt_Con_Cheques.rpt", "Reporte_Cheques.pdf");
                }
                else
                {
                    Mostrar_Error("La consulta no arroj&oacute; resultados.", true);
                }
            }
            else
                Mostrar_Error(Valida, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Nombre_PDF)
    {

        ReportDocument Reporte = new ReportDocument();
        DataRow Renglon; //Renglon para el llenado de las tablas
        int Cont_Elementos; //Variable para el contador

        //Ciclo para el barrido de la cabecera
        for (Cont_Elementos = 0; Cont_Elementos < Data_Set_Consulta_DB.Tables[0].Rows.Count; Cont_Elementos++)
        {
            //Instanciar el renglon
            Renglon = Data_Set_Consulta_DB.Tables[0].Rows[Cont_Elementos];

            //Importar el renglon
            Ds_Reporte.Tables[0].ImportRow(Renglon);
        }

        //Ciclo para el barrido de los detalles
        for (Cont_Elementos = 0; Cont_Elementos < Data_Set_Consulta_DB.Tables[1].Rows.Count; Cont_Elementos++)
        {
            //Instanciar el renglon
            Renglon = Data_Set_Consulta_DB.Tables[1].Rows[Cont_Elementos];

            //Importar el renglon
            Ds_Reporte.Tables[1].ImportRow(Renglon);
        }

        String File_Path = Server.MapPath("../Rpt/Contabilidad/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        //Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        String Ruta = "../../Reporte/" + Nombre_PDF;
        Mostrar_Reporte(Nombre_PDF, "PDF");
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
    }

    #endregion

    #region (Grid)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Grid_Cheques
    ///DESCRIPCION:             Llenar el grid de los cheques
    ///PARAMETROS:              Pagina: Entero que contiene el numero de la pagina
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              19/Abril/2012 10:53
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Grid_Cheques(int Pagina)
    {
        //Declaracion de variables
        DataTable Dt_Cheques = new DataTable(); //Tabla para el resultado de la consulta
        Cls_Rpt_Con_Cheques_Negocio Cheques_Negocio = new Cls_Rpt_Con_Cheques_Negocio(); //variable para la capa de negocios 
        String Valida = "";
        try
        {
            //Verificar si existe la variable de sesion
            if (HttpContext.Current.Session[P_Dt_Cheques] != null)
            {
                //COlocar l variable de sesion en la tabla
                Dt_Cheques = ((DataTable)HttpContext.Current.Session[P_Dt_Cheques]);
            }
            else
            {
                //Asignar parametros para la consulta
                if (Cmb_Bancos.SelectedIndex > 0)
                {
                    Cheques_Negocio.P_Banco_ID = Cmb_Bancos.SelectedItem.Value;
                }

                if (Cmb_Estatus.SelectedIndex > 0)
                {
                    Cheques_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                }

                if (String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) == false)
                {
                    Cheques_Negocio.P_Fecha_Inicio = Convert.ToDateTime(Txt_Fecha_Inicial.Text);

                    if (String.IsNullOrEmpty(Txt_Fecha_Final.Text) == false)
                    {
                        Cheques_Negocio.P_Fecha_Fin = Convert.ToDateTime(Txt_Fecha_Final.Text);
                    }
                }

                Cheques_Negocio.P_Tipo = Cmb_Tipos.SelectedItem.Value;

                //Verificar el tipo de la consulta
                switch (Cmb_Tipos.SelectedItem.Value)
                {
                    case "Empleado":
                        //verificar si hay seleccionado un empleado
                        if (Cmb_Empleado_Proveedor.Items.Count > 0)
                        {
                            if (Cmb_Empleado_Proveedor.SelectedItem != null)
                                Cheques_Negocio.P_Empleado_ID = Cmb_Empleado_Proveedor.SelectedItem.Value;
                            else
                                Valida = "Selecicone al Empleado.<br/>";
                        }
                        break;

                    case "Proveedor":
                        //verificar si esta seleccionado un proveedor
                        if (Cmb_Empleado_Proveedor.Items.Count > 0)
                        {
                            if (Cmb_Empleado_Proveedor.SelectedItem != null)
                                Cheques_Negocio.P_Proveedor_ID = Cmb_Empleado_Proveedor.SelectedItem.Value;
                            else
                                Valida = "Selecicone al Proveedor.<br/>";
                        }
                        break;

                    default:
                        break;
                }

                //Ejecutar consulta
                Dt_Cheques = Cheques_Negocio.Consulta_Cheques();
            }
            //Verifica que no falte el proovedor o el empleado si aplica
            if (String.IsNullOrEmpty(Valida))
            {
                //verificar si la consulta arrojo resultados
                if (Dt_Cheques.Rows.Count > 0)
                {
                    //llenar el grid
                    Grid_Cheques.DataSource = Dt_Cheques;

                    //verificar el tipo
                    if (Cmb_Tipos.SelectedItem.Value == "Empleado")
                    {
                        Grid_Cheques.Columns[13].Visible = false;
                        Grid_Cheques.Columns[14].Visible = true;
                    }
                    else
                    {
                        Grid_Cheques.Columns[13].Visible = true;
                        Grid_Cheques.Columns[14].Visible = false;
                    }

                    //verificar si hay pagina
                    if (Pagina > -1)
                    {
                        Grid_Cheques.PageIndex = Pagina;
                    }

                    Grid_Cheques.DataBind();

                    //Colocar la tabla en una variable de sesion 
                    HttpContext.Current.Session[P_Dt_Cheques] = Dt_Cheques;
                }
                else
                {
                    Mostrar_Error("La consulta no arroj&oacute; resultados.", true);
                }
            }
            else
                Mostrar_Error(Valida, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    protected void Grid_Cheques_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Llena_Grid_Cheques(e.NewPageIndex);
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Grid_Cheques_PageIndexChanging) " + ex.Message, true);
        }
    }

    #endregion

    #region (Eventos)
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Elimina_Sesiones();
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Salir_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Busqueda_Empleado_Proveedor_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //verificar si se ha escrito algo en el criterio de busqueda
            if (String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()) == false)
            {
                Llena_Combo_Empleados_Proveedores(Cmb_Tipos.SelectedItem.Value, Txt_Busqueda.Text.Trim());
                buscado = true;
            }
            else
            {
                Mostrar_Error("Favor de proporcionar un criterio de b&uacute;squeda", true);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Busqueda_Empleado_Proveedor_Click) " + ex.Message, true);
        }
    }

    protected void Cmb_Tipos_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //limpiar campos
            Txt_Busqueda.Text = "";
            Cmb_Empleado_Proveedor.Items.Clear();

            //verificar el indice
            switch (Cmb_Tipos.SelectedIndex)
            {
                case 1:
                    Lbl_Empleado_Proveedor.Visible = true;
                    Cmb_Empleado_Proveedor.Visible = true;
                    Btn_Busqueda_Empleado_Proveedor.Visible = true;
                    Txt_Busqueda.Visible = true;
                    Lbl_Empleado_Proveedor.Text = "No Empleado/Nombre";
                    break;

                case 2:
                    Lbl_Empleado_Proveedor.Visible = true;
                    Cmb_Empleado_Proveedor.Visible = true;
                    Btn_Busqueda_Empleado_Proveedor.Visible = true;
                    Txt_Busqueda.Visible = true;
                    Lbl_Empleado_Proveedor.Text = "Padr&oacute;n/Nombre";
                    break;

                default:
                    Lbl_Empleado_Proveedor.Visible = false;
                    Cmb_Empleado_Proveedor.Visible = false;
                    Btn_Busqueda_Empleado_Proveedor.Visible = false;
                    Txt_Busqueda.Visible = false;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Reporte", "alert('Tipo : " + Cmb_Tipos.SelectedItem.ToString() + "');", true);
                    break;
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Cmb_Tipos_SelectedIndexChanged) " + ex.Message, true);
        }
    }

    protected void Btn_Consultar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //verificar si se ha seleccionado un tipo
            if (Cmb_Tipos.SelectedIndex > 0)
            {
                Elimina_Sesiones();
                Llena_Grid_Cheques(-1);
            }
            else
            {
                Mostrar_Error("Favor de seleccionar el tipo de consulta.", true);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Consultar_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //verificar si se ha seleccionado un tipo
            if (Cmb_Tipos.SelectedIndex > 0)
            {
                Llena_Reporte_PDF();
            }
            else
            {
                Mostrar_Error("Favor de seleccionar el tipo de consulta.", true);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Imprimir_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Imprimir_Excel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //verificar si se ha seleccionado un tipo
            if (Cmb_Tipos.SelectedIndex > 0)
            {
                Llena_Reporte_Excel();
            }
            else
            {
                Mostrar_Error("Favor de seleccionar el tipo de consulta.", true);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Imprimir_Excel_Click) " + ex.Message, true);
        }
    }

    #endregion
}