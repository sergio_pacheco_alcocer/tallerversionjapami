<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Con_Cuentas_Contables.aspx.cs" Inherits="paginas_Contabilidad_Frm_Cat_Con_Cuentas_Contables" Title="Cat�logo de Cuentas Contables" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%--    <style type="text/css">
        .style1
        {
            width: 426px;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <p>
        &nbsp;</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
 <script type="text/javascript" language="javascript">
 function pageLoad() { 
        Contar_Caracteres();
    }
    function Contar_Caracteres(){
        $('textarea[id$=Txt_Comentarios_Cuenta_Contable]').keyup(function() {
            var Caracteres =  $(this).val().length;
            
            if (Caracteres > 250) {
                this.value = this.value.substring(0, 250);
                $(this).css("background-color", "Yellow");
                $(this).css("color", "Red");
            }else{
                $(this).css("background-color", "White");
                $(this).css("color", "Black");
            }
            
            $('#Txt_Comentarios_Cuenta_Contable').text('Car�cteres Ingresados [ ' + Caracteres + ' ]/[ 250 ]');
        });
    }
</script>
    <asp:UpdateProgress ID="Uprg_Reporte" runat="server" 
        AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
        <ProgressTemplate>
            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
            <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <cc1:ToolkitScriptManager ID="ScriptManager_Cuentas_Contables" runat="server"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <div id="Div_Cuentas_Contables" >
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Cat&aacute;logo de Cuentas Contables</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>               
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" 
                                TabIndex="1" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                onclick="Btn_Nuevo_Click"/>
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" 
                                TabIndex="2" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                                onclick="Btn_Modificar_Click"/>
                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" CssClass="Img_Button" 
                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                OnClientClick="return confirm('�Est� seguro de eliminar la Cuenta Contable seleccionada?');" 
                                onclick="Btn_Eliminar_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="4" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                onclick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%">B&uacute;squeda
                            <asp:TextBox ID="Txt_Busqueda_Cuenta_Contable" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar por Descripci�n"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Cuenta_Contable" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Ingrese Cuenta � Descripci�n>" TargetControlID="Txt_Busqueda_Cuenta_Contable" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Cuenta_Contable" runat="server" 
                                TargetControlID="Txt_Busqueda_Cuenta_Contable" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                ValidChars="������������-. ">
                            </cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="Btn_Buscar_Descripcion_Cuenta_Contable" runat="server" 
                                ToolTip="Consultar" TabIndex="6" 
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                onclick="Btn_Buscar_Descripcion_Cuenta_Contable_Click"/>
                        </td> 
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td width="15%">&nbsp;</td>
                        <td width="30%"></td>
                        <td width="5%"></td>
                        <td width="15%"></td>
                        <td width="30%"></td>
                    <%--<td width="15%"></td>
                        <td width="10%"></td>--%>
                    </tr>
                    <tr>
                        <td align="left">Cuenta Contable ID</td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Cuenta_Contable_ID" runat="server" ReadOnly="True" Width="77%"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td align="left">*Naturaleza</td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Naturaleza" runat="server" Width="89%">
                                <asp:ListItem>&lt;Seleccione&gt;</asp:ListItem>
                                <asp:ListItem>DEUDOR</asp:ListItem>
                                <asp:ListItem>ACREEDOR</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">*Descripci&oacute;n</td>
                        <td colspan="4" align="left">
                            <asp:TextBox ID="Txt_Descripcion_Cuenta_Contable" runat="server" MaxLength="100" TabIndex="7" Width="95.5%"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Descripcion_Cuenta_Contable" runat="server" TargetControlID="Txt_Descripcion_Cuenta_Contable"
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="������������. ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">*Cuenta Padre</td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Busqueda_Cuenta_Contable_Padre" runat="server"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="Txt_Busqueda_Cuenta_Contable_Padre_FTE" runat="server" 
                                TargetControlID="Txt_Busqueda_Cuenta_Contable_Padre" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                ValidChars="������������-. "></cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="Img_Btn_Busqueda_Cuenta_Contable_Padre" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                ToolTip="Buscar Cuenta Contable Padre" 
                                onclick="Img_Btn_Busqueda_Cuenta_Contable_Padre_Click" />
                        </td>
                        <td align="left" colspan="3"><asp:DropDownList ID="Cmb_Cuenta_Contable_Padre" runat="server" Width="94%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">*Nivel</td>
                        <td style="text-align:left;" colspan="2" align="left">
                            <asp:DropDownList ID="Cmb_Nivel_Cuenta_Contable" runat="server" TabIndex="8" Width="77%"/>
                        </td>
                        <td align="left">*Cuenta</td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Cuenta_Contable" runat="server" MaxLength="20" TabIndex="9" ontextchanged="Txt_Cuenta_Contable_TextChanged" Width="88%" AutoPostBack ="true"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="Txt_Cuenta_Contable"
                                FilterType="Custom" ValidChars="1234567890-"></cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        
                       <%-- <td>Tipo</td>
                        <td colspan="2">
                            <asp:DropDownList ID="Cmb_Tipo_Cuenta_Contable" runat ="server" Width="77%" TabIndex="11">
                                <asp:ListItem>&lt; Seleccione &gt;</asp:ListItem>
                                <asp:ListItem>BALANCE</asp:ListItem>
                                <asp:ListItem>RESULTADO</asp:ListItem>
                            </asp:DropDownList>
                        </td>--%>
                        <td align="left">*Cuenta Afectable</td>  
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Cuenta_Detalle" runat="server" TabIndex="10" Width="90%">
                                <asp:ListItem>&lt; Seleccione &gt;</asp:ListItem>
                                <asp:ListItem>NO</asp:ListItem>
                                <asp:ListItem>SI</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr style="display:none;">
                        <td align="left">Tipo Presupuestal</td>
                        <td colspan="2" align="left">
                            <asp:DropDownList ID="Cmb_Tipo_Presupuestal" runat ="server" Width="77%" TabIndex="11"
                                AutoPostBack="true" OnSelectedIndexChanged="Cmb_Tipo_Presupuestal_OnSelectedIndexChanged">
                                <asp:ListItem>&lt; Seleccione &gt;</asp:ListItem>
                                <asp:ListItem>EGRESOS</asp:ListItem>
                                <asp:ListItem>INGRESOS</asp:ListItem>
                                <asp:ListItem>OTROS</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                   <tr style="display:none;">
                        <td align="left">Partida Presupuestal</td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Cuenta_Presupuestal" runat="server" AutoPostBack="true" Width="90%"
                                MaxLength="20" ontextchanged="Txt_Cuenta_Presupuestal_TextChanged" TabIndex="13"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="Txt_Cuenta_Presupuestal_FilteredTextBoxExtender" 
                                runat="server" FilterType="Custom" TargetControlID="Txt_Cuenta_Presupuestal" 
                                ValidChars="1234567890">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Cuenta_Presupuestal" runat="server" 
                                TabIndex="14" AutoPostBack="true" Width="94%"
                                onselectedindexchanged="Cmb_Cuenta_Presupuestal_SelectedIndexChanged">
                                <asp:ListItem>&lt;--Seleccione--&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Comentarios</td>
                        <td colspan="4" align="left">
                            <asp:TextBox ID="Txt_Comentarios_Cuenta_Contable" runat="server" TabIndex="15" Width="95.5%"
                                TextMode="MultiLine"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Comentarios_Cuenta_Contable" runat="server" WatermarkCssClass="watermarked"
                                TargetControlID ="Txt_Comentarios_Cuenta_Contable" WatermarkText="L�mite de Caractes 250">
                            </cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comentarios_Cuenta_Contable" runat="server" 
                                TargetControlID="Txt_Comentarios_Cuenta_Contable" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                ValidChars="��.,:;()���������� ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>                    
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>                    
                    <tr align="center">
                        <td colspan="5">
                            <!--<div style="overflow:auto;height:275px;width:95%;vertical-align:top;border-style:outset;border-color:Silver;">-->
                            <asp:GridView ID="Grid_Cuenta_Contable" runat="server"  Width="99%" HeaderStyle-CssClass="tblHead" 
                                AutoGenerateColumns="False" AllowPaging="true" PageSize="10" 
                                    CssClass="GridView_1" GridLines="None" 
                                onselectedindexchanged="Grid_Cuenta_Contable_SelectedIndexChanged" 
                                onsorting="Grid_Cuenta_Contable_Sorting" 
                                    onpageindexchanging="Grid_Cuenta_Contable_PageIndexChanging">
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="7%" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="Cuenta_Contable_ID" HeaderText="Cuenta ID" Visible="True" SortExpression="Cuenta_Contable_ID">
                                        <HeaderStyle HorizontalAlign="Left" Width="18%" />
                                        <ItemStyle HorizontalAlign="Left" Width="18%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Cuenta" HeaderText="Cuenta" Visible="True" SortExpression="Cuenta_Contable">
                                        <HeaderStyle HorizontalAlign="Left" Width="18%" />
                                        <ItemStyle HorizontalAlign="Left" Width="18%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" Visible="True" SortExpression="Descripcion">
                                        <HeaderStyle HorizontalAlign="Left" Width="55%" />
                                        <ItemStyle HorizontalAlign="Left" Width="55%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Nivel" HeaderText="Nivel" Visible="True">
                                        <HeaderStyle HorizontalAlign="Left" Width="18%" />
                                        <ItemStyle HorizontalAlign="Left" Width="18%" />
                                    </asp:BoundField>
                                </Columns>
                                <SelectedRowStyle CssClass="GridSelected" />
                                <PagerStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>  
                            <!--</div>-->
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
        <asp:AsyncPostBackTrigger  ControlID="Txt_Cuenta_Contable"/>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>