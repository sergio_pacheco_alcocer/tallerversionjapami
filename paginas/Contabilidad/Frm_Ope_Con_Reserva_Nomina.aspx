﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Con_Reserva_Nomina.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Reserva_Nomina" 
Title="Reserva Nomina" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <link href="../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../estilos/estilo_masterpage.css" rel="stylesheet" type="text/css" />
    <script src="../../jquery/jquery-1.5.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.formatCurrency-1.4.0.min.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.formatCurrency.all.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Grid_Anidado(Control, Fila)
        {
            var div = document.getElementById(Control); 
            var img = document.getElementById('img' + Control);
            
            if (div.style.display == "none") 
            {
                div.style.display = "inline";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                else {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                img.alt = "Contraer Registros";
            }
            else 
            {
                div.style.display = "none";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                else {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                img.alt = "Expandir Registros";
            }
        }
        function on(ctrl) {
            var disponible = parseFloat(ctrl.title.split(":")[1].replace(/,/gi,''));
            var valor = parseFloat(ctrl.value);

            if (valor > disponible) {
                $(ctrl).val('');
                alert('El importe no puede ser mayor al disponible!!');
            }
        }
        function onix(ctrl) {
            var disponible = parseFloat(ctrl.title.split(":")[1].replace(/,/gi, ''));
            var valor = parseFloat(ctrl.value.replace(/,/gi, ''));

            if (valor > disponible) {
                $(ctrl).val('');
                alert('El importe no puede ser mayor al disponible!!');
            }
        }
        
        (function ($) {
             $.formatCurrency.regions['es-MX'] = {
                 symbol: '',
                 positiveFormat: '%s%n',
                 negativeFormat: '-%s%n',
                 decimalSymbol: '.',
                 digitGroupSymbol: ',',
                 groupDigits: true
             };
         })(jQuery);
    </script>
    <script type="text/javascript" language="javascript">
        //<--
            //El nombre del controlador que mantiene la sesión
            var CONTROLADOR = "../../Mantenedor_Session.ashx";

            //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
            function MantenSesion() {
                var head = document.getElementsByTagName('head').item(0);
                script = document.createElement('script');
                script.src = CONTROLADOR;
                script.setAttribute('type', 'text/javascript');
                script.defer = true;
                head.appendChild(script);
            }

            //Temporizador para matener la sesión activa
            setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        //-->
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Polizas" runat="server" 
            EnableScriptGlobalization="true" EnableScriptLocalization="true" AsyncPostBackTimeout = "36000">
            </cc1:ToolkitScriptManager>
            <asp:UpdatePanel ID="Upd_Panel" runat="server" >
                <ContentTemplate>        
                    <asp:UpdateProgress ID="Uprg_Polizas" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                        <ProgressTemplate>
                               <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                               <div class="processMessage" id="div_progress">
                                    <img alt="" src="../Imagenes/paginas/Updating.gif" />
                               </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    
                    <div id="Div_Compromisos" style="background-color:#ffffff; width:99%; height:100%;">
                        <table width="100%" class="estilo_fuente">
                            <tr align="center">
                                <td class="label_titulo">Reserva Nomina</td>
                            </tr>
                            <tr>
                                <td align="left">&nbsp;
                                    <asp:UpdatePanel ID="Upnl_Mensajes_Error" runat="server" UpdateMode="Always" RenderMode="Inline">
                                        <ContentTemplate>                         
                                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"/>
                                        </ContentTemplate>                                
                                    </asp:UpdatePanel>                                      
                                </td>
                            </tr> 
                        </table>

                        <table width="100%"  border="0" cellspacing="0">
                            <tr align="center">
                                <td>                
                                    <div align="right" class="barra_busqueda">
                                        <table style="width:100%;height:28px;">
                                            <tr>
                                                <td align="left" style="width:59%;"> 
                                                    <asp:UpdatePanel ID="Upnl_Botones_Operacion" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
                                                        <ContentTemplate> 
                                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                                CssClass="Img_Button" TabIndex="1"
                                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                                onclick="Btn_Nuevo_Click" />
                                                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" 
                                                                CssClass="Img_Button" TabIndex="2"
                                                                ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                                                                onclick="Btn_Modificar_Click" />
                                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                                CssClass="Img_Button" TabIndex="4"
                                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                                onclick="Btn_Salir_Click" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td align="right" style="width:41%;">
                                                    <asp:HiddenField id="Hf_Existe_Reserva" runat="server"/>
                                                </td>       
                                            </tr>         
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>           
                            <div id="Div_Reserva_Datos" runat="server" style="width:98%">
                                <asp:Panel ID="Pnl_Datos_Generales" runat="server" GroupingText="Datos de Reserva" Width="815px">
                                    <table  width="100%" border="0">
                                        <tr><td colspan="7">&nbsp;</td> </tr>
                                        <tr visible = "false">
                                            <td  style=" width:20%; "> &nbsp; &nbsp;
                                                <asp:Label ID="Lbl_Anio_Psp" runat="server" Text="Año"></asp:Label>
                                            </td>
                                            <td style=" width:20%;">
                                               <asp:DropDownList runat="server" Width="60%" TabIndex="6" ID="Cmb_Anio_Psp"
                                                OnSelectedIndexChanged="Cmb_Anio_Psp_SelectedIndexChanged" AutoPostBack ="true"></asp:DropDownList>
                                            </td>
                                            <td style="width:10%;">
                                                <asp:Label ID="Lbl_Tipo_Modificacion" runat="server" Text="Modificación" />
                                            </td>
                                            <td style="width:20%;">
                                               <asp:DropDownList ID="Cmb_Tipo_Modificacion" runat="server" Width="94%" TabIndex="7" 
                                                AutoPostBack="true" OnSelectedIndexChanged="Cmb_Tipo_Modificacion_SelectedIndexChanged"/>
                                            </td>
                                            <td style=" width:10%">
                                                
                                                 <asp:Label ID="Lbl_Folio" runat="server" Text="No. Reserva"></asp:Label>
                                            </td>
                                            <td style=" width:15%">
                                                <asp:TextBox ID="Txt_No_Folio" runat="server" Width="95%"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="Txt_No_Folio_TextBoxWatermarkExtender" runat="server"
                                                TargetControlID="Txt_No_Folio" WatermarkCssClass="watermarked" WatermarkText="<No. Reserva>"
                                                Enabled="True" />
                                                <cc1:FilteredTextBoxExtender  ID="Txt_No_Folio_FilteredTextBoxExtender" 
                                                  runat="server" FilterType="Numbers" TargetControlID="Txt_No_Folio">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width:5%">
                                                <asp:ImageButton ID="Btn_Buscar_Reserva" runat="server" 
                                                    ToolTip="Consultar" TabIndex="6" onclick="Btn_Buscar_Reserva_Click"
                                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" Visible = "false"
                                                    />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            &nbsp;
                                            &nbsp;
                                                <asp:Label ID="Lbl_Estatus" runat="server" Text="*Estatus"></asp:Label>
                                            </td>
                                            <td colspan = "2">
                                                <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="94%" >
                                                <asp:ListItem Value="GENERADA">GENERADA</asp:ListItem>
                                                <asp:ListItem Value="CANCELADA">CANCELADA</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td >*Tipo de Solicitud</td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="Cmb_Tipo_Solicitud_Pago" runat="server" TabIndex="6" Width="94%"
                                                AutoPostBack = "true" OnSelectedIndexChanged="Cmb_Tipo_Solicitud_Pago_SelectedIndexChanged"/> 
                                            </td>
                                        </tr>
                                        <tr id="Tr_Proveedor" runat="server">
                                            <td id="Td_Proveedor" runat="server">&nbsp;&nbsp;
                                                <asp:Label ID="Lbl_Proveedor" runat="server" Text="*Proveedor"></asp:Label>
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox ID="Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" MaxLength="100" TabIndex="11" Width="80%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" TargetControlID="Txt_Nombre_Proveedor_Solicitud_Pago"
                                                    FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:ImageButton ID="Btn_Buscar_Proveedor_Solicitud_Pagos" 
                                                    runat="server" ToolTip="Consultar"
                                                    TabIndex="12" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                                    onclick="Btn_Buscar_Proveedor_Solicitud_Pagos_Click"/>
                                            </td>
                                            <td colspan="4">
                                                <asp:DropDownList ID="Cmb_Proveedor_Solicitud_Pago" runat="server" TabIndex="13" Width="97%" />
                                            </td>
                                        </tr>
                                        <tr id="Tr_Empleado" runat="server">
                                            <td>&nbsp;&nbsp;
                                                <asp:Label ID="Lbl_Empleado" runat="server" Text="*Empleado"></asp:Label>
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox ID="Txt_Nombre_Empleado" runat="server" MaxLength="100" TabIndex="11" Width="80%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Empleado" runat="server" TargetControlID="Txt_Nombre_Empleado"
                                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:ImageButton ID="Btn_Buscar_Empleado" 
                                                    runat="server" ToolTip="Consultar"
                                                    TabIndex="12" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                                    onclick="Btn_Buscar_Empleado_Click" />
                                            </td>
                                            <td colspan="4">
                                                <asp:DropDownList ID="Cmb_Nombre_Empleado" runat="server" TabIndex="13" Width="97%" />
                                            </td>
                                        </tr>
                                        <tr>
                                        
                                            <td>
                                                &nbsp;&nbsp;
                                                <asp:Label ID="Lbl_Tipo_Reserva" runat="server" Text="*Tipo de reserva"></asp:Label>
                                            </td>
                                             <td colspan="2">
                                                <asp:DropDownList ID="Cmb_Tipo_Reserva" runat="server" Width="94%">
                                                    <asp:ListItem>ABIERTA</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>                                    
                                            <td>
                                                <asp:Label ID="Lbl_Recursos" runat="server" Text="*Recursos"></asp:Label>
                                            </td>
                                             <td colspan="3">
                                                <asp:TextBox ID="Txt_Fuente_Ramo" runat="server" Width="94%" ReadOnly="true" Text="RECURSO ASIGNADO"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;
                                                <asp:Label ID="Lbl_Programa" runat="server" 
                                                    Text="Programa"></asp:Label>
                                            </td>
                                            <td colspan="6">
                                                <asp:DropDownList ID="Cmb_Programa" runat="server" AutoPostBack="true" 
                                                    OnSelectedIndexChanged="Cmb_Programa_SelectedIndexChanged"  Width="98%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;
                                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" 
                                                    Text="Unidad Responsable"></asp:Label>
                                            </td>
                                            <td colspan="6">
                                                <asp:DropDownList ID="Cmb_Unidad_Responsable_Busqueda" runat="server" 
                                                    AutoPostBack="true" 
                                                    OnSelectedIndexChanged="Cmb_Unidad_Responsable_Busqueda_SelectedIndexChanged" 
                                                    Width="98%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                            <tr>
                                                <td>
                                                    &nbsp; &nbsp;
                                                    <asp:Label ID="Lbl_Concepto" runat="server" Text="*Concepto"></asp:Label>
                                                </td>
                                                <td colspan="6">
                                                    <asp:TextBox ID="Txt_Conceptos" runat="server" TextMode="MultiLine" 
                                                        Width="97.5%"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="Txt_Conceptos_TextBoxWatermarkExtender" 
                                                        runat="server" Enabled="True" TargetControlID="Txt_Conceptos" 
                                                        WatermarkCssClass="watermarked" 
                                                        WatermarkText="Ingrese sus comentarios &lt;Maximo de 255 caracteres&gt;">
                                                    </cc1:TextBoxWatermarkExtender>
                                                    <cc1:FilteredTextBoxExtender ID="Txt_Conceptos_FilteredTextBoxExtender" 
                                                        runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                        InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Conceptos" 
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ ">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp; &nbsp;
                                                    <asp:Label ID="Lbl_Busqueda_Partida" runat="server" Text=" Busqueda Partida"></asp:Label>
                                                </td>
                                                <td colspan="6">
                                                    <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="100" TabIndex="11" Width="94%"></asp:TextBox>
                                                    <asp:ImageButton ID="Btn_Busqueda_Partida" runat="server" 
                                                            ToolTip="Consultar" TabIndex="6" onclick="Btn_Busqueda_Partida_Click"
                                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                                    />
                                                </td>
                                            </tr>
                                            <tr ID="Tr_Div_Partidas" runat="server">
                                                <td colspan="7" style=" width:60%; text-align:center;">
                                                    <div style=" max-height:150px; overflow:auto; width:100%; vertical-align:top;">
                                                        <table style="width:97%;">
                                                            <tr>
                                                                <td style="width:100%;">
                                                                    <asp:GridView ID="Grid_Partidas" runat="server" AllowSorting="true" 
                                                                        AutoGenerateColumns="false" CssClass="GridView_Nested" ShowHeader="true"
                                                                        DataKeyNames="Partida_ID" EmptyDataText="No se encontraron partidas con saldo" 
                                                                        GridLines="None" OnRowCreated="Grid_Partidas_Asignadas_Detalle_RowCreated"  style="white-space:normal" Width="100%">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="Btn_Seleccionar" runat="server" 
                                                                                        CommandArgument='<%#Eval("PARTIDA_ID")%>' 
                                                                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png" />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="3%" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="PARTIDA_ID" />
                                                                            <asp:BoundField DataField="NOMBRE" HeaderText="Partida" SortExpression="NOMBRE">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="77%" />
                                                                                <ItemStyle HorizontalAlign="Left" Width="77%" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="DISPONIBLE" DataFormatString="{0:#,###,##0.00}" 
                                                                                HeaderText="Disponible" SortExpression="DISPONIBLE">
                                                                                <HeaderStyle HorizontalAlign="Right" Width="20%" />
                                                                                <ItemStyle HorizontalAlign="Right" Width="20%" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                        <PagerStyle CssClass="GridHeader_Nested" />
                                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                                        <AlternatingRowStyle CssClass="GridAltItem_Nested" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr ID="Tr_Partidas_Codigo" runat="server">
                                                <td colspan="7" style=" width:60%; text-align:center;">
                                                    <center>
                                                        <div id="Div_Partidas_Reserva_Nueva"  runat="server" style=" max-height:180px; overflow:auto; width:780px; vertical-align:top;">
                                                            <table style="width:100%;">
                                                                <tr>
                                                                    <td style="width:100%;">
                                                                        <asp:GridView ID="Grid_Partida_Saldo" runat="server"
                                                                            AutoGenerateColumns="false" CssClass=""  Font-Size="XX-Small"  ShowHeader="true"
                                                                            EmptyDataText="No se encontraron partidas con saldo" GridLines="None" 
                                                                            OnRowDataBound="Grid_Partidas_Saldo_RowDataBound" style="white-space:normal" 
                                                                            Width="100%">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="DEPENDENCIA_ID" />
                                                                                <asp:BoundField DataField="FTE_FINANCIAMIENTO_ID" />
                                                                                <asp:BoundField DataField="CLAVE_FINANCIAMIENTO" HeaderText="FF">
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="4%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Center" Width="4%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="PROYECTO_PROGRAMA_ID" />
                                                                                <asp:BoundField DataField="PROGRAMA" HeaderText="Programa">
                                                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="PARTIDA_ID" />
                                                                                <asp:BoundField DataField="CLAVE_NOM_UR" HeaderText="Gerencia">
                                                                                    <HeaderStyle HorizontalAlign="Left" Width="14%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" Width="14%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="CLAVE_PARTIDA" HeaderText="Partida">
                                                                                    <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                                                    <ItemStyle Font-Size="XX-Small"  HorizontalAlign="Left" Width="15%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="APROBADO" DataFormatString="{0:#,###,##0.00}" HeaderText="Aprobado">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="8%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="8%" />
                                                                                </asp:BoundField>
                                                                                 <asp:BoundField DataField="DISPONIBLE" DataFormatString="{0:#,###,##0.00}" HeaderText="Disponible">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="10%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="COMPROMETIDO" DataFormatString="{0:#,###,##0.00}" HeaderText="Comprometido">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="11%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="11%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DEVENGADO" DataFormatString="{0:#,###,##0.00}" HeaderText="Devengado">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="11%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="11%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="EJERCIDO" DataFormatString="{0:#,###,##0.00}" HeaderText="Ejercido">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="11%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="11%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="PAGADO" DataFormatString="{0:#,###,##0.00}" HeaderText="Pagado">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="10%" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle HorizontalAlign="right" Width="8%" />
                                                                                    <ItemStyle HorizontalAlign="right" Width="8%" />
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="Txt_Importe_Partida" runat="server" Font-Size="X-Small" 
                                                                                            CssClass="text_cantidades_grid" name="<%=Txt_Importe_Partida.ClientID %>" 
                                                                                            onkeyup="javascript:on(this);" Width="80%"></asp:TextBox>
                                                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Importe_Grid" runat="server" 
                                                                                            Enabled="True" FilterType="Custom, Numbers" 
                                                                                            TargetControlID="Txt_Importe_Partida" ValidChars="0123456789.,">
                                                                                        </cc1:FilteredTextBoxExtender>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="CAPITULO_ID" HeaderText="CAPITULO_ID" />
                                                                                <asp:BoundField DataField="DISPONIBLE_ENERO" DataFormatString="{0:#,###,##0.00}" HeaderText="Ene">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_FEBRERO" DataFormatString="{0:#,###,##0.00}" HeaderText="Feb">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_MARZO" DataFormatString="{0:#,###,##0.00}" HeaderText="Mar">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_ABRIL" DataFormatString="{0:#,###,##0.00}" HeaderText="Abr">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_MAYO" DataFormatString="{0:#,###,##0.00}" HeaderText="May">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_JUNIO" DataFormatString="{0:#,###,##0.00}" HeaderText="Jun">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_JULIO" DataFormatString="{0:#,###,##0.00}" HeaderText="Jul">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_AGOSTO" DataFormatString="{0:#,###,##0.00}" HeaderText="Ago">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_SEPTIEMBRE" DataFormatString="{0:#,###,##0.00}" HeaderText="Sep">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_OCTUBRE" DataFormatString="{0:#,###,##0.00}" HeaderText="Oct">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_NOVIEMBRE" DataFormatString="{0:#,###,##0.00}" HeaderText="Nov">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="DISPONIBLE_DICIEMBRE" DataFormatString="{0:#,###,##0.00}" HeaderText="Dic">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="IMPORTE" DataFormatString="{0:#,###,##0.00}" HeaderText="Total">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="5%" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                            <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                            <PagerStyle CssClass="GridHeader_Nested" />
                                                                            <HeaderStyle CssClass="GridHeader_Nested"/>
                                                                            <AlternatingRowStyle CssClass="GridAltItem_Nested" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="Div_Cabecera_Modificar" runat="server">
                                                            <table width="99%"  border="0" cellspacing="0">
                                                                <tr>
                                                                    <td colspan="7" style=" text-align:right">
                                                                        <asp:Label ID="Lbl_Masiva_Modificacion" runat="server" Text="Modificación Masiva"></asp:Label>
                                                                        <asp:CheckBox ID="Chk_Movimiento_Masivo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <%--<td Font-Size="XX-Small"  style=" width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;"> &nbsp;F.F.</td>--%>
                                                                    <td Font-Size="XX-Small" style="width:20%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Gerencia.</td>
                                                                    <td Font-Size="XX-Small" style="width:20%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Programa</td>
                                                                    <td Font-Size="XX-Small" style="width:22%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Partida</td>
                                                                    <td Font-Size="XX-Small" style="width:8%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;Disponible</td>
                                                                    <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">&nbsp;&nbsp;Comprometido</td>
                                                                    <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="center">Monto</td>    
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div  id="Div_Partidas_A_Modificar" runat="server" style=" max-height:180px; overflow:auto; width:100%; vertical-align:top;">
                                                            <table style="width:100%;">
                                                                <tr>
                                                                    <td style="width:100%;">
                                                                        <asp:GridView ID="Grid_Partidas_Reserva" runat="server"
                                                                            AutoGenerateColumns="false" CssClass=""  Font-Size="XX-Small"  ShowHeader="false"
                                                                            EmptyDataText="No se encontraron partidas con saldo" GridLines="Both" 
                                                                            OnRowDataBound="Grid_Partidas_d_Reserva_RowDataBound" style="white-space:normal" 
                                                                            Width="100%">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="DEPENDENCIA_ID" />
                                                                                <asp:BoundField DataField="FTE_FINANCIAMIENTO_ID" />
                                                                                <asp:BoundField DataField="CLAVE_FINANCIAMIENTO">
                                                                                    <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" Width="5%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="PROYECTO_PROGRAMA_ID" />
                                                                                <asp:BoundField DataField="CLAVE_DEPENDENCIA">
                                                                                    <HeaderStyle HorizontalAlign="Left" Width="22%" />
                                                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="22%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="CLAVE_PROGRAMA">
                                                                                    <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="PARTIDA_ID" />
                                                                                  <asp:BoundField DataField="PROGRAMA">
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="12%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Center" Width="12%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="CLAVE_PARTIDA" >
                                                                                    <HeaderStyle HorizontalAlign="Left" Width="26%" />
                                                                                    <ItemStyle Font-Size="XX-Small"  HorizontalAlign="Left" Width="26%" />
                                                                                </asp:BoundField>
                                                                                 <asp:BoundField DataField="DISPONIBLE" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="11%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="11%" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="COMPROMETIDO" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Width="11%" />
                                                                                    <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" Width="11%" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle HorizontalAlign="right" Width="11%" />
                                                                                    <ItemStyle HorizontalAlign="right" Width="11%" />
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="Txt_Importe_Comprometido" runat="server" Font-Size="X-Small" 
                                                                                            CssClass="text_cantidades_grid" name="<%=Txt_Importe_Comprometido.ClientID %>" 
                                                                                            onkeyup="javascript:onix(this);" onClick="$(this).select();" 
                                                                                            onblur="$(this).formatCurrency({colorize:true, region: 'es-MX'});" Width="90%"></asp:TextBox>
                                                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Importe_Grid" runat="server" 
                                                                                            Enabled="True" FilterType="Custom, Numbers" 
                                                                                            TargetControlID="Txt_Importe_Comprometido" ValidChars="0123456789.,">
                                                                                        </cc1:FilteredTextBoxExtender>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="CAPITULO_ID" HeaderText="CAPITULO_ID" />
                                                                                <asp:BoundField DataField="NO_RESERVA" HeaderText="NO_RESERVA" />
                                                                            </Columns>
                                                                            <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                            <PagerStyle CssClass="GridHeader_Nested" />
                                                                            <HeaderStyle Height="0px"/>
                                                                            <AlternatingRowStyle CssClass="GridAltItem_Nested" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </center>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                </td>
                                                <td colspan="2" align="right">
                                                        <asp:ImageButton ID="Btn_Agregar" runat="server" 
                                                            ImageUrl="~/paginas/imagenes/gridview/add_grid.png" OnClick="Btn_Agregar_Click" 
                                                            TabIndex="19" ToolTip="Agregar" />
                                                </td>
                                            </tr>                                
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="Pnl_Partidas_Asignadas" runat="server" GroupingText="Reservas" Width="815px">
                                    <div style="width:100%; height:auto; max-height: 400px; overflow:auto; vertical-align:top;">
                                        <table style="width:100%;">
                                            <tr>
                                                <td style="width:100%;">
                                                  <asp:GridView ID="Grid_Partida_Asignada" runat="server" style="white-space:normal;"
                                                    AutoGenerateColumns="False" GridLines="None" 
                                                    Width="100%"  EmptyDataText="No se encontraron reservas" 
                                                    CssClass="GridView_1" HeaderStyle-CssClass="tblHead"
                                                    DataKeyNames="DEPENDENCIA_ID"   OnRowDataBound="Grid_Partida_Asignada_RowDataBound"
                                                    OnRowCreated="Grid_Partidas_Asignadas_Detalle_RowCreated"
                                                    HeaderStyle-Font-Size="XX-Small">
                                                    <Columns>
                                                        <asp:TemplateField> 
                                                          <ItemTemplate> 
                                                                <a href="javascript:Grid_Anidado('div<%# Eval("DEPENDENCIA_ID") %>', 'alt');"> 
                                                                     <img id="imgdiv<%# Eval("DEPENDENCIA_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                                </a> 
                                                          </ItemTemplate> 
                                                          <AlternatingItemTemplate> 
                                                               <a href="javascript:Grid_Anidado('div<%# Eval("DEPENDENCIA_ID") %>', 'one');"> 
                                                                    <img id="imgdiv<%# Eval("DEPENDENCIA_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                               </a> 
                                                          </AlternatingItemTemplate> 
                                                          <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="Btn_Seleccionar_Reserva" runat="server" 
                                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png" 
                                                                    onclick="Btn_Seleccionar_Reserva_Click" CommandArgument='<%#Eval("NO_RESERVA")%>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                                        </asp:TemplateField>      
                                                        <asp:BoundField DataField="DEPENDENCIA_ID" />
                                                        <asp:BoundField DataField="FUENTE_FINANCIAMIENTO_ID" />
                                                        <asp:BoundField DataField="PROGRAMA_ID" />
                                                        <asp:BoundField DataField="EMPLEADO_ID" />
                                                        <asp:BoundField DataField="PROVEEDOR_ID" />
                                                        <asp:BoundField DataField="NO_RESERVA" HeaderText="No. Reserva" >
                                                            <HeaderStyle HorizontalAlign="Left"/>
                                                            <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" >
                                                            <HeaderStyle HorizontalAlign="Left"/>
                                                            <ItemStyle HorizontalAlign="Left" Width="40%" Font-Size="X-Small"/>
                                                        </asp:BoundField>
                                                       <asp:BoundField DataField="FUENTE_FINANCIAMIENTO" HeaderText="Fuente Financiamiento">
                                                            <HeaderStyle HorizontalAlign="Left"/>
                                                            <ItemStyle HorizontalAlign="Left"  Width="6%" Font-Size="X-Small" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="PROGRAMA" HeaderText="Programa">
                                                            <HeaderStyle HorizontalAlign="Left"/>
                                                            <ItemStyle HorizontalAlign="Left"  Width="40%" Font-Size="X-Small"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="BENEFICIARIO" />
                                                        <asp:BoundField DataField="CONCEPTO" />
                                                        <asp:BoundField DataField="TOTAL" HeaderText="Total" DataFormatString="{0:#,###,##0.00}">
                                                           <HeaderStyle HorizontalAlign="Center"/>
                                                            <ItemStyle HorizontalAlign="Right"  Width="15%" Font-Size="X-Small"/>
                                                        </asp:BoundField>
                                                        <asp:TemplateField>
                                                           <ItemTemplate>
                                                             </td>
                                                             </tr> 
                                                             <tr>
                                                              <td colspan="100%">
                                                               <div id="div<%# Eval("DEPENDENCIA_ID") %>" style="display:block;position:relative;left:20px;" >
                                                                   <asp:GridView ID="Grid_Partidas_Asignadas_Detalle" runat="server" style="white-space:normal;"
                                                                       CssClass="GridView_Nested" HeaderStyle-CssClass="tblHead"
                                                                       AutoGenerateColumns="false" GridLines="None" Width="98%"
                                                                       OnRowCreated="Grid_Partidas_Asignadas_Detalle_RowCreated">
                                                                       <Columns>
                                                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                                                ImageUrl="~/paginas/imagenes/paginas/Select_Grid_Inner.png">
                                                                                <ItemStyle Width="3%" />
                                                                            </asp:ButtonField> 
                                                                            <asp:BoundField DataField="DEPENDENCIA_ID"  />
                                                                            <asp:BoundField DataField="PARTIDA_ID"  />
                                                                            
                                                                            <asp:BoundField DataField="PARTIDA" HeaderText="Partida" >
                                                                                <HeaderStyle HorizontalAlign="Left" Font-Size="XX-Small"/>
                                                                                <ItemStyle HorizontalAlign="Left" Width="77%" Font-Size="XX-Small"/>
                                                                            </asp:BoundField>  
                                                                                                                                                 
                                                                            <asp:BoundField DataField="IMPORTE" HeaderText="Importe" DataFormatString="{0:#,###,##0.00}">
                                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                <ItemStyle HorizontalAlign="Right"  Width="10%" Font-Size="XX-Small" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                                                        ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                                                        onclick="Btn_Eliminar_Click" CommandArgument='<%#Eval("PARTIDA_ID") + "_" + Eval("DEPENDENCIA_ID")%>'
                                                                                        OnClientClick="return confirm('&iquest;Esta seguro que desea elimina el registro?');"/>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="SALDO" HeaderText="saldo">
                                                                            <HeaderStyle HorizontalAlign="Right" Width="5%" />
                                                                            <ItemStyle HorizontalAlign="Right" Width="5%" Font-Size="X-Small"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CAPITULO_ID" HeaderText="CAPITULO_ID" />
                                                                       </Columns>
                                                                       <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                       <PagerStyle CssClass="GridHeader_Nested" />
                                                                       <HeaderStyle CssClass="GridHeader_Nested" />
                                                                       <AlternatingRowStyle CssClass="GridAltItem_Nested" /> 
                                                                   </asp:GridView>
                                                               </div>
                                                              </td>
                                                             </tr>
                                                           </ItemTemplate>
                                                        </asp:TemplateField>
                                                     </Columns>
                                                     <SelectedRowStyle CssClass="GridSelected" />
                                                        <PagerStyle CssClass="GridHeader" />
                                                        <HeaderStyle CssClass="GridHeader" />
                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                  </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="width:100%; text-align:right">
                                        Total Reserva: &nbsp; <asp:TextBox ID="Txt_Total_Reserva" runat = "server" ReadOnly="true" Width ="120px"
                                        style="border-color:Navy; font-size:8pt; text-align:right; font-weight:bold;"></asp:TextBox>
                                    </div>
                                </asp:Panel>
                            </div>
                      </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
</asp:Content>