﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;

public partial class paginas_Contabilidad_Frm_Rpt_Con_Consulta_Tramite : System.Web.UI.Page
{
    #region (Page_Load)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Page_Load
    /// DESCRIPCION : Carga la configuración inicial de los controles de la página.
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 15/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Refresca la session del usuario lagueado al sistema.
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            //Valida que exista algun usuario logueado al sistema.
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                //Acciones();
                ViewState["SortDirection"] = "ASC";
                Txt_Lector_Codigo_Barras.Focus();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Metodos)
        #region (Metodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
            ///               diferentes operaciones
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 15/Mayo/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                try
                {
                    Limpia_Controles();             //Limpia los controles del forma
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString());
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION : Limpia los controles que se encuentran en la forma
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 15/Mayo/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Controles()
            {
                try
                {
                    Txt_Lector_Codigo_Barras.Text = "";
                    Txt_Aut_Contabilidad.Text = "";
                    Txt_Aut_Documentacion_Fisica.Text = "";
                    Txt_Aut_Ejercido.Text = "";
                    Txt_Beneficiario_Det.Text = "";
                    Txt_Concepto_Reserva_Det.Text = "";
                    Txt_Fecha_Aut_Contabilidad.Text = "";
                    Txt_Fecha_Aut_Ejercido.Text = "";
                    Txt_Fecha_Autoriza_Director_Det.Text = "";
                    Txt_Fecha_Recepcion_Doc_Contabilidad.Text = "";
                    Txt_Fecha_Recepcion_Doc_Det.Text = "";
                    Txt_Fecha_Recepcion_Doc_Ejercido.Text = "";
                    Txt_Fecha_Recibio_Documentacion_Fisica.Text = "";
                    Txt_Fecha_Solicitud_Det.Text = "";
                    Txt_Monto_Solicitud_Det.Text = "";
                    Txt_No_Pago_Det.Text = "";
                    Txt_No_poliza_Det.Text = "";
                    Txt_No_Reserva_Det.Text = "";
                    Txt_No_Solicitud_Pagos.Text = "";
                    Txt_Recibio_Documentacion_Contabilidad.Text = "";
                    Txt_Recibio_Documentacion_Ejercido.Text = "";
                    Txt_Recibio_Documentacion_Fisica.Text = "";
                    Grid_Partidas.DataSource = null;
                    Grid_Partidas.DataBind();
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Imprimir
            ///DESCRIPCIÓN: Imprime la solicitud
            ///PROPIEDADES:     
            ///CREO: Sergio Manuel Gallardo
            ///FECHA_CREO: 06/Enero/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Imprimir(String Numero_Solicitud)
            {
                DataSet Ds_Reporte = null;
                DataTable Dt_Pagos = null;
                try
                {
                    Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
                    Ds_Reporte = new DataSet();
                    Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
                    Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
                    if (Dt_Pagos.Rows.Count > 0)
                    {
                        Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                        Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                        //Se llama al método que ejecuta la operación de generar el reporte.
                        Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
                    }
                }
                //}
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                    Lbl_Mensaje_Error.Visible = true;
                }

            }
        #endregion

        #region (Control Acceso Pagina)
            ///*******************************************************************************
            /// NOMBRE: Configuracion_Acceso
            /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
            /// PARÁMETROS  :
            /// USUARIO CREÓ: Salvador L. Rea Ayala
            /// FECHA CREÓ  : 15/Septiembre/2011
            /// USUARIO MODIFICO  :
            /// FECHA MODIFICO    :
            /// CAUSA MODIFICACIÓN:
            ///*******************************************************************************
            protected void Configuracion_Acceso(String URL_Pagina)
            {
                List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
                DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

                try
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
                    {
                        if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                        {
                            //Consultamos el menu de la página.
                            Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                            if (Dr_Menus.Length > 0)
                            {
                                //Validamos que el menu consultado corresponda a la página a validar.
                                if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                                {
                                    Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                                }
                                else
                                {
                                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                                }
                            }
                            else
                            {
                                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                            }
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: IsNumeric
            /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
            /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
            /// USUARIO CREÓ: Salvador L. Rea Ayala
            /// FECHA CREÓ  : 15/Septiembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Es_Numero(String Cadena)
            {
                Boolean Resultado = true;
                Char[] Array = Cadena.ToCharArray();
                try
                {
                    for (int index = 0; index < Array.Length; index++)
                    {
                        if (!Char.IsDigit(Array[index])) return false;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
                }
                return Resultado;
            }
        #endregion

        #region (Metodos Consulta)
            protected void Txt_Lector_Codigo_Barras_TextChanged(object sender, EventArgs e)
            {
                if (!String.IsNullOrEmpty(Txt_Lector_Codigo_Barras.Text)) Consulta_Tramite(Txt_Lector_Codigo_Barras.Text); //Consulta el ID de la cuenta contable que tiene asignado el núumero de cuenta que fue proporcionada por el usuario
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Parametros
            /// DESCRIPCION : Consulta el Parametro que estan dado de alta en la BD
            /// PARAMETROS  : 
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 21/Septiembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Tramite(String Solicitud)
            {
                Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
                DataTable Dt_Detalles = new DataTable();
                DataTable Dt_Documentos = new DataTable();
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Text = "";
                try
                {
                    if (Solicitud.Length < 10)
                    {
                        Solicitud_Negocio.P_No_Solicitud_Pago = String.Format("{0:0000000000}",Convert.ToInt64(Solicitud));
                    }
                    else
                    {
                        Solicitud_Negocio.P_No_Solicitud_Pago = Solicitud;
                    }
                    Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
                    Dt_Documentos = Solicitud_Negocio.Consulta_Documentos();

                    if (Dt_Detalles != null)
                    {
                        if (Dt_Detalles.Rows.Count > 0)
                        {
                            Div_Informacion.Style.Add("display", "block");
                            Txt_No_Pago_Det.Text = Dt_Detalles.Rows[0]["ESTATUS"].ToString().Trim();
                            Txt_No_Solicitud_Pagos.Text = Dt_Detalles.Rows[0]["NO_SOLICITUD_PAGO"].ToString().Trim();
                            Txt_No_Reserva_Det.Text = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString().Trim();
                            Txt_Concepto_Reserva_Det.Text = Dt_Detalles.Rows[0]["CONCEPTO_RESERVA"].ToString().Trim();
                            Txt_Beneficiario_Det.Text = Dt_Detalles.Rows[0]["BENEFICIARIO"].ToString().Trim();
                            Txt_Fecha_Solicitud_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_SOLICITUD"]);
                            Txt_Monto_Solicitud_Det.Text = String.Format("{0:c}", Dt_Detalles.Rows[0]["MONTO"]);
                            Txt_Fecha_Autoriza_Director_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZO_RECHAZO_JEFE"]);
                            Txt_No_poliza_Det.Text = Dt_Detalles.Rows[0]["NO_POLIZA"].ToString().Trim();
                            Txt_Fecha_Recepcion_Doc_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECEPCION_DOCUMENTOS"]);
                            Txt_Recibio_Documentacion_Fisica.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_DOC_FISICA"].ToString().Trim();
                            Txt_Fecha_Recibio_Documentacion_Fisica.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECEPCION_DOCUMENTOS"]);
                            Txt_Aut_Documentacion_Fisica.Text = Dt_Detalles.Rows[0]["USUARIO_AUTORIZO_DOCUMENTOS"].ToString().Trim();
                            Txt_Fecha_Recepcion_Doc_Contabilidad.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECIBIO_CONTABILIDAD"]);
                            Txt_Recibio_Documentacion_Contabilidad.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_CONTABILIDAD"].ToString().Trim();
                            Txt_Aut_Contabilidad.Text = Dt_Detalles.Rows[0]["USUARIO_AUTORIZO_CONTABILIDAD"].ToString().Trim();
                            Txt_Fecha_Aut_Contabilidad.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZO_RECHAZO_CONTABI"]);
                            Txt_Recibio_Documentacion_Ejercido.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_EJERCIDO"].ToString().Trim();
                            Txt_Fecha_Recepcion_Doc_Ejercido.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECIBIO_EJERCIDO"]);
                            Txt_Aut_Ejercido.Text = Dt_Detalles.Rows[0]["USUARIO_AUTORIZO_EJERCIDO"].ToString().Trim();
                            Txt_Fecha_Aut_Ejercido.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZA_RECHAZA_EJD"]);
                            Txt_Lector_Codigo_Barras.Text = Solicitud;
                            Consulta_Datos_Reserva(Convert.ToDouble(Dt_Detalles.Rows[0]["NO_RESERVA"].ToString().Trim()));
                            if (String.IsNullOrEmpty(Txt_No_poliza_Det.Text))
                            {
                                Tr_Poliza.Style.Add("Display", "none");
                            }
                            else
                            {
                                Tr_Poliza.Style.Add("Display", "block");
                            }

                            Grid_Documentos.Columns[3].Visible = true;
                            Grid_Documentos.Columns[4].Visible = true;
                            Grid_Documentos.DataSource = Dt_Documentos;
                            Grid_Documentos.DataBind();
                            Grid_Documentos.Columns[3].Visible = false;
                            Grid_Documentos.Columns[4].Visible = false;
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Text = "Un Número de Solicitud Existente <br>";
                            Div_Informacion.Style.Add("display", "none");
                            Txt_Lector_Codigo_Barras.Text = "";
                            Txt_Lector_Codigo_Barras.Focus();
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Limpia_Controles();
                        }

                    }
                    else
                    {
                            Lbl_Mensaje_Error.Text = "Un Número de Solicitud Existente <br>";
                            Div_Informacion.Style.Add("display", "none");
                            Txt_Lector_Codigo_Barras.Text = "";
                            Txt_Lector_Codigo_Barras.Focus();
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Limpia_Controles();
                    }
                    Txt_Lector_Codigo_Barras.Text = "";
                    Txt_Lector_Codigo_Barras.Focus();
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Parametros" + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Datos_Reserva
            /// DESCRIPCION : Consulta todos los datos que corresponden al número de reserva
            ///               que fue seleccionada por el usuario
            /// PARAMETROS  : No_Reserva : Indica el No de Reserva a obtener los datos de la
            ///                             base de datos
            /// CREO        : Yazmisn Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Datos_Reserva(Double No_Reserva)
            {
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_PSP_Reservas = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Datos_Reserva; //Variable a obtener los datos de la consulta
                try
                {
                    Rs_Consulta_Ope_PSP_Reservas.P_No_Reserva = No_Reserva;
                    Dt_Datos_Reserva = Rs_Consulta_Ope_PSP_Reservas.Consulta_Datos_Reserva(); //Consulta todos los datos correspondientes a la reserva que el usuario selecciono
                    if (Dt_Datos_Reserva.Rows.Count > 0)
                    {
                        Grid_Partidas.DataSource = Dt_Datos_Reserva;
                        Grid_Partidas.DataBind();
                    }


                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Datos_Reserva " + ex.Message.ToString(), ex);
                }
            }
        #endregion

        #region Metodos Reportes
        /// *************************************************************************************
        /// NOMBRE:             Generar_Reporte
        /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
        ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
        ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
        /// FECHA MODIFICO:     16/Mayo/2011
        /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
        ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
        /// *************************************************************************************
        public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
        {
            ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
            String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
            try
            {
                Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
                Reporte.Load(Ruta);

                if (Ds_Reporte_Crystal is DataSet)
                {
                    if (Ds_Reporte_Crystal.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Reporte_Crystal);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                        Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        /// *************************************************************************************
        /// NOMBRE:             Exportar_Reporte_PDF
        /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
        ///                     especificada.
        /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        protected void Mostrar_Reporte(String Nombre_Reporte)
        {
            String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

            try
            {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                    "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        #endregion

    #endregion

    #region (Eventos)
            protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    if (Btn_Salir.ToolTip == "Salir")
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                    else
                    {
                        Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
            protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    if (!String.IsNullOrEmpty(Txt_No_Solicitud_Pagos.Text))
                    {
                        Imprimir(Txt_No_Solicitud_Pagos.Text);
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "Es necesario Tener el número de solicitud: <br />";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
    #endregion
}
