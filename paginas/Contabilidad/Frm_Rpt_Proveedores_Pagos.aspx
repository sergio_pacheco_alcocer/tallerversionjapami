﻿<%@ Page Language="C#"  MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Proveedores_Pagos.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Proveedores_Pagos" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="Tsm_Proveedores_Pagos" runat="server"  AsyncPostBackTimeout="36000"  EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>        
           <asp:UpdateProgress ID="Uprg_Polizas" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
                
                <div id="Div_Reporte_Balance_Mensual" style="background-color:#ffffff; width:100%; height:100%;">    
                    <table width="100%" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo">Reporte Proveedores Pago</td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                                <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table width="98%"  border="0" cellspacing="0">
                        <tr align="center">
                            <td>                
                                <div align="right" class="barra_busqueda">                        
                                    <table style="width:100%;height:28px;">
                                        <tr>
                                            <td align="left" style="width:59%;">
                                            <asp:UpdatePanel ID="Upnl_Export_PDF" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                <ContentTemplate>
                                                <asp:ImageButton ID="Btn_Reporte_Analitico" runat="server" ToolTip="Generar Reporte" 
                                                    CssClass="Img_Button" TabIndex="1"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                                                    onclick="Btn_Reporte_Analitico_Click" />
                                               </ContentTemplate>
                                            </asp:UpdatePanel>
                                           <asp:UpdatePanel ID="Upnl_Export_EXCEL" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                <ContentTemplate>
                                                 <asp:ImageButton ID="Btn_Imprimir_Excel" runat="server" 
                                                 ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png"
                                                  CssClass="Img_Button"  AlternateText="Imprimir Excel" 
                                                 ToolTip="Exportar Excel"
                                                 OnClick="Btn_Excel_Click" Visible="true"/> 
                                                 <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                    CssClass="Img_Button" TabIndex="2"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"/>

                                               </ContentTemplate>
                                            </asp:UpdatePanel>  

                                            </td>
                                            <td align="right" style="width:41%;">&nbsp;</td>       
                                        </tr>         
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    
                    <table width="98%" class="estilo_fuente"> 
                    <tr >
                            <td style="width:20%;text-align:left;" class="button_agregar">*REPORTE:</td>
                            <td style="width:30%;text-align:left;">
                            </td>
                            <td style="width:20%;text-align:left;"></td>
                            <td style="width:30%;text-align:left;"></td>
                        </tr>                       
                          <tr id="Tr_Proveedor" runat="server">
                            <td>&nbsp;&nbsp;
                                <asp:Label ID="Lbl_Proveedor" runat="server" Text="*Proveedor"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" MaxLength="100" TabIndex="11" Width="80%"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" TargetControlID="Txt_Nombre_Proveedor_Solicitud_Pago"
                                    FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                </cc1:FilteredTextBoxExtender>
                                <asp:ImageButton ID="Btn_Buscar_Proveedor_Solicitud_Pagos" 
                                    runat="server" ToolTip="Consultar"
                                    TabIndex="12" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                    onclick="Btn_Buscar_Proveedor_Solicitud_Pagos_Click"/>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="Cmb_Proveedor_Solicitud_Pago" runat="server" TabIndex="13" Width="97%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Lbl_Fecha_Ini" runat="server" Text="*Fecha Inicio"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Height="18px" MaxLength="11" TabIndex="12" AutoPostBack="true"
                                    Width="84%" />                                 
                                <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Inicial" runat="server" Enabled="True"
                                    TargetControlID="Txt_Fecha_Inicial" WatermarkCssClass="watermarked" WatermarkText="Dia/Mes/Año" />
                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicial" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                                    PopupButtonID="Btn_Txt_Fecha_Inicial" TargetControlID="Txt_Fecha_Inicial" />
                                <asp:ImageButton ID="Btn_Txt_Fecha_Inicial" runat="server" CausesValidation="false"
                                    Height="18px" ImageUrl="../imagenes/paginas/SmallCalendar.gif" Style="vertical-align: top;" />
                            </td>
                            <td>
                                <asp:Label ID="Lbl_Fecha_Final" runat="server" Text="*Fecha Final"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Height="18px" MaxLength="11" TabIndex="12" AutoPostBack="true"
                                    Width="84%" />
                                <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Fecha_Final" runat="server" Enabled="True"
                                    TargetControlID="Txt_Fecha_Final" WatermarkCssClass="watermarked" WatermarkText="Dia/Mes/Año" />
                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Final" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                                    PopupButtonID="Btn_Txt_Fecha_Final" TargetControlID="Txt_Fecha_Final" />
                                <asp:ImageButton ID="Btn_Txt_Fecha_Final" runat="server" CausesValidation="false"
                                    Height="18px" ImageUrl="../imagenes/paginas/SmallCalendar.gif" Style="vertical-align: top;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                    </table>                       
               </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Btn_Reporte_Analitico" EventName="Click" />
            <asp:PostBackTrigger ControlID="Btn_Imprimir_Excel"/>
            <asp:PostBackTrigger ControlID="Btn_Salir" />
        </Triggers> 
    </asp:UpdatePanel>
</asp:Content>
