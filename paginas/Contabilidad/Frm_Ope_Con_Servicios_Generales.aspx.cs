﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Cuentas_Gastos.Negocio;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;
using JAPAMI.Ope_Con_Servicios_Generales.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Sessiones;
using System.Text;
using JAPAMI.Solicitud_Pagos.Negocio;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class paginas_Servicios_Generales_Frm_Ope_Con_Servicios_Generales : System.Web.UI.Page
{

    #region Variables

    Cls_Ope_Con_Servicios_Generales_Negocio SG_Negocio;

    #endregion

    #region Page_Load
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Page_Load
    /// DESCRIPCION : Se carga al dar PostBack
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 24/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Text = "";
        Lbl_Mensaje_Error.Visible = false;
        if (String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Session["Activa"] = true;//Variable para mantener la session activa. 
            Llenar_Combo_Fuentes_Financiamiento();
            Llenar_Combo_Cuentas_Gasto();
            Llenar_Combo_Tipos_Solicitud();
            Limpiar_Controles_Formulario();
            Habilitar_Controles_Formulario(false);
        }
    }
    #endregion Page_Load

    #region Metodos

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Combo_Cuentas_Gasto
    /// DESCRIPCION :Llena el Combo de Tipo de Gastos
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 24/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llenar_Combo_Cuentas_Gasto()
    {
        Cls_Cat_Con_Cuentas_Gastos_Negocio CG_Negocio = new Cls_Cat_Con_Cuentas_Gastos_Negocio();
        CG_Negocio.P_Estatus = "ACTIVA";
        DataTable Dt_Datos = CG_Negocio.Consultar_Gastos();
        Cmb_Gasto.DataSource = Dt_Datos;
        Cmb_Gasto.DataTextField = Cat_Con_Cuentas_Gastos.Campo_Descripcion;
        Cmb_Gasto.DataValueField = Cat_Con_Cuentas_Gastos.Campo_Id_Gasto;
        Cmb_Gasto.DataBind();
        Cmb_Gasto.Items.Insert(0, new ListItem("<- Seleccione ->", "00000"));
        Cmb_Gastos_SelectedIndexChanged(Cmb_Gasto, null);
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Combo_Tipos_Solicitud
    /// DESCRIPCION :Llena el Combo de Tipo de Solicitudes de Pago
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 24/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llenar_Combo_Tipos_Solicitud()
    {
        Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio TSP_Negocio = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        TSP_Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Datos = TSP_Negocio.Consulta_Tipo_Solicitud_Pagos_Combo();
        Cmb_Tipo_Solicitud_Pago.DataSource = Dt_Datos;
        Cmb_Tipo_Solicitud_Pago.DataTextField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion;
        Cmb_Tipo_Solicitud_Pago.DataValueField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
        Cmb_Tipo_Solicitud_Pago.DataBind();
        Cmb_Tipo_Solicitud_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Combo_Fuentes_Financiamiento
    /// DESCRIPCION :Llena el Combo de Fuentes Financiamiento
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llenar_Combo_Fuentes_Financiamiento()
    {
        SG_Negocio = new Cls_Ope_Con_Servicios_Generales_Negocio();
        SG_Negocio.P_Estatus = "ACTIVO";
        SG_Negocio.P_Anio = DateTime.Now.Year;
        DataTable Dt_Datos = SG_Negocio.Consultar_Fuentes_Financiamiento();
        Cmb_Fuente_Financiamiento.DataSource = Dt_Datos;
        Cmb_Fuente_Financiamiento.DataTextField = "FTE_FINANCIAMIENTO";
        Cmb_Fuente_Financiamiento.DataValueField = "FTE_FINANCIAMIENTO_ID";
        Cmb_Fuente_Financiamiento.DataBind();
        Cmb_Fuente_Financiamiento.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Cuenta_Contable_Proveedor
    /// DESCRIPCION :Carga el número y la Descripción de la Cuenta Contable del Proveedor
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 26/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Cuenta_Contable_Proveedor(String Cuenta_Contable)
    {
        if (!String.IsNullOrEmpty(Cuenta_Contable))
        {
            Txt_Cuenta_Contable_Proveedor.Text = Cuenta_Contable;
            Txt_Cuenta_Contable_Proveedor.ForeColor = System.Drawing.Color.Black;
        }
        else
        {
            Txt_Cuenta_Contable_Proveedor.Text = "EL PROVEEDOR NO TIENE UNA CUENTA CONTABLE, FAVOR DE REVISARLO CON CONTABILIDAD.";
            Txt_Cuenta_Contable_Proveedor.ForeColor = System.Drawing.Color.Red;
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Generales_Formulario
    /// DESCRIPCION :Limpia los Datos del Formulario
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 28/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles_Formulario()
    {
        Cmb_Fuente_Financiamiento.SelectedIndex = 0;
        Cmb_Gasto.SelectedIndex = 0;
        Cmb_Gastos_SelectedIndexChanged(Cmb_Gasto, null);
        Cmb_Tipo_Solicitud_Pago.SelectedIndex = 0;
        Txt_Nombre_Proveedor_Solicitud_Pago.Text = "";
        Btn_Buscar_Proveedor_Solicitud_Pagos_Click(Btn_Buscar_Proveedor_Solicitud_Pagos, null);
        Txt_No_Factura.Text = "";
        Txt_Fecha_Factura.Text = "__/___/____";
        Txt_Conceptos.Text = "";
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles_Formulario
    /// DESCRIPCION :Habilita los campos del Formulario
    /// PARAMETROS  :  Estado. Si se habilita o no los controles.
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 28/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles_Formulario(Boolean Estado)
    {
        if (Estado)
        {
            Btn_Nuevo.ToolTip = "Guardar";
            Btn_Nuevo.AlternateText = "Guardar";
            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
            Btn_Salir.ToolTip = "Cancelar";
            Btn_Salir.AlternateText = "Cancelar";
            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
        }
        else
        {
            Btn_Nuevo.ToolTip = "Nuevo";
            Btn_Nuevo.AlternateText = "Nuevo";
            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
            Btn_Salir.ToolTip = "Salir";
            Btn_Salir.AlternateText = "Salir";
            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
        }
        Cmb_Fuente_Financiamiento.Enabled = Estado;
        Cmb_Gasto.Enabled = Estado;
        Cmb_Tipo_Solicitud_Pago.Enabled = Estado;
        Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = Estado;
        Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = Estado;
        Cmb_Proveedor_Solicitud_Pago.Enabled = Estado;
        Txt_No_Factura.Enabled = Estado;
        Txt_Fecha_Factura.Enabled = Estado;
        Btn_Fecha_Factura.Enabled = Estado;
        Txt_Conceptos.Enabled = Estado;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validacion_Alta
    /// DESCRIPCION :Valida antes de dar de alta.
    /// PARAMETROS  :   
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 28/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validacion_Alta()
    {
        Boolean Aceptado = true;
        StringBuilder Mensaje = new StringBuilder();
        if (Cmb_Fuente_Financiamiento.SelectedIndex == 0)
        {
            Mensaje.Append("- Debe seleccionarse la Fuente de Financiamiento. <br />");
        }
        if (Cmb_Gasto.SelectedIndex == 0)
        {
            Mensaje.Append("- Debe seleccionarse el Gasto. <br />");
        }
        if (Cmb_Tipo_Solicitud_Pago.SelectedIndex == 0)
        {
            Mensaje.Append("- Debe seleccionarse el Tipo de Solicitud de Pago. <br />");
        }
        if (Cmb_Proveedor_Solicitud_Pago.SelectedIndex == 0 && Hdf_Cuenta_Contable_Proveedor.Value.Trim().Length == 0)
        {
            Mensaje.Append("- Debe seleccionarse el Proveedor. <br />");
        }
        if (Hdf_Cuenta_Contable_Proveedor.Value.Trim().Length == 0)
        {
            Mensaje.Append("- El Proveedor no tiene Cuenta Contable asignada. <br />");
        }
        if (Txt_No_Factura.Text.Trim().Length == 0)
        {
            Mensaje.Append("- Debe introducirse el Número de Documento. <br />");
        }
        if (Txt_Fecha_Factura.Text.Trim().Length == 0 || Txt_Fecha_Factura.Text.Trim().Equals("__/___/____"))
        {
            Mensaje.Append("- Debe introducirse y/o seleccionarse la Fecha de la Factura. <br />");
        }
        if (Txt_Conceptos.Text.Trim().Length == 0)
        {
            Mensaje.Append("- Debe introducirse el Concepto del Pago. <br />");
        }
        if (Convert.ToDouble(Txt_Total_Total.Text.Trim().Replace(",", "")) == 0)
        {
            Mensaje.Append("- Debe introducirse el(los) montos a pagar. <br />");
        }
        Validacion_Presupuestal(ref Mensaje);
        if (Mensaje.Length > 0)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode("<b>VERIFICAR... </b><br />" + Mensaje.ToString());
            Lbl_Mensaje_Error.Visible = true;
            Aceptado = false;
        }
        return Aceptado;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validacion_Presupuestal
    /// DESCRIPCION :Hace la validación presupuestal de los montos de Cada Dependencia.
    /// PARAMETROS  :   
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Validacion_Presupuestal(ref StringBuilder Mensaje)
    {
        foreach (GridViewRow Fila_Grid in Grid_Detalles_Pago.Rows)
        {
            TextBox Txt_Total_Tmp = (TextBox)Fila_Grid.FindControl("Txt_Total");
            Double Total_Dep_Facturado = Convert.ToDouble(Txt_Total_Tmp.Text.Trim().Replace(",", ""));
            if (Total_Dep_Facturado > 0)
            {
                Double Total_Dep_Disponible = Convert.ToDouble(HttpUtility.HtmlDecode(Fila_Grid.Cells[9].Text.Trim()).Trim().Replace(",", ""));
                if (Total_Dep_Facturado > Total_Dep_Disponible)
                {
                    Mensaje.Append("- La Unidad Responsable '<b>" + HttpUtility.HtmlDecode(Fila_Grid.Cells[5].Text.Trim()).Trim().Replace(",", "") + "</b>' no cuenta con Suficiencia Presupuestal. Excede por: <b>" + String.Format("{0:c}", (Total_Dep_Facturado - Total_Dep_Disponible)) + "</b>. <br />");
                }
            }
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Reserva_Solicitud
    /// DESCRIPCION :Hace la validación presupuestal de los montos de Cada Dependencia.
    /// PARAMETROS  :   
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Alta_Reserva_Solicitud()
    {
        String Reserva_Solicitud = String.Empty;
        SG_Negocio = new Cls_Ope_Con_Servicios_Generales_Negocio();
        SG_Negocio.P_Gasto_ID = Cmb_Gasto.SelectedItem.Value;
        SG_Negocio.P_Tipo_Solicitud_ID = Cmb_Tipo_Solicitud_Pago.SelectedItem.Value;
        SG_Negocio.P_Proveedor_ID = Cmb_Proveedor_Solicitud_Pago.SelectedItem.Value;
        SG_Negocio.P_Aprox_Proveedor = Cmb_Proveedor_Solicitud_Pago.SelectedItem.Text.Trim();
        SG_Negocio.P_Cuenta_Contable_Proveedor_ID = Hdf_Cuenta_Contable_Proveedor.Value;
        SG_Negocio.P_No_Factura = Txt_No_Factura.Text.Trim();
        SG_Negocio.P_Fecha_Factura = Convert.ToDateTime(Txt_Fecha_Factura.Text);
        SG_Negocio.P_Concepto = Txt_Conceptos.Text.Trim();
        SG_Negocio.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
        SG_Negocio.P_Usuario_Nombre = Cls_Sessiones.Nombre_Empleado;
        SG_Negocio.P_Total = Convert.ToDouble(Txt_Total_Total.Text.Trim().Replace(",", ""));
        SG_Negocio.P_Dt_Detalles_Presupuesto = Obtener_Detalles_Presupuesto();
        SG_Negocio.P_Dt_Detalles_Reserva = Obtener_Detalles_Reserva();
        SG_Negocio.P_Dt_Detalles_Solicitud_Pago = Obtener_Detalles_Solicitud_Pago();
        Reserva_Solicitud = SG_Negocio.Alta_Reserva_Solicitud_Pago();
        return Reserva_Solicitud;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Obtener_Detalles_Presupuesto
    /// DESCRIPCION :Obtiene los Detalles.
    /// PARAMETROS  :   
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private DataTable Obtener_Detalles_Presupuesto()
    {
        DataTable Dt_Partidas = new DataTable();
        Dt_Partidas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("ANIO", System.Type.GetType("System.String"));
        foreach (GridViewRow Fila_Grid in Grid_Detalles_Pago.Rows)
        {
            TextBox Txt_Total_Tmp = (TextBox)Fila_Grid.FindControl("Txt_Total");
            Double Total_Dep_Facturado = Convert.ToDouble(Txt_Total_Tmp.Text.Trim().Replace(",", ""));
            if (Total_Dep_Facturado > 0)
            {
                DataRow Dr_Fila_Nueva = Dt_Partidas.NewRow();
                Dr_Fila_Nueva["DEPENDENCIA_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[3].Text.Trim()).Trim();
                Dr_Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = Cmb_Fuente_Financiamiento.SelectedItem.Value;
                Dr_Fila_Nueva["PROGRAMA_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[14].Text.Trim()).Trim();
                Dr_Fila_Nueva["PARTIDA_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[0].Text.Trim()).Trim();
                Dr_Fila_Nueva["IMPORTE"] = Total_Dep_Facturado;
                Dr_Fila_Nueva["ANIO"] = DateTime.Now.Year;
                Dt_Partidas.Rows.Add(Dr_Fila_Nueva);
            }
        }
        return Dt_Partidas;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Obtener_Detalles_Reserva
    /// DESCRIPCION :Obtiene los Detalles.
    /// PARAMETROS  :   
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private DataTable Obtener_Detalles_Reserva()
    {
        DataTable Dt_Partidas = new DataTable();
        Dt_Partidas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("ANIO", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
        foreach (GridViewRow Fila_Grid in Grid_Detalles_Pago.Rows)
        {
            TextBox Txt_Total_Tmp = (TextBox)Fila_Grid.FindControl("Txt_Total");
            Double Total_Dep_Facturado = Convert.ToDouble(Txt_Total_Tmp.Text.Trim().Replace(",", ""));
            if (Total_Dep_Facturado > 0)
            {
                DataRow Dr_Fila_Nueva = Dt_Partidas.NewRow();
                Dr_Fila_Nueva["DEPENDENCIA_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[3].Text.Trim()).Trim();
                Dr_Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = Cmb_Fuente_Financiamiento.SelectedItem.Value;
                Dr_Fila_Nueva["PROGRAMA_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[14].Text.Trim()).Trim();
                Dr_Fila_Nueva["PARTIDA_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[0].Text.Trim()).Trim();
                Dr_Fila_Nueva["IMPORTE"] = Total_Dep_Facturado;
                Dr_Fila_Nueva["ANIO"] = DateTime.Now.Year;
                Dr_Fila_Nueva["CAPITULO_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[15].Text.Trim()).Trim();
                Dt_Partidas.Rows.Add(Dr_Fila_Nueva);
            }
        }
        return Dt_Partidas;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Obtener_Detalles_Solicitud_Pago
    /// DESCRIPCION :Obtiene los Detalles de Solicitud de Pago.
    /// PARAMETROS  :   
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private DataTable Obtener_Detalles_Solicitud_Pago()
    {
        DataTable Dt_Partidas = new DataTable();
        Dt_Partidas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("IVA", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("TOTAL", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("ANIO", System.Type.GetType("System.String"));
        Dt_Partidas.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
        foreach (GridViewRow Fila_Grid in Grid_Detalles_Pago.Rows)
        {
            TextBox Txt_Importe_Tmp = (TextBox)Fila_Grid.FindControl("Txt_Importe");
            TextBox Txt_IVA_Tmp = (TextBox)Fila_Grid.FindControl("Txt_IVA");
            TextBox Txt_Total_Tmp = (TextBox)Fila_Grid.FindControl("Txt_Total");
            Double Importe_Dep_Facturado = Convert.ToDouble(Txt_Importe_Tmp.Text.Trim().Replace(",", ""));
            Double IVA_Dep_Facturado = Convert.ToDouble(Txt_IVA_Tmp.Text.Trim().Replace(",", ""));
            Double Total_Dep_Facturado = Convert.ToDouble(Txt_Total_Tmp.Text.Trim().Replace(",", ""));
            if (Total_Dep_Facturado > 0)
            {
                DataRow Dr_Fila_Nueva = Dt_Partidas.NewRow();
                Dr_Fila_Nueva["DEPENDENCIA_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[3].Text.Trim()).Trim();
                Dr_Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = Cmb_Fuente_Financiamiento.SelectedItem.Value;
                Dr_Fila_Nueva["PROGRAMA_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[14].Text.Trim()).Trim();
                Dr_Fila_Nueva["PARTIDA_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[0].Text.Trim()).Trim();
                Dr_Fila_Nueva["IMPORTE"] = Importe_Dep_Facturado;
                Dr_Fila_Nueva["IVA"] = IVA_Dep_Facturado;
                Dr_Fila_Nueva["TOTAL"] = Total_Dep_Facturado;
                Dr_Fila_Nueva["ANIO"] = DateTime.Now.Year;
                Dr_Fila_Nueva["CAPITULO_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[15].Text.Trim()).Trim();
                Dt_Partidas.Rows.Add(Dr_Fila_Nueva);
            }
        }
        return Dt_Partidas;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String Numero_Solicitud)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        try
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            Ds_Reporte = new DataSet();
            Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
            Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode("<b>VERIFICAR... </b><br />" + Ex.Message.Trim());
            Lbl_Mensaje_Error.Visible = true;
        }

    }

    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Archivo(Nombre_Reporte_Generar + Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE:             Mostrar_Archivo
    /// DESCRIPCIÓN:        Muestra el Archivo
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    private void Mostrar_Archivo(String URL)
    {
        string Pagina = "../../Reporte/";
        try
        {
            Pagina = Pagina + URL;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "B1",
                "window.open('" + Pagina + "', 'Solicitud Pago','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el archivo de dispersion generado generado. Error: [" + Ex.Message + "]");
        }
    }

    #endregion Metodos

    #region Grids

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Detalles_Pago_RowDataBound
    /// DESCRIPCION :Carga Propiedades cuando se esta en construcción la tabla
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 26/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Detalles_Pago_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String Clave_Descripcion_Partida = HttpUtility.HtmlDecode(e.Row.Cells[2].Text.Trim()).Trim();
                String Clave_Descripcion_Dependencia = HttpUtility.HtmlDecode(e.Row.Cells[5].Text.Trim()).Trim();
                String Clave_Descripcion_Programa = HttpUtility.HtmlDecode(e.Row.Cells[8].Text.Trim()).Trim();
                e.Row.Cells[1].ToolTip = Clave_Descripcion_Partida;
                e.Row.Cells[4].ToolTip = Clave_Descripcion_Dependencia;
                e.Row.Cells[6].ToolTip = Clave_Descripcion_Programa;
                e.Row.Cells[9].Font.Bold = true;
                e.Row.Cells[9].ForeColor = System.Drawing.Color.Black;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message;
            Lbl_Mensaje_Error.Visible = true;
        }
    }

    #endregion Grids

    #region Eventos

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Gatos_SelectedIndexChanged
    /// DESCRIPCION :Llena las Unidades Responsables Segun el Gasto Seleccionado.
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 24/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Gastos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Grid_Detalles_Pago.DataSource = new DataTable();
        Grid_Detalles_Pago.DataBind();
        Txt_Total_Importe.Text = "0.00";
        Txt_Total_IVA.Text = "0.00";
        Txt_Total_Total.Text = "0.00";
        String Gasto_Seleccionado = Cmb_Gasto.SelectedItem.Value;
        if (!String.IsNullOrEmpty(Gasto_Seleccionado))
        {
            Cls_Ope_Con_Servicios_Generales_Negocio SP_Negocio = new Cls_Ope_Con_Servicios_Generales_Negocio();
            SP_Negocio.P_Cuenta_Gasto_ID = Gasto_Seleccionado.Trim();
            DataTable Dt_Gasto_Seleccionado = SP_Negocio.Consultar_Partida_Gasto();
            if (Dt_Gasto_Seleccionado != null)
            {
                if (Dt_Gasto_Seleccionado.Rows.Count > 0)
                {
                    String Partida_ID = String.Empty;
                    foreach (DataRow Fila_Actual in Dt_Gasto_Seleccionado.Rows)
                    {
                        if (!String.IsNullOrEmpty(Partida_ID)) Partida_ID += ",";
                        Partida_ID += "'" + Fila_Actual["PARTIDA_ID"] + "'";
                    }
                    Partida_ID = Partida_ID.Trim('\'');
                    SG_Negocio = new Cls_Ope_Con_Servicios_Generales_Negocio();
                    SG_Negocio.P_Partida_ID = Partida_ID;
                    SG_Negocio.P_Anio = DateTime.Now.Year;
                    SG_Negocio.P_Mes = String.Format("{0:'DISPONIBLE_'MMMMMMMMMMMMMMMMMMMMMMMMM}", DateTime.Today).ToUpper().Trim();
                    DataTable Dt_Dependencias = SG_Negocio.Consultar_Dependencias_Partida();
                    Grid_Detalles_Pago.Columns[0].Visible = true;
                    Grid_Detalles_Pago.Columns[2].Visible = true;
                    Grid_Detalles_Pago.Columns[3].Visible = true;
                    Grid_Detalles_Pago.Columns[5].Visible = true;
                    Grid_Detalles_Pago.Columns[7].Visible = true;
                    Grid_Detalles_Pago.Columns[8].Visible = true;
                    Grid_Detalles_Pago.Columns[10].Visible = true;
                    Grid_Detalles_Pago.Columns[14].Visible = true;
                    Grid_Detalles_Pago.Columns[15].Visible = true;
                    Grid_Detalles_Pago.DataSource = Dt_Dependencias;
                    Grid_Detalles_Pago.DataBind();
                    Grid_Detalles_Pago.Columns[0].Visible = false;
                    Grid_Detalles_Pago.Columns[2].Visible = false;
                    Grid_Detalles_Pago.Columns[3].Visible = false;
                    Grid_Detalles_Pago.Columns[5].Visible = false;
                    Grid_Detalles_Pago.Columns[7].Visible = false;
                    Grid_Detalles_Pago.Columns[8].Visible = false;
                    Grid_Detalles_Pago.Columns[10].Visible = false;
                    Grid_Detalles_Pago.Columns[14].Visible = false;
                    Grid_Detalles_Pago.Columns[15].Visible = false;
                }
            }
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Buscar_Proveedor_Solicitud_Pagos_Click
    /// DESCRIPCION :Llena el Combo de Proveedor deacuerdo a la Busqueda
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 24/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Buscar_Proveedor_Solicitud_Pagos_Click(object sender, ImageClickEventArgs e)
    {
        Hdf_Cuenta_Contable_Proveedor.Value = "";
        Txt_Cuenta_Contable_Proveedor.Text = "";
        Cmb_Proveedor_Solicitud_Pago.Items.Clear();
        Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", "0000000000"));
        String Proveedor_Busqueda_Aproximacion = Txt_Nombre_Proveedor_Solicitud_Pago.Text.Trim();
        if (!String.IsNullOrEmpty(Proveedor_Busqueda_Aproximacion))
        {
            SG_Negocio = new Cls_Ope_Con_Servicios_Generales_Negocio();
            SG_Negocio.P_Aprox_Proveedor = Proveedor_Busqueda_Aproximacion.Trim();
            SG_Negocio.P_Estatus = "ACTIVO";
            DataTable Dt_Datos = SG_Negocio.Consultar_Proveedores();
            if (Dt_Datos != null)
            {
                if (Dt_Datos.Rows.Count > 0)
                {
                    Cmb_Proveedor_Solicitud_Pago.DataSource = Dt_Datos;
                    Cmb_Proveedor_Solicitud_Pago.DataTextField = "NOMBRE_PROVEEDOR";
                    Cmb_Proveedor_Solicitud_Pago.DataValueField = "PROVEEDOR_ID";
                    Cmb_Proveedor_Solicitud_Pago.DataBind();
                    if (Dt_Datos.Rows.Count == 1)
                    {
                        Cmb_Proveedor_Solicitud_Pago.SelectedIndex = Cmb_Proveedor_Solicitud_Pago.Items.IndexOf(Cmb_Proveedor_Solicitud_Pago.Items.FindByValue(Dt_Datos.Rows[0]["PROVEEDOR_ID"].ToString().Trim()));
                        Hdf_Cuenta_Contable_Proveedor.Value = Dt_Datos.Rows[0]["CUENTA_CONTABLE_ID"].ToString();
                        Cargar_Cuenta_Contable_Proveedor(Dt_Datos.Rows[0]["CUENTA_DESCRIPCION"].ToString());
                    }
                    else
                    {
                        Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                    }
                }
            }
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Nuevo_Click
    /// DESCRIPCION :Habilita los Controles y Guarda una nueva reserva y Solicitud de Pago
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 28/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Limpiar_Controles_Formulario();
                Habilitar_Controles_Formulario(true);
            }
            else
            {
                if (Validacion_Alta())
                {
                    String Mensaje_Exito = Alta_Reserva_Solicitud();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito [" + Mensaje_Exito + "].');", true);
                    Limpiar_Controles_Formulario();
                    Habilitar_Controles_Formulario(false);
                    Imprimir(SG_Negocio.P_No_Solicitud_Pago);
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode("<b>VERIFICAR... </b><br />" + Ex.Message.Trim());
            Lbl_Mensaje_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Salir_Click
    /// DESCRIPCION : Cancela la operación o regresa a la pantalla inicial.
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 28/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.ToolTip == "Salir")
        {
            Limpiar_Controles_Formulario();
            Habilitar_Controles_Formulario(false);
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else
        {
            Limpiar_Controles_Formulario();
            Habilitar_Controles_Formulario(false);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Proveedor_Solicitud_Pago_SelectedIndexChanged
    /// DESCRIPCION :Carga la Cuenta Contable del Proveedor cada que se realiza el cambio.
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 26/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Proveedor_Solicitud_Pago_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Txt_Cuenta_Contable_Proveedor.Text = "";
            Hdf_Cuenta_Contable_Proveedor.Value = "";
            if (Cmb_Proveedor_Solicitud_Pago.SelectedIndex > 0)
            {
                SG_Negocio = new Cls_Ope_Con_Servicios_Generales_Negocio();
                SG_Negocio.P_Proveedor_ID = Cmb_Proveedor_Solicitud_Pago.SelectedItem.Value;
                DataTable Dt_Proveedor = SG_Negocio.Consultar_Proveedores();
                if (Dt_Proveedor.Rows.Count > 0)
                {
                    Hdf_Cuenta_Contable_Proveedor.Value = Dt_Proveedor.Rows[0]["CUENTA_CONTABLE_ID"].ToString();
                    Cargar_Cuenta_Contable_Proveedor(Dt_Proveedor.Rows[0]["CUENTA_DESCRIPCION"].ToString());
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = "Verificar. " + Ex.Message;
            Lbl_Mensaje_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Unidad_Responsable_Busqueda_SelectedIndexChanged
    /// DESCRIPCION :Carga el Programa y el Disponible en el Presupuesto.
    /// PARAMETROS  :  
    /// CREO        : Francisco Gallardo
    /// FECHA_CREO  : 26/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Unidad_Responsable_Busqueda_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = "Verificar. " + Ex.Message;
            Lbl_Mensaje_Error.Visible = true;
        }
    }

    #endregion Eventos

}
