﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Reflection;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;
using System.Text;

public partial class paginas_Contabilidad_Frm_Rpt_Con_Estado_Cuentas : System.Web.UI.Page
{
    #region (Load/Init)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Valida que exista algun usuario logueado al sistema.
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Session["Activa"] = true;//Variable para mantener la session activa.
                Limpia_Controles();//Limpia los controles de la forma

                //Colocar el valor del campo oculto de la CONAC
                Txt_Campo_Oculto.Text = Consulta_Campo_Oculto_CONAC();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
    #region (Metodos)
    #region (Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 21-Febrero-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Cmb_Tipo_Reporte.SelectedIndex = 0;
            Cmb_Anio.SelectedIndex = 0;
            Cmb_Mes.SelectedIndex = 0;
            Session["3210_Actual"] = "";
            Session["3210_Anterior"] = "";
            Llenar_Combo_Anio();
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO  : 21-Febrero-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    private void Abrir_Ventana(String Nombre_Archivo)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
        try
        {
            Pagina = Pagina + Nombre_Archivo;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
        }
    }
    #endregion
    #region(Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Reporte
    /// DESCRIPCION : Validar que se se encuentre todos los datos para continuar con el reporte
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 18/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Reporte()
    {
        String Espacios_Blanco;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Lbl_Mensaje_Error.Text += Espacios_Blanco + "Es necesario: <br>";
        Lbl_Mensaje_Error.Visible = true;
        Img_Error.Visible = true;
        if (Cmb_Tipo_Reporte.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Selecionar algun tipo de reporte.<br>";
            Datos_Validos = false;
        }
        if (Cmb_Anio.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Selecionar algun año.<br>";
            Datos_Validos = false;
        }
        if (Cmb_Mes.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Selecionar algun mes.<br>";
            Datos_Validos = false;
        }
        return Datos_Validos;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Fecha
    ///DESCRIPCIÓN:          Metodo que permite generar la cadena de la fecha y valida las fechas
    ///PARAMETROS:   
    ///CREO:                 Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:           21/Febrero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public bool Validar_Fecha()
    {
      Boolean Fecha_Valida = true;
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            return Fecha_Valida;
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion
    #region (Consulta)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Estado
    /// DESCRIPCION : Consulta todos los movimientos con sus detalles de las pólizas
    ///               realizadas en el rango de fechas proporcionadas por el usuario
    /// PARAMETROS  : String Formato.- Para saber que formato sera el archivo pdf, excel
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 21/Febrero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Estado(String Formato)
    {
        Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Estado_Cuentas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta_Situacion_Financiera = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Tipo_Reporte = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Periodo_Anterior = new DataTable(); 
        DataTable Dt_Consulta_Tem = new DataTable();
        DataTable Dt_Periodo_Anterior_Tem = new DataTable();        
        Ds_Rpt_Con_Sit_Fin Ds_Reporte = new Ds_Rpt_Con_Sit_Fin();
        Ds_Rpt_Con_Estado_Actividades Ds_Actividades = new Ds_Rpt_Con_Estado_Actividades();
        Ds_Rpt_Con_Estado_Variaciones_Hacienda_Publica Ds_Variaciones = new Ds_Rpt_Con_Estado_Variaciones_Hacienda_Publica();
        ReportDocument Reporte = new ReportDocument();
        String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
        String Nombre_Archivo = Cmb_Tipo_Reporte.SelectedValue.ToString().Trim().Replace(" ", "_") + "_" + Session.SessionID + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MMMM'-'dd'_'HH'-'mm'-'ss"));
        String Mes_Anterior = "";
        String Nombre = "";
        DataSet Ds_Esf_01 = new DataSet();
        DataSet Ds_Esf_02 = new DataSet();
        DataSet Ds_Reporte_Excel = new DataSet();
        DataTable Dt_Consulta_Anterior = new DataTable();
        String Tipo = "";
        String Mes="";
        String Mes_Acumulado = "";
        String Saldo_Anterior = "0";
        String Saldo_Anterior_Primero = "0";
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            //  para saber el nombre que llevara el reporte inicio y se realiza la consulta
            DataRow Dt_Row = Dt_Tipo_Reporte.NewRow();
            if (Cmb_Tipo_Reporte.SelectedValue == "SITUACIÓN FINANCIERA")
            {
                Tipo = "ESTADO DE "+Cmb_Tipo_Reporte.SelectedValue;
                Dt_Tipo_Reporte.Columns.Add("NOMBRE");
                Dt_Tipo_Reporte.Columns.Add("MES");
                Dt_Tipo_Reporte.Columns.Add("AÑO");
                Dt_Row["AÑO"] = " " + Cmb_Anio.SelectedValue;
                Dt_Tipo_Reporte.TableName = "Dt_Tipo_Reporte";
                Dt_Row["NOMBRE"] = " " + Cmb_Tipo_Reporte.SelectedValue;
                Mes = Cmb_Mes.SelectedValue;
                Mes = Mes.Substring(0, 2);
                Mes = Asignar_Mes(Mes);
                Dt_Row["MES"] = Mes;
                //  se realiza la consulta
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1";
                Rs_Consulta_Situacion_Financiera.P_Filtro2 = "2";
                Rs_Consulta_Situacion_Financiera.P_Filtro3 = "3";
                Dt_Consulta = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

                Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1";
                Rs_Consulta_Situacion_Financiera.P_Filtro2 = "2";
                Rs_Consulta_Situacion_Financiera.P_Filtro3 = "3";
                Dt_Consulta_Anterior = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                //Obtener el 3210
                //  se realiza la consulta
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                Dt_Consulta_Tem = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                //  para el periodo de enero
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                Dt_Periodo_Anterior_Tem = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                //  para construir la tabla
                Dt_Consulta_Tem = Generar_Tabla_Estado_Actividades(Dt_Consulta_Tem, Dt_Periodo_Anterior_Tem);
                //Generar la consulta
                Dt_Consulta = Generar_Tabla_Final(Dt_Consulta, Dt_Consulta_Anterior, Session["3210_Actual"].ToString(), Session["3210_Anterior"].ToString());
                //  carga la informacion al dataset
                Dt_Tipo_Reporte.Rows.Add(Dt_Row);
                Dt_Consulta.TableName = "Dt_Sin_Fin";
                Ds_Reporte.Clear();
                Ds_Reporte.Tables.Clear();
                Ds_Reporte.Tables.Add(Dt_Consulta.Copy());
                Ds_Reporte.Tables.Add(Dt_Tipo_Reporte.Copy());
                Ds_Reporte_Excel.Clear();
                Ds_Reporte_Excel.Tables.Clear();
                Ds_Reporte_Excel.Tables.Add(Dt_Tipo_Reporte.Copy());
                Ds_Reporte_Excel.Tables.Add(Dt_Consulta.Copy());
                Reporte.Load(Ruta_Archivo + "Rpt_Con_Sit_Fin.rpt");
                Reporte.SetDataSource(Ds_Reporte);
                Nombre = "01_ESF_15_AP";
            }
            else if (Cmb_Tipo_Reporte.SelectedValue == "ESTADO DE ACTIVIDADES/RESULTADOS")
            {
                Tipo = Cmb_Tipo_Reporte.SelectedValue;
                Dt_Tipo_Reporte.Columns.Add("AÑO");
                Dt_Tipo_Reporte.Columns.Add("MES");
                Dt_Tipo_Reporte.TableName = "Dt_Periodo";
                Dt_Row["AÑO"] = " " + Cmb_Anio.SelectedValue;
                Mes = Cmb_Mes.SelectedValue;
                Mes = Mes.Substring(0, 2);
                Mes = Asignar_Mes(Mes);
                Dt_Row["MES"] = Mes;
                //  se realiza la consulta
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                Dt_Consulta = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                //  para el periodo de enero
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                Dt_Periodo_Anterior = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                //  para construir la tabla
                Dt_Consulta = Generar_Tabla_Estado_Actividades(Dt_Consulta, Dt_Periodo_Anterior);
                //  carga la informacion al dataset
                Dt_Tipo_Reporte.Rows.Add(Dt_Row);
                Dt_Consulta.TableName = "Dt_Estado_Actividades";
                Ds_Actividades.Clear();
                Ds_Actividades.Tables.Clear();
                Ds_Actividades.Tables.Add(Dt_Consulta.Copy());
                Ds_Actividades.Tables.Add(Dt_Tipo_Reporte.Copy());
                Ds_Reporte_Excel.Clear();
                Ds_Reporte_Excel.Tables.Clear();
                Ds_Reporte_Excel.Tables.Add(Dt_Tipo_Reporte.Copy());
                Ds_Reporte_Excel.Tables.Add(Dt_Consulta.Copy());
                Nombre = "02_EA_15_AP";
            }
            else if (Cmb_Tipo_Reporte.SelectedValue == "VARIACIONES EN LA HACIENDA PÚBLICA")
            {
                Tipo = Cmb_Tipo_Reporte.SelectedValue;
                Dt_Tipo_Reporte.Columns.Add("AÑO");
                Dt_Tipo_Reporte.Columns.Add("MES");
                Dt_Tipo_Reporte.TableName = "Dt_Periodo";
                Dt_Row["AÑO"] = " " + Cmb_Anio.SelectedValue;
                Mes = Cmb_Mes.SelectedValue;
                Mes = Mes.Substring(0, 2);
                Mes = Asignar_Mes(Mes);
                Dt_Row["MES"] = Mes;
                Mes_Anterior = Periodo_Anterior(Cmb_Mes.SelectedValue.ToString());
                Mes_Acumulado =Cmb_Mes.SelectedValue.ToString();
                for (Int16 Cont_Mensual = Convert.ToInt16(Cmb_Mes.SelectedValue.Substring(0, 2)); Cont_Mensual > 1; Cont_Mensual--)
                    {
                        Mes_Acumulado = Mes_Acumulado + "' OR Cierre.MES_ANIO='" + String.Format("{0:00}", (Cont_Mensual - 1)) + Mes_Anterior.Substring(2, 2);
                    }
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = Mes_Acumulado;
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "3";
                Dt_Consulta = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + String.Format("{0:00}", Convert.ToInt16(Cmb_Mes.SelectedValue.Substring(2, 2)) - 1);
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "3";
                Dt_Periodo_Anterior = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                 //Obtener el 3210
                //  se realiza la consulta
                if (Mes_Anterior.Substring(0, 2) != "12")
                {
                }
                else
                {
                    Mes_Anterior = Cmb_Mes.SelectedValue.ToString();
                }
                Dt_Consulta_Tem = new DataTable();
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = Mes_Anterior; 
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                Dt_Consulta_Tem = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                //  para el periodo de enero
                Dt_Periodo_Anterior_Tem = new DataTable();
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                Dt_Periodo_Anterior_Tem = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                //  para construir la tabla
                Dt_Consulta_Tem = Generar_Tabla_Estado_Actividades(Dt_Consulta_Tem, Dt_Periodo_Anterior_Tem);
                Saldo_Anterior = Session["3210_Actual"].ToString();
                Saldo_Anterior_Primero = Session["3210_Anterior"].ToString();
                Dt_Consulta_Tem = new DataTable();
                if (Convert.ToInt16(Cmb_Mes.SelectedValue.Substring(0, 2)) == 1)
                {
                    Rs_Consulta_Situacion_Financiera.P_Mes_Año = "13" + String.Format("{0:00}", Convert.ToInt16(Cmb_Mes.SelectedValue.Substring(2, 2)) - 1);
                    Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                    Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                    Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                    Dt_Consulta_Tem = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                }
                else
                {
                    Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue.ToString();
                    Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                    Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                    Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                    Dt_Consulta_Tem = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                }
                //  para el periodo de enero
                Dt_Periodo_Anterior_Tem = new DataTable();
                Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);
                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                Dt_Periodo_Anterior_Tem = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                //  para construir la tabla
                Dt_Consulta_Tem = Generar_Tabla_Estado_Actividades(Dt_Consulta_Tem, Dt_Periodo_Anterior_Tem);
                Dt_Consulta = Generar_Tabla_Variaciones(Dt_Consulta, Dt_Periodo_Anterior, Saldo_Anterior.Replace(",", ""), Saldo_Anterior_Primero.Replace(",", ""), Session["3210_Actual"].ToString());
                //  carga la informacion al dataset
                Dt_Tipo_Reporte.Rows.Add(Dt_Row);
                Dt_Consulta.TableName = "Dt_Variaciones";
                Ds_Reporte.Clear();
                Ds_Reporte.Tables.Clear();
                Ds_Reporte.Tables.Add(Dt_Consulta.Copy());
                Ds_Reporte.Tables.Add(Dt_Tipo_Reporte.Copy());
                Ds_Reporte_Excel.Clear();
                Ds_Reporte_Excel.Tables.Clear();
                Ds_Reporte_Excel.Tables.Add(Dt_Tipo_Reporte.Copy());
                Ds_Reporte_Excel.Tables.Add(Dt_Consulta.Copy());
                Nombre = "03_EVHP_15_AP";
            }
            if (Formato == "PDF")
            {
                switch (Cmb_Tipo_Reporte.SelectedItem.Value)
                {
                    case "SITUACIÓN FINANCIERA":
                        Generar_Reporte(ref Ds_Reporte_Excel, "Rpt_Con_Sit_Fin.rpt", Nombre_Archivo + ".pdf");
                        break;
                    case "ESTADO DE ACTIVIDADES/RESULTADOS":
                        Generar_Reporte(ref Ds_Reporte_Excel, "Rpt_Con_Estado_Actividades.rpt", Nombre_Archivo + ".pdf");
                        break;
                    case "VARIACIONES EN LA HACIENDA PÚBLICA":
                        Generar_Reporte(ref Ds_Reporte_Excel, "Rpt_Con_Estado_Variaciones_Hacienda.rpt", Nombre_Archivo + ".pdf");
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Generar_Rpt_Excel(Ds_Reporte_Excel, Nombre, Tipo);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Diario_General " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Rpt_Excel
    /// DESCRIPCION :   Se encarga de generar el archivo de excel pasandole los paramentros
    ///               al documento
    /// PARAMETROS  :   Dt_Consulta_Reporte.- Es la consulta que contiene la informacion que se reportara
    /// CREO        :   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   25/Abril/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Generar_Rpt_Excel(DataSet Ds_Reporte, String Nombre, String Tipo)
    {
        String Nombre_Archivo = "";
        String Ruta_Archivo = "";
        Double Importe = 0.0;
        DateTime Dia_Ultimo = new DateTime();
        Dia_Ultimo = new DateTime(Convert.ToInt32(Cmb_Anio.SelectedItem.Text), (Convert.ToInt32(Cmb_Mes.SelectedValue.Substring(0, 2)) + 1), 1);
        Dia_Ultimo = Dia_Ultimo.AddDays(-1);
        String Indice = "";
        string Titulo_Reporte_Unificado = string.Empty; //Variable que contendra el titulo del reporte
        string aux = string.Empty; //variable auxiliar para la construccion del titulo
        string Campo_Oculto_CONAC = string.Empty; //variable para el campo ocukto que tendran los reportes de la CONAC

        try
        {
            Nombre_Archivo = Nombre + "_" + Cmb_Mes.SelectedItem.Text.Substring(0, 1) + "" + Cmb_Mes.SelectedItem.Text.Substring(1, 2).ToLower() + "_" + DateTime.Now.ToString("yy") + ".xls";
            Ruta_Archivo = @Server.MapPath("../../Reporte/" + Nombre_Archivo);
            //Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            Libro.Properties.Title = Tipo;
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI_Irapuato";
            //Creamos una hoja que tendrá el libro.
            Worksheet Hoja = Libro.Worksheets.Add("BDMC");
            //Agregamos un renglón a la hoja de excel.
            WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //Creamos el estilo cabecera para la hoja de excel. 
            WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("Encabezado");
            WorksheetStyle Estilo_Texto = Libro.Styles.Add("Texto");
            //Creamos el estilo Totales para la hoja de excel. 
            WorksheetStyle Estilo_Totales = Libro.Styles.Add("Totales");
            //Creamos el estilo Totales_Rojo para la hoja de excel. 
            WorksheetStyle Estilo_Totales_Color_Rojo = Libro.Styles.Add("Totales_Rojo");
            //Creamos el estilo Totales_Rojo para la hoja de excel. 
            WorksheetStyle Estilo_Totales_Negritas = Libro.Styles.Add("Totales_Negritas");
            //Creamos el estilo Totales_Negro para la hoja de excel. 
            WorksheetStyle Estilo_Totales_Negritas_Montos = Libro.Styles.Add("Totales_Negritas_Montos");
            //Estilo del campo oculto de la CONAC
            WorksheetStyle Estilo_Encabezado_Oculto = Libro.Styles.Add("Encabezado_Oculto");
            //Creamos una celda
            WorksheetCell Celda = new WorksheetCell();
            #region Estilos

            Estilo_Cabecera.Font.FontName = "Arial";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#000000";
            Estilo_Cabecera.Interior.Color = "#F79646";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            //Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Alignment.WrapText = true; //Indica que puede haber multilinea

            Estilo_Texto.Font.FontName = "Arial";
            Estilo_Texto.Font.Size = 10;
            Estilo_Texto.Font.Bold = false;
            Estilo_Texto.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Texto.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Texto.Alignment.Rotate = 0;
            Estilo_Texto.Font.Color = "#000000";
            Estilo_Texto.Interior.Color = "White";
            Estilo_Texto.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Texto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Texto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Texto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Texto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");


            Estilo_Totales.Font.FontName = "Arial";
            Estilo_Totales.Font.Size = 10;
            Estilo_Totales.Font.Bold = false;
            Estilo_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales.Alignment.Rotate = 0;
            Estilo_Totales.Font.Color = "#000000";
            Estilo_Totales.Interior.Color = "White";
            Estilo_Totales.NumberFormat = "_0* #,##0.00;\\-* #,##0.00_0;* \"0.00\";_-@_-";
            Estilo_Totales.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Totales_Color_Rojo.Font.FontName = "Arial";
            Estilo_Totales_Color_Rojo.Font.Size = 10;
            Estilo_Totales_Color_Rojo.Font.Bold = false;
            Estilo_Totales_Color_Rojo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales_Color_Rojo.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales_Color_Rojo.Alignment.Rotate = 0;
            Estilo_Totales_Color_Rojo.Font.Color = "RED";
            Estilo_Totales_Color_Rojo.Interior.Color = "White";
            Estilo_Totales_Color_Rojo.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Totales_Negritas.Font.FontName = "Arial";
            Estilo_Totales_Negritas.Font.Size = 10;
            Estilo_Totales_Negritas.Font.Bold = true;
            Estilo_Totales_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Totales_Negritas.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales_Negritas.Alignment.Rotate = 0;
            Estilo_Totales_Negritas.Font.Color = "#000000";
            Estilo_Totales_Negritas.Interior.Color = "White";
            Estilo_Totales_Negritas.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Totales_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Totales_Negritas_Montos.Font.FontName = "Arial";
            Estilo_Totales_Negritas_Montos.Font.Size = 10;
            Estilo_Totales_Negritas_Montos.Font.Bold = true;
            Estilo_Totales_Negritas_Montos.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales_Negritas_Montos.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales_Negritas_Montos.Alignment.Rotate = 0;
            Estilo_Totales_Negritas_Montos.Font.Color = "#000000";
            Estilo_Totales_Negritas_Montos.Interior.Color = "White";
            Estilo_Totales_Negritas_Montos.NumberFormat = "_0* #,##0.00;\\-* #,##0.00_0;* \"0.00\";_-@_-";
            Estilo_Totales_Negritas_Montos.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas_Montos.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Encabezado_Oculto.Font.FontName = "Arial";
            Estilo_Encabezado_Oculto.Font.Size = 8;
            Estilo_Encabezado_Oculto.Font.Bold = true;
            Estilo_Encabezado_Oculto.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Encabezado_Oculto.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Encabezado_Oculto.Alignment.Rotate = 0;
            Estilo_Encabezado_Oculto.Font.Color = "#F79646";
            Estilo_Encabezado_Oculto.Interior.Color = "#F79646";
            Estilo_Encabezado_Oculto.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Encabezado_Oculto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Encabezado_Oculto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Encabezado_Oculto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Encabezado_Oculto.Alignment.WrapText = true; //Indica que puede haber multilinea    
            #endregion
            if (Nombre == "01_ESF_15_AP" || Nombre == "02_EA_15_AP")
            {
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//  1 indice.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(360));//  4 Tipo.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  1 indice.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  4 Tipo.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//  1 indice.
            }
            else
            {
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//  1 indice.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(360));//  4 Tipo.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  1 indice.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  4 Tipo.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  1 indice.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  1 indice.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  4 Tipo.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//  1 indice.
            }
            //Colocar los datos de la cabecera
            //se llena el encabezado principal
            aux = "JUNTA DE AGUA POTABLE DRENAJE ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO";
            aux = aux.PadRight(1024 - aux.Length);
            Titulo_Reporte_Unificado = aux;

            //Verificar el encabezado
            if (Tipo == "VARIACIONES EN LA HACIENDA PÚBLICA")
            {
                aux = "ESTADO DE CAMBIOS EN EL PATRIMONIO/" + Tipo;
                Titulo_Reporte_Unificado += aux.PadRight(1024 - aux.Length);
            }
            else
            {
                aux = Tipo;
                Titulo_Reporte_Unificado += aux.PadRight(1024 - aux.Length);
            }
           
            Celda.MergeAcross = Ds_Reporte.Tables[1].Columns.Count - 1;
            Celda.StyleID = "Encabezado";
            if (Nombre == "01_ESF_15_AP")
            {
                Titulo_Reporte_Unificado += "Al " + Dia_Ultimo.Day + " DE " + Cmb_Mes.SelectedItem.Text + " DEL " + Cmb_Anio.SelectedValue;
            }
            else
            {
                Titulo_Reporte_Unificado += "Del 01 al " + Dia_Ultimo.Day + " DE " + Cmb_Mes.SelectedItem.Text + " DEL " + Cmb_Anio.SelectedValue;
            }

            //Colocar el titulo
            Celda = Renglon.Cells.Add(Titulo_Reporte_Unificado, DataType.String, "Encabezado");
            Celda.Row.Height = 50;
            Celda.MergeAcross = Ds_Reporte.Tables[1].Columns.Count - 2;

            //Colocar el campo oculto de la conac
            Campo_Oculto_CONAC = Consulta_Campo_Oculto_CONAC();
            Renglon.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Oculto"));

            Renglon = Hoja.Table.Rows.Add();
            foreach (DataColumn COLUMNA in Ds_Reporte.Tables[1].Columns)
            {
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));

                //Seleccionar el nombre de la columna
                switch (COLUMNA.ColumnName.ToUpper().Trim())
                {
                    case "PERIODO_ACTUAL":
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PERIODO ACTUAL", "Encabezado"));
                        break;

                    case "SALDO_FINAL_ANTERIOR":
                        //Verificar el tipo de reporte
                        if (Tipo == "VARIACIONES EN LA HACIENDA PÚBLICA")
                        {
                            aux = "SALDO INICIAL";
                            aux = aux.PadRight(1024 - aux.Length);
                            aux += "(A)";
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(aux, "Encabezado"));
                        }
                        else
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
                        }
                        break;

                    case "SALDO_FINAL":
                        //Verificar el tipo de reporte
                        if (Tipo == "VARIACIONES EN LA HACIENDA PÚBLICA")
                        {
                            aux = "SALDO FINAL";
                            aux = aux.PadRight(1024 - aux.Length);
                            aux += "(B)";
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(aux, "Encabezado"));
                        }
                        else
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
                        }
                        break;

                    case "INDICE":
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ÍNDICE", "Encabezado"));
                        break;

                    case "FLUJO":
                        //Verificar el tipo de reporte
                        if (Tipo == "VARIACIONES EN LA HACIENDA PÚBLICA")
                        {
                            aux = "FLUJO";
                            aux = aux.PadRight(1024 - aux.Length);
                            aux += "(B-A)";
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(aux, "Encabezado"));
                        }
                        else
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
                        }                        
                        break;

                    case "ABONO":
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ABONOS", "Encabezado"));
                        break;

                    default:
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName.Replace('_', ' '), "Encabezado"));
                        break;
                }
            }
            foreach (DataRow FILA in Ds_Reporte.Tables[1].Rows)
            {
                Renglon = Hoja.Table.Rows.Add();
                if (FILA[0].ToString() != "")
                {
                        Indice = FILA[0].ToString();
                }
                else
                {
                    Indice = "0000";
                }
                foreach (DataColumn COLUMNA in Ds_Reporte.Tables[1].Columns)
                {
                    switch (COLUMNA.ToString().ToUpper())
                    {
                        case "PERIODO_ACTUAL":
                            {
                                if (Indice.Substring(3, 1) == "0")
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales_Negritas_Montos"));
                                }
                                else
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales"));
                                }
                            }
                            break;
                        case "PERIODO_ANTERIOR":
                            {
                                if (Indice.Substring(3, 1) == "0")
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales_Negritas_Montos"));
                                }
                                else
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales"));
                                }
                            }
                            break;
                        case "CARGO":
                            {
                                if (Indice.Substring(3, 1) == "0")
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales_Negritas_Montos"));
                                }
                                else
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales"));
                                }
                            }
                            break;
                        case "CARGOS":
                            {
                                if (Indice.Substring(3, 1) == "0")
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales_Negritas_Montos"));
                                }
                                else
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales"));
                                }
                            }
                            break;
                        case "ABONO":
                            {
                                if (Indice.Substring(3, 1) == "0")
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales_Negritas_Montos"));
                                }
                                else
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales"));
                                }
                            }
                            break;
                        case "FLUJO":
                            {
                                if (Indice.Substring(3, 1) == "0")
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales_Negritas_Montos"));
                                }
                                else
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales"));
                                }
                            }
                            break;
                        case "SALDO_INICIAL":
                            {
                                if (Indice.Substring(3, 1) == "0")
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales_Negritas_Montos"));
                                }
                                else
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        String.Format("{0:n}", FILA[COLUMNA].ToString()),
                                        DataType.Number, "Totales"));
                                }
                            }
                            break;
                        case "SALDO_FINAL":
                            {
                                if (Indice.Substring(3, 1) == "0")
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        FILA[COLUMNA].ToString(), DataType.String, "Totales_Negritas_Montos"));
                                }
                                else
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA].ToString(), DataType.String, "Totales"));
                                }
                            }
                            break;

                        case "SALDO_FINAL_ANTERIOR":
                            {
                                if (Indice.Substring(3, 1) == "0")
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                        FILA[COLUMNA].ToString(), DataType.Number, "Totales_Negritas_Montos"));
                                }
                                else
                                {
                                    Importe = String.IsNullOrEmpty(FILA[COLUMNA].ToString()) ? 0.0 : Convert.ToDouble(FILA[COLUMNA].ToString());

                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA].ToString(), DataType.Number, "Totales"));
                                }
                            }
                            break;
                            
                        default: 
                            if (Indice.Substring(3, 1) == "0")
                            {
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA].ToString(), "Totales_Negritas"));
                            }
                            else
                            {
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA].ToString(), "Texto"));
                            }
                            break;
                    }
                }
            }
            Ruta_Archivo = HttpContext.Current.Server.MapPath("~") + "\\Exportaciones\\" + Nombre_Archivo;
            //Hoja.Protected = true;
            Libro.Save(Ruta_Archivo);
            Mostrar_Reporte(Nombre_Archivo, "Excel");
        }
        //catch (System.Threading.ThreadAbortException Ex)
        //{ }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Rpt_Excel
    /// DESCRIPCION :   Se encarga de generar el archivo de excel pasandole los paramentros
    ///               al documento
    /// PARAMETROS  :   Dt_Consulta_Reporte.- Es la consulta que contiene la informacion que se reportara
    /// CREO        :   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   25/Abril/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Generar_Rpt_Excel(DataSet Ds_Reporte)
    {
        String Nombre_Archivo = "";
        String Ruta_Archivo = "";
        try
        {
            Nombre_Archivo = "Estado_Cuentas" + Session.SessionID + Convert.ToString(DateTime.Now.ToString("yyyy'-'MMMM'-'dd'_'HH'-'mm'-'ss")) + ".xls";
            //Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            Libro.Properties.Title = "Cuenta Pública";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI_Irapuato";
            //Creamos una hoja que tendrá el libro.
            Worksheet Hoja = Libro.Worksheets.Add("BDMC");
            //Agregamos un renglón a la hoja de excel.
            WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //Creamos el estilo cabecera para la hoja de excel. 
            WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("Encabezado");
            //Creamos el estilo Totales para la hoja de excel. 
            WorksheetStyle Estilo_Totales = Libro.Styles.Add("Totales");
            //Creamos el estilo Totales_Rojo para la hoja de excel. 
            WorksheetStyle Estilo_Totales_Color_Rojo = Libro.Styles.Add("Totales_Rojo");
            //Creamos una celda
            WorksheetCell Celda = new WorksheetCell();
            #region Estilos

            Estilo_Cabecera.Font.FontName = "Arial";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#000000";
            Estilo_Cabecera.Interior.Color = "#FCD8BA";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Totales.Font.FontName = "Arial";
            Estilo_Totales.Font.Size = 10;
            Estilo_Totales.Font.Bold = false;
            Estilo_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales.Alignment.Rotate = 0;
            Estilo_Totales.Font.Color = "#000000";
            Estilo_Totales.Interior.Color = "White";
            Estilo_Totales.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Totales_Color_Rojo.Font.FontName = "Arial";
            Estilo_Totales_Color_Rojo.Font.Size = 10;
            Estilo_Totales_Color_Rojo.Font.Bold = false;
            Estilo_Totales_Color_Rojo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales_Color_Rojo.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales_Color_Rojo.Alignment.Rotate = 0;
            Estilo_Totales_Color_Rojo.Font.Color = "RED";
            Estilo_Totales_Color_Rojo.Interior.Color = "White";
            Estilo_Totales_Color_Rojo.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Color_Rojo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            #endregion

            foreach (DataTable Dt_Consulta_Reporte in Ds_Reporte.Tables)
            {
                Celda = Renglon.Cells.Add(Dt_Consulta_Reporte.TableName);

                Celda.MergeAcross = Dt_Consulta_Reporte.Columns.Count - 1;
                Celda.StyleID = "Encabezado";
                Renglon = Hoja.Table.Rows.Add();

                foreach (DataColumn COLUMNA in Dt_Consulta_Reporte.Columns)
                {
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName, "Encabezado"));
                }

                foreach (DataRow FILA in Dt_Consulta_Reporte.Rows)
                {
                    Renglon = Hoja.Table.Rows.Add();

                    foreach (DataColumn COLUMNA in Dt_Consulta_Reporte.Columns)
                    {
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA].ToString(), "Totales"));
                    }
                }
                Renglon = Hoja.Table.Rows.Add();
            }
            //Asignar la ruta del archivo
            Ruta_Archivo = HttpContext.Current.Server.MapPath("~") + "\\Exportaciones\\" + Nombre_Archivo;
            Libro.Save(Ruta_Archivo);
            Mostrar_Reporte(Nombre_Archivo, "Excel");
        }
        catch (System.Threading.ThreadAbortException Ex)
        { }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Exportaciones/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consulta_Campo_Oculto_CONAC
    ///DESCRIPCIÓN: Consultar el parametro oculto de los reportes de la CONAC
    ///PARAMETROS: 
    ///CREO:        Noe Mosqueda Valadez
    ///FECHA_CREO:  23/Mayo/2013 10:55
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private string Consulta_Campo_Oculto_CONAC()
    {
        //Declaracion de variables
        Cls_Cat_Con_Parametros_Negocio Parametros_Negocio = new Cls_Cat_Con_Parametros_Negocio(); //variable para la capa de negocios
        DataTable Dt_Parametros = new DataTable(); //tabla para la consulta de los parametros
        string Resultado = string.Empty; //variable para el resultado

        try
        {
            //Consultar los parametros de contabilidad
            Dt_Parametros = Parametros_Negocio.Consulta_Datos_Parametros_2();

            //verificar si la consulta arrojo resultado
            if (Dt_Parametros.Rows.Count > 0)
            {
                //verificar si hay dato
                if (Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Campo_Escondido_CONAC] != DBNull.Value)
                {
                    if (string.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Campo_Escondido_CONAC].ToString().Trim()) == false)
                    {
                        Resultado = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Campo_Escondido_CONAC].ToString().Trim();
                    }
                }
            }

            //entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    #endregion
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Todos_Estado
    /// DESCRIPCION : Consulta todos los movimientos con sus detalles de las pólizas
    ///               realizadas en el rango de fechas proporcionadas por el usuario
    /// PARAMETROS  : String Formato.- Para saber que formato sera el archivo pdf, excel
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 21/Febrero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Todos_Estado()
    {
        Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Estado_Cuentas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta_Situacion_Financiera = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Tipo_Reporte = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Principal = new DataTable();
        DataTable Dt_Periodo_Anterior = new DataTable();
        DataSet Ds_Situacion_Financiera = new DataSet();
        DataSet Ds_Actividades = new DataSet();
        DataSet Ds_Variaciones = new DataSet();
        DataSet Ds_Analitico = new DataSet();
        DataSet Ds_Principal = new DataSet();
        DataSet Ds_Flujo = new DataSet();
        DataSet Ds_Analitico_Deuda = new DataSet();
        DataSet Ds_Esf_01 = new DataSet();
        DataSet Ds_Esf_02 = new DataSet();
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            #region Dt_Principal
            Dt_Principal.Columns.Add("Notas", typeof(System.String));
            Dt_Principal.Columns.Add("Descripción", typeof(System.String));
            Dt_Principal.TableName = "Dt_Principal";
            DataRow Row = Dt_Principal.NewRow();
            Row["Notas"] = "";
            Row["Descripción"] = "I. DE DESGLOSE:";
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row = Dt_Principal.NewRow();
            Row["Notas"] = "";
            Row["Descripción"] = "INFORMACION CONTABLE";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-01";
            Row["Descripción"] = "FONDOS CON AFECTACIÓN ESPECÍFICA E INVERSIONES FINANCIERAS";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-02";
            Row["Descripción"] = "CONTRIBUCIONES POR RECUPERAR";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-03";
            Row["Descripción"] = "CONTRIBUCIONES POR RECUPERAR CORTO PLAZO";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-05";
            Row["Descripción"] = "INVENTARIO Y ALMACENES";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-06";
            Row["Descripción"] = "FIDEICOMISOS";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-07";
            Row["Descripción"] = "PARTICIPACIONES Y APORTACIONES DE CAPITAL";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-08";
            Row["Descripción"] = "BIENES MUEBLES E INMUEBLES";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-09";
            Row["Descripción"] = "INTANGIBLES Y DIFERIDOS";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-10";
            Row["Descripción"] = "ESTIMACIONES Y DETERIOROS";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-11";
            Row["Descripción"] = "OTROS ACTIVOS NO CIRCULANTES";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-13";
            Row["Descripción"] = "DIFERIDOS Y OTROS PASIVOS";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-14";
            Row["Descripción"] = "OTROS PASIVOS CIRCULANTES";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ESF-15";
            Row["Descripción"] = "DEUDA PÚBLICA A LARGO PLAZO";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ERA-01";
            Row["Descripción"] = "INGRESOS";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ERA-02";
            Row["Descripción"] = "OTROS INGRESOS";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "ERA-03";
            Row["Descripción"] = "GASTOS";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "VHP-01";
            Row["Descripción"] = "PATRIMONIO CONTRIBUIDO";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "VHP-02";
            Row["Descripción"] = "PATRIMONIO GENERADO";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "EFE-01";
            Row["Descripción"] = "FLUJO DE EFECTIVO";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "EFE-02";
            Row["Descripción"] = "ADQ. BIENES MUEBLES E INMUEBLES";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "";
            Row["Descripción"] = "II. DE MEMORIA (DE ORDEN): ";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "";
            Row["Descripción"] = "CONTABLES";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Row["Notas"] = "";
            Row["Descripción"] = "PRESUPUESTALES";
            Row = Dt_Principal.NewRow();
            Dt_Principal.Rows.Add(Row);
            Dt_Principal.AcceptChanges();
            Ds_Principal.Tables.Add(Dt_Principal.Copy());
            #endregion
            //  para saber el nombre que llevara el reporte inicio y se realiza la consulta
            DataRow Dt_Row = Dt_Tipo_Reporte.NewRow(); 
            #region ESF-01

            DataTable Dt_ESF_01 = new DataTable();
            DataTable Dt_ESF_011 = new DataTable();
            DataTable Dt_ESF_012 = new DataTable();
            DataTable Dt_ESF_013 = new DataTable();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1114";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_01 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_01 = Generar_Tabla_Esf(Dt_ESF_01, null, 1, null, "");

            //  Para la cuenta 1115
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1115";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_011 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_011 = Generar_Tabla_Esf(Dt_ESF_011, null, 1, null, "");

            //  Para la cuenta 1121
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1121";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_012 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_012 = Generar_Tabla_Esf(Dt_ESF_012, null, 1, null, "");

            //  Para la cuenta 1211
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1211";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_013 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_013 = Generar_Tabla_Esf(Dt_ESF_013, null, 1, null, "");

            Ds_Esf_01.Tables.Add(Dt_ESF_01.Copy());
            Ds_Esf_01.Tables.Add(Dt_ESF_011.Copy());
            Ds_Esf_01.Tables.Add(Dt_ESF_012.Copy());
            Ds_Esf_01.Tables.Add(Dt_ESF_013.Copy());
            //Ds_Reporte.Tables.Add(Dt_Consulta.Copy());
            #endregion
            #region ESF-02
            DataTable Dt_ESF_02 = new DataTable();
            DataTable Dt_ESF_021 = new DataTable();
            DataTable Dt_Auxiliar_2 = new DataTable();
            DataTable Dt_Auxiliar_1 = new DataTable();

            //  para la cuenta 1122
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue.ToString();
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1122";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_02 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12"+Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString())-2).Substring(2,2);//  Dos Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1122";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_Auxiliar_2 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);//  Un Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1122";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_Auxiliar_1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_02 = Generar_Tabla_Esf(Dt_ESF_02, Dt_Auxiliar_2, 2, Dt_Auxiliar_1, "");

            //  para la cuenta 1124
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1124";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_021 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_Auxiliar_2 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 2).Substring(2, 2);//  Dos Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1124";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_Auxiliar_2 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_Auxiliar_1 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2,2);//  Un Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1124";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_Auxiliar_1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_ESF_021 = Generar_Tabla_Esf(Dt_ESF_021, Dt_Auxiliar_2, 2, Dt_Auxiliar_1, "");

            Ds_Esf_02.Tables.Add(Dt_ESF_02.Copy());
            Ds_Esf_02.Tables.Add(Dt_ESF_021.Copy());
            #endregion
            #region ESF-03

            //  para el sub reporte de vhp_01 del saldo 
            DataTable Dt_ESF_03 = new DataTable();
            DataTable Dt_ESF_031 = new DataTable();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1123";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_03 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_03 = Generar_Tabla_Esf(Dt_ESF_03, null, 3, null, "");

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1125";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_031 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_031 = Generar_Tabla_Esf(Dt_ESF_031, null, 3, null, "");
            #endregion
            #region ESF-05

            //  para el sub reporte de vhp_01 del saldo 
            DataTable Dt_ESF_05 = new DataTable();
            DataTable Dt_ESF_051 = new DataTable();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "114";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_05 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_05 = Generar_Tabla_Esf(Dt_ESF_05, null, 5, null, "114");

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "115";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_051 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_051 = Generar_Tabla_Esf(Dt_ESF_051, null, 5, null, "115");
            #endregion
            #region ESF-06

            DataTable Dt_ESF_06 = new DataTable();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1213";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_06 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_06 = Generar_Tabla_Esf(Dt_ESF_06, null, 1, null, "");

            #endregion
            #region ESF-07

            DataTable Dt_ESF_07 = new DataTable();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1214";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_07 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_07 = Generar_Tabla_Esf(Dt_ESF_07, null, 1, null, "");

            #endregion
            #region ESF-08

            DataTable Dt_ESF_08 = new DataTable();
            DataTable Dt_ESF_081 = new DataTable();
            DataTable Dt_ESF_08_126 = new DataTable();
            DataTable Dt_ESF_Aux1 = new DataTable();
            DataTable Dt_ESF_Aux2 = new DataTable();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12"+Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString())-1).Substring(2,2);//  Dos Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "123";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "123";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux2 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_08 = Generar_Tabla_Esf(Dt_ESF_Aux2, null, 8, Dt_ESF_Aux1, "123");

            Dt_ESF_Aux1 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);//  Dos Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "124";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_ESF_Aux2 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "124";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux2 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_081 = Generar_Tabla_Esf(Dt_ESF_Aux2, null, 8, Dt_ESF_Aux1, "124");

            //agregar el 126
            Dt_ESF_Aux1 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "126";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux2 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_ESF_Aux1 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);//  Dos Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "126";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_08_126 = Generar_Tabla_Esf(Dt_ESF_Aux2, null, 8, Dt_ESF_Aux1, "126");
            #endregion
            #region ESF-09

            DataTable Dt_ESF_09 = new DataTable();
            DataTable Dt_ESF_1265 = new DataTable();
            DataTable Dt_ESF_091 = new DataTable();
            Dt_ESF_Aux1 = new DataTable();
            Dt_ESF_Aux2 = new DataTable();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);//  Dos Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "125";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "125";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux2 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_09 = Generar_Tabla_Esf(Dt_ESF_Aux2, null, 8, Dt_ESF_Aux1, "125");

            //Agregar para 1265
            Dt_ESF_Aux1 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "01" + Cmb_Anio.SelectedItem.Text.ToString().Substring(2, 2);
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1265";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_ESF_Aux2 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "1265";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux2 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_1265 = Generar_Tabla_Esf(Dt_ESF_Aux2, null, 8, Dt_ESF_Aux1, "");

            //127
            Dt_ESF_Aux1 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "01" + Cmb_Anio.SelectedItem.Text.ToString().Substring(2, 2);
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "127";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_ESF_Aux2 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "127";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux2 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_091 = Generar_Tabla_Esf(Dt_ESF_Aux2, null, 8, Dt_ESF_Aux1, "");

            #endregion
            #region ESF-10

            DataTable Dt_ESF_10 = new DataTable();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "128";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_10 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_10 = Generar_Tabla_Esf(Dt_ESF_10, null, 11, null, "");

            #endregion
            #region ESF-11

            DataTable Dt_ESF_11 = new DataTable();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "129";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_11 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_11 = Generar_Tabla_Esf(Dt_ESF_11, null, 11, null, "");

            #endregion
            #region ESF-12
            //Declaracion de variables de la region
            DataTable Dt_ESF_12 = new DataTable();
            Dt_ESF_Aux1 = new DataTable();
            Dt_ESF_Aux2 = new DataTable();
            DataRow Renglon; //Renglon para llenar la tabla
            decimal Total = 0; //Total de la 211
            DataView Dv_Filtro; //Vista para los filtros
            DataTable Dt_ESF_12_Previa = new DataTable(); //tabla previa para la consulta
            int Cont_Elementos = 0; //variable apra el contador
            DataTable Dt_aux = new DataTable(); //tabla auxiliar

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "211";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = "212";
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_12_Previa = Generar_Tabla_Esf(Dt_ESF_Aux1, null, 3, null, "");
            
            //Cuentas 2110 y 2120

            //verificar si hay elementos de la cuenta 2110
            Dv_Filtro = new DataView(Dt_ESF_12_Previa);
            Dv_Filtro.RowFilter = "CUENTA LIKE '211%'";
            if (Dv_Filtro.ToTable().Rows.Count > 0)
            {               
                //instanciar renglon
                Renglon = Dt_ESF_12_Previa.NewRow();
                Renglon["CUENTA"] = "211000000";
                Renglon["NOMBRE"] = "CUENTAS POR PAGAR A CORTO PLAZO";

                //Ciclo para el total
                Dt_aux = Dv_Filtro.ToTable();
                for (Cont_Elementos = 0; Cont_Elementos < Dt_aux.Rows.Count; Cont_Elementos++)
                {
                    Total += Convert.ToDecimal(Dt_aux.Rows[Cont_Elementos]["IMPORTE"]);
                }

                Renglon["IMPORTE"] = Convert.ToDouble(Total);
                Renglon["IMPORTE_90"] = 0;
                Renglon["IMPORTE_180"] = 0;
                Renglon["IMPORTE_365"] = 0;
                Renglon["CARACTERISTICAS"] = "";
                Renglon["ESTATUS DEL ADEUDO"] = "";

                //Agregar renglon
                Dt_ESF_12_Previa.Rows.Add(Renglon);
                Dt_ESF_12_Previa.AcceptChanges();
            }

            //verificar si hay elementos de la cuenta 2110
            Dv_Filtro = new DataView(Dt_ESF_12_Previa);
            Dv_Filtro.RowFilter = "CUENTA LIKE '212%'";
            if (Dv_Filtro.ToTable().Rows.Count > 0)
            {
                //instanciar renglon
                Renglon = Dt_ESF_12_Previa.NewRow();
                Renglon["CUENTA"] = "212000000";
                Renglon["NOMBRE"] = "DOCUMENTOS POR PAGAR A CORTO PLAZO";

                //Ciclo para el total
                Dt_aux = new DataTable();
                Dt_aux = Dv_Filtro.ToTable();
                Total = 0;
                for (Cont_Elementos = 0; Cont_Elementos < Dt_aux.Rows.Count; Cont_Elementos++)
                {
                    Total += Convert.ToDecimal(Dt_aux.Rows[Cont_Elementos]["IMPORTE"]);
                }

                Renglon["IMPORTE"] = Convert.ToDouble(Total);
                Renglon["IMPORTE_90"] = 0;
                Renglon["IMPORTE_180"] = 0;
                Renglon["IMPORTE_365"] = 0;
                Renglon["CARACTERISTICAS"] = "";
                Renglon["ESTADO DEL ACTIVO"] = "";

                //Agregar renglon
                Dt_ESF_12_Previa.Rows.Add(Renglon);
                Dt_ESF_12_Previa.AcceptChanges();
            }

            //ordenar la tabla
            Dv_Filtro = new DataView(Dt_ESF_12_Previa);
            Dv_Filtro.Sort = "CUENTA ASC";

            //Copiar la tabla
            Dt_ESF_12 = Dv_Filtro.ToTable().Copy();

            #endregion
            #region ESF-13

            DataTable Dt_ESF_13 = new DataTable();
            DataTable Dt_ESF_13_1 = new DataTable();
            DataTable Dt_ESF_13_2 = new DataTable();
            Dt_ESF_Aux1 = new DataTable();
            Dt_ESF_Aux2 = new DataTable();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "2159";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_13 = Generar_Tabla_Esf(Dt_ESF_Aux1, null, 11, null, "");

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "216";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux2 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_13_1 = Generar_Tabla_Esf(Dt_ESF_Aux2, null, 11, null, "");

            Dt_ESF_Aux1 = new DataTable();
            //Agregar un a nueva consulta
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "224";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_ESF_Aux1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_ESF_13_2 = Generar_Tabla_Esf(Dt_ESF_Aux1, null, 11, null, "");

            #endregion
            #region ESF-14

            DataTable Dt_ESF_14 = new DataTable();

            //Dt_ESF_Aux1 = new DataTable();
            //Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            //Rs_Consulta_Situacion_Financiera.P_Filtro1 = "2199";
            //Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            //Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            //Dt_ESF_Aux1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            //Dt_ESF_14 = Generar_Tabla_Esf(Dt_ESF_Aux1, null, 11, null, "");

            #endregion
            //Agregar 213 y 223
            #region ESF-15

            DataTable Dt_ESF_15 = new DataTable();

            //Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            //Rs_Consulta_Situacion_Financiera.P_Filtro1 = "2199";
            //Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            //Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            //Dt_ESF_14 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            //Dt_ESF_14 = Generar_Tabla_Esf(Dt_ESF_14, null, 8,null);

            #endregion
            // inicio nota ERA-01
            #region ERA-01
            //  para el sub reporte de vhp_01 del saldo 
            DataTable Dt_Era_01 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "41";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = "42";
            Dt_Era_01 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_Era_01 = Generar_Tabla_Era(Dt_Era_01, 1);
            #endregion
            // fin nota ERA-01
            // inicio nota ERA-02
            #region ERA-02

            //  para el sub reporte de vhp_01 del saldo 
            DataTable Dt_Era_02 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "43";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Dt_Era_02 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_Era_02 = Generar_Tabla_Era(Dt_Era_02, 2);
            #endregion
            // fin nota ERA-02
            // inicio nota ERA-03
            #region ERA-03

            //  para el sub reporte de vhp_01 del saldo 
            DataTable Dt_Era_03 = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "5";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Dt_Era_03 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_Era_03 = Generar_Tabla_Era(Dt_Era_03, 3);
            #endregion
            // fin nota ERA-03
            #region VHP-01
            //  para el sub reporte de vhp_01 del saldo anterior
            DataTable Dt_Vhp_01_Saldo_Anterior = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);//  Dos Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "31";
            Dt_Vhp_01_Saldo_Anterior = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            //  para el sub reporte de vhp_01
            DataTable Dt_Vhp_01_Saldo_Actual = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "31";
            Dt_Vhp_01_Saldo_Actual = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_Vhp_01_Saldo_Actual = Generar_Tabla_Vhp_01(Dt_Vhp_01_Saldo_Actual, Dt_Vhp_01_Saldo_Anterior, 1);

            #endregion
            #region VHP-02
            //  para el sub reporte de vhp_02 del saldo anterior
            DataTable Dt_Vhp_02_Saldo_Anterior = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "13" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);//  Dos Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "32";
            Dt_Vhp_02_Saldo_Anterior = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            //  para el sub reporte de vhp_02
            DataTable Dt_Vhp_02_Saldo_Actual = new DataTable();
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "32";
            Dt_Vhp_02_Saldo_Actual = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_Vhp_02_Saldo_Actual = Generar_Tabla_Vhp_01(Dt_Vhp_02_Saldo_Actual, Dt_Vhp_02_Saldo_Anterior, 2);
            #endregion
            #region EFE-01
            DataTable Dt_EFE_01 = new DataTable();
            Dt_Auxiliar_2 = new DataTable();
            Dt_Auxiliar_1 = new DataTable();

            //  para la cuenta 1122
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "111";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_Auxiliar_1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);//  Dos Años antes del actual
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "111";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = null;
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
            Dt_Auxiliar_2 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();

            Dt_EFE_01 = Generar_Tabla_EFE(Dt_Auxiliar_1, 1, Dt_Auxiliar_2);
            #endregion
            #region EFE-02
            DataTable Dt_EFE_02 = new DataTable();
            Dt_Auxiliar_1 = new DataTable();

            //  para la cuenta 1122
            Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta_Situacion_Financiera.P_Filtro1 = "121";
            Rs_Consulta_Situacion_Financiera.P_Filtro2 = "123";
            Rs_Consulta_Situacion_Financiera.P_Filtro3 = "124";
            Rs_Consulta_Situacion_Financiera.P_Filtro4 = "125";
            Dt_Auxiliar_1 = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
            Dt_EFE_02 = Generar_Tabla_EFE(Dt_Auxiliar_1, 2, null);
            Dt_EFE_02 = new DataTable();
            #endregion
            //  carga la informacion al dataset
            Dt_Tipo_Reporte.Rows.Add(Dt_Row);
            Dt_Consulta.TableName = "Dt_Sin_Fin";
            Dt_ESF_01.TableName = "Dt_Esf_01";
            Dt_ESF_011.TableName = "Dt_Esf_011";
            Dt_ESF_012.TableName = "Dt_Esf_012";
            Dt_ESF_013.TableName = "Dt_Esf_013";
            Dt_ESF_02.TableName = "Dt_Esf_02";
            Dt_ESF_021.TableName = "Dt_Esf_021";
            Dt_ESF_03.TableName = "Dt_Esf_03";
            Dt_ESF_031.TableName = "Dt_Esf_031";
            Dt_ESF_05.TableName = "Dt_Esf_05";
            Dt_ESF_051.TableName = "Dt_Esf_051";
            Dt_ESF_06.TableName = "Dt_Esf_06";
            Dt_ESF_07.TableName = "Dt_Esf_07";
            Dt_ESF_08.TableName = "Dt_Esf_08";
            Dt_ESF_081.TableName = "Dt_Esf_081";
            Dt_ESF_08_126.TableName = "Dt_ESF_08_126";
            Dt_ESF_09.TableName = "Dt_Esf_09";
            Dt_ESF_091.TableName = "Dt_Esf_091";
            Dt_ESF_1265.TableName = "Dt_ESF_1265";
            Dt_ESF_10.TableName = "Dt_ESF_10";
            Dt_ESF_11.TableName = "Dt_Esf_11";
            Dt_ESF_12.TableName = "Dt_Esf_12";
            Dt_ESF_13.TableName = "Dt_Esf_13";
            Dt_ESF_13_1.TableName = "Dt_ESF_13_1";
            Dt_ESF_13_2.TableName = "Dt_ESF_13_2";
            Dt_ESF_14.TableName = "Dt_Esf_14";
            Dt_ESF_15.TableName = "Dt_Esf_15";
            Dt_Vhp_01_Saldo_Actual.TableName = "Dt_Vhp_01";
            Dt_Vhp_02_Saldo_Actual.TableName = "Dt_Vhp_02";

            Ds_Situacion_Financiera.Clear();
            Ds_Situacion_Financiera.Tables.Clear();
            Ds_Situacion_Financiera.Tables.Add(Dt_Consulta.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_Tipo_Reporte.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_01.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_011.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_012.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_013.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_02.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_021.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_03.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_031.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_05.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_051.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_06.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_07.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_08.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_081.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_08_126.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_09.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_091.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_1265.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_10.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_11.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_12.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_13.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_13_1.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_13_2.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_14.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_ESF_15.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_Vhp_01_Saldo_Actual.Copy());
            Ds_Situacion_Financiera.Tables.Add(Dt_Vhp_02_Saldo_Actual.Copy());

            Ds_Actividades.Clear();
            Ds_Actividades.Tables.Clear();
            Ds_Actividades.Tables.Add(Dt_Era_01.Copy());
            Ds_Actividades.Tables.Add(Dt_Era_02.Copy());
            Ds_Actividades.Tables.Add(Dt_Era_03.Copy());
            Ds_Actividades.Tables.Add(Dt_EFE_01.Copy());
            Ds_Actividades.Tables.Add(Dt_EFE_02.Copy());

            if (Cmb_Tipo_Reporte.SelectedValue == "NOTAS DE LOS ESTADOS FINANCIEROS")
                Generar_Rpt_Excel_Notas(Ds_Situacion_Financiera, Ds_Actividades, Ds_Variaciones, Ds_Flujo, Ds_Principal);

            else if (Cmb_Tipo_Reporte.SelectedValue == "INFORMACION FINANCIERA - CONTABLE")
                Generar_Rpt_Excel_Contabilidad(Ds_Situacion_Financiera, Ds_Actividades, Ds_Variaciones, Ds_Analitico, Ds_Flujo, Ds_Analitico_Deuda);
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Diario_General " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Anio_OnSelectedIndexChanged
    ///DESCRIPCIÓN: cargara el combo de Anios con los que se encuentren cerrados
    ///PARAMETROS: 
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  22/Abril/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Llenar_Combo_Anio()
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta_Anios = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Dt_Consulta = Rs_Consulta_Anios.Consulta_Anios();
            if (Dt_Consulta.Rows.Count > 0)
            {
                Cmb_Anio.Items.Clear();
                Cmb_Anio.DataSource = Dt_Consulta;
                Cmb_Anio.DataValueField = "ANIO";
                Cmb_Anio.DataTextField = "ANIO";
                Cmb_Anio.DataBind();
                Cmb_Anio.Items.Insert(0, "----- < SELECCIONE > -----");
                Cmb_Anio.SelectedIndex = 0;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    
    #region(Operaciones)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Rpt_Excel_Contabilidad
    /// DESCRIPCION :   Se encarga de generar el archivo de excel pasandole los paramentros
    ///                 al documento
    /// PARAMETROS  :   Dt_situacion.- Es la consulta que contiene las notas esf
    ///                 Ds_Actividades.- Es la consulta que contiene las notas era
    ///                 Ds_Variaciones.- Es la consulta que contiene las notas vhp
    ///                 Ds_Flujo.- Es la consulta que contiene las notas efe
    /// CREO        :   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   07/Marzo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Generar_Rpt_Excel_Contabilidad(DataSet Ds_Situacion, DataSet Ds_Actividades, DataSet Ds_Variaciones, DataSet Ds_Analitico, DataSet Ds_Flujo, DataSet Ds_Deuda)
    {
        WorksheetCell Celda = new WorksheetCell();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Documentos = new DataTable();
        String Nombre_Archivo = "";
        String Ruta_Archivo = "";
        String Indice = ""; //  contendra la cuenta
        String Año = "";
        String Mes = "";
        String Mes_Archivo = "";
        String Mes_Numerico = "";
        String Formato_Importe = "";
        Double Total_Cuenta_1000 = 0.0;
        Double Total_Cuenta_2000 = 0.0;
        Double Total_Cuenta_3000 = 0.0;
        DataTable Dt_Principal = new DataTable();
        DataTable Dt_Actividades = new DataTable();
        DataTable Dt_Variaciones = new DataTable();
        DataTable Dt_Flujo = new DataTable();
        DataTable Dt_Analitico = new DataTable();
        DataTable Dt_Analitico_Deuda = new DataTable();
        try
        {
            #region Cargar Datatable
            Dt_Principal = Ds_Situacion.Tables[0].Copy();
            Dt_Actividades = Ds_Actividades.Tables[0].Copy();
            Dt_Variaciones = Ds_Variaciones.Tables[0].Copy();
            Dt_Flujo = Ds_Flujo.Tables[0].Copy();
            Dt_Analitico = Ds_Analitico.Tables[0].Copy();
            Dt_Analitico_Deuda = Ds_Deuda.Tables[0].Copy();
            #endregion
            //  Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            //  propiedades del libro
            Libro.Properties.Title = "Cuenta Pública";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI_Irapuato";
            //  Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("Encabezado");
            //  Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera_Izquierda = Libro.Styles.Add("Encabezado_Izquierda");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Indice = Libro.Styles.Add("Contenido_Indice");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Nombre = Libro.Styles.Add("Contenido_Nombre");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Nota = Libro.Styles.Add("Contenido_Nota");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Nombre_Negritas = Libro.Styles.Add("Contenido_Nombre_Negritas");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Firma = Libro.Styles.Add("Contenido_Firma");
            //  Creamos el estilo contenido del presupuesto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Totales = Libro.Styles.Add("Totales");
            //  Creamos el estilo contenido del presupuesto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Totales_Negritas = Libro.Styles.Add("Totales_Negritas");

            #region Estilos
            //***************************************inicio de los estilos***********************************************************
            //  estilo para la cabecera    Encabezado
            Estilo_Cabecera.Font.FontName = "Arial";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#000000";
            Estilo_Cabecera.Interior.Color = "white";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.None;
            //  estilo para la cabecera    Encabezado_Izquierda
            Estilo_Cabecera_Izquierda.Font.FontName = "Arial";
            Estilo_Cabecera_Izquierda.Font.Size = 10;
            Estilo_Cabecera_Izquierda.Font.Bold = true;
            Estilo_Cabecera_Izquierda.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Cabecera_Izquierda.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera_Izquierda.Alignment.Rotate = 0;
            Estilo_Cabecera_Izquierda.Font.Color = "#000000";
            Estilo_Cabecera_Izquierda.Interior.Color = "white";
            Estilo_Cabecera_Izquierda.Interior.Pattern = StyleInteriorPattern.None;

            //estilo para el contenido   contenido_indice
            Estilo_Contenido_Indice.Font.FontName = "Arial";
            Estilo_Contenido_Indice.Font.Size = 10;
            Estilo_Contenido_Indice.Font.Bold = false;
            Estilo_Contenido_Indice.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido_Indice.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Contenido_Indice.Alignment.Rotate = 0;
            Estilo_Contenido_Indice.Font.Color = "#000000";
            Estilo_Contenido_Indice.Interior.Color = "White";
            Estilo_Contenido_Indice.Interior.Pattern = StyleInteriorPattern.None;

            //estilo para el    Contenido_Nombre
            Estilo_Contenido_Nombre.Font.FontName = "Arial";
            Estilo_Contenido_Nombre.Font.Size = 10;
            Estilo_Contenido_Nombre.Font.Bold = false;
            Estilo_Contenido_Nombre.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Nombre.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre.Font.Color = "#000000";
            Estilo_Contenido_Nombre.Interior.Color = "White";
            Estilo_Contenido_Nombre.Interior.Pattern = StyleInteriorPattern.None;

            //estilo para el    Contenido_Firma
            Estilo_Contenido_Firma.Font.FontName = "Arial";
            Estilo_Contenido_Firma.Font.Size = 10;
            Estilo_Contenido_Firma.Font.Bold = false;
            Estilo_Contenido_Firma.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Firma.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Firma.Alignment.Rotate = 0;
            Estilo_Contenido_Firma.Font.Color = "#000000";
            Estilo_Contenido_Firma.Interior.Color = "White";
            Estilo_Contenido_Firma.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Firma.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            //estilo para el    Contenido_Nombre Negritas
            Estilo_Contenido_Nombre_Negritas.Font.FontName = "Arial";
            Estilo_Contenido_Nombre_Negritas.Font.Size = 10;
            Estilo_Contenido_Nombre_Negritas.Font.Bold = true;
            Estilo_Contenido_Nombre_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Nombre_Negritas.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre_Negritas.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre_Negritas.Font.Color = "#000000";
            Estilo_Contenido_Nombre_Negritas.Interior.Color = "White";
            Estilo_Contenido_Nombre_Negritas.Interior.Pattern = StyleInteriorPattern.None;
            //estilo para el    Contenido_Nota
            Estilo_Contenido_Nota.Font.FontName = "Arial";
            Estilo_Contenido_Nota.Font.Size = 10;
            Estilo_Contenido_Nota.Font.Bold = false;
            Estilo_Contenido_Nota.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Nota.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Contenido_Nota.Alignment.Rotate = 0;
            Estilo_Contenido_Nota.Font.Color = "#000000";
            Estilo_Contenido_Nota.Interior.Color = "White";
            Estilo_Contenido_Nota.Interior.Pattern = StyleInteriorPattern.None;
            //  estilo para el presupuesto (importe)
            Estilo_Totales.Font.FontName = "Arial";
            Estilo_Totales.Font.Size = 10;
            Estilo_Totales.Font.Bold = false;
            Estilo_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales.Alignment.Rotate = 0;
            Estilo_Totales.Font.Color = "#000000";
            Estilo_Totales.Interior.Color = "White";
            Estilo_Totales.Interior.Pattern = StyleInteriorPattern.None;
            //  estilo para el totales negritas
            Estilo_Totales_Negritas.Font.FontName = "Arial";
            Estilo_Totales_Negritas.Font.Size = 10;
            Estilo_Totales_Negritas.Font.Bold = true;
            Estilo_Totales_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales_Negritas.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales_Negritas.Alignment.Rotate = 0;
            Estilo_Totales_Negritas.Font.Color = "#000000";
            Estilo_Totales_Negritas.Interior.Color = "White";
            Estilo_Totales_Negritas.Interior.Pattern = StyleInteriorPattern.None;
            //*************************************** fin de los estilos***********************************************************
            #endregion

            Int32 Fecha;
            Mes = Cmb_Mes.SelectedValue;
            Mes = Mes.Substring(0, 2);
            Fecha = DateTime.DaysInMonth(Convert.ToInt32(Cmb_Anio.SelectedValue), Convert.ToInt32(Mes));
            Mes_Numerico = Mes;
            Mes = Asignar_Mes(Mes);
            Mes_Archivo = Mes;
            Mes_Archivo = Mes_Archivo.Substring(0, 3);
            Año = Cmb_Anio.SelectedValue;
            Año = Año.Substring(2, 2);

            //02_NOT_DYM_CódigoSF_CódigoPeriodo_Año
            Nombre_Archivo = "01_CON_15_" + Mes_Archivo + "_" + Año + ".xls";
            //Nombre_Archivo = "01_CON_CódigoSF_Código" + Mes + "_" + Cmb_Anio.SelectedValue + ".xls";
            Ruta_Archivo = @Server.MapPath("../../Reporte/" + Nombre_Archivo);
            #region Hojas
            //  Creamos una hoja que tendrá la situacion financiera.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("ESF");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();

            //  para la hoja de la estado de actividades
            CarlosAg.ExcelXmlWriter.Worksheet Hoja_Ea = Libro.Worksheets.Add("EA");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_Ea = Hoja_Ea.Table.Rows.Add();

            //  para la hoja de variacion ante hacienda publica
            CarlosAg.ExcelXmlWriter.Worksheet Hoja_Evhp = Libro.Worksheets.Add("EVHP");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();

            //  para la hoja de  flujo de efectivo
            CarlosAg.ExcelXmlWriter.Worksheet Hoja_Efe = Libro.Worksheets.Add("EFE");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_Efe = Hoja_Efe.Table.Rows.Add();

            //  para la hoja de analitico activo
            CarlosAg.ExcelXmlWriter.Worksheet Hoja_Eaa = Libro.Worksheets.Add("EAA");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();

            //  para la hoja de Estado Analítico de la Deuda y Otros Pasivos
            CarlosAg.ExcelXmlWriter.Worksheet Hoja_Eadop = Libro.Worksheets.Add("EADOP");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();

            //  para la hoja de Informes sobre Pasivos Contingentes
            CarlosAg.ExcelXmlWriter.Worksheet Hoja_Ipc = Libro.Worksheets.Add("IPC");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_Ipc = Hoja_Ipc.Table.Rows.Add();

            #endregion
            #region Estado de Situación Financiera
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//  1 indice.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//  2 NOMBRE / NOTA.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  3 SALDO EJERCICIO.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//  4 Nota.

            //  se llena el encabezado principal
            Renglon = Hoja.Table.Rows.Add();
            Celda = Renglon.Cells.Add("NOMBRE DEL ENTE");
            Celda.MergeAcross = 3; // Merge 4 cells together
            Celda.StyleID = "Encabezado";

            Renglon = Hoja.Table.Rows.Add();
            Celda = Renglon.Cells.Add("ESTADO DE SITUACIÓN FINANCIERA DEL 01 DE " + Mes + " AL " + Fecha + " DE " +
                Mes + " DE " + Cmb_Anio.SelectedValue);
            Celda.MergeAcross = 3; // Merge 4 cells together
            Celda.StyleID = "Encabezado";

            Renglon = Hoja.Table.Rows.Add();
            Celda = Renglon.Cells.Add(" (EN  PESOS) ");
            Celda.MergeAcross = 3; // Merge 4 cells together
            Celda.StyleID = "Encabezado";

            Renglon = Hoja.Table.Rows.Add();
            Renglon = Hoja.Table.Rows.Add();

            Celda = Renglon.Cells.Add("ÍNDICE");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("NOMBRE / NOTA");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";


            Celda = Renglon.Cells.Add("SALDO EJERCICIO ACTUAL MES O TRIM.");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("NOTA");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Renglon = Hoja.Table.Rows.Add();

            foreach (DataRow Renglon_Reporte in Dt_Principal.Rows)
            {
                Indice = Renglon_Reporte["INDICE"].ToString();

                if (Convert.ToDouble(Renglon_Reporte["ACUMULADO"].ToString()) != 0)
                {
                    if (Convert.ToDouble(Indice) == 1000)
                        Total_Cuenta_1000 = Convert.ToDouble(Renglon_Reporte["ACUMULADO"]);

                    else if (Convert.ToDouble(Indice) == 2000)
                    {
                        Total_Cuenta_2000 = Convert.ToDouble(Renglon_Reporte["ACUMULADO"]);

                        Formato_Importe = String.Format("{0:n}", Total_Cuenta_1000);

                        Renglon = Hoja.Table.Rows.Add();
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Total Activo", "Contenido_Nombre_Negritas"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales_Negritas"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nota"));
                        Renglon = Hoja.Table.Rows.Add();
                    }
                    else if (Convert.ToDouble(Indice) == 3000)
                    {
                        Total_Cuenta_3000 = Convert.ToDouble(Renglon_Reporte["ACUMULADO"]);

                        Formato_Importe = String.Format("{0:n}", Total_Cuenta_2000);

                        Renglon = Hoja.Table.Rows.Add();
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Total Pasivo", "Contenido_Nombre_Negritas"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales_Negritas"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nota"));
                        Renglon = Hoja.Table.Rows.Add();
                    }
                    Renglon = Hoja.Table.Rows.Add();
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Renglon_Reporte["INDICE"], "Contenido_Indice"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Renglon_Reporte["NOMBRE"], "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Renglon_Reporte["ACUMULADO"]);
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Renglon_Reporte["NOTA"], "Contenido_Nota"));

                }// fin del if de saldo diferente a cero

            }// fin del for

            //  para el total de la cuenta 3000
            Renglon = Hoja.Table.Rows.Add();
            Formato_Importe = String.Format("{0:n}", Total_Cuenta_3000);
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Total Patrimonio", "Contenido_Nombre_Negritas"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales_Negritas"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nota"));
            Renglon = Hoja.Table.Rows.Add();

            //  para el total de pasivo mas patrimonio
            Renglon = Hoja.Table.Rows.Add();
            Total_Cuenta_1000 = Total_Cuenta_2000 + Total_Cuenta_3000;
            Formato_Importe = String.Format("{0:n}", Total_Cuenta_1000);
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Total Pasivo más Patrimonio", "Contenido_Nombre_Negritas"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales_Negritas"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nota"));
            Renglon = Hoja.Table.Rows.Add();

            Renglon = Hoja.Table.Rows.Add();
            Renglon = Hoja.Table.Rows.Add();
            //  se la leyenda de protesta
            Renglon = Hoja.Table.Rows.Add();
            Celda = Renglon.Cells.Add("Bajo protesta de decir verdad declaramos que los Estados Financieros y sus notas, son razonablemente correctos y son responsabilidad del emisor.");
            Celda.MergeAcross = 3; // Merge 4 cells together
            Celda.MergeDown = 1;// 2
            Celda.StyleID = "Encabezado_Izquierda";

            Renglon = Hoja.Table.Rows.Add();
            Renglon = Hoja.Table.Rows.Add();
            Renglon = Hoja.Table.Rows.Add();

            //  para la firma
            Renglon = Hoja.Table.Rows.Add();
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));

            //  para los responsables de firmar
            Renglon = Hoja.Table.Rows.Add();
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Titular de la Entidad", "Contenido_Nombre"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Responsable del área administrativa", "Contenido_Nota"));
            #endregion
            #region Estado de Actividades/Resultados
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//  1 indice.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  3 SALDO EJERCICIO.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  4 saldo final.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//  5 Nota.

            //  se llena el encabezado principal
            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            Celda = Renglon_Ea.Cells.Add("NOMBRE DEL ENTE");
            Celda.MergeAcross = 4; // Merge 5 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            Celda = Renglon_Ea.Cells.Add("ESTADO DE ACTIVIDADES DEL 01 DE " + Mes + " AL " + Fecha + " DE " +
                Mes + " DE " + Cmb_Anio.SelectedValue);
            Celda.MergeAcross = 4; // Merge 5 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            Celda = Renglon_Ea.Cells.Add(" (EN  PESOS) ");
            Celda.MergeAcross = 4; // Merge 5 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            Renglon_Ea = Hoja_Ea.Table.Rows.Add();

            Celda = Renglon_Ea.Cells.Add("ÍNDICE");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon_Ea.Cells.Add("NOMBRE");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";


            Celda = Renglon_Ea.Cells.Add("MOVIMIENTOS DEL PERIODO REPORTADO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon_Ea.Cells.Add("SALDO FINAL DEL PERIODO REPORTADO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon_Ea.Cells.Add("NOTA");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Renglon_Ea = Hoja_Ea.Table.Rows.Add();

            foreach (DataRow Renglon_Reporte in Dt_Actividades.Rows)
            {
                Renglon_Ea = Hoja_Ea.Table.Rows.Add();
                Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Renglon_Reporte["INDICE"], "Contenido_Indice"));
                Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Renglon_Reporte["NOMBRE"], "Contenido_Nombre"));
                Formato_Importe = String.Format("{0:n}", Renglon_Reporte["DIFERENCIA"]);
                Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                Formato_Importe = String.Format("{0:n}", Renglon_Reporte["SALDO_FINAL"]);
                Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Renglon_Reporte["NOTA"], "Contenido_Nota"));
            }

            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            //  se la leyenda de protesta
            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            Celda = Renglon_Ea.Cells.Add("Bajo protesta de decir verdad declaramos que los Estados Financieros y sus notas, son razonablemente correctos y son responsabilidad del emisor.");
            Celda.MergeAcross = 4; // Merge 5 cells together
            Celda.MergeDown = 1;//  2 celdas juntas para abajo
            Celda.StyleID = "Encabezado_Izquierda";

            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            Renglon_Ea = Hoja_Ea.Table.Rows.Add();

            //  para la firma
            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));
            Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));

            //  para los responsables de firmar
            Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Titular de la Entidad", "Contenido_Nombre"));
            Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Responsable del área administrativa", "Contenido_Nota"));
            #endregion
            #region Estado de Variación en la Hacienda Pública

            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Evhp.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//  1 indice.
            Hoja_Evhp.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//  2 NOMBRE / NOTA.
            Hoja_Evhp.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  3 SALDO final anterior.
            Hoja_Evhp.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//  4 cargos.
            Hoja_Evhp.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//   5 abonos.
            Hoja_Evhp.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//  6 saldo final
            Hoja_Evhp.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//  7 variaciones.
            Hoja_Evhp.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//  8 Nota.

            //  se llena el encabezado principal
            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Celda = Renglon_Evhp.Cells.Add("NOMBRE DEL ENTE");
            Celda.MergeAcross = 7; // Merge 8 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Celda = Renglon_Evhp.Cells.Add("ESTADO DE VARIACIONES EN LA HACIENDA PÚBLICA DEL 01 DE " + Mes + " AL " + Fecha + " DE " +
                Mes + " DE " + Cmb_Anio.SelectedValue);
            Celda.MergeAcross = 7; // Merge 8 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Celda = Renglon_Evhp.Cells.Add(" (EN  PESOS) ");
            Celda.MergeAcross = 7; // Merge 8 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();

            //PERIODO ANTERIOR
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PERIODO ANTERIOR", "Encabezado"));


            Celda = Renglon_Evhp.Cells.Add("EJERCICIO ACTUAL");
            Celda.MergeAcross = 2; // Merge 3 cells together
            Celda.StyleID = "Encabezado";
            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            //Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();

            //Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INDICE", "Encabezado"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CONCEPTO", "Encabezado"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARGOS", "Encabezado"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ABONOS", "Encabezado"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("VARIACION", "Encabezado"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA", "Encabezado"));

            foreach (DataRow Renglon_Reporte in Dt_Variaciones.Rows)
            {
                Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
                Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Renglon_Reporte["INDICE"], "Contenido_Indice"));
                Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Renglon_Reporte["NOMBRE"], "Contenido_Nombre"));

                Formato_Importe = String.Format("{0:n}", Renglon_Reporte["SALDO_FINAL_ANTERIOR"]);
                Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));

                Formato_Importe = String.Format("{0:n}", Renglon_Reporte["CARGOS"]);
                Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));

                Formato_Importe = String.Format("{0:n}", Renglon_Reporte["ABONO"]);
                Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));

                Formato_Importe = String.Format("{0:n}", Renglon_Reporte["SALDO_FINAL"]);
                Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));

                Formato_Importe = String.Format("{0:n}", Renglon_Reporte["FLUJO"]);
                Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));

                Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Renglon_Reporte["NOTA"], "Contenido_Nota"));
            }

            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            //  se la leyenda de protesta
            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Celda = Renglon_Evhp.Cells.Add("Bajo protesta de decir verdad declaramos que los Estados Financieros y sus notas, son razonablemente correctos y son responsabilidad del emisor.");
            Celda.MergeAcross = 7; // Merge 8 cells together
            Celda.MergeDown = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Izquierda";

            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();

            //  para la firma
            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));

            //  para los responsables de firmar
            Renglon_Evhp = Hoja_Evhp.Table.Rows.Add();
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Titular de la Entidad", "Contenido_Nombre"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            Renglon_Evhp.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Responsable del área administrativa", "Contenido_Nota"));
            #endregion
            #region Estado de Flujos de Efectivo
            // Renglon_Efe = Hoja_Efe.Table.Rows.Add()
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Efe.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//  1 indice.
            Hoja_Efe.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(280));// 2 NOMBRE 
            Hoja_Efe.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));// 3 IMPORTES.
            Hoja_Efe.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//  4 Nota.

            //  se llena el encabezado principal
            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            Celda = Renglon_Efe.Cells.Add("NOMBRE DEL ENTE");
            Celda.MergeAcross = 3; // Merge 4 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            Celda = Renglon_Efe.Cells.Add("ESTADO DE FLUJO DE EFECTIVO DEL 01 DE " + Mes + " AL " + Fecha + " DE " +
                Mes + " DE " + Cmb_Anio.SelectedValue);
            Celda.MergeAcross = 3; // Merge 4 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            Celda = Renglon_Efe.Cells.Add(" (EN  PESOS) ");
            Celda.MergeAcross = 3; // Merge 4 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Efe = Hoja_Efe.Table.Rows.Add();

            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INDICE", "Encabezado"));
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CONCEPTO", "Encabezado"));
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("IMPORTES", "Encabezado"));
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA", "Encabezado"));

            String Estilo = "";//Encabezado,Contenido_Nombre_Negritas
            foreach (DataRow Registro in Dt_Flujo.Rows)
            {
                if (Registro["INDICE"].ToString() == "")
                {
                    //  para el estilo centrado en negritas;
                    if (Registro["NOMBRE"].ToString() == "ACTIVIDADES DE OPERACIÓN" || Registro["NOMBRE"].ToString() == "ACTIVIDADES DE INVERSIÓN" ||
                        Registro["NOMBRE"].ToString() == "ACTIVIDADES DE FINANCIAMIENTO")
                        Estilo = "Encabezado";

                    else
                        Estilo = "Contenido_Nombre_Negritas";

                    Renglon_Efe = Hoja_Efe.Table.Rows.Add();
                    Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["INDICE"].ToString(), "Contenido_Indice"));
                    Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), Estilo));
                    Formato_Importe = String.Format("{0:n}", Registro["IMPORTE"]);
                    Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales_Negritas"));
                    Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOTA"].ToString(), "Contenido_Nombre_Negritas"));
                }


                else
                {
                    Renglon_Efe = Hoja_Efe.Table.Rows.Add();
                    Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["INDICE"].ToString(), "Contenido_Indice"));
                    Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Registro["IMPORTE"]);
                    Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                    Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOTA"].ToString(), "Contenido_Nota"));
                }
            }

            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            //  se la leyenda de protesta
            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            Celda = Renglon_Efe.Cells.Add("Bajo protesta de decir verdad declaramos que los Estados Financieros y sus notas, son razonablemente correctos y son responsabilidad del emisor.");
            Celda.MergeAcross = 3; // Merge 4 cells together
            Celda.MergeDown = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Izquierda";

            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            Renglon_Efe = Hoja_Efe.Table.Rows.Add();

            //  para la firma
            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            //Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));

            Celda = Renglon_Efe.Cells.Add("");
            Celda.MergeAcross = 2; // Merge 3 cells together
            Celda.StyleID = "Contenido_Firma";

            //  para los responsables de firmar
            Renglon_Efe = Hoja_Efe.Table.Rows.Add();
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Titular de la Entidad", "Contenido_Nombre"));
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            Renglon_Efe.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Responsable del área administrativa", "Contenido_Nota"));

            #endregion
            #region Estado Analítico del Activo
            // Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Eaa.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//  1 indice.
            Hoja_Eaa.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));// 2 NOMBRE 
            Hoja_Eaa.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));// 3 saldo inicial
            Hoja_Eaa.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  4 cargo.
            Hoja_Eaa.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));// 5 abono
            Hoja_Eaa.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));// 6 saldo final.
            Hoja_Eaa.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  7 flujo.

            //  se llena el encabezado principal
            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            Celda = Renglon_Eaa.Cells.Add("NOMBRE DEL ENTE");
            Celda.MergeAcross = 6; // Merge 7 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            Celda = Renglon_Eaa.Cells.Add("ESTADO ANALÍTICO DEL ACTIVO DEL 01 DE " + Mes + " AL " + Fecha + " DE " +
                Mes + " DE " + Cmb_Anio.SelectedValue);
            Celda.MergeAcross = 6; // Merge 7 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            Celda = Renglon_Eaa.Cells.Add(" (EN  PESOS) ");
            Celda.MergeAcross = 6; // Merge 7 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();

            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INDICE", "Encabezado"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CONCEPTO", "Encabezado"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARGO", "Encabezado"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ABONO", "Encabezado"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));

            //  para ordenar la tabla
            DataView Dv_Ordenar = new DataView(Dt_Analitico);
            Dv_Ordenar.Sort = "INDICE";
            Dt_Analitico = Dv_Ordenar.ToTable();

            foreach (DataRow Registro in Dt_Analitico.Rows)
            {
                Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
                Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["INDICE"].ToString(), "Contenido_Indice"));
                Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["DESCRIPCION "].ToString(), "Contenido_Nombre"));
                Formato_Importe = String.Format("{0:n}", Registro["SALDO_INICIAL"]);
                Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                Formato_Importe = String.Format("{0:n}", Registro["CARGO"]);
                Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                Formato_Importe = String.Format("{0:n}", Registro["ABONO"]);
                Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                Formato_Importe = String.Format("{0:n}", Registro["SALDO_FINAL"]);
                Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                Formato_Importe = String.Format("{0:n}", Registro["FLUJO"]);
                Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
            }

            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            //  se la leyenda de protesta
            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            Celda = Renglon_Eaa.Cells.Add("Bajo protesta de decir verdad declaramos que los Estados Financieros y sus notas, son razonablemente correctos y son responsabilidad del emisor.");
            Celda.MergeAcross = 6; // Merge 7 cells together
            Celda.MergeDown = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Izquierda";

            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();

            //  para la firma
            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            //Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));

            Celda = Renglon_Eaa.Cells.Add("");
            Celda.MergeAcross = 2; // Merge 3 cells together
            Celda.StyleID = "Contenido_Firma";

            //  para los responsables de firmar
            Renglon_Eaa = Hoja_Eaa.Table.Rows.Add();
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Titular de la Entidad", "Contenido_Nombre"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            Renglon_Eaa.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Responsable del área administrativa", "Contenido_Nota"));

            #endregion
            #region Estado Analítico de la Deuda y Otros Pasivos
            //  Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Eadop.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//  1 indice.
            Hoja_Eadop.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));// 2 NOMBRE 
            Hoja_Eadop.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));// 3 saldo inicial
            Hoja_Eadop.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  4 cargo.
            Hoja_Eadop.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));// 5 abono
            Hoja_Eadop.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));// 6 saldo final.
            Hoja_Eadop.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  7 flujo.

            //  se llena el encabezado principal
            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            Celda = Renglon_Eadop.Cells.Add("NOMBRE DEL ENTE");
            Celda.MergeAcross = 6; // Merge 7 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            Celda = Renglon_Eadop.Cells.Add("ESTADO ANALÍTICO DE LA DEUDA Y OTROS PASIVOS DEL 01 DE " + Mes + " AL " + Fecha + " DE " +
                Mes + " DE " + Cmb_Anio.SelectedValue);
            Celda.MergeAcross = 6; // Merge 7 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            Celda = Renglon_Eadop.Cells.Add(" (EN  PESOS) ");
            Celda.MergeAcross = 6; // Merge 7 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();

            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INDICE", "Encabezado"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CONCEPTO", "Encabezado"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARGO", "Encabezado"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ABONO", "Encabezado"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));

            //  para ordenar la tabla
            Dv_Ordenar = new DataView(Dt_Analitico_Deuda);
            Dv_Ordenar.Sort = "INDICE";
            Dt_Analitico_Deuda = Dv_Ordenar.ToTable();

            foreach (DataRow Registro in Dt_Analitico_Deuda.Rows)
            {
                Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
                Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["INDICE"].ToString(), "Contenido_Indice"));
                Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                Formato_Importe = String.Format("{0:n}", Registro["SALDO_INICIAL"]);
                Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                Formato_Importe = String.Format("{0:n}", Registro["CARGO"]);
                Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                Formato_Importe = String.Format("{0:n}", Registro["ABONO"]);
                Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                Formato_Importe = String.Format("{0:n}", Registro["SALDO_FINAL"]);
                Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
                Formato_Importe = String.Format("{0:n}", Registro["FLUJO"]);
                Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales"));
            }

            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            //  se la leyenda de protesta
            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            Celda = Renglon_Eadop.Cells.Add("Bajo protesta de decir verdad declaramos que los Estados Financieros y sus notas, son razonablemente correctos y son responsabilidad del emisor.");
            Celda.MergeAcross = 6; // Merge 7 cells together
            Celda.MergeDown = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Izquierda";

            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();

            //  para la firma
            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            //Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Firma"));

            Celda = Renglon_Eadop.Cells.Add("");
            Celda.MergeAcross = 2; // Merge 3 cells together
            Celda.StyleID = "Contenido_Firma";

            //  para los responsables de firmar
            Renglon_Eadop = Hoja_Eadop.Table.Rows.Add();
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Indice"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Titular de la Entidad", "Contenido_Nombre"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            Renglon_Eadop.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Responsable del área administrativa", "Contenido_Nota"));

            #endregion
            ////Abre el archivo de excel
            //Asignar la ruta del archivo
            Ruta_Archivo = HttpContext.Current.Server.MapPath("~") + "\\Exportaciones\\" + Nombre_Archivo;
            Libro.Save(Ruta_Archivo);
            Mostrar_Reporte(Nombre_Archivo, "Excel");
        }// fin try
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Rpt_Excel_Notas
    /// DESCRIPCION :   Se encarga de generar el archivo de excel pasandole los paramentros
    ///                 al documento
    /// PARAMETROS  :   Dt_situacion.- Es la consulta que contiene las notas esf
    ///                 Ds_Actividades.- Es la consulta que contiene las notas era
    ///                 Ds_Variaciones.- Es la consulta que contiene las notas vhp
    ///                 Ds_Flujo.- Es la consulta que contiene las notas efe
    /// CREO        :   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   07/Marzo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Generar_Rpt_Excel_Notas(DataSet Ds_Situacion, DataSet Ds_Actividades, DataSet Ds_Variaciones, DataSet Ds_Flujo, DataSet Ds_Principal)
    {
        #region Variables
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Documentos = new DataTable();
        String Nombre_Archivo = "";
        String Ruta_Archivo = "";
        String Dia = "";
        Double Suma_Nota_2 = 0;
        String Año = "";
        String Mes_Numerico = "";
        Double Año_Anterior = 0.0;
        String Formato_Importe = "";
        String Formato_Importe_2 = "";
        Double Suma_Nota = 0.0;
        Double Suma_Inicial = 0.0;
        Double Suma_Final = 0.0;
        Double Suma_Modificado = 0.0;
        Double Suma_Nota_2010 = 0.0;//  para la cuenta de 2010 o para 365 dias
        Double Suma_Nota_2011 = 0.0;//  para la cuenta de 2011 o para mas de 365 dias
        Double Suma_Nota_90 = 0.0; //   para 90 dias
        Double Suma_Nota_180 = 0.0;//   para 180 dias
        DataTable Dt_Inicial = new DataTable();
        DataTable Dt_Esf_01 = new DataTable();
        DataTable Dt_Esf_011 = new DataTable();
        DataTable Dt_Esf_012 = new DataTable();
        DataTable Dt_Esf_013 = new DataTable();
        DataTable Dt_Esf_02 = new DataTable();
        DataTable Dt_Esf_021 = new DataTable();
        DataTable Dt_Esf_03 = new DataTable();
        DataTable Dt_Esf_031 = new DataTable();
        DataTable Dt_Esf_05 = new DataTable();
        DataTable Dt_Esf_051 = new DataTable();
        DataTable Dt_Esf_06 = new DataTable();
        DataTable Dt_Esf_07 = new DataTable();
        DataTable Dt_Esf_08 = new DataTable();
        DataTable Dt_Esf_081 = new DataTable();
        DataTable Dt_Esf_08_126 = new DataTable();
        DataTable Dt_Esf_09 = new DataTable();
        DataTable Dt_Esf_091 = new DataTable();
        DataTable Dt_Esf_1265 = new DataTable();
        DataTable Dt_Esf_10 = new DataTable();
        DataTable Dt_Esf_11 = new DataTable();
        DataTable Dt_Esf_12 = new DataTable();
        DataTable Dt_Esf_13 = new DataTable();
        DataTable Dt_ESF_13_1 = new DataTable();
        DataTable Dt_ESF_13_2 = new DataTable();
        DataTable Dt_Esf_14 = new DataTable();
        DataTable Dt_Esf_15 = new DataTable();
        DataTable Dt_Vhp_01 = new DataTable();
        DataTable Dt_Vhp_02 = new DataTable();
        DataTable Dt_Era_01 = new DataTable();
        DataTable Dt_Era_02 = new DataTable();
        DataTable Dt_Era_03 = new DataTable();
        DataTable Dt_Efe_01 = new DataTable();
        DataTable Dt_Efe_02 = new DataTable();
        string Campo_Oculto_CONAC = string.Empty; //variable para el campo oculto de la conac
        #endregion
        try
        {
            #region Cargar Datatable
            //Dt_Principal = Ds_Situacion.Tables[0].Copy();
            Dt_Inicial = Ds_Principal.Tables[0].Copy();
            Dt_Esf_01 = Ds_Situacion.Tables[2].Copy();
            Dt_Esf_011 = Ds_Situacion.Tables[3].Copy();
            Dt_Esf_012 = Ds_Situacion.Tables[4].Copy();
            Dt_Esf_013 = Ds_Situacion.Tables[5].Copy();
            Dt_Esf_02 = Ds_Situacion.Tables[6].Copy();
            Dt_Esf_021 = Ds_Situacion.Tables[7].Copy();
            Dt_Esf_03 = Ds_Situacion.Tables[8].Copy();
            Dt_Esf_031 = Ds_Situacion.Tables[9].Copy();
            Dt_Esf_05 = Ds_Situacion.Tables[10].Copy();
            Dt_Esf_051 = Ds_Situacion.Tables[11].Copy();
            Dt_Esf_06 = Ds_Situacion.Tables[12].Copy();
            Dt_Esf_07 = Ds_Situacion.Tables[13].Copy();
            Dt_Esf_08 = Ds_Situacion.Tables[14].Copy();
            Dt_Esf_081 = Ds_Situacion.Tables[15].Copy();
            Dt_Esf_08_126 = Ds_Situacion.Tables[16].Copy();
            Dt_Esf_09 = Ds_Situacion.Tables[17].Copy();
            Dt_Esf_091 = Ds_Situacion.Tables[18].Copy();
            Dt_Esf_1265 = Ds_Situacion.Tables[19].Copy();
            Dt_Esf_10 = Ds_Situacion.Tables[20].Copy();
            Dt_Esf_11 = Ds_Situacion.Tables[21].Copy();
            Dt_Esf_12 = Ds_Situacion.Tables[22].Copy();
            Dt_Esf_13 = Ds_Situacion.Tables[23].Copy();
            Dt_ESF_13_1 = Ds_Situacion.Tables[24].Copy();
            Dt_ESF_13_2 = Ds_Situacion.Tables[25].Copy();
            Dt_Esf_14 = Ds_Situacion.Tables[26].Copy();
            Dt_Esf_15 = Ds_Situacion.Tables[27].Copy();
            Dt_Vhp_01 = Ds_Situacion.Tables[28].Copy();
            Dt_Vhp_02 = Ds_Situacion.Tables[29].Copy();

            Dt_Era_01 = Ds_Actividades.Tables[0].Copy();
            Dt_Era_02 = Ds_Actividades.Tables[1].Copy();
            Dt_Era_03 = Ds_Actividades.Tables[2].Copy();
            Dt_Efe_01 = Ds_Actividades.Tables[3].Copy();
            Dt_Efe_02 = Ds_Actividades.Tables[4].Copy();


            #endregion
            //  Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            //  propiedades del libro
            Libro.Properties.Title = "Cuenta Pública";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI_Irapuato";
            #region Hojas
            //  para la hoja principal
            Worksheet Hoja_Esf = Libro.Worksheets.Add("Notas a los Edos Financieros");
            //  Agregamos un renglón a la hoja de excel.
            WorksheetRow Renglon_Esf = Hoja_Esf.Table.Rows.Add();
            //  para la hoja de la nota 1
            Worksheet Hoja_Esf_01 = Libro.Worksheets.Add("ESF-01");
            WorksheetRow Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            //  para la hoja de la nota 2
            Worksheet Hoja_Esf_02 = Libro.Worksheets.Add("ESF-02");
            WorksheetRow Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            //  para la hoja de la nota 3
            Worksheet Hoja_Esf_03 = Libro.Worksheets.Add("ESF-03");
            WorksheetRow Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            //  para la hoja de la nota 5
            Worksheet Hoja_Esf_05 = Libro.Worksheets.Add("ESF-05");
            WorksheetRow Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            //  para la hoja de la nota 6
            Worksheet Hoja_Esf_06 = Libro.Worksheets.Add("ESF-06");
            WorksheetRow Renglon_Esf_06 = Hoja_Esf_06.Table.Rows.Add();
            //  para la hoja de la nota 7
            Worksheet Hoja_Esf_07 = Libro.Worksheets.Add("ESF-07");
            WorksheetRow Renglon_Esf_07 = Hoja_Esf_07.Table.Rows.Add();
            //  para la hoja de la nota 8
            Worksheet Hoja_Esf_08 = Libro.Worksheets.Add("ESF-08");
            WorksheetRow Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            //  para la hoja de la nota 9
            Worksheet Hoja_Esf_09 = Libro.Worksheets.Add("ESF-09");
            WorksheetRow Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            //  para la hoja de la nota 10
            Worksheet Hoja_Esf_10 = Libro.Worksheets.Add("ESF-10");
            WorksheetRow Renglon_Esf_10 = Hoja_Esf_10.Table.Rows.Add();
            //  para la hoja de la nota 11
            Worksheet Hoja_Esf_11 = Libro.Worksheets.Add("ESF-11");
            WorksheetRow Renglon_Esf_11 = Hoja_Esf_11.Table.Rows.Add();
            //  para la hoja de la nota 12
            Worksheet Hoja_Esf_12 = Libro.Worksheets.Add("ESF-12");
            WorksheetRow Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
            //  para la hoja de la nota 13
            Worksheet Hoja_Esf_13 = Libro.Worksheets.Add("ESF-13");
            WorksheetRow Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            //  para la hoja de la nota 14
            Worksheet Hoja_Esf_14 = Libro.Worksheets.Add("ESF-14");
            WorksheetRow Renglon_Esf_14 = Hoja_Esf_14.Table.Rows.Add();
            //  para la hoja de la nota 14
            Worksheet Hoja_Esf_15 = Libro.Worksheets.Add("ESF-15");
            WorksheetRow Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            //  para la hoja de la nota era 01
            Worksheet Hoja_Era_01 = Libro.Worksheets.Add("ERA-01");
            WorksheetRow Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
            //  para la hoja de la nota era 01
            Worksheet Hoja_Era_02 = Libro.Worksheets.Add("ERA-02");
            WorksheetRow Renglon_Era_02 = Hoja_Era_02.Table.Rows.Add();
            //  para la hoja de la nota era 01
            Worksheet Hoja_Era_03 = Libro.Worksheets.Add("ERA-03");
            WorksheetRow Renglon_Era_03 = Hoja_Era_03.Table.Rows.Add();
            //  para la hoja de la nota vhp01
            Worksheet Hoja_Vhp_01 = Libro.Worksheets.Add("VHP-01");
            WorksheetRow Renglon_Vhp_01 = Hoja_Vhp_01.Table.Rows.Add();
            //  para la hoja de la nota vhp02
            Worksheet Hoja_Vhp_02 = Libro.Worksheets.Add("VHP-02");
            WorksheetRow Renglon_Vhp_02 = Hoja_Vhp_02.Table.Rows.Add();
            //  para la hoja de la nota efe 01
            Worksheet Hoja_Efe_01 = Libro.Worksheets.Add("EFE-01");
            WorksheetRow Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();
            //  para la hoja de la nota efe 02
            Worksheet Hoja_Efe_02 = Libro.Worksheets.Add("EFE-02");
            WorksheetRow Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
            //  para la hoja de la nota efe 02
            Worksheet Hoja_Con = Libro.Worksheets.Add("Memoria");
            WorksheetRow Renglon_Con = Hoja_Con.Table.Rows.Add();
            #endregion
            //  Creamos el estilo cabecera para la hoja de excel. 
            WorksheetStyle Estilo_Cabecera_Inicial = Libro.Styles.Add("Encabezado_Inicial");
            //  Creamos el estilo cabecera para la hoja de excel. 
            WorksheetStyle Estilo_Cabecera_Inicial_Centrado = Libro.Styles.Add("Encabezado_Inicial_Centrado");
            //  Creamos el estilo cabecera para la hoja de excel. 
            WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("Encabezado");
            WorksheetStyle Estilo_Cabecera_Sin_Linea = Libro.Styles.Add("Encabezado_Sin_Linea");
            WorksheetStyle Estilo_Cabecera_Sin_Linea_2 = Libro.Styles.Add("Encabezado_Sin_Linea_2");
            //  Creamos el estilo cabecera para la hoja de excel. 
            WorksheetStyle Estilo_Cabecera_Izquierda = Libro.Styles.Add("Encabezado_Izquierda");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            WorksheetStyle Estilo_Contenido_Indice = Libro.Styles.Add("Contenido_Indice");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            WorksheetStyle Estilo_Contenido_Nombre = Libro.Styles.Add("Contenido_Nombre");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            WorksheetStyle Estilo_Contenido_Nota = Libro.Styles.Add("Contenido_Nota");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            WorksheetStyle Estilo_Contenido_Nombre_Negritas = Libro.Styles.Add("Contenido_Nombre_Negritas");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            WorksheetStyle Estilo_Contenido_Firma = Libro.Styles.Add("Contenido_Firma");
            //  Creamos el estilo contenido del presupuesto para la hoja de excel. 
            WorksheetStyle Estilo_Totales = Libro.Styles.Add("Totales");
            //  Creamos el estilo contenido del presupuesto para la hoja de excel. 
            WorksheetStyle Estilo_Totales_Negritas = Libro.Styles.Add("Totales_Negritas");
            //  Creamos el estilo contenido del presupuesto para la hoja de excel. 
            WorksheetStyle Estilo_Totales_Verde = Libro.Styles.Add("Totales_Verde");
            WorksheetStyle Estilo_Totales_Verde_Texto = Libro.Styles.Add("Totales_Verde_Texto");
            WorksheetStyle Estilo_Contenido_Nombre_Sin_Encerrar = Libro.Styles.Add("Contenido_Nombre_Sin_Encerrar");
            WorksheetStyle Estilo_Contenido_Nombre_Sin_Encerrar_Negrita = Libro.Styles.Add("Contenido_Nombre_Sin_Encerrar_Negrita");
            WorksheetStyle Estilo_Contenido_Nombre_Punteado = Libro.Styles.Add("Contenido_Nombre_Punteado");
            WorksheetStyle Estilo_Contenido_Nombre_Punteado_Negritas = Libro.Styles.Add("Contenido_Nombre_Punteado_Negritas");
            WorksheetStyle Estilo_Contenido_Inferior_Vacio = Libro.Styles.Add("Contenido_Inferior_Vacio");
            WorksheetStyle Estilo_Encabezado_Transparente = Libro.Styles.Add("Encabezado_Transparente");
            WorksheetCell Celda = new WorksheetCell();
            #region Estilos
            //***************************************inicio de los estilos***********************************************************
            //  estilo para la cabecera    Encabezado
            Estilo_Cabecera_Inicial.Font.FontName = "Arial";
            Estilo_Cabecera_Inicial.Font.Size = 8;
            Estilo_Cabecera_Inicial.Font.Bold = true;
            Estilo_Cabecera_Inicial.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Cabecera_Inicial.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera_Inicial.Alignment.Rotate = 0;
            Estilo_Cabecera_Inicial.Font.Color = "#000000";
            Estilo_Cabecera_Inicial.Interior.Color = "white";
            Estilo_Cabecera_Inicial.Interior.Pattern = StyleInteriorPattern.None;

            Estilo_Cabecera_Inicial_Centrado.Font.FontName = "Arial";
            Estilo_Cabecera_Inicial_Centrado.Font.Size = 8;
            Estilo_Cabecera_Inicial_Centrado.Font.Bold = true;
            Estilo_Cabecera_Inicial_Centrado.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera_Inicial_Centrado.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera_Inicial_Centrado.Alignment.Rotate = 0;
            Estilo_Cabecera_Inicial_Centrado.Font.Color = "#000000";
            Estilo_Cabecera_Inicial_Centrado.Interior.Color = "white";
            Estilo_Cabecera_Inicial_Centrado.Interior.Pattern = StyleInteriorPattern.None;
            //  estilo para la cabecera    Encabezado
            Estilo_Cabecera.Font.FontName = "Arial";
            Estilo_Cabecera.Font.Size = 8;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#000000";
            Estilo_Cabecera.Interior.Color = "#F7CC99";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la cabecera sin linea derecha
            Estilo_Cabecera_Sin_Linea.Font.FontName = "Arial";
            Estilo_Cabecera_Sin_Linea.Font.Size = 8;
            Estilo_Cabecera_Sin_Linea.Font.Bold = true;
            Estilo_Cabecera_Sin_Linea.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera_Sin_Linea.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera_Sin_Linea.Alignment.Rotate = 0;
            Estilo_Cabecera_Sin_Linea.Font.Color = "#000000";
            Estilo_Cabecera_Sin_Linea.Interior.Color = "#F7CC99";
            Estilo_Cabecera_Sin_Linea.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera_Sin_Linea.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera_Sin_Linea.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera_Sin_Linea.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la cabecera sin linea izquierda
            Estilo_Cabecera_Sin_Linea_2.Font.FontName = "Arial";
            Estilo_Cabecera_Sin_Linea_2.Font.Size = 8;
            Estilo_Cabecera_Sin_Linea_2.Font.Bold = true;
            Estilo_Cabecera_Sin_Linea_2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera_Sin_Linea_2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera_Sin_Linea_2.Alignment.Rotate = 0;
            Estilo_Cabecera_Sin_Linea_2.Font.Color = "#000000";
            Estilo_Cabecera_Sin_Linea_2.Interior.Color = "#F7CC99";
            Estilo_Cabecera_Sin_Linea_2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera_Sin_Linea_2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera_Sin_Linea_2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera_Sin_Linea_2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la cabecera    Encabezado_Izquierda
            Estilo_Cabecera_Izquierda.Font.FontName = "Arial";
            Estilo_Cabecera_Izquierda.Font.Size = 8;
            Estilo_Cabecera_Izquierda.Font.Bold = true;
            Estilo_Cabecera_Izquierda.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Cabecera_Izquierda.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera_Izquierda.Alignment.Rotate = 0;
            Estilo_Cabecera_Izquierda.Font.Color = "#000000";
            Estilo_Cabecera_Izquierda.Interior.Color = "#F7CC99";
            Estilo_Cabecera_Izquierda.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera_Izquierda.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera_Izquierda.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera_Izquierda.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera_Izquierda.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");


            //estilo para el contenido   contenido_indice
            Estilo_Contenido_Indice.Font.FontName = "Arial";
            Estilo_Contenido_Indice.Font.Size = 8;
            Estilo_Contenido_Indice.Font.Bold = false;
            Estilo_Contenido_Indice.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido_Indice.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Contenido_Indice.Alignment.Rotate = 0;
            Estilo_Contenido_Indice.Font.Color = "#000000";
            Estilo_Contenido_Indice.Interior.Color = "White";
            Estilo_Contenido_Indice.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Indice.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Indice.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Indice.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Indice.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el    Contenido_Nombre
            Estilo_Contenido_Nombre.Font.FontName = "Arial";
            Estilo_Contenido_Nombre.Font.Size = 8;
            Estilo_Contenido_Nombre.Font.Bold = false;
            Estilo_Contenido_Nombre.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Nombre.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre.Font.Color = "#000000";
            Estilo_Contenido_Nombre.Interior.Color = "White";
            Estilo_Contenido_Nombre.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Nombre.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nombre.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nombre.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nombre.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el    Contenido_Nombre sin encerrar
            Estilo_Contenido_Nombre_Sin_Encerrar.Font.FontName = "Arial";
            Estilo_Contenido_Nombre_Sin_Encerrar.Font.Size = 8;
            Estilo_Contenido_Nombre_Sin_Encerrar.Font.Bold = false;
            Estilo_Contenido_Nombre_Sin_Encerrar.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Nombre_Sin_Encerrar.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre_Sin_Encerrar.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre_Sin_Encerrar.Font.Color = "#000000";
            Estilo_Contenido_Nombre_Sin_Encerrar.Interior.Color = "White";
            Estilo_Contenido_Nombre_Sin_Encerrar.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Nombre_Sin_Encerrar.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el    Contenido_Nombre sin encerrar negrita
            Estilo_Contenido_Nombre_Sin_Encerrar_Negrita.Font.FontName = "Arial";
            Estilo_Contenido_Nombre_Sin_Encerrar_Negrita.Font.Size = 8;
            Estilo_Contenido_Nombre_Sin_Encerrar_Negrita.Font.Bold = true;
            Estilo_Contenido_Nombre_Sin_Encerrar_Negrita.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Nombre_Sin_Encerrar_Negrita.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre_Sin_Encerrar_Negrita.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre_Sin_Encerrar_Negrita.Font.Color = "#000000";
            Estilo_Contenido_Nombre_Sin_Encerrar_Negrita.Interior.Color = "White";
            Estilo_Contenido_Nombre_Sin_Encerrar_Negrita.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Nombre_Sin_Encerrar_Negrita.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el    Contenido_Nombre punteado
            Estilo_Contenido_Nombre_Punteado.Font.FontName = "Arial";
            Estilo_Contenido_Nombre_Punteado.Font.Size = 8;
            Estilo_Contenido_Nombre_Punteado.Font.Bold = false;
            Estilo_Contenido_Nombre_Punteado.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Nombre_Punteado.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre_Punteado.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre_Punteado.Font.Color = "#000000";
            Estilo_Contenido_Nombre_Punteado.Interior.Color = "White";
            Estilo_Contenido_Nombre_Punteado.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Nombre_Punteado.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nombre_Punteado.Borders.Add(StylePosition.Bottom, LineStyleOption.Dot, 1, "Black");

            //estilo para el    Contenido_Nombre punteado negrita
            Estilo_Contenido_Nombre_Punteado_Negritas.Font.FontName = "Arial";
            Estilo_Contenido_Nombre_Punteado_Negritas.Font.Size = 8;
            Estilo_Contenido_Nombre_Punteado_Negritas.Font.Bold = true;
            Estilo_Contenido_Nombre_Punteado_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Nombre_Punteado_Negritas.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre_Punteado_Negritas.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre_Punteado_Negritas.Font.Color = "#000000";
            Estilo_Contenido_Nombre_Punteado_Negritas.Interior.Color = "White";
            Estilo_Contenido_Nombre_Punteado_Negritas.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Nombre_Punteado_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nombre_Punteado_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Dot, 1, "Black");


            //estilo para el    Contenido_Firma
            Estilo_Contenido_Firma.Font.FontName = "Arial";
            Estilo_Contenido_Firma.Font.Size = 10;
            Estilo_Contenido_Firma.Font.Bold = false;
            Estilo_Contenido_Firma.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Firma.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Firma.Alignment.Rotate = 0;
            Estilo_Contenido_Firma.Font.Color = "#000000";
            Estilo_Contenido_Firma.Interior.Color = "White";
            Estilo_Contenido_Firma.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Firma.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Firma.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Firma.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Firma.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el    Contenido_Nombre Negritas
            Estilo_Contenido_Nombre_Negritas.Font.FontName = "Arial";
            Estilo_Contenido_Nombre_Negritas.Font.Size = 8;
            Estilo_Contenido_Nombre_Negritas.Font.Bold = true;
            Estilo_Contenido_Nombre_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Nombre_Negritas.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre_Negritas.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre_Negritas.Font.Color = "#000000";
            Estilo_Contenido_Nombre_Negritas.Interior.Color = "White";
            Estilo_Contenido_Nombre_Negritas.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Nombre_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nombre_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nombre_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nombre_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el    Contenido_Nota
            Estilo_Contenido_Nota.Font.FontName = "Arial";
            Estilo_Contenido_Nota.Font.Size = 10;
            Estilo_Contenido_Nota.Font.Bold = false;
            Estilo_Contenido_Nota.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Nota.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Contenido_Nota.Alignment.Rotate = 0;
            Estilo_Contenido_Nota.Font.Color = "#000000";
            Estilo_Contenido_Nota.Interior.Color = "White";
            Estilo_Contenido_Nota.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Nota.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nota.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nota.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Nota.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para el presupuesto (importe)
            Estilo_Totales.Font.FontName = "Arial";
            Estilo_Totales.Font.Size = 8;
            Estilo_Totales.Font.Bold = false;
            Estilo_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales.Alignment.Rotate = 0;
            Estilo_Totales.Font.Color = "#000000";
            Estilo_Totales.Interior.Color = "White";
            Estilo_Totales.NumberFormat = "_0* #,##0.00;\\-* #,##0.00_0;* \"0.00\";_-@_-";
            Estilo_Totales.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para el totales negritas
            Estilo_Totales_Negritas.Font.FontName = "Arial";
            Estilo_Totales_Negritas.Font.Size = 8;
            Estilo_Totales_Negritas.Font.Bold = true;
            Estilo_Totales_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales_Negritas.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales_Negritas.Alignment.Rotate = 0;
            Estilo_Totales_Negritas.Font.Color = "#000000";
            Estilo_Totales_Negritas.Interior.Color = "White";
            Estilo_Totales_Negritas.NumberFormat = "_0* #,##0.00;\\-* #,##0.00_0;* \"0.00\";_-@_-";
            Estilo_Totales_Negritas.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Totales_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para el totales Fondo
            Estilo_Totales_Verde.Font.FontName = "Arial";
            Estilo_Totales_Verde.Font.Size = 8;
            Estilo_Totales_Verde.Font.Bold = true;
            Estilo_Totales_Verde.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales_Verde.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales_Verde.Alignment.Rotate = 0;
            Estilo_Totales_Verde.Font.Color = "#000000";
            Estilo_Totales_Verde.Interior.Color = "#92D050";
            Estilo_Totales_Verde.NumberFormat = "_0* #,##0.00;\\-* #,##0.00_0;* \"0.00\";_-@_-";
            Estilo_Totales_Verde.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Totales_Verde.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Verde.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Verde.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Verde.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para el totales Fondo texto
            Estilo_Totales_Verde_Texto.Font.FontName = "Arial";
            Estilo_Totales_Verde_Texto.Font.Size = 8;
            Estilo_Totales_Verde_Texto.Font.Bold = true;
            Estilo_Totales_Verde_Texto.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Totales_Verde_Texto.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales_Verde_Texto.Alignment.Rotate = 0;
            Estilo_Totales_Verde_Texto.Font.Color = "#000000";
            Estilo_Totales_Verde_Texto.Interior.Color = "#92D050";
            Estilo_Totales_Verde_Texto.NumberFormat = "_0* #,##0.00;\\-* #,##0.00_0;* \"0.00\";_-@_-";
            Estilo_Totales_Verde_Texto.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Totales_Verde_Texto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Verde_Texto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Verde_Texto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales_Verde_Texto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Contenido_Inferior_Vacio.Font.FontName = "Arial";
            Estilo_Contenido_Inferior_Vacio.Font.Size = 8;
            Estilo_Contenido_Inferior_Vacio.Font.Bold = true;
            Estilo_Contenido_Inferior_Vacio.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Inferior_Vacio.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Inferior_Vacio.Alignment.Rotate = 0;
            Estilo_Contenido_Inferior_Vacio.Font.Color = "#000000";
            Estilo_Contenido_Inferior_Vacio.Interior.Color = "White";
            Estilo_Contenido_Inferior_Vacio.Interior.Pattern = StyleInteriorPattern.None;
            Estilo_Contenido_Inferior_Vacio.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Inferior_Vacio.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");

            Estilo_Encabezado_Transparente.Font.FontName = "Arial";
            Estilo_Encabezado_Transparente.Font.Size = 8;
            Estilo_Encabezado_Transparente.Font.Color = "White";
            #endregion
            #region Situacion Financiera
            String Mes = Cmb_Mes.SelectedValue;
            Mes = Mes.Substring(0, 2);
            Mes_Numerico = Mes;
            Mes = Asignar_Mes(Mes);
            Mes = Mes.Substring(0, 3);
            Año = Cmb_Anio.SelectedValue;
            Año = Año.Substring(2, 2);
            //02_NOT_DYM_CódigoSF_CódigoPeriodo_Año
            Nombre_Archivo = "08_NOT_DYM_15_AP_" +Cmb_Mes.SelectedItem.Text.Substring(0, 1) + "" + Cmb_Mes.SelectedItem.Text.Substring(1, 2).ToLower() +"_" + Año + ".xls";
            //Nombre_Archivo = "01_CON_CódigoSF_Código" + Mes + "_" + Cmb_Anio.SelectedValue + ".xls";
            Ruta_Archivo = @Server.MapPath("../../Reporte/" + Nombre_Archivo);

            //Consultar el campo oculto
            Campo_Oculto_CONAC = Consulta_Campo_Oculto_CONAC();

            #region Inicial
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//  1 indice.
            Hoja_Esf.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  4 Tipo.
            Celda = Renglon_Esf.Cells.Add("NOTAS A LOS ESTADOS FINANCIEROS");
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado";
            Renglon_Esf = Hoja_Esf.Table.Rows.Add();
            //Encabezado_Izquierda
            Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Notas", DataType.String, "Encabezado"));
            Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Descripción", DataType.String, "Encabezado"));
            Renglon_Esf = Hoja_Esf.Table.Rows.Add();
            Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Encabezado"));
            Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Contenido_Nombre_Punteado"));
            if (Dt_Inicial.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Inicial.Rows)
                {
                    if (Registro["Descripción"].ToString().Contains("II. DE MEMORIA (DE ORDEN): ") == true)
                    {
                        Renglon_Esf = Hoja_Esf.Table.Rows.Add();
                        Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Encabezado"));
                        Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Contenido_Nombre_Punteado"));
                    }
                    Renglon_Esf = Hoja_Esf.Table.Rows.Add();
                    Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["notas"].ToString(), "Encabezado"));
                    if (Registro["Descripción"].ToString().Contains("II. DE MEMORIA (DE ORDEN): ") == true || Registro["Descripción"].ToString().Contains("I. DE DESGLOSE:") == true)
                    {
                        Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["Descripción"].ToString(), "Contenido_Nombre_Punteado_Negritas"));
                        
                        //Verificar si es el renglon del desglose
                        if (Registro["Descripción"].ToString().Contains("I. DE DESGLOSE:") == true)
                        {
                            Renglon_Esf = Hoja_Esf.Table.Rows.Add();
                            Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Encabezado"));
                            Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INFORMACION CONTABLE", DataType.String, "Contenido_Nombre_Punteado_Negritas"));
                        }
                    }
                    else
                    {
                        //Verificar si es vacio
                        if (Registro["Descripción"] == null || String.IsNullOrEmpty(Registro["Descripción"].ToString().Trim()) == true)
                        {
                            Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nombre_Punteado"));
                            Renglon_Esf = Hoja_Esf.Table.Rows.Add();
                            Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Encabezado"));
                            Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Inferior_Vacio"));
                        }
                        else
                        {
                            Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["Descripción"].ToString(), "Contenido_Nombre_Punteado"));
                        }
                        
                    }
                    //Renglon_Esf.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["Descripción"].ToString(), "Encabezado_Inicial"));
                }
            }
            #endregion
            #region Nota Esf_01

            // Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  1 indice.
            Hoja_Esf_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  3 Monto.
            Hoja_Esf_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 Tipo.
            Hoja_Esf_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 Tipo.
            Hoja_Esf_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 Tipo.
            //  se llena el encabezado principal
            Celda = Renglon_Esf_01.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_01.Cells.Add();
            Renglon_Esf_01.Cells.Add();
            Renglon_Esf_01.Cells.Add();
            Renglon_Esf_01.Cells.Add();
            Renglon_Esf_01.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Celda = Renglon_Esf_01.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            //Encabezado_Izquierda
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Celda = Renglon_Esf_01.Cells.Add("1114    INVERSIONES TEMPORALES (HASTA 3 MESES)");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:   ESF-01", "Encabezado"));

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TIPO", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO PARCIAL", "Encabezado"));

            if (Dt_Esf_01.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_01.Rows)
                {
                    Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" " + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" " + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Registro["MONTO"]);
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" " + Registro["TIPO"].ToString(), "Contenido_Nota"));
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" " + Registro["TIPO"].ToString(), "Contenido_Nota"));
                }
            }
            else
                Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Contenido_Indice"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("3 MESES ", "Contenido_Nombre_Negritas"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Negritas"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Contenido_Nota"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Contenido_Nota"));

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Totales_Verde"));

            //  para la cuenta 1115
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Celda = Renglon_Esf_01.Cells.Add("1115    FONDOS C/AFECTACION ESPECIFICA");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:   ESF-01", "Encabezado"));

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TIPO", "Encabezado"));

            Suma_Nota = 0.0;

            if (Dt_Esf_012.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_012.Rows)
                {
                    Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Registro["MONTO"]);
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["TIPO"].ToString(), "Contenido_Nota"));
                }
            }
            else
                Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Totales_Verde"));


            //  para la cuenta 1121
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Celda = Renglon_Esf_01.Cells.Add("1121    INVERSIONES FINANCIERAS DE CORTO PLAZO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:   ESF-01", "Encabezado"));

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TIPO", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO PARCIAL", "Encabezado"));
            Suma_Nota = 0.0;

            if (Dt_Esf_013.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_013.Rows)
                {
                    Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Registro["MONTO"]);
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["TIPO"].ToString(), "Contenido_Nota"));
                    Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Contenido_Nota"));
                }
            }
            else
                Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            //  para la cuenta 1211
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Celda = Renglon_Esf_01.Cells.Add("1211    INVERSIONES A LARGO PLAZO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:   ESF-01", "Encabezado"));

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TIPO", "Encabezado"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO PARCIAL", "Encabezado"));
            Suma_Nota = 0.0;

            Renglon_Esf_01 = Hoja_Esf_01.Table.Rows.Add();
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            #endregion
            #region Nota Esf_02
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  1 indice.
            Hoja_Esf_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 2010.
            Hoja_Esf_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  5 2011.

            //  se llena el encabezado principal
            Celda = Renglon_Esf_02.Cells.Add("DE DESGLOSE");

            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_02.Cells.Add();
            Renglon_Esf_02.Cells.Add();
            Renglon_Esf_02.Cells.Add();
            Renglon_Esf_02.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            Celda = Renglon_Esf_02.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            Celda = Renglon_Esf_02.Cells.Add("1122    CUENTAS POR COBRAR A CORTO PLAZO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Celda = Renglon_Esf_02.Cells.Add("NOTA:   ESF-02");
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado";
            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();

            Año = String.Format("{0: yyyy}", DateTime.Today);
            Año_Anterior = Convert.ToInt32(Año) - 1;
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Año_Anterior, "Encabezado"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Convert.ToInt32(Año_Anterior - 1), "Encabezado"));

            Suma_Nota = 0.0;
            Suma_Nota_2010 = 0.0;
            Suma_Nota_2011 = 0.0;

            if (Dt_Esf_02.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_02.Rows)
                {
                    //if (Convert.ToDouble(Registro["MONTO"].ToString()) != 0)
                    //{
                    Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());
                    Suma_Nota_2010 += Convert.ToDouble(Registro[Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString())-2)].ToString());
                    Suma_Nota_2011 += Convert.ToDouble(Registro[Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1)].ToString());

                    Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Registro["MONTO"]);
                    Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Registro[Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1)]);
                    Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Registro[Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString())-2)]);
                    Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    //}
                }
            }
            else
                Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();

            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2011);
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2010);
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));


            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            Celda = Renglon_Esf_02.Cells.Add("1124    INGRESOS POR RECUPERAR A CORTO PLAZO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Celda = Renglon_Esf_02.Cells.Add("NOTA:   ESF-02");
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado";

            Año = String.Format("{0: yyyy}", DateTime.Today);
            Año_Anterior = Convert.ToInt32(Año) - 1;
            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();

            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Año_Anterior, "Encabezado"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Convert.ToInt32(Año_Anterior - 1), "Encabezado"));
            Suma_Nota = 0.0;
            Suma_Nota_2010 = 0.0;
            Suma_Nota_2011 = 0.0;

            if (Dt_Esf_021.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_021.Rows)
                {
                    Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());
                    Suma_Nota_2010 += Convert.ToDouble(Registro[Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 2)].ToString());
                    Suma_Nota_2011 += Convert.ToDouble(Registro[Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1)].ToString());

                    Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Registro["MONTO"]);
                    Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Registro[Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1)]);
                    Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Registro[Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString())-2)]);
                    Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                }
            }
            else
                Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();

            Renglon_Esf_02 = Hoja_Esf_02.Table.Rows.Add();
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2011);
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2010);
            Renglon_Esf_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            
            #endregion
            #region Nota Esf_03
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  1 indice.
            Hoja_Esf_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 90 dias.
            Hoja_Esf_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  5 180 dias.
            Hoja_Esf_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  6 365 dias.
            Hoja_Esf_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  7 + 365 dias.
            Hoja_Esf_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  7 + 365 dias.
            Hoja_Esf_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  7 + 365 dias.

            //  se llena el encabezado principal
            Celda = Renglon_Esf_03.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_03.Cells.Add();
            Renglon_Esf_03.Cells.Add();
            Renglon_Esf_03.Cells.Add();
            Renglon_Esf_03.Cells.Add();
            Renglon_Esf_03.Cells.Add();
            Renglon_Esf_03.Cells.Add();
            Renglon_Esf_03.Cells.Add();
            Renglon_Esf_03.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Celda = Renglon_Esf_03.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Celda = Renglon_Esf_03.Cells.Add("1123    DEUDORES DIVERSOS POR COBRAR A CORTO PLAZO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:   ESF-03", "Encabezado"));

            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("IMPORTE", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("A 90 días", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("A 180 días", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("A 365 días", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("+ 365 días", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERÍSTICAS", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ESTATUS DEL ADEUDO", "Encabezado"));
            Suma_Nota = 0.0;
            Suma_Nota_90 = 0.0;
            Suma_Nota_180 = 0.0;
            Suma_Nota_2010 = 0.0;// 365 dias
            Suma_Nota_2011 = 0.0;// para mas de +365 dias
            Dt_Esf_03.DefaultView.RowFilter = "IMPORTE <>'0'";
            if (Dt_Esf_03.DefaultView.ToTable().Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_03.DefaultView.ToTable().Rows)
                {
                    Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["IMPORTE"].ToString());

                    if (!String.IsNullOrEmpty(Registro["IMPORTE_90"].ToString()))
                        Suma_Nota_90 += Convert.ToDouble(Registro["IMPORTE_90"].ToString());

                    if (!String.IsNullOrEmpty(Registro["IMPORTE_180"].ToString()))
                        Suma_Nota_180 += Convert.ToDouble(Registro["IMPORTE_180"].ToString());

                    if (!String.IsNullOrEmpty(Registro["IMPORTE_365"].ToString()))
                        Suma_Nota_2010 += Convert.ToDouble(Registro["IMPORTE_365"].ToString());
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Registro["IMPORTE"]);
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    if (!String.IsNullOrEmpty(Registro["IMPORTE_90"].ToString()))
                        Formato_Importe = String.Format("{0:n}", Registro["IMPORTE_90"]);
                    else
                        Formato_Importe = "0.0";
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    if (!String.IsNullOrEmpty(Registro["IMPORTE_180"].ToString()))
                        Formato_Importe = String.Format("{0:n}", Registro["IMPORTE_180"]);
                    else
                        Formato_Importe = "0.0";
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    if (!String.IsNullOrEmpty(Registro["IMPORTE_365"].ToString()))
                        Formato_Importe = String.Format("{0:n}", Registro["IMPORTE_365"]);
                    else
                        Formato_Importe = "0.0";
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                }
            }
            else
                Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();

            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_90);
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_180);
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2010);
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2011);
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Celda = Renglon_Esf_03.Cells.Add("1125    DEUDORES POR ANTICIPOS DE TESORERÍA A CORTO PLAZO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:   ESF-03", "Encabezado"));

            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("IMPORTE", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("A 90 días", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("A 180 días", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("A 365 días", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("+ 365 días", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERÍSTICAS", "Encabezado"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ESTATUS DEL ADEUDO", "Encabezado"));
            Suma_Nota = 0.0;
            Suma_Nota_90 = 0.0;
            Suma_Nota_180 = 0.0;
            Suma_Nota_2010 = 0.0;// 365 dias
            Suma_Nota_2011 = 0.0;// para mas de +365 dias

            if (Dt_Esf_031.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_031.Rows)
                {
                    Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["IMPORTE"].ToString());

                    if (!String.IsNullOrEmpty(Registro["IMPORTE_90"].ToString()))
                        Suma_Nota_90 += Convert.ToDouble(Registro["IMPORTE_90"].ToString());

                    if (!String.IsNullOrEmpty(Registro["IMPORTE_180"].ToString()))
                        Suma_Nota_180 += Convert.ToDouble(Registro["IMPORTE_180"].ToString());

                    if (!String.IsNullOrEmpty(Registro["IMPORTE_365"].ToString()))
                        Suma_Nota_2010 += Convert.ToDouble(Registro["IMPORTE_365"].ToString());

                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Registro["IMPORTE"]);
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));

                    if (!String.IsNullOrEmpty(Registro["IMPORTE_90"].ToString()))
                        Formato_Importe = String.Format("{0:n}", Registro["IMPORTE_90"]);
                    else
                        Formato_Importe = "0.0";
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));

                    if (!String.IsNullOrEmpty(Registro["IMPORTE_180"].ToString()))
                        Formato_Importe = String.Format("{0:n}", Registro["IMPORTE_180"]);
                    else
                        Formato_Importe = "0.0";
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));

                    if (!String.IsNullOrEmpty(Registro["IMPORTE_365"].ToString()))
                        Formato_Importe = String.Format("{0:n}", Registro["IMPORTE_365"]);
                    else
                        Formato_Importe = "0.0";
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));

                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));

                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                }
            }
            else
                Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();

            Renglon_Esf_03 = Hoja_Esf_03.Table.Rows.Add();
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_90);
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_180);
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2010);
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2011);
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            #endregion
            #region Nota Esf_05
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_05.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  1 indice.
            Hoja_Esf_05.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_05.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_05.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 Metodo

            //  se llena el encabezado principal
            Celda = Renglon_Esf_05.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_05.Cells.Add();
            Renglon_Esf_05.Cells.Add();
            Renglon_Esf_05.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Celda = Renglon_Esf_05.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Celda = Renglon_Esf_05.Cells.Add("1140    INVENTARIOS");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Encabezado_Inicial"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:    ESF-05", "Encabezado"));

            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MÉTODO", "Encabezado"));
            Suma_Nota = 0.0;

            if (Dt_Esf_05.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_05.Rows)
                {
                    Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());

                    Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["METODO"], "Contenido_Nota"));
                }
            }

            else
                Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();

            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            
            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Celda = Renglon_Esf_05.Cells.Add("1150    ALMACENES");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:    ESF-05", "Encabezado"));

            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MÉTODO", "Encabezado"));
            Suma_Nota = 0.0;

            if (Dt_Esf_051.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_051.Rows)
                {
                    Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());

                    Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["METODO"], "Contenido_Nota"));
                }
            }
            else
                Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();

            Renglon_Esf_05 = Hoja_Esf_05.Table.Rows.Add();
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_05.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            #endregion
            #region Nota Esf_06
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_06.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Esf_06.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_06.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_06.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 TIPO
            Hoja_Esf_06.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  4 TIPO
            Hoja_Esf_06.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  4 TIPO
            Hoja_Esf_06.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  4 TIPO

            //  se llena el encabezado principal
            Celda = Renglon_Esf_06.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_06.Cells.Add();
            Renglon_Esf_06.Cells.Add();
            Renglon_Esf_06.Cells.Add();
            Renglon_Esf_06.Cells.Add();
            Renglon_Esf_06.Cells.Add();
            Renglon_Esf_06.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_06 = Hoja_Esf_06.Table.Rows.Add();
            Celda = Renglon_Esf_06.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_06 = Hoja_Esf_06.Table.Rows.Add();
            Renglon_Esf_06 = Hoja_Esf_06.Table.Rows.Add();
            Renglon_Esf_06 = Hoja_Esf_06.Table.Rows.Add();
            Celda = Renglon_Esf_06.Cells.Add("1213    FIDEICOMISOS, MANDATOS Y CONTRATOS ANÁLOGOS");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:        ESF-06", "Encabezado"));

            Renglon_Esf_06 = Hoja_Esf_06.Table.Rows.Add();
            Renglon_Esf_06 = Hoja_Esf_06.Table.Rows.Add();
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TIPO", "Encabezado"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERÍSTICAS", "Encabezado"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DEL FIDEICOMISO", "Encabezado"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("OBJETO DEL FIDEICOMISO", "Encabezado"));
            Suma_Nota = 0.0;

            if (Dt_Esf_06.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_06.Rows)
                {
                    Renglon_Esf_06 = Hoja_Esf_06.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());

                    Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["TIPO"], "Contenido_Nota"));

                    Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Contenido_Nota"));
                    Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Contenido_Nota"));
                    Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Contenido_Nota"));
                }
            }
            else
                Renglon_Esf_06 = Hoja_Esf_06.Table.Rows.Add();


            Renglon_Esf_06 = Hoja_Esf_06.Table.Rows.Add();
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_06.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            #endregion
            #region Nota Esf_07
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_07.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Esf_07.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_07.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_07.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 TIPO
            Hoja_Esf_07.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  4 TIPO

            //  se llena el encabezado principal
            Celda = Renglon_Esf_07.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_07.Cells.Add();
            Renglon_Esf_07.Cells.Add();
            Renglon_Esf_07.Cells.Add();
            Renglon_Esf_07.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_07 = Hoja_Esf_07.Table.Rows.Add();
            Celda = Renglon_Esf_07.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_07 = Hoja_Esf_07.Table.Rows.Add();
            Renglon_Esf_07 = Hoja_Esf_07.Table.Rows.Add();
            Renglon_Esf_07 = Hoja_Esf_07.Table.Rows.Add();
            Celda = Renglon_Esf_07.Cells.Add("1214    PARTICIPACIONES Y APORTACIONES DE CAPITAL");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:        ESF-07", "Encabezado"));

            Renglon_Esf_07 = Hoja_Esf_07.Table.Rows.Add();
            Renglon_Esf_07 = Hoja_Esf_07.Table.Rows.Add();
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TIPO", "Encabezado"));
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("EMPRESA/OPDes", "Encabezado"));
            Suma_Nota = 0.0;

            if (Dt_Esf_07.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_07.Rows)
                {
                    Renglon_Esf_07 = Hoja_Esf_07.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());

                    Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["TIPO"], "Contenido_Nota"));
                    Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Contenido_Nota"));
                }
            }
            else
                Renglon_Esf_07 = Hoja_Esf_07.Table.Rows.Add();
            Renglon_Esf_07 = Hoja_Esf_07.Table.Rows.Add();
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Totales_Verde"));
            Renglon_Esf_07.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" ", "Totales_Verde"));
            #endregion
            #region Nota Esf_08
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_08.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Esf_08.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_08.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_08.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_08.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_08.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.

            //  se llena el encabezado principal
            Celda = Renglon_Esf_08.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_08.Cells.Add();
            Renglon_Esf_08.Cells.Add();
            Renglon_Esf_08.Cells.Add();
            Renglon_Esf_08.Cells.Add();
            Renglon_Esf_08.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Celda = Renglon_Esf_08.Cells.Add("I. INFORMACIÓN CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Celda = Renglon_Esf_08.Cells.Add("1230    BIENES INMUEBLES, INFRAESTRUCTURA Y CONSTRUCCIONES EN PROCESO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:       ESF-08", "Encabezado"));

            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CRITERIO", "Encabezado"));
            Suma_Nota = 0.0;
            Suma_Nota_2 = 0.0;
            Suma_Final = 0.0;

            if (Dt_Esf_08.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_08.Rows)
                {
                    Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["SALDO INICIAL"].ToString());
                    Suma_Nota_2 += Convert.ToDouble(Registro["SALDO FINAL"].ToString());
                    Suma_Final += Convert.ToDouble(Registro["FLUJO"].ToString());
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["FLUJO"].ToString()));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                }
            }
            else
                Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();

            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Suma_Nota));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Suma_Nota_2));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Suma_Final));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));


            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Celda = Renglon_Esf_08.Cells.Add("1240    BIENES MUEBLES");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:       ESF-08", "Encabezado"));

            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CRITERIO", "Encabezado"));
            Suma_Nota = 0.0;
            Suma_Nota_2 = 0.0;
            Suma_Final = 0.0;
            if (Dt_Esf_081.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_081.Rows)
                {
                    Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["SALDO INICIAL"].ToString());
                    Suma_Nota_2 += Convert.ToDouble(Registro["SALDO FINAL"].ToString());
                    Suma_Final += Convert.ToDouble(Registro["FLUJO"].ToString());

                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["FLUJO"].ToString()));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                }
            }
            else
                Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();

            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Suma_Nota));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Suma_Nota_2));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Suma_Final));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Celda = Renglon_Esf_08.Cells.Add("1260    DEPRECIACION Y DETERIORO ACUMULADA DE BIENES");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:       ESF-08", "Encabezado"));

            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CRITERIO", "Encabezado"));
            Suma_Nota = 0.0;
            Suma_Nota_2 = 0.0;
            Suma_Final = 0.0;
            if (Dt_Esf_08_126.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_08_126.Rows)
                {
                    Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["SALDO INICIAL"].ToString());
                    Suma_Nota_2 += Convert.ToDouble(Registro["SALDO FINAL"].ToString());
                    Suma_Final += Convert.ToDouble(Registro["FLUJO"].ToString());

                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["FLUJO"].ToString()));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                }
            }
            else
                Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();

            Renglon_Esf_08 = Hoja_Esf_08.Table.Rows.Add();
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Suma_Nota));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Suma_Nota_2));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Suma_Final));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_08.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            #endregion
            #region Nota Esf_09
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_09.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Esf_09.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_09.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_09.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_09.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_09.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.

            //  se llena el encabezado principal
            Celda = Renglon_Esf_09.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_09.Cells.Add();
            Renglon_Esf_09.Cells.Add();
            Renglon_Esf_09.Cells.Add();
            Renglon_Esf_09.Cells.Add();
            Renglon_Esf_09.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Celda = Renglon_Esf_09.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Celda = Renglon_Esf_09.Cells.Add("1250    ACTIVOS INTANGIBLES");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:        ESF-09", "Encabezado"));

            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CRITERIO", "Encabezado"));
            Suma_Nota_2 = 0.0;
            Suma_Nota = 0.0;
            if (Dt_Esf_09.Rows.Count > 0)
            {
                Suma_Nota = 0;
                foreach (DataRow Registro in Dt_Esf_09.Rows)
                {
                    Formato_Importe=String.Format("{0:n}", Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                    Formato_Importe_2 = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()));
                    if (Formato_Importe != "0.00" || Formato_Importe_2 != "0.00")
                    {
                        Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
                        //  para la suma de los saldos
                        Suma_Nota += Convert.ToDouble(Registro["SALDO INICIAL"].ToString());
                        Suma_Nota_2 += Convert.ToDouble(Registro["SALDO FINAL"].ToString());

                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                        Formato_Importe_2 = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe_2, DataType.Number, "Totales"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()) - Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    }
                }
            }
            else
                Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();


            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2);
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2-Suma_Nota);
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));


            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Celda = Renglon_Esf_09.Cells.Add("1265    AMORTIZACION ACUMULADA DE BIENES");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:        ESF-09", "Encabezado"));

            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CRITERIO", "Encabezado"));
            Suma_Nota = 0.0;
            Suma_Nota_2 = 0.0;
            if (Dt_Esf_1265.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_1265.Rows)
                {
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                    Formato_Importe_2 = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()));
                    if (Formato_Importe != "0.00" || Formato_Importe_2 != "0.00")
                    {
                        Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
                        //  para la suma de los saldos
                        Suma_Nota += Convert.ToDouble(Registro["SALDO INICIAL"].ToString());
                        Suma_Nota_2 += Convert.ToDouble(Registro["SALDO FINAL"].ToString());

                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                        Formato_Importe_2 = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe_2, DataType.Number, "Totales"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()) - Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    }
                }
            }
            else
                Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();

            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2);
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2 - Suma_Nota);
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Celda = Renglon_Esf_09.Cells.Add("1270    ACTIVOS DIFERIDOS");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:        ESF-09", "Encabezado"));

            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CRITERIO", "Encabezado"));
            Suma_Nota = 0.0;
            Suma_Nota_2 = 0.0;
            if (Dt_Esf_091.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_091.Rows)
                {
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                    Formato_Importe_2 = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()));
                    if (Formato_Importe != "0.00" || Formato_Importe_2 != "0.00")
                    {
                        Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
                        Suma_Nota += Convert.ToDouble(Registro["SALDO INICIAL"].ToString());
                        Suma_Nota_2 += Convert.ToDouble(Registro["SALDO FINAL"].ToString());

                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO FINAL"].ToString()) - Convert.ToDouble(Registro["SALDO INICIAL"].ToString()));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                        Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    }
                }
            }
            else
                Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();

            Renglon_Esf_09 = Hoja_Esf_09.Table.Rows.Add();
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2);
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota_2 - Suma_Nota);
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_09.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            #endregion
            #region Nota Esf_10
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_10.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Esf_10.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_10.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_10.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 CARACTERÍSTICAS

            //  se llena el encabezado principal
            Celda = Renglon_Esf_10.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_10.Cells.Add();
            Renglon_Esf_10.Cells.Add();
            Renglon_Esf_10.Cells.Add();
            Renglon_Esf_10.Cells.Add();
            Renglon_Esf_10.Cells.Add();
            Renglon_Esf_10.Cells.Add();
            Renglon_Esf_10.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_10 = Hoja_Esf_10.Table.Rows.Add();
            Celda = Renglon_Esf_10.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_10 = Hoja_Esf_10.Table.Rows.Add();
            Renglon_Esf_10 = Hoja_Esf_10.Table.Rows.Add();
            Renglon_Esf_10.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_10.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_10.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_10.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_10.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_10.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Celda = Renglon_Esf_10.Cells.Add("NOTA:        ESF-10");
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Esf_10 = Hoja_Esf_10.Table.Rows.Add();
            Celda = Renglon_Esf_10.Cells.Add("1280        ESTIMACIONES Y DETERIOROS");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_10.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_10.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));
            Renglon_Esf_10.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));
            Renglon_Esf_10 = Hoja_Esf_10.Table.Rows.Add();
            Renglon_Esf_10 = Hoja_Esf_10.Table.Rows.Add();
            #endregion
            #region Nota Esf_11
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_11.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Esf_11.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_11.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_11.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 CARACTERÍSTICAS

            //  se llena el encabezado principal
            Celda = Renglon_Esf_11.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_11.Cells.Add();
            Renglon_Esf_11.Cells.Add();
            Renglon_Esf_11.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_11 = Hoja_Esf_11.Table.Rows.Add();
            Celda = Renglon_Esf_11.Cells.Add("I. INFORMACIÓN CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_11 = Hoja_Esf_11.Table.Rows.Add();
            Renglon_Esf_11 = Hoja_Esf_11.Table.Rows.Add();
            Renglon_Esf_11 = Hoja_Esf_11.Table.Rows.Add();
            Celda = Renglon_Esf_11.Cells.Add("1290    OTROS ACTIVOS NO CIRCULANTES");
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado";
            Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:   ESF-11", "Encabezado"));

            Renglon_Esf_11 = Hoja_Esf_11.Table.Rows.Add();
            Renglon_Esf_11 = Hoja_Esf_11.Table.Rows.Add();
            Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERÍSTICAS", "Encabezado"));
            Suma_Nota = 0.0;

            if (Dt_Esf_11.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_11.Rows)
                {
                    Renglon_Esf_11 = Hoja_Esf_11.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());
                    Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nota"));
                }
            }
            else
                Renglon_Esf_11 = Hoja_Esf_11.Table.Rows.Add();


            Renglon_Esf_11 = Hoja_Esf_11.Table.Rows.Add();
            Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_11.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            #endregion
            #region Nota Esf_12
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_12.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Esf_12.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_12.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_12.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_12.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_12.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_12.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_12.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.

            //variables auxiliares
            bool Primer_Encabezado = false; //variable que indica si es el primer encabezado
            double Total_211 = 0; //variable para el total de las cuentas 211
            double Total_212 = 0; //variable para el total de las cuentas 212

            //  se llena el encabezado principal
            //Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();

            Celda = Renglon_Esf_12.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_12.Cells.Add();
            Renglon_Esf_12.Cells.Add();
            Renglon_Esf_12.Cells.Add();
            Renglon_Esf_12.Cells.Add();
            Renglon_Esf_12.Cells.Add();
            Renglon_Esf_12.Cells.Add();
            Renglon_Esf_12.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
            Celda = Renglon_Esf_12.Cells.Add("I. INFORMACIÓN CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
            Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
            Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
            Celda = Renglon_Esf_12.Cells.Add("2110  Y  2120    CUENTAS Y DOCUMENTOS POR PAGAR");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:         ESF-12", "Encabezado"));
            Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
            Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("A 90 días", "Encabezado"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("A 180 días", "Encabezado"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("A 356 días", "Encabezado"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("+ 365 días", "Encabezado"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERÍSTICAS", "Encabezado"));
            Suma_Nota = 0.0;
            Dt_Esf_12.DefaultView.RowFilter = "IMPORTE <>'0'";
            if (Dt_Esf_12.DefaultView.ToTable().Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_12.DefaultView.ToTable().Rows)
                {
                    //if (Convert.ToDouble(Registro["MONTO"].ToString()) != 0)
                    //{
                    Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
                    //  para la suma de los saldos
                    if (Registro["CUENTA"].ToString().Trim() != "211000000" && Registro["CUENTA"].ToString().Trim() != "211000000")
                    {
                        Suma_Nota += Convert.ToDouble(Registro["IMPORTE"].ToString());
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["IMPORTE"].ToString()));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    }
                    else
                    {
                        //verificar si es el primer encabezado
                        if (Primer_Encabezado == true)
                        {
                            Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
                            Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
                        }
                        else
                        {
                            Primer_Encabezado = true;
                        }

                        //Verificar si es la 211
                        if (Registro["CUENTA"].ToString().Trim() != "211000000")
                        {
                            Total_211 = Convert.ToDouble(Registro["IMPORTE"].ToString());
                        }
                        else
                        {
                            Total_212 = Convert.ToDouble(Registro["IMPORTE"].ToString());
                        }
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Nombre_Negritas"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre_Negritas"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["IMPORTE"].ToString()));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Negritas"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                        Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                    }

                }
            }
            else
                Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
            ////Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
            ////Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            ////Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTAS X PAGAR CP", "Totales_Verde"));
            ////Formato_Importe = String.Format("{0:n}", Suma_Nota);
            ////Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales_Verde"));

            //Agregar los totales
            if (Total_211 > 0)
            {
                Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTAS X PAGAR CP", "Contenido_Nombre_Negritas"));
                Formato_Importe = String.Format("{0:n}", Total_211);
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            }

            if (Total_212 > 0)
            {
                Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DOCUMENTOS X PAGAR CP", "Contenido_Nombre_Negritas"));
                Formato_Importe = String.Format("{0:n}", Total_212);
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
                Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));
            }

            Renglon_Esf_12 = Hoja_Esf_12.Table.Rows.Add();
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_12.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));


            #endregion
            #region Nota Esf_13
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_13.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Esf_13.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_13.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_13.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_13.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.

            //  se llena el encabezado principal
            // Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();

            Celda = Renglon_Esf_13.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Celda = Renglon_Esf_13.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_13.Cells.Add();
            Renglon_Esf_13.Cells.Add();
            Renglon_Esf_13.Cells.Add();
            Renglon_Esf_13.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Celda = Renglon_Esf_13.Cells.Add("2159    OTROS PASIVOS DIFERIDOS A CORTO PLAZO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:         ESF-13", "Encabezado"));

            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NATURALEZA", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERÍSTICAS", "Encabezado"));
            Suma_Nota = 0.0;

            if (Dt_Esf_13.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_13.Rows)
                {
                    //if (Convert.ToDouble(Registro["MONTO"].ToString()) != 0)
                    //{
                    Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());

                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    //}
                }
            }
            else
                Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();


            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));


            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Celda = Renglon_Esf_13.Cells.Add("2160    FONDOS Y BIENES DE TERCEROS EN GARANTÍA Y/O ADMINISTRACIÓN A CORTO PLAZO");
            //Celda.MergeAcross = 2; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:         ESF-13", "Encabezado"));

            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NATURALEZA", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERÍSTICAS", "Encabezado"));
            Suma_Nota = 0.0;

            if (Dt_ESF_13_1.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_ESF_13_1.Rows)
                {
                    //if (Convert.ToDouble(Registro["MONTO"].ToString()) != 0)
                    //{
                    Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());

                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    //}
                }
            }
            else
                Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();

            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));


            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Celda = Renglon_Esf_13.Cells.Add("2240    PASIVO DIFERIDO A LARGO PLAZO");
            //Celda.MergeAcross = 2; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:         ESF-13", "Encabezado"));

            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NATURALEZA", "Encabezado"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERÍSTICAS", "Encabezado"));
            Suma_Nota = 0.0;

            if (Dt_ESF_13_2.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_ESF_13_1.Rows)
                {
                    Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());

                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                }
            }

            Renglon_Esf_13 = Hoja_Esf_13.Table.Rows.Add();
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_13.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            #endregion
            #region Nota Esf_14
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_14.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Esf_14.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Esf_14.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_14.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Esf_14.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            //  se llena el encabezado principal

            Celda = Renglon_Esf_14.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_14.Cells.Add();
            Renglon_Esf_14.Cells.Add();
            Renglon_Esf_14.Cells.Add();
            Renglon_Esf_14.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_14 = Hoja_Esf_14.Table.Rows.Add();
            Celda = Renglon_Esf_14.Cells.Add("I. INFORMACIÓN CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_14 = Hoja_Esf_14.Table.Rows.Add();
            Renglon_Esf_14 = Hoja_Esf_14.Table.Rows.Add();
            Renglon_Esf_14 = Hoja_Esf_14.Table.Rows.Add();
            Celda = Renglon_Esf_14.Cells.Add("2199    OTROS PASIVOS CIRCULANTES");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:     ESF-14", "Encabezado"));

            Renglon_Esf_14 = Hoja_Esf_14.Table.Rows.Add();
            Renglon_Esf_14 = Hoja_Esf_14.Table.Rows.Add();
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NATURALEZA", "Encabezado"));
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERÍSTICAS", "Encabezado"));
            Suma_Nota = 0.0;

            if (Dt_Esf_14.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Esf_14.Rows)
                {
                    Renglon_Esf_14 = Hoja_Esf_14.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());
                    Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                    Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                }
            }
            else
                Renglon_Esf_14 = Hoja_Esf_14.Table.Rows.Add();

            Suma_Nota = 0.0;
            Renglon_Esf_14 = Hoja_Esf_14.Table.Rows.Add();
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_14.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            #endregion
            #region Nota Esf_15
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   2 Destino del Crédito
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   3 Acreedor
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   4 # contrato de crédito
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   5 Clase del título
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   6 En UDIS
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   7 En Pesos
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   7 En Pesos
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   8 Saldo en pesos
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   9 Tasa de Interés
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   10 En UDIS
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   11 En Pesos
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   12 Intereses pagados
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   12 Intereses pagados
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   13 Capital Pagado
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   14 Número de pagos
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   14 Número de pagos
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   15 Fecha de Contratación
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   16 Fecha de Vencimiento
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   17 Registro Estatal
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   18 Período de Gracia
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   19 Aval
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   20 Garantía
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   20 Garantía
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   21 No. de Decreto del Congreso / Autorización
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   22 Fecha del Acuerdo de cada ente
            Hoja_Esf_15.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   23 Observaciones

            Celda = Renglon_Esf_15.Cells.Add("NOTAS A LOS ESTADOS FINANCIEROS DE " + Cmb_Mes.SelectedItem.Text.ToUpper() + " DE " + Cmb_Anio.SelectedItem.Text);
            Celda.MergeAcross = 25; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial_Centrado";

            Renglon_Esf_15.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();

            Celda = Renglon_Esf_15.Cells.Add("DE GESTION ADMINISTRATIVA");
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Celda = Renglon_Esf_15.Cells.Add("2130  Y  2230   DEUDA PUBLICA");
            Celda.MergeAcross = 4; // Merge 2 cells together
            Celda.StyleID = "Encabezado";
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Celda = Renglon_Esf_15.Cells.Add("NOTA: ESF-15");
            Celda.MergeAcross = 4;
            Celda.StyleID = "Encabezado";
            //Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA: ESF-15", "Encabezado"));

            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();


            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));
            Celda = Renglon_Esf_15.Cells.Add("Estado Analítico de la Deuda y Otros Pasivos");
            Celda.MergeAcross = 25; // Merge 23 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//1
            //Celda = Renglon_Esf_15.Cells.Add("Índice");
            //Celda.MergeDown = 1;
            //Celda.StyleID = "Encabezado";
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//2
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//3
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//4
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//5

            Celda = Renglon_Esf_15.Cells.Add("Financiamiento Contratado");// 6 y 7
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado";
            Celda = Renglon_Esf_15.Cells.Add("Financiamiento Dispuesto");// 6 y 7
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado";
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//8
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//9

            Celda = Renglon_Esf_15.Cells.Add("Capital amortizado");// 11 y 12
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado";

            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//13
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//14
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//15
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//16
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//17
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//18
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//19
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//20
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//21
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//22
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//23
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//20
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//21
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));//22


            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Indice", "Encabezado"));//1
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Destino del Crédito", "Encabezado"));//2
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Acreedor", "Encabezado"));//3
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Núm. contrato de crédito", "Encabezado"));//4
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Clase del Título", "Encabezado"));//5
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("En UDIS", "Encabezado"));//6
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("En Pesos", "Encabezado"));//7
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("En Pesos", "Encabezado"));//7
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Saldo en pesos", "Encabezado"));//8
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Tasa de Interés", "Encabezado"));//9
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("En UDIS", "Encabezado"));//10
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("En Pesos", "Encabezado"));//11
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Intereses Pagados Acumulado", "Encabezado"));//12
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Intereses Pagados en el Ejercicio", "Encabezado"));//12
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Capital Pagado", "Encabezado"));//13
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Núm. total de pagos", "Encabezado"));//14
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Núm. de pagos del Periodo", "Encabezado"));//14
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Fecha de Contratación", "Encabezado"));//15
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Fecha de Vencimiento", "Encabezado"));//16
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Registro Estatal", "Encabezado"));//17
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Período de Gracia", "Encabezado"));//18
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Aval", "Encabezado"));//19
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Garantía", "Encabezado"));//20
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Fuente de Financiamiento", "Encabezado"));//20
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("No. de Decreto del Congreso / Autorización", "Encabezado"));//21
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Fecha del Acuerdo de cada ente", "Encabezado"));//22
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Observaciones", "Encabezado"));//23

            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();

            Renglon_Esf_15 = Hoja_Esf_15.Table.Rows.Add();
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL CREDITOS ", "Totales_Verde"));

            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Esf_15.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));


            #endregion
            #region Nota Era_01
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Era_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Era_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Era_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Era_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.

            Celda = Renglon_Era_01.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Era_01.Cells.Add();
            Renglon_Era_01.Cells.Add();
            Renglon_Era_01.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
            Celda = Renglon_Era_01.Cells.Add("I. INFORMACIÓN CONTABLE/PRESUPUESTAL");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
            Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
            Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
            Celda = Renglon_Era_01.Cells.Add("4100  Y  4200    INGRESOS");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:   ERA-01", "Encabezado"));

            Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
            Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERÍSTICAS", "Encabezado"));
            Suma_Nota = 0.0;
            Dt_Era_01.DefaultView.RowFilter = "MONTO<>'0.00'";
            if (Dt_Era_01.DefaultView.ToTable().Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Era_01.DefaultView.ToTable().Rows)
                {
                    if (Registro["CUENTA"].ToString() == "" || Registro["CUENTA"].ToString() == "z")
                    {
                        Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
                        Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

                        //verificar si es el de total
                        if (Registro["NOMBRE"].ToString().ToUpper().Trim() == "TOTAL")
                        {
                            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL", "Totales_Verde_Texto"));
                        }
                        else
                        {
                            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Totales_Verde"));
                        }
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                        Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
                        Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
                    }
                    else
                    {
                        if (Registro["CUENTA"].ToString().Length == 9)
                        {
                            Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
                            //  para la suma de los saldos
                            Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());

                            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Nombre_Negritas"));
                            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre_Negritas"));
                            Formato_Importe = String.Format("{0:n}", Registro["MONTO"]);
                            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Negritas"));
                            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Negritas"));

                        }
                        else
                        {
                            Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
                            //  para la suma de los saldos
                            Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());

                            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                            Formato_Importe = String.Format("{0:n}", Registro["MONTO"]);
                            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                            Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                        }
                    }
                }
            }
            else
                Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();

            //Renglon_Era_01 = Hoja_Era_01.Table.Rows.Add();
            //Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            //Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTALesssss", "Totales_Verde"));
            //Formato_Importe = String.Format("{0:n}", Suma_Nota);
            //Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Formato_Importe, "Totales_Verde"));
            //Renglon_Era_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));

            #endregion
            #region Nota Era_02
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Era_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Era_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Era_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Era_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Naturaleza.
            Hoja_Era_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Naturaleza.

            Celda = Renglon_Era_02.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Era_02.Cells.Add();
            Renglon_Era_02.Cells.Add();
            Renglon_Era_02.Cells.Add();
            Renglon_Era_02.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Era_02 = Hoja_Era_02.Table.Rows.Add();
            Celda = Renglon_Era_02.Cells.Add("I. INFORMACIÓN CONTABLE/PRESUPUESTAL");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Era_02 = Hoja_Era_02.Table.Rows.Add();
            Renglon_Era_02 = Hoja_Era_02.Table.Rows.Add();
            Renglon_Era_02 = Hoja_Era_02.Table.Rows.Add();
            Celda = Renglon_Era_02.Cells.Add("4300    OTROS INGRESOS Y BENEFICIOS");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:   ERA-02", "Encabezado"));

            Renglon_Era_02 = Hoja_Era_02.Table.Rows.Add();
            Renglon_Era_02 = Hoja_Era_02.Table.Rows.Add();
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NATURALEZA", "Encabezado"));
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARACTERISTICAS", "Encabezado"));
            Suma_Nota = 0.0;
            Dt_Era_02.DefaultView.RowFilter = "MONTO <>'0.00'";
            if (Dt_Era_02.DefaultView.ToTable().Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Era_02.DefaultView.ToTable().Rows)
                {
                    //if (Convert.ToDouble(Registro["MONTO"].ToString()) != 0)
                    //{
                    Renglon_Era_02 = Hoja_Era_02.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());

                    Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NATURALEZA"].ToString(), "Contenido_Nota"));
                    Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nota"));
                }
            }
            else
                Renglon_Era_02 = Hoja_Era_02.Table.Rows.Add();

            Renglon_Era_02 = Hoja_Era_02.Table.Rows.Add();
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Era_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            #endregion
            #region Nota Era_03
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Era_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Era_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Era_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Monto.
            Hoja_Era_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 gastos.
            Hoja_Era_03.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 explicacion.

            Celda = Renglon_Era_03.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Era_03.Cells.Add();
            Renglon_Era_03.Cells.Add();
            Renglon_Era_03.Cells.Add();
            Renglon_Era_03.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Era_03 = Hoja_Era_03.Table.Rows.Add();
            Celda = Renglon_Era_03.Cells.Add("I. INFORMACIÓN CONTABLE/PRESUPUESTAL");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Era_03 = Hoja_Era_03.Table.Rows.Add();
            Renglon_Era_03 = Hoja_Era_03.Table.Rows.Add();
            Renglon_Era_03 = Hoja_Era_03.Table.Rows.Add();
            Celda = Renglon_Era_03.Cells.Add("5000    GASTOS Y OTRAS PÉRDIDAS");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:    ERA-03", "Encabezado"));

            Renglon_Era_03 = Hoja_Era_03.Table.Rows.Add();
            Renglon_Era_03 = Hoja_Era_03.Table.Rows.Add();
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MONTO", "Encabezado"));
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("% GASTO", "Encabezado"));
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("EXPLICACIÓN", "Encabezado"));
            Suma_Nota = 0.0;
            Suma_Modificado = 0.0;
            Dt_Era_03.DefaultView.RowFilter = "MONTO <>'0.00'";
            if (Dt_Era_03.DefaultView.ToTable().Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Era_03.DefaultView.ToTable().Rows)
                {
                    Renglon_Era_03 = Hoja_Era_03.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Nota += Convert.ToDouble(Registro["MONTO"].ToString());
                    Suma_Modificado += Convert.ToDouble(Registro["GASTO"].ToString());
                    Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MONTO"].ToString()));
                    Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["GASTO"].ToString()));
                    Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales"));
                }
            }
            else
                Renglon_Era_03 = Hoja_Era_03.Table.Rows.Add();

            Renglon_Era_03 = Hoja_Era_03.Table.Rows.Add();
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Nota);
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Modificado);
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Era_03.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            #endregion
            #region Nota Vhp_01
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Vhp_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Vhp_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Vhp_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 saldo inicial.
            Hoja_Vhp_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 saldo final
            Hoja_Vhp_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  5 Modificado.
            Hoja_Vhp_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  6 Tipo.
            Hoja_Vhp_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  7 Naturaleza.

            Celda = Renglon_Vhp_01.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Vhp_01.Cells.Add();
            Renglon_Vhp_01.Cells.Add();
            Renglon_Vhp_01.Cells.Add();
            Renglon_Vhp_01.Cells.Add();
            Renglon_Vhp_01.Cells.Add();
            Renglon_Vhp_01.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Vhp_01 = Hoja_Vhp_01.Table.Rows.Add();
            Celda = Renglon_Vhp_01.Cells.Add("I. INFORMACIÓN CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Vhp_01 = Hoja_Vhp_01.Table.Rows.Add();
            Renglon_Vhp_01 = Hoja_Vhp_01.Table.Rows.Add();
            Renglon_Vhp_01 = Hoja_Vhp_01.Table.Rows.Add();
            Celda = Renglon_Vhp_01.Cells.Add("3100    HACIENDA PÚBLICA/PATRIMONIO CONTRIBUIDO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:    VHP-01", "Encabezado"));

            Renglon_Vhp_01 = Hoja_Vhp_01.Table.Rows.Add();
            Renglon_Vhp_01 = Hoja_Vhp_01.Table.Rows.Add();
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MODIFICACION", "Encabezado"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TIPO", "Encabezado"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NATURALEZA", "Encabezado"));
            Suma_Inicial = 0.0;
            Suma_Final = 0.0;
            Suma_Modificado = 0.0;

            if (Dt_Vhp_01.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Vhp_01.Rows)
                {
                    //if (Convert.ToDouble(Registro["SALDO_FINAL"].ToString()) != 0)
                    //{
                    Renglon_Vhp_01 = Hoja_Vhp_01.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Inicial += Convert.ToDouble(Registro["SALDO_INICIAL"].ToString());
                    Suma_Final += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                    Suma_Modificado += Convert.ToDouble(Registro["MODIFICACION"].ToString());


                    Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO_INICIAL"].ToString()));
                    Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO_FINAL"].ToString()));
                    Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MODIFICACION"].ToString()));
                    Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["TIPO"].ToString(), "Contenido_Nota"));
                    Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NATURALEZA"].ToString(), "Contenido_Nota"));
                }
            }
            else
                Renglon_Vhp_01 = Hoja_Vhp_01.Table.Rows.Add();


            Renglon_Vhp_01 = Hoja_Vhp_01.Table.Rows.Add();
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Inicial);
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Final);
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Modificado);
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Vhp_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            #endregion
            #region Nota Vhp_02
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Vhp_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Vhp_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Vhp_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 saldo inicial.
            Hoja_Vhp_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 saldo final
            Hoja_Vhp_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  5 Modificado.
            Hoja_Vhp_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  6 Naturaleza.

            Celda = Renglon_Vhp_02.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Vhp_02.Cells.Add();
            Renglon_Vhp_02.Cells.Add();
            Renglon_Vhp_02.Cells.Add();
            Renglon_Vhp_02.Cells.Add();
            Renglon_Vhp_02.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Vhp_02 = Hoja_Vhp_02.Table.Rows.Add();
            Celda = Renglon_Vhp_02.Cells.Add("I. INFORMACIÓN CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Vhp_02 = Hoja_Vhp_02.Table.Rows.Add();
            Renglon_Vhp_02 = Hoja_Vhp_02.Table.Rows.Add();
            Renglon_Vhp_02 = Hoja_Vhp_02.Table.Rows.Add();
            Celda = Renglon_Vhp_02.Cells.Add("3200    HACIENDA PÚBLICA/PATRIMONIO GENERADO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA: VHP-02", "Encabezado"));

            Renglon_Vhp_02 = Hoja_Vhp_02.Table.Rows.Add();
            Renglon_Vhp_02 = Hoja_Vhp_02.Table.Rows.Add();
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MODIFICACION", "Encabezado"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NATURALEZA", "Encabezado"));
            Suma_Inicial = 0.0;
            Suma_Final = 0.0;
            Suma_Modificado = 0.0;

            if (Dt_Vhp_02.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Vhp_02.Rows)
                {
                    Renglon_Vhp_02 = Hoja_Vhp_02.Table.Rows.Add();
                    //  para la suma de los saldos
                    Suma_Inicial += Convert.ToDouble(Registro["SALDO_INICIAL"].ToString());
                    Suma_Final += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                    Suma_Modificado += Convert.ToDouble(Registro["MODIFICACION"].ToString());


                    Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                    Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO_INICIAL"].ToString()));
                    Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO_FINAL"].ToString()));
                    Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["MODIFICACION"].ToString()));
                    Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NATURALEZA"].ToString(), "Contenido_Nota"));
                }
            }
            else
                Renglon_Vhp_02 = Hoja_Vhp_02.Table.Rows.Add();

            Renglon_Vhp_02 = Hoja_Vhp_02.Table.Rows.Add();
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL ", "Totales_Verde_Texto"));
            Formato_Importe = String.Format("{0:n}", Suma_Inicial);
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Final);
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Formato_Importe = String.Format("{0:n}", Suma_Modificado);
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
            Renglon_Vhp_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Totales_Verde"));
            #endregion
            #region Nota EFE_01
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Efe_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Efe_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Efe_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 saldo inicial.
            Hoja_Efe_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 saldo final
            Hoja_Efe_01.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  5 Flujo.

            Celda = Renglon_Efe_01.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Efe_01.Cells.Add();
            Renglon_Efe_01.Cells.Add();
            Renglon_Efe_01.Cells.Add();
            Renglon_Efe_01.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();
            Celda = Renglon_Efe_01.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();
            Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();
            Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();
            Celda = Renglon_Efe_01.Cells.Add("1110    FLUJO DE EFECTIVO");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Sin_Linea";
            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Sin_Linea_2"));
            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:         EFE-01", "Encabezado"));

            Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();
            Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();
            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
            Suma_Inicial = 0.0;
            Suma_Final = 0.0;
            Suma_Modificado = 0.0;
            String Ayudante = "";
            if (Dt_Efe_01.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Efe_01.Rows)
                {
                    Ayudante = Registro["CUENTA"].ToString();
                    if (Registro["CUENTA"].ToString() == " " || Registro["CUENTA"].ToString() == "")
                    {
                        Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Totales_Verde"));

                        //Verificar si es el total
                        if (Registro["NOMBRE"].ToString().ToUpper().Trim() == "TOTAL")
                        {
                            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL", "Totales_Verde_Texto"));
                        }
                        else
                        {
                            Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Totales_Verde"));
                        }
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO_INICIAL"].ToString()));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO_FINAL"].ToString()));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["FLUJO"].ToString()));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Verde"));
                    }
                    else if (Convert.ToDouble(Registro["CUENTA"].ToString()) == 1111 ||
                        Convert.ToDouble(Registro["CUENTA"].ToString()) == 1112 ||
                        Convert.ToDouble(Registro["CUENTA"].ToString()) == 1114 ||
                        Convert.ToDouble(Registro["CUENTA"].ToString()) == 1115 ||
                        Convert.ToDouble(Registro["CUENTA"].ToString()) == 1116)
                    {
                        Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Totales_Negritas"));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre_Negritas"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO_INICIAL"].ToString()));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Negritas"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO_FINAL"].ToString()));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Negritas"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["FLUJO"].ToString()));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales_Negritas"));
                    }
                    else
                    {
                        Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO_INICIAL"].ToString()));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SALDO_FINAL"].ToString()));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["FLUJO"].ToString()));
                        Renglon_Efe_01.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Totales"));
                    }
                }
            }
            else
                Renglon_Efe_01 = Hoja_Efe_01.Table.Rows.Add();

            #endregion
            #region Nota Efe_02
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Efe_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//   1 indice.
            Hoja_Efe_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//  2 NOMBRE / NOTA.
            Hoja_Efe_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 Flujo
            Hoja_Efe_02.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 sub

            Celda = Renglon_Efe_02.Cells.Add("DE DESGLOSE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Efe_02.Cells.Add();
            Renglon_Efe_02.Cells.Add();
            Renglon_Efe_02.Cells.Add(new WorksheetCell(Campo_Oculto_CONAC, DataType.String, "Encabezado_Transparente"));

            Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
            Celda = Renglon_Efe_02.Cells.Add("I. INFORMACION CONTABLE");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
            Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
            Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
            Celda = Renglon_Efe_02.Cells.Add("1210,1230,1240 y 1250,ADQ. BIENES MUEBLES,INMUEBLES E INTANGIBLES");
            Celda.MergeAcross = 2; // Merge 2 cells together
            Celda.StyleID = "Encabezado";
            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOTA:     EFE-02", "Encabezado"));

            Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
            Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("% SUB", "Encabezado"));
            Suma_Inicial = 0.0;
            Suma_Final = 0.0;
            Suma_Modificado = 0.0;
            if (Dt_Efe_02.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Efe_02.Rows)
                {
                    if (Registro["CUENTA"].ToString() == " " || Registro["CUENTA"].ToString() == "")
                    {
                        if (Registro["NOMBRE"].ToString()=="TOTAL")
                        {
                            Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
                            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Totales_Verde"));
                            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Totales_Verde"));
                            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["FLUJO"].ToString()));
                            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Contenido_Nombre"));
                            //Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SUB"].ToString()));
                            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nombre"));
                        }else{
                            Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
                            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Nombre"));
                            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                            Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["FLUJO"].ToString()));
                            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Contenido_Nombre"));
                            //Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SUB"].ToString()));
                            Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nombre"));
                        }
                        
                    }
                    else if (Convert.ToDouble(Registro["CUENTA"].ToString()) == 1235 ||
                        Convert.ToDouble(Registro["CUENTA"].ToString()) == 1236 ||
                        Convert.ToDouble(Registro["CUENTA"].ToString()) == 1241 ||
                        Convert.ToDouble(Registro["CUENTA"].ToString()) == 1242 ||
                        Convert.ToDouble(Registro["CUENTA"].ToString()) == 1243 ||
                        Convert.ToDouble(Registro["CUENTA"].ToString()) == 1244 ||
                        Convert.ToDouble(Registro["CUENTA"].ToString()) == 1246)
                    {
                        Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
                        Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Nombre_Negritas"));
                        Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre_Negritas"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["FLUJO"].ToString()));
                        Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Contenido_Nombre"));
                       // Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SUB"].ToString()));
                        Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nombre"));
                    }
                    else
                    {
                        Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();
                        Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["CUENTA"].ToString(), "Contenido_Indice"));
                        Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Registro["NOMBRE"].ToString(), "Contenido_Nombre"));
                        Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["FLUJO"].ToString()));
                        Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Formato_Importe, DataType.Number, "Contenido_Nombre"));
                        //Formato_Importe = String.Format("{0:n}", Convert.ToDouble(Registro["SUB"].ToString()));
                        Renglon_Efe_02.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Contenido_Nombre"));
                    }
                }
            }
            else
                Renglon_Efe_02 = Hoja_Efe_02.Table.Rows.Add();

            #endregion
            #region Nota Memoria_Contables
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja_Con.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  1 NÚMERO DE CTA
            Hoja_Con.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//  2 CONCEPTO
            Hoja_Con.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 SALDO INICIAL
            Hoja_Con.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 FLUJO
            Hoja_Con.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  5 SALDO FINAL

            //  se llena el encabezado principal
            Renglon_Con = Hoja_Con.Table.Rows.Add();

            Celda = Renglon_Con.Cells.Add("II. DE MEMORIA (DE ORDEN)");
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";


            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Celda = Renglon_Con.Cells.Add("A) Contables:");
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";

            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Celda = Renglon_Con.Cells.Add("B) Presupuestales:");
            Celda.MergeAcross = 1; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Celda = Renglon_Con.Cells.Add("NOTAS DE MEMORIA");
            Celda.MergeAcross = 3; // Merge 2 cells together
            Celda.StyleID = "Encabezado_Inicial";
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CUENTA", "Encabezado"));
            Renglon_Con.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOMBRE DE LA CUENTA", "Encabezado"));
            Renglon_Con.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            Renglon_Con.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));
            Renglon_Con.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));

            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con = Hoja_Con.Table.Rows.Add();

            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Renglon_Con.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado_Inicial"));
            Renglon_Con.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL", "Contenido_Nombre_Negritas"));

            Renglon_Con = Hoja_Con.Table.Rows.Add();

            Renglon_Con = Hoja_Con.Table.Rows.Add();
            Celda = Renglon_Con.Cells.Add("Bajo protesta de decir verdad declaramos que los Estados Financieros y sus notas, son razonablemente correctos y son responsabilidad del emisor.");
            Celda.MergeAcross = 4; // Merge 5 cells together
            Celda.MergeDown = 1;// 2
            Celda.StyleID = "Encabezado_Inicial";
            #endregion
            #region Nota Memoria_Presupuestales
            ////  Agregamos las columnas que tendrá la hoja de excel.
            //Hoja_Pre.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  1 NÚMERO DE CTA
            //Hoja_Pre.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//  2 CONCEPTO
            //Hoja_Pre.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  3 SALDO INICIAL
            //Hoja_Pre.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  4 FLUJO
            //Hoja_Pre.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  5 SALDO FINAL

            ////  se llena el encabezado principal
            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();

            //Celda = Renglon_Pre.Cells.Add("II. Notas de Memoria (Cuentas de Orden)");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            //Celda.StyleID = "Encabezado_Izquierda";

            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();

            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Celda = Renglon_Pre.Cells.Add("B) Presupuestales:");
            //Celda.MergeAcross = 1; // Merge 2 cells together
            //Celda.StyleID = "Encabezado_Izquierda";

            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Renglon_Pre.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NÚMERO DE CTA", "Encabezado"));
            //Renglon_Pre.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CONCEPTO", "Encabezado"));
            //Renglon_Pre.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO INICIAL", "Encabezado"));
            //Renglon_Pre.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FLUJO", "Encabezado"));
            //Renglon_Pre.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO FINAL", "Encabezado"));

            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();

            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Renglon_Pre.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "Encabezado"));
            //Renglon_Pre.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL", "Contenido_Nombre_Negritas"));

            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();

            //Renglon_Pre = Hoja_Pre.Table.Rows.Add();
            //Celda = Renglon_Pre.Cells.Add("Bajo protesta de decir verdad declaramos que los Estados Financieros y sus notas, son razonablemente correctos y son responsabilidad del emisor.");
            //Celda.MergeAcross = 4; // Merge 5 cells together
            //Celda.MergeDown = 1;// 2
            //Celda.StyleID = "Encabezado_Izquierda";


            #endregion
            #endregion
            //Asignar la ruta del archivo
            Ruta_Archivo = HttpContext.Current.Server.MapPath("~") + "\\Exportaciones\\" + Nombre_Archivo;
            Libro.Save(Ruta_Archivo);
            Mostrar_Reporte(Nombre_Archivo, "Excel");
        }// fin try
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Asignar_Mes
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  String Mes_Numerico.- es el numero del mes
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  23/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected String Asignar_Mes(String Mes_Numerico)
    {
        string Mes = "";
        try
        {
            switch (Mes_Numerico)
            {
                case "01":
                    Mes = "ENERO";
                    break;
                case "02":
                    Mes = "FEBRERO";
                    break;
                case "03":
                    Mes = "MARZO";
                    break;
                case "04":
                    Mes = "ABRIL";
                    break;
                case "05":
                    Mes = "MAYO";
                    break;
                case "06":
                    Mes = "JUNIO";
                    break;
                case "07":
                    Mes = "JULIO";
                    break;
                case "08":
                    Mes = "AGOSTO";
                    break;
                case "09":
                    Mes = "SEPTIEMBRE";
                    break;
                case "10":
                    Mes = "OCTUBRE";
                    break;
                case "11":
                    Mes = "NOVIEMBRE";
                    break;
                case "12":
                    Mes = "DICIEMBRE";
                    break;
            }
            return Mes;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Periodo_Anterior
    ///DESCRIPCIÓN: Realiza el calculo del periodo anterior
    ///PARAMETROS:  String Mes_Numerico.- es el numero del mes
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  24/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected String Periodo_Anterior(String Mes_Numerico)
    {
        String Mes_Anterior = "";
        String Año_Anterior = "";
        Int32 Mes = 0;
        Int32 Año = 0;
        try
        {
            Mes_Anterior = Mes_Numerico.Substring(0, 2);
            if (Mes_Anterior == "01")
            {
                Año_Anterior = Mes_Numerico.Substring(2, 2);
                Año = Convert.ToInt32(Año_Anterior);
                Año--;
                Mes_Anterior = "12" + Año;
            }
            else
            {
                Mes = Convert.ToInt32(Mes_Anterior);
                Año_Anterior = Mes_Numerico.Substring(2, 2);
                Mes--;
                if (Mes <= 9)
                    Mes_Anterior = "0" + Mes + "" + Año_Anterior;
                else
                    Mes_Anterior = "" + Mes + "" + Año_Anterior;
            }

            return Mes_Anterior;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Variaciones
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///             Dt_Periodo_anterior.- son las cuentas del periodo anterior que se calcularan
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  24/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Variaciones(DataTable Dt_Consulta, DataTable Dt_Periodo_Anterior, String Saldo_Mes_Anterior, String Saldo_3210_Ant, String Saldo_3210)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataRow Row_Nueva;
        String Indice = "";
        int Contador_For = 0;
        DataRow Row_Final;
        DataTable Dt_Temp_Consulta = new DataTable(); 
        String Cuenta;
        Double SALDO = 0.0;
        Double CARGO = 0.0;
        Double ABONO = 0.0;
        Double SALDO_INICIAL = 0.0;
        try
        {
            Dt_Nueva.Columns.Add("INDICE", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_FINAL_ANTERIOR", typeof(System.String));
            Dt_Nueva.Columns.Add("CARGOS", typeof(System.String));
            Dt_Nueva.Columns.Add("ABONO", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_FINAL", typeof(System.String));
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.String));
            Dt_Nueva.Columns.Add("NOTA", typeof(System.String));
            Dt_Nueva.TableName = "Dt_VAriaciones";

            Rs_Consulta.P_Filtro1 = "3";
            Dt_Cuentas = Rs_Consulta.Consulta_Cuentas();

            Dt_Temp_Consulta.Columns.Add("CUENTA", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("DESCRIPCION", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("NIVEL_ID", typeof(System.String));
            DataRow row;
            foreach (DataRow Renglon in Dt_Cuentas.Rows)
            {

                if (Renglon["CUENTA"].ToString().StartsWith("3"))
                {
                    row = Dt_Temp_Consulta.NewRow();
                    row["CUENTA"] = Renglon["CUENTA"].ToString();
                    row["DESCRIPCION"] = Renglon["DESCRIPCION"].ToString();
                    row["NIVEL_ID"] = Renglon["NIVEL_ID"].ToString();
                    Dt_Temp_Consulta.Rows.Add(row);
                    Dt_Temp_Consulta.AcceptChanges();
                }
            }
            //  se agregaran los saldos de las cuentas 
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00004")
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Consulta.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 4) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString()) - Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                        }
                    }
                    foreach (DataRow Renglon in Dt_Periodo_Anterior.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 4) == Cuenta)
                        {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_FINAL_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                    Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO + SALDO_INICIAL).Replace("$", "");
                    Row_Nueva["CARGOS"] = String.Format("{0:c}", CARGO).Replace("$", "");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", (SALDO)).Replace("$", "");
                    //  para las notas
                    if (Cuenta == "311")
                        Row_Nueva["NOTA"] = "VHP-01";

                    else if (Cuenta == "32")
                        Row_Nueva["NOTA"] = "VHP-02";

                    else
                        Row_Nueva["NOTA"] = "";
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Dt_Nueva.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00003")
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 3);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Consulta.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 3) == Cuenta)
                        {
                            SALDO = SALDO - Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString()) + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                        }
                    }
                    foreach (DataRow Renglon in Dt_Periodo_Anterior.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 3) == Cuenta)
                        {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                        }
                    }
                    if (Cuenta == "321")
                    {
                        if (Convert.ToDouble(Saldo_3210) == 0)
                        {
                            ABONO = ABONO + Convert.ToDouble(Saldo_Mes_Anterior);
                        }
                        else
                        {
                            ABONO = ABONO + Convert.ToDouble(Saldo_3210); //- Convert.ToDouble(Saldo_Mes_Anterior);
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    if (Cuenta == "322")
                    {
                            Row_Nueva["SALDO_FINAL_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL + Convert.ToDouble(Saldo_3210_Ant.Replace(",", ""))).Replace("$", "");
                            Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO + SALDO_INICIAL + Convert.ToDouble(Saldo_3210_Ant.Replace(",", ""))).Replace("$", "");
                    }
                    else
                    {
                        Row_Nueva["SALDO_FINAL_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO + SALDO_INICIAL).Replace("$", "");
                    }
                    Row_Nueva["CARGOS"] = String.Format("{0:c}", CARGO).Replace("$", "");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", (SALDO)).Replace("$", "");

                    //  para las notas
                    if (Cuenta == "311")
                        Row_Nueva["NOTA"] = "VHP-01";

                    else if (Cuenta == "32")
                        Row_Nueva["NOTA"] = "VHP-02";

                    else
                        Row_Nueva["NOTA"] = "";
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Dt_Nueva.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00002")
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 2);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Consulta.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 2) == Cuenta)
                        {
                            SALDO = SALDO - Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString()) + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                        }
                    }
                    foreach (DataRow Renglon in Dt_Periodo_Anterior.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 2) == Cuenta)
                        {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    if (Cuenta == "32")
                    {
                        Row_Nueva["SALDO_FINAL_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL + Convert.ToDouble(Saldo_3210_Ant.Replace(",", ""))).Replace("$", "");
                        Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO + SALDO_INICIAL + Convert.ToDouble(Saldo_3210_Ant.Replace(",", ""))).Replace("$", "");
                    }
                    else
                    {
                        Row_Nueva["SALDO_FINAL_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO + SALDO_INICIAL).Replace("$", "");
                    }
                    Row_Nueva["CARGOS"] = String.Format("{0:c}", CARGO).Replace("$", "");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    Row_Nueva["FLUJO"] = String.Format("{0:c}", (SALDO)).Replace("$", "");
                    //  para las notas
                    if (Cuenta == "311")
                        Row_Nueva["NOTA"] = "VHP-01";

                    else if (Cuenta == "32")
                        Row_Nueva["NOTA"] = "VHP-02";

                    else
                        Row_Nueva["NOTA"] = "";
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Dt_Nueva.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00001")
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 1);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Consulta.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 1) == Cuenta)
                        {
                            SALDO = SALDO - Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString()) + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                        }
                    }
                    foreach (DataRow Renglon in Dt_Periodo_Anterior.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 1) == Cuenta)
                        {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString(); 
                    if (Cuenta == "3")
                    {
                            Row_Nueva["SALDO_FINAL_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL + Convert.ToDouble(Saldo_3210_Ant.Replace(",", ""))).Replace("$", "");
                            Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO + SALDO_INICIAL + Convert.ToDouble(Saldo_3210_Ant.Replace(",", ""))).Replace("$", "");
                    }
                    else
                    {
                        Row_Nueva["SALDO_FINAL_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        Row_Nueva["SALDO_FINAL"] = String.Format("{0:c}", SALDO + SALDO_INICIAL).Replace("$", "");
                    }
                    Row_Nueva["CARGOS"] = String.Format("{0:c}", CARGO).Replace("$", "");
                    Row_Nueva["ABONO"] = String.Format("{0:c}", ABONO).Replace("$", "");
                    if (Cuenta == "3")
                    {
                        Row_Nueva["FLUJO"] = String.Format("{0:c}", (SALDO + SALDO_INICIAL + Convert.ToDouble(Saldo_3210_Ant.Replace(",", ""))) - (SALDO_INICIAL + Convert.ToDouble(Saldo_3210_Ant.Replace(",", "")))).Replace("$", "");
                    }
                    else
                    {
                        Row_Nueva["FLUJO"] = String.Format("{0:c}", (SALDO)).Replace("$", "");
                    }
                    
                    //  para las notas
                    if (Cuenta == "311")
                        Row_Nueva["NOTA"] = "VHP-01";

                    else if (Cuenta == "32")
                        Row_Nueva["NOTA"] = "VHP-02";

                    else
                        Row_Nueva["NOTA"] = "";
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Dt_Nueva.AcceptChanges();
                }
            }

            //  ordenar la tabla
            Dt_Final.Columns.Add("INDICE", typeof(System.String));
            Dt_Final.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Final.Columns.Add("SALDO_FINAL_ANTERIOR", typeof(System.String));
            Dt_Final.Columns.Add("CARGOS", typeof(System.String));
            Dt_Final.Columns.Add("ABONO", typeof(System.String));
            Dt_Final.Columns.Add("SALDO_FINAL", typeof(System.String));
            Dt_Final.Columns.Add("FLUJO", typeof(System.String));
            Dt_Final.Columns.Add("NOTA", typeof(System.String));
            Dt_Final.TableName = "Dt_VAriaciones";


            //  ordenar la tabla
            for (Contador_For = Dt_Nueva.Rows.Count - 1; Contador_For >= 0; Contador_For--)
            {
                Row_Final = Dt_Final.NewRow();
                Indice = Dt_Nueva.Rows[Contador_For]["INDICE"].ToString();
                if (Indice == "3210")
                {
                    Row_Final["INDICE"] = Convert.ToDouble(Indice);
                    Row_Final["NOMBRE"] = Dt_Nueva.Rows[Contador_For]["NOMBRE"].ToString();
                    Row_Final["SALDO_FINAL_ANTERIOR"] = Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString();
                    Row_Final["CARGOS"] = Dt_Nueva.Rows[Contador_For]["CARGOS"].ToString();
                    Row_Final["ABONO"] = Dt_Nueva.Rows[Contador_For]["ABONO"].ToString();
                    if (Convert.ToDouble(Saldo_3210) == 0)
                    {
                        Row_Final["SALDO_FINAL"] = String.Format("{0:c}", Convert.ToDouble(Saldo_Mes_Anterior)).Replace("$", "");
                    }
                    else
                    {
                        Row_Final["SALDO_FINAL"] = String.Format("{0:c}", Convert.ToDouble(Saldo_3210)).Replace("$", "");
                    }
                    Row_Final["FLUJO"] = String.Format("{0:c}", (Convert.ToDouble(Saldo_3210)) - Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString())).Replace("$", "");
                    Row_Final["NOTA"] = Dt_Nueva.Rows[Contador_For]["NOTA"].ToString();
                }
                else
                {
                    if (Indice == "3200")
                    {
                        Row_Final["INDICE"] = Convert.ToDouble(Indice);
                        Row_Final["NOMBRE"] = Dt_Nueva.Rows[Contador_For]["NOMBRE"].ToString();
                        Row_Final["SALDO_FINAL_ANTERIOR"] = Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString();
                        Row_Final["CARGOS"] = Dt_Nueva.Rows[Contador_For]["CARGOS"].ToString();
                        Row_Final["ABONO"] = Dt_Nueva.Rows[Contador_For]["ABONO"].ToString();
                        if (Convert.ToDouble(Saldo_3210) == 0)
                        {
                            Row_Final["SALDO_FINAL"] = String.Format("{0:c}", Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString()) + Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["ABONO"].ToString()) + ( Convert.ToDouble(Saldo_Mes_Anterior))).Replace("$", " ");
                            Row_Final["FLUJO"] = String.Format("{0:c}", (Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString()) + Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["ABONO"].ToString()) + (Convert.ToDouble(Saldo_Mes_Anterior))) - (Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString()))).Replace("$", "");
                        }
                        else
                        {
                            Row_Final["SALDO_FINAL"] = String.Format("{0:c}", Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString()) + Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["ABONO"].ToString()) + (Convert.ToDouble(Saldo_3210))).Replace("$", " ");
                            Row_Final["FLUJO"] = String.Format("{0:c}", (Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString()) + Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["ABONO"].ToString()) + (Convert.ToDouble(Saldo_3210))) - (Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString()))).Replace("$", "");
                        }
                        Row_Final["NOTA"] = Dt_Nueva.Rows[Contador_For]["NOTA"].ToString();
                    }
                    else
                    {
                        if (Indice == "3000")
                        {
                            Row_Final["INDICE"] = Convert.ToDouble(Indice);
                            Row_Final["NOMBRE"] = Dt_Nueva.Rows[Contador_For]["NOMBRE"].ToString();
                            Row_Final["SALDO_FINAL_ANTERIOR"] = Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString();
                            Row_Final["CARGOS"] = Dt_Nueva.Rows[Contador_For]["CARGOS"].ToString();
                            Row_Final["ABONO"] = Dt_Nueva.Rows[Contador_For]["ABONO"].ToString();
                            if (Convert.ToDouble(Saldo_3210) == 0)
                            {
                                Row_Final["SALDO_FINAL"] = String.Format("{0:c}", Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL"].ToString()) + (Convert.ToDouble(Saldo_Mes_Anterior))).Replace("$", " ");
                                Row_Final["FLUJO"] = String.Format("{0:c}", (Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL"].ToString()) + (Convert.ToDouble(Saldo_Mes_Anterior))) - Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString())).Replace("$", "");
                            }
                            else
                            {
                                Row_Final["SALDO_FINAL"] = String.Format("{0:c}", Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL"].ToString()) + (Convert.ToDouble(Saldo_3210))).Replace("$", " ");
                                Row_Final["FLUJO"] = String.Format("{0:c}", (Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL"].ToString()) + (Convert.ToDouble(Saldo_3210))) - Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString())).Replace("$", "");
                            }
                            Row_Final["NOTA"] = Dt_Nueva.Rows[Contador_For]["NOTA"].ToString();
                        }
                        else
                        {
                            Row_Final["INDICE"] = Convert.ToDouble(Indice);
                            Row_Final["NOMBRE"] = Dt_Nueva.Rows[Contador_For]["NOMBRE"].ToString();
                            Row_Final["SALDO_FINAL_ANTERIOR"] = Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString();
                            Row_Final["CARGOS"] = Dt_Nueva.Rows[Contador_For]["CARGOS"].ToString();
                            Row_Final["ABONO"] = Dt_Nueva.Rows[Contador_For]["ABONO"].ToString();
                            Row_Final["SALDO_FINAL"] = Dt_Nueva.Rows[Contador_For]["SALDO_FINAL"].ToString();
                            Row_Final["FLUJO"] = String.Format("{0:c}", (Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL"].ToString()) - Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["SALDO_FINAL_ANTERIOR"].ToString()))).Replace("$", "");
                            Row_Final["NOTA"] = Dt_Nueva.Rows[Contador_For]["NOTA"].ToString();
                        }
                    }
                }

                //Verificar si tiene un filtrado
                if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                {
                    //verificar si el cargo y el abono son ceros
                    if (Convert.ToDecimal(Row_Final["CARGOS"]) != 0 || Convert.ToDecimal(Row_Final["ABONO"]) != 0 || Convert.ToDecimal(Row_Final["SALDO_FINAL_ANTERIOR"]) != 0 || Convert.ToDecimal(Row_Final["SALDO_FINAL"]) != 0)
                    {
                        Dt_Final.Rows.Add(Row_Final);
                    }
                }
                else
                {
                    Dt_Final.Rows.Add(Row_Final);
                }
            }
            Dt_Final.DefaultView.Sort = " INDICE ASC";
            return Dt_Final.DefaultView.ToTable();
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Esf
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///             Dt_Periodo_2010.- Consulta del periodo 2010
    ///             Tipo_Reporte.- establece que reporte es esf
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  02/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Esf(DataTable Dt_Consulta, DataTable Dt_Periodo_2, int Tipo_Reporte,DataTable Dt_Periodo_1,String Filtro)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas_Contables = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataRow Row_Nueva;
        String Indice = "";
        Double Suma_Saldo = 0;
        Double Salso_Final = 0;
        try
        {
            if (Tipo_Reporte == 2)
            {
                Dt_Nueva.Columns.Add("CUENTA", typeof(System.String));
                Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
                Dt_Nueva.Columns.Add("MONTO", typeof(System.Double));
                Dt_Nueva.Columns.Add(Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString())-1), typeof(System.Double));
                Dt_Nueva.Columns.Add(Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 2), typeof(System.Double));

                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    if (Convert.ToDouble(Registro["SALDO_FINAL"].ToString()) != 0)
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = (Registro["CUENTA"].ToString());
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Row_Nueva["MONTO"] = (Registro["SALDO_FINAL"].ToString());
                        //Suma_Saldo_Final_2 = Convert.ToDouble((Registro["SALDO_FINAL"].ToString()));
                        foreach (DataRow Registro_1 in Dt_Periodo_1.Rows)
                        {
                            if (Indice == (Registro_1["CUENTA"].ToString()))
                            {
                                Suma_Saldo += Convert.ToDouble((Registro_1["SALDO_FINAL"].ToString()));
                            }
                        }
                        Row_Nueva[Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1)] = Suma_Saldo;
                        // Suma_Saldo_Final_1 = Suma_Saldo_Final_2 - Suma_Saldo;
                        Suma_Saldo = 0.0;
                        foreach (DataRow Registro_2 in Dt_Periodo_2.Rows)
                        {
                            if (Indice == (Registro_2["CUENTA"].ToString()))
                            {
                                Suma_Saldo += Convert.ToDouble((Registro_2["SALDO_FINAL"].ToString()));
                            }
                        }
                        Row_Nueva[Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 2)] = Suma_Saldo;
                        Dt_Nueva.Rows.Add(Row_Nueva);
                        Suma_Saldo = 0.0;
                    }
                }
            }
            else
            {
                Dt_Nueva.Columns.Add("CUENTA", typeof(System.String));
                Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
                if (Tipo_Reporte == 3)
                {
                    Dt_Nueva.Columns.Add("IMPORTE", typeof(System.Double));
                    Dt_Nueva.Columns.Add("IMPORTE_90", typeof(System.Double));
                    Dt_Nueva.Columns.Add("IMPORTE_180", typeof(System.Double));
                    Dt_Nueva.Columns.Add("IMPORTE_365", typeof(System.Double));
                    Dt_Nueva.Columns.Add("CARACTERISTICAS", typeof(System.String));
                    Dt_Nueva.Columns.Add("ESTATUS DEL ADEUDO", typeof(System.String));
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        if (Convert.ToDouble(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Row_Nueva = Dt_Nueva.NewRow();
                            Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                            Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                            Row_Nueva["IMPORTE"] = (Registro["SALDO_FINAL"].ToString());
                            Row_Nueva["IMPORTE_90"]=0;
                            Row_Nueva["IMPORTE_180"]=0;
                            Row_Nueva["IMPORTE_365"]=0;
                            Row_Nueva["CARACTERISTICAS"]="";
                            Row_Nueva["ESTATUS DEL ADEUDO"] = "";
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                }
                else if (Tipo_Reporte == 5)
                {
                    Dt_Nueva.Columns.Add("MONTO", typeof(System.Double));
                    Dt_Nueva.Columns.Add("METODO", typeof(System.String));
                    Rs_Consulta.P_Filtro1 = Filtro;
                    Dt_Cuentas_Contables = Rs_Consulta.Consulta_Cuentas();
                    Indice = "";
                    Suma_Saldo = 0.0;
                    for (Int16 Contador_For = 0; Contador_For < Dt_Cuentas_Contables.Rows.Count; Contador_For++)
                    {
                        if (Dt_Cuentas_Contables.Rows[Contador_For]["NIVEL_ID"].ToString() == "00006")
                        {
                            Salso_Final = 0;
                            Row_Nueva = Dt_Nueva.NewRow();
                            Indice = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 12);
                            Row_Nueva["CUENTA"] = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString();
                            Row_Nueva["NOMBRE"] = Dt_Cuentas_Contables.Rows[Contador_For]["DESCRIPCION"].ToString();
                            foreach (DataRow Registro in Dt_Consulta.Rows)
                            {
                                if (Indice == (Registro["CUENTA"].ToString().Substring(0, 12)))
                                {
                                    Salso_Final = Salso_Final + Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                                }
                            }
                            Row_Nueva["MONTO"] = Salso_Final;
                            Row_Nueva["METODO"] = "";
                            if (Salso_Final != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                    }
                }
                else if (Tipo_Reporte == 8)
                {
                    Rs_Consulta.P_Filtro1 = Filtro;
                    Dt_Cuentas_Contables= Rs_Consulta.Consulta_Cuentas();
                    Dt_Nueva.Columns.Add("SALDO INICIAL", typeof(System.Double)); 
                    Dt_Nueva.Columns.Add("SALDO FINAL", typeof(System.Double));
                    Dt_Nueva.Columns.Add("FLUJO", typeof(System.Double));
                    Indice = "";
                    Suma_Saldo = 0.0;
                    for (Int16 Contador_For = 0; Contador_For < Dt_Cuentas_Contables.Rows.Count; Contador_For++)
                    {
                        if (Filtro != "126" && Filtro != "125")
                        {
                            if (Dt_Cuentas_Contables.Rows[Contador_For]["NIVEL_ID"].ToString() == "00006")
                            {
                                Salso_Final = 0;
                                Suma_Saldo = 0;
                                Row_Nueva = Dt_Nueva.NewRow();
                                Indice = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 9);
                                Row_Nueva["CUENTA"] = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString();
                                Row_Nueva["NOMBRE"] = Dt_Cuentas_Contables.Rows[Contador_For]["DESCRIPCION"].ToString();
                                foreach (DataRow Registro in Dt_Consulta.Rows)
                                {
                                    if (Indice == (Registro["CUENTA"].ToString().Substring(0, 9)))
                                    {
                                        Salso_Final = Salso_Final + Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                                    }

                                }
                                foreach (DataRow Registro_1 in Dt_Periodo_1.Rows)
                                {
                                    if (Indice == (Registro_1["CUENTA"].ToString().Substring(0, 9)))
                                    {
                                        Suma_Saldo += Convert.ToDouble((Registro_1["SALDO_FINAL"].ToString()));
                                    }
                                }
                                Row_Nueva["SALDO INICIAL"] = Suma_Saldo;
                                Row_Nueva["SALDO FINAL"] = Salso_Final;
                                Row_Nueva["FLUJO"] = Salso_Final - Suma_Saldo;
                                if (Suma_Saldo != 0 || Salso_Final != 0)
                                {
                                    Dt_Nueva.Rows.Add(Row_Nueva);
                                }
                            }
                        }
                        else
                        {
                            if (Filtro == "125")
                            {
                                if (Dt_Cuentas_Contables.Rows[Contador_For]["NIVEL_ID"].ToString() == "00006")
                                {
                                    Salso_Final = 0;
                                    Suma_Saldo = 0;
                                    Row_Nueva = Dt_Nueva.NewRow();
                                    Indice = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 9);
                                    Row_Nueva["CUENTA"] = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString();
                                    Row_Nueva["NOMBRE"] = Dt_Cuentas_Contables.Rows[Contador_For]["DESCRIPCION"].ToString();
                                    foreach (DataRow Registro in Dt_Consulta.Rows)
                                    {
                                        if (Indice == (Registro["CUENTA"].ToString().Substring(0, 9)))
                                        {
                                            Salso_Final = Salso_Final + Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                                        }

                                    }
                                    foreach (DataRow Registro_1 in Dt_Periodo_1.Rows)
                                    {
                                        if (Indice == (Registro_1["CUENTA"].ToString().Substring(0,9)))
                                        {
                                            Suma_Saldo += Convert.ToDouble((Registro_1["SALDO_FINAL"].ToString()));
                                        }
                                    }
                                    Row_Nueva["SALDO INICIAL"] = Suma_Saldo ;
                                    Row_Nueva["SALDO FINAL"] = Salso_Final ;
                                    Row_Nueva["FLUJO"] = (Salso_Final - Suma_Saldo);
                                    if (Suma_Saldo != 0 || Salso_Final != 0)
                                    {
                                        Dt_Nueva.Rows.Add(Row_Nueva);
                                    }
                                }
                            }
                            else
                            {
                                if (Dt_Cuentas_Contables.Rows[Contador_For]["NIVEL_ID"].ToString() == "00004")
                                {
                                    Salso_Final = 0;
                                    Suma_Saldo = 0;
                                    Row_Nueva = Dt_Nueva.NewRow();
                                    Indice = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                                    Row_Nueva["CUENTA"] = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString();
                                    Row_Nueva["NOMBRE"] = Dt_Cuentas_Contables.Rows[Contador_For]["DESCRIPCION"].ToString();
                                    foreach (DataRow Registro in Dt_Consulta.Rows)
                                    {
                                        if (Indice == (Registro["CUENTA"].ToString().Substring(0, 4)))
                                        {
                                            Salso_Final = Salso_Final + Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                                        }

                                    }
                                    foreach (DataRow Registro_1 in Dt_Periodo_1.Rows)
                                    {
                                        if (Indice == (Registro_1["CUENTA"].ToString().Substring(0, 4)))
                                        {
                                            Suma_Saldo += Convert.ToDouble((Registro_1["SALDO_FINAL"].ToString()));
                                        }
                                    }
                                    Row_Nueva["SALDO INICIAL"] = Suma_Saldo * (-1);
                                    Row_Nueva["SALDO FINAL"] = Salso_Final * (-1);
                                    Row_Nueva["FLUJO"] = (Salso_Final - Suma_Saldo) * (-1);
                                    if (Suma_Saldo != 0 || Salso_Final != 0)
                                    {
                                        Dt_Nueva.Rows.Add(Row_Nueva);
                                    }
                                }
                            }
                        }
                    }
                }
                else if (Tipo_Reporte == 11)
                {
                    Dt_Nueva.Columns.Add("MONTO", typeof(System.Double));
                    Dt_Nueva.Columns.Add("CARACTERISTICAS", typeof(System.String));

                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        if (Convert.ToDouble(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Row_Nueva = Dt_Nueva.NewRow();
                            Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                            Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                            Row_Nueva["MONTO"] = (Registro["SALDO_FINAL"].ToString());
                            Row_Nueva["CARACTERISTICAS"] = "";
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                }
                else if (Tipo_Reporte == 09)
                {
                    Dt_Nueva.Columns.Add("SALDO INICIAL", typeof(System.Double));
                    Dt_Nueva.Columns.Add("SALDO FINAL", typeof(System.Double));
                    Dt_Nueva.Columns.Add("FLUJO", typeof(System.Double));
                    Indice = "";
                    Suma_Saldo = 0.0;
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = (Registro["CUENTA"].ToString());
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        foreach (DataRow Registro_1 in Dt_Periodo_1.Rows)
                        {
                            if (Indice == (Registro_1["CUENTA"].ToString()))
                            {
                                Suma_Saldo += Convert.ToDouble((Registro_1["SALDO_FINAL"].ToString()));
                            }
                        }
                        Row_Nueva["SALDO INICIAL"] = Suma_Saldo;
                        Row_Nueva["SALDO FINAL"] = Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDouble(Registro["SALDO_FINAL"].ToString()) - Suma_Saldo;
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }

                }
                else
                {
                    Dt_Nueva.Columns.Add("MONTO", typeof(System.Double));
                    Dt_Nueva.Columns.Add("TIPO", typeof(System.String));
                    Dt_Nueva.Columns.Add("MONTO PARCIAL", typeof(System.String));

                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        if (Convert.ToDouble(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Row_Nueva = Dt_Nueva.NewRow();
                            Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                            Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                            Row_Nueva["MONTO"] = (Registro["SALDO_FINAL"].ToString());
                            Row_Nueva["TIPO"] = "";
                            Row_Nueva["MONTO PARCIAL"] = "";
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                }
            }
          return Dt_Nueva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Situacion_Financiera
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  03/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Situacion_Financiera(DataTable Dt_Consulta)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Temp_Consulta = new DataTable();
        String Indice = "";
        String Indice_Segundo_Nivel = "";
        String Auxiliar = "";
        String Auxiliar_Cuenta = "5";
        String Tipo_Tercera_Posicion = "";
        DataRow Row_Nueva;
        int Contador_For = 0;
        Double Suma_Saldo = 0.0;
        Double Suma_Cuenta_4 = 0.0;
        Double Suma_Cuenta_5 = 0.0;
        Double Suma_Origen = 0.0;
        Double Suma_Origen_31 = 0.0;
        Double Suma_Origen_12 = 0.0;
        Double Suma_Origen_45 = 0.0;
        Double Suma_Origen_2233 = 0.0;
        Double Suma_Origen_2234 = 0.0;
        Double Suma_Origen_32 = 0.0;
        Double Suma_Aplicacion = 0.0;
        Double Suma_Aplicacion_121 = 0.0;
        Double Suma_Aplicacion_123 = 0.0;
        Double Suma_Aplicacion_1235 = 0.0;
        Double Suma_Aplicacion_124 = 0.0;
        Double Suma_Aplicacion_46 = 0.0;
        Double Suma_Aplicacion_2131 = 0.0;
        Double Suma_Aplicacion_2132 = 0.0;
        Double Suma_Aplicacion_23 = 0.0;
        Double Suma_Neto_Operaciones = 0.0;
        Double Suma_Neto_Inversion = 0.0;
        Double Suma_Neto_Financiamiento = 0.0;
        Double Suma_Cuenta_111 = 0.0;
        DataRow Row_Final;

        try
        {
            Dt_Nueva.Columns.Add("INDICE", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("IMPORTE", typeof(System.Double));
            Dt_Nueva.Columns.Add("NOTA", typeof(System.String));
            Dt_Nueva.TableName = "Dt_Flujo_De_Efectivo";

            Dt_Final.Columns.Add("INDICE", typeof(System.String));
            Dt_Final.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Final.Columns.Add("IMPORTE", typeof(System.Double));
            Dt_Final.Columns.Add("NOTA", typeof(System.String));
            Dt_Final.TableName = "Dt_Flujo_De_Efectivo";

            Rs_Consulta.P_Filtro1 = "41";
            Rs_Consulta.P_Filtro2 = "42";
            Rs_Consulta.P_Filtro3 = "430";
            Rs_Consulta.P_Filtro4 = "51";
            Rs_Consulta.P_Filtro5 = "52";
            Rs_Consulta.P_Filtro6 = "53";
            Rs_Consulta.P_Filtro7 = "54";
            Dt_Cuentas = Rs_Consulta.Consulta_Cuentas_Tercer_Nivel();

            Rs_Consulta.P_Filtro2 = null;
            Rs_Consulta.P_Filtro3 = null;
            Rs_Consulta.P_Filtro4 = null;
            Rs_Consulta.P_Filtro5 = null;
            Rs_Consulta.P_Filtro6 = null;
            Rs_Consulta.P_Filtro7 = null;

            for (Contador_For = Dt_Cuentas.Rows.Count - 1; Contador_For >= 0; Contador_For--)
            {
                Indice = Dt_Cuentas.Rows[Contador_For]["CUENTA"].ToString();
                Tipo_Tercera_Posicion = Indice;
                Indice_Segundo_Nivel = Indice;
                Indice_Segundo_Nivel = Indice_Segundo_Nivel.Substring(2, 1);
                Tipo_Tercera_Posicion = Tipo_Tercera_Posicion.Substring(0, 3);
                Indice = Indice.Substring(0, 4);

                ////  para los que tengan en la tercera posicion diferente a CERO 0
                if (Convert.ToDouble(Indice_Segundo_Nivel) != 0)
                {
                    //  se buscan los saldos
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        Auxiliar = (Registro["CUENTA"].ToString());
                        Auxiliar = Auxiliar.Substring(0, 3);

                        if (Tipo_Tercera_Posicion == Auxiliar)
                            Suma_Saldo += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                    }

                    Indice = Indice.Substring(0, 1);
                    Auxiliar_Cuenta = Auxiliar_Cuenta.Substring(0, 1);

                    //if (Indice != Auxiliar_Cuenta)
                    //{
                    //    //  se llena el encabezado principal de la cuenta 5000
                    //    if (Convert.ToDouble(Indice) == 4)
                    //    {
                    //        Row_Nueva = Dt_Nueva.NewRow();
                    //        Row_Nueva["NOMBRE"] = "APLICACIÓN";
                    //        Row_Nueva["IMPORTE"] = Suma_Cuenta_5;
                    //        Dt_Nueva.Rows.Add(Row_Nueva);
                    //    }

                    //}
                    //  para las sumas de las cuentas 4000 y 5000
                    if (Convert.ToDouble(Indice) == 4)
                        Suma_Cuenta_4 += Suma_Saldo;

                    else if (Convert.ToDouble(Indice) == 5)
                        Suma_Cuenta_5 += Suma_Saldo;

                    //  se pasa la cuenta para poder compararla en el siguiente ciclo
                    Auxiliar_Cuenta = Indice;
                    //  se ingresa los valores a la tabla
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["INDICE"] = Dt_Cuentas.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Cuentas.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["IMPORTE"] = Suma_Saldo;
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Suma_Saldo = 0;
                }// fin del if 0

                else if (Convert.ToDouble(Indice) == 4300)
                {
                    if (Convert.ToDouble(Auxiliar_Cuenta) == 5)
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Row_Nueva["NOMBRE"] = "APLICACIÓN";
                        Row_Nueva["IMPORTE"] = Suma_Cuenta_5;
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    //  se buscan los saldos de la cuenta 4300
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        Auxiliar = (Registro["CUENTA"].ToString());
                        Auxiliar = Auxiliar.Substring(0, 2);
                        String Auxilar_2 = Indice;
                        Auxilar_2 = Auxilar_2.Substring(0, 2);

                        if (Auxilar_2 == Auxiliar)
                            Suma_Saldo += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                    }
                    //  para las sumas de las cuentas 4000 
                    if (Convert.ToDouble(Indice) == 4)
                        Suma_Cuenta_4 += Suma_Saldo;

                    //  se pasa la cuenta para poder compararla en el siguiente ciclo
                    Auxiliar_Cuenta = Indice;
                    //  se ingresa los valores a la tabla
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["INDICE"] = Dt_Cuentas.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Cuentas.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["IMPORTE"] = Suma_Saldo;
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Suma_Saldo = 0;
                }// fin del else if 4300

            }// fin del for

            //  se llena el encabezado principal de la cuenta 4000
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "ORIGEN";
            Row_Nueva["IMPORTE"] = Suma_Cuenta_4;
            Dt_Nueva.Rows.Add(Row_Nueva);

            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "ACTIVIDADES DE OPERACIÓN";
            Dt_Nueva.Rows.Add(Row_Nueva);

            //  se acomoda la tabla 
            for (Contador_For = Dt_Nueva.Rows.Count - 1; Contador_For >= 0; Contador_For--)
            {
                Row_Final = Dt_Final.NewRow();
                Indice = Dt_Nueva.Rows[Contador_For]["INDICE"].ToString();
                Row_Final["INDICE"] = Indice;
                Row_Final["NOMBRE"] = Dt_Nueva.Rows[Contador_For]["NOMBRE"].ToString();

                if (!String.IsNullOrEmpty(Dt_Nueva.Rows[Contador_For]["IMPORTE"].ToString()))
                {
                    Row_Final["IMPORTE"] = Convert.ToDouble(Dt_Nueva.Rows[Contador_For]["IMPORTE"].ToString());
                }

                Dt_Final.Rows.Add(Row_Final);
            }

            Row_Final = Dt_Final.NewRow();
            Suma_Neto_Operaciones = Suma_Cuenta_4 - Suma_Cuenta_5;
            Row_Final["NOMBRE"] = "FLUJO NETO DE EFECTIVO DE LAS ACTIVIDADES DE OPERACIÓN";
            Row_Final["IMPORTE"] = Suma_Cuenta_4 - Suma_Cuenta_5;
            Dt_Final.Rows.Add(Row_Final);


            #region Inversion
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "ACTIVIDADES DE INVERSIÓN";
            Dt_Final.Rows.Add(Row_Final);

            //  para el origen
            #region Cuenta origen
            //  para la cuenta 31
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "31";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_31 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //  para la cuenta 2133
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "1233";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_12 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }


            //  para la cuenta 2133
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "45";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_45 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            Suma_Origen = Suma_Origen_12 + Suma_Origen_31 + Suma_Origen_45;

            //  cuenta origen
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "ORIGEN";
            Row_Final["IMPORTE"] = Suma_Origen;
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 31
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "3.1.0.0-A";
            Row_Final["NOMBRE"] = "Contribuciones de capital";
            Row_Final["IMPORTE"] = Suma_Origen_31;
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 1.2.3.3-A
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1.2.3.3-A";
            Row_Final["NOMBRE"] = "Venta de activos fisicos";
            Row_Final["IMPORTE"] = Suma_Origen_12;
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 4.5.0.0
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "4.5.0.0";
            Row_Final["NOMBRE"] = "Otros";
            Row_Final["IMPORTE"] = Suma_Origen_45;
            Dt_Final.Rows.Add(Row_Final);
            #endregion



            //  para la aplicacion
            #region Aplicacion

            //  para la cuenta 1.2.1
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "121";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_121 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //  para la cuenta 1.2.3
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "123";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Indice = Registro["CUENTA"].ToString();
                Indice = Indice.Substring(0, 4);
                if (Convert.ToDouble(Indice) != 1235)
                {
                    Suma_Aplicacion_123 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                }

            }
            //  para la cuenta 1.2.3.5
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "1235";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_1235 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //  para la cuenta 1.2.4
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "124";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_124 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //  para la cuenta 4.6
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "46";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_46 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //  cuenta Aplicacion
            Suma_Aplicacion = Suma_Aplicacion_123 + Suma_Aplicacion_1235 + Suma_Aplicacion_124 + Suma_Aplicacion_46;
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "APLICACIÓN";
            Row_Final["IMPORTE"] = Suma_Aplicacion;
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 1.2.1
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1.2.1.0-C";
            Row_Final["NOMBRE"] = "Inversiones financieras (aportaciones a fideicomisos)";
            Row_Final["IMPORTE"] = Suma_Aplicacion_121;
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 1.2.3.0-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1.2.3.0-C";
            Row_Final["NOMBRE"] = "Bienes inmuebles";
            Row_Final["IMPORTE"] = Suma_Aplicacion_123;
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 1.2.3.5-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1.2.3.5-C";
            Row_Final["NOMBRE"] = "Construcciones en proceso";
            Row_Final["IMPORTE"] = Suma_Aplicacion_1235;
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 1.2.4-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "1.2.4.0-C";
            Row_Final["NOMBRE"] = "Bienes muebles e intangibles";
            Row_Final["IMPORTE"] = Suma_Aplicacion_124;
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 4.6.0.0
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "4.6.0.0";
            Row_Final["NOMBRE"] = "Otros";
            Row_Final["IMPORTE"] = Suma_Aplicacion_46;
            Row_Final["NOTA"] = "EFE-02";
            Dt_Final.Rows.Add(Row_Final);

            //  TOTAL
            Suma_Neto_Inversion = Suma_Origen - Suma_Aplicacion;
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "FLUJO NETO DE EFECTIVO DE LAS ACTIVIDADES DE INVERSIÓN";
            Row_Final["IMPORTE"] = Suma_Origen - Suma_Aplicacion;
            Dt_Final.Rows.Add(Row_Final);

            #endregion

            #endregion

            #region Financiamiento
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "ACTIVIDADES DE FINANCIAMIENTO";
            Dt_Final.Rows.Add(Row_Final);

            #region Origen

            //  para la cuenta 2.2.3.3
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "2233";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_2233 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //  para la cuenta 2.2.3.4
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "2234";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_2234 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //  para la cuenta 3.2
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "32";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Origen_32 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            Suma_Origen = Suma_Origen_2234 + Suma_Origen_2233 + Suma_Origen_32;
            //  cuenta origen
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "ORIGEN";
            Row_Final["IMPORTE"] = Suma_Origen;
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 2.2.3.3-A
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "2.2.3.3-A";
            Row_Final["NOMBRE"] = "Endeudamiento neto interno";
            Row_Final["IMPORTE"] = Suma_Origen_2233;
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 2.2.3.4-A
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "2.2.3.4-A";
            Row_Final["NOMBRE"] = "Endeudamiento neto externo";
            Row_Final["IMPORTE"] = Suma_Origen_2234;
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 3.2.0.0
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "3.2.0.0";
            Row_Final["NOMBRE"] = "Incremento de Patrimonio/Pasivos";
            Row_Final["IMPORTE"] = Suma_Origen_32;
            Dt_Final.Rows.Add(Row_Final);

            #endregion

            #region Aplicacion
            //  para la cuenta 2.1.3.1-C
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "2131";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_2131 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //  para la cuenta 2.1.3.2-C
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "2132";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_2132 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            //  para la cuenta 2.3.0.0 -C
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "23";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Aplicacion_23 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            Suma_Aplicacion = Suma_Aplicacion_2131 + Suma_Aplicacion_2132 + Suma_Aplicacion_23;

            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "APLICACIÓN";
            Row_Final["IMPORTE"] = Suma_Aplicacion;
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 2.1.3.1-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "2.1.3.1-C";
            Row_Final["NOMBRE"] = "Servicios de la deuda interna";
            Row_Final["IMPORTE"] = Suma_Aplicacion_2131;
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 2.1.3.2-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "2.1.3.2-C";
            Row_Final["NOMBRE"] = "Servicios de la deuda externa";
            Row_Final["IMPORTE"] = Suma_Aplicacion_2132;
            Dt_Final.Rows.Add(Row_Final);

            //  cuenta 2.3.0.0-C
            Row_Final = Dt_Final.NewRow();
            Row_Final["INDICE"] = "2.3.0.0-C";
            Row_Final["NOMBRE"] = "Disminucion de otros pasivos";
            Row_Final["IMPORTE"] = Suma_Aplicacion_23;
            Dt_Final.Rows.Add(Row_Final);

            //  TOTAL
            Suma_Neto_Financiamiento = Suma_Origen - Suma_Aplicacion;
            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "FLUJO NETO DE EFECTIVO DE LAS ACTIVIDADES DE FINANCIAMIENTO";
            Row_Final["IMPORTE"] = Suma_Origen - Suma_Aplicacion;
            Row_Final["NOTA"] = "EFE-01";
            Dt_Final.Rows.Add(Row_Final);
            #endregion
            #endregion


            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = "INCREMENTO/DISMINUCIÓN NETA EN EL EFECTIVO Y EQUIVALENTES AL EFECTIVO";
            Row_Final["IMPORTE"] = Suma_Neto_Financiamiento + Suma_Neto_Inversion + Suma_Neto_Operaciones;
            Dt_Final.Rows.Add(Row_Final);


            //  para la cuenta 1.1.1
            Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
            Rs_Consulta.P_Filtro1 = "111";
            Rs_Consulta.P_Filtro2 = null;
            Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Suma_Cuenta_111 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
            }

            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = " EFECTIVO Y EQUIVALENTES AL EFECTIVO AL INICIO DEL PERIODO";
            Row_Final["IMPORTE"] = Suma_Cuenta_111;
            Dt_Final.Rows.Add(Row_Final);

            Row_Final = Dt_Final.NewRow();
            Row_Final["NOMBRE"] = " EFECTIVO Y EQUIVALENTES AL EFECTIVO AL INICIO DEL PERIODO";
            Row_Final["IMPORTE"] = (Suma_Neto_Financiamiento + Suma_Neto_Inversion + Suma_Neto_Operaciones) + Suma_Cuenta_111;
            Dt_Final.Rows.Add(Row_Final);
            return Dt_Final;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Efe_01
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte de la nota efe-01
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramirez Aguilera
    ///FECHA_CREO:  05/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Efe_01()
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Estado_Anterior = new DataTable();
        DataTable Dt_Temp_Consulta = new DataTable();
        DataRow Row_Nueva;
        String Mes_Anterior = "";
        String Indice = "";
        String Auxiliar = "";
        String Filtro = "";
        Double Suma_Saldo_Final = 0.0;
        Double Suma_Saldo_Inicial = 0.0;
        Double Suma_Saldo_Flujo = 0.0;
        Double Suma_Saldo_Cuenta_Final = 0.0;
        Double Suma_Saldo_Cuenta_Inicial = 0.0;
        Double Suma_Saldo_Cuenta_Flujo = 0.0;
        Double Suma_Saldo_Final_Final = 0.0;
        Double Suma_Saldo_Final_Inicial = 0.0;
        Double Suma_Saldo_Final_Flujo = 0.0;
        Int32 Contador_For = 0;

        try
        {
            Dt_Nueva.Columns.Add("CUENTA", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_INICIAL", typeof(System.Double));
            Dt_Nueva.Columns.Add("SALDO_FINAL", typeof(System.Double));
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.Double));
            Dt_Nueva.TableName = "Dt_Efe_01";

            for (Contador_For = 0; Contador_For < 5; Contador_For++)
            {
                if (Contador_For == 0)
                    Filtro = "1111";
                else if (Contador_For == 1)
                    Filtro = "1112";
                else if (Contador_For == 2)
                    Filtro = "1114";
                else if (Contador_For == 3)
                    Filtro = "1115";
                else if (Contador_For == 4)
                    Filtro = "1116";

                Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();
                //  para el mes anterior al reporte a reportar
                Mes_Anterior = Periodo_Anterior(Cmb_Mes.SelectedValue.ToString());
                Rs_Consulta.P_Mes_Año = Mes_Anterior;
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Estado_Anterior = Rs_Consulta.Consulta_Situacion_Financiera();

                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    Indice = (Registro["CUENTA"].ToString());

                    foreach (DataRow Cuenta_Anterior in Dt_Estado_Anterior.Rows)
                    {
                        Auxiliar = (Cuenta_Anterior["CUENTA"].ToString());
                        if (Indice == Auxiliar)
                        {
                            if (!String.IsNullOrEmpty(Cuenta_Anterior["SALDO_FINAL"].ToString()))
                                Suma_Saldo_Inicial += Convert.ToDouble(Cuenta_Anterior["SALDO_FINAL"].ToString());
                            else
                                Suma_Saldo_Inicial += 0;
                        }
                    }
                    Suma_Saldo_Final += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());
                    Suma_Saldo_Flujo = Suma_Saldo_Final - Suma_Saldo_Inicial;
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString();
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                    Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                    Row_Nueva["FLUJO"] = Suma_Saldo_Flujo;
                    Dt_Nueva.Rows.Add(Row_Nueva);

                    //  se pasan los valores para el total
                    Suma_Saldo_Cuenta_Inicial += Suma_Saldo_Inicial;
                    Suma_Saldo_Cuenta_Final += Suma_Saldo_Final;
                    Suma_Saldo_Cuenta_Flujo += Suma_Saldo_Flujo;
                    //  inicializar variables
                    Suma_Saldo_Inicial = 0.0;
                    Suma_Saldo_Final = 0.0;
                    Suma_Saldo_Flujo = 0.0;
                }// fin del for

                //  para el total de la cuenta principal
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Cuentas = Rs_Consulta.Consulta_Cuentas();
                Row_Nueva = Dt_Nueva.NewRow();
                foreach (DataRow Registro in Dt_Cuentas.Rows)
                {
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                }
                Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Cuenta_Inicial;
                Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Cuenta_Final;
                Row_Nueva["FLUJO"] = Suma_Saldo_Cuenta_Flujo;
                Dt_Nueva.Rows.Add(Row_Nueva);

                Suma_Saldo_Final_Inicial += Suma_Saldo_Cuenta_Inicial;
                Suma_Saldo_Final_Final += Suma_Saldo_Cuenta_Final;
                Suma_Saldo_Final_Flujo += Suma_Saldo_Cuenta_Flujo;
                //  inicializar variables para otra cuenta
                Suma_Saldo_Inicial = 0.0;
                Suma_Saldo_Final = 0.0;
                Suma_Saldo_Flujo = 0.0;
                Suma_Saldo_Cuenta_Inicial = 0.0;
                Suma_Saldo_Cuenta_Final = 0.0;
                Suma_Saldo_Cuenta_Flujo = 0.0;

            }// fin del for principal

            //  para el total final de la nota
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "TOTAL";
            Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Final_Inicial;
            Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final_Final;
            Row_Nueva["FLUJO"] = Suma_Saldo_Final_Flujo;
            Dt_Nueva.Rows.Add(Row_Nueva);
            return Dt_Nueva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Efe_02
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte de la nota efe-02
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramirez Aguilera
    ///FECHA_CREO:  06/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Efe_02()
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Estado_Anterior = new DataTable();
        DataTable Dt_Temp_Consulta = new DataTable();
        DataRow Row_Nueva;
        String Filtro = "";
        Double Suma_Cuenta_123 = 0.0;
        Double Suma_Cuenta_Final_123 = 0.0;
        Double Suma_Cuenta_124 = 0.0;
        Double Suma_Cuenta_Final_124 = 0.0;
        Int32 Contador_For = 0;

        try
        {
            Dt_Nueva.Columns.Add("CUENTA", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.Double));
            Dt_Nueva.Columns.Add("SUB", typeof(System.Double));
            Dt_Nueva.TableName = "Dt_Efe_02";

            // para la cuenta 123
            for (Contador_For = 0; Contador_For < 2; Contador_For++)
            {
                if (Contador_For == 0)
                    Filtro = "1235";

                else if (Contador_For == 1)
                    Filtro = "1236";

                Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString();
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                    Row_Nueva["FLUJO"] = Registro["SALDO_FINAL"].ToString();
                    //Row_Nueva["SUB"] = 0.0;
                    Dt_Nueva.Rows.Add(Row_Nueva);

                    //  se pasan los valores para el total
                    Suma_Cuenta_123 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());

                }// fin del for

                //  para el total de la cuenta principal
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Cuentas = Rs_Consulta.Consulta_Cuentas();
                Row_Nueva = Dt_Nueva.NewRow();
                foreach (DataRow Registro in Dt_Cuentas.Rows)
                {
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                }
                Row_Nueva["FLUJO"] = Suma_Cuenta_123;
                Dt_Nueva.Rows.Add(Row_Nueva);

                Suma_Cuenta_Final_123 += Suma_Cuenta_123;

                //  inicializar variables para otra cuenta
                Suma_Cuenta_123 = 0.0;

            }// fin del for principal

            //  para el total de la cuenta 1235, 1236
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "INMUEBLES";
            Row_Nueva["FLUJO"] = Suma_Cuenta_Final_123;
            //Row_Nueva["SUB"] = 0.0;
            Dt_Nueva.Rows.Add(Row_Nueva);


            //  para las cuentas de 124
            for (Contador_For = 0; Contador_For < 5; Contador_For++)
            {
                if (Contador_For == 0)
                    Filtro = "1241";

                else if (Contador_For == 1)
                    Filtro = "1242";

                else if (Contador_For == 2)
                    Filtro = "1243";

                else if (Contador_For == 3)
                    Filtro = "1244";

                else if (Contador_For == 4)
                    Filtro = "1246";

                Rs_Consulta.P_Mes_Año = Cmb_Mes.SelectedValue;
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Consulta = Rs_Consulta.Consulta_Situacion_Financiera();

                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString();
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                    Row_Nueva["FLUJO"] = Registro["SALDO_FINAL"].ToString();
                    //Row_Nueva["SUB"] = 0.0;
                    Dt_Nueva.Rows.Add(Row_Nueva);

                    //  se pasan los valores para el total
                    Suma_Cuenta_124 += Convert.ToDouble(Registro["SALDO_FINAL"].ToString());

                }// fin del for

                //  para el total de la cuenta principal
                Rs_Consulta.P_Filtro1 = Filtro;
                Dt_Cuentas = Rs_Consulta.Consulta_Cuentas();
                Row_Nueva = Dt_Nueva.NewRow();
                foreach (DataRow Registro in Dt_Cuentas.Rows)
                {
                    Row_Nueva["CUENTA"] = Registro["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Registro["DESCRIPCION"].ToString();
                }
                Row_Nueva["FLUJO"] = Suma_Cuenta_124;
                Dt_Nueva.Rows.Add(Row_Nueva);

                Suma_Cuenta_Final_124 += Suma_Cuenta_124;

                //  inicializar variables para otra cuenta
                Suma_Cuenta_124 = 0.0;

            }// fin del for principal

            //  para el total de la cuenta 1235, 1236
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "MUEBLES";
            Row_Nueva["FLUJO"] = Suma_Cuenta_Final_124;
            //Row_Nueva["SUB"] = 0.0;
            Dt_Nueva.Rows.Add(Row_Nueva);

            //  para el total de la cuenta 1235, 1236
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["NOMBRE"] = "TOTAL";
            Row_Nueva["FLUJO"] = Suma_Cuenta_Final_124 + Suma_Cuenta_Final_123;
            Row_Nueva["SUB"] = 100.0;
            Dt_Nueva.Rows.Add(Row_Nueva);

            return Dt_Nueva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Final
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  23/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Final_DyP(DataTable Dt_Consulta)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Temp_Consulta = new DataTable();
        DataRow Row_Nueva;
        String Cuenta;
        int Contador_For = 0;
        Double SALDO = 0.0;
        Double CARGO = 0.0;
        Double ABONO = 0.0;
        Double SALDO_INICIAL = 0.0;
        try
        {
            //se declaran las columnas de los datatable
            Dt_Nueva.Columns.Add("INDICE", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_INICIAL", typeof(System.Double));
            Dt_Nueva.Columns.Add("CARGO", typeof(System.Double));
            Dt_Nueva.Columns.Add("ABONO", typeof(System.Double));
            Dt_Nueva.Columns.Add("SALDO_FINAL", typeof(System.Double));
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.Double));
            Dt_Nueva.TableName = "Dt_Est_Analitico_DyP";
            Dt_Final.Columns.Add("INDICE", typeof(System.String));
            Dt_Final.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Final.Columns.Add("SALDO_INICIAL", typeof(System.Double));
            Dt_Final.Columns.Add("CARGO", typeof(System.Double));
            Dt_Final.Columns.Add("ABONO", typeof(System.Double));
            Dt_Final.Columns.Add("SALDO_FINAL", typeof(System.Double));
            Dt_Final.Columns.Add("FLUJO", typeof(System.Double));
            Dt_Final.TableName = "Dt_Est_Analitico_DyP";

            Dt_Cuentas = Rs_Consulta.Consulta_Cuentas2();

            Dt_Temp_Consulta.Columns.Add("CUENTA", typeof(System.Double));
            Dt_Temp_Consulta.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("NIVEL_ID", typeof(System.String));
            DataRow row;
            foreach (DataRow Renglon in Dt_Cuentas.Rows)
            {

                if (Renglon["CUENTA"].ToString().Substring(0, 1) == "2")
                {
                    row = Dt_Temp_Consulta.NewRow();
                    row["CUENTA"] = Renglon["CUENTA"].ToString(); ;
                    row["NOMBRE"] = Renglon["DESCRIPCION"].ToString();
                    row["NIVEL_ID"] = Renglon["NIVEL_ID"].ToString();
                    Dt_Temp_Consulta.Rows.Add(row);
                    Dt_Temp_Consulta.AcceptChanges();
                }

            }
            //  se agregaran los saldos de las cuentas 
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {

                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00004")
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Consulta.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 4) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString()) + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString()) - Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = SALDO_INICIAL;
                    Row_Nueva["SALDO_FINAL"] = SALDO;
                    Row_Nueva["CARGO"] = CARGO;
                    Row_Nueva["ABONO"] = ABONO;
                    Row_Nueva["FLUJO"] = CARGO - ABONO;
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Dt_Nueva.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00003")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 3);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 3) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString()) + Convert.ToDouble(Renglon["CARGO"].ToString()) - Convert.ToDouble(Renglon["ABONO"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString());
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = SALDO_INICIAL;
                    Row_Nueva["SALDO_FINAL"] = SALDO;
                    Row_Nueva["CARGO"] = CARGO;
                    Row_Nueva["ABONO"] = ABONO;
                    Row_Nueva["FLUJO"] = CARGO - ABONO;
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {

                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00002")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 2);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 2) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString()) + Convert.ToDouble(Renglon["CARGO"].ToString()) - Convert.ToDouble(Renglon["ABONO"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString());
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = SALDO_INICIAL;
                    Row_Nueva["SALDO_FINAL"] = SALDO;
                    Row_Nueva["CARGO"] = CARGO;
                    Row_Nueva["ABONO"] = ABONO;
                    Row_Nueva["FLUJO"] = CARGO - ABONO;
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00001")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 1);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 1) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString()) + Convert.ToDouble(Renglon["CARGO"].ToString()) - Convert.ToDouble(Renglon["ABONO"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString());
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = SALDO_INICIAL;
                    Row_Nueva["SALDO_FINAL"] = SALDO;
                    Row_Nueva["CARGO"] = CARGO;
                    Row_Nueva["ABONO"] = ABONO;
                    Row_Nueva["FLUJO"] = CARGO - ABONO;
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            foreach (DataRow Renglon in Dt_Final.Rows)
            {
                Row_Nueva = Dt_Nueva.NewRow();
                Row_Nueva["INDICE"] = Renglon["INDICE"].ToString();
                Row_Nueva["NOMBRE"] = Renglon["NOMBRE"].ToString();
                Row_Nueva["SALDO_INICIAL"] = Renglon["SALDO_INICIAL"].ToString();
                Row_Nueva["SALDO_FINAL"] = Renglon["SALDO_FINAL"].ToString();
                Row_Nueva["CARGO"] = Renglon["CARGO"].ToString();
                Row_Nueva["ABONO"] = Renglon["ABONO"].ToString();
                Row_Nueva["FLUJO"] = Renglon["FLUJO"].ToString();
                Dt_Nueva.Rows.Add(Row_Nueva);
                Dt_Nueva.AcceptChanges();

            }

            return Dt_Nueva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Era
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///             Tipo_Reporte.- establece que reporte es era
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  01/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Era(DataTable Dt_Consulta, int Tipo_Reporte)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();

        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        Decimal Saldo_Era_03 = 0;
        DataRow Row_Nueva;
        Decimal SALDO_FINAL = 0;
        //  para saber los tipos de cuenta
        String Indice = "";
        String Auxiliar = "";
        //  para los saldos finales correspondientes al periodo
        Int32 Contador = 0;
        Double Suma_Saldo = 0.0;
        Double Suma_Saldo_Total = 0.0;
        Double Suma_Modificado = 0.0;
        Double Saldo = 0.0;
        DataTable Dt_Cuentas_Contables = new DataTable();
        try
        {
            Dt_Nueva.Columns.Add("CUENTA", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("MONTO", typeof(System.Double));

            if (Tipo_Reporte == 2)
            {
                Dt_Nueva.Columns.Add("NATURALEZA", typeof(System.String));
                Dt_Nueva.TableName = "Dt_Era_02";
            }
            else if (Tipo_Reporte == 3)
            {
                Dt_Nueva.Columns.Add("GASTO", typeof(System.Decimal));
                Dt_Nueva.TableName = "Dt_Era_03";
            }
            else
                Dt_Nueva.TableName = "Dt_Era_01";


            if (Tipo_Reporte == 1)
            {
                Rs_Consulta.P_Filtro1 = "41";
                Rs_Consulta.P_Filtro2 = "42";
                Dt_Cuentas = Rs_Consulta.Consulta_Cuentas();

                for (Int16 Contador_For = 0; Contador_For < Dt_Cuentas.Rows.Count; Contador_For++)
                {
                    if (Dt_Cuentas.Rows[Contador_For]["NIVEL_ID"].ToString() == "00006")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Cuentas.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 9);
                         Auxiliar = Indice.Substring(0, 2);
                        if (Auxiliar == "41")
                        {
                            Row_Nueva["CUENTA"] = Dt_Cuentas.Rows[Contador_For]["CUENTA"].ToString();
                            Row_Nueva["NOMBRE"] = Dt_Cuentas.Rows[Contador_For]["DESCRIPCION"].ToString();
                            Saldo = 0;
                            foreach (DataRow Registro in Dt_Consulta.Rows)
                            {
                                if (Indice == Registro["CUENTA"].ToString().Substring(0, 9))
                                {
                                    Suma_Saldo += Convert.ToDouble((Registro["SALDO_FINAL"].ToString()));
                                    Saldo += Convert.ToDouble((Registro["SALDO_FINAL"].ToString()));
                                }
                            }
                            Row_Nueva["MONTO"] = Saldo;
                            if (Saldo != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                        else
                        {
                            if (Contador == 0)
                            {
                                //para la cuenta final de la 41
                                Row_Nueva["CUENTA"] = "410000000";
                                Row_Nueva["NOMBRE"] = "INGRESOS DE GESTION";
                                Row_Nueva["MONTO"] = Suma_Saldo;
                                Suma_Saldo_Total += Suma_Saldo;
                                if (Suma_Saldo != 0)
                                {
                                    Dt_Nueva.Rows.Add(Row_Nueva);
                                }
                                Suma_Saldo = 0.0;
                                Row_Nueva = Dt_Nueva.NewRow();
                                Contador++;
                            }
                            Row_Nueva["CUENTA"] = Dt_Cuentas.Rows[Contador_For]["CUENTA"].ToString();
                            Row_Nueva["NOMBRE"] = Dt_Cuentas.Rows[Contador_For]["DESCRIPCION"].ToString();
                            Saldo = 0;
                            foreach (DataRow Registro in Dt_Consulta.Rows)
                            {
                                if (Indice == Registro["CUENTA"].ToString().Substring(0, 9))
                                {
                                    Suma_Saldo += Convert.ToDouble((Registro["SALDO_FINAL"].ToString()));
                                    Saldo += Convert.ToDouble((Registro["SALDO_FINAL"].ToString()));
                                }
                            }
                            Row_Nueva["MONTO"] = Saldo;
                            if (Saldo != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                    }
                }
                Row_Nueva = Dt_Nueva.NewRow();
                Row_Nueva["CUENTA"] = "420000000";
                Row_Nueva["NOMBRE"] = "PARTICIPACIONES, APORTACIONES, TRANSFERENCIAS, ASIGNACIONES, SUBSIDIOS Y OTRAS AYUDAS";
                Row_Nueva["MONTO"] = Suma_Saldo;
                if (Suma_Saldo != 0)
                {
                    Dt_Nueva.Rows.Add(Row_Nueva);
                }
                Suma_Saldo_Total += Suma_Saldo;
                Row_Nueva = Dt_Nueva.NewRow();
                Row_Nueva["CUENTA"] = "z";
                Row_Nueva["NOMBRE"] = "TOTAL";
                Row_Nueva["MONTO"] = Suma_Saldo_Total;
                if (Suma_Saldo_Total != 0)
                {
                    Dt_Nueva.Rows.Add(Row_Nueva);
                }
                Dt_Nueva.DefaultView.Sort ="CUENTA ASC";
                Dt_Final = Dt_Nueva.DefaultView.ToTable();
            }
            else if (Tipo_Reporte == 2)
            {
                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    if (Convert.ToDouble(Registro["SALDO_FINAL"].ToString()) != 0)
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Row_Nueva["MONTO"] = (Registro["SALDO_FINAL"].ToString());
                        Row_Nueva["NATURALEZA"] = "";
                        Dt_Nueva.Rows.Add(Row_Nueva);
                        Dt_Final = Dt_Nueva;
                    }
                }
            }
            else if (Tipo_Reporte == 3)
            {
                Rs_Consulta.P_Filtro1 = "5";
                Dt_Cuentas_Contables = Rs_Consulta.Consulta_Cuentas();
                Dt_Temporal = Dt_Consulta;
                for (Int32 Contador_For = 0; Contador_For < Dt_Cuentas_Contables.Rows.Count; Contador_For++)
                {
                    if (Dt_Cuentas_Contables.Rows[Contador_For]["NIVEL_ID"].ToString() == "00004")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        SALDO_FINAL = 0;
                        Saldo_Era_03 = 0;
                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 4) == Indice)
                            {
                                SALDO_FINAL = SALDO_FINAL + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                            }
                            Saldo_Era_03 = Saldo_Era_03 + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                        }
                        Row_Nueva["CUENTA"] = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        Row_Nueva["NOMBRE"] = Dt_Cuentas_Contables.Rows[Contador_For]["DESCRIPCION"].ToString();
                        Row_Nueva["MONTO"] = String.Format("{0:c}", (SALDO_FINAL)).Replace("$", "");
                        Row_Nueva["GASTO"] = (SALDO_FINAL * 100) / Saldo_Era_03;
                        if (SALDO_FINAL !=0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                }
                Dt_Final = Dt_Nueva;
            }
            return Dt_Final;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_EFE
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///             Tipo_Reporte.- establece que reporte es era
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  01/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_EFE(DataTable Dt_Consulta_Actual, int Tipo_Reporte,DataTable Dt_Consulta_Inicio)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataRow Row_Nueva ;
        String Indice="";
        Decimal Saldo_Inicial = 0;
        String Auxiliar = "";
        Decimal Suma_Saldo_Inicial=0;
        Decimal Suma_Saldo_Final = 0;
        Decimal Suma_Saldo_Total_Incial = 0;
        Decimal Suma_Saldo_Total_Final = 0;
        int Contador = 0;
        int Contador_2 = 0;
        int Contador_16 = 0;
        int Contador_3 = 0;
                        
        try
        {
            Dt_Nueva.Columns.Add("CUENTA", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.Decimal));

            if (Tipo_Reporte == 1)
            {
                Dt_Nueva.Columns.Add("SALDO_INICIAL", typeof(System.Decimal));
                Dt_Nueva.Columns.Add("SALDO_FINAL", typeof(System.Decimal));
                Dt_Nueva.TableName = "Dt_EFE_01";
            }
            else if (Tipo_Reporte == 2)
            {
                Dt_Nueva.Columns.Add("SUB", typeof(System.Decimal));
                Dt_Nueva.TableName = "Dt_EFE_02";
            }
            if (Tipo_Reporte == 1)
            {
                foreach (DataRow Registro in Dt_Consulta_Actual.Rows)
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    //   para la cuenta y descripcion
                    Indice = (Registro["CUENTA"].ToString());
                    Auxiliar = Indice.Substring(0, 4);

                    if (Convert.ToInt32(Auxiliar) == 1111)
                    {
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Saldo_Inicial = 0;
                        foreach (DataRow Fila in Dt_Consulta_Inicio.Rows)
                        {
                            if (Indice == Fila["CUENTA"].ToString())
                            {
                                Saldo_Inicial = Saldo_Inicial + Convert.ToDecimal((Fila["SALDO_FINAL"].ToString()));
                            }
                        }
                        Suma_Saldo_Inicial += Saldo_Inicial;
                        Suma_Saldo_Final += Convert.ToDecimal((Registro["SALDO_FINAL"].ToString()));
                        Row_Nueva["SALDO_INICIAL"] = Saldo_Inicial;
                        Row_Nueva["SALDO_FINAL"] = (Registro["SALDO_FINAL"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) - Saldo_Inicial;
                        if (Saldo_Inicial != 0 || Convert.ToDecimal(Registro["SALDO_FINAL"].ToString())!=0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    else if (Convert.ToInt32(Auxiliar) == 1112)
                    {

                        if (Contador == 0)
                        {
                            //para la cuenta final de la 41
                            Row_Nueva["CUENTA"] = "1111";
                            Row_Nueva["NOMBRE"] = "EFECTIVO";
                            Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                            Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                            Row_Nueva["FLUJO"] = Suma_Saldo_Final - Suma_Saldo_Inicial;
                            Suma_Saldo_Total_Incial += Suma_Saldo_Inicial;
                            Suma_Saldo_Total_Final += Suma_Saldo_Final;
                            Suma_Saldo_Inicial = 0;
                            Suma_Saldo_Final = 0;
                            Dt_Nueva.Rows.Add(Row_Nueva);
                            Row_Nueva = Dt_Nueva.NewRow();
                            Contador++;
                        }
                        //  para las cuentas 42
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Saldo_Inicial = 0;
                        foreach (DataRow Fila_2 in Dt_Consulta_Inicio.Rows)
                        {
                            if (Indice == Fila_2["CUENTA"].ToString())
                            {
                                Saldo_Inicial = Saldo_Inicial + Convert.ToDecimal((Fila_2["SALDO_FINAL"].ToString()));
                            }
                        }
                        Suma_Saldo_Inicial += Saldo_Inicial;
                        Suma_Saldo_Final += Convert.ToDecimal((Registro["SALDO_FINAL"].ToString()));
                        Row_Nueva["SALDO_INICIAL"] = Saldo_Inicial;
                        Row_Nueva["SALDO_FINAL"] = (Registro["SALDO_FINAL"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) - Saldo_Inicial;
                        if (Saldo_Inicial != 0 || Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    else if (Convert.ToInt32(Auxiliar) == 1114)
                    {

                        if (Contador_3 == 0)
                        {
                            //para la cuenta final de la 41
                            Row_Nueva["CUENTA"] = "1112";
                            Row_Nueva["NOMBRE"] = "BANCOS/TESORERIA";
                            Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                            Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                            Row_Nueva["FLUJO"] = Suma_Saldo_Final - Suma_Saldo_Inicial;
                            Suma_Saldo_Total_Incial += Suma_Saldo_Inicial;
                            Suma_Saldo_Total_Final += Suma_Saldo_Final;
                            Suma_Saldo_Inicial = 0;
                            Suma_Saldo_Final = 0;
                            Dt_Nueva.Rows.Add(Row_Nueva);
                            Row_Nueva = Dt_Nueva.NewRow();
                            Contador_3++;
                        }
                        //  para las cuentas 4
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Saldo_Inicial = 0;
                        foreach (DataRow Fila_2 in Dt_Consulta_Inicio.Rows)
                        {
                            if (Indice == Fila_2["CUENTA"].ToString())
                            {
                                Saldo_Inicial = Saldo_Inicial + Convert.ToDecimal((Fila_2["SALDO_FINAL"].ToString()));
                            }
                        }
                        Suma_Saldo_Inicial += Saldo_Inicial;
                        Suma_Saldo_Final += Convert.ToDecimal((Registro["SALDO_FINAL"].ToString()));
                        Row_Nueva["SALDO_INICIAL"] = Saldo_Inicial;
                        Row_Nueva["SALDO_FINAL"] = (Registro["SALDO_FINAL"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) - Saldo_Inicial;
                        if (Saldo_Inicial != 0 || Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    else if (Convert.ToInt32(Auxiliar) == 1115)
                    {

                        if (Contador_2 == 0)
                        {
                            //para la cuenta final de la 41
                            Row_Nueva["CUENTA"] = "1114";
                            Row_Nueva["NOMBRE"] = "INVERSIONES TEMPORALES(3 MESES)";
                            Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                            Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                            Row_Nueva["FLUJO"] = Suma_Saldo_Final - Suma_Saldo_Inicial;
                            Suma_Saldo_Total_Incial += Suma_Saldo_Inicial;
                            Suma_Saldo_Total_Final += Suma_Saldo_Final;
                            Suma_Saldo_Inicial = 0;
                            Suma_Saldo_Final = 0;
                            Dt_Nueva.Rows.Add(Row_Nueva);
                            Row_Nueva = Dt_Nueva.NewRow();
                            Contador_2++;
                        }
                        //  para las cuentas 5
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Saldo_Inicial = 0;
                        foreach (DataRow Fila_2 in Dt_Consulta_Inicio.Rows)
                        {
                            if (Indice == Fila_2["CUENTA"].ToString())
                            {
                                Saldo_Inicial = Saldo_Inicial + Convert.ToDecimal((Fila_2["SALDO_FINAL"].ToString()));
                            }
                        }
                        Suma_Saldo_Inicial += Saldo_Inicial;
                        Suma_Saldo_Final += Convert.ToDecimal((Registro["SALDO_FINAL"].ToString()));
                        Row_Nueva["SALDO_INICIAL"] = Saldo_Inicial;
                        Row_Nueva["SALDO_FINAL"] = (Registro["SALDO_FINAL"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) - Saldo_Inicial;
                        if (Saldo_Inicial != 0 || Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    else if (Convert.ToInt32(Auxiliar) == 1116)
                    {

                        if (Contador_16 == 0)
                        {
                            //para la cuenta final de la 41
                            Row_Nueva["CUENTA"] = "1115";
                            Row_Nueva["NOMBRE"] = "FONDOS CON AFECTACION ESPECIFICA";
                            Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                            Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                            Row_Nueva["FLUJO"] = Suma_Saldo_Final - Suma_Saldo_Inicial;
                            Suma_Saldo_Total_Incial += Suma_Saldo_Inicial;
                            Suma_Saldo_Total_Final += Suma_Saldo_Final;
                            if (Suma_Saldo_Inicial != 0 || Suma_Saldo_Final != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                            Suma_Saldo_Inicial = 0;
                            Suma_Saldo_Final = 0;
                            Row_Nueva = Dt_Nueva.NewRow();
                            Contador_16++;
                        }
                        //  para las cuentas 6
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Saldo_Inicial = 0;
                        foreach (DataRow Fila_2 in Dt_Consulta_Inicio.Rows)
                        {
                            if (Indice == Fila_2["CUENTA"].ToString())
                            {
                                Saldo_Inicial = Saldo_Inicial + Convert.ToDecimal((Fila_2["SALDO_FINAL"].ToString()));
                            }
                        }
                        Suma_Saldo_Inicial += Saldo_Inicial;
                        Suma_Saldo_Final += Convert.ToDecimal((Registro["SALDO_FINAL"].ToString()));
                        Row_Nueva["SALDO_INICIAL"] = Saldo_Inicial;
                        Row_Nueva["SALDO_FINAL"] = (Registro["SALDO_FINAL"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) - Saldo_Inicial;
                        if (Saldo_Inicial != 0 || Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                }
                if (Contador == 0)
                {
                    //para la cuenta final de la 41
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1111";
                    Row_Nueva["NOMBRE"] = "EFECTIVO";
                    Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                    Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                    Row_Nueva["FLUJO"] = Suma_Saldo_Final - Suma_Saldo_Inicial;
                    Suma_Saldo_Total_Incial += Suma_Saldo_Inicial;
                    Suma_Saldo_Total_Final += Suma_Saldo_Final;
                    if (Suma_Saldo_Inicial != 0 || Suma_Saldo_Final != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Suma_Saldo_Inicial = 0;
                    Suma_Saldo_Final = 0;
                    Row_Nueva = Dt_Nueva.NewRow();
                    Contador++;
                }
                if (Contador_3 == 0)
                {
                    //para la cuenta final de la 41
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1112";
                    Row_Nueva["NOMBRE"] = "BANCOS/TESORERIA";
                    Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                    Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                    Row_Nueva["FLUJO"] = Suma_Saldo_Final - Suma_Saldo_Inicial;
                    Suma_Saldo_Total_Incial += Suma_Saldo_Inicial;
                    Suma_Saldo_Total_Final += Suma_Saldo_Final;
                    if (Suma_Saldo_Inicial != 0 || Suma_Saldo_Final != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Suma_Saldo_Inicial = 0;
                    Suma_Saldo_Final = 0;
                    Row_Nueva = Dt_Nueva.NewRow();
                    Contador_3++;
                }
                if (Contador_2 == 0)
                {
                    //para la cuenta final de la 41
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1114";
                    Row_Nueva["NOMBRE"] = "INVERSIONES TEMPORALES(3 MESES)";
                    Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                    Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                    Row_Nueva["FLUJO"] = Suma_Saldo_Final - Suma_Saldo_Inicial;
                    Suma_Saldo_Total_Incial += Suma_Saldo_Inicial;
                    Suma_Saldo_Total_Final += Suma_Saldo_Final;
                    if (Suma_Saldo_Inicial != 0 || Suma_Saldo_Final != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Suma_Saldo_Inicial = 0;
                    Suma_Saldo_Final = 0;
                    Row_Nueva = Dt_Nueva.NewRow();
                    Contador_2++;
                }
                if (Contador_16 == 0)
                {
                    //para la cuenta final de la 41
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1115";
                    Row_Nueva["NOMBRE"] = "FONDOS CON AFECTACION ESPECIFICA";
                    Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                    Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                    Row_Nueva["FLUJO"] = Suma_Saldo_Final - Suma_Saldo_Inicial;
                    Suma_Saldo_Total_Incial += Suma_Saldo_Inicial;
                    Suma_Saldo_Total_Final += Suma_Saldo_Final;
                    if (Suma_Saldo_Inicial != 0 || Suma_Saldo_Final != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Suma_Saldo_Inicial = 0;
                    Suma_Saldo_Final = 0;
                    Contador_16++;
                }
                if (Suma_Saldo_Inicial!=0)
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1116";
                    Row_Nueva["NOMBRE"] = "DEPOSITOS DE FONDOS DE TERCEROS";
                    Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Inicial;
                    Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                    Row_Nueva["FLUJO"] = Suma_Saldo_Final - Suma_Saldo_Inicial;
                    Suma_Saldo_Total_Incial += Suma_Saldo_Inicial;
                    Suma_Saldo_Total_Final += Suma_Saldo_Final;
                    if (Suma_Saldo_Inicial != 0 || Suma_Saldo_Final != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Suma_Saldo_Inicial = 0;
                    Suma_Saldo_Final = 0;
                }

                Row_Nueva = Dt_Nueva.NewRow();
                Row_Nueva["CUENTA"] = " ";
                Row_Nueva["NOMBRE"] = "TOTAL";
                Row_Nueva["SALDO_INICIAL"] = Suma_Saldo_Total_Incial;
                Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Total_Final;
                Row_Nueva["FLUJO"] = Suma_Saldo_Total_Final - Suma_Saldo_Total_Incial;
                if (Suma_Saldo_Total_Incial != 0 || Suma_Saldo_Total_Final != 0)
                {
                    Dt_Nueva.Rows.Add(Row_Nueva);
                }
                Dt_Final = Dt_Nueva;
            }
            else if (Tipo_Reporte == 2)
            {
                Decimal SUMA_SALDO_1235 = 0;
                Decimal SUMA_SALDO_1236 = 0;
                Decimal SUMA_SALDO_1241 = 0;
                Decimal SUMA_SALDO_1242 = 0;
                Decimal SUMA_SALDO_1243 = 0;
                Decimal SUMA_SALDO_1244 = 0;
                Decimal SUMA_SALDO_1246 = 0;
                Decimal Suma_Saldo_Total_Flujo = 0;
                 Contador = 0;
                 Contador_2 = 0;
                 int Contador_1 = 0;
                 int Contador_4 = 0;
                 int Contador_12 = 0;
                 Contador_3 = 0;

                foreach (DataRow Registro in Dt_Consulta_Actual.Rows)
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    //   para la cuenta y descripcion
                    Indice = (Registro["CUENTA"].ToString());
                    Auxiliar = Indice.Substring(0, 4);

                    if (Convert.ToInt32(Auxiliar) == 1235)
                    {
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        SUMA_SALDO_1235 = SUMA_SALDO_1235 + Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        if (Convert.ToDecimal(Registro["SALDO_FINAL"].ToString())!=0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    else if (Convert.ToInt32(Auxiliar) == 1236)
                    {
                        if (Contador_12 == 0)
                        {
                            //para la cuenta final de la 41
                            Row_Nueva["CUENTA"] = "1235";
                            Row_Nueva["NOMBRE"] = "CONSTR. EN PROCESO DOMINIO PUBLICO";
                            Row_Nueva["FLUJO"] = SUMA_SALDO_1235;
                            Suma_Saldo_Total_Flujo += SUMA_SALDO_1235;
                            if (Suma_Saldo_Total_Flujo != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                            Row_Nueva = Dt_Nueva.NewRow();
                            Contador_12++;
                        }
                        //  para las cuentas 1236
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        SUMA_SALDO_1236 = SUMA_SALDO_1236 + Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        if (Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    else if (Convert.ToInt32(Auxiliar) == 1241)
                    {
                        if (Contador == 0)
                        {
                            //para la cuenta final de la 41
                            Row_Nueva["CUENTA"] = "1236";
                            Row_Nueva["NOMBRE"] = "CONSTR. EN PROCESO BIENES PROPIOS";
                            Row_Nueva["FLUJO"] = SUMA_SALDO_1236;
                            Suma_Saldo_Total_Flujo += SUMA_SALDO_1236;
                            if (SUMA_SALDO_1236 != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                            Row_Nueva = Dt_Nueva.NewRow();
                            //para la cuenta final de la 41
                            Row_Nueva["CUENTA"] = "";
                            Row_Nueva["NOMBRE"] = "INMUEBLES";
                            Row_Nueva["FLUJO"] = SUMA_SALDO_1236 + SUMA_SALDO_1235;
                            if ((SUMA_SALDO_1236 + SUMA_SALDO_1235) != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                            Row_Nueva = Dt_Nueva.NewRow();
                            Contador++;
                        }
                        //  para las cuentas 4
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()); 
                        SUMA_SALDO_1241= SUMA_SALDO_1241 + Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        if (Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    else if (Convert.ToInt32(Auxiliar) == 1242)
                    {
                        if (Contador_1 == 0)
                        {
                            //para la cuenta final de la 41
                            Row_Nueva["CUENTA"] = "1241";
                            Row_Nueva["NOMBRE"] = "MOBILIARIO Y EQ. DE ADMINISTRACION";
                            Row_Nueva["FLUJO"] = SUMA_SALDO_1241;
                            Suma_Saldo_Total_Flujo += SUMA_SALDO_1241;
                            if (SUMA_SALDO_1241 != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                            Row_Nueva = Dt_Nueva.NewRow();
                            Contador_1++;
                        }
                        //  para las cuentas 5
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        SUMA_SALDO_1242= SUMA_SALDO_1242 + Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        if (Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    else if (Convert.ToInt32(Auxiliar) == 1243)
                    {
                        if (Contador_2 == 0)
                        {
                            //para la cuenta final de la 42
                            Row_Nueva["CUENTA"] = "1242";
                            Row_Nueva["NOMBRE"] = "MOBILIARIO Y EQ. EDUCACIONAL Y REC.";
                            Row_Nueva["FLUJO"] = SUMA_SALDO_1242;
                            Suma_Saldo_Total_Flujo += SUMA_SALDO_1242;
                            if (SUMA_SALDO_1242 != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                            Row_Nueva = Dt_Nueva.NewRow();
                            Contador_2++;
                        }
                        //  para las cuentas 43
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        SUMA_SALDO_1243 = SUMA_SALDO_1243 + Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        if (Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    else if (Convert.ToInt32(Auxiliar) == 1244)
                    {
                        if (Contador_3 == 0)
                        {
                            //para la cuenta final de la 43
                            Row_Nueva["CUENTA"] = "1243";
                            Row_Nueva["NOMBRE"] = "EQ. E INSTRUMENTAL MEDICO Y DE LAB.";
                            Row_Nueva["FLUJO"] = SUMA_SALDO_1243;
                            Suma_Saldo_Total_Flujo += SUMA_SALDO_1243;
                            if (SUMA_SALDO_1243 != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                            Row_Nueva = Dt_Nueva.NewRow();
                            Contador_3++;
                        }
                        //  para las cuentas 44
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        SUMA_SALDO_1244 = SUMA_SALDO_1244 + Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        if (Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    else if (Convert.ToInt32(Auxiliar) == 1246)
                    {
                        if (Contador_4 == 0)
                        {
                            //para la cuenta final de la 42
                            Row_Nueva["CUENTA"] = "1244";
                            Row_Nueva["NOMBRE"] = "EQUIPO DE TRANSPORTE";
                            Row_Nueva["FLUJO"] = SUMA_SALDO_1244;
                            Suma_Saldo_Total_Flujo += SUMA_SALDO_1244;
                            if (SUMA_SALDO_1244 != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                            Row_Nueva = Dt_Nueva.NewRow();
                            Contador_4++;
                        }
                        //  para las cuentas 46
                        Row_Nueva["CUENTA"] = (Registro["CUENTA"].ToString());
                        Row_Nueva["NOMBRE"] = (Registro["DESCRIPCION"].ToString());
                        Row_Nueva["FLUJO"] = Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        SUMA_SALDO_1246 = SUMA_SALDO_1246 + Convert.ToDecimal(Registro["SALDO_FINAL"].ToString());
                        if (Convert.ToDecimal(Registro["SALDO_FINAL"].ToString()) != 0)
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                }
                if (Contador_12 == 0)
                {
                    //para la cuenta final de la 41
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1235";
                    Row_Nueva["NOMBRE"] = "CONSTR. EN PROCESO DOMINIO PUBLICO";
                    Row_Nueva["FLUJO"] = SUMA_SALDO_1235;
                    Suma_Saldo_Total_Flujo += SUMA_SALDO_1235;
                    if (SUMA_SALDO_1235 != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Row_Nueva = Dt_Nueva.NewRow();
                    Contador_12++;
                }
                if (Contador == 0)
                {
                    //para la cuenta final de la 41
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1236";
                    Row_Nueva["NOMBRE"] = "CONSTR. EN PROCESO BIENES PROPIOS";
                    Row_Nueva["FLUJO"] = SUMA_SALDO_1236;
                    Suma_Saldo_Total_Flujo += SUMA_SALDO_1236;
                    if (SUMA_SALDO_1236 != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Row_Nueva = Dt_Nueva.NewRow();
                    //para la cuenta final de la 41
                    Row_Nueva["CUENTA"] = "";
                    Row_Nueva["NOMBRE"] = "INMUEBLES";
                    Row_Nueva["FLUJO"] = SUMA_SALDO_1236 + SUMA_SALDO_1235;
                    if ((SUMA_SALDO_1236 + SUMA_SALDO_1235) != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Row_Nueva = Dt_Nueva.NewRow();
                    Contador++;
                }
                if (Contador_1 == 0)
                {
                    //para la cuenta final de la 41
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1241";
                    Row_Nueva["NOMBRE"] = "MOBILIARIO Y EQ. DE ADMINISTRACION";
                    Row_Nueva["FLUJO"] = SUMA_SALDO_1241;
                    Suma_Saldo_Total_Flujo += SUMA_SALDO_1241;
                    if (SUMA_SALDO_1241 != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Row_Nueva = Dt_Nueva.NewRow();
                    Contador_1++;
                }
                if (Contador_2 == 0)
                {
                    //para la cuenta final de la 42
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1242";
                    Row_Nueva["NOMBRE"] = "MOBILIARIO Y EQ. EDUCACIONAL Y REC.";
                    Row_Nueva["FLUJO"] = SUMA_SALDO_1242;
                    Suma_Saldo_Total_Flujo += SUMA_SALDO_1242;
                    if (SUMA_SALDO_1242 != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Row_Nueva = Dt_Nueva.NewRow();
                    Contador_2++;
                }
                if (Contador_3 == 0)
                {
                    //para la cuenta final de la 43
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1243";
                    Row_Nueva["NOMBRE"] = "EQ. E INSTRUMENTAL MEDICO Y DE LAB.";
                    Row_Nueva["FLUJO"] = SUMA_SALDO_1243;
                    Suma_Saldo_Total_Flujo += SUMA_SALDO_1243;
                    if (SUMA_SALDO_1243 != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Row_Nueva = Dt_Nueva.NewRow();
                    Contador_3++;
                }
                if (Contador_4 == 0)
                {
                    //para la cuenta final de la 42
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1244";
                    Row_Nueva["NOMBRE"] = "EQUIPO DE TRANSPORTE";
                    Row_Nueva["FLUJO"] = SUMA_SALDO_1244;
                    Suma_Saldo_Total_Flujo += SUMA_SALDO_1244;
                    if (SUMA_SALDO_1244 != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                    Row_Nueva = Dt_Nueva.NewRow();
                    Contador_4++;
                }
                if (SUMA_SALDO_1246 != 0)
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Row_Nueva["CUENTA"] = "1246";
                    Row_Nueva["NOMBRE"] = "MAQUINARIA, OTROS EQUIPOS Y HERR.";
                    Row_Nueva["FLUJO"] = SUMA_SALDO_1246;
                    Suma_Saldo_Total_Flujo += SUMA_SALDO_1246;
                    if (SUMA_SALDO_1246 != 0)
                    {
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
                }
                Row_Nueva = Dt_Nueva.NewRow();
                Row_Nueva["CUENTA"] = " ";
                Row_Nueva["NOMBRE"] = "MUEBLES";
                Row_Nueva["FLUJO"] = SUMA_SALDO_1241+SUMA_SALDO_1242+SUMA_SALDO_1243+SUMA_SALDO_1244+SUMA_SALDO_1246;
                if ((SUMA_SALDO_1241 + SUMA_SALDO_1242 + SUMA_SALDO_1243 + SUMA_SALDO_1244 + SUMA_SALDO_1246) != 0)
                {
                    Dt_Nueva.Rows.Add(Row_Nueva);
                }
                Row_Nueva = Dt_Nueva.NewRow();
                Row_Nueva["CUENTA"] = " ";
                Row_Nueva["NOMBRE"] = "TOTAL";
                Row_Nueva["FLUJO"] = Suma_Saldo_Total_Flujo;
                if (Suma_Saldo_Total_Flujo != 0)
                {
                    Dt_Nueva.Rows.Add(Row_Nueva);
                }
                Dt_Final = Dt_Nueva;
            }
            return Dt_Final;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Vhp_01
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///             Dt_Periodo_anterior.- son las cuentas del periodo anterior que se calcularan
    ///             Tipo_Reporte.- establece que reporte es vhp
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  24/Febrero/2012
    ///MODIFICO: Sergio Manuel Gallardo Andrade
    ///FECHA_MODIFICO:04/Abril/2013
    ///CAUSA_MODIFICACIÓN:Se modifico debido a que los reportes de la conac cambiaron y la informacion no era la correcta
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Vhp_01(DataTable Dt_Consulta, DataTable Dt_Periodo_Anterior, int Tipo_Reporte)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Cuentas_Contables = new DataTable();
        DataRow Row_Nueva;
        //  para saber los tipos de cuenta
        String Indice = "";
        String Auxiliar = "";
        //  para los saldos finales correspondientes al periodo
        Decimal Suma_Saldo = 0;
        Decimal Suma_Saldo_Final = 0;
        Double Suma_Saldo_Actual = 0.0;
        Double Suma_Modificado = 0.0;
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta_Situacion_Financiera = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Periodo_Anterior_Tem = new DataTable(); //Variable a conter los valores a pasar al reporte
        DataTable Dt_Consulta_Tem = new DataTable();
        
        try
        {
            Dt_Nueva.Columns.Add("CUENTA", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_INICIAL", typeof(System.Double));
            Dt_Nueva.Columns.Add("SALDO_FINAL", typeof(System.Double));
            Dt_Nueva.Columns.Add("MODIFICACION", typeof(System.Double));
            if (Tipo_Reporte == 1)
                Dt_Nueva.Columns.Add("TIPO", typeof(System.String));

            Dt_Nueva.Columns.Add("NATURALEZA", typeof(System.String));
            Dt_Nueva.TableName = "Dt_Vhp_01";
            if (Tipo_Reporte == 1)
            {
                Rs_Consulta.P_Filtro1 = "31";
            }
            else
            {
                Rs_Consulta.P_Filtro1 = "32";
            }
            Dt_Cuentas_Contables = Rs_Consulta.Consulta_Cuentas();
            for (Int32 Contador_For = 0; Contador_For < Dt_Cuentas_Contables.Rows.Count; Contador_For++)
            {
                    if (Dt_Cuentas_Contables.Rows[Contador_For]["NIVEL_ID"].ToString() == "00006")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Cuentas_Contables.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 9);
                        Suma_Saldo = 0;
                        Suma_Saldo_Final = 0;
                        if (Indice.Substring(0, 4) == "3210")
                            {
                                if (Convert.ToInt16(Cmb_Mes.SelectedValue.Substring(0, 2)) == 1)
                                {
                                    Rs_Consulta_Situacion_Financiera.P_Mes_Año = "13" + String.Format("{0:00}", Convert.ToInt16(Cmb_Mes.SelectedValue.Substring(2, 2)) - 1);
                                    Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                                    Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                                    Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                                    Dt_Consulta_Tem = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                                }
                                else
                                {
                                    Rs_Consulta_Situacion_Financiera.P_Mes_Año = Cmb_Mes.SelectedValue.ToString();
                                    Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                                    Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                                    Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                                    Dt_Consulta_Tem = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                                }
                                //  para el periodo de enero
                                Dt_Periodo_Anterior_Tem = new DataTable();
                                Rs_Consulta_Situacion_Financiera.P_Mes_Año = "12" + Convert.ToString(Convert.ToDouble(Cmb_Anio.SelectedItem.Text.ToString()) - 1).Substring(2, 2);
                                Rs_Consulta_Situacion_Financiera.P_Filtro1 = "4";
                                Rs_Consulta_Situacion_Financiera.P_Filtro2 = "5";
                                Rs_Consulta_Situacion_Financiera.P_Filtro3 = null;
                                Dt_Periodo_Anterior_Tem = Rs_Consulta_Situacion_Financiera.Consulta_Situacion_Financiera();
                                //  para construir la tabla
                                Dt_Consulta_Tem = Generar_Tabla_Estado_Actividades(Dt_Consulta_Tem, Dt_Periodo_Anterior_Tem);
                                Suma_Saldo_Final = Suma_Saldo_Final + Convert.ToDecimal(Session["3210_Actual"].ToString());
                            }
                        foreach (DataRow Fila in Dt_Periodo_Anterior.Rows)
                        {
                                if (Fila["CUENTA"].ToString().Substring(0, 9) == Indice)
                                {
                                    Suma_Saldo = Suma_Saldo + Convert.ToDecimal(Fila["SALDO_FINAL"].ToString());
                                }
                        }
                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 9) == Indice)
                            {
                                Suma_Saldo_Final = Suma_Saldo_Final + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        Row_Nueva["CUENTA"] = Indice;
                        Row_Nueva["NOMBRE"] = Dt_Cuentas_Contables.Rows[Contador_For]["DESCRIPCION"].ToString();
                        //  para el saldo anterior
                        Row_Nueva["SALDO_INICIAL"] = Suma_Saldo;
                        //  para el saldo final correspondiente al mes
                        Row_Nueva["SALDO_FINAL"] = Suma_Saldo_Final;
                        //  para el saldo modficicado
                        Row_Nueva["MODIFICACION"] = Suma_Saldo_Final - Suma_Saldo;

                        if (Tipo_Reporte == 1)
                            Row_Nueva["TIPO"] = "";

                        Row_Nueva["NATURALEZA"] = "";
                        Dt_Nueva.Rows.Add(Row_Nueva);
                    }
            }
            Dt_Nueva.DefaultView.RowFilter = "SALDO_INICIAL<>'0.00' OR SALDO_FINAL <>'0.00'";
            return Dt_Nueva.DefaultView.ToTable();
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Estado_Actividades
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Mes_Actual.- el numero del mes quq se esta reportando
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  23/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Estado_Actividades(DataTable Dt_Consulta,DataTable Dt_Consulta_Anterior)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Periodo_Anterior = new DataTable();
        DataRow Row_Nueva;
        String Indice = "";
        String Año_Actual = "";
        int Contador_For = 0;
        Decimal Suma_4000 = 0;
        Decimal Suma_5000 = 0;
        Decimal Suma_4000_Ant = 0;
        Decimal Suma_5000_Ant = 0;
        DataTable Dt_Temp_Consulta = new DataTable();
        Decimal SALDO = 0;
        Decimal SALDO_INICIAL = 0;

        try
        {
            Dt_Nueva.Columns.Add("INDICE", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("PERIODO_ACTUAL", typeof(System.String));
            Dt_Nueva.Columns.Add("PERIODO_ANTERIOR", typeof(System.String));
            Dt_Nueva.Columns.Add("NOTA", typeof(System.String));
            Dt_Nueva.TableName = "Dt_Estado_Actividades";

            Rs_Consulta.P_Filtro1 = "4";
            Rs_Consulta.P_Filtro2 = "5";
            Dt_Cuentas = Rs_Consulta.Consulta_Cuentas();

            //  para sacar el año que se reportara
            Año_Actual = Cmb_Anio.SelectedValue;
            Año_Actual = Año_Actual.Substring(2, 2);
            for (int i = 4; i < 6; i++)
            {
                Dt_Temp_Consulta = new DataTable();
                Dt_Temp_Consulta.Columns.Add("CUENTA", typeof(System.String));
                Dt_Temp_Consulta.Columns.Add("NOMBRE", typeof(System.String));
                Dt_Temp_Consulta.Columns.Add("NIVEL_ID", typeof(System.String));
                DataRow row;
                foreach (DataRow Renglon in Dt_Cuentas.Rows)
                {
                    if (Renglon["CUENTA"].ToString().Substring(0, 1) == i.ToString())
                    {
                        row = Dt_Temp_Consulta.NewRow();
                        row["CUENTA"] = Renglon["CUENTA"].ToString();
                        row["NIVEL_ID"] = Renglon["NIVEL_ID"].ToString();
                        row["NOMBRE"] = Renglon["DESCRIPCION"].ToString();
                        Dt_Temp_Consulta.Rows.Add(row);
                        Dt_Temp_Consulta.AcceptChanges();
                    }
                }
                //  se agregaran los saldos de las cuentas 
                for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
                {
                    if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00004")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        SALDO = 0;
                        SALDO_INICIAL = 0;
                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 4) == Indice)
                            {
                                SALDO = SALDO + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 4) == Indice)
                            {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                        Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", (SALDO)).Replace("$", "");
                        Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        //  para las notas
                        if (Indice == "41" || Indice == "42")
                            Row_Nueva["NOTA"] = "ERA-01";

                        else if (Indice == "43")
                            Row_Nueva["NOTA"] = "ERA-02";

                        else if (Indice == "5")
                            Row_Nueva["NOTA"] = "ERA-03";
                        else
                            Row_Nueva["NOTA"] = "";

                        //Verificar si se tiene que colocar el registro
                        if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                        {
                            //Verificar al condicion
                            if (Convert.ToDecimal(Row_Nueva["PERIODO_ACTUAL"]) != 0 || Convert.ToDecimal(Row_Nueva["PERIODO_ANTERIOR"]) != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                        else
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00003")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 3);
                        SALDO = 0;
                        SALDO_INICIAL = 0;
                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 3) == Indice)
                            {
                                SALDO = SALDO + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 3) == Indice)
                            {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                        Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", (SALDO )).Replace("$", "");
                        Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        //  para las notas
                        if (Indice == "41" || Indice == "42")
                            Row_Nueva["NOTA"] = "ERA-01";

                        else if (Indice == "43")
                            Row_Nueva["NOTA"] = "ERA-02";

                        else if (Indice == "5")
                            Row_Nueva["NOTA"] = "ERA-03";
                        else
                            Row_Nueva["NOTA"] = "";
                        //Verificar si se tiene que colocar el registro
                        if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                        {
                            //Verificar al condicion
                            if (Convert.ToDecimal(Row_Nueva["PERIODO_ACTUAL"]) != 0 || Convert.ToDecimal(Row_Nueva["PERIODO_ANTERIOR"]) != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                        else
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }

                    }
                    if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00002")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 2);
                        SALDO = 0;
                        SALDO_INICIAL = 0;
                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 2) == Indice)
                            {
                                SALDO = SALDO + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 2) == Indice)
                            {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                        Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", (SALDO)).Replace("$", "");
                        Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        //  para las notas
                        if (Indice == "41" || Indice == "42")
                            Row_Nueva["NOTA"] = "ERA-01";

                        else if (Indice == "43")
                            Row_Nueva["NOTA"] = "ERA-02";

                        else if (Indice == "5")
                            Row_Nueva["NOTA"] = "ERA-03";
                        else
                            Row_Nueva["NOTA"] = "";
                        //Verificar si se tiene que colocar el registro
                        if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                        {
                            //Verificar al condicion
                            if (Convert.ToDecimal(Row_Nueva["PERIODO_ACTUAL"]) != 0 || Convert.ToDecimal(Row_Nueva["PERIODO_ANTERIOR"]) != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                        else
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }

                    if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00001")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 1);
                        SALDO = 0;
                        SALDO_INICIAL = 0;
                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 1) == Indice)
                            {
                                SALDO = SALDO + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 1) == Indice)
                            {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDecimal(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                        Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", (SALDO )).Replace("$", "");
                        Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        if (Indice == "4")
                        {
                            Suma_4000 = Suma_4000 + SALDO;
                            Suma_4000_Ant = Suma_4000_Ant + SALDO_INICIAL;
                        }
                        else
                        {
                            Suma_5000 = Suma_5000 + SALDO;
                            Suma_5000_Ant = Suma_5000_Ant + SALDO_INICIAL;
                        }
                        //  para las notas
                        if (Indice == "41" || Indice == "42")
                            Row_Nueva["NOTA"] = "ERA-01";

                        else if (Indice == "43")
                            Row_Nueva["NOTA"] = "ERA-02";

                        else if (Indice == "5")
                            Row_Nueva["NOTA"] = "ERA-03";
                        else
                            Row_Nueva["NOTA"] = "";
                        //Verificar si se tiene que colocar el registro
                        if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                        {
                            //Verificar al condicion
                            if (Convert.ToDecimal(Row_Nueva["PERIODO_ACTUAL"]) != 0 || Convert.ToDecimal(Row_Nueva["PERIODO_ANTERIOR"]) != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                        else
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                } 
            }
            Row_Nueva = Dt_Nueva.NewRow();
            Row_Nueva["INDICE"] = "3210";
            Row_Nueva["NOMBRE"] = "RESULTADOS DEL EJERCICIO:(AHORRO/DESAHORRO)";
            Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", (Suma_4000 - Suma_5000)).Replace("$", "");
            Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", (Suma_4000_Ant - Suma_5000_Ant)).Replace("$", "");
            Row_Nueva["NOTA"] = "";
            Session["3210_Actual"]=String.Format("{0:c}", (Suma_4000 - Suma_5000)).Replace("$", "");
            Session["3210_Anterior"] = String.Format("{0:c}", (Suma_4000_Ant - Suma_5000_Ant)).Replace("$", "");
            //Verificar si se tiene que colocar el registro
            if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
            {
                //Verificar al condicion
                if (Convert.ToDecimal(Row_Nueva["PERIODO_ACTUAL"]) != 0 || Convert.ToDecimal(Row_Nueva["PERIODO_ANTERIOR"]) != 0)
                {
                    Dt_Nueva.Rows.Add(Row_Nueva);
                }
            }
            else
            {
                Dt_Nueva.Rows.Add(Row_Nueva);
            }
            return Dt_Nueva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Final
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  22/Febrero/2012
    ///MODIFICO:sergio Manuel Gallardo Andrade  
    ///FECHA_MODIFICO:20/marzo/2013
    ///CAUSA_MODIFICACIÓN:la conac cambio el reporte por lo que se modifico la informacion
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Final(DataTable Dt_Consulta, DataTable Dt_Consulta_Anterior, String Actual_3210, String Anterior_3210)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataRow Row_Nueva;
        String Indice = "";
        int Contador_For = 0;
        DataTable Dt_Temp_Consulta = new DataTable();
        Double SALDO = 0;
        Double SALDO_INICIAL = 0;
        try
        {
            Dt_Nueva.Columns.Add("INDICE", typeof(System.String));
            Dt_Nueva.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Nueva.Columns.Add("PERIODO_ACTUAL", typeof(System.String));
            Dt_Nueva.Columns.Add("PERIODO_ANTERIOR", typeof(System.String));
            //Dt_Nueva.Columns.Add("ACUMULADO", typeof(System.Double));
            Dt_Nueva.Columns.Add("NOTA", typeof(System.String));
            Dt_Nueva.TableName = "Dt_Sin_Fin";

            Rs_Consulta.P_Filtro1 = "1";
            Rs_Consulta.P_Filtro2 = "2";
            Rs_Consulta.P_Filtro3 = "3";
            
            Dt_Cuentas = Rs_Consulta.Consulta_Cuentas();
            for (int i = 1; i < 4; i++)
            {
                Dt_Temp_Consulta = new DataTable();
                Dt_Temp_Consulta.Columns.Add("CUENTA", typeof(System.String));
                Dt_Temp_Consulta.Columns.Add("NOMBRE", typeof(System.String));
                Dt_Temp_Consulta.Columns.Add("NIVEL_ID", typeof(System.String));
                DataRow row;
                foreach (DataRow Renglon in Dt_Cuentas.Rows)
                {
                    if (Renglon["CUENTA"].ToString().Substring(0, 1) == i.ToString())
                    {
                        row = Dt_Temp_Consulta.NewRow();
                        row["CUENTA"] = Renglon["CUENTA"].ToString();  
                        row["NIVEL_ID"] = Renglon["NIVEL_ID"].ToString();
                        row["NOMBRE"] = Renglon["DESCRIPCION"].ToString();
                        Dt_Temp_Consulta.Rows.Add(row);
                        Dt_Temp_Consulta.AcceptChanges();
                    }
                }
                for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
                {
                    if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00004")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        SALDO = 0;
                        SALDO_INICIAL = 0;
                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 4) == Indice)
                            {
                                SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 4) == Indice)
                            {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                        Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", SALDO).Replace("$", "");
                        Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        //  para las notas
                        if (Indice == "1114" || Indice == "1115" || Indice == "1121" || Indice == "1211")
                            Row_Nueva["NOTA"] = "ESF-01";
                        else if (Indice == "1122" || Indice == "1124")
                            Row_Nueva["NOTA"] = "ESF-02";
                        else if (Indice == "1123" || Indice == "1125")
                            Row_Nueva["NOTA"] = "ESF-03";
                        else if (Indice == "1140" || Indice == "1150")
                            Row_Nueva["NOTA"] = "ESF-05";
                        else if (Indice == "1213")
                            Row_Nueva["NOTA"] = "ESF-06";
                        else if (Indice == "1214")
                            Row_Nueva["NOTA"] = "ESF-07";
                        else if (Indice == "1230" || Indice == "1240" || Indice == "1261" || Indice == "1262" || Indice == "1263" || Indice == "1264")
                            Row_Nueva["NOTA"] = "ESF-08";
                        else if (Indice == "1250" || Indice == "1265" || Indice == "1270")
                            Row_Nueva["NOTA"] = "ESF-09";
                        else if (Indice == "1290")
                            Row_Nueva["NOTA"] = "ESF-11";
                        else if (Indice == "2110" || Indice == "2111" || Indice == "2112" || Indice == "2113" || Indice == "2114" || Indice == "2115" ||
                            Indice == "2116" || Indice == "2117" || Indice == "2118" || Indice == "2119" || Indice == "2120" ||
                            Indice == "2121" || Indice == "2122" || Indice == "2119" || Indice == "2129")
                            Row_Nueva["NOTA"] = "ESF-12";
                        else if (Indice == "2159" || Indice == "2160" || Indice == "2240")
                            Row_Nueva["NOTA"] = "ESF-13";
                        else if (Indice == "2199" || Indice == "2240")
                            Row_Nueva["NOTA"] = "ESF-14";
                        else if (Indice == "2130" || Indice == "2230")
                            Row_Nueva["NOTA"] = "ESF-15";
                        else if (Indice == "3100")
                            Row_Nueva["NOTA"] = "VHP-01";
                        else if (Indice == "3200")
                            Row_Nueva["NOTA"] = "VHP-02";
                        else
                            Row_Nueva["NOTA"] = "";

                        //Verificar si se tiene que colocar el registro
                        if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                        {
                            //Verificar al condicion
                            if (Convert.ToDecimal(Row_Nueva["PERIODO_ACTUAL"]) != 0 || Convert.ToDecimal(Row_Nueva["PERIODO_ANTERIOR"]) != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                        else
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }

                    }
                    if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00003")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 3);
                        SALDO = 0;
                        SALDO_INICIAL = 0;
                        if (Indice != "321")
                        {
                            foreach (DataRow Renglon in Dt_Consulta.Rows)
                            {
                                if (Renglon["CUENTA"].ToString().Substring(0, 3) == Indice)
                                {
                                    SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                                }
                            }
                            foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                            {
                                if (Renglon["CUENTA"].ToString().Substring(0, 3) == Indice)
                                {
                                    SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                                }
                            }
                            Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                            Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                            Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", SALDO).Replace("$", "");
                            Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        }
                        else
                        {
                            Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                            Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                            Row_Nueva["PERIODO_ACTUAL"] = Actual_3210;
                            Row_Nueva["PERIODO_ANTERIOR"] = Anterior_3210;
                        }
                        if (Indice == "1114" || Indice == "1115" || Indice == "1121" || Indice == "1211")
                            Row_Nueva["NOTA"] = "ESF-01";
                        else if (Indice == "1122" || Indice == "1124")
                            Row_Nueva["NOTA"] = "ESF-02";
                        else if (Indice == "1123" || Indice == "1125")
                            Row_Nueva["NOTA"] = "ESF-03";
                        else if (Indice == "114" || Indice == "115")
                            Row_Nueva["NOTA"] = "ESF-05";
                        else if (Indice == "1213")
                            Row_Nueva["NOTA"] = "ESF-06";
                        else if (Indice == "1214")
                            Row_Nueva["NOTA"] = "ESF-07";
                        else if (Indice == "123" || Indice == "124" || Indice == "1261" || Indice == "1262" || Indice == "1263" || Indice == "1264")
                            Row_Nueva["NOTA"] = "ESF-08";
                        else if (Indice == "125" || Indice == "1265" || Indice == "127")
                            Row_Nueva["NOTA"] = "ESF-09";
                        else if (Indice == "129")
                            Row_Nueva["NOTA"] = "ESF-11";
                        else if (Indice == "211" || Indice == "2111" || Indice == "2112" || Indice == "2113" || Indice == "2114" || Indice == "2115" ||
                            Indice == "2116" || Indice == "2117" || Indice == "2118" || Indice == "2119" || Indice == "2120" ||
                            Indice == "2121" || Indice == "2122" || Indice == "2119" || Indice == "2029")
                            Row_Nueva["NOTA"] = "ESF-12";
                        else if (Indice == "2159" || Indice == "216" || Indice == "224")
                            Row_Nueva["NOTA"] = "ESF-13";
                        else if (Indice == "2199" || Indice == "224")
                            Row_Nueva["NOTA"] = "ESF-14";
                        else if (Indice == "213" || Indice == "223")
                            Row_Nueva["NOTA"] = "ESF-15";
                        else if (Indice == "3100")
                            Row_Nueva["NOTA"] = "VHP-01";
                        else if (Indice == "3200")
                            Row_Nueva["NOTA"] = "VHP-02";
                        else
                            Row_Nueva["NOTA"] = "";


                        //Verificar si se tiene que colocar el registro
                        if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                        {
                            //Verificar al condicion
                            if (Convert.ToDecimal(Row_Nueva["PERIODO_ACTUAL"]) != 0 || Convert.ToDecimal(Row_Nueva["PERIODO_ANTERIOR"]) != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                        else
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00002")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 2);
                        SALDO = 0;
                        SALDO_INICIAL = 0;
                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0,2) == Indice)
                            {
                                SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0,2) == Indice)
                            {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                        if (Indice == "32")
                        {
                            SALDO = SALDO + Convert.ToDouble(Actual_3210);
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Anterior_3210);
                            Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", SALDO).Replace("$", "");
                            Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        }
                        else
                        {
                            Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", SALDO).Replace("$", "");
                            Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        }
                        //  para las notas
                        if (Indice == "1114" || Indice == "1115" || Indice == "1121" || Indice == "1211")
                            Row_Nueva["NOTA"] = "ESF-01";
                        else if (Indice == "1122" || Indice == "1124")
                            Row_Nueva["NOTA"] = "ESF-02";
                        else if (Indice == "1123" || Indice == "1125")
                            Row_Nueva["NOTA"] = "ESF-03";
                        else if (Indice == "1140" || Indice == "1150")
                            Row_Nueva["NOTA"] = "ESF-05";
                        else if (Indice == "1213")
                            Row_Nueva["NOTA"] = "ESF-06";
                        else if (Indice == "1214")
                            Row_Nueva["NOTA"] = "ESF-07";
                        else if (Indice == "1230" || Indice == "1240" || Indice == "1261" || Indice == "1262" || Indice == "1263" || Indice == "1264")
                            Row_Nueva["NOTA"] = "ESF-08";
                        else if (Indice == "1250" || Indice == "1265" || Indice == "1270")
                            Row_Nueva["NOTA"] = "ESF-09";
                        else if (Indice == "1290")
                            Row_Nueva["NOTA"] = "ESF-11";
                        else if (Indice == "2111" || Indice == "2112" || Indice == "2113" || Indice == "2114" || Indice == "2115" ||
                            Indice == "2116" || Indice == "2117" || Indice == "2118" || Indice == "2119" || Indice == "2120" ||
                            Indice == "2121" || Indice == "2122" || Indice == "2119" || Indice == "2029")
                            Row_Nueva["NOTA"] = "ESF-12";
                        else if (Indice == "2159" || Indice == "2160" || Indice == "2240")
                            Row_Nueva["NOTA"] = "ESF-13";
                        else if (Indice == "2199" || Indice == "2240")
                            Row_Nueva["NOTA"] = "ESF-14";
                        else if (Indice == "2130" || Indice == "2230")
                            Row_Nueva["NOTA"] = "ESF-15";
                        else if (Indice == "31")
                            Row_Nueva["NOTA"] = "VHP-01";
                        else if (Indice == "32")
                            Row_Nueva["NOTA"] = "VHP-02";
                        else
                            Row_Nueva["NOTA"] = "";


                        //Verificar si se tiene que colocar el registro
                        if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                        {
                            //Verificar al condicion
                            if (Convert.ToDecimal(Row_Nueva["PERIODO_ACTUAL"]) != 0 || Convert.ToDecimal(Row_Nueva["PERIODO_ANTERIOR"]) != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                        else
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                    if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00001")
                    {
                        Row_Nueva = Dt_Nueva.NewRow();
                        Indice = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 1);
                        SALDO = 0;
                        SALDO_INICIAL = 0;
                        foreach (DataRow Renglon in Dt_Consulta.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 1) == Indice)
                            {
                                SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        foreach (DataRow Renglon in Dt_Consulta_Anterior.Rows)
                        {
                            if (Renglon["CUENTA"].ToString().Substring(0, 1) == Indice)
                            {
                                SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_FINAL"].ToString());
                            }
                        }
                        Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                        Row_Nueva["NOMBRE"] = Dt_Temp_Consulta.Rows[Contador_For]["NOMBRE"].ToString();
                        if (Indice == "3")
                        {
                            SALDO = SALDO + Convert.ToDouble(Actual_3210);
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Anterior_3210);
                            Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", SALDO).Replace("$", "");
                            Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        }
                        else
                        {
                            Row_Nueva["PERIODO_ACTUAL"] = String.Format("{0:c}", SALDO).Replace("$", "");
                            Row_Nueva["PERIODO_ANTERIOR"] = String.Format("{0:c}", SALDO_INICIAL).Replace("$", "");
                        }
                        //  para las notas
                        if (Indice == "1114" || Indice == "1115" || Indice == "1121" || Indice == "1211")
                            Row_Nueva["NOTA"] = "ESF-01";
                        else if (Indice == "1122" || Indice == "1124")
                            Row_Nueva["NOTA"] = "ESF-02";
                        else if (Indice == "1123" || Indice == "1125")
                            Row_Nueva["NOTA"] = "ESF-03";
                        else if (Indice == "1140" || Indice == "1150")
                            Row_Nueva["NOTA"] = "ESF-05";
                        else if (Indice == "1213")
                            Row_Nueva["NOTA"] = "ESF-06";
                        else if (Indice == "1214")
                            Row_Nueva["NOTA"] = "ESF-07";
                        else if (Indice == "1230" || Indice == "1240" || Indice == "1261" || Indice == "1262" || Indice == "1263" || Indice == "1264")
                            Row_Nueva["NOTA"] = "ESF-08";
                        else if (Indice == "1250" || Indice == "1265" || Indice == "1270")
                            Row_Nueva["NOTA"] = "ESF-09";
                        else if (Indice == "1290")
                            Row_Nueva["NOTA"] = "ESF-11";
                        else if (Indice == "2111" || Indice == "2112" || Indice == "2113" || Indice == "2114" || Indice == "2115" ||
                            Indice == "2116" || Indice == "2117" || Indice == "2118" || Indice == "2119" || Indice == "2120" ||
                            Indice == "2121" || Indice == "2122" || Indice == "2119" || Indice == "2029")
                            Row_Nueva["NOTA"] = "ESF-12";
                        else if (Indice == "2159" || Indice == "2160" || Indice == "2240")
                            Row_Nueva["NOTA"] = "ESF-13";
                        else if (Indice == "2199" || Indice == "2240")
                            Row_Nueva["NOTA"] = "ESF-14";
                        else if (Indice == "2130" || Indice == "2230")
                            Row_Nueva["NOTA"] = "ESF-15";
                        else if (Indice == "3100")
                            Row_Nueva["NOTA"] = "VHP-01";
                        else if (Indice == "3200")
                            Row_Nueva["NOTA"] = "VHP-02";
                        else
                            Row_Nueva["NOTA"] = "";

                        //Verificar si se tiene que colocar el registro
                        if (Chk_Movimientos_Saldo_No_Cero.Checked == true)
                        {
                            //Verificar al condicion
                            if (Convert.ToDecimal(Row_Nueva["PERIODO_ACTUAL"]) != 0 || Convert.ToDecimal(Row_Nueva["PERIODO_ANTERIOR"]) != 0)
                            {
                                Dt_Nueva.Rows.Add(Row_Nueva);
                            }
                        }
                        else
                        {
                            Dt_Nueva.Rows.Add(Row_Nueva);
                        }
                    }
                }
            }
            //  ordenar la tabla
            Dt_Final.Columns.Add("INDICE", typeof(System.String));
            Dt_Final.Columns.Add("NOMBRE", typeof(System.String));
            Dt_Final.Columns.Add("PERIODO_ACTUAL", typeof(System.String));
            Dt_Final.Columns.Add("PERIODO_ANTERIOR", typeof(System.String));
            Dt_Final.Columns.Add("NOTA", typeof(System.String));
            Dt_Final.TableName = "Dt_Sit_Fin";
            return Dt_Nueva;

        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Tabla_Final_Analitico
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS:  Dt_Consulta.- la consulta a la que se le sumaran los cuentas contables
    ///CREO:        Sergio Manuel Gallardo Andrade
    ///FECHA_CREO:  23/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Generar_Tabla_Final_Analitico(DataTable Dt_Consulta)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Nueva = new DataTable();
        DataTable Dt_Final = new DataTable();
        DataTable Dt_Cuentas = new DataTable();
        DataTable Dt_Temp_Consulta = new DataTable();
        DataRow Row_Nueva;
        String Cuenta;
        int Contador_For = 0;
        Double SALDO = 0.0;
        Double CARGO = 0.0;
        Double ABONO = 0.0;
        Double SALDO_INICIAL = 0.0;
        try
        {
            Dt_Nueva.Columns.Add("INDICE", typeof(System.Double));
            Dt_Nueva.Columns.Add("DESCRIPCION ", typeof(System.String));
            Dt_Nueva.Columns.Add("SALDO_INICIAL", typeof(System.Double));
            Dt_Nueva.Columns.Add("CARGO", typeof(System.Double));
            Dt_Nueva.Columns.Add("ABONO", typeof(System.Double));
            Dt_Nueva.Columns.Add("SALDO_FINAL", typeof(System.Double));
            Dt_Nueva.Columns.Add("FLUJO", typeof(System.Double));
            Dt_Nueva.TableName = "Dt_Estado_Analitico";
            Dt_Final.Columns.Add("INDICE", typeof(System.Double));
            Dt_Final.Columns.Add("DESCRIPCION ", typeof(System.String));
            Dt_Final.Columns.Add("SALDO_INICIAL", typeof(System.Double));
            Dt_Final.Columns.Add("CARGO", typeof(System.Double));
            Dt_Final.Columns.Add("ABONO", typeof(System.Double));
            Dt_Final.Columns.Add("SALDO_FINAL", typeof(System.Double));
            Dt_Final.Columns.Add("FLUJO", typeof(System.Double));
            Dt_Final.TableName = "Dt_Estado_Analitico";

            Dt_Cuentas = Rs_Consulta.Consulta_Cuentas2();

            Dt_Temp_Consulta.Columns.Add("CUENTA", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("DESCRIPCION", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("NIVEL_ID", typeof(System.String));
            DataRow row;
            foreach (DataRow Renglon in Dt_Cuentas.Rows)
            {

                if (Renglon["CUENTA"].ToString().StartsWith("1"))
                {
                    row = Dt_Temp_Consulta.NewRow();
                    row["CUENTA"] = Renglon["CUENTA"].ToString();
                    row["DESCRIPCION"] = Renglon["DESCRIPCION"].ToString();
                    row["NIVEL_ID"] = Renglon["NIVEL_ID"].ToString();
                    Dt_Temp_Consulta.Rows.Add(row);
                    Dt_Temp_Consulta.AcceptChanges();
                }

            }
            //  se agregaran los saldos de las cuentas 
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {

                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00004")
                {
                    Row_Nueva = Dt_Nueva.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Consulta.Rows)
                    {
                        if (Renglon["CUENTA"].ToString().Substring(0, 4) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString()) + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString()) - Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["TOTAL_DEBE"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["TOTAL_HABER"].ToString());
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["DESCRIPCION "] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = SALDO_INICIAL;
                    Row_Nueva["SALDO_FINAL"] = SALDO;
                    Row_Nueva["CARGO"] = CARGO;
                    Row_Nueva["ABONO"] = ABONO;
                    Row_Nueva["FLUJO"] = CARGO - ABONO;
                    Dt_Nueva.Rows.Add(Row_Nueva);
                    Dt_Nueva.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00003")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 3);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 3) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString()) + Convert.ToDouble(Renglon["CARGO"].ToString()) - Convert.ToDouble(Renglon["ABONO"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString());
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["DESCRIPCION "] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = SALDO_INICIAL;
                    Row_Nueva["SALDO_FINAL"] = SALDO;
                    Row_Nueva["CARGO"] = CARGO;
                    Row_Nueva["ABONO"] = ABONO;
                    Row_Nueva["FLUJO"] = CARGO - ABONO;
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {

                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00002")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 2);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 2) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString()) + Convert.ToDouble(Renglon["CARGO"].ToString()) - Convert.ToDouble(Renglon["ABONO"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString());
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["DESCRIPCION "] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = SALDO_INICIAL;
                    Row_Nueva["SALDO_FINAL"] = SALDO;
                    Row_Nueva["CARGO"] = CARGO;
                    Row_Nueva["ABONO"] = ABONO;
                    Row_Nueva["FLUJO"] = CARGO - ABONO;
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            for (Contador_For = 0; Contador_For < Dt_Temp_Consulta.Rows.Count; Contador_For++)
            {
                if (Dt_Temp_Consulta.Rows[Contador_For]["NIVEL_ID"].ToString() == "00001")
                {
                    Row_Nueva = Dt_Final.NewRow();
                    Cuenta = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 1);
                    SALDO = 0;
                    CARGO = 0;
                    ABONO = 0;
                    SALDO_INICIAL = 0;
                    foreach (DataRow Renglon in Dt_Nueva.Rows)
                    {
                        if (Renglon["INDICE"].ToString().Substring(0, 1) == Cuenta)
                        {
                            SALDO = SALDO + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString()) + Convert.ToDouble(Renglon["CARGO"].ToString()) - Convert.ToDouble(Renglon["ABONO"].ToString());
                            CARGO = CARGO + Convert.ToDouble(Renglon["CARGO"].ToString());
                            ABONO = ABONO + Convert.ToDouble(Renglon["ABONO"].ToString());
                            SALDO_INICIAL = SALDO_INICIAL + Convert.ToDouble(Renglon["SALDO_INICIAL"].ToString());
                        }
                    }
                    Row_Nueva["INDICE"] = Dt_Temp_Consulta.Rows[Contador_For]["CUENTA"].ToString().Substring(0, 4);
                    Row_Nueva["DESCRIPCION "] = Dt_Temp_Consulta.Rows[Contador_For]["DESCRIPCION"].ToString();
                    Row_Nueva["SALDO_INICIAL"] = SALDO_INICIAL;
                    Row_Nueva["SALDO_FINAL"] = SALDO;
                    Row_Nueva["CARGO"] = CARGO;
                    Row_Nueva["ABONO"] = ABONO;
                    Row_Nueva["FLUJO"] = CARGO - ABONO;
                    Dt_Final.Rows.Add(Row_Nueva);
                    Dt_Final.AcceptChanges();
                }
            }
            foreach (DataRow Renglon in Dt_Final.Rows)
            {
                Row_Nueva = Dt_Nueva.NewRow();
                Row_Nueva["INDICE"] = Renglon["INDICE"].ToString();
                Row_Nueva["DESCRIPCION "] = Renglon["DESCRIPCION "].ToString();
                Row_Nueva["SALDO_INICIAL"] = Renglon["SALDO_INICIAL"].ToString();
                Row_Nueva["SALDO_FINAL"] = Renglon["SALDO_FINAL"].ToString();
                Row_Nueva["CARGO"] = Renglon["CARGO"].ToString();
                Row_Nueva["ABONO"] = Renglon["ABONO"].ToString();
                Row_Nueva["FLUJO"] = Renglon["FLUJO"].ToString();
                Dt_Nueva.Rows.Add(Row_Nueva);
                Dt_Nueva.AcceptChanges();

            }

            return Dt_Nueva;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    /// USUARIO CREO:        Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO:          18/Enero/2012
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar)
    {
        String Ruta = "../../Archivos/" + Nombre_Reporte_Generar;
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=300,height=100')", true);
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
    #endregion
    #region Eventos

    #region (Botones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_Analitico_Click
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  21/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Reporte_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Validar_Reporte())
            {
                if (Cmb_Tipo_Reporte.SelectedIndex < 4)
                    Consulta_Estado("PDF");

                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "El tipo de reporte para PDF es el siguiente: <br>SITUACION FINANCIERA <br>ESTADO DE ACTIVIDADES <br> VARIACIONES EN LA HACIENDA PÚBLICA";
                }

            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Excel_Click
    ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  19/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Imprimir_Excel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Validar_Reporte())
            {
                if (Cmb_Tipo_Reporte.SelectedIndex < 4)
                    Consulta_Estado("EXCEL");
                else
                    Consulta_Todos_Estado();
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operacion actual que se este realizando
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  18/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }
    #endregion

    #region(cmb)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Anio_OnSelectedIndexChanged
    ///DESCRIPCIÓN: cargara el combo de meses con los que se encuentren cerrados
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  22/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Anio_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Rpt_Con_Sit_Fin_Negocio Rs_Consulta_Meses = new Cls_Rpt_Con_Sit_Fin_Negocio();
        DataTable Dt_Consulta = new DataTable();
        String Año = "";
        String Mes;
        DataTable Dt_Temp_Consulta = new DataTable();
        try
        {
            Año = Cmb_Anio.SelectedValue;
            Año = Año.Substring(2, 2);
            Rs_Consulta_Meses.P_Año = Año;
            Dt_Consulta = Rs_Consulta_Meses.Consulta_Meses_Cerrados();
            // se definen los campos del Dt_Temp_Consulta
            Dt_Temp_Consulta.Columns.Add("MES", typeof(System.String));
            Dt_Temp_Consulta.Columns.Add("MES_ANIO", typeof(System.String));
            DataRow row;
            foreach (DataRow Renglon in Dt_Consulta.Rows)
            {
                row = Dt_Temp_Consulta.NewRow();
                Mes = Renglon["Mes_Anio"].ToString().Substring(0, 2);
                switch (Mes)
                {
                    case "01":
                        Mes = "ENERO";
                        break;
                    case "02":
                        Mes = "FEBRERO";
                        break;
                    case "03":
                        Mes = "MARZO";
                        break;
                    case "04":
                        Mes = "ABRIL";
                        break;
                    case "05":
                        Mes = "MAYO";
                        break;
                    case "06":
                        Mes = "JUNIO";
                        break;
                    case "07":
                        Mes = "JULIO";
                        break;
                    case "08":
                        Mes = "AGOSTO";
                        break;
                    case "09":
                        Mes = "SEPTIEMBRE";
                        break;
                    case "10":
                        Mes = "OCTUBRE";
                        break;
                    case "11":
                        Mes = "NOVIEMBRE";
                        break;
                    default:
                        Mes = "DICIEMBRE";
                        break;
                }
                row["MES"] = Mes;
                row["MES_ANIO"] = Renglon["Mes_Anio"].ToString();
                Dt_Temp_Consulta.Rows.Add(row);
                Dt_Temp_Consulta.AcceptChanges();
            }

            Cmb_Mes.Items.Clear();
            Cmb_Mes.DataSource = Dt_Temp_Consulta;
            Cmb_Mes.DataValueField = "MES_ANIO";
            Cmb_Mes.DataTextField = "MES";
            Cmb_Mes.DataBind();
            Cmb_Mes.Items.Insert(0, "----- < SELECCIONE > -----");
            Cmb_Mes.SelectedIndex = 0;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
    }

    #endregion
    #endregion
    #region (Reportes)

    /// *************************************************************************************
    /// NOMBRE: Generar_Reporte
    /// 
    /// DESCRIPCIÓN: Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS: Nombre_Plantilla_Reporte.- Nombre del archivo del Crystal Report.
    ///             Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
    {
        ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
        String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Nombre_Plantilla_Reporte);
            Reporte.Load(Ruta);

            if (Ds_Datos is DataSet)
            {
                if (Ds_Datos.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Datos);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Abrir Reporte", "window.open('../../Reporte/" + Nombre_Reporte_Generar + "');", true);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE: Exportar_Reporte_PDF
    /// 
    /// DESCRIPCIÓN: Método que guarda el reporte generado en formato PDF en la ruta
    ///              especificada.
    ///              
    /// PARÁMETROS: Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///             Nombre_Reporte.- Nombre que se le dará al reporte.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************************************
    /// NOMBRE_FUNCIÓN: Exportar_Reporte
    /// DESCRIPCIÓN: Genera el reporte de Crystal con los datos proporcionados en el DataTable 
    /// PARÁMETROS:
    /// 		1. Ds_Reporte: Dataset con datos a imprimir
    /// 		2. Nombre_Reporte: Nombre del archivo de reporte .rpt
    /// 		3. Nombre_Archivo: Nombre del archivo a generar
    /// CREO: Roberto González Oseguera
    /// FECHA_CREO: 04-sep-2011
    /// MODIFICÓ: 
    /// FECHA_MODIFICÓ: 
    /// CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private void Exportar_Reporte(DataSet Ds_Reporte, String Nombre_Reporte, String Nombre_Archivo, String Extension_Archivo, ExportFormatType Formato)
    {
        ReportDocument Reporte = new ReportDocument();
        String Ruta = Server.MapPath("../Rpt/Contabilidad/" + Nombre_Reporte);

        try
        {
            Reporte.Load(Ruta);
            Reporte.SetDataSource(Ds_Reporte);
        }
        catch
        {
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "No se pudo cargar el reporte";
        }

        String Archivo_Reporte = Nombre_Archivo + "." + Extension_Archivo;  // formar el nombre del archivo a generar 
        try
        {
            ExportOptions Export_Options_Calculo = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options_Calculo = new DiskFileDestinationOptions();
            Disk_File_Destination_Options_Calculo.DiskFileName = Server.MapPath("../../Reporte/" + Archivo_Reporte);
            Export_Options_Calculo.ExportDestinationOptions = Disk_File_Destination_Options_Calculo;
            Export_Options_Calculo.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options_Calculo.ExportFormatType = Formato;
            Reporte.Export(Export_Options_Calculo);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Abrir Reporte", "window.open('../../Reporte/" + Archivo_Reporte + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=500,height=300')", true);
            Abrir_Ventana(Archivo_Reporte);

            //if (Formato == ExportFormatType.Excel)
            //{
            //    Mostrar_Excel(Server.MapPath("../../Reporte/" + Archivo_Reporte), "application/vnd.ms-excel");
            //}
            //else if (Formato == ExportFormatType.WordForWindows)
            //{
            //    Mostrar_Excel(Server.MapPath("../../Reporte/" + Archivo_Reporte), "application/vnd.ms-word");
            //}
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }

    /// *************************************************************************************
    /// NOMBRE: Mostrar_Excel
    /// 
    /// DESCRIPCIÓN: Muestra el reporte en excel.
    ///              
    /// PARÁMETROS: No Aplica
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 10/Diciembre/2011.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    private void Mostrar_Excel(string Ruta_Archivo, string Contenido)
    {
        try
        {
            System.IO.FileInfo ArchivoExcel = new System.IO.FileInfo(Ruta_Archivo);
            if (ArchivoExcel.Exists)
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = Contenido;
                Response.AddHeader("Content-Disposition", "attachment;filename=" + ArchivoExcel.Name);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Response.WriteFile(ArchivoExcel.FullName);
                //Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception Ex)
        {
            //// Response.End(); siempre genera una excepción (http://support.microsoft.com/kb/312629/EN-US/)
            throw new Exception("Error al mostrar el reporte en excel. Error: [" + Ex.Message + "]");
        }
    }

    #endregion

}
