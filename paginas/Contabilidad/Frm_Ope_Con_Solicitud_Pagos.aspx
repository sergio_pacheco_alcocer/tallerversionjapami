﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true"  enableEventValidation="false" CodeFile="Frm_Ope_Con_Solicitud_Pagos.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Solicitud_Pagos" Title="Solicitud de Pagos" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server" >

    <script src="../../easyui/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.formatCurrency-1.4.0.min.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.formatCurrency.all.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript">
            // manejo de ocultar tr dependiendo del tipo de operacion que realicen
            $(document).ready(function() {
                $("select[id$=Cmb_Operacion]").change(function() {
                    $(".Cmb_Operacion option:selected").each(function() {
                        var Tipo = $(this).val();
                        if (Tipo == 3 || Tipo == 2) {
                            $("#Td_Cedular").show();
                            $("#Td_Txt_Reten_Cedular").show();
                            $("#Td_encabezado1").show();
                            $("#Td_encabezado2").hide();
                            $('#Txt_Fectura_ISR').show();
                            $('#Txt_Fectura_Reten_IVA').show();
                            $('#Txt_Fectura_ISH').hide();
                            $('#Txt_Fectura_IEPS').hide();
                            $('#Td_Encab_Iva').show();
                            $('#Td_Encab_Mes').hide();
                            $('#Td_Iva').show();
                            $('#Td_Mes').hide();
                            $('#Td_ISH').hide();
                            $("#Td_ISR").show();
                            $('#Td_IEPS').hide();
                            $("#Td_Reten_Iva").show();
                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                if (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value != "") {
                                    document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                } else {
                                    document.getElementById("<%=Txt_IVA.ClientID%>").value = 0;
                                }
                                if (document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value != "") {
                                    document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                } else {
                                    document.getElementById("<%=Txt_ISR.ClientID%>").value = 0;
                                }
                                if (document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value != "") {
                                    document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                } else {
                                    document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                                }
                                if (document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value != "") {
                                    document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                } else {
                                    document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;
                                }
//                                document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
//                                document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
//                                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
//                                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value))) * 100) / 100; ;
                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                            }
                        }
                        if (Tipo == 1) {
                            $("#Td_Cedular").hide();
                            $("#Td_Txt_Reten_Cedular").hide();
                            $("#Td_encabezado1").hide();
                            $("#Td_encabezado2").show();
                            $('#Txt_Fectura_ISR').hide();
                            $('#Txt_Fectura_Reten_IVA').hide();
                            $('#Td_ISH').show();
                            $("#Td_ISR").hide();
                            $('#Td_IEPS').show();
                            $('#Txt_Fectura_ISH').show();
                            $('#Txt_Fectura_IEPS').show();
                            $('#Td_Encab_Iva').show();
                            $('#Td_Encab_Mes').hide();
                            $('#Td_Iva').show();
                            $("#Td_Reten_Iva").hide();
                            $('#Td_Mes').hide();
                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value / 100)) * 100) / 100;
                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value))) * 100) / 100;
                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                document.getElementById("<%=Txt_ISR.ClientID%>").value = 0;
                                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;

                            }
                        }
                        if (Tipo == 4) {
                            $("#Td_Cedular").hide();
                            $("#Td_Txt_Reten_Cedular").hide();
                            $('#Txt_Fectura_ISR').show();
                            $('#Txt_Fectura_Reten_IVA').hide();
                            $("#Td_Reten_Iva").hide();
                            $("#Td_ISR").show();
                            $("#Td_ISH").hide();
                            $("#Td_IEPS").hide();
                            $('#Txt_Fectura_ISH').hide();
                            $('#Txt_Fectura_IEPS').hide();
                            $('#Td_Encab_Iva').hide();
                            $('#Td_Encab_Mes').show();
                            $('#Td_Iva').hide();
                            $('#Td_Mes').show();
                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                try {
                                    var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                                    $.ajax({
                                        url: "Controlador_Contabilidad.aspx?" + cadena,
                                        type: 'POST',
                                        async: false,
                                        cache: false,
                                        success: function(data) {
                                            if (data != null) {
                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                            }
                                            else {
                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                            }
                                        }
                                    });
                                } catch (Ex) {
                                    $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                                }
                                if (document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value != "") {
                                    document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                
                                } else {
                                document.getElementById("<%=Txt_ISR.ClientID%>").value = 0;
                                }
                               // document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;
                            }
                        }
                    });
                }).trigger('change');
            });
            function calculo() {
                if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 2 || document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 3) {
                    if (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value != "") {
                        document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                    } else {
                        document.getElementById("<%=Txt_IVA.ClientID%>").value = 0;
                    }
                    if (document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value != "") {
                        document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
                    } else {
                        document.getElementById("<%=Txt_ISR.ClientID%>").value = 0;
                    }
                    if (document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value != "") {
                        document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                    } else {
                        document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                    }
                    if (document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value != "") {
                        document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                    } else {
                        document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;
                    }
//                    document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
//                    document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
//                    document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
//                    document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value))) * 100) / 100; ;
                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                } else {
                    if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 1) {
                        if (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value != "") {
                            document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value / 100)) * 100) / 100;
                        } else {
                            document.getElementById("<%=Txt_IVA.ClientID%>").value = 0;
                        }
                        //document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value / 100)) * 100) / 100;
                        document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value))) * 100) / 100;
                        document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                    } else {
                        if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 4) {
                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0 && document.getElementById("<%=Txt_Meses.ClientID%>").value > 0) {
                                try {
                                    var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                                    $.ajax({
                                        url: "Controlador_Contabilidad.aspx?" + cadena,
                                        type: 'POST',
                                        async: false,
                                        cache: false,
                                        success: function(data) {
                                            if (data != null) {
                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                            }
                                            else {
                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                            }
                                        }
                                    });
                                } catch (Ex) {
                                    $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                                }
                                document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                            }
                        }
                    }
                }
                if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value <= 0) {
                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = "0";
                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                }
            }
            function calculo2() {
                if (document.getElementById("<%=Txt_IVA.ClientID%>").value < 0) {
                    document.getElementById("<%=Txt_IVA.ClientID%>").value = "0";
                }
                if (document.getElementById("<%=Txt_ISR.ClientID%>").value < 0) {
                    document.getElementById("<%=Txt_ISR.ClientID%>").value = "0";
                }
                if (document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value < 0) {
                    document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = "0";
                }
                if (document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value < 0) {
                    document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = "0";
                }
                if (document.getElementById("<%=Txt_ISH.ClientID%>").value < 0) {
                    document.getElementById("<%=Txt_ISH.ClientID%>").value = "0";
                }
                if (document.getElementById("<%=Txt_IEPS.ClientID%>").value < 0) {
                    document.getElementById("<%=Txt_IEPS.ClientID%>").value = "0";
                }
                if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 2 || document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 3) {
                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = (parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value));
                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                } else {
                    if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 1) {
                        document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = (parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value));
                        document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                    } else {
                        if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 4) {
                            document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                            document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                        }
                    }
                }

            }
            function StartUpload(sender, args) {
                try {
                    var filename = args.get_fileName();

                    if (filename != "") {
                        // code to get File Extension..   
                        var arr1 = new Array;
                        arr1 = filename.split("\\");
                        var len = arr1.length;
                        var img1 = arr1[len - 1];
                        var filext = img1.substring(img1.lastIndexOf(".") + 1);


                        if (filext == "txt" || filext == "doc" || filext == "pdf" || filext == "docx" || filext == "jpg" || filext == "JPG" || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" || filext == "GIF" || filext == "xlsx") {
                            if (args.get_length() > 2621440) {
                                var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                                alert("El Archivo " + filename + ". Excedio el Tamaño Permitido:\n\nTamaño del Archivo: [" + args.get_length() + " Bytes]\nTamaño Permitido: [2621440 Bytes o 2.5 Mb]" + mensaje);
                                return false;

                            }
                            return true;
                        } else {
                            var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                            alert("Tipo de archivo inválido " + filename + "\n\nFormatos Validos [.txt, .doc, .docx, .zip, .rar, .pdf, .jpg, .jpeg, .png, .gif, .xlsx]" + mensaje);
                            return false;
                        }
                    }
                } catch (e) {

                }
            }

            function uploadError(sender, args) {
                try {
                    var mensaje = "\n\nLa Tabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                    alert("Error al Intentar cargar el archivo. [ " + args.get_fileName() + " ]\n\nTamaño Válido de los Archivos:\n + El Archivo debé ser mayor a 1Kb.\n + El Archivo debé ser Menor a 2.5 Mb" + mensaje);
                    //refreshGridView();
                } catch (e) {

                }
            }

            //Metodo para mantener los calendarios en una capa mas alat.
            function calendarShown(sender, args) {
                sender._popupBehavior._element.style.zIndex = 10000005;
            }
            //Metodos para limpiar los controles de la busqueda.
            function Limpiar_Ctlr() {
                document.getElementById("<%=Txt_Busqueda_No_Reserva.ClientID%>").value = "";
                document.getElementById("<%=Txt_Busqueda_No_Solicitud_Pago.ClientID%>").value = "";
                document.getElementById("<%=Cmb_Busqueda_Tipo_Solicitud.ClientID%>").value = "";
                document.getElementById("<%=Cmb_Busqueda_Dependencia.ClientID%>").value = "";
                document.getElementById("<%=Cmb_Busqueda_Estatus_Solicitud_Pago.ClientID%>").value = "";
                document.getElementById("<%=Txt_Busqueda_Fecha_Inicio.ClientID%>").value = "";
                document.getElementById("<%=Txt_Busqueda_Fecha_Fin.ClientID%>").value = "";
                return false;
            }
            function Abrir_Modal_Popup() {
                $find('Busqueda_Solicitud_Pago').show();
                return false;
            }
            function Calculo3() {
                if (document.getElementById("<%=Txt_Meses.ClientID%>").value != "" && document.getElementById("<%=Txt_Meses.ClientID%>").value != 0) {
                    if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                        try {
                            var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                            $.ajax({
                                url: "Controlador_Contabilidad.aspx?" + cadena,
                                type: 'POST',
                                async: false,
                                cache: false,
                                success: function(data) {
                                    if (data != null) {
                                        document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                    }
                                    else {
                                        document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                    }
                                }
                            });
                        } catch (Ex) {
                            $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                        }
                        document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                        document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                        document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                    }
                }
            }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    
    <cc1:ToolkitScriptManager ID="ScriptManager_Tipo_Solicitud_Pagos" runat="server" AsyncPostBackTimeout="720000" EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
           <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
           </asp:UpdateProgress>
            <div id="Div_Solicitud_Pagos" >
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Solicitud de Pagos</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>               
                    <tr class="barra_busqueda" align="right">
                        <td style="width:50%" align="left">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" 
                                TabIndex="1" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                onclick="Btn_Nuevo_Click" />
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" 
                                TabIndex="2" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                                onclick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Imprimir" runat="server" ToolTip="Imprimir" CssClass="Img_Button" 
                                TabIndex="2" ImageUrl="~/paginas/imagenes/gridview/grid_print.png"  Visible="false"
                                onclick="Btn_Imprimir_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="4" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                onclick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%">
                            <table style="width:100%;height:28px;">
                                <tr>
                                    <td style="width:100%;vertical-align:top;" align="right">
                                        B&uacute;squeda 
                                        <asp:UpdatePanel ID="Udp_Modal_Popup" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
                                            <ContentTemplate> 
                                                    <asp:ImageButton ID="Btn_Mostrar_Popup_Busqueda" runat="server" 
                                                        ToolTip="Busqueda Avanzada" TabIndex="1"
                                                        ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Height="24px" Width="24px"
                                                        OnClientClick="javascript:return Abrir_Modal_Popup();" CausesValidation="false" />
                                                    <cc1:ModalPopupExtender ID="Mpe_Busqueda_Solicitud_Pago" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Busqueda_Solicitud_Pago"
                                                        PopupControlID="Pnl_Busqueda_Contenedor" TargetControlID="Btn_Comodin_Open" CancelControlID="Btn_Comodin_Close" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                                    <asp:Button Style="background-color: transparent; border-style:none;" 
                                                        ID="Btn_Comodin_Close" runat="server" Text="" />
                                                    <asp:Button  Style="background-color: transparent; border-style:none;" 
                                                        ID="Btn_Comodin_Open" runat="server" Text="" />                                                                                                    
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>                                                                                                                                                   
                                </tr>                                                                          
                            </table>
                        </td> 
                    </tr>
                </table>
                <asp:UpdatePanel ID="Upnl_Datos_Generales_Solicitud_Pagos" runat="server" UpdateMode="Conditional">
                    <ContentTemplate> 
                        <asp:Panel ID="Pnl_Datos_Generales_Solicitud_Pagos" runat="server" GroupingText="Datos Generales" Width="98%" BackColor="White">
                            <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                <tr>
                                    <td style="width:20%">No. Solicitud</td>
                                    <td style="width:30%">
                                        <asp:TextBox ID="Txt_No_Solicitud_Pago" runat="server" ReadOnly="True" Width="236px" BorderStyle="Solid" BorderWidth="1"></asp:TextBox>
                                    </td>
                                    <td style="width:20%">Estatus</td>
                                    <td style="width:30%">
                                        <asp:TextBox ID="Txt_Estatus_solicitud_Pago" runat="server" ReadOnly="True" Width="236px" BorderStyle="Solid" BorderWidth="1"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%">Fecha Solicitud</td>
                                    <td style="width:30%">
                                        <asp:TextBox ID="Txt_Fecha_Solicitud_Pago" runat="server" ReadOnly="True" Width="236px" BorderStyle="Solid" BorderWidth="1"></asp:TextBox>
                                    </td>
                                    <td style="width:20%">*Tipo de Solicitud</td>
                                    <td style="width:30%">
                                        <asp:DropDownList ID="Cmb_Tipo_Solicitud_Pago" runat="server" TabIndex="6" Width="240px"/> 
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate> 
                </asp:UpdatePanel>
               <asp:UpdatePanel ID="Upnl_Datos_Reserva" runat="server" UpdateMode="Conditional">
                    <ContentTemplate> 
                        <asp:Panel ID="Pnl_Datos_Reserva" runat="server" GroupingText="Datos Reserva" Width="98%" BackColor="White">
                            <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                <tr>
                                    <td style="width:20%">*No. Reserva</td>
                                    <td style="width:30%">
                                        <asp:TextBox ID="Txt_No_Reserva" runat="server" Visible="false" Width="240px" ReadOnly=true></asp:TextBox>
                                        <asp:DropDownList ID="Cmb_Reserva_Pago" runat="server" TabIndex="7" AutoPostBack="true"
                                            Width="240px" onselectedindexchanged="Cmb_Reserva_Pago_SelectedIndexChanged"/> 
                                    </td>
                                    <td style="width:20%">Saldo</td>
                                    <td style="width:30%">
                                        <asp:TextBox ID="Txt_Saldo_Reserva" runat="server" ReadOnly="true" Width="236px" BorderStyle="Solid" BorderWidth="1"></asp:TextBox>
                                    </td>
                                </tr>                            
                                <tr>
                                    <td style="width:20%">Reservado</td>
                                    <td colspan="3" style="width:80%">
                                        <asp:TextBox ID="Txt_Concepto_Reserva" runat="server" Width="99%" ReadOnly="true" BorderStyle="Solid" BorderWidth="1"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                                <tr>  
                                    <td>
                                        <div style="overflow:auto;width:99%;vertical-align:top; max-height:150px">
                                                <asp:GridView ID="Grid_Partidas" runat="server" Width="97%"
                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None">
                                                    <Columns>                                               
                                                           <asp:BoundField DataField="Dependencia" HeaderText="Dependencia" ItemStyle-Font-Size="XX-Small">
                                                             <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"  />
                                                             <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Fuente_Financiamiento" HeaderText="Fuente" ItemStyle-Font-Size="XX-Small">
                                                             <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                             <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="PROYECTOS_PROGRAMAS" HeaderText="Programas" ItemStyle-Font-Size="XX-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                           <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="AREA_FUNCIONAL" HeaderText="Area funcional" ItemStyle-Font-Size="XX-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                           <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="PARTIDA" HeaderText="Partida" ItemStyle-Font-Size="XX-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                           <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="CODIGO_PROGRAMATICO" HeaderText="Codigo_Programatico" ItemStyle-Font-Size="XX-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                                           <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:c}" ItemStyle-Font-Size="XX-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small" />
                                                           <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                           </asp:BoundField>
                                                    </Columns>                                                    
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="tblHead" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView> 
                                           </div>                            
                                        </td>                                  
                                </tr>
                                <asp:HiddenField ID="Txt_No_Reserva_Anterior" runat="server" />
                                <asp:HiddenField ID="Txt_Monto_Solicitud_Anterior" runat="server" />
                                <asp:HiddenField ID="Txt_Total_anterior" runat="server" />
                                <asp:HiddenField ID="Txt_ID_Proveedor" runat="server" />
                                <asp:HiddenField ID="Txt_ID_Empleado" runat="server" />
                                <asp:HiddenField ID="Txt_porcentaje_ISR_HA" runat="server" />
                            </table>
                        </asp:Panel>
                    </ContentTemplate> 
                </asp:UpdatePanel>
               <asp:UpdatePanel ID="Upnl_Datos_Solicutd_Pago" runat="server" UpdateMode="Conditional">
                    <ContentTemplate> 
                        <asp:Panel ID="Pnl_Datos_Solicutd_Pago" runat="server" GroupingText="Solicitud de Pago" Width="98%" BackColor="White">
                            <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td >
                                    <asp:HiddenField ID="Txt_iva_parametro" runat="server" />
                                    <asp:HiddenField ID="Txt_Reten_Cedular_parametro" runat="server" />
                                    <asp:HiddenField ID="Txt_Monto_partida" runat="server" />
                                <%--<asp:TextBox ID="Txt_iva_parametro" runat="server" Visible= "False"></asp:TextBox>
                                <asp:TextBox ID="Txt_Reten_Cedular_parametro" runat="server" Visible= "False"></asp:TextBox>--%>
                                </td>
                                <td >
                                <asp:HiddenField ID="Txt_reten_iva_parametro" runat="server" />
                                <asp:HiddenField ID="Txt_ISR_parametro" runat="server" />
                               <%-- <asp:TextBox ID="Txt_reten_iva_parametro" runat="server" Visible= "False"></asp:TextBox>
                                <asp:TextBox ID="Txt_ISR_parametro" runat="server" Visible= "False"></asp:TextBox>--%>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                <asp:TextBox ID="Txt_Ruta" runat="server" Visible= "False"></asp:TextBox>

                                </td>
                                <td >
                                <asp:TextBox ID="Txt_Nombre_Archivo" runat="server" Visible= "False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                 <td style="width:80%" colspan="3">
                                        <asp:Label ID="Lbl_nom_Proveedor" runat="server" Text="*Nombre Proveedor"></asp:Label>
                                    </td>                                 
                                </tr>
                            <tr>        
                                    <td style="width:99%" colspan="4">
                                        <asp:TextBox ID="Txt_Nom_Proveedor" runat="server" Width="97%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Nom_Proveedor" runat="server" TargetControlID="Txt_Nom_Proveedor"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>                            
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                            <tr>
                                                   <td style="width:25%">*RFC</td>                                   
                                                    <td style="width:25%" >*CURP</td> 
                                                   <td style="width:25%">*Numero Documento</td>                                   
                                                   <td style="width:25%">*Fecha Documento</td>    
                                            </tr>
                                            <tr>
                                                    <td style="width:25%">
                                                        <asp:TextBox ID="Txt_RFC" runat="server" MaxLength="13" Width="90%"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="Txt_RFC"
                                                            FilterType="Numbers,Custom, UppercaseLetters, LowercaseLetters" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </td>
                                                    <td style="width:25%">                                        
                                                        <asp:TextBox ID="Txt_CURP" runat="server" Width="90%" MaxLength="19"></asp:TextBox> 
                                                        <cc1:FilteredTextBoxExtender ID="Filt_CURP" runat="server" TargetControlID="Txt_CURP"
                                                            FilterType="Numbers,Custom, UppercaseLetters, LowercaseLetters">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </td>
                                                     <td style="width:25%">
                                                      <asp:TextBox ID="Txt_No_Factura_Solicitud_Pago" runat="server" Width="90%" MaxLength="15"></asp:TextBox>   
                                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_No_Factura_Solicitud_Pago" runat="server" TargetControlID="Txt_No_Factura_Solicitud_Pago"
                                                            FilterType="Numbers,Custom, UppercaseLetters, LowercaseLetters" >
                                                        </cc1:FilteredTextBoxExtender>
                                                    </td>
                                                    <td style="width:25%">
                                                          <asp:TextBox ID="Txt_Fecha_Factura_Solicitud_Pago" runat="server" MaxLength="100" Width="85%" Enable="false"/>
                                                        <cc1:CalendarExtender ID="DTP_Fecha_Factura_Solicitud_Pago" runat="server" 
                                                            TargetControlID="Txt_Fecha_Factura_Solicitud_Pago" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Factura_Solicitud_Pago"/>
                                                         <asp:ImageButton ID="Btn_Fecha_Factura_Solicitud_Pago" runat="server" TabIndex="9" 
                                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                                            Height="18px" CausesValidation="false" Enabled="true"/> 
                                                    </td>
                                             </tr>
                                             <tr>
                                                 <td style="width:75%" colspan="3">
                                                        <asp:Label ID="Lbl_Partida" runat="server" Text="Partidas"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Lbl_Subtotal1" runat="server" Text="SubTotal" Width="55px"></asp:Label>
                                                    </td>
                                            </tr>
                                            <tr>        
                                                    <td style="width:75%" colspan="3">
                                                        <asp:DropDownList ID="Cmb_Partida" runat="server" Width="98%">
                                                        </asp:DropDownList>
                                                    </td>   
                                                    <td style=" width:40%;">
                                                        <asp:TextBox ID="Txt_Subtotal_factura" runat="server" Width="85%" OnKeyUp="javascript:calculo();" OnKeyPress="this.value=(this.value.match(/^[0-9]*(\.[0-9]{0,2})?$/))?this.value :'';" ></asp:TextBox>
                                                    </td>   
                                             </tr>
                                        </table>
                                    </td>
                                </tr>
                             <tr>
                                <td style="width:25%">
                                    <asp:Label ID="Lbl_Operacion" runat="server" Text="Operacion"></asp:Label>
                                </td>
                                <td  style="width:47%">
                                        Documento Electr&oacute;nico
                                </td>
                                <td id="Td_Encab_Iva" style="width:14%">
                                    &nbsp;&nbsp;&nbsp;
                                     <asp:Label ID="Lbl_IVA_Factura" runat="server" Text="IVA" ></asp:Label>
                                </td>
                                <td id="Td_Encab_Mes" style="width:14%;display:none">
                                     <asp:Label ID="Lbl_Meses" runat="server" Text="Meses"></asp:Label>
                                </td>
                                <td id="Td_Cedular" style="width:14%">
                                     <asp:Label ID="Lbl_Cedular" runat="server" Text="Cedular"></asp:Label>
                                </td>
                             </tr>
                            <tr> 
                                    <td  style="width:25%">
                                         <asp:DropDownList ID="Cmb_Operacion"   CssClass="Cmb_Operacion" runat="server" Width="195px">
                                             <asp:ListItem value="1">OTROS</asp:ListItem>
                                             <asp:ListItem Value="2">ARRENDAMIENTO</asp:ListItem>
                                             <asp:ListItem Value="3">HONORARIOS</asp:ListItem>
                                             <asp:ListItem Value="4">HON. ASIMILABLE</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td  style="width:47%">                                  
                                          <cc1:AsyncFileUpload ID="Asy_Cargar_Archivo" runat="server"  Width="380px" ThrobberID="Throbber" 
                                            ForeColor="Black" Font-Bold="True" CompleteBackColor="LightGreen" 
                                            UploadingBackColor="LightBlue" OnClientUploadComplete="StartUpload"  OnClientUploadError="uploadError"
                                            OnUploadedComplete="Asy_Cargar_Archivo_Complete" FailedValidation="False" />  
                                    </td>
                                    <td  id="Td_Iva" style="width:14%">
                                        &nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="Txt_IVA" runat="server" Width="70%" OnKeyUp="javascript:calculo2();" ToolTip="IVA" ></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_IVA" runat="server" 
                                            TargetControlID="Txt_IVA" FilterType="Custom, Numbers" ValidChars="-,."/>                                          
                                    </td>
                                    <td id="Td_Mes" style="width:14%;display:none">
                                            &nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="Txt_Meses" runat="server" Width="70%" OnKeyUp="javascript:Calculo3();" Text="1" ></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Meses" runat="server" 
                                            TargetControlID="Txt_Meses" FilterType="Numbers" />
                                    </td> 
                                    <td style="width:14%" id="Td_Txt_Reten_Cedular">
                                        <asp:TextBox ID="Txt_Reten_Cedular" runat="server" Width="60%" OnKeyUp="javascript:calculo2();" ToolTip="Retencion Cedular"></asp:TextBox>                                        
                                        <cc1:FilteredTextBoxExtender ID="Fil_Txt_Reten_Cedular" runat="server" 
                                            TargetControlID="Txt_Reten_Cedular" FilterType="Custom, Numbers" ValidChars="-,."/>  
                                    </td>
                                </tr>
                                <tr>
                                <td style="width:75%" colspan="2">
                                </td>
                                <td id="Td_ISR" style="width:14%">
                                    &nbsp;&nbsp;&nbsp;
                                     <asp:Label ID="Lbl_ISR" runat="server" Text="ISR" Width="55px" ToolTip="Impuesto Sobre la Renta"></asp:Label>
                                </td>                                
                                <td id="Td_Reten_Iva" style="width:14%">
                                     <asp:Label ID="Lbl_Reten_IVA" runat="server" Text="Ret.IVA" Width="45px" ToolTip="Retención Iva"></asp:Label>
                                </td>
                                <td id="Td_ISH" style="width:14%; display:none;">
                                    &nbsp;&nbsp;&nbsp;
                                     <asp:Label ID="Lbl_ISH" runat="server" Text="ISH" Width="55px"  ToolTip="Impuesto Sobre Hospedaje"></asp:Label>  
                                </td>                                
                                <td id="Td_IEPS" style="width:14%; display:none;">
                                     <asp:Label ID="Lbl_IEPS" runat="server" Text="IEPS" Width="45px"  ToolTip="Impuesto Especial SObre Producción y Servicios"></asp:Label>
                                </td>
                                <%--<td  colspan="2" id="Td_encabezado2" style="display:none; width:25%;">
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Lbl_ISH" runat="server" Text="ISH" Width="55px" ></asp:Label>  
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                  
                                    <asp:Label ID="Lbl_IEPS" runat="server" Text="IEPS" Width="45px" ></asp:Label>
                                </td>--%>
                                </tr>
                                <tr>
                                    <td style="width:75%" colspan="2">
                                     &nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td style="width:14%;display:" id="Txt_Fectura_ISR">
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:TextBox ID="Txt_ISR" runat="server" Width="70%" OnKeyUp="javascript:calculo2();" ToolTip="Impuesto Sobre la Renta"></asp:TextBox>                                            
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_ISR" runat="server" 
                                            TargetControlID="Txt_ISR" FilterType="Custom, Numbers" ValidChars="-,."/>  
                                     </td>
                                     <td style="width:14%;display:" id="Txt_Fectura_Reten_IVA">
                                            <asp:TextBox ID="Txt_Reten_IVA" runat="server" Width="60%" OnKeyUp="javascript:calculo2();" ToolTip="Retención de Iva"></asp:TextBox>                                            
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_Reten_IVA" runat="server" 
                                            TargetControlID="Txt_Reten_IVA" FilterType="Custom, Numbers" ValidChars="-,."/>  
                                     </td>     
                                     <td style="width:14%;display:none;" id="Txt_Fectura_ISH">
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:TextBox ID="Txt_ISH" runat="server"  Width="70%" OnKeyUp="javascript:calculo2();" ToolTip="Impuesto Sobre Hospedaje"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_ISH" runat="server" 
                                            TargetControlID="Txt_ISH" FilterType="Custom, Numbers" ValidChars="-,."/>  
                                     </td>
                                     <td style="width:14%;display:none;" id="Txt_Fectura_IEPS">
                                             <asp:TextBox ID="Txt_IEPS" runat="server" Width="60%" OnKeyUp="javascript:calculo2();" ToolTip="Impuesto Especial SObre Producción y Servicios"></asp:TextBox> 
                                        <cc1:FilteredTextBoxExtender ID="Filt_Txt_IEPS" runat="server" 
                                            TargetControlID="Txt_IEPS" FilterType="Custom, Numbers" ValidChars="-,."/>  
                                     </td>     
                                </tr>
                                <tr>
                                    <td style="width:75%" colspan="2">
                                    </td>
                                    <td colspan="2" style="width:25%">
                                        &nbsp;&nbsp;&nbsp;
                                     <asp:Label ID="Lbl_total_Factura" runat="server" Text="Total" Width="55px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                <td style="width:75%" colspan="2">
                                </td>
                                     <td colspan="2" style="width:25%">
                                        &nbsp;&nbsp;&nbsp;
                                         <asp:TextBox ID="Txt_Monto_Solicitud_Pago" runat="server" CssClass="text_cantidades_grid" TabIndex="10" width="70%" ReadOnly="true"/><%--  onblur="$('input[id$=Txt_Monto_Solicitud_Pago]').formatCurrency({colorize:true, region: 'es-MX'});"/>--%>
                                            <%--<cc1:FilteredTextBoxExtender ID="FTE_Txt_Monto_Solicitud_Pago" runat="server" 
                                            TargetControlID="Txt_Monto_Solicitud_Pago" FilterType="Custom, Numbers" ValidChars="-,."/>  --%>
                                        <asp:ImageButton ID="Btn_Agregar_Documento" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_add.png"
                                            CssClass="Img_Button" ToolTip="Agregar"  Height="15px" Width="15px" OnClick="Btn_Agregar_Documento_Click" AutoPostBack="true" /> 
                                    </td>
                                </tr>
                                </table>                                
                                    </asp:Panel>
                                    <script type="text/javascript" language="javascript">
                                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(fin_peticion);

                                        function fin_peticion() {
                                            $(document).ready(function() {
                                                $("select[id$=Cmb_Operacion]").change(function() {
                                                    $(".Cmb_Operacion option:selected").each(function() {
                                                        var Tipo = $(this).val();
                                                        if (Tipo == 3 || Tipo == 2) {
                                                            $("#Td_Cedular").show();
                                                            $("#Td_Txt_Reten_Cedular").show();
                                                            $("#Td_encabezado1").show();
                                                            $("#Td_encabezado2").hide();
                                                            $('#Txt_Fectura_ISR').show();
                                                            $('#Txt_Fectura_Reten_IVA').show();
                                                            $('#Txt_Fectura_ISH').hide();
                                                            $('#Txt_Fectura_IEPS').hide();
                                                            $("#Td_Reten_Iva").show();
                                                            $('#Td_Encab_Iva').show();
                                                            $('#Td_Encab_Mes').hide();
                                                            $('#Td_Iva').show();
                                                            $('#Td_Mes').hide();
                                                            $('#Td_ISH').hide();
                                                            $("#Td_ISR").show();
                                                            $('#Td_IEPS').hide();
                                                            $("#Td_Reten_Iva").show();
                                                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                                                if (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value != "") {
                                                                    document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                } else {
                                                                    document.getElementById("<%=Txt_IVA.ClientID%>").value = 0;
                                                                }
                                                                if (document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value != "") {
                                                                    document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                } else {
                                                                    document.getElementById("<%=Txt_ISR.ClientID%>").value = 0;
                                                                }
                                                                if (document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value != "") {
                                                                    document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                } else {
                                                                    document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                                                                }
                                                                if (document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value != "") {
                                                                    document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                } else {
                                                                    document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;
                                                                }
//                                                                document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
//                                                                document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
//                                                                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
//                                                                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value))) * 100) / 100; ;
                                                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                            }
                                                        }
                                                        if (Tipo == 1) {

                                                            $("#Td_Cedular").hide();
                                                            $("#Td_Txt_Reten_Cedular").hide();
                                                            $("#Td_encabezado1").hide();
                                                            $("#Td_encabezado2").show();
                                                            $('#Txt_Fectura_ISR').hide();
                                                            $("#Td_Reten_Iva").hide();
                                                            $('#Txt_Fectura_Reten_IVA').hide();
                                                            $('#Txt_Fectura_ISH').show();
                                                            $('#Txt_Fectura_IEPS').show();
                                                            $('#Td_Encab_Iva').show();
                                                            $('#Td_Encab_Mes').hide();
                                                            $('#Td_ISH').show();
                                                            $("#Td_ISR").hide();
                                                            $('#Td_IEPS').show();
                                                            $('#Td_Iva').show();
                                                            $('#Td_Mes').hide();
                                                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                                                document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value))) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                                document.getElementById("<%=Txt_ISR.ClientID%>").value = 0;
                                                                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                                                                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;
                                                            }
                                                        }
                                                        if (Tipo == 4) {
                                                            $("#Td_Cedular").hide();
                                                            $("#Td_Txt_Reten_Cedular").hide();
                                                            $('#Txt_Fectura_ISR').show();
                                                            $('#Txt_Fectura_Reten_IVA').hide();
                                                            $("#Td_Reten_Iva").hide();
                                                            $("#Td_ISR").show();
                                                            $("#Td_ISH").hide();
                                                            $("#Td_IEPS").hide();
                                                            $('#Txt_Fectura_ISH').hide();
                                                            $('#Txt_Fectura_IEPS').hide();
                                                            $('#Td_Encab_Iva').hide();
                                                            $('#Td_Encab_Mes').show();
                                                            $('#Td_Iva').hide();
                                                            $('#Td_Mes').show();
                                                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                                                try {
                                                                    var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                                                                    $.ajax({
                                                                        url: "Controlador_Contabilidad.aspx?" + cadena,
                                                                        type: 'POST',
                                                                        async: false,
                                                                        cache: false,
                                                                        success: function(data) {
                                                                            if (data != null) {
                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                                                            }
                                                                            else {
                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                                                            }
                                                                        }
                                                                    });
                                                                } catch (Ex) {
                                                                    $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                                                                }
                                                                if (document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value != "") {
                                                                    document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;

                                                                } else {
                                                                    document.getElementById("<%=Txt_ISR.ClientID%>").value = 0;
                                                                }
                                                                //document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                                document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                                                                document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;
                                                            }
                                                        }
                                                    });
                                                }).trigger('change');
                                            });
                                            function calculo2() {
                                                if (document.getElementById("<%=Txt_IVA.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_IVA.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Txt_ISR.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_ISR.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Txt_ISH.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_ISH.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Txt_IEPS.ClientID%>").value < 0) {
                                                    document.getElementById("<%=Txt_IEPS.ClientID%>").value = "0";
                                                }
                                                if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 2 || document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 3) {
                                                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = (parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value));
                                                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                } else {
                                                    if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 1) {
                                                        document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = (parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value));
                                                        document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                    } else {
                                                        if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 4) {
                                                            document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                                            document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                            //                                                        if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                                            //                                                            try{
                                                            //                                                                        var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses="+document.getElementById("<%=Txt_Meses.ClientID%>").value +"&";
                                                            //                                                                        $.ajax({
                                                            //                                                                        url: "Controlador_Contabilidad.aspx?"+ cadena,
                                                            //                                                                        type: 'POST',
                                                            //                                                                        async: false,
                                                            //                                                                        cache: false,
                                                            //                                                                        success: function(data) {
                                                            //                                                                            if (data != null) {
                                                            //                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                                            //                                                                            }
                                                            //                                                                            else {
                                                            //                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                                            //                                                                            }
                                                            //                                                                        }
                                                            //                                                                    });
                                                            //                                                                } catch (Ex) {
                                                            //                                                                    $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                                                            //                                                                }
                                                            //                                                                document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                                            //                                                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                                            //                                                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                            //                                                        }
                                                        }
                                                    }
                                                }
                                            }
                                            function calculo() {
                                                if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 2 || document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 3) {
                                                    if (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value != "") {
                                                        document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    } else {
                                                        document.getElementById("<%=Txt_IVA.ClientID%>").value = 0;
                                                    }
                                                    if (document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value != "") {
                                                        document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    } else {
                                                        document.getElementById("<%=Txt_ISR.ClientID%>").value = 0;
                                                    }
                                                    if (document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value != "") {
                                                        document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    } else {
                                                        document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = 0;
                                                    }
                                                    if (document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value != "") {
                                                        document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    } else {
                                                        document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = 0;
                                                    }
                                                    //                    document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    //                    document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (parseFloat(document.getElementById("<%=Txt_ISR_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    //                    document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_reten_iva_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    //                    document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_Reten_Cedular_parametro.ClientID%>").value) / 100)) * 100) / 100;
                                                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_IVA.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_Reten_Cedular.ClientID%>").value))) * 100) / 100; ;
                                                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                } else {
                                                    if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 1) {
                                                        if (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value != "") {
                                                            document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value / 100)) * 100) / 100;
                                                        } else {
                                                            document.getElementById("<%=Txt_IVA.ClientID%>").value = 0;
                                                        }
                                                        //document.getElementById("<%=Txt_IVA.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value * (document.getElementById("<%=Txt_iva_parametro.ClientID%>").value / 100)) * 100) / 100;
                                                        document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round(((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IVA.ClientID%>").value)) - (parseFloat(document.getElementById("<%=Txt_ISH.ClientID%>").value) + parseFloat(document.getElementById("<%=Txt_IEPS.ClientID%>").value))) * 100) / 100;
                                                        document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                    } else {
                                                        if (document.getElementById("<%=Cmb_Operacion.ClientID%>").value == 4) {
                                                            if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0 && document.getElementById("<%=Txt_Meses.ClientID%>").value > 0) {
                                                                try {
                                                                    var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                                                                    $.ajax({
                                                                        url: "Controlador_Contabilidad.aspx?" + cadena,
                                                                        type: 'POST',
                                                                        async: false,
                                                                        cache: false,
                                                                        success: function(data) {
                                                                            if (data != null) {
                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                                                            }
                                                                            else {
                                                                                document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                                                            }
                                                                        }
                                                                    });
                                                                } catch (Ex) {
                                                                    $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                                                                }
                                                                document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                                                document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                            }
                                                        }
                                                    }
                                                }
                                                if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value <= 0) {
                                                    document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = "0";
                                                    document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                }
                                            }
                                            function Calculo3() {
                                                if (document.getElementById("<%=Txt_Meses.ClientID%>").value != "" && document.getElementById("<%=Txt_Meses.ClientID%>").value != 0) {
                                                    if (document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value > 0) {
                                                        try {
                                                            var cadena = "Accion=Porcentaje&valor=" + document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value + "&meses=" + document.getElementById("<%=Txt_Meses.ClientID%>").value + "&";
                                                            $.ajax({
                                                                url: "Controlador_Contabilidad.aspx?" + cadena,
                                                                type: 'POST',
                                                                async: false,
                                                                cache: false,
                                                                success: function(data) {
                                                                    if (data != null) {
                                                                        document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = data.trim();
                                                                    }
                                                                    else {
                                                                        document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value = 0;
                                                                    }
                                                                }
                                                            });
                                                        } catch (Ex) {
                                                            $.messager.alert('Mensaje', '. Error: [' + Ex + ']');
                                                        }
                                                        document.getElementById("<%=Txt_ISR.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) * (parseFloat(document.getElementById("<%=Txt_porcentaje_ISR_HA.ClientID%>").value) / 100)) * 100) / 100;
                                                        document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value = Math.round((parseFloat(document.getElementById("<%=Txt_Subtotal_factura.ClientID%>").value) - (parseFloat(document.getElementById("<%=Txt_ISR.ClientID%>").value))) * 100) / 100;
                                                        document.getElementById("<%=Txt_Monto_partida.ClientID%>").value = parseFloat(document.getElementById("<%=Txt_Monto_Solicitud_Pago.ClientID%>").value);
                                                    }
                                                }
                                            }
                                        }
                                   </script> 
                                   </ContentTemplate>
                              </asp:UpdatePanel>
                                <table  width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                <tr align="center">
                                        <td>
                                        <div style="overflow:auto;width:98%;height:80px;vertical-align:top;">
                                                <asp:GridView ID="Grid_Documentos" runat="server" Width="97%"
                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                                     OnRowDataBound="Grid_Documentos_RowDataBound">
                                                    <Columns>
                                                            <asp:TemplateField HeaderText="Link">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="Hyp_Lnk_Ruta" ForeColor="Blue" runat="server" >Archivo</asp:HyperLink></ItemTemplate><HeaderStyle HorizontalAlign ="Left" width ="5%" />
                                                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                                                            </asp:TemplateField> 
                                                            <asp:BoundField DataField="Partida" HeaderText="Partida" ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                           <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                           </asp:BoundField>                                                                                                         
                                                           <asp:BoundField DataField="Fecha_Documento" HeaderText="Fecha" DataFormatString="{0:dd/MMMM/yyyy}" ItemStyle-Font-Size="X-Small">
                                                             <HeaderStyle HorizontalAlign="Center" Width="0%" />
                                                             <ItemStyle HorizontalAlign="Center" Width="0%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Iva" HeaderText="Iva" ItemStyle-Font-Size="X-Small">
                                                             <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                             <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Operacion" HeaderText="Operación" ItemStyle-Font-Size="X-Small">
                                                             <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                             <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Monto" HeaderText="Monto" DataFormatString="{0:c}" ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                           <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="No_Documento" HeaderText="Documento" ItemStyle-Font-Size="X-Small">
                                                             <HeaderStyle HorizontalAlign="Center" Width="0%" />
                                                             <ItemStyle HorizontalAlign="Center" Width="0%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Partida_Id" HeaderText="Partida_Id" DataFormatString="{0:c}" ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                           </asp:BoundField>                                                           
                                                           <asp:BoundField DataField="Ruta" HeaderText="Ruta" ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Archivo" HeaderText="Archivo" ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Proyecto_Programa_Id" HeaderText="Proyecto_Programa_Id" ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Fte_Financiamiento_Id" HeaderText="Fte_Financiamineto_Id"  ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Nombre_Proveedor_Fact" HeaderText="Nombre_Proveedor_Fact"  ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="RFC" HeaderText="RFC"  ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="CURP" HeaderText="CURP"  ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Retencion_ISR" HeaderText="ISR"  ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Retencion_IVA" HeaderText="Ret_IVA"  ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Retencion_Celula" HeaderText="Celular"  ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                           </asp:BoundField>
                                                           <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <center>
                                                                    <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                                        CausesValidation="false" 
                                                                        ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                                        OnClick="Btn_Eliminar_Partida" 
                                                                        OnClientClick="return confirm('¿Está seguro de eliminar de la tabla la cuenta seleccionada?');" />
                                                                </center>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="5%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="ISH" HeaderText="ISH"  ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                           </asp:BoundField>
                                                        <asp:BoundField DataField="IEPS" HeaderText="IEPS"  ItemStyle-Font-Size="X-Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                           </asp:BoundField>
                                                    </Columns>                                                    
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="tblHead" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView> 
                                           </div>                            
                                        </td>
                                    </tr>
                                    <tr>
                                    <td>&nbsp;</td></tr></table><table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                <tr>
                                    <td style="width:70%" colspan="2">
                                        &nbsp;
                                    </td>
                                    <td style="width:30%">
                                        <asp:Label ID="Lbl_Subtotal" runat="server" Text="SubTotal:"></asp:Label>&nbsp;
                                        <asp:TextBox ID="Txt_Subtotal" runat="server" Width="70%" CssClass="text_cantidades_grid" TabIndex="10" ReadOnly="True"
                                            onblur="$('input[id$=Txt_Subtotal]').formatCurrency({colorize:true, region: 'es-MX'});" />
                                            <cc1:FilteredTextBoxExtender ID="Filt_Txt_Subtotal" runat="server" 
                                            TargetControlID="Txt_Total" FilterType="Custom, Numbers" ValidChars="-,."/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%">*Beneficiario</td><td style="width:50%">
                                        <asp:TextBox ID="Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" MaxLength="200" TabIndex="11" Width="90%" ReadOnly="true"></asp:TextBox><cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" TargetControlID="Txt_Nombre_Proveedor_Solicitud_Pago"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td style="width:30%">
                                        <asp:Label ID="Lbl_Total" runat="server" Text="Total:"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="Txt_Total" runat="server" Width="70%" CssClass="text_cantidades_grid" TabIndex="10" ReadOnly="True"
                                            onblur="$('input[id$=Txt_Total]').formatCurrency({colorize:true, region: 'es-MX'});" />
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                                            TargetControlID="Txt_Total" FilterType="Custom, Numbers" ValidChars="-,."/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%">*Concepto</td><td colspan="2">
                                        <asp:TextBox ID="Txt_Concepto_Solicitud_Pago" runat="server" MaxLength="250" TabIndex="14" Width="99%"></asp:TextBox><cc1:FilteredTextBoxExtender ID="FTE_Txt_Concepto_Solicitud_Pago" runat="server" TargetControlID="Txt_Concepto_Solicitud_Pago"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>                                
                            </table> 
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td>&nbsp;</td></tr><tr align="center">
                        <td style="width:100%">
                        <div style="overflow:auto;height:250px;width:95%;vertical-align:top;">
                            <asp:GridView ID="Grid_Solicitud_Pagos" runat="server"   Width="99%"
                                AutoGenerateColumns="False" CssClass="GridView_1" HeaderStyle-CssClass="tblHead" 
                                onselectedindexchanged="Grid_Solicitud_Pagos_SelectedIndexChanged" 
                                onsorting="Grid_Solicitud_Pagos_Sorting">
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="5%" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="No_Solicitud_Pago" HeaderText="No Solicitud" Visible="True">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Solicitud" HeaderText="Solicitud" Visible="True" SortExpression="Descripcion">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Beneficiario" HeaderText="Beneficiario" Visible="True">
                                        <HeaderStyle HorizontalAlign="Left" Width="18%" />
                                        <ItemStyle HorizontalAlign="Left" Width="18%" />
                                    </asp:BoundField>                             
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="True" SortExpression="Estatus">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    </asp:BoundField>
                                </Columns>
                                <SelectedRowStyle CssClass="GridSelected" />
                                <PagerStyle CssClass="GridHeader" />
                                <HeaderStyle CssClass="tblHead" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView> 
                            </div>                           
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
        style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
        <asp:Panel ID="Pnl_Busqueda_Cabecera" runat="server" 
            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
            <table width="99%">
                <tr>
                    <td style="color:Black;font-size:12;font-weight:bold;">
                       <asp:Image ID="Img_Informatcion_Autorizacion" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                         B&uacute;squeda: Solicitud de Pagos
                    </td>
                    <td align="right" style="width:10%;">
                       <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Ventana_Click"/>  
                    </td>
                </tr>
            </table>            
        </asp:Panel>                                                                          
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Solicitud_Pagos" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Solicitud_Pagos" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Solicitud_Pagos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>                                                             
                                <table width="100%">
                                    <tr>
                                        <td style="width:100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>                         
                                        </td>
                                    </tr>     
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">No Reserva</td><td style="width:30%;text-align:left;font-size:11px;">
                                            <asp:TextBox ID="Txt_Busqueda_No_Reserva" runat="server" Width="98%" TabIndex="11" MaxLength="50"/>
                                            <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_No_Reserva" runat="server" FilterType="Numbers"
                                                TargetControlID="Txt_Busqueda_No_Reserva" />
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Reserva" runat="server" WatermarkCssClass="watermarked"
                                                TargetControlID ="Txt_Busqueda_No_Reserva" WatermarkText="Busqueda por No Reserva" />                                                                                                                                          
                                        </td> 
                                        <td colspan="2" style="width:50%;text-align:left;font-size:11px;"></td>                     
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">No Solicitud</td><td style="width:30%;text-align:left;font-size:11px;">
                                            <asp:TextBox ID="Txt_Busqueda_No_Solicitud_Pago" runat="server" Width="98%" TabIndex="11" MaxLength="10"/>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_No_Solicitud_Pago" runat="server" FilterType="Numbers"
                                                TargetControlID="Txt_Busqueda_No_Solicitud_Pago" />
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_No_Solicitud_Pago" runat="server" WatermarkCssClass="watermarked"
                                                TargetControlID ="Txt_Busqueda_No_Solicitud_Pago" WatermarkText="Busqueda por No Solicitud" />                                                                                                                                          
                                        </td> 
                                        <td colspan="2" style="width:50%;text-align:left;font-size:11px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px">Estatus</td><td style="width:30%; text-align:left; font-size:11px">
                                            <asp:DropDownList ID="Cmb_Busqueda_Estatus_Solicitud_Pago" runat="server" Width="101%" TabIndex="2">
                                                <asp:ListItem>&lt; - Seleccione - &gt;</asp:ListItem><asp:ListItem>PENDIENTE</asp:ListItem><asp:ListItem>PREAUTORIZADO</asp:ListItem><asp:ListItem>PORPAGAR</asp:ListItem><asp:ListItem>PAGADO</asp:ListItem><asp:ListItem>EJERCIDO</asp:ListItem><asp:ListItem>DOCUMENTADO</asp:ListItem></asp:DropDownList></td><td colspan="2" style="width:50%;text-align:left;font-size:11px;"></td>
                                    </tr>                                                                                                   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">Tipo Solicitud</td><td colspan="3" style="width:80%;text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Tipo_Solicitud" runat="server" Width="100%" />                                                                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">Depedencia</td><td colspan="3" style="width:80%;text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Dependencia" runat="server" Width="100%" />                                                                                
                                        </td>
                                    </tr>                                                                                                          
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">Fecha Inicio</td><td style="width:30%;text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Fecha_Inicio" runat="server" Width="85%" MaxLength="1" TabIndex="13" Enabled="false"/>
                                            <cc1:CalendarExtender ID="CE_Txt_Busqueda_Fecha_Inicio" runat="server" PopupButtonID="Btn_Busqueda_Fecha_Inicio"
                                                TargetControlID="Txt_Busqueda_Fecha_Inicio" Format="dd/MMM/yyyy" OnClientShown="calendarShown"/>
                                            <asp:ImageButton ID="Btn_Busqueda_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                                ToolTip="Seleccione la Fecha"/>
                                        </td>
                                        <td style="width:20%;text-align:left;font-size:11px;">Fecha Fin</td><td style="width:30%;text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Fecha_Fin" runat="server" Width="85%" MaxLength="1" TabIndex="14" Enabled="false"/>
                                            <cc1:CalendarExtender ID="CE_Txt_Busqueda_Fecha_Fin" runat="server" OnClientShown="calendarShown"
                                                TargetControlID="Txt_Busqueda_Fecha_Fin" PopupButtonID="Btn_Busqueda_Fecha_Fin" Format="dd/MMM/yyyy"/>
                                            <asp:ImageButton ID="Btn_Busqueda_Fecha_Fin" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                                ToolTip="Seleccione la Fecha"/> 
                                        </td>                                                            
                                    </tr>                                                                                                      
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>                                    
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Solicitud_Pago" runat="server"  Text="Busqueda de Solicitud de Pago" CssClass="button"  
                                                    CausesValidation="false" Width="300px" TabIndex="15" OnClick="Btn_Busqueda_Solicitud_Pago_Click"/> 
                                            </center>
                                        </td>                                                     
                                    </tr>                                                       
                                </table>                                                                                                                                                              
                            </ContentTemplate>                                                                   
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>                                                   
        </div>
    </asp:Panel>
</asp:Content>

