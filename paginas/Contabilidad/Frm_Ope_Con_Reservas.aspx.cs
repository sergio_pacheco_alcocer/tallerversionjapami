﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sindicatos.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using AjaxControlToolkit;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Globalization;
using System.Text.RegularExpressions;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Area_Funcional.Negocio;
using JAPAMI.Catalogo_Compras_Proyectos_Programas.Negocio;
using JAPAMI.SAP_Operacion_Departamento_Presupuesto.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Compromisos_Contabilidad.Negocios;
using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Deudores.Negocios;
using JAPAMI.Solicitud_Pagos.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Reservas : System.Web.UI.Page
{
    private static String P_Dt_Reservas = "P_Dt_Reservas";
    private static String P_Dt_Programas = "P_Dt_Programas";
    private static String P_Dt_Partidas = "P_Dt_Partidas";
    private static String Importe = "Importe";

    #region (Page Load)
        protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

        try
        {
            if (!IsPostBack)
            {
                Session["P_Dt_Reservas"] = null;
                Session["P_Dt_Programas"] = null;
                Session["P_Dt_Partidas"] = null;
                Session["Importe"] = null;

                ViewState["SortDirection"] = "ASC";

                Limpia_Controles();

                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);
                Txt_Fecha_Inicio.Text = _DateTime.ToString("dd/MMM/yyyy");
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy");
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                Consultar_Tipos_Solicitud();
                Tr_Busqueda.Visible = true;
                Txt_Fecha_Factura_Solicitud.Text = DateTime.Now.ToString("dd/MMM/yyyy");
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// 
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            //Botones.Add(Btn_Eliminar);
            //Botones.Add(Btn_Mostrar_Popup_Busqueda);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region(Metodos Generales)
        //*******************************************************************************
        // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Reservas
        // DESCRIPCIÓN: Llena el grid principal de reservas
        // RETORNA: 
        // CREO: Sergio Manuel Gallardo Andrade
        // FECHA_CREO: 17/noviembre/2011 
        // MODIFICO:Leslie Gonzalez Vazquez
        // FECHA_MODIFICO:19/Enero/2012
        // CAUSA_MODIFICACIÓN: Hice la validacion del rango de fechas
        //********************************************************************************/
       public void Llenar_Grid_Reservas()
        {
            Cls_Ope_Con_Reservas_Negocio Reserva_Negocio = new Cls_Ope_Con_Reservas_Negocio();
            DataTable Dt_Partidas = new DataTable();
            String Rol_Grupo = "";
            try
            {
                if (!String.IsNullOrEmpty(Txt_Fecha_Inicio.Text.Trim()) && !String.IsNullOrEmpty(Txt_Fecha_Final.Text.Trim()))
                {
                    if (Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()).CompareTo(Convert.ToDateTime(Txt_Fecha_Final.Text.Trim())) > 0)
                    {
                        Mostrar_Informacion("La fecha inicial no puede ser mayor a la fecha final", true);
                        return;
                    }
                    Reserva_Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                    Reserva_Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));

                    //obtenenmos las dependencias
                    Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                    Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                    DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                    Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Unidad_Responsable_Busqueda, Dt_Dependencias, 1, 0);
                    Cmb_Unidad_Responsable_Busqueda.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
                    //Verificar si su rol es jefe de dependencia, admin de modulo o admin de sistema
                    DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                    if (Dt_Grupo_Rol != null)
                    {
                        String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                        if (Grupo_Rol == "00003")
                        {
                            Reserva_Negocio.P_Grupo_Dependencia = "'" + Rol_Grupo + "'";
                        }
                        else
                        {
                            Reserva_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                        }
                    }
                    //DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                    //if (Dt_Grupo_Rol != null)
                    //{
                    //    String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                    //    if (Grupo_Rol == "00001" || Grupo_Rol == "00002")
                    //    {
                    //        Reserva_Negocio.P_Dependencia_ID = "'" + Cls_Sessiones.Dependencia_ID_Empleado + "'";
                    //    }
                    //    else
                    //    {
                    //        DataTable Dt_URs = Cls_Util.Consultar_URs_De_Empleado(Cls_Sessiones.Empleado_ID);
                    //        if (Dt_URs.Rows.Count > 1)
                    //        {
                    //            foreach (DataRow Dr in Dt_URs.Rows)
                    //            {
                    //                Reserva_Negocio.P_Dependencia_ID += "'" + Dr["DEPENDENCIA_ID"].ToString().Trim() + "', ";
                    //            }
                    //            Reserva_Negocio.P_Dependencia_ID = Reserva_Negocio.P_Dependencia_ID.Substring(0, Reserva_Negocio.P_Dependencia_ID.Length - 2);
                    //        }
                    //    }
                    //}
                    if (!String.IsNullOrEmpty(Txt_No_Folio.Text.Trim()))
                    {
                        Reserva_Negocio.P_No_Reserva = Txt_No_Folio.Text.Trim();
                        Dt_Partidas = Reserva_Negocio.Consultar_Reservas_Detalladas();
                        if (Dt_Partidas.Rows.Count > 0)
                        {
                            Cmb_Estatus.SelectedValue = Dt_Partidas.Rows[0][Ope_Psp_Reservas.Campo_Estatus].ToString();
                        }
                        Session["Dt_Partidas_Asignadas"] = Dt_Partidas;
                        Llenar_Grid_Partida_Asignada(false);
                    }
                    else 
                    {
                        Dt_Partidas = new DataTable();
                        Session["Dt_Partidas_Asignadas"] = Dt_Partidas ;
                        Llenar_Grid_Partida_Asignada(false);
                    }
                    
                }
            }
            catch (Exception Ex) 
            {
                throw new Exception("Error al llenado de la tabla de reservas Erro:[" + Ex.Message + "]");
            }
        }
       //*******************************************************************************
       // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Partidas_De_Reserva
       // DESCRIPCIÓN: Llena el grid principal de reservas con las partidas de la reserva consultada
       // RETORNA: 
       // CREO: Sergio Manuel Gallardo Andrade
       // FECHA_CREO: 26/Abril/2013 
       // MODIFICO:
       // FECHA_MODIFICO:
       // CAUSA_MODIFICACIÓN:
       //********************************************************************************/
       public void 
           Llenar_Grid_Partidas_De_Reserva()
       {
           Cls_Ope_Con_Reservas_Negocio Reserva_Negocio = new Cls_Ope_Con_Reservas_Negocio();
           DataTable Dt_Partidas = new DataTable();
           DataTable Dt_Partidas_Reserva = new DataTable(); 
           String Rol_Grupo = "";
           DataRow Fila_Reserva;
           Cls_Ope_Con_Reservas_Negocio Rs_Consulta_Dependencias = new Cls_Ope_Con_Reservas_Negocio();
           DataTable Dt_Consulta = new DataTable();
           try
           {
               if (!String.IsNullOrEmpty(Txt_Fecha_Inicio.Text.Trim()) && !String.IsNullOrEmpty(Txt_Fecha_Final.Text.Trim()))
               {
                   if (Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()).CompareTo(Convert.ToDateTime(Txt_Fecha_Final.Text.Trim())) > 0)
                   {
                       Mostrar_Informacion("La fecha inicial no puede ser mayor a la fecha final", true);
                       return;
                       Habilitar_Controles("Inicial");
                   }
                   Reserva_Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                   Reserva_Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));

                   //obtenenmos las dependencias
                   Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                   Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                   DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                   Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
                   Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Unidad_Responsable_Busqueda, Dt_Dependencias, 1, 0);
                   Cmb_Unidad_Responsable_Busqueda.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
                   //Verificar si su rol es jefe de dependencia, admin de modulo o admin de sistema
                   DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                   if (Dt_Grupo_Rol != null)
                   {
                       String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                       if (Grupo_Rol == "00003")
                       {
                           Reserva_Negocio.P_Grupo_Dependencia = "'" + Rol_Grupo + "'";
                       }
                       else
                       {
                           String Conjunto_Dependencias = "";
                           DataTable Dt_URs = Cls_Util.Consultar_URs_De_Empleado(Cls_Sessiones.Empleado_ID);
                           foreach (DataRow Registro_Dependencia in Dt_URs.Rows)
                           {
                               Conjunto_Dependencias =Conjunto_Dependencias +"'"+Registro_Dependencia["DEPENDENCIA_ID"].ToString()+"',";
                           } 
                           Conjunto_Dependencias = Conjunto_Dependencias.Substring(0, Conjunto_Dependencias.Length - 1);
                           Reserva_Negocio.P_Dependencia_ID = Conjunto_Dependencias;
                       }
                   }
                   if (!String.IsNullOrEmpty(Txt_No_Folio.Text.Trim()))
                   {
                       Reserva_Negocio.P_No_Reserva = Txt_No_Folio.Text.Trim();
                       Dt_Partidas = Reserva_Negocio.Consultar_Reservas_Detalladas();
                       if (Dt_Partidas != null)
                       {
                           if (Dt_Partidas.Rows.Count > 0)
                           {
                               Cmb_Estatus.SelectedValue = Dt_Partidas.Rows[0][Ope_Psp_Reservas.Campo_Estatus].ToString();
                               if (Dt_Partidas_Reserva.Rows.Count <= 0)
                               {
                                   Dt_Partidas_Reserva.Columns.Add("NO_RESERVA", System.Type.GetType("System.String"));
                                   Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                                   Dt_Partidas_Reserva.Columns.Add("FTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                                   Dt_Partidas_Reserva.Columns.Add("CLAVE_FINANCIAMIENTO", System.Type.GetType("System.String"));
                                   Dt_Partidas_Reserva.Columns.Add("PROYECTO_PROGRAMA_ID", System.Type.GetType("System.String"));
                                   Dt_Partidas_Reserva.Columns.Add("CLAVE_PROGRAMA", System.Type.GetType("System.String"));
                                   Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                                   Dt_Partidas_Reserva.Columns.Add("PROGRAMA", System.Type.GetType("System.String"));
                                   Dt_Partidas_Reserva.Columns.Add("CLAVE_PARTIDA", System.Type.GetType("System.String"));
                                   Dt_Partidas_Reserva.Columns.Add("DISPONIBLE", System.Type.GetType("System.Double"));
                                   Dt_Partidas_Reserva.Columns.Add("COMPROMETIDO", System.Type.GetType("System.Double"));
                                   Dt_Partidas_Reserva.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                               }
                               foreach (DataRow Registro_Reserva in Dt_Partidas.Rows)
                               {
                                   Fila_Reserva = Dt_Partidas_Reserva.NewRow();
                                   //Se consulta el presupuesto
                                   Dt_Consulta = new DataTable();
                                   Rs_Consulta_Dependencias.P_Dependencia_ID = Registro_Reserva["DEPENDENCIA_ID"].ToString().Trim();
                                   Rs_Consulta_Dependencias.P_Ramo_33 = "NO";
                                   Rs_Consulta_Dependencias.P_Partida_ID = Registro_Reserva["PARTIDA_ID"].ToString().Trim();
                                   Rs_Consulta_Dependencias.P_Fuente_Financiamiento = Registro_Reserva["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                   Rs_Consulta_Dependencias.P_Proyecto_Programa_ID = Registro_Reserva["PROGRAMA_ID"].ToString().Trim();
                                   Rs_Consulta_Dependencias.P_Capitulo_ID = Registro_Reserva["CAPITULO_ID"].ToString().Trim();
                                   Rs_Consulta_Dependencias.P_Filtro_Campo_Mes = Consultar_Nombre_Mes(String.Format("{0:0#}", (DateTime.Now.Month)));
                                   Dt_Consulta = Rs_Consulta_Dependencias.Consultar_Reservas_Unidad_Responsable_y_Partida();

                                   Fila_Reserva["NO_RESERVA"] = Registro_Reserva["NO_RESERVA"].ToString().Trim();
                                   Fila_Reserva["DEPENDENCIA_ID"] = Registro_Reserva["DEPENDENCIA_ID"].ToString().Trim();
                                   Fila_Reserva["FTE_FINANCIAMIENTO_ID"] = Registro_Reserva["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                   Fila_Reserva["CLAVE_FINANCIAMIENTO"] = Dt_Consulta.Rows[0]["CLAVE_FINANCIAMIENTO"].ToString().Trim();
                                   Fila_Reserva["PROYECTO_PROGRAMA_ID"] = Registro_Reserva["PROGRAMA_ID"].ToString().Trim();
                                   Fila_Reserva["CLAVE_PROGRAMA"] = Dt_Consulta.Rows[0]["CLAVE_PROGRAMA"].ToString().Trim();
                                   Fila_Reserva["PARTIDA_ID"] = Registro_Reserva["PARTIDA_ID"].ToString().Trim();
                                   Fila_Reserva["PROGRAMA"] = Registro_Reserva["PROGRAMA"].ToString().Trim(); ;
                                   Fila_Reserva["CLAVE_PARTIDA"] = Dt_Consulta.Rows[0]["CLAVE_PARTIDA"].ToString().Trim();
                                   Fila_Reserva["DISPONIBLE"] = Convert.ToDouble(Dt_Consulta.Rows[0]["DISPONIBLE"].ToString().Trim());
                                   Fila_Reserva["COMPROMETIDO"] = Convert.ToDouble(Registro_Reserva["SALDO"].ToString().Trim());
                                   Fila_Reserva["CAPITULO_ID"] = Registro_Reserva["CAPITULO_ID"].ToString().Trim();
                                   Dt_Partidas_Reserva.Rows.Add(Fila_Reserva);
                                   Dt_Partidas_Reserva.AcceptChanges();
                               }
                               Grid_Partidas_Reserva.Columns[0].Visible = true;
                               Grid_Partidas_Reserva.Columns[1].Visible = true;
                               Grid_Partidas_Reserva.Columns[3].Visible = true;
                               Grid_Partidas_Reserva.Columns[5].Visible = true;
                               Grid_Partidas_Reserva.Columns[6].Visible = true;
                               Grid_Partidas_Reserva.Columns[11].Visible = true;
                               Grid_Partidas_Reserva.Columns[12].Visible = true;
                               if (Cmb_Tipo_Modificacion.SelectedIndex == 0)
                               {
                                   Dt_Partidas_Reserva.DefaultView.RowFilter = "Disponible>0";
                               }
                               else
                               {
                                   Dt_Partidas_Reserva.DefaultView.RowFilter = "Comprometido>0";
                               }
                               Grid_Partidas_Reserva.DataSource = Dt_Partidas_Reserva.DefaultView;
                               Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Reserva.DefaultView.ToTable();
                               Grid_Partidas_Reserva.DataBind();
                               Grid_Partidas_Reserva.Columns[0].Visible = false;
                               Grid_Partidas_Reserva.Columns[1].Visible = false;
                               Grid_Partidas_Reserva.Columns[3].Visible = false;
                               Grid_Partidas_Reserva.Columns[5].Visible = false;
                               Grid_Partidas_Reserva.Columns[6].Visible = false;
                               Grid_Partidas_Reserva.Columns[11].Visible = false;
                               Grid_Partidas_Reserva.Columns[12].Visible = false;
                               Cmb_Tipo_Solicitud_Pago.Enabled = false;
                               Cmb_Tipo_Reserva.Enabled = false;
                               Cmb_Unidad_Responsable_Busqueda.Enabled = false;
                               Txt_Conceptos.Enabled = false;
                               Btn_Busqueda_Partida.Enabled = false;
                               Div_Cabecera_Modificar.Visible = true;
                               Btn_Agregar.Enabled = false;
                           }
                           else
                           {
                               Mostrar_Informacion("La Reserva no existe o no se encuentra dentro del rango seleccionado \n Favor de Verificar la busqueda", true);
                               Div_Cabecera_Modificar.Visible = false;
                               Habilitar_Controles("Inicial");
                               return;
                           }
                       }
                       else
                       {
                           Mostrar_Informacion("La Reserva no existe o no se encuentra dentro del rango seleccionado \n Favor de Verificar la busqueda", true);
                           Div_Cabecera_Modificar.Visible = false;
                           Habilitar_Controles("Inicial");
                           return;
                       }
                   }
                   else
                   {
                       Mostrar_Informacion("La Reserva no existe o no se encuentra dentro del rango seleccionado \n Favor de Verificar la busqueda", true);
                       Div_Cabecera_Modificar.Visible = false;
                       Habilitar_Controles("Inicial");
                       return;
                   }

               }
           }
           catch (Exception Ex)
           {
               throw new Exception("Error al llenado de la tabla de reservas Erro:[" + Ex.Message + "]");
           }
       }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Inicializa_Controles
        /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
        ///               diferentes operaciones
        /// PARAMETROS  : 
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 13/Octubre/2011
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
       private void Inicializa_Controles()
       {
           try
           {
               Cmb_Estatus.SelectedIndex = 0;
               String Rol_Grupo = "";
               Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
               Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
               Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
               DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
               Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
               Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Unidad_Responsable_Busqueda, Dt_Dependencias, 1, 0);
               Cmb_Unidad_Responsable_Busqueda.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
               //Verificar si su rol es jefe de dependencia, admin de modulo o admin de sistema
               DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
               if (Dt_Grupo_Rol != null)
               {
                   String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                   if (Grupo_Rol != "00003")
                   {
                       Cmb_Unidad_Responsable_Busqueda.Enabled = false;
                       DataTable Dt_URs = Cls_Util.Consultar_URs_De_Empleado(Cls_Sessiones.Empleado_ID);
                       if (Dt_URs.Rows.Count > 1)
                       {
                           Cmb_Unidad_Responsable_Busqueda.Items.Clear();
                           Cmb_Unidad_Responsable_Busqueda.Enabled = true;
                           Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Unidad_Responsable_Busqueda, Dt_URs, 1, 0);
                           Cmb_Unidad_Responsable_Busqueda.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
                       }
                   }
                   else
                   {
                       DataTable Dt_URs = Cls_Util.Consultar_URS_Por_Grupo(Rol_Grupo);
                       if (Dt_URs.Rows.Count > 1)
                       {
                           Cmb_Unidad_Responsable_Busqueda.Enabled = false;
                           Cmb_Unidad_Responsable_Busqueda.Items.Clear();
                           Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Unidad_Responsable_Busqueda, Dt_URs, 1, 0);
                           Cmb_Unidad_Responsable_Busqueda.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
                       }
                   }
               }
               Cmb_Unidad_Responsable_Busqueda.Enabled = false;
               Llenar_Combos_Generales();
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message.ToString());
           }
       }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Limpiar_Controles
        /// DESCRIPCION : Limpia los controles que se encuentran en la forma
        /// PARAMETROS  : 
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 13/Octubre/2011
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Limpia_Controles()
        {
            try
            {
                Txt_No_Folio.Text = "";
                //Txt_Importe.Text = "";
                Txt_Conceptos.Text = "";
                //Txt_Saldo.Text = "";
                //Lbl_disponible.Text = "";
                Cmb_Tipo_Solicitud_Pago.SelectedIndex = -1;
                Cmb_Proveedor_Solicitud_Pago.SelectedIndex = -1;
                Cmb_Deudores.SelectedIndex = -1;
                Cmb_anticipos.SelectedIndex = -1;
                Txt_Deudor.Text = "";
                Cmb_Nombre_Empleado.SelectedIndex = -1; ;
                Txt_Nombre_Empleado.Text = "";
                Txt_Nombre_Proveedor_Solicitud_Pago.Text = "";
                Txt_Gastos_Anticipos.Text = "";
                Grid_Partida_Asignada.DataSource = null;
                Grid_Partidas.DataBind();
                Grid_Partida_Saldo.DataSource = null;
                Grid_Partida_Saldo.DataBind(); 
                if (Session["Dt_Documentos"] != null)
                {
                    Session.Remove("Dt_Documentos");
                }
                Grid_Documentos.DataSource = null;
                Grid_Documentos.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception("Limpiar_Controles " + ex.Message.ToString(), ex);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Habilitar_Controles
        /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
        ///               para a siguiente operación
        /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
        ///                          si es una alta, modificacion
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 11/Julio/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Habilitar_Controles(String Operacion)
        {
            Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
            try
            {
                Habilitado = false;
                switch (Operacion)
                {
                    case "Inicial":
                        Habilitado = false;
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Nuevo.Visible = true;
                        Btn_Modificar.Visible = false;
                        //Btn_Eliminar.Visible = true;
                        Btn_Nuevo.CausesValidation = false;
                        Btn_Modificar.CausesValidation = false;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Configuracion_Acceso("Frm_Ope_Con_Reservas.aspx");
                        Btn_Modificar.Visible = false;
                        Cmb_Estatus.Enabled= false;
                        Div_Partidas_A_Modificar.Visible = false;
                        Div_Cabecera_Modificar.Visible = false;
                        Div_Encabezados_Partidas_Disponibles.Visible = false;
                        Div_Partidas_Disponibles.Visible = false;
                        Tr_Busqueda_Partida_Especifica.Visible = true;
                        //Tr_Busqueda_Partida_Especifica.Style.Add("display", "block");
                        Tr_Busqueda.Visible = true;
                        break;

                    case "Nuevo":
                        Habilitado = true;
                        Btn_Nuevo.ToolTip = "Dar de Alta";
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Nuevo.Visible = true;
                        Btn_Modificar.Visible = false;
                        //Btn_Eliminar.Visible = false;
                        Btn_Nuevo.CausesValidation = true;
                        Btn_Modificar.CausesValidation = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Cmb_Estatus.Enabled = false;
                        Div_Partidas_A_Modificar.Visible = false;
                        Div_Cabecera_Modificar.Visible = false;
                        Div_Encabezados_Partidas_Disponibles.Visible = true;
                        Div_Partidas_Disponibles.Visible = true;
                        Tr_Busqueda_Partida_Especifica.Visible = true;
                        //Tr_Busqueda_Partida_Especifica.Style.Add("display", "block");
                        //tr_Saldo.Style.Add("display", "none"); 
                        Tr_Busqueda.Visible = false;
                        break;

                    case "Modificar":
                        Habilitado = true;
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Modificar.ToolTip = "Actualizar";
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Nuevo.Visible = false;
                        Btn_Modificar.Visible = true;
                        //Btn_Eliminar.Visible = false;
                        Btn_Nuevo.CausesValidation = true;
                        Btn_Modificar.CausesValidation = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        Cmb_Estatus.Enabled = true;
                        Div_Partidas_A_Modificar.Visible = true;
                        Div_Cabecera_Modificar.Visible = true;
                        Div_Encabezados_Partidas_Disponibles.Visible = false;
                        Div_Partidas_Disponibles.Visible = false;
                        Tr_Busqueda_Partida_Especifica.Visible = false;
                        //Tr_Busqueda_Partida_Especifica.Style.Add("display", "none");
                        //tr_Saldo.Style.Add("display", "block"); 
                        Tr_Busqueda.Visible = false;
                        break;
                }
                Btn_Fecha_Inicio.Enabled = !Habilitado;
                Btn_Fecha_Final.Enabled = !Habilitado;
                Txt_No_Folio.Enabled = !Habilitado;
                Txt_Fecha_Inicio.Enabled = false;
                Txt_Fecha_Final.Enabled = false;
                Cmb_Proveedor_Solicitud_Pago.Enabled = false;
                Cmb_Deudores.Enabled = false;
                Txt_Deudor.Enabled = false;
                Btn_Buscar_Deudor.Enabled = false;
                //Tr_Deudor_Anticipos.Style.Add("display", "none");
                //Tr_Deudor.Style.Add("display", "none");
                Tr_Deudor_Anticipos.Visible = false;
                Tr_Deudor_Anticipos_2.Visible = false;
                Tr_Deudor_Anticipos_3.Visible = false;
                Tr_Grid_Facturas.Visible = false;
                Tr_Deudor.Visible = false;
                Cmb_Nombre_Empleado.Enabled = false;
                Txt_Nombre_Empleado.Enabled = false;
                Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = false;
                Btn_Buscar_Empleado.Enabled = false;
                Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = false;
                Btn_Buscar_Reserva.Enabled = !Habilitado;
                //Tr_Empleado.Style.Add("display","none");
                //Tr_Proveedor.Style.Add("display", "none");
                //Tr_Div_Partidas.Style.Add("display", "none");
                Tr_Empleado.Visible = false;
                Tr_Proveedor.Visible = false;
                Tr_Div_Partidas.Visible = false;
                Txt_Fuente_Ramo.Enabled = Habilitado;
                Cmb_Unidad_Responsable_Busqueda.Enabled = Habilitado;
                Cmb_Tipo_Reserva.Enabled = Habilitado;
                Txt_Conceptos.Enabled = Habilitado;
                Cmb_Tipo_Solicitud_Pago.Enabled = Habilitado;
                Btn_Agregar.Enabled = Habilitado;
                Txt_Busqueda.Enabled = Habilitado;
                Btn_Busqueda_Partida.Enabled = Habilitado;
            }
            catch (Exception ex)
            {
                throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
            }
        }
        //*******************************************************************************
        // NOMBRE DE LA FUNCIÓN: Consultar_Nombre_Mes
        // DESCRIPCIÓN: Consultara el nombre del mes correspondiente a la fecha actual
        // RETORNA: 
        // CREO: Hugo Enrique Ramírez Aguilera
        // FECHA_CREO: 17/Noviembre/2011 
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        public String Consultar_Nombre_Mes(String Mes)
        {
            try
            {
                switch (Mes)
                {
                    case "01":
                        Mes = "_ENERO";
                        break;
                    case "02":
                        Mes = "_FEBRERO";
                        break;
                    case "03":
                        Mes = "_MARZO";
                        break;
                    case "04":
                        Mes = "_ABRIL";
                        break;
                    case "05":
                        Mes = "_MAYO";
                        break;
                    case "06":
                        Mes = "_JUNIO";
                        break;
                    case "07":
                        Mes = "_JULIO";
                        break;
                    case "08":
                        Mes = "_AGOSTO";
                        break;
                    case "09":
                        Mes = "_SEPTIEMBRE";
                        break;
                    case "10":
                        Mes = "_OCTUBRE";
                        break;
                    case "11":
                        Mes = "_NOVIEMBRE";
                        break;
                    default:
                        Mes = "_DICIEMBRE";
                        break;
                }
            }
            catch (Exception Ex)
            {
                Mostrar_Informacion(Ex.ToString(), true);
            }
            return Mes;
        }
        //*******************************************************************************
        // NOMBRE DE LA FUNCIÓN: Llenar_Combos_Generales()
        // DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
        // RETORNA: 
        // CREO: Sergio Manuel Gallardo Andrade
        // FECHA_CREO: 17/Noviembre/2011 
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        public void Llenar_Combos_Generales()
        {
            //Cmb_Unidad_Responsable.SelectedValue =Cmb_Unidad_Responsable_Busqueda.SelectedValue;//Cls_Sessiones.Dependencia_ID_Empleado.ToString();
            //Cmb_Programa.Items.Clear();
            //Cmb_Fuente_Financiamiento.Items.Clear();
            if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex > 0)
            {
                //Cls_Ope_Con_Reservas_Negocio Reserva_Negocio = new Cls_Ope_Con_Reservas_Negocio();
                //Reserva_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Busqueda.SelectedValue;//Cls_Sessiones.Dependencia_ID_Empleado.ToString();
                //DataTable Dt_Fte_Financiamiento = Reserva_Negocio.Consultar_Fuentes_Financiamiento();
                //Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Fuente_Financiamiento, Dt_Fte_Financiamiento, 1, 0);

                //DataTable Data_Table_Proyectos = Reserva_Negocio.Consultar_Proyectos_Programas();
                //Session[P_Dt_Programas] = Data_Table_Proyectos;
                //Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Programa, Data_Table_Proyectos, 1, 0);
            }
        }
        private void Mostrar_Informacion(String txt, Boolean mostrar)
        {
            Lbl_Mensaje_Error.Style.Add("color", "#990000");
            Lbl_Mensaje_Error.Visible = mostrar;
            Img_Error.Visible = mostrar;
            Lbl_Mensaje_Error.Text = txt;
        }
        //*******************************************************************************
        // NOMBRE DE LA FUNCIÓN: Validaciones
        // DESCRIPCIÓN: Genera el String con la informacion que falta y ejecuta la 
        // operacion solicitada si las validaciones son positivas
        // RETORNA: 
        // CREO: Gustavo Angeles Cruz
        // FECHA_CREO: 24/Agosto/2010 
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private Boolean Validaciones(bool Validar_Completo)
        {
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
            String Tipo_Comprobacion = "";
            Boolean Bln_Bandera;
            Bln_Bandera = true;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "Es necesario: <br />";
            if (Grid_Partida_Asignada.Rows.Count<=0)
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Debes agregar una partida minimo para realizar una reserva<br />";
                Bln_Bandera = false;
            }
            if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex == 0)
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Seleccionar La Unidad Responsable <br />";
                Bln_Bandera = false;
            }
            if (String.IsNullOrEmpty(Txt_Conceptos.Text))
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Introducir un Concepto. <br />";
                Bln_Bandera = false;
            }
            else
            {
                if (Txt_Conceptos.Text.Length >= 250)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  El concepto solo permite un maximo de 250 caracteres. <br />";
                    Bln_Bandera = false;
                }
            }
            if (Cmb_Tipo_Solicitud_Pago.SelectedIndex == 0)
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Seleccionar el Tipo de Solicitud <br />";
                Bln_Bandera = false;
            }
            else
            {
                Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Cmb_Tipo_Solicitud_Pago.SelectedValue;
                Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                if (Tipo_Comprobacion == "SI")
                {
                    if (Cmb_Deudores .SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Seleccionar el Deudor <br />";
                        Bln_Bandera = false;
                    }
                    if (Cmb_anticipos.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Seleccionar el anticipo <br />";
                        Bln_Bandera = false;
                    }

                    //Verificar si se tiene la fecha y la factura
                    DataTable  Dt_Detalles = new DataTable();
                    Dt_Detalles = (DataTable)Session["Dt_Documentos"];
                    if (Dt_Detalles.Rows.Count==0)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Debes Agregar Minimo una factura <br />";
                        Bln_Bandera = false;
                    }
                }
                else 
                {
                    //Verificar si fue la opcion de los empleados
                    if (Cmb_Tipo_Solicitud_Pago.SelectedItem.Value == "00001")
                    {
                        //Verificar si se ha seleccionado un empleado
                        if (Cmb_Nombre_Empleado.SelectedIndex <= 0)
                        {
                            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Seleccionar un Empleado <br />";
                            Bln_Bandera = false;
                        }
                    }
                    else
                    {
                        if (Cmb_Proveedor_Solicitud_Pago.SelectedIndex <= 0)
                        {
                            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Seleccionar el Proveedor <br />";
                            Bln_Bandera = false;
                        }
                    }
                }
            }
            if (!Bln_Bandera)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
            return Bln_Bandera;
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Tipos_Solicitud
        /// DESCRIPCION : Llena el Cmb_Tipo_Solicitud_Pago con los tipos de solicitud de pago.
        /// PARAMETROS  : 
        /// CREO        : Yazmin Abigail Delgado Gómez
        /// FECHA_CREO  : 18-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Consultar_Tipos_Solicitud()
        {
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Cat_Con_Tipo_Solicitud_Pagos = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
            DataTable Dt_Tipo_Solicitud; //Variable a obtener los datos de la consulta
            try
            {

                Dt_Tipo_Solicitud = Rs_Cat_Con_Tipo_Solicitud_Pagos.Consulta_Tipo_Solicitud_Pagos_Combo(); //Consulta los tipos de solicitud que fueron dados de alta en la base de datos
                Cmb_Tipo_Solicitud_Pago.Items.Clear();
                Cmb_Tipo_Solicitud_Pago.DataSource = Dt_Tipo_Solicitud;
                Cmb_Tipo_Solicitud_Pago.DataTextField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion;
                Cmb_Tipo_Solicitud_Pago.DataValueField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
                Cmb_Tipo_Solicitud_Pago.DataBind();
                Cmb_Tipo_Solicitud_Pago.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                Cmb_Tipo_Solicitud_Pago.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                throw new Exception("Consultar_Tipos_Solicitud " + ex.Message.ToString(), ex);
            }
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Datos
        ///DESCRIPCIÓN          : Metodo para validar los datos de las partidas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 20/Enero/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private Boolean Validar_Datos()
        {
            Boolean Datos_Validos = true;
            Lbl_Mensaje_Error.Text = String.Empty;
            DataTable Dt_Partidas_Asignadas = new DataTable();
            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
            
            try
            { 
                if (Cmb_Tipo_Solicitud_Pago.SelectedIndex == 0)
                {
                    Lbl_Mensaje_Error.Text += " Seleccionar el Tipo de Solicitud <br />";
                    Datos_Validos = false;
                }
                else
                { if (Cmb_Tipo_Solicitud_Pago.SelectedItem.ToString().Trim().Equals("Gastos por Pagar") || Cmb_Tipo_Solicitud_Pago.SelectedItem.ToString().Trim().Equals("Gastos por Comprobar Proveedores"))
                    {
                        if (Cmb_Proveedor_Solicitud_Pago.SelectedIndex <= 0)
                        {
                            Lbl_Mensaje_Error.Text += " Seleccionar el Proveedor <br />";
                            Datos_Validos = false;
                        }
                    }
                }
                return Datos_Validos;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
            }
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Partida_Asignada
        ///DESCRIPCIÓN          : Metodo para crear el datatable de las partidas asignadas del grid anidado
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 20/Enero/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Crear_Dt_Partida_Asignada()
        {
            DataTable Dt_Partida_Asignada = new DataTable();
            DataTable Dt_Session = new DataTable();
            Dt_Partida_Asignada = (DataTable)Session["Dt_Partidas_Asignadas"];
            DataRow Fila;
            Double Total = 0.00;
            try
            {

                if (Dt_Partida_Asignada != null)
                {
                }
                else
                {
                    //creamos las columnas del datatable donde se guardaran los datos
                    Dt_Session.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("DEPENDENCIA", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("FUENTE_FINANCIAMIENTO", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("PROGRAMA", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("NO_RESERVA", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("BENEFICIARIO", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("EMPLEADO_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("PROVEEDOR_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("CONCEPTO", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("TOTAL", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("PARTIDA", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("SALDO", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("ANIO", System.Type.GetType("System.String"));
                    Dt_Session.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada = Dt_Session;
                    //Total = Convert.ToDouble(String.IsNullOrEmpty(Txt_Importe.Text.Trim()) ? "0" : Txt_Importe.Text.Trim());
                }
                
                Grid_Partida_Saldo.Columns[0].Visible = true;
                Grid_Partida_Saldo.Columns[1].Visible = true;
                Grid_Partida_Saldo.Columns[3].Visible = true;
                Grid_Partida_Saldo.Columns[4].Visible = true;
                Grid_Partida_Saldo.Columns[5].Visible = true;
                Grid_Partida_Saldo.Columns[15].Visible = true;

                for (int Contador_For = 0; Contador_For < Grid_Partida_Saldo.Rows.Count; Contador_For++)
                {
                    System.Web.UI.WebControls.TextBox Txt_Importe = (System.Web.UI.WebControls.TextBox)Grid_Partida_Saldo.Rows[Contador_For].Cells[11].FindControl("Txt_Importe_Partida");
                    String Importe = Txt_Importe.Text;

                    if (Importe != "" && Convert.ToDouble(Importe) > 0)
                    {
                        Total += Convert.ToDouble(Importe);

                        Fila = Dt_Partida_Asignada.NewRow();
                        Fila["DEPENDENCIA_ID"] = Cmb_Unidad_Responsable_Busqueda.SelectedItem.Value.Trim();
                        Fila["FUENTE_FINANCIAMIENTO_ID"] = Convert.ToString(Grid_Partida_Saldo.Rows[Contador_For].Cells[1].Text.ToString());//1
                        Fila["PROGRAMA_ID"] = Grid_Partida_Saldo.Rows[Contador_For].Cells[3].Text.ToString();
                        Fila["DEPENDENCIA"] = HttpUtility.HtmlDecode(Cmb_Unidad_Responsable_Busqueda.SelectedItem.Text.Trim());
                        Fila["FUENTE_FINANCIAMIENTO"] = Grid_Partida_Saldo.Rows[Contador_For].Cells[2].Text.ToString();
                        Fila["PROGRAMA"] = HttpUtility.HtmlDecode(Convert.ToString(Grid_Partida_Saldo.Rows[Contador_For].Cells[4].Text.ToString()));

                        Fila["NO_RESERVA"] = "";
                        if (Cmb_Tipo_Solicitud_Pago.SelectedItem.ToString().Trim().Equals("Gastos por Comprobar Empleados"))
                        {
                            //Fila["BENEFICIARIO"] = Cmb_Nombre_Empleado.SelectedItem.Text.Trim();
                            //Fila["EMPLEADO_ID"] = Cmb_Nombre_Empleado.SelectedItem.Value.Trim();
                            Fila["PROVEEDOR_ID"] = "";
                        }
                        else if (Cmb_Tipo_Solicitud_Pago.SelectedItem.ToString().Trim().Equals("Gastos por Pagar") || Cmb_Tipo_Solicitud_Pago.SelectedItem.ToString().Trim().Equals("Gastos por Comprobar Proveedores"))
                        {
                            Fila["BENEFICIARIO"] = Cmb_Proveedor_Solicitud_Pago.SelectedItem.Text.Trim();
                            Fila["EMPLEADO_ID"] = "";
                            Fila["PROVEEDOR_ID"] = Cmb_Proveedor_Solicitud_Pago.SelectedItem.Value.Trim();
                        }
                        Fila["CONCEPTO"] = HttpUtility.HtmlDecode(Txt_Conceptos.Text.Trim());
                        Fila["TOTAL"] = String.Format("{0:##,###,##0.00}", Total);
                        Fila["PARTIDA_ID"] = Grid_Partida_Saldo.Rows[Contador_For].Cells[5].Text.ToString();
                        Fila["PARTIDA"] = HttpUtility.HtmlDecode(Grid_Partida_Saldo.Rows[Contador_For].Cells[7].Text.ToString());
                        Fila["IMPORTE"] = String.Format("{0:##,###,##0.00}", Importe);
                        Fila["SALDO"] = String.Format("{0:##,###,##0.00}", Importe);
                        Fila["ANIO"] = DateTime.Now.ToString("yyyy");
                        Fila["CAPITULO_ID"] = Convert.ToString(Grid_Partida_Saldo.Rows[Contador_For].Cells[15].Text.ToString());//1
                        Dt_Partida_Asignada.Rows.Add(Fila);
                    }

                }// fin del for
                Grid_Partida_Saldo.Columns[0].Visible = false;
                Grid_Partida_Saldo.Columns[1].Visible = false;
                Grid_Partida_Saldo.Columns[3].Visible = false;
                Grid_Partida_Saldo.Columns[4].Visible = false;
                Grid_Partida_Saldo.Columns[5].Visible = false;
                Grid_Partida_Saldo.Columns[15].Visible = false;
                 Session["Dt_Partidas_Asignadas"] = Dt_Partida_Asignada;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de crear la tabla de partidas asignadas Error[" + ex.Message + "]");
            }
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Partida_Asignada
        ///DESCRIPCIÓN          : Metodo para llenar el grid anidado de las partidas asignadas
        ///PROPIEDADES          1 Dt_Partidas: Datos de las partidas 
        ///                     2 Estado:-para daber cuando limpiar el grid
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 20/Enero/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Grid_Partida_Asignada(Boolean Estado)
        {
            DataTable Dt_Partidas_Asignadas = new DataTable();
            DataTable Dt_Datos_Generales = new DataTable();
            DataRow Fila;

            try
            {
                if (Estado == true)
                {
                    Session["Dt_Partidas_Asignadas"] = null;
                    Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                }
                else
                {
                    Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];

                }

                Grid_Partida_Asignada.DataSource = new DataTable();
                Grid_Partidas.DataBind();

                if (Dt_Partidas_Asignadas != null)
                {
                    if (Dt_Partidas_Asignadas.Rows.Count > 0)
                    {
                        Dt_Datos_Generales.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("DEPENDENCIA", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("FUENTE_FINANCIAMIENTO", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("PROGRAMA", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("NO_RESERVA", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("BENEFICIARIO", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("EMPLEADO_ID", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("PROVEEDOR_ID", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("CONCEPTO", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("TOTAL", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("SALDO", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("ANIO", System.Type.GetType("System.String"));
                        Dt_Datos_Generales.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                        Fila = Dt_Datos_Generales.NewRow();
                        Fila["DEPENDENCIA_ID"] = Dt_Partidas_Asignadas.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                        Fila["FUENTE_FINANCIAMIENTO_ID"] = Dt_Partidas_Asignadas.Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                        Fila["PROGRAMA_ID"] = Dt_Partidas_Asignadas.Rows[0]["PROGRAMA_ID"].ToString().Trim();
                        Fila["DEPENDENCIA"] = Dt_Partidas_Asignadas.Rows[0]["DEPENDENCIA"].ToString().Trim();
                        Fila["FUENTE_FINANCIAMIENTO"] = Dt_Partidas_Asignadas.Rows[0]["FUENTE_FINANCIAMIENTO"].ToString().Trim();
                        Fila["PROGRAMA"] = Dt_Partidas_Asignadas.Rows[0]["PROGRAMA"].ToString().Trim();
                        Fila["NO_RESERVA"] = Dt_Partidas_Asignadas.Rows[0]["NO_RESERVA"].ToString().Trim(); ;
                        Fila["BENEFICIARIO"] = Dt_Partidas_Asignadas.Rows[0]["BENEFICIARIO"].ToString().Trim();
                        Fila["EMPLEADO_ID"] = Dt_Partidas_Asignadas.Rows[0]["EMPLEADO_ID"].ToString().Trim();
                        Fila["PROVEEDOR_ID"] = Dt_Partidas_Asignadas.Rows[0]["PROVEEDOR_ID"].ToString().Trim();
                        Fila["CONCEPTO"] = Dt_Partidas_Asignadas.Rows[0]["CONCEPTO"].ToString().Trim();
                        Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(Dt_Partidas_Asignadas.Rows[Dt_Partidas_Asignadas.Rows.Count - 1]["TOTAL"].ToString().Trim()));
                        Fila["SALDO"] = Dt_Partidas_Asignadas.Rows[0]["SALDO"].ToString().Trim();
                        Fila["ANIO"] = Dt_Partidas_Asignadas.Rows[0]["ANIO"].ToString().Trim();
                        Fila["CAPITULO_ID"] = Dt_Partidas_Asignadas.Rows[0]["CAPITULO_ID"].ToString().Trim();
                        Dt_Datos_Generales.Rows.Add(Fila);
                        if (Btn_Nuevo.ToolTip.Equals("Nuevo"))
                        {
                            Grid_Partida_Asignada.Columns[1].Visible = true;
                            Grid_Partida_Asignada.Columns[2].Visible = true;
                            Grid_Partida_Asignada.Columns[3].Visible = true;
                            Grid_Partida_Asignada.Columns[4].Visible = true;
                            Grid_Partida_Asignada.Columns[5].Visible = true;
                            Grid_Partida_Asignada.Columns[6].Visible = true;
                            Grid_Partida_Asignada.Columns[7].Visible = true;
                            Grid_Partida_Asignada.Columns[9].Visible = true;
                            Grid_Partida_Asignada.Columns[15].Visible = true;
                            Grid_Partida_Asignada.DataSource = Dt_Datos_Generales;
                            Grid_Partida_Asignada.DataBind();
                            Grid_Partida_Asignada.Columns[1].Visible = false;
                            Grid_Partida_Asignada.Columns[2].Visible = false;
                            Grid_Partida_Asignada.Columns[3].Visible = false;
                            Grid_Partida_Asignada.Columns[4].Visible = false;
                            Grid_Partida_Asignada.Columns[5].Visible = false;
                            Grid_Partida_Asignada.Columns[6].Visible = false;
                            Grid_Partida_Asignada.Columns[9].Visible = false;
                            Grid_Partida_Asignada.Columns[15].Visible = false;
                        }
                        else
                        {
                            Grid_Partida_Asignada.Columns[1].Visible = true;
                            Grid_Partida_Asignada.Columns[2].Visible = true;
                            Grid_Partida_Asignada.Columns[3].Visible = true;
                            Grid_Partida_Asignada.Columns[4].Visible = true;
                            Grid_Partida_Asignada.Columns[5].Visible = true;
                            Grid_Partida_Asignada.Columns[6].Visible = true;
                            Grid_Partida_Asignada.Columns[7].Visible = true;
                            Grid_Partida_Asignada.Columns[9].Visible = true;
                            Grid_Partida_Asignada.DataSource = Dt_Datos_Generales;
                            Grid_Partida_Asignada.DataBind();
                            Grid_Partida_Asignada.Columns[1].Visible = false;
                            Grid_Partida_Asignada.Columns[2].Visible = false;
                            Grid_Partida_Asignada.Columns[3].Visible = false;
                            Grid_Partida_Asignada.Columns[4].Visible = false;
                            Grid_Partida_Asignada.Columns[5].Visible = false;
                            Grid_Partida_Asignada.Columns[6].Visible = false;
                            Grid_Partida_Asignada.Columns[7].Visible = false;
                            Grid_Partida_Asignada.Columns[9].Visible = false;
                        }
                    }
                    else
                    {
                        Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                        Grid_Partida_Asignada.DataBind();
                    }
                }
                else
                {
                    Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de llenar la tabla de las partidas asignadas Error[" + ex.Message + "]");
            }
        }
    #endregion

    #region (Metodos de Operacion [Alta - Modificar - Eliminar])
   ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Reserva
    /// DESCRIPCION : Modifica la reserva con los datos proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 22/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
   private void Modificar_Reserva()
        {
            Cls_Ope_Con_Reservas_Negocio Rs_Reserva = new Cls_Ope_Con_Reservas_Negocio(); //Variable de conexion hacia la capa de negocios
            Cls_Ope_Con_Reservas_Negocio Rs_Reserva_Saldo_Anio = new Cls_Ope_Con_Reservas_Negocio(); //Variable de conexion hacia la capa de negocios
            Cls_Ope_Con_Deudores_Negocio Rs_Deudores_Modificacion = new Cls_Ope_Con_Deudores_Negocio();
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Partidas_Reserva = new DataTable();
            DataTable Dt_Datos_Saldo = new DataTable();
            DataTable Dt_Datos_Reserva = new DataTable();
            DataRow Fila;
            DataTable Dt_Anio = new DataTable();
            Double Saldo_Reserva;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;

            // crear transaccion para crear el convenio 
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Cn;
            Cmmd.Transaction = Trans;
            try
            {
                int Registro_Presupuestal;
                Rs_Reserva.P_No_Reserva = Txt_No_Folio.Text;
                Rs_Reserva.P_Estatus = "CANCELADA";
                Rs_Reserva.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                Rs_Reserva.P_Cmmd = Cmmd;
                Rs_Reserva.Modificar_Reserva(); //Modifica el registro en base a los datos proporcionado
                Dt_Partidas = (DataTable)Session["Dt_Partidas_Asignadas"];
                Saldo_Reserva = 0;
                if (Dt_Partidas_Reserva != null)
                {
                    Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                    Rs_Reserva_Saldo_Anio.P_No_Reserva = Txt_No_Folio.Text;
                    Rs_Reserva_Saldo_Anio.P_Cmmd=Cmmd;
                    Dt_Anio=Rs_Reserva_Saldo_Anio.Consultar_Reservas();
                    foreach (DataRow Registro in Dt_Partidas.Rows)
                    {
                        Fila = Dt_Partidas_Reserva.NewRow();
                        Fila["DEPENDENCIA_ID"] = Registro["DEPENDENCIA_ID"].ToString();
                        Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                        Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                        Fila["PARTIDA_ID"] = Registro["PARTIDA_ID"].ToString();
                        Rs_Reserva_Saldo_Anio.P_No_Reserva = Txt_No_Folio.Text;
                        Rs_Reserva_Saldo_Anio.P_Cmmd = Cmmd;
                        Dt_Datos_Saldo = Rs_Reserva_Saldo_Anio.Consultar_Reservas_Detalladas();
                        Dt_Datos_Saldo.DefaultView.RowFilter = "DEPENDENCIA_ID='" + Registro["DEPENDENCIA_ID"].ToString() + "' AND FUENTE_FINANCIAMIENTO_ID='" + Registro["FTE_FINANCIAMIENTO_ID"].ToString() + "' AND PROGRAMA_ID='" + Registro["PROYECTO_PROGRAMA_ID"].ToString() + "' AND PARTIDA_ID='" + Registro["PARTIDA_ID"].ToString() + "'";
                        Fila["IMPORTE"] = Dt_Datos_Saldo.DefaultView.ToTable().Rows[0]["SALDO"].ToString();
                        Fila["ANIO"] = Dt_Anio.Rows[0][Ope_Psp_Reservas.Campo_Anio].ToString();
                        Saldo_Reserva = Saldo_Reserva + Convert.ToDouble(Dt_Datos_Saldo.DefaultView.ToTable().Rows[0]["SALDO"].ToString());
                        Dt_Partidas_Reserva.Rows.Add(Fila);
                        Dt_Partidas_Reserva.AcceptChanges();
                    }
                }
                Registro_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DISPONIBLE", "PRE_COMPROMETIDO", Dt_Partidas_Reserva, Cmmd);
                if (Registro_Presupuestal > 0)
                {
                    //Registro_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Txt_No_Folio.Text.Trim(), "DISPONIBLE", "PRE_COMPROMETIDO", Dt_Partidas_Reserva,Cmmd);
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Txt_No_Folio.Text.Trim(), "DISPONIBLE", "PRE_COMPROMETIDO", Saldo_Reserva, "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                    //para actualizar el tipo de reserva
                    //Actualizar el registro del deudor
                    //Rs_Reserva.P_No_Reserva = Txt_No_Folio.Text.Trim();
                    //Rs_Reserva.P_Cmmd = Cmmd;
                    //Dt_Datos_Reserva = Rs_Reserva.Consultar_Reservas();
                    //if (Dt_Datos_Reserva.Rows.Count > 0)
                    //{
                    //    ////if (Dt_Datos_Reserva.Rows[0]["TIPO_SOLICITUD_PAGO_ID"].ToString() == "00001")
                    //    ////{
                    //    ////    Saldo_Reserva = Saldo_Reserva * -1;
                    //    ////    Rs_Deudores_Modificacion.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                    //    ////    Rs_Deudores_Modificacion.P_No_Deuda = Dt_Datos_Reserva.Rows[0]["NO_DEUDA"].ToString();
                    //    ////    Rs_Deudores_Modificacion.P_Importe = Convert.ToString(Saldo_Reserva);
                    //    ////    Rs_Deudores_Modificacion.P_Cmmd = Cmmd;
                    //    ////    Rs_Deudores_Modificacion.Modificar_Saldo_Deudor();
                    //    ////}
                    //}
                    Trans.Commit();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La Cancelacion de la reserva fue exitosa');", true);
                    Limpia_Controles(); //Limpia los controles del modulo
                    Inicializa_Controles();
                    Session["Dt_Partidas_Asignadas"] = new DataTable();
                    Llenar_Grid_Partida_Asignada(false);
                }
                else
                {
                    Trans.Rollback();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La Cancelacion de la reserva no se pudo efectuar debido a  que no hay suficiencia presupuestal');", true);
                }
                
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                Img_Error.Visible = true;
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                Img_Error.Visible = true;
            }
            finally
            {
                Cn.Close();
            }
        }
   ///*******************************************************************************
   /// NOMBRE DE LA FUNCION: Modificar_Reserva
   /// DESCRIPCION : Modifica la reserva con los datos proporcionados por el usuario
   /// PARAMETROS  : 
   /// CREO        : Sergio Manuel Gallardo Andrade
   /// FECHA_CREO  : 22/Noviembre/2011
   /// MODIFICO          :
   /// FECHA_MODIFICO    :
   /// CAUSA_MODIFICACION:
   ///*******************************************************************************
   private void Modificar_Reserva(DataTable Dt_Partidas_Reserva, String Tipo_Modificacion, Double Importe_Total_Modificado, SqlCommand Command)
   {
       Cls_Ope_Con_Reservas_Negocio Rs_Reserva = new Cls_Ope_Con_Reservas_Negocio(); //Variable de conexion hacia la capa de negocios
       int Registro_Presupuestal;
       SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
       SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
       SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
       if (Command == null)
       {
           if (Conexion_Base.State != ConnectionState.Open)
           {
               Conexion_Base.Open(); //Abre la conexión a la base de datos            
           }
           Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
           Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
           Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
       }
       else
       {
           Comando_SQL = Command;
       }
       try
       {
           if (Tipo_Modificacion == "AMPLIACION")
           {
               Registro_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PRE_COMPROMETIDO", "DISPONIBLE", Dt_Partidas_Reserva, Comando_SQL);
           }
           else
           {
               Registro_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DISPONIBLE", "PRE_COMPROMETIDO", Dt_Partidas_Reserva, Comando_SQL);
           }
           if (Registro_Presupuestal > 0)
           {
               if (Tipo_Modificacion == "AMPLIACION")
               {
                   Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Dt_Partidas_Reserva.Rows[0]["NO_RESERVA"].ToString(), "PRE_COMPROMETIDO", "DISPONIBLE", Importe_Total_Modificado, "", "", "", "", Comando_SQL); //Agrega el historial del movimiento de la partida presupuestal
               }
               else
               {
                   Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Dt_Partidas_Reserva.Rows[0]["NO_RESERVA"].ToString(), "DISPONIBLE", "PRE_COMPROMETIDO", Importe_Total_Modificado, "", "", "", "", Comando_SQL); //Agrega el historial del movimiento de la partida presupuestal
               }
               //Afectar el saldo de la reserva
               Rs_Reserva.P_No_Reserva = Dt_Partidas_Reserva.Rows[0]["NO_RESERVA"].ToString();
               Rs_Reserva.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
               Rs_Reserva.P_Dt_Detalles_Reserva = Dt_Partidas_Reserva;
               Rs_Reserva.P_Tipo_Modificacion = Tipo_Modificacion;
               Rs_Reserva.P_Importe = Convert.ToString(Importe_Total_Modificado);
               Rs_Reserva.P_Cmmd = Comando_SQL;
               Rs_Reserva.Modificar_Saldos_Reserva(); //Modifica el registro en base a los datos proporcionado
               if (Command == null)
               {
                   Transaccion_SQL.Commit();
               }
           }
           else
           {
               if (Command == null)
               {
                   Transaccion_SQL.Rollback();
               }
               ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La Modificación de la reserva no se pudo efectuar debido a  que no hay suficiencia presupuestal');", true);
           }

       }
       catch (SqlException Ex)
       {
           if (Command == null)
           {
               Transaccion_SQL.Rollback();
           }
           Lbl_Mensaje_Error.Text = "Error:";
           Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
           Img_Error.Visible = true;
       }
       catch (Exception Ex)
       {
           Lbl_Mensaje_Error.Text = "Error:";
           Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
           Img_Error.Visible = true;
       }
       finally
       {
           if (Command == null)
           {
               Conexion_Base.Close();
           }
       }
   }
   ///*******************************************************************************
   /// NOMBRE DE LA FUNCION: Modificar_Saldo_Reserva
   /// DESCRIPCION : Modifica la reserva con los datos proporcionados por el usuario
   /// PARAMETROS  : 
   /// CREO        : Sergio Manuel Gallardo Andrade
   /// FECHA_CREO  : 29/Abril/2013
   /// MODIFICO          :
   /// FECHA_MODIFICO    :
   /// CAUSA_MODIFICACION:
   ///*******************************************************************************
   private void Modificar_Saldo_Reserva()
   {
       Cls_Ope_Con_Reservas_Negocio Rs_Reserva = new Cls_Ope_Con_Reservas_Negocio(); //Variable de conexion hacia la capa de negocios
       Cls_Ope_Con_Deudores_Negocio Rs_Deudores_Modificacion = new Cls_Ope_Con_Deudores_Negocio();
       DataTable Dt_Partidas = new DataTable();
       DataTable Dt_Partidas_Reserva = new DataTable();
       DataTable Dt_Datos_Reserva = new DataTable();
       Double Monto = 0;
       Double Total_Modificado = 0;
       DataRow Fila_Reserva;
       Int32 Indice_Grid_Saldos = 0;
       try
       {
           if (Dt_Partidas_Reserva.Rows.Count <= 0)
           {
               Dt_Partidas_Reserva.Columns.Add("NO_RESERVA", System.Type.GetType("System.String"));
               Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
               Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
               Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
               Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
               Dt_Partidas_Reserva.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
               Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.Double"));
               Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
           }
           //Se recorre el grid con los saldos modificados
           foreach (GridViewRow Renglon_Grid_Saldos in Grid_Partidas_Reserva.Rows)
           {
               Indice_Grid_Saldos++;
               Grid_Partidas_Reserva.SelectedIndex = Indice_Grid_Saldos;
               if (!String.IsNullOrEmpty(((System.Web.UI.WebControls.TextBox)Renglon_Grid_Saldos.FindControl("Txt_Importe_Comprometido")).Text.ToString()))
               {
                   Monto = Convert.ToDouble(((System.Web.UI.WebControls.TextBox)Renglon_Grid_Saldos.FindControl("Txt_Importe_Comprometido")).Text.Replace(",", ""));
                   if (Monto > 0)
                   {
                       Fila_Reserva = Dt_Partidas_Reserva.NewRow();
                       Fila_Reserva["NO_RESERVA"] = Renglon_Grid_Saldos.Cells[12].Text.ToString();
                       Fila_Reserva["DEPENDENCIA_ID"] = Renglon_Grid_Saldos.Cells[0].Text.ToString().Trim();
                       Fila_Reserva["FUENTE_FINANCIAMIENTO_ID"] = Renglon_Grid_Saldos.Cells[1].Text.ToString().Trim();
                       Fila_Reserva["PROGRAMA_ID"] = Renglon_Grid_Saldos.Cells[3].Text.ToString().Trim();
                       Fila_Reserva["PARTIDA_ID"] = Renglon_Grid_Saldos.Cells[5].Text.ToString().Trim();
                       Fila_Reserva["CAPITULO_ID"] = Renglon_Grid_Saldos.Cells[11].Text.ToString().Trim();
                       Fila_Reserva["IMPORTE"] = Monto;
                       Total_Modificado = Total_Modificado + Monto;
                       Fila_Reserva["ANIO"] = DateTime.Now.Year;
                       Dt_Partidas_Reserva.Rows.Add(Fila_Reserva);
                       Dt_Partidas_Reserva.AcceptChanges();
                   }
               }
           }
           Modificar_Reserva(Dt_Partidas_Reserva, Cmb_Tipo_Modificacion.SelectedItem.Text,Total_Modificado, null);
       }
       catch (SqlException Ex)
       {
           Lbl_Mensaje_Error.Text = "Error:";
           Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
           Img_Error.Visible = true;
       }
       catch (Exception Ex)
       {
           Lbl_Mensaje_Error.Text = "Error:";
           Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
           Img_Error.Visible = true;
       }
   }

   ///*******************************************************************************
   ///NOMBRE DE LA FUNCIÓN: Imprimir
   ///DESCRIPCIÓN: Imprime la solicitud
   ///PROPIEDADES:     
   ///CREO: Sergio Manuel Gallardo
   ///FECHA_CREO: 06/Enero/2012 
   ///MODIFICO:
   ///FECHA_MODIFICO
   ///CAUSA_MODIFICACIÓN
   ///*******************************************************************************
   protected void Imprimir(String Numero_Solicitud)
   {
       DataSet Ds_Reporte = null;
       DataTable Dt_Pagos = null;
       try
       {
           Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
           Ds_Reporte = new DataSet();
           Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
           Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
           if (Dt_Pagos.Rows.Count > 0)
           {
               Dt_Pagos.TableName = "Dt_Solicitud_Pago";
               Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
               //Se llama al método que ejecuta la operación de generar el reporte.
               Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
           }
       }
       //}
       catch (Exception Ex)
       {
           Lbl_Mensaje_Error.Text = "Error:";
           Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
           Img_Error.Visible = true;
       }

   }

   /// *************************************************************************************
   /// NOMBRE:             Generar_Reporte
   /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
   ///              
   /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
   ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
   ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
   ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
   /// USUARIO CREO:       Juan Alberto Hernández Negrete.
   /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
   /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
   /// FECHA MODIFICO:     16/Mayo/2011
   /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
   ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
   /// *************************************************************************************
   public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
   {
       ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
       String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
       try
       {
           Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
           Reporte.Load(Ruta);

           if (Ds_Reporte_Crystal is DataSet)
           {
               if (Ds_Reporte_Crystal.Tables.Count > 0)
               {
                   Reporte.SetDataSource(Ds_Reporte_Crystal);
                   Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                   //Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                   Mostrar_Archivo(Nombre_Reporte_Generar + Formato);
               }
           }
       }
       catch (Exception Ex)
       {
           throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
       }
   }

   /// *************************************************************************************
   /// NOMBRE:             Exportar_Reporte_PDF
   /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
   ///                     especificada.
   /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
   ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
   /// USUARIO CREO:       Juan Alberto Hernández Negrete.
   /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
   /// USUARIO MODIFICO:
   /// FECHA MODIFICO:
   /// CAUSA MODIFICACIÓN:
   /// *************************************************************************************
   public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
   {
       ExportOptions Opciones_Exportacion = new ExportOptions();
       DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
       PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

       try
       {
           if (Reporte is ReportDocument)
           {
               Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
               Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
               Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
               Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
               Reporte.Export(Opciones_Exportacion);
           }
       }
       catch (Exception Ex)
       {
           throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
       }
   }

   private void Mostrar_Archivo(String URL)
   {
       string Pagina = "../../Reporte/";
       try
       {
           Pagina = Pagina + URL;
           ScriptManager.RegisterStartupScript(this, this.GetType(), "B1",
               "window.open('" + Pagina + "', 'Solicitud Pago','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
           //ScriptManager.RegisterStartupScript(this, this.GetType(), "B", "window.location='Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL + "';", true);
           //Response.Redirect("Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL, false);
       }
       catch (Exception Ex)
       {
           throw new Exception("Error al mostrar el archivo de dispersion generado generado. Error: [" + Ex.Message + "]");
       }
   }

    #endregion

    #region (Eventos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
        ///DESCRIPCIÓN          : Metodo que permite dar de alta una reserva
        ///CREO                 : 
        ///FECHA_CREO           : 17/Noviembre/2011 
        ///MODIFICO             : Leslie Gonzalez Vazquez
        ///FECHA_MODIFICO       : 20/enero/2012
        ///CAUSA_MODIFICACIÓN   : Para agregar los detalles de las reservas
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            int No_reserva;
            Boolean Bandera = true;
            int Registro_Afectado;
            int Registro_Movimiento;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            String Beneficiario = String.Empty;
            String Proveedor_Id = String.Empty;
            String Empleado_Id = String.Empty;
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Detalles_Solicitud = new DataTable();
            Cls_Ope_Con_Deudores_Negocio Rs_Deudores_Modificacion = new Cls_Ope_Con_Deudores_Negocio();
            Cls_Ope_Con_Reservas_Negocio Rs_Actualizar_Tipo_Reserva = new Cls_Ope_Con_Reservas_Negocio();
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
            String Tipo_Comprobacion = "";
            double Total = 0.00;
            string Tipo_Beneficiario = string.Empty; //variable que indica el tipo de beneficiario (EMPLEADO, PROVEEDOR)
            string No_Solicitud_Pago = string.Empty; //variable para el numero de la solicitud de pago

            try
            {
                if (Btn_Nuevo.ToolTip == "Nuevo")
                {
                    Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                    Limpia_Controles();           //Limpia los controles de la forma para poder introducir nuevos datos
                    Cmb_Estatus.SelectedIndex = 0;
                    Session["Dt_Partidas_Asignadas"] = new DataTable();
                    Llenar_Grid_Partida_Asignada(false);
                    Cls_Ope_Con_Reservas_Negocio Rs_Consulta_Dependencias = new Cls_Ope_Con_Reservas_Negocio();
                    DataTable Dt_Consulta = new DataTable();
                    if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex > 0)
                    {
                        Rs_Consulta_Dependencias.P_Dependencia_ID = Cmb_Unidad_Responsable_Busqueda.SelectedValue;
                        Rs_Consulta_Dependencias.P_Ramo_33="NO";
                        Rs_Consulta_Dependencias.P_Filtro_Campo_Mes = Consultar_Nombre_Mes(String.Format("{0:0#}", (DateTime.Now.Month)));
                        Dt_Consulta = Rs_Consulta_Dependencias.Consultar_Reservas_Unidad_Responsable();
                        Grid_Partida_Saldo.Columns[0].Visible = true;
                        Grid_Partida_Saldo.Columns[1].Visible = true;
                        Grid_Partida_Saldo.Columns[3].Visible = true;
                        Grid_Partida_Saldo.Columns[4].Visible = true;
                        Grid_Partida_Saldo.Columns[5].Visible = true;
                        Grid_Partida_Saldo.Columns[15].Visible = true;
                        Grid_Partida_Saldo.DataSource = Dt_Consulta;
                        Grid_Partida_Saldo.DataBind();
                        Grid_Partida_Saldo.Columns[0].Visible = false;
                        Grid_Partida_Saldo.Columns[1].Visible = false;
                        Grid_Partida_Saldo.Columns[3].Visible = false;
                        Grid_Partida_Saldo.Columns[4].Visible = false;
                        Grid_Partida_Saldo.Columns[5].Visible = false;
                        Grid_Partida_Saldo.Columns[15].Visible = false;
                        Session["Dt_Partidas_Asignadas"] = new DataTable();
                        Llenar_Grid_Partida_Asignada(false);
                        Tr_Div_Partidas.Visible = false;
                        //Tr_Div_Partidas.Style.Add("display", "none");
                        Session["Dt_Consulta"] = Dt_Consulta;
                    }
                    Btn_Agregar.Visible = true;
                }
                else
                {
                    //Valida los datos ingresados por el usuario.
                    if (Validaciones(true))
                    {

                        Dt_Partidas = (DataTable)Session["Dt_Partidas_Asignadas"];
                        if (Dt_Partidas != null)
                        {
                            if (Dt_Partidas.Rows.Count > 0)
                            {
                                Total = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas.Rows[Dt_Partidas.Rows.Count - 1]["TOTAL"].ToString().Trim()) ? "0" : Dt_Partidas.Rows[Dt_Partidas.Rows.Count - 1]["TOTAL"].ToString().Trim());
                            }
                        }
                        Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Cmb_Tipo_Solicitud_Pago.SelectedValue;
                        Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();

                        //Verificar si son gastos a comprobar
                        if (Tipo_Comprobacion == "SI")
                        {
                            //verificar si el total no excede el total de los gastos de anticipo
                            if (Total > Convert.ToDouble(Txt_Gastos_Anticipos.Text.ToString().Replace("$", "").Replace(",", "")))
                            {
                                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El total de la reserva no puede ser mayor al del anticipo <br />";
                                Lbl_Mensaje_Error.Visible = true;
                                Img_Error.Visible = true;
                                Bandera = false;
                            }

                            //verificar el tipo de deudor
                            if (Cmb_Deudores.SelectedValue.Substring(0, 1) == "E")
                            {
                                Beneficiario = Cmb_Deudores.SelectedItem.ToString().Trim();
                                Proveedor_Id = "";
                                Empleado_Id = Cmb_Deudores.SelectedValue.Substring(2);
                                Tipo_Beneficiario = "EMPLEADO";
                            }
                            else
                            {
                                if (Cmb_Deudores.SelectedValue.Substring(0, 1) == "P")
                                {
                                    Beneficiario = "D" + Cmb_Deudores.SelectedItem.ToString().Trim();
                                    Proveedor_Id = Cmb_Deudores.SelectedValue.Substring(2);
                                    Empleado_Id = "";
                                    Tipo_Beneficiario = "PROVEEDOR";
                                }
                            }
                        }
                        if (Bandera)
                        {
                            if (Tipo_Comprobacion == "NO")
                            {
                                if (Cmb_Proveedor_Solicitud_Pago.SelectedIndex > 0)
                                {
                                    Beneficiario = Cmb_Proveedor_Solicitud_Pago.SelectedItem.ToString().Trim();
                                    Proveedor_Id = Cmb_Proveedor_Solicitud_Pago.SelectedValue.Trim();
                                    Empleado_Id = "";
                                }
                            }
                            SqlConnection Cn = new SqlConnection();
                            SqlCommand Cmmd = new SqlCommand();
                            SqlTransaction Trans = null;

                            // crear transaccion para crear el convenio 
                            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                            Cn.Open();
                            Trans = Cn.BeginTransaction();
                            Cmmd.Connection = Cn;
                            Cmmd.Transaction = Trans;
                            try
                            {

                                DataTable Dt_Partidas_Saldo = (DataTable)Session["Dt_Partidas_Asignadas"];
                                No_reserva = Cls_Ope_Psp_Manejo_Presupuesto.Crear_Reserva_Directa(Cmb_Unidad_Responsable_Busqueda.SelectedValue.Trim(), "GENERADA", Beneficiario, "", "", Txt_Conceptos.Text.Trim(), Convert.ToString(DateTime.Now.Year), Total, Proveedor_Id, Empleado_Id, Cmb_Tipo_Solicitud_Pago.SelectedValue.Trim(), Dt_Partidas, Txt_Fuente_Ramo.Text,Cmmd,"SI");
                                Registro_Afectado = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PRE_COMPROMETIDO", "DISPONIBLE", Dt_Partidas, Cmmd);
                                
                                if (Registro_Afectado > 0)
                                {
                                    //Registro_Afectado = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(No_reserva), "PRE_COMPROMETIDO", "DISPONIBLE", Dt_Partidas,Cmmd);
                                    Registro_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(No_reserva), "PRE_COMPROMETIDO", "DISPONIBLE", Total, "", "", "", "", Cmmd);

                                    //para actualizar el tipo de reserva
                                    Rs_Actualizar_Tipo_Reserva.P_No_Reserva = "" + No_reserva;
                                    Rs_Actualizar_Tipo_Reserva.P_Tipo_Reserva = Cmb_Tipo_Reserva.SelectedValue;
                                    if (Tipo_Comprobacion == "SI")
                                    {
                                        Rs_Actualizar_Tipo_Reserva.P_No_Deuda = Cmb_anticipos.SelectedValue;
                                    }
                                    Rs_Actualizar_Tipo_Reserva.P_Cmmd = Cmmd;
                                    Rs_Actualizar_Tipo_Reserva.Modificar_Tipo_Reserva();

                                    //Actualizar el registro del deudor
                                    if (Tipo_Comprobacion == "SI")
                                    {
                                        Rs_Deudores_Modificacion.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                                        Rs_Deudores_Modificacion.P_No_Deuda = Cmb_anticipos.SelectedValue;
                                        Rs_Deudores_Modificacion.P_Importe = Convert.ToString(Total);
                                        Rs_Deudores_Modificacion.P_Deudor_ID = Cmb_Deudores.SelectedValue;
                                        Rs_Deudores_Modificacion.P_Cmmd = Cmmd;
                                        Rs_Deudores_Modificacion.P_No_Reserva = No_reserva;
                                        Rs_Deudores_Modificacion.P_Tipo_Deudor = Tipo_Beneficiario;
                                        Rs_Deudores_Modificacion.P_Concepto = Txt_Conceptos.Text.Trim();
                                        Rs_Deudores_Modificacion.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                                        Rs_Deudores_Modificacion.P_No_Factura_Solicitud = Txt_No_Factura_Solicitud.Text.Trim();
                                        Rs_Deudores_Modificacion.P_Fecha_Factura_Solicitud = Convert.ToDateTime(Txt_Fecha_Factura_Solicitud.Text.Trim());
                                        Rs_Deudores_Modificacion.Modificar_Saldo_Deudor();

                                        //Dar de alta la solicitud de pago interna
                                        Dt_Detalles_Solicitud = (DataTable)Session["Dt_Documentos"];
                                        Rs_Deudores_Modificacion.P_Dt_Detalles_Solicitud_Pago = Dt_Detalles_Solicitud;
                                        No_Solicitud_Pago = Rs_Deudores_Modificacion.Crear_Solicitud_Pago_Interna();
                                    }
                                    Trans.Commit();
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Reservas", "alert('El alta de la reserva No." + No_reserva + "  fue exitosa');", true);

                                    //verificar si hay una solicitud de pago
                                    if (string.IsNullOrEmpty(No_Solicitud_Pago) == false)
                                    {
                                        //Imprimir la solcitud de pago
                                        Imprimir(No_Solicitud_Pago);
                                    }

                                    Inicializa_Controles();
                                    Limpia_Controles();
                                    Session["Dt_Partidas_Asignadas"] = new DataTable();
                                    Llenar_Grid_Partida_Asignada(false);
                                }
                                else
                                {
                                    Trans.Rollback();
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Reservas", "alert('El alta de la reserva No se pudo crear debido a que no hay suficiencia presupuestal');", true);
                                }
                            }
                            catch (SqlException Ex)
                            {
                                Trans.Rollback();
                                Lbl_Mensaje_Error.Text = "Error:";
                                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                            }
                            catch (Exception Ex)
                            {

                                Trans.Rollback();
                                Lbl_Mensaje_Error.Text = "Error:";
                                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                            }
                            finally
                            {
                                Cn.Close();
                            }
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Metodo que permite modificar la reserva
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 17/Noviembre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
         {
             DataTable Dt_Partidas = new DataTable();
            Cls_Ope_Con_Reservas_Negocio Reservas_Datos = new Cls_Ope_Con_Reservas_Negocio();
            DataTable Dt_Detalles = new DataTable();
                try
                {
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    Lbl_Mensaje_Error.Text = "";
                    if (Btn_Modificar.ToolTip == "Modificar")
                    {
                            Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                    }
                    else
                    {
                        if (Cmb_Estatus.SelectedItem.Text == "CANCELADA")
                        {
                            //if (Dt_Partidas.Rows.Count > 0)
                            //{
                                Dt_Partidas=(DataTable)Session["Dt_Partidas_Asignadas"];
                                Btn_Modificar.Visible = true;
                                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                                Btn_Modificar.ToolTip = "Cancelar Reserva";
                                Reservas_Datos.P_No_Reserva = Dt_Partidas.Rows[0]["NO_RESERVA"].ToString();
                                Dt_Detalles = Reservas_Datos.Consultar_Reservas();
                                if (Dt_Detalles.Rows.Count > 0)
                                {
                                    if (Dt_Detalles.Rows[0]["ESTATUS"].ToString() == "CANCELADA")
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La reserva ya fue cancelada anteriormente');", true);
                                    }
                                    else
                                    {
                                        Modificar_Reserva(); //Modifica los datos de la Cuenta Contable con los datos proporcionados por el usuario
                                    }
                                }
                            ////}
                        }
                        else
                        {
                            Dt_Partidas = (DataTable)Session["Dt_Partidas_Asignadas"];
                            Modificar_Saldo_Reserva();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La Modificación de la reserva No. " + Dt_Partidas.Rows[0]["NO_RESERVA"].ToString() + " fue exitosa');", true);
                            Limpia_Controles(); //Limpia los controles del modulo
                            Inicializa_Controles();
                            Session["Dt_Partidas_Asignadas"] = new DataTable();
                            Llenar_Grid_Partida_Asignada(false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Reserva_Click
        ///DESCRIPCIÓN          : Metodo que permite buscar la reserva
        ///CREO                 : Gustavo Angeles
        ///FECHA_CREO           : 9/Diciembre/2010 
        ///MODIFICO             : Leslie Gonzalez Vazquez
        ///FECHA_MODIFICO       : 23/Enero/2012
        ///CAUSA_MODIFICACIÓN   : realice la busqueda de los detalles de la reserva
        ///*******************************************************************************
        protected void Btn_Buscar_Reserva_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Ope_Con_Reservas_Negocio Reserva_Negocio = new Cls_Ope_Con_Reservas_Negocio();
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Detalles = new DataTable();
            try
            {
                Lbl_Mensaje_Error.Text = "";
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Visible = false;
                Habilitar_Controles("Modificar");
               // Llenar_Grid_Reservas();
                Llenar_Grid_Partidas_De_Reserva();
                Btn_Agregar.Visible = false;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en la busqueda de reservas. Error[" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_No_Folio_OnTextChanged
        ///DESCRIPCIÓN          : Metodo que permite buscar la reserva
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 28/Mayo/2013
        ///MODIFICO             : 
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        protected void Txt_No_Folio_OnTextChanged(object sender, EventArgs e)
        {
            Cls_Ope_Con_Reservas_Negocio Reserva_Negocio = new Cls_Ope_Con_Reservas_Negocio();
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Detalles = new DataTable();
            try
            {
                Lbl_Mensaje_Error.Text = "";
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Visible = false;
                Habilitar_Controles("Modificar");
               // Llenar_Grid_Reservas();
                Llenar_Grid_Partidas_De_Reserva();
                Btn_Agregar.Visible = false;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en la busqueda de reservas. Error[" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Metodo que permite modificar la reserva
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 17/Noviembre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Btn_Salir.ToolTip == "Salir")
                {
                    Session["Dt_Partidas_Asignadas"] = new DataTable();
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
                else
                {
                    Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
                    Limpia_Controles();//Limpia los controles de la forma
                    Mostrar_Informacion("", false);
                    Session["Dt_Partidas_Asignadas"] = new DataTable();
                    Llenar_Grid_Partida_Asignada(false);
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Requisicion_Click
        ///DESCRIPCIÓN: Metodo para consultar la reserva
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 17/noviembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Seleccionar_Reserva_Click(object sender, ImageClickEventArgs e)
        {
            String No_Reserva = ((ImageButton)sender).CommandArgument;
            //Evento_Grid_Requisiciones_Seleccionar(No_Reserva);
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Tipo_Solicitud_Pago_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de tipo de solicitud
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Cmb_Tipo_Solicitud_Pago_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
            String Tipo_Comprobacion = "";
            try
            {
                if (Cmb_Tipo_Solicitud_Pago.SelectedIndex > 0)
                {
                    //Consultar el tipo de comprobacion de la opcion seleccionada
                    Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Cmb_Tipo_Solicitud_Pago.SelectedValue;
                    Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();

                    //verificar el tipo de comprobacion
                    if (Tipo_Comprobacion == "SI")
                    {
                        Cmb_Proveedor_Solicitud_Pago.Enabled = false;
                        //Cmb_Nombre_Empleado.Enabled = true;
                        //Txt_Nombre_Empleado.Enabled = true;
                        Cmb_Deudores.Enabled = true;
                        Txt_Deudor.Enabled = true;
                        Btn_Buscar_Deudor.Enabled = true;
                        Tr_Deudor.Visible = true;
                        Cmb_Deudores.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                        Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = false;
                        //Btn_Buscar_Empleado.Enabled = true;
                        Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = false;
                        //Tr_Empleado.Style.Add("display", "block");
                        Tr_Proveedor.Visible = false;
                        //Cmb_Nombre_Empleado.Items.Clear();
                        Cmb_Proveedor_Solicitud_Pago.Items.Clear();
                        //Cmb_Nombre_Empleado.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                        Cmb_Tipo_Reserva.SelectedIndex = 0;
                        Cmb_Tipo_Reserva.Enabled = false;

                        //Ejecutar el evento click del boton
                        Btn_Buscar_Deudor_Click(this, new ImageClickEventArgs(0, 0));
                    }
                    else
                    {
                        Cmb_Deudores.Enabled = false;
                        Txt_Deudor.Enabled = false;
                        Btn_Buscar_Deudor.Enabled = false;
                        Tr_Deudor.Visible = false;
                        Cmb_Deudores.Items.Clear();
                        Cmb_Proveedor_Solicitud_Pago.Enabled = true;
                        //Cmb_Nombre_Empleado.Enabled = false;
                        //Txt_Nombre_Empleado.Enabled = false;
                        Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = true;
                        //Btn_Buscar_Empleado.Enabled = false;
                        Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = true;
                        //Tr_Empleado.Style.Add("display", "none");
                        Tr_Proveedor.Visible = true;
                        Tr_Deudor_Anticipos.Visible = false;
                        Tr_Deudor_Anticipos_2.Visible = false;
                        Tr_Deudor_Anticipos_3.Visible = false;
                        Tr_Grid_Facturas.Visible = false;
                        //Cmb_Nombre_Empleado.Items.Clear();
                        Cmb_anticipos.Items.Clear();
                        Txt_Gastos_Anticipos.Text = "";
                        Cmb_Proveedor_Solicitud_Pago.Items.Clear();
                        Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                        Cmb_Tipo_Reserva.Enabled = true;
                    }
                }
                else
                {
                    Tr_Deudor_Anticipos.Visible = false;
                    Tr_Deudor_Anticipos_2.Visible = false;
                    Tr_Deudor_Anticipos_3.Visible = false;
                    Tr_Grid_Facturas.Visible = false;
                    Tr_Deudor.Visible = false;
                    Tr_Empleado.Visible = false;
                    Tr_Proveedor.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al generar el evento del combo de tipos de solicitud Error["+ ex.Message +"]");
            }
        }



        /////////*********************************************************************************************************
        /////////NOMBRE DE LA FUNCIÓN : Cmb_Tipo_Solicitud_Pago_SelectedIndexChanged
        /////////DESCRIPCIÓN          : Evento del combo de tipo de solicitud
        /////////PROPIEDADES          :
        /////////CREO                 : Leslie González Vázquez
        /////////FECHA_CREO           : 21/Diciembre/2011 
        /////////MODIFICO             :
        /////////FECHA_MODIFICO       :
        /////////CAUSA_MODIFICACIÓN...:
        /////////*********************************************************************************************************
        //////protected void Cmb_Tipo_Solicitud_Pago_SelectedIndexChanged(object sender, EventArgs e)
        //////{
        //////    Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
        //////    String Tipo_Comprobacion = "";
        //////    try
        //////    {
        //////        if (Cmb_Tipo_Solicitud_Pago.SelectedIndex > 0)
        //////        {
        //////            Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Cmb_Tipo_Solicitud_Pago.SelectedValue;
        //////            Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();

        //////            //Verificar si es el de los empleados
        //////            if (Cmb_Tipo_Solicitud_Pago.SelectedItem.Value == "00001")
        //////            {
        //////                Cmb_Proveedor_Solicitud_Pago.Enabled = false;
        //////                Cmb_Nombre_Empleado.Enabled = true;
        //////                Txt_Nombre_Empleado.Enabled = true;
        //////                Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = false;
        //////                Btn_Buscar_Empleado.Enabled = true;
        //////                Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = false;
        //////                //Tr_Empleado.Style.Add("display", "table-cell");
        //////                //Tr_Proveedor.Style.Add("display", "none");
        //////                Tr_Empleado.Visible = true;
        //////                Tr_Proveedor.Visible = false;
        //////                Cmb_Nombre_Empleado.Items.Clear();
        //////                Cmb_Proveedor_Solicitud_Pago.Items.Clear();
        //////                Cmb_Nombre_Empleado.Items.Insert(0, new ListItem("<- Seleccione ->", ""));

        //////                Tr_Deudor_Anticipos.Style.Add("display", "none");
        //////                Tr_Deudor.Style.Add("display", "none");
        //////            }
        //////            else
        //////            {
        //////                //Codigo inicial MANUEL
        //////                //if (Tipo_Comprobacion == "SI")
        //////                //{

        //////                //    Cmb_Proveedor_Solicitud_Pago.Enabled = false;
        //////                //    Cmb_Nombre_Empleado.Enabled = true;
        //////                //    Txt_Nombre_Empleado.Enabled = true;
        //////                //    Cmb_Deudores.Enabled = true;
        //////                //    Txt_Deudor.Enabled = true;
        //////                //    Btn_Buscar_Deudor.Enabled = true;
        //////                //    //Tr_Deudor.Style.Add("display", "table-cell");
        //////                //    Tr_Deudor.Visible = true;
        //////                //    Cmb_Deudores.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
        //////                //    Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = false;
        //////                //    Btn_Buscar_Empleado.Enabled = true;
        //////                //    Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = false;
        //////                //    //Tr_Empleado.Style.Add("display", "block");
        //////                //    //Tr_Proveedor.Style.Add("display", "none");
        //////                //    Tr_Empleado.Visible = true;
        //////                //    Tr_Proveedor.Visible = false;
        //////                //    Cmb_Nombre_Empleado.Items.Clear();
        //////                //    Cmb_Proveedor_Solicitud_Pago.Items.Clear();
        //////                //    Cmb_Nombre_Empleado.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
        //////                //    Cmb_Tipo_Reserva.SelectedIndex = 0;
        //////                //    Cmb_Tipo_Reserva.Enabled = false;
        //////                //}
        //////                //else
        //////                //{
        //////                Cmb_Deudores.Enabled = false;
        //////                Txt_Deudor.Enabled = false;
        //////                Btn_Buscar_Deudor.Enabled = false;
        //////                //Tr_Deudor.Style.Add("display", "none");
        //////                Tr_Deudor.Visible = false;
        //////                Cmb_Deudores.Items.Clear();
        //////                Cmb_Proveedor_Solicitud_Pago.Enabled = true;
        //////                Cmb_Nombre_Empleado.Enabled = false;
        //////                Txt_Nombre_Empleado.Enabled = false;
        //////                Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = true;
        //////                Btn_Buscar_Empleado.Enabled = false;
        //////                Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = true;
        //////                //Tr_Empleado.Style.Add("display", "none");
        //////                //Tr_Proveedor.Style.Add("display", "table-cell");
        //////                //Tr_Deudor_Anticipos.Style.Add("display", "none");
        //////                Tr_Empleado.Visible = false;
        //////                Tr_Proveedor.Visible = true;
        //////                Tr_Deudor_Anticipos.Visible = false;
        //////                Cmb_Nombre_Empleado.Items.Clear();
        //////                Cmb_anticipos.Items.Clear();
        //////                Txt_Gastos_Anticipos.Text = "";
        //////                Cmb_Proveedor_Solicitud_Pago.Items.Clear();
        //////                Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
        //////                Cmb_Tipo_Reserva.Enabled = true;
        //////                //}
        //////            }
        //////        }
        //////        else
        //////        {
        //////            //Tr_Deudor_Anticipos.Style.Add("display", "none");
        //////            //Tr_Deudor.Style.Add("display", "none");
        //////            //Tr_Empleado.Style.Add("display", "none");
        //////            //Tr_Proveedor.Style.Add("display", "none");
        //////            Tr_Deudor_Anticipos.Visible = false;
        //////            Tr_Deudor.Visible = false;
        //////            Tr_Empleado.Visible = false;
        //////            Tr_Proveedor.Visible = false;
        //////        }

        //////    }
        //////    catch (Exception ex)
        //////    {
        //////        throw new Exception("Error al generar el evento del combo de tipos de solicitud Error[" + ex.Message + "]");
        //////    }
        //////}





        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Deudor_OnSelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de tipo de solicitud
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 10/Agosto/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Cmb_Deudor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            Cls_Ope_Con_Deudores_Negocio Rs_Consulta_Deudores = new Cls_Ope_Con_Deudores_Negocio();
            DataTable Dt_Anticipos = new DataTable();
            Lbl_Mensaje_Error.Text = String.Empty;
            try
            {
                if (Cmb_Deudores.SelectedIndex > 0)
                {
                    if (Cmb_Deudores.SelectedValue.Substring(0,1) == "P")
                    {
                        Rs_Consulta_Deudores.P_Tipo_Deudor="PROVEEDOR";
                    }
                    else
                    {
                        Rs_Consulta_Deudores.P_Tipo_Deudor ="EMPLEADO";
                    }
                    Rs_Consulta_Deudores.P_Deudor_ID = Cmb_Deudores.SelectedValue.Substring(2);
                    Dt_Anticipos=Rs_Consulta_Deudores.Consulta_Deudores_Activos();
                    if (Dt_Anticipos.Rows.Count > 0)
                    {
                        Cmb_anticipos.DataSource = new DataTable();
                        Cmb_anticipos.DataBind();
                        Cmb_anticipos.DataSource = Dt_Anticipos;
                        Cmb_anticipos.DataTextField = "CONCEPTO";
                        Cmb_anticipos.DataValueField = OPE_CON_DEUDORES.Campo_No_Deuda;
                        Cmb_anticipos.DataBind();
                        Cmb_anticipos.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                        Cmb_anticipos.SelectedIndex = -1;
                        Tr_Deudor_Anticipos.Visible = true;
                        Tr_Deudor_Anticipos_2.Visible = true;
                        Tr_Deudor_Anticipos_3.Visible = true;
                        Tr_Grid_Facturas.Visible = true;
                        //Tr_Deudor_Anticipos.Style.Add("display", "block");
                        Txt_Gastos_Anticipos.Text = "";
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text += " Este Deudor no tiene Anticipos<br />";
                        Mostrar_Informacion(Lbl_Mensaje_Error.Text.Trim(), true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al generar el evento del combo de tipos de solicitud Error["+ ex.Message +"]");
            }
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Agregar_Documento_Click
        ///DESCRIPCIÓN          : Agregar Facturas a la reserva
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 26/Agosto/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Agregar_Documento_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Cat_Empleados_Negocios Consulta_EMP = new Cls_Cat_Empleados_Negocios();
            Cls_Cat_Com_Proveedores_Negocio Consulta_PRO = new Cls_Cat_Com_Proveedores_Negocio();
            DataTable Dt_Consulta_EMP = new DataTable(); //Variable que obtendra los datos de la consulta
            DataTable Dt_Consulta_PRO = new DataTable(); //Variable que obtendra los datos de la consulta
            DataTable Dt_Consulta = new DataTable();
            DataTable Dt_Documento = new DataTable(); //Obtiene los datos de la póliza que fueron proporcionados por el usuario
            //string Codigo_Programatico = "";
            String Espacios = "";
            String extension = String.Empty;
            int error = 0;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            String aux = String.Empty; //variable auxiliar
            if (Validar())
            {
                if (Session["Dt_Documentos"] == null)
                {
                    //Agrega los campos que va a contener el DataTable
                    Dt_Documento.Columns.Add("No_Documento", typeof(System.String));
                    Dt_Documento.Columns.Add("Fecha_Documento", typeof(System.DateTime));
                    Dt_Documento.Columns.Add("Monto", typeof(System.String));
                    Dt_Documento.Columns.Add("Partida", typeof(System.String));
                    }
                else
                {
                    Dt_Documento = (DataTable)Session["Dt_Documentos"];
                    Session.Remove("Dt_Documentos");
                }
                DataRow row = Dt_Documento.NewRow(); //Crea un nuevo registro a la tabla
                //Asigna los valores al nuevo registro creado a la tabla
                row["No_Documento"] = Txt_No_Factura_Solicitud.Text.Trim();
                //row["Fecha_Documento"] = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(Txt_Fecha_Factura_Solicitud_Pago.Text.Trim()));
                row["Fecha_Documento"] = Convierte_Fecha_DT(Txt_Fecha_Factura_Solicitud.Text.Trim());
                row["Monto"] = Txt_Monto_Factura.Text;
                row["Partida"] = Convert.ToString(Convert.ToDecimal(Dt_Documento.Rows.Count) + 1) +Txt_No_Factura_Solicitud.Text.Trim();
                Dt_Documento.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Documento.AcceptChanges();
                Session["Dt_Documentos"] = Dt_Documento;//Agrega los valores del registro a la sesión
                Grid_Documentos.Columns[0].Visible = true;
                Grid_Documentos.DataSource = Dt_Documento; //Agrega los valores de todas las partidas que se tienen al grid
                Grid_Documentos.DataBind();
                Grid_Documentos.Columns[0].Visible = false;
                Txt_No_Factura_Solicitud.Text = "";
                Txt_Fecha_Factura_Solicitud.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);
                Txt_Monto_Factura.Text = "";
            }
        //Indica al usuario que datos son los que falta por proporcionar para poder agregar la partida a la poliza
            else
            {
                //error = 1;
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br />";
                if (String.IsNullOrEmpty(Txt_No_Factura_Solicitud.Text.Trim()))
                {
                    Lbl_Mensaje_Error.Text += Espacios + " + El Número del Documento <br />";
                }
                if (String.IsNullOrEmpty(Txt_Fecha_Factura_Solicitud.Text.Trim()))
                {
                    Lbl_Mensaje_Error.Text += Espacios + " + La Fecha del Documento <br />";
                }
                if (String.IsNullOrEmpty(Txt_Monto_Factura.Text.Trim()))
                {
                    Lbl_Mensaje_Error.Text += Espacios + " + El Monto del Documento <br />";
                }
            }
            if (error == 1)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        } 
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Btn_Eliminar_Partida
        /// DESCRIPCION : Eliminar la partida de un grid 
        /// CREO        : sergio manuel gallardo
        /// FECHA_CREO  : 26- agosto-2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Btn_Eliminar_Partida(object sender, EventArgs e)
        {
            ImageButton Btn_Eliminar_Partida = (ImageButton)sender;
            DataTable Dt_Partidas = (DataTable)Session["Dt_Documentos"];
            DataRow[] Filas = Dt_Partidas.Select("Partida" +
                    "='" + Btn_Eliminar_Partida.CommandArgument + "'");
            if (!(Filas == null))
            {
                if (Filas.Length >= 0)
                {
                    Dt_Partidas.Rows.Remove(Filas[0]);
                    Session["Dt_Documentos"] = Dt_Partidas;
                    Grid_Documentos.Columns[0].Visible = true;
                    Grid_Documentos.DataSource = Session["Dt_Documentos"]; ; //Agrega los valores de todas las partidas que se tienen al grid
                    Grid_Documentos.DataBind();
                    Grid_Documentos.Columns[0].Visible = false;
                
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Grid_Cuentas_RowDataBound
        /// DESCRIPCION : Agrega un identificador al boton de cancelar de la tabla
        ///               para identicar la fila seleccionada de tabla.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 26/Agosto/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                {
                    ((ImageButton)e.Row.Cells[4].FindControl("Btn_Eliminar")).CommandArgument = e.Row.Cells[0].Text.Trim();
                    ((ImageButton)e.Row.Cells[4].FindControl("Btn_Eliminar")).ToolTip = "Quitar el Documento " + e.Row.Cells[1].Text;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
        
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Validar
        /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
        /// CREO        : sergio manuel gallardo
        /// FECHA_CREO  : 26- agosto-2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private Boolean Validar()
        {
            String Espacios_Blanco;
            Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
            try
            {
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br />";
                Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                if (Txt_Monto_Factura.Text == "")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El monto de la factura. <br />";
                    Datos_Validos = false;
                }
                if (Txt_Fecha_Factura_Solicitud.Text == "")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + La fecha de la factura. <br />";
                    Datos_Validos = false;
                }
                if (Txt_No_Factura_Solicitud.Text == "")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El No. del Documento. <br />";
                    Datos_Validos = false;
                }
                return Datos_Validos;
            }
            catch (Exception ex)
            {
                throw new Exception("Validar_Datos_Solicitud_Pagos " + ex.Message.ToString(), ex);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Convierte_Fecha_DT
        ///DESCRIPCION:             Convertir la fecha de una cadena de texto en DateTime
        ///PARAMETROS:              Busqueda: Cadena de texto con el texto a buscar de los proveedores
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              28/Febrero/2013 12:00
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private DateTime Convierte_Fecha_DT(String Fecha)
        {
            //Declaracion de variables
            int Dia = 0; //variable para el dia
            int Mes = 0; //variable para el mes
            int Anio = 0; //Variable para el año
            DateTime Resultado = new DateTime(1900, 1, 1); //Variable para el resultado

            try
            {
                //Obtener el dia
                Dia = Convert.ToInt32(Fecha.Substring(0, 2));

                //Obtener el año
                Anio = Convert.ToInt32(Fecha.Substring(7, 4));

                //Obtener el mes
                switch (Fecha.Substring(3, 3).ToUpper())
                {
                    case "ENE":
                    case "JAN":
                        Mes = 1;
                        break;

                    case "FEB":
                        Mes = 2;
                        break;

                    case "MAR":
                        Mes = 3;
                        break;

                    case "ABR":
                    case "APR":
                        Mes = 4;
                        break;

                    case "MAY":
                        Mes = 5;
                        break;

                    case "JUN":
                        Mes = 6;
                        break;

                    case "JUL":
                        Mes = 7;
                        break;

                    case "AGO":
                    case "AUG":
                        Mes = 8;
                        break;

                    case "SEP":
                        Mes = 9;
                        break;

                    case "OCT":
                        Mes = 10;
                        break;

                    case "NOV":
                        Mes = 11;
                        break;

                    case "DIC":
                    case "DEC":
                        Mes = 12;
                        break;

                    default:
                        Mes = 1;
                        break;
                }

                //Construir el resultado
                Resultado = new DateTime(Anio, Mes, Dia);

                //Entregar resultado
                return Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Anticipos_OnSelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de tipo de solicitud
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 26/Agosto/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Cmb_Anticipos_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            Cls_Ope_Con_Deudores_Negocio Rs_Consulta_Deudores = new Cls_Ope_Con_Deudores_Negocio();
            DataTable Dt_Resultado = new DataTable();
            try
            {
                Txt_Gastos_Anticipos.Text = "";
                Txt_Conceptos.Text = "";
                if (Cmb_anticipos.SelectedIndex > 0)
                {
                    Txt_Gastos_Anticipos.Text = "0";
                    if (Cmb_Deudores.SelectedValue.Substring(0, 1) == "E")
                    {
                        Rs_Consulta_Deudores.P_Tipo_Deudor = "EMPLEADO";
                    }
                    else
                    {
                        if (Cmb_Deudores.SelectedValue.Substring(0, 1) == "P")
                        {
                            Rs_Consulta_Deudores.P_Tipo_Deudor = "PROVEEDOR";
                        }
                    }
                    Rs_Consulta_Deudores.P_Deudor_ID = Cmb_Deudores.SelectedValue.Substring(2);
                    Rs_Consulta_Deudores.P_No_Deuda = Cmb_anticipos.SelectedValue;
                    Dt_Resultado = Rs_Consulta_Deudores.Consulta_Deudores_Activos();
                    if (Dt_Resultado.Rows.Count > 0)
                    {
                        Txt_Gastos_Anticipos.Text = String.Format("{0:c}",Convert.ToDouble(Dt_Resultado.Rows[0]["IMPORTE"].ToString()));
                    }
                    Txt_Conceptos.Text = Cmb_anticipos.SelectedItem.Text.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al generar el evento del combo de tipos de solicitud Error["+ ex.Message +"]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Btn_Buscar_Proveedor_Solicitud_Pagos_Click
        /// DESCRIPCION : Consulta a todos los proveedores que coincidan con el nombre, rfc
        ///               o compañia de acuerdo a lo proporcionado por el usuario
        /// PARAMETROS  : 
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 23-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Btn_Buscar_Proveedor_Solicitud_Pagos_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Cat_Com_Proveedores_Negocio Rs_Consulta_Cat_Com_Proveedores = new Cls_Cat_Com_Proveedores_Negocio(); //Variable de conexión hacia la capa de negocios
            DataTable Dt_Proveedores;
            DataTable Dt_Proveedores_Tipo = new DataTable();
            DataRow Fila;
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (!String.IsNullOrEmpty(Txt_Nombre_Proveedor_Solicitud_Pago.Text))
                {
                    Rs_Consulta_Cat_Com_Proveedores.P_Nombre_Comercial = Txt_Nombre_Proveedor_Solicitud_Pago.Text;
                    Rs_Consulta_Cat_Com_Proveedores.P_Estatus = "ACTIVO";
                    Rs_Consulta_Cat_Com_Proveedores.P_tipo = "CONTABILIDAD";
                    Dt_Proveedores = Rs_Consulta_Cat_Com_Proveedores.Consulta_Avanzada_Proveedor(); //Consulta los proveedores que coincidan con el nombre, compañia, rfc
                    Dt_Proveedores_Tipo.Columns.Add("COMPANIA", typeof(System.String));
                    Dt_Proveedores_Tipo.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                    foreach (DataRow Renglon in Dt_Proveedores.Rows)
                    {

                        if (!String.IsNullOrEmpty(Renglon["CUENTA_PROVEEDOR_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "P-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        if (!String.IsNullOrEmpty(Renglon["CUENTA_CONTRATISTA_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "C-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        if (!String.IsNullOrEmpty(Renglon["CUENTA_JUDICIAL_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "J-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        if (!String.IsNullOrEmpty(Renglon["CUENTA_NOMINA_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "N-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        if (!String.IsNullOrEmpty(Renglon["CUENTA_ACREEDOR_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "A-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        if (!String.IsNullOrEmpty(Renglon["CUENTA_PREDIAL_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "Z-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        //if (!String.IsNullOrEmpty(Renglon["CUENTA_DEUDOR_ID"].ToString().Trim()))
                        //{
                        //    Fila = Dt_Proveedores_Tipo.NewRow();
                        //    Fila["COMPANIA"] = "D-" + Renglon["COMPANIA"].ToString().Trim();
                        //    Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                        //    Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                        //    Dt_Proveedores_Tipo.AcceptChanges();
                        //}
                    }
                    Cmb_Proveedor_Solicitud_Pago.DataSource = new DataTable();
                    Cmb_Proveedor_Solicitud_Pago.DataBind();
                    Cmb_Proveedor_Solicitud_Pago.DataSource = Dt_Proveedores_Tipo;
                    Cmb_Proveedor_Solicitud_Pago.DataTextField = Cat_Com_Proveedores.Campo_Compañia;
                    Cmb_Proveedor_Solicitud_Pago.DataValueField = Cat_Com_Proveedores.Campo_Proveedor_ID;
                    Cmb_Proveedor_Solicitud_Pago.DataBind();
                    Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                    Cmb_Proveedor_Solicitud_Pago.SelectedIndex = -1;
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Deudor_Click
        ///DESCRIPCIÓN          : Evento del boton de busqueda de empleados
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manueol Gallardo
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Buscar_Deudor_Click(object sender, ImageClickEventArgs e)
        {

            //Cls_Cat_Com_Proveedores_Negocio Rs_Consulta_Cat_Com_Proveedores = new Cls_Cat_Com_Proveedores_Negocio(); //Variable de conexión hacia la capa de negocios
            Cls_Ope_Con_Deudores_Negocio Rs_Consulta_Deudores = new Cls_Ope_Con_Deudores_Negocio();
            DataTable Dt_Proveedores;
            DataTable Dt_Proveedores_Tipo = new DataTable();
            String Tipo="";
            DataRow Fila;
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (Dt_Proveedores_Tipo.Rows.Count <= 0)
                {
                    Dt_Proveedores_Tipo.Columns.Add("NOMBRE", typeof(System.String));
                    Dt_Proveedores_Tipo.Columns.Add("DEUDOR_ID", typeof(System.String));
                }
                Tipo = Cmb_Tipo_Solicitud_Pago.SelectedItem.Text.ToUpper();
                Dt_Proveedores = Rs_Consulta_Deudores.Consulta_Deudores_Activos(); //Consulta los proveedores que coincidan con el nombre, compañia, rfc
                if (Dt_Proveedores.Rows.Count>0)
                {
                    if (!String.IsNullOrEmpty(Txt_Deudor.Text))
                    {
                        Dt_Proveedores.DefaultView.RowFilter = " NOMBRE like'%" + Txt_Deudor.Text.ToString() + "%' AND TIPO_MOVIMIENTO='" + Tipo + "'";
                        if (Dt_Proveedores.DefaultView.ToTable().Rows.Count > 0)
                        {
                            foreach (DataRow Renglon in Dt_Proveedores.DefaultView.ToTable().Rows)
                            {

                                if (Renglon["TIPO_DEUDOR"].ToString() == "PROVEEDOR")
                                {
                                    Fila = Dt_Proveedores_Tipo.NewRow();
                                    Fila["NOMBRE"] = Renglon["NOMBRE"].ToString().Trim();
                                    Fila["DEUDOR_ID"] = "P-" + Renglon["DEUDOR_ID"].ToString().Trim();
                                    Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Proveedores_Tipo.AcceptChanges();
                                }
                                else
                                {
                                    if (Dt_Proveedores_Tipo.Rows.Count == 0)
                                    {
                                        Fila = Dt_Proveedores_Tipo.NewRow();
                                        Fila["NOMBRE"] = Renglon["NOMBRE"].ToString().Trim();
                                        Fila["DEUDOR_ID"] = "E-" + Renglon["DEUDOR_ID"].ToString().Trim();
                                        Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Proveedores_Tipo.AcceptChanges();
                                    }
                                    else
                                    {
                                        Boolean Insertar = true;
                                        foreach (DataRow Renglon_Deudor in Dt_Proveedores_Tipo.Rows)
                                        {
                                            if (Renglon_Deudor["NOMBRE"].ToString().Trim() == Renglon["NOMBRE"].ToString().Trim())
                                            {
                                                Insertar = false;
                                            }
                                        }
                                        if (Insertar)
                                        {
                                            Fila = Dt_Proveedores_Tipo.NewRow();
                                            Fila["NOMBRE"] = Renglon["NOMBRE"].ToString().Trim();
                                            Fila["DEUDOR_ID"] = "E-" + Renglon["DEUDOR_ID"].ToString().Trim();
                                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                                            Dt_Proveedores_Tipo.AcceptChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Dt_Proveedores.DefaultView.RowFilter = " TIPO_MOVIMIENTO='" + Tipo + "'";
                        if (Dt_Proveedores.DefaultView.ToTable().Rows.Count >0)
                        {

                            foreach (DataRow Renglon in Dt_Proveedores.DefaultView.ToTable().Rows)
                            {

                                if (Renglon["TIPO_DEUDOR"].ToString() == "PROVEEDOR")
                                {
                                    Fila = Dt_Proveedores_Tipo.NewRow();
                                    Fila["NOMBRE"] = Renglon["NOMBRE"].ToString().Trim();
                                    Fila["DEUDOR_ID"] = "P-" + Renglon["DEUDOR_ID"].ToString().Trim();
                                    Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Proveedores_Tipo.AcceptChanges();
                                }
                                else
                                {
                                    if (Dt_Proveedores_Tipo.Rows.Count == 0)
                                    {
                                        Fila = Dt_Proveedores_Tipo.NewRow();
                                        Fila["NOMBRE"] = Renglon["NOMBRE"].ToString().Trim();
                                        Fila["DEUDOR_ID"] = "E-" + Renglon["DEUDOR_ID"].ToString().Trim();
                                        Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Proveedores_Tipo.AcceptChanges();
                                    }
                                    else
                                    {
                                        Boolean Insertar=true;
                                        foreach (DataRow Renglon_Deudor in Dt_Proveedores_Tipo.Rows)
                                        {
                                            if (Renglon_Deudor["NOMBRE"].ToString().Trim() == Renglon["NOMBRE"].ToString().Trim())
                                            {
                                                Insertar = false;
                                            }
                                        }
                                        if (Insertar)
                                        {
                                            Fila = Dt_Proveedores_Tipo.NewRow();
                                            Fila["NOMBRE"] = Renglon["NOMBRE"].ToString().Trim();
                                            Fila["DEUDOR_ID"] = "E-" + Renglon["DEUDOR_ID"].ToString().Trim();
                                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                                            Dt_Proveedores_Tipo.AcceptChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Cmb_Deudores.DataSource = new DataTable();
                Cmb_Deudores.DataBind();
                Cmb_Deudores.DataSource = Dt_Proveedores_Tipo;
                Cmb_Deudores.DataTextField = "NOMBRE";
                Cmb_Deudores.DataValueField = OPE_CON_DEUDORES.Campo_Deudor_ID;
                Cmb_Deudores.DataBind();
                Cmb_Deudores.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                Cmb_Deudores.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Unidad_Responsable_Busqueda_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de unidad responsable
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 22/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Cmb_Unidad_Responsable_Busqueda_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cls_Ope_Con_Reservas_Negocio Rs_Consulta_Dependencias = new Cls_Ope_Con_Reservas_Negocio();
            DataTable Dt_Consulta = new DataTable();

            try
            {
                if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex > 0)
                {
                    //  Para el grid
                    Grid_Partida_Saldo.Columns[0].Visible = true;
                    Grid_Partida_Saldo.Columns[1].Visible = true;
                    Grid_Partida_Saldo.Columns[3].Visible = true;
                    Grid_Partida_Saldo.Columns[4].Visible = true;
                    Grid_Partida_Saldo.Columns[5].Visible = true;
                    Grid_Partida_Saldo.Columns[15].Visible = true;
                    Grid_Partida_Saldo.DataSource = null;
                    Grid_Partida_Saldo.DataBind();

                    Rs_Consulta_Dependencias.P_Dependencia_ID = Cmb_Unidad_Responsable_Busqueda.SelectedValue;
                    Rs_Consulta_Dependencias.P_Ramo_33 = "NO";
                    Rs_Consulta_Dependencias.P_Filtro_Campo_Mes = Consultar_Nombre_Mes(String.Format("{0:0#}", (DateTime.Now.Month)));
                    Dt_Consulta = Rs_Consulta_Dependencias.Consultar_Reservas_Unidad_Responsable();

                    Grid_Partida_Saldo.DataSource = Dt_Consulta;
                    Grid_Partida_Saldo.DataBind();
                    Grid_Partida_Saldo.Columns[0].Visible = false;
                    Grid_Partida_Saldo.Columns[1].Visible = false;
                    Grid_Partida_Saldo.Columns[3].Visible = false;
                    Grid_Partida_Saldo.Columns[4].Visible = false;
                    Grid_Partida_Saldo.Columns[5].Visible = false;
                    Grid_Partida_Saldo.Columns[15].Visible = false;

                    //    //  para los combos
                    //    Cmb_Fuente_Financiamiento.Items.Clear();
                    //    Cmb_Programa.Items.Clear();
                    //    Cmb_Partida.Items.Clear();
                    //    Llenar_Combos_Generales();
                    //    if (Btn_Nuevo.ToolTip != "Nuevo")
                    //    {
                    //        Cmb_Fuente_Financiamiento.Enabled = true;
                    //        Cmb_Programa.Enabled = true;
                    //        Cmb_Partida.Enabled = false;
                    //        Grid_Partidas.DataSource = new DataTable();
                    //        Grid_Partidas.DataBind();
                    //    }
                    //}
                    //else 
                    //{
                    //    Cmb_Fuente_Financiamiento.Enabled = false;
                    //    Cmb_Programa.Enabled = false;
                    //    Cmb_Partida.Enabled = false;
                    //    Grid_Partidas.DataSource = new DataTable();
                    //    Grid_Partidas.DataBind();
                    //}
                    Session["Dt_Consulta"] = Dt_Consulta;
                    Session["Dt_Partidas_Asignadas"] = new DataTable();
                    Llenar_Grid_Partida_Asignada(false);
                    Tr_Div_Partidas.Visible = false;
                    //Tr_Div_Partidas.Style.Add("display", "none");
                }
                else
                {
                    Grid_Partida_Saldo.Columns[0].Visible = true;
                    Grid_Partida_Saldo.Columns[1].Visible = true;
                    Grid_Partida_Saldo.Columns[3].Visible = true;
                    Grid_Partida_Saldo.Columns[4].Visible = true;
                    Grid_Partida_Saldo.Columns[5].Visible = true;
                    Grid_Partida_Saldo.Columns[15].Visible = true;
                    Grid_Partida_Saldo.DataSource = null;
                    Grid_Partida_Saldo.DataBind();
                    Session["Dt_Partidas_Asignadas"] = new DataTable();
                    Llenar_Grid_Partida_Asignada(false);
                    Tr_Div_Partidas.Visible = false;
                    //Tr_Div_Partidas.Style.Add("display", "none");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al generar el evento del combo de unidad responsable Error[" + ex.Message + "]");
            }
    }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Fuente_Ramo_OnSelectedIndexChanged
        ///DESCRIPCIÓN          : Metodo que filtra el tipo de reserva para que solo muestre el ramo 33 o los que no son de ramo 33 
        ///CREO                 : Sergio Manuel Gallardo Andrade 
        ///FECHA_CREO           : 28/marzo/2012 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Cmb_Fuente_Ramo_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Grid_Partida_Asignada(false);
                Cls_Ope_Con_Reservas_Negocio Rs_Consulta_Dependencias = new Cls_Ope_Con_Reservas_Negocio();
                DataTable Dt_Consulta = new DataTable();
                if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex > 0)
                {
                    Rs_Consulta_Dependencias.P_Dependencia_ID = Cmb_Unidad_Responsable_Busqueda.SelectedValue;
                    Rs_Consulta_Dependencias.P_Ramo_33 = "NO";
                    Rs_Consulta_Dependencias.P_Filtro_Campo_Mes = Consultar_Nombre_Mes(String.Format("{0:0#}", (DateTime.Now.Month)));
                    Dt_Consulta = Rs_Consulta_Dependencias.Consultar_Reservas_Unidad_Responsable();
                    Grid_Partida_Saldo.Columns[0].Visible = true;
                    Grid_Partida_Saldo.Columns[1].Visible = true;
                    Grid_Partida_Saldo.Columns[3].Visible = true;
                    Grid_Partida_Saldo.Columns[4].Visible = true;
                    Grid_Partida_Saldo.Columns[5].Visible = true;
                    Grid_Partida_Saldo.Columns[15].Visible = true;
                    Grid_Partida_Saldo.DataSource = Dt_Consulta;
                    Grid_Partida_Saldo.DataBind();
                    Grid_Partida_Saldo.Columns[0].Visible = false;
                    Grid_Partida_Saldo.Columns[1].Visible = false;
                    Grid_Partida_Saldo.Columns[3].Visible = false;
                    Grid_Partida_Saldo.Columns[4].Visible = false;
                    Grid_Partida_Saldo.Columns[5].Visible = false;
                    Grid_Partida_Saldo.Columns[15].Visible = false;
                    Session["Dt_Partidas_Asignadas"] = new DataTable();
                    Llenar_Grid_Partida_Asignada(false);
                    Tr_Div_Partidas.Visible = false;
                    //Tr_Div_Partidas.Style.Add("display", "none");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de agregar un registro a la tabla. Error[" + ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Tipo_Modificacion_SelectedIndexChanged
        ///DESCRIPCIÓN          : Metodo para modificar el grid de las reservas para ampliar o reducir
        ///CREO                 : Sergio Manuel Gallardo Andrade 
        ///FECHA_CREO           : 27/Abril/2013 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Cmb_Tipo_Modificacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Habilitar_Controles("Modificar");
                // Llenar_Grid_Reservas();
                Llenar_Grid_Partidas_De_Reserva();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de agregar un registro a la tabla. Error[" + ex.Message + "]");
            }
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Agregar_Click
        ///DESCRIPCIÓN          : Evento del boton de agregar una partida a la reserva
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 20/Enero/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Agregar_Click(object sender, EventArgs e)
        {
            DataTable Dt_Partidas_Asignadas = new DataTable();
            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
            Mostrar_Informacion("", false);
            try
            {
                if (Validar_Datos())
                {
                    Llenar_Grid_Partida_Asignada(true);
                    Crear_Dt_Partida_Asignada();
                    Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                    //Txt_Importe.Text = String.Empty;
                    //Lbl_disponible.Text = String.Empty;
                    //Cmb_Partida.SelectedIndex = -1;
                    
                    Llenar_Grid_Partida_Asignada(false);
                    //  para limpiar las partidas y asi no se puedan meter mas paritdas
                    //Grid_Partida_Saldo.DataSource = new DataTable();
                    //Grid_Partida_Saldo.DataBind();
                }
                else
                {
                    Mostrar_Informacion(Lbl_Mensaje_Error.Text.Trim(), true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de agregar un registro a la tabla. Error[" + ex.Message + "]");
            }
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
        ///DESCRIPCIÓN          : Evento del boton de eliminar un registro de las partidas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 20/Enero/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            DataTable Dt_Partidas_Asignadas = new DataTable();
            ImageButton Btn_Eliminar = (ImageButton)sender;
            Int32 No_Fila = -1;
            String Id = String.Empty;
            try
            {
                Id = Btn_Eliminar.CommandArgument.ToString().Trim();
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                if (Dt_Partidas_Asignadas != null)
                {
                    if (Dt_Partidas_Asignadas.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                        {
                            No_Fila++;
                            if (Dr["PARTIDA_ID"].ToString().Trim().Equals(Id))
                            {
                                Dt_Partidas_Asignadas.Rows.RemoveAt(No_Fila);
                                Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Asignadas;
                                Llenar_Grid_Partida_Asignada(false);
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Btn_Busqueda_Cuentas_Popup_Click
        /// DESCRIPCION : Busca las cuentas contables referentes a la descripcion
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 02/Mayo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Btn_Busqueda_Partida_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DataTable Dt_Consulta = new DataTable();
                Dt_Consulta = (DataTable)Session["Dt_Consulta"];
                if (Dt_Consulta != null && Dt_Consulta.Rows.Count > 0)
                {
                    Dt_Consulta.DefaultView.RowFilter = "Clave_Partida LIKE '%" + Txt_Busqueda.Text.Trim() + "%'";
                    Grid_Partida_Saldo.Columns[0].Visible = true;
                    Grid_Partida_Saldo.Columns[1].Visible = true;
                    Grid_Partida_Saldo.Columns[3].Visible = true;
                    Grid_Partida_Saldo.Columns[4].Visible = true;
                    Grid_Partida_Saldo.Columns[5].Visible = true;
                    Grid_Partida_Saldo.Columns[15].Visible = true;
                    Grid_Partida_Saldo.DataSource = Dt_Consulta.DefaultView;
                    Grid_Partida_Saldo.DataBind();
                    Grid_Partida_Saldo.Columns[0].Visible = false;
                    Grid_Partida_Saldo.Columns[1].Visible = false;
                    Grid_Partida_Saldo.Columns[3].Visible = false;
                    Grid_Partida_Saldo.Columns[4].Visible = false;
                    Grid_Partida_Saldo.Columns[5].Visible = false;
                    Grid_Partida_Saldo.Columns[15].Visible = false;
                    Txt_Busqueda.Text = "";
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de buscar un registro a la tabla. Error[" + ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Txt_Busqueda_Partida_Click
        /// DESCRIPCION : Busca las cuentas contables referentes a la descripcion
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 28/Mayo/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Txt_Busqueda_Partida_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable Dt_Consulta = new DataTable();
                Dt_Consulta = (DataTable)Session["Dt_Consulta"];
                if (Dt_Consulta != null && Dt_Consulta.Rows.Count > 0)
                {
                    Dt_Consulta.DefaultView.RowFilter = "Clave_Partida LIKE '%" + Txt_Busqueda.Text.Trim() + "%'";
                    Grid_Partida_Saldo.Columns[0].Visible = true;
                    Grid_Partida_Saldo.Columns[1].Visible = true;
                    Grid_Partida_Saldo.Columns[3].Visible = true;
                    Grid_Partida_Saldo.Columns[4].Visible = true;
                    Grid_Partida_Saldo.Columns[5].Visible = true;
                    Grid_Partida_Saldo.Columns[15].Visible = true;
                    Grid_Partida_Saldo.DataSource = Dt_Consulta.DefaultView;
                    Grid_Partida_Saldo.DataBind();
                    Grid_Partida_Saldo.Columns[0].Visible = false;
                    Grid_Partida_Saldo.Columns[1].Visible = false;
                    Grid_Partida_Saldo.Columns[3].Visible = false;
                    Grid_Partida_Saldo.Columns[4].Visible = false;
                    Grid_Partida_Saldo.Columns[5].Visible = false;
                    Grid_Partida_Saldo.Columns[15].Visible = false;
                    Txt_Busqueda.Text = "";
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de buscar un registro a la tabla. Error[" + ex.Message + "]");
            }
        }
    ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Empleado_Click
        ///DESCRIPCIÓN          : Evento del boton de busqueda de empleados
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Buscar_Empleado_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Cat_Empleados_Negocios Rs_Consulta_Ca_Empleados = new Cls_Cat_Empleados_Negocios(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Empleados; //Variable que obtendra los datos de la consulta 
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (!String.IsNullOrEmpty(Txt_Nombre_Empleado.Text))
                {
                    //Verificar si es numerico
                    if (Cls_Util.EsNumerico(Txt_Nombre_Empleado.Text.Trim()) == false)
                    {
                        Rs_Consulta_Ca_Empleados.P_Nombre = Txt_Nombre_Empleado.Text.Trim();
                    }
                    else
                    {
                        Rs_Consulta_Ca_Empleados.P_No_Empleado = String.Format("{0:000000}", Convert.ToInt32(Txt_Nombre_Empleado.Text.Trim()));
                    }

                    Rs_Consulta_Ca_Empleados.P_Estatus = "ACTIVO";
                    Dt_Empleados = Rs_Consulta_Ca_Empleados.Consulta_Empleados_General();
                    Cmb_Nombre_Empleado.DataSource = new DataTable();
                    Cmb_Nombre_Empleado.DataBind();
                    Cmb_Nombre_Empleado.DataSource = Dt_Empleados;
                    Cmb_Nombre_Empleado.DataTextField = "Empleado";
                    Cmb_Nombre_Empleado.DataValueField = Cat_Empleados.Campo_Empleado_ID;
                    Cmb_Nombre_Empleado.DataBind();
                    Cmb_Nombre_Empleado.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                    Cmb_Nombre_Empleado.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Txt_Nombre_Proveedor_Solicitud_Pago_TextChanged
        ///DESCRIPCIÓN: Metodo al presionar enter 
        ///PARAMETROS: 
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 28/mayo/2013 10:54am
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Txt_Nombre_Proveedor_Solicitud_Pago_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Txt_Nombre_Proveedor_Solicitud_Pago.Text))
            {
                Cls_Cat_Com_Proveedores_Negocio Rs_Consulta_Cat_Com_Proveedores = new Cls_Cat_Com_Proveedores_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Proveedores;
                DataTable Dt_Proveedores_Tipo = new DataTable();
                DataRow Fila;
                try
                {
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    Rs_Consulta_Cat_Com_Proveedores.P_Nombre_Comercial = Txt_Nombre_Proveedor_Solicitud_Pago.Text;
                    Rs_Consulta_Cat_Com_Proveedores.P_Estatus = "ACTIVO";
                    Rs_Consulta_Cat_Com_Proveedores.P_tipo = "CONTABILIDAD";
                    Dt_Proveedores = Rs_Consulta_Cat_Com_Proveedores.Consulta_Avanzada_Proveedor(); //Consulta los proveedores que coincidan con el nombre, compañia, rfc
                    Dt_Proveedores_Tipo.Columns.Add("COMPANIA", typeof(System.String));
                    Dt_Proveedores_Tipo.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                    foreach (DataRow Renglon in Dt_Proveedores.Rows)
                    {

                        if (!String.IsNullOrEmpty(Renglon["CUENTA_PROVEEDOR_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "P-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        if (!String.IsNullOrEmpty(Renglon["CUENTA_CONTRATISTA_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "C-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        if (!String.IsNullOrEmpty(Renglon["CUENTA_JUDICIAL_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "J-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        if (!String.IsNullOrEmpty(Renglon["CUENTA_NOMINA_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "N-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        if (!String.IsNullOrEmpty(Renglon["CUENTA_ACREEDOR_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "A-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                        if (!String.IsNullOrEmpty(Renglon["CUENTA_PREDIAL_ID"].ToString().Trim()))
                        {
                            Fila = Dt_Proveedores_Tipo.NewRow();
                            Fila["COMPANIA"] = "Z-" + Renglon["COMPANIA"].ToString().Trim();
                            Fila["PROVEEDOR_ID"] = Renglon["PROVEEDOR_ID"].ToString().Trim();
                            Dt_Proveedores_Tipo.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Proveedores_Tipo.AcceptChanges();
                        }
                    }
                    Cmb_Proveedor_Solicitud_Pago.DataSource = new DataTable();
                    Cmb_Proveedor_Solicitud_Pago.DataBind();
                    Cmb_Proveedor_Solicitud_Pago.DataSource = Dt_Proveedores_Tipo;
                    Cmb_Proveedor_Solicitud_Pago.DataTextField = Cat_Com_Proveedores.Campo_Compañia;
                    Cmb_Proveedor_Solicitud_Pago.DataValueField = Cat_Com_Proveedores.Campo_Proveedor_ID;
                    Cmb_Proveedor_Solicitud_Pago.DataBind();
                    Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                    if (Cmb_Proveedor_Solicitud_Pago.Items.Count >= 2)
                    {
                        Cmb_Proveedor_Solicitud_Pago.SelectedIndex = 1;
                    }
                }
                catch
                {
                }
            }
        }
    
    #endregion

        #region (Grid)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Partidas_Saldo_RowDataBound
        ///DESCRIPCIÓN: 
        ///
        ///PARAMETROS:  
        ///CREO:    Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO: 29/Febrero/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Partidas_Saldo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                {
                    ((System.Web.UI.WebControls.TextBox)e.Row.Cells[13].FindControl("Txt_Importe_Partida")).ToolTip = "Disponible: " + e.Row.Cells[9].Text;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Partidas_Saldo_RowDataBound
        ///DESCRIPCIÓN: 
        ///
        ///PARAMETROS:  
        ///CREO:    Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO: 29/Febrero/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Partidas_d_Reserva_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                {
                    if (Cmb_Tipo_Modificacion.SelectedIndex == 0)
                    {
                        ((System.Web.UI.WebControls.TextBox)e.Row.Cells[10].FindControl("Txt_Importe_Comprometido")).ToolTip = "Disponible: " + e.Row.Cells[8].Text;
                    }
                    else
                    {
                        ((System.Web.UI.WebControls.TextBox)e.Row.Cells[10].FindControl("Txt_Importe_Comprometido")).ToolTip = "Disponible: " + e.Row.Cells[9].Text;
                    }
                    
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Sorting
        ///DESCRIPCIÓN          : Evento del grid para ordenar ascendentemente o descendentemente las columnas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 13/Enero/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Partidas_Sorting(object sender, GridViewSortEventArgs e)
        {
        //    Cls_Ope_Con_Reservas_Negocio Reservas_Negocio = new Cls_Ope_Con_Reservas_Negocio();
        //    Reservas_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Busqueda.SelectedValue.Trim();
        //    Reservas_Negocio.P_Fuente_Financiamiento = Cmb_Fuente_Financiamiento.SelectedValue.Trim();
        //    Reservas_Negocio.P_Proyecto_Programa_ID = Cmb_Programa.SelectedValue.Trim();
        //    Reservas_Negocio.P_Anio_Presupuesto = string.Format("{0:yyyy}", DateTime.Now);
        //    DataTable Data_Table = Reservas_Negocio.Consultar_Partidas_De_Un_Programa();

        //    if (Data_Table != null)
        //    {
        //        if (Data_Table.Rows.Count > 0)
        //        {
        //            Grid_Partidas.Columns[1].Visible = true;
        //            Grid_Partidas.DataSource = Data_Table;
        //            Grid_Partidas.DataBind();
        //            Grid_Partidas.Columns[1].Visible = false;
        //        }
        //    }

        //    DataTable Dt_Partidas = (Grid_Partidas.DataSource as DataTable);

        //    if (Dt_Partidas != null)
        //    {
        //        DataView Dv_Partidas = new DataView(Dt_Partidas);
        //        String Orden = ViewState["SortDirection"].ToString();

        //        if (Orden.Equals("ASC"))
        //        {
        //            Dv_Partidas.Sort = e.SortExpression + " " + "DESC";
        //            ViewState["SortDirection"] = "DESC";
        //        }
        //        else
        //        {
        //            Dv_Partidas.Sort = e.SortExpression + " " + "ASC";
        //            ViewState["SortDirection"] = "ASC";
        //        }

        //        Grid_Partidas.Columns[1].Visible = true;
        //        Grid_Partidas.DataSource = Dv_Partidas;
        //        Grid_Partidas.DataBind();
        //        Grid_Partidas.Columns[1].Visible = false;
        //    }
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_RowDataBound
        ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 20/enero/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Partida_Asignada_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataTable Dt_Partidas_Asignadas = new DataTable();
            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];

            try
            {
                if (Dt_Partidas_Asignadas != null)
                {
                    if (Dt_Partidas_Asignadas.Rows.Count > 0)
                    {
                        GridView Grid_Partidas_Detalle = (GridView)e.Row.Cells[4].FindControl("Grid_Partidas_Asignadas_Detalle");
                        if (e.Row.RowType == DataControlRowType.DataRow)
                        {
                            Grid_Partidas_Detalle.Columns[0].Visible = false;
                            Grid_Partidas_Detalle.Columns[1].Visible = true;
                            Grid_Partidas_Detalle.Columns[2].Visible = true;
                            Grid_Partidas_Detalle.Columns[5].Visible = true;
                            Grid_Partidas_Detalle.Columns[6].Visible = true;
                            Grid_Partidas_Detalle.Columns[7].Visible = true;
                            Grid_Partidas_Detalle.DataSource = Dt_Partidas_Asignadas;
                            Grid_Partidas_Detalle.DataBind();
                            Grid_Partidas_Detalle.Columns[1].Visible = false;
                            Grid_Partidas_Detalle.Columns[2].Visible = false;
                            Grid_Partidas_Detalle.Columns[6].Visible = false;
                            Grid_Partidas_Detalle.Columns[7].Visible = false;

                            if (Btn_Nuevo.ToolTip == "Nuevo")
                            {
                                Grid_Partidas_Detalle.Columns[5].Visible = false;
                            }
                            else
                            {
                                Grid_Partidas_Detalle.Columns[0].Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error:[" + Ex.Message + "]");
            }
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_Detalle_RowCreated
        ///DESCRIPCIÓN          : Evento del grid del movimiento del cursor sobre los registros
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 22/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Partidas_Asignadas_Detalle_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';this.style.color='#FFFFFF'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;this.style.color=this.originalstyle;");
            }
        }
    #endregion
}