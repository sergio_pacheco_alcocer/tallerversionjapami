﻿using System;
using System.Data;
using System.Web.Services;
using JAPAMI.Sessiones;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Ayudante_JQuery;


public partial class paginas_Contabilidad_Controlador_Contabilidad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Controlador_Inicio();
    }
    //// '*******************************************************************************
    ////'NOMBRE DE LA FUNCION:  contraldor
    ////'DESCRIPCION:           Obtener los datos en forma cadena y el json los convierte a datagrid en su caso
    ////'PARAMETROS:            response: 
    ////'                       request:
    ////'CREO:                  Sergio Manuel Gallardo 
    ////'FECHA_CREO:            13-Abril-2012
    ////'MODIFICO:
    ////'FECHA_MODIFICO
    ////'CAUSA_MODIFICACION
    ////'*******************************************************************************
    private void Controlador_Inicio()
    {
        String Accion = String.Empty;
        String Resuldato = "";
        Double Cantidad = Convert.ToDouble(Request.QueryString["valor"]);
        Double Meses = Convert.ToDouble(Request.QueryString["meses"].ToString())*2;
        Cantidad = Math.Round(Cantidad / Meses,2);
        Response.Clear();
        try
        {
            if (this.Request.QueryString["Accion"] != null)
            {
                if (!String.IsNullOrEmpty(this.Request.QueryString["Accion"].ToString().Trim()))
                {
                    Accion = this.Request.QueryString["Accion"].ToString().Trim();
                    switch (Accion)
                    {
                        case "Porcentaje":
                            Resuldato = Obtener_porcentaje(Convert.ToString(Cantidad));
                            break;
                    }
                }
            }
            Response.Write(Resuldato);
            Response.Flush();
            Response.Close();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Controlador_Inicio Error[" + ex.Message + "]");
        }
    }
    private String Obtener_porcentaje(String filtro)
    {
        Cls_Ope_Con_Solicitud_Pagos_Negocio Negocio = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //conexion con la capa de negocios
        String Json_Anios = string.Empty;
        DataTable Dt_Anios = new DataTable();

        try
        {
            Negocio.P_Limite_Inferior = filtro;
            Dt_Anios = Negocio.Consulta_Parametro_ISR();
            if (Dt_Anios != null)
            {
                if (Dt_Anios.Rows.Count > 0)
                {
                    Json_Anios = Dt_Anios.Rows[0]["Porcentaje"].ToString();
                }
            }
            return Json_Anios;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Obtener_Anios Error[" + ex.Message + "]");
        }
    }


}
