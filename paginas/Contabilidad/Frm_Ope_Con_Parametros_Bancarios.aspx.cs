﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
using System.Windows.Forms;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sindicatos.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using AjaxControlToolkit;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Parametros_Bancarios : System.Web.UI.Page
{
    #region Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones   //Limpia los controles del forma
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE:         Configuracion_Acceso
    /// DESCRIPCIÓN:    Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS:     No Áplica.
    /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   30/Enero/2012
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Salir);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Buscar);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION :   Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS:     Cadena.- El dato a evaluar si es numerico.
    /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   30/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region Metodos
    #region(Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda
    ///               realizar diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 17/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpiar_Controles();
            //Habilitar_Controles("Nuevo");
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Cargar_Grid();
            Btn_Modificar.Visible = false;
            Cls_Cat_Con_Parametros_Negocio consulta_parametros = new Cls_Cat_Con_Parametros_Negocio();
            DataTable Dt_Parametros = new DataTable();
            Dt_Parametros = consulta_parametros.Consulta_Datos_Parametros();
            if (Dt_Parametros.Rows.Count > 0)
            {
                Txt_IVA_Parametro.Value = Dt_Parametros.Rows[0]["IVA"].ToString().Trim();
            }
            else
            {
                Txt_IVA_Parametro.Value = "0";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 30/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            Txt_Busqueda.Text = "";
            Txt_Clabe.Text = "";
            Txt_Nombre_Banco.Text = "";
            Txt_Iva_Comision.Text = "";
           Txt_Comision.Text = "";
           Txt_IVA_Parametro.Value = "";

        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }
     ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION :   Habilita y Deshabilita los controles de la forma para prepara la página
    ///                 para a siguiente operación
    /// PARAMETROS:     1.- Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                 si es una alta, modificacion
     /// CREO:          Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO:     20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Modificar.CausesValidation = false;
                    Btn_Modificar.Visible = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Configuracion_Acceso("Frm_Ope_Con_Parametros_Bancarios.aspx");
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    break;
            }
            //  mensajes de error
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            Txt_Clabe.Enabled = Habilitado;
            //Cmb_Banco.Enabled = Habilitado;
            Txt_Nombre_Banco.Enabled = Habilitado;
            Txt_Comision.Enabled = Habilitado;
            Txt_Iva_Comision.Enabled = Habilitado;
            Txt_Busqueda.Enabled = !Habilitado;
        }

        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }
    #endregion

    #region(Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos
    /// DESCRIPCION : Validar que se se encuentre todos los datos para continuar con el proceso
    /// CREO        : Sergio Manuel Gallardo
    /// FECHA_CREO  : 17/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        String Espacios_Blanco = "";
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Lbl_Mensaje_Error.Text += Espacios_Blanco + "Es necesario Introducir: <br>";
        Lbl_Mensaje_Error.Visible = true;
        Img_Error.Visible = true;
        try
        {
            if (Txt_Nombre_Banco.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Selecciona un banco.<br>";
                Datos_Validos = false;
            }
            if (Txt_Comision.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese La comision que cobra el banco por transferencia.<br>";
                Datos_Validos = false;
            }
            if (Txt_Iva_Comision.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el iva que cobra el banco por transferencia.<br>";
                Datos_Validos = false;
            }
        }
        catch
        {
        }
        return Datos_Validos;
    }

    #endregion

    #region (Consultas)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Grid
    /// DESCRIPCION : Carga la informacion en el combo banco
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 17/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Grid()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Cls_Ope_Con_Cheques_Bancos_Negocio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        //Cls_Cat_Nom_Bancos_Negocio Rs_Bancos = new Cls_Cat_Nom_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.P_Comisiones = "true";
            Dt_Consulta = Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.Consultar_Bancos_Existentes();
            DataView Dv_Ordenar = new DataView(Dt_Consulta);
            Dv_Ordenar.Sort = "NOMBRE";
            Dt_Consulta = Dv_Ordenar.ToTable();


            Session["Grid_Bancos"] = Dt_Consulta;
            Grid_Bancos.Columns[5].Visible = true;
            Grid_Bancos.Columns[6].Visible = true;
            Grid_Bancos.Columns[7].Visible = true;
            Grid_Bancos.DataSource = (DataTable)Session["Grid_Bancos"];
            Grid_Bancos.DataBind();
            Grid_Bancos.Columns[5].Visible = false;
            Grid_Bancos.Columns[6].Visible = false;
            Grid_Bancos.Columns[7].Visible = false;
            Grid_Bancos.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Comisiones Bancarias" + ex.Message.ToString(), ex);
        }
    } 

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Grid_Banco
    /// DESCRIPCION : Carga la informacion en el combo banco
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 17/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Grid_Banco()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Cls_Ope_Con_Cheques_Bancos_Negocio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.P_Nombre_Banco = Txt_Busqueda.Text.ToUpper();
            Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.P_Comisiones = "true";
            Dt_Consulta = Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.Consultar_Bancos_Existentes();
            Session["Grid_Bancos"] = Dt_Consulta;
            Grid_Bancos.DataSource = (DataTable)Session["Grid_Bancos"];
            Grid_Bancos.DataBind();
            Grid_Bancos.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Gastos por Comprobar" + ex.Message.ToString(), ex);
        }
    } 
    #endregion

    #region (Operaciones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Modificar_Banco
    ///DESCRIPCIÓN: Pasa los elementos a la capa de negocios
    ///PARAMETROS:  
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Banco()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Modificar_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        try
        {
            //Rs_Modificar_Banco.P_Folio_Inicial = Txt_Folio_Inicial.Text;
            Rs_Modificar_Banco.P_Nombre_Banco = Txt_Nombre_Banco.Text.ToUpper();
            Rs_Modificar_Banco.P_Comision_Transferencia = Txt_Comision.Text;
            Rs_Modificar_Banco.P_Iva_Comision = Txt_Iva_Comision.Text;
            Rs_Modificar_Banco.P_Banco_Clabe = Txt_Clabe.Text;
            Rs_Modificar_Banco.P_Comision_Spay = Txt_Comision_Spay.Text;
            Rs_Modificar_Banco.P_IVA_Spay = Txt_IVA_Comision_Spay.Text;
            Rs_Modificar_Banco.P_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Banco.Modificar_Comisiones_Bancos();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Parametros Bancarios", "alert('La modificacion de los parametros bancarios fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion

    #endregion

    #region Eventos
    
    #region (Botones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: realizara una modificaion
    ///PARAMETROS: 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
                if (Validar_Datos())
                {
                    Modificar_Banco();
                    Div_Grid.Style.Value = "overflow:auto;height:200px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:block";
                    Div_Detalles.Style.Value = "display:none";
                    Inicializa_Controles();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar
    ///DESCRIPCIÓN: Busca los bancos
    ///PARAMETROS: 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Cargar_Grid_Banco();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operacion actual que se este realizando
    ///PARAMETROS: 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        //Div_Grid.Style.Value = "overflow:auto;height:200px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:block"";
        //            Div_Detalles.Style.Value = "display:none";
        if ((Btn_Salir.ToolTip == "Cancelar") || (Div_Grid.Style.Value == "display:none"))
        {
            Div_Grid.Style.Value = "overflow:auto;height:200px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:block";
            Div_Detalles.Style.Value = "display:none";
            Inicializa_Controles();
        }
        
        else
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
    }
    #endregion

    #endregion

    #region Grids
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Bancos_SelectedIndexChanged
    /// DESCRIPCION : Consulta los datos de los movimientos seleccionada por el usuario
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 17/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Bancos_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            GridViewRow selectedRow = Grid_Bancos.Rows[Grid_Bancos.SelectedIndex];
            Txt_Nombre_Banco.Text = HttpUtility.HtmlDecode(selectedRow.Cells[1].Text).ToString();
            Txt_Clabe.Text = HttpUtility.HtmlDecode(selectedRow.Cells[2].Text).ToString();
            Txt_Comision.Text = HttpUtility.HtmlDecode(selectedRow.Cells[3].Text).ToString();
            Txt_Iva_Comision.Text = HttpUtility.HtmlDecode(selectedRow.Cells[4].Text).ToString();
            Txt_No_Cuenta.Text = HttpUtility.HtmlDecode(selectedRow.Cells[5].Text).ToString();
            Txt_Comision_Spay.Text = HttpUtility.HtmlDecode(selectedRow.Cells[6].Text).ToString();
            Txt_IVA_Comision_Spay.Text = HttpUtility.HtmlDecode(selectedRow.Cells[7].Text).ToString();
            Div_Grid.Style.Value = "display:none";
            Div_Detalles.Style.Value = "display:block";

            Txt_Busqueda.Enabled = false;

            Habilitar_Controles("Modificar");
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
}
