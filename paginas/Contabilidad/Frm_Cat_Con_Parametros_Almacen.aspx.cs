﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Con_Parametros_Almacen.Negocio;
using System.Data;
using JAPAMI.Constantes;


public partial class paginas_Contabilidad_Frm_Cat_Con_Parametros_Almacen : System.Web.UI.Page
{
    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region PAGE LOAD
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Configurar_Formulario("Inicio");
            Llenar_Combos();
            Consultar_Parametros_Inicio();
        }
    }
    #endregion

    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************
    
    #region Metodos
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
    ///´de los componentes de la pagina
    ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 22/Nov/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Configurar_Formulario(String Estatus)
    {

        switch (Estatus)
        {
            case "Inicio":

                //Boton Modificar
                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Modificar.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Inicio";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                //habilitar los componenetes
                Txt_Cuenta_A_General.Enabled = false;
                Txt_Cuenta_A_Papeleria.Enabled = false;
                Txt_Cuenta_IVA.Enabled = false;
                Txt_Cuenta_IVA_Pendiente.Enabled = false;
                Cmb_Cuenta_A_General.Enabled = false;
                Cmb_Cuenta_A_Papeleria.Enabled = false;
                Cmb_Cuenta_IVA.Enabled = false;
                Cmb_Cuenta_IVA_Pendiente.Enabled = false;
                Cmb_Poliza_Egresos.Enabled = false;
                Cmb_Poliza_Diario.Enabled = false;


                break;
            case "Modificar":

                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Guardar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Modificar.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                //habilitar los componenetes
                Txt_Cuenta_A_General.Enabled = true;
                Txt_Cuenta_A_Papeleria.Enabled = true;
                Txt_Cuenta_IVA.Enabled = true;
                Txt_Cuenta_IVA_Pendiente.Enabled = true;
                Cmb_Cuenta_A_General.Enabled = true;
                Cmb_Cuenta_A_Papeleria.Enabled = true;
                Cmb_Cuenta_IVA.Enabled = true;
                Cmb_Cuenta_IVA_Pendiente.Enabled = true;
                Cmb_Poliza_Egresos.Enabled = true;
                Cmb_Poliza_Diario.Enabled = true;

                break;
        }//fin del switch

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo
    ///DESCRIPCIÓN: Metodo que llena los combos
    ///PARAMETROS: 
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 22/Nov/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combos()
    {
        try
        {
            Cmb_Cuenta_A_General.Items.Clear();
            Cmb_Cuenta_A_General.Items.Add("<<SELECCIONAR>>");
            Cmb_Cuenta_A_General.Items[0].Value = "0";
            Cmb_Cuenta_A_General.Items[0].Selected = true;

            Cmb_Cuenta_A_Papeleria.Items.Clear();
            Cmb_Cuenta_A_Papeleria.Items.Add("<<SELECCIONAR>>");
            Cmb_Cuenta_A_Papeleria.Items[0].Value = "0";
            Cmb_Cuenta_A_Papeleria.Items[0].Selected = true;

            Cmb_Cuenta_IVA.Items.Clear();
            Cmb_Cuenta_IVA.Items.Add("<<SELECCIONAR>>");
            Cmb_Cuenta_IVA.Items[0].Value = "0";
            Cmb_Cuenta_IVA.Items[0].Selected = true;
            //llenamos el Combo de Tipo polizas
            Cls_Cat_Con_Parametros_Almacen_Negocio Negocio = new Cls_Cat_Con_Parametros_Almacen_Negocio();
            DataTable Dt_Tipo_Polizas = Negocio.Consultar_Tipos_Polizas();
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Poliza_Diario, Dt_Tipo_Polizas);
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Poliza_Egresos, Dt_Tipo_Polizas);
        }
        catch (Exception Ex) 
        {
            Mensaje_Error("Error al  llenar los combos " + Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Mensaje_Error
    ///DESCRIPCIÓN          : Metodo que hace visible el mesaje de error
    ///                       o lo oculta segun los parametros.
    ///PARAMETROS           : String Mensaje,  mesaje del error a mostrar en pantalla.
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 26/Nov/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Mensaje_Error(String Mensaje) 
    {
        Lbl_Mensaje_Error.Text += "+ " + Mensaje + "</br>";
        IBtn_Imagen_Error.Visible = true;
        Div_Contenedor_Msj_Error.Visible = true;
    }
    public void Mensaje_Error() 
    {
        Lbl_Mensaje_Error.Text = "";
        IBtn_Imagen_Error.Visible = false;
        Div_Contenedor_Msj_Error.Visible = false;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Consultar_Cuenta
    ///DESCRIPCIÓN          : Permite Consultar las cuentas contables.
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 26/Nov/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public DataTable Consultar_Cuenta(TextBox Txt_Cuenta) 
    {
        Mensaje_Error();
        DataTable Dt_Cuentas_Contables =  new DataTable();
        Cls_Cat_Con_Parametros_Almacen_Negocio Clase_Negocio =  new Cls_Cat_Con_Parametros_Almacen_Negocio();
        try
        {
            Clase_Negocio.P_Cuenta_Busqueda = Txt_Cuenta.Text.ToString().Trim(); //Caracteres a considerar para la busqueda de las cuentas
            Dt_Cuentas_Contables = Clase_Negocio.Consultar_Cuentas_Contables(); //Consultar las cuentas contables
        }
        catch (Exception Ex)
        {
            Mensaje_Error("Error al consultar las cuentas: " + Ex.Message);
        }
        return Dt_Cuentas_Contables;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Parametros_Inicio
    ///DESCRIPCIÓN: Metodo que carga los combos con la información general.
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 03/DIC/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Consultar_Parametros_Inicio()
    {
        try
        {
            Cls_Cat_Con_Parametros_Almacen_Negocio Negocio = new Cls_Cat_Con_Parametros_Almacen_Negocio();
            DataTable Dt_Parametros_Almacen = Negocio.Consultar_Parametros();
            DataTable Dt_Cuentas_Almacen = Negocio.Consultar_Cuentas_Almacen();
            DataTable Dt_Cuentas = null;
            if (Dt_Cuentas_Almacen != null && Dt_Cuentas_Almacen.Rows.Count > 0)
            {
                Negocio.P_Cuenta_ID = Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID].ToString();
                Dt_Cuentas = Negocio.Consultar_Cuentas_Contables();
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Cuenta_A_Papeleria, Dt_Cuentas);
                Cmb_Cuenta_A_Papeleria.SelectedValue = Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID].ToString();
                Txt_Cuenta_A_Papeleria.Text = Dt_Cuentas.Rows[0][1].ToString().Trim();

                Negocio.P_Cuenta_ID = Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID].ToString();
                Dt_Cuentas = Negocio.Consultar_Cuentas_Contables();
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Cuenta_A_General, Dt_Cuentas);
                Cmb_Cuenta_A_General.SelectedValue = Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID].ToString();
                Txt_Cuenta_A_General.Text = Dt_Cuentas.Rows[0][1].ToString().Trim();

                Negocio.P_Cuenta_ID = Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString();
                Dt_Cuentas = Negocio.Consultar_Cuentas_Contables();
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Cuenta_IVA, Dt_Cuentas);
                Cmb_Cuenta_IVA.SelectedValue = Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString();
                Txt_Cuenta_IVA.Text = Dt_Cuentas.Rows[0][1].ToString().Trim();

                Negocio.P_Cuenta_ID = Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString();
                Dt_Cuentas = Negocio.Consultar_Cuentas_Contables();
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Cuenta_IVA_Pendiente, Dt_Cuentas);
                Cmb_Cuenta_IVA_Pendiente.SelectedValue = Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString();
                Txt_Cuenta_IVA_Pendiente.Text = Dt_Cuentas.Rows[0][1].ToString().Trim();
            }
            else
            {
                Txt_Cuenta_A_General.Text = "";
                Txt_Cuenta_A_Papeleria.Text = "";
                Txt_Cuenta_IVA.Text = "";
                Txt_Cuenta_IVA_Pendiente.Text = "";
            }

            if(Dt_Parametros_Almacen != null && Dt_Parametros_Almacen.Rows.Count > 0)
            {
                Cmb_Poliza_Diario.SelectedValue = Dt_Parametros_Almacen.Rows[0][Cat_Con_Parametros.Campo_Tipo_Poliza_Diario_ID].ToString();
                Cmb_Poliza_Egresos.SelectedValue = Dt_Parametros_Almacen.Rows[0][Cat_Con_Parametros.Campo_Tipo_Poliza_Egresos_ID].ToString();                
            }
           
        }
        catch (Exception Ex)
        {
            Mensaje_Error("Error al cargar los datos " + Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Datos
    ///DESCRIPCIÓN: Valida que todos los datos de los paramentros se encuentren
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 03/Dic/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public Boolean Validar_Datos()
    {
        Boolean Validacion = true;
        if (Cmb_Cuenta_A_General.SelectedIndex <= 0) 
        {
            Validacion = false;
            Mensaje_Error("Favor de ingresar y seleccionar la Cuenta General");
        }
        if (Cmb_Cuenta_A_Papeleria.SelectedIndex <= 0)
        {
            Validacion = false;
            Mensaje_Error("Favor de ingresar y seleccionar la Cuenta Papelería");
        }
        if (Cmb_Cuenta_IVA.SelectedIndex <= 0)
        {
            Validacion = false; 
            Mensaje_Error("Favor de ingresar y seleccionar la Cuenta IVA ");
        }
        if (Cmb_Poliza_Diario.SelectedIndex <= 0)
        {
            Validacion = false;
            Mensaje_Error("Favor de seleccionar la poliza de Diario ");
        }
        if (Cmb_Poliza_Egresos.SelectedIndex <= 0)
        {
            Validacion = false;
            Mensaje_Error("Favor de seleccionar la poliza de Egresos");
        }
        return Validacion;
    }
    ///******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Guardar_Parametros_Almacen
    ///DESCRIPCIÓN: Metodo que inserta o modifca los 
    ///PARAMETROS: 
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 22/Nov/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Guardar_Parametros_Almacen()
    {
        try
        {
            Mensaje_Error();
            Cls_Cat_Con_Parametros_Almacen_Negocio Negocio = new Cls_Cat_Con_Parametros_Almacen_Negocio();
            if (Validar_Datos() == true)
            {
                DataTable Dt_Parametros_Almacen = Negocio.Consultar_Parametros();
                Negocio.P_Cuenta_Almacen_General = Cmb_Cuenta_A_General.SelectedValue.ToString();
                Negocio.P_Cuenta_Almacen_Papeleria = Cmb_Cuenta_A_Papeleria.SelectedValue.ToString();
                Negocio.P_Cuenta_IVA_Acreditable = Cmb_Cuenta_IVA.SelectedValue.ToString();
                Negocio.P_Cuenta_IVA_Pendiente = Cmb_Cuenta_IVA_Pendiente.SelectedValue.ToString();
                Negocio.P_Tipo_Poliza_Diario_ID = Cmb_Poliza_Diario.SelectedValue.ToString();
                Negocio.P_Tipo_Poliza_Egresos_ID = Cmb_Poliza_Egresos.SelectedValue.ToString();
                if (Dt_Parametros_Almacen != null && Dt_Parametros_Almacen.Rows.Count > 0)
                {
                    Negocio.Modificar_Parametros_Almacen();
                    Configurar_Formulario("Inicio");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Parametros Para Almacen ", "alert('El Alta de los parametros fue Exitosa');", true);
                }
                else
                {
                    Mensaje_Error("Es necesario configurar la mascara de la cuenta contable en la pantalla en donde se configuran los parametros de contabilidad ");
                }
            }
            else 
            {
                Lbl_Mensaje_Error.Visible = true;
                IBtn_Imagen_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error("No se pudieron actualizar los parametros del almacen " + Ex.Message);
        }
    }
    #endregion

    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Txt_Cuenta_A_General_TextChanged
    ///DESCRIPCIÓN          : Evento de la caja de texto en el que se consulta la 
    ///                       cuenta segun el contendo de la  caja de texto
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 26/Nov/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Txt_Cuenta_A_General_TextChanged(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Cuenta_A_General.Text.ToString())) 
        {
            Mensaje_Error();
            DataTable Dt_Cuenta_A_General = Consultar_Cuenta(Txt_Cuenta_A_General);
            if (Dt_Cuenta_A_General != null && Dt_Cuenta_A_General.Rows.Count > 0)
            {
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Cuenta_A_General, Dt_Cuenta_A_General);
                Cmb_Cuenta_A_General.SelectedIndex = 1;
                String Cuenta = Cmb_Cuenta_A_General.SelectedItem.ToString().Trim();
                String[] NO_Cuenta = Cuenta.Split('-');
                Txt_Cuenta_A_General.Text = NO_Cuenta[0];
            }
            else 
            {
                Mensaje_Error("No se encontraron coincidencias con ese número de cuenta ");

                Cmb_Cuenta_A_General.Items.Clear();
                Cmb_Cuenta_A_General.Items.Add("<<SELECCIONAR>>");
                Cmb_Cuenta_A_General.Items[0].Value = "0";
                Cmb_Cuenta_A_General.Items[0].Selected = true;
            }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Txt_Cuenta_A_Papeleria_TextChanged
    ///DESCRIPCIÓN          : Evento de la caja de texto en el que se consulta la 
    ///                       cuenta segun el contendo de la  caja de texto
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 26/Nov/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Txt_Cuenta_A_Papeleria_TextChanged(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Cuenta_A_Papeleria.Text.ToString()))
        {
            DataTable Dt_Cuenta_A_Papeleria = Consultar_Cuenta(Txt_Cuenta_A_Papeleria);
            if (Dt_Cuenta_A_Papeleria != null && Dt_Cuenta_A_Papeleria.Rows.Count > 0)
            {
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Cuenta_A_Papeleria, Dt_Cuenta_A_Papeleria);
                Cmb_Cuenta_A_Papeleria.SelectedIndex = 1;
                String Cuenta = Cmb_Cuenta_A_Papeleria.SelectedItem.ToString().Trim();
                String[] NO_Cuenta = Cuenta.Split('-');
                Txt_Cuenta_A_Papeleria.Text = NO_Cuenta[0];
            }
            else 
            {
                Mensaje_Error("No se encontraron coincidencias con ese número de cuenta ");

                Cmb_Cuenta_A_Papeleria.Items.Clear();
                Cmb_Cuenta_A_Papeleria.Items.Add("<<SELECCIONAR>>");
                Cmb_Cuenta_A_Papeleria.Items[0].Value = "0";
                Cmb_Cuenta_A_Papeleria.Items[0].Selected = true;
            }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Txt_Cuenta_IVA_TextChanged
    ///DESCRIPCIÓN          : Evento de la caja de texto en el que se consulta la 
    ///                       cuenta segun el contendo de la  caja de texto
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 26/Nov/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Txt_Cuenta_IVA_TextChanged(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Cuenta_IVA.Text.ToString()))
        {
            DataTable Dt_Cuenta_IVA = Consultar_Cuenta(Txt_Cuenta_IVA);
            if (Dt_Cuenta_IVA != null && Dt_Cuenta_IVA.Rows.Count > 0)
            {
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Cuenta_IVA, Dt_Cuenta_IVA);
                Cmb_Cuenta_IVA.SelectedIndex = 1;
                String Cuenta = Cmb_Cuenta_IVA.SelectedItem.ToString().Trim();
                String[] NO_Cuenta = Cuenta.Split('-');
                Txt_Cuenta_IVA.Text = NO_Cuenta[0];
            }
            else 
            {
                Mensaje_Error("No se encontraron coincidencias con ese número de cuenta ");
                Cmb_Cuenta_IVA.Items.Clear();
                Cmb_Cuenta_IVA.Items.Add("<<SELECCIONAR>>");
                Cmb_Cuenta_IVA.Items[0].Value = "0";
                Cmb_Cuenta_IVA.Items[0].Selected = true;
            }
        }
    }

    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Modificar.ToolTip == "Modificar")
        {
            Configurar_Formulario("Modificar");
        }
        else
        {
            Guardar_Parametros_Almacen();           
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.ToolTip == "Salir")
        {
            Session.Remove("Consulta_Cuentas_Contables");
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else
        {
            Configurar_Formulario("Inicio"); //COnfigura los cotroles en el estatus inicio
            Consultar_Parametros_Inicio();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Cuenta_A_General_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo, llena la caja de texto con la cuenta seleccionada
    ///                       del combo
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 27/Nov/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Cuenta_A_General_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Cuenta_A_General.SelectedIndex > 0) 
        {
            String Cuenta = Cmb_Cuenta_A_General.SelectedItem.ToString().Trim();
            String [] NO_Cuenta = Cuenta.Split('-');
            Txt_Cuenta_A_General.Text = NO_Cuenta[0];
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Cuenta_A_Papeleria_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo, llena la caja de texto con la cuenta seleccionada
    ///                       del combo
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 27/Nov/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Cuenta_A_Papeleria_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Cuenta_A_Papeleria.SelectedIndex > 0)
        {
            String Cuenta = Cmb_Cuenta_A_Papeleria.SelectedItem.ToString().Trim();
            String[] NO_Cuenta = Cuenta.Split('-');
            Txt_Cuenta_A_Papeleria.Text = NO_Cuenta[0];
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Cuenta_IVA_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo, llena la caja de texto con la cuenta seleccionada
    ///                       del combo
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 27/Nov/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Cuenta_IVA_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Cuenta_IVA.SelectedIndex > 0)
        {
            String Cuenta = Cmb_Cuenta_IVA.SelectedItem.ToString().Trim();
            String[] NO_Cuenta = Cuenta.Split('-');
            Txt_Cuenta_IVA.Text = NO_Cuenta[0];
        }
    }
    #endregion
}
