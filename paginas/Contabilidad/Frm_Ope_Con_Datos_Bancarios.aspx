﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Datos_Bancarios.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Datos_Bancarios" Title="Datos Bancos" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
   <cc1:ToolkitScriptManager ID="ScriptManager_Datos" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager>
       <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
             <ContentTemplate>
               <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div  class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                
            <div id="Div_General" runat="server"  style="background-color:#ffffff; width:100%; height:100%;"> <%--Fin del div General--%>
                    <table  width="98%" border="0" cellspacing="0" class="estilo_fuente" frame="border">
                        <tr align="center">
                            <td class="label_titulo" colspan="2">Datos Bancarios Proveedores</td>
                       </tr>
                        <tr> <!--Bloque del mensaje de error-->
                            <td colspan="2">
                                <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                            </td>      
                        </tr>
                        
                        <tr class="barra_busqueda" align="right">
                            <td align="left">
                               <asp:ImageButton ID="Btn_Modificar" runat="server"  ToolTip="Modificar" CssClass="Img_Button" TabIndex="2"
                                    ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" />     
                                <asp:ImageButton ID="Btn_Salir" runat="server" 
                                    CssClass="Img_Button" 
                                    ToolTip="Salir"
                                    ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                    onclick="Btn_Salir_Click"/>
                             </td>
                             <td style="width:50%">B&uacute;squeda   
                                <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="100" TabIndex="5"  ToolTip = "Buscar por Nombre" Width="180px"/>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                        WatermarkText="Ingrese nombre del banco" TargetControlID="Txt_Busqueda" />
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" 
                                        runat="server" TargetControlID="Txt_Busqueda" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. "/>
                                 <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6"
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Consultar"
                                        onclick="Btn_Buscar_Click" />
                            </td>                         
                        </tr>
                    </table>
                
                    <div id="Div_Detalles" runat="server" style="overflow:auto;height:auto;width:100%;vertical-align:top;border-style:outset;border-color:Silver;display:none">
                        <asp:UpdatePanel ID="Upnl_Detalle" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>             
                                <asp:Panel ID="Pnl_Detalle" runat="server" GroupingText="Datos Generales" Width="98%" BackColor="white">                                
                                    <table class="estilo_fuente" width="98%">
                                         
                                        <tr>
                                            <td style="width:20%;">
                                                <asp:Label ID="Lbl_Proveedor" runat="server" Text="Proveedor"></asp:Label>
                                            </td>
                                            <td colspan="4">
                                                <asp:TextBox ID="Txt_Proveedor" runat="server" ReadOnly="true" Width="94%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <asp:Panel ID="Panel1" runat="server" GroupingText="Datos Banco" Width="98%" BackColor="White">
                                                    <table width="99.5%"  border="0" cellspacing="0" class="estilo_fuente">              
                                                        <tr>
                                                            <td style="width:20%; text-align:left; cursor:default;">
                                                                *Banco 
                                                            </td>
                                                            <td style="width:30%; text-align:left; cursor:default;">
                                                                <asp:TextBox ID="Txt_Banco_Proveedor" runat="server" Width="99%" ></asp:TextBox>
                                                                 <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Empleado" runat="server" TargetControlID="Txt_Banco_Proveedor"
                                                                    FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars=" ÁÉÍÓÚáéíóúÑñ"/>
                                                                <%--<asp:DropDownList ID="Cmb_Banco" runat="server" Width="98%"></asp:DropDownList>--%>                         
                                                            </td>
                                                            <td style="width:20%; text-align:left; cursor:default;">*Tipo Cuenta</td>
                                                            <td style="width:30%; text-align:left; cursor:default;">
                                                                <asp:DropDownList ID="Cmb_Tipo_Cuenta_Proveedor" runat="server" Width="99%">
                                                                    <asp:ListItem Text="&larr;Seleccione&rarr;" Value="0"></asp:ListItem>
                                                                    <asp:ListItem Text="N&uacute;mero de Cuenta" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="N&uacute;mero de Tarjeta de D&eacute;bito" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="Cuenta CLABE" Value="40"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                             <%--<td style="width:20%; text-align:left; cursor:default;">
                                                                ID Banco Acreedor/Deudor
                                                            </td>
                                                            <td style="width:30%; text-align:left; cursor:default;">
                                                                <asp:TextBox ID="Txt_Bancos_ID" runat="server"  Width="98%" TabIndex="0" MaxLength="20"></asp:TextBox>
                                                                 <cc1:FilteredTextBoxExtender ID="Filt_Txt_Bancos_ID" runat="server" 
                                                                TargetControlID="Txt_Bancos_ID" FilterType="Numbers" ValidChars="0123456789" />
                                                            </td>--%>
                                                       </tr> 
                                                    </table>                                                
                                                    <table width="99.5%"  border="0" cellspacing="0" class="estilo_fuente">  
                                                       <tr>
                                                            <td style="width:20%; text-align:left; cursor:default;">
                                                                *BANCO ID
                                                            </td>
                                                            <td style="width:30%; text-align:left; cursor:default;">
                                                                <asp:TextBox ID="Txt_Clabe" runat="server" Width="98%" TabIndex="0" MaxLength="3"/>
                                                                <cc1:FilteredTextBoxExtender ID="Filt_Clabe" runat="server" 
                                                                    TargetControlID="Txt_Clabe" FilterType="Numbers" ValidChars="0123456789" />                        
                                                            </td> 
                                                            <td style="width:20%; text-align:left; cursor:default;">
                                                                N&uacute;mero Cuenta
                                                            </td>
                                                            <td style="width:30%; text-align:left; cursor:default;">
                                                                <asp:TextBox ID="Txt_Cuenta_Banco" runat="server" Width="98%" TabIndex="0" MaxLength="18"/>
                                                                <cc1:FilteredTextBoxExtender ID="Filt_Cuenta_Banco" runat="server" 
                                                                    TargetControlID="Txt_Cuenta_Banco" FilterType="Numbers" ValidChars="0123456789" />                        
                                                            </td>
                                                       </tr> 
                                                       <tr>
                                                            <td style="width:20%; text-align:left; cursor:default;">
                                                                Sucursal
                                                            </td>
                                                            <td style="width:30%; text-align:left; cursor:default;">
                                                                <asp:TextBox ID="Txt_Sucursal" runat="server" Width="98%" TabIndex="0" MaxLength="400"/>                        
                                                            </td> 
                                                            <td style="width:20%; text-align:left; cursor:default;">
                                                                Correo de Transferencia
                                                            </td>
                                                            <td style="width:30%; text-align:left; cursor:default;">
                                                                <asp:TextBox ID="Txt_Correo_Tranferencia" runat="server" Width="98%" TabIndex="0" />
                                                            </td>
                                                       </tr> 
                                                       <tr>
                                                        <td colspan="2">
                                                             <asp:HiddenField ID="Hdf_Proveedor_Id" runat="server" />
                                                        </td>
                                                       </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>                                          
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>        
                        </asp:UpdatePanel>
                    </div>
                    
                    
                    <table class="estilo_fuente" width="98%" >
                    
                         <%--<tr>
                            <td colspan="3" >
                                <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">--%>
                                <tr >  
                                    <td>
                                        <div id="Div_Grid" runat="server" style="overflow:auto;height:auto;width:100%;vertical-align:top;border-style:outset;border-color:Silver;display:block">
                        
                                            <asp:GridView ID="Grid_Bancos" runat="server" Width="100%" AllowPaging="true" 
                                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" 
                                                PageSize="15" DataKeyNames="PROVEEDOR_ID"
                                                 OnSelectedIndexChanged="Grid_Bancos_SelectedIndexChanged" 
                                                onpageindexchanging="Grid_Bancos_PageIndexChanging" >
                                                <Columns>   
                                                     <asp:ButtonField ButtonType="Image" CommandName="Select"
                                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                        <ItemStyle Width="3%" />
                                                    </asp:ButtonField>  
                                                      <asp:BoundField DataField="PROVEEDOR_ID" HeaderText="PADRON" ItemStyle-Font-Size="Small">
                                                         <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small" />
                                                         <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                       </asp:BoundField>                                          
                                                       <asp:BoundField DataField="COMPANIA" HeaderText="COMPAÑIA" ItemStyle-Font-Size="Small">
                                                         <HeaderStyle HorizontalAlign="Left"   Font-Size="X-Small"/>
                                                         <ItemStyle HorizontalAlign="Left" />
                                                       </asp:BoundField>
                                                       <asp:BoundField DataField="ESTATUS" HeaderText="ESTATUS" ItemStyle-Font-Size="Small">
                                                         <HeaderStyle HorizontalAlign="Center" Width="10%"   Font-Size="X-Small"/>
                                                         <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                       </asp:BoundField>
                                                       <asp:BoundField DataField="TIPO" HeaderText="TIPO" ItemStyle-Font-Size="Small">
                                                         <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small"/>
                                                         <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                       </asp:BoundField>
                                                       <%--<asp:TemplateField HeaderText="Eliminar">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="Btn_Quitar" runat="server" CommandName="Quitar_Registro" 
                                                                    ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                                    CommandArgument='<%# Eval("Banco_ID") %>' 
                                                                    OnClick="Btn_Quitar_Click"/>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                                <ItemStyle HorizontalAlign="Center" Width="15%" />                                                        
                                                       </asp:TemplateField>     --%>
                                                </Columns>                                                    
                                                <SelectedRowStyle CssClass="GridSelected" />
                                                <PagerStyle CssClass="GridHeader" />
                                                <HeaderStyle CssClass="tblHead" />
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                            </asp:GridView> 
                                        </div>                         
                                    </td>  
                                </tr>                              
                        </table>                        
                </div>
            </ContentTemplate>
      </asp:UpdatePanel>
</asp:Content>