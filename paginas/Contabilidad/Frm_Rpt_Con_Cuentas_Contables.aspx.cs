﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
public partial class paginas_Contabilidad_Frm_Rpt_Con_Cuentas_Contables : System.Web.UI.Page
{
    #region (Load/Init)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Refresca la session del usuario lagueado al sistema.
                Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
                //Valida que exista algun usuario logueado al sistema.
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion
    #region (Metodos)
        #region (Métodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION : Inicializa los controles de la forma para prepararla para el
            ///               reporte
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Cuentas_Contables = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
                DataTable Dt_Existencia = new DataTable();
                try
                {
                    Limpia_Controles(); //limpia los campos de la forma
                    Rs_Cuentas_Contables.P_Nivel_Cuenta = "00001";
                    Dt_Existencia = Rs_Cuentas_Contables.Consulta_Cuentas_Por_Nivel();
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Generos, Dt_Existencia, "DESCRIPCION", "CUENTA");
                }
                catch (Exception ex)
                {
                    throw new Exception("Inicializa_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION : Limpia los controles que se encuentran en la forma
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Controles()
            {
                try
                {
                    Cmb_Generos.SelectedIndex=-1;
                    Session["Dt_Datos_Detalles"] = null;
                    Session["Dt_Datos_Detalles2"] = null;
                    Session["Dt_Datos_Detalles3"] = null;
                    Session["Dt_Datos_Detalles4"] = null;
                    Session["Dt_Datos_Detalles5"] = null;
                    Session["Contador"] = null;
                    Session["Contador2"] = null;
                    Session["Contador3"] = null;
                    Session["Contador4"] = null;
                    Session["Contador5"] = null;
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
                }
            }
        #endregion
        #region (Consultas)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Galardo Andrade
            /// FECHA_CREO  : 28-Mayo-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Cuentas_Contables()
            {
                String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
                String Nombre_Archivo = "Cuentas_Contables" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyyHHmmss}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
                DataTable Dt_Cuentas = new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Cuentas_Ordenadas = new DataTable();       //Va a conter los valores de la consulta realizada
                Ds_Rpt_Con_Cuentas_Contables Ds_Cuentas_Contables = new Ds_Rpt_Con_Cuentas_Contables();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Cuentas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
                double tamano;
                double Incrementar=1;
                int Cont_Elementos = 0; //variable para el contador
                DataRow Renglon; //Renglon para el llenado de la tabla
                int Cont_Columnas = 0; //variable para las columnas
                
                try
                {
                    if (Cmb_Generos.SelectedIndex > 0)
                    {
                        Rs_Consulta_Cuentas.P_Cuenta_Inicial = Cmb_Generos.SelectedValue;
                        tamano = Cmb_Generos.SelectedValue.Length;
                        for (int x = 1; x < tamano; x++)
                        {
                            Incrementar = Incrementar * 10;
                        }
                        Rs_Consulta_Cuentas.P_Cuenta_Final = string.Format("{0:000000000000000000}", Convert.ToDouble(Cmb_Generos.SelectedValue) + Incrementar);
                    }
                    Dt_Cuentas = Rs_Consulta_Cuentas.Consultar_Cuentas_Por_Genero(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
                    if (Dt_Cuentas.Rows.Count > 0)
                    {
                        //instanciar renglon de la cabecera
                        Renglon = Ds_Cuentas_Contables.Tables[0].NewRow();

                        Renglon["ID"] = 0;
                        Renglon["Datos_Filtro"] = "";

                        Dt_Cuentas.TableName = "Dt_Cuentas_Contables";
                        //Ds_Cuentas_Contables.Clear();
                        //Ds_Cuentas_Contables.Tables.Clear();
                        //Ds_Cuentas_Contables.Tables.Add(Dt_Cuentas.Copy());

                        //Ciclo para el barrido de la tabla
                        for (Cont_Elementos = 0; Cont_Elementos < Dt_Cuentas.Rows.Count; Cont_Elementos++)
                        {
                            //instanciar renglon y subirlo al dataset
                            Renglon = Ds_Cuentas_Contables.Tables[1].NewRow();

                            Renglon["ID"] = 0;

                            //Agregar las columnas
                            Renglon["Cuenta"] = Dt_Cuentas.Rows[Cont_Elementos][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
                            Renglon["Descripcion"] = Dt_Cuentas.Rows[Cont_Elementos][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                            Renglon["Nivel"] = Dt_Cuentas.Rows[Cont_Elementos]["NIVEL"].ToString().Trim();
                            Ds_Cuentas_Contables.Tables[1].Rows.Add(Renglon);
                        }
                    }
                    ReportDocument Reporte = new ReportDocument();
                    Reporte.Load(Ruta_Archivo + "Rpt_Con_Cuentas_Contables.rpt");
                    Reporte.SetDataSource(Ds_Cuentas_Contables);

                    //ParameterFieldDefinitions Cr_Parametros;
                    //ParameterFieldDefinition Cr_Parametro;
                    //ParameterValues Cr_Valor_Parametro = new ParameterValues();
                    //ParameterDiscreteValue Cr_Valor = new ParameterDiscreteValue();

                    //Cr_Parametros = Reporte.DataDefinition.ParameterFields;

                    //Cr_Parametro = Cr_Parametros["Fecha_Inicial"];
                    //Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                    //Cr_Valor_Parametro.Clear();

                    //Cr_Valor.Value = String.Format("{0:dd/mm/yyyy}",Convert.ToDateTime(Txt_Fecha_Inicio.Text.ToString()));
                    //Cr_Valor_Parametro.Add(Cr_Valor);
                    //Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);

                    //Cr_Parametro = Cr_Parametros["Fecha_Final"];
                    //Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                    //Cr_Valor_Parametro.Clear();

                    DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                    Nombre_Archivo += ".pdf";
                    Ruta_Archivo = @Server.MapPath("../../Reporte/");
                    m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                    ExportOptions Opciones_Exportacion = new ExportOptions();
                    Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);

                    Abrir_Ventana(Nombre_Archivo);
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Mostrar_Reporte
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 24-Mayo-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Mostrar_Reporte()
            {
                DataTable Dt_Cuentas = new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Cuentas_Nivel = new DataTable(); //Obtiene los valores a pasar al reporte
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Cuentas= new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                DataTable Dt_Datos_Detalles = new DataTable();
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                int Contador = 0;
                Double tamano;
                double Incrementar =1;
                Session["Contador"] = Contador;
                try
                {
                    if (Cmb_Generos.SelectedIndex > 0)
                    {
                        Rs_Consulta_Cuentas.P_Cuenta_Inicial = Cmb_Generos.SelectedValue;
                        tamano = Cmb_Generos.SelectedValue.Length;
                        for (int x = 1; x < tamano; x++)
                        {
                            Incrementar = Incrementar * 10;
                        }
                        Rs_Consulta_Cuentas.P_Cuenta_Final = string.Format("{0:000000000000000000}", Convert.ToDouble(Cmb_Generos.SelectedValue)+Incrementar);
                    }
                    Dt_Cuentas = Rs_Consulta_Cuentas.Consultar_Cuentas_Por_Genero(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
                    Dt_Cuentas.DefaultView.RowFilter = "NIVEL_ID='00001'";
                    if (Dt_Cuentas.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Session["Dt_Datos_Detalles"] = Dt_Cuentas.DefaultView.ToTable();
                        Grid_Cuentas_Contables.DataSource = Dt_Cuentas.DefaultView;
                        Grid_Cuentas_Contables.DataBind();
                        Btn_Reporte_Cheques.Visible = true;
                        Tr_Grid_Cuentas_Contables.Style.Add("display", "");
                    }
                    else
                    {
                        Grid_Cuentas_Contables.DataSource = null;
                        Grid_Cuentas_Contables.DataBind();
                        Btn_Reporte_Cheques.Visible = false;
                        Tr_Grid_Cuentas_Contables.Style.Add("display", "none");
                        Lbl_Mensaje_Error.Text = "No se encontraron cuentas <br />";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            #region Grid_RowDatabound
            //'****************************************************************************************
            //'NOMBRE DE LA FUNCION: Grid_Proveedores_RowDataBound
            //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            //'PARAMETROS  : 
            //'CREO        : Sergio Manuel Gallardo
            //'FECHA_CREO  : 16/julio/2011 10:01 am
            //'MODIFICO          :
            //'FECHA_MODIFICO    :
            //'CAUSA_MODIFICACION:
            //'****************************************************************************************
            protected void Grid_Cuentas_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                DataTable Ds_Consulta_Modificada = new DataTable();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Cuentas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
                String Cuenta = "";
                int Contador;
                Double Tamano;
                int Contador2 = 0;
                Session["Contador2"] = Contador2;
                DataTable Dt_Datos_Detalles_2 = new DataTable();
                Dt_Datos_Detalles_2 = ((DataTable)(Session["Dt_Datos_Detalles_2"]));
                Double incrementar=1;
                Image Img = new Image();
                Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
                Label Lbl_facturas = new Label();
                Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos");
                try
                {
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {
                        Contador = (int)(Session["Contador"]);
                        Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                        Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                        + (Lbl_facturas.Text + ("\',\'"
                                        + (Img.ClientID + "\')")))));
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                        Cuenta = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["CUENTA"].ToString());
                            Rs_Cuentas.P_Cuenta_Inicial = Cuenta;
                            Tamano = Cuenta.Length;
                            for (int x = 1; x < Tamano; x++)
                            {
                                incrementar = incrementar * 10;
                            }
                            Rs_Cuentas.P_Cuenta_Final = string.Format("{0:000000000000000000}", Convert.ToDouble(Cuenta) + incrementar);
                        Ds_Consulta = Rs_Cuentas.Consultar_Cuentas_Por_Genero();
                        Ds_Consulta.DefaultView.RowFilter = "NIVEL_ID='00002'";
                        if (Ds_Consulta.DefaultView.ToTable().Rows.Count > 0)
                        {
                            Session["Dt_Datos_Detalles_2"] = Ds_Consulta.DefaultView.ToTable();
                            Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Cuentas_Nivel_2");
                            Gv_Detalles.DataSource = Ds_Consulta.DefaultView;
                            Gv_Detalles.DataBind();
                        }
                        else
                        {
                            Img.Visible = false;
                        }
                        Session["Contador"] = Contador + 1;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            //'****************************************************************************************
            //'NOMBRE DE LA FUNCION: Grid_Cuentas_2_RowDataBound
            //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            //'PARAMETROS  : 
            //'CREO        : Sergio Manuel Gallardo
            //'FECHA_CREO  : 16/julio/2011 10:01 am
            //'MODIFICO          :
            //'FECHA_MODIFICO    :
            //'CAUSA_MODIFICACION:
            //'****************************************************************************************
            protected void Grid_Cuentas_2_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                DataTable Ds_Consulta_Modificada = new DataTable();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Cuentas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
                String Cuenta = "";
                int Contador;
                Double Tamanos;
                Double incrementa = 1;
                Image Img = new Image();
                int Contador3 = 0;
                Session["Contador3"] = Contador3;
                DataTable Dt_Datos_Detalles_3 = new DataTable();
                Dt_Datos_Detalles_3 = ((DataTable)(Session["Dt_Datos_Detalles_3"]));
                Img = (Image)e.Row.FindControl("Img_Btn_Expandir_2");
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio_2");
                Label Lbl_facturas = new Label();
                Lbl_facturas = (Label)e.Row.FindControl("Lbl_Cuentas");
                try
                {
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {
                        Contador = (int)(Session["Contador2"]);
                        Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                        Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                        + (Lbl_facturas.Text + ("\',\'"
                                        + (Img.ClientID + "\')")))));
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles_2"]));
                        Cuenta = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["CUENTA"].ToString());
                        Rs_Cuentas.P_Cuenta_Inicial = Cuenta;
                        Tamanos = Cuenta.Length;
                        for (int x = 2; x < Tamanos; x++)
                        {
                            incrementa = incrementa * 10;
                        }
                        Rs_Cuentas.P_Cuenta_Final = string.Format("{0:000000000000000000}", Convert.ToDouble(Cuenta) + incrementa);
                        Ds_Consulta = Rs_Cuentas.Consultar_Cuentas_Por_Genero();
                        Ds_Consulta.DefaultView.RowFilter = "NIVEL_ID='00003'";
                        if (Ds_Consulta.DefaultView.ToTable().Rows.Count > 0)
                        {
                            Session["Dt_Datos_Detalles_3"] = Ds_Consulta.DefaultView.ToTable();
                            Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Cuentas_Nivel_3");
                            Gv_Detalles.DataSource = Ds_Consulta.DefaultView;
                            Gv_Detalles.DataBind();
                        }
                        else
                        {
                            Img.Visible = false;
                        }
                        Session["Contador2"] = Contador + 1;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            //'****************************************************************************************
            //'NOMBRE DE LA FUNCION: Grid_Cuentas_3_RowDataBound
            //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            //'PARAMETROS  : 
            //'CREO        : Sergio Manuel Gallardo
            //'FECHA_CREO  : 16/julio/2011 10:01 am
            //'MODIFICO          :
            //'FECHA_MODIFICO    :
            //'CAUSA_MODIFICACION:
            //'****************************************************************************************
            protected void Grid_Cuentas_3_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                DataTable Ds_Consulta_Modificada = new DataTable();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Cuentas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
                String Cuenta = "";
                int Contador;
                Double Tamanos;
                Double incrementa = 1;
                int Contador4 = 0;
                Session["Contador4"] = Contador4;
                DataTable Dt_Datos_Detalles_4 = new DataTable();
                Dt_Datos_Detalles_4 = ((DataTable)(Session["Dt_Datos_Detalles_4"]));
                Image Img = new Image();
                Img = (Image)e.Row.FindControl("Img_Btn_Expandir_3");
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio_3");
                Label Lbl_facturas = new Label();
                Lbl_facturas = (Label)e.Row.FindControl("Lbl_Cuentas_2");
                try
                {
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {
                        Contador = (int)(Session["Contador3"]);
                        Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                        Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                        + (Lbl_facturas.Text + ("\',\'"
                                        + (Img.ClientID + "\')")))));
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles_3"]));
                        Cuenta = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["CUENTA"].ToString());
                        Rs_Cuentas.P_Cuenta_Inicial = Cuenta;
                        Tamanos = Cuenta.Length;
                        for (int x = 3; x < Tamanos; x++)
                        {
                            incrementa = incrementa * 10;
                        }
                        Rs_Cuentas.P_Cuenta_Final = string.Format("{0:000000000000000000}", Convert.ToDouble(Cuenta) + incrementa);
                        Ds_Consulta = Rs_Cuentas.Consultar_Cuentas_Por_Genero();
                        Ds_Consulta.DefaultView.RowFilter = "NIVEL_ID='00004'";
                        if (Ds_Consulta.DefaultView.ToTable().Rows.Count > 0)
                        {
                            Session["Dt_Datos_Detalles_4"] = Ds_Consulta.DefaultView.ToTable();
                            Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Cuentas_Nivel_4");
                            Gv_Detalles.DataSource = Ds_Consulta.DefaultView;
                            Gv_Detalles.DataBind();
                        }
                        else
                        {
                            Img.Visible = false;
                        }
                        Session["Contador3"] = Contador + 1;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            //'****************************************************************************************
            //'NOMBRE DE LA FUNCION: Grid_Cuentas_4_RowDataBound
            //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            //'PARAMETROS  : 
            //'CREO        : Sergio Manuel Gallardo
            //'FECHA_CREO  : 16/julio/2011 10:01 am
            //'MODIFICO          :
            //'FECHA_MODIFICO    :
            //'CAUSA_MODIFICACION:
            //'****************************************************************************************
            protected void Grid_Cuentas_4_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                DataTable Ds_Consulta_Modificada = new DataTable();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Cuentas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
                String Cuenta = "";
                int Contador;
                Double Tamanos;
                Double incrementa = 1;
                int Contador5 = 0;
                Session["Contador5"] = Contador5;
                DataTable Dt_Datos_Detalles_5 = new DataTable();
                Dt_Datos_Detalles_5 = ((DataTable)(Session["Dt_Datos_Detalles_5"]));
                Image Img = new Image();
                Img = (Image)e.Row.FindControl("Img_Btn_Expandir_4");
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio_4");
                Label Lbl_facturas = new Label();
                Lbl_facturas = (Label)e.Row.FindControl("Lbl_Cuentas_3");
                try
                {
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {
                        Contador = (int)(Session["Contador4"]);
                        Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                        Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                        + (Lbl_facturas.Text + ("\',\'"
                                        + (Img.ClientID + "\')")))));
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles_4"]));
                        Cuenta = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["CUENTA"].ToString());
                        Rs_Cuentas.P_Cuenta_Inicial = Cuenta;
                        Tamanos = Cuenta.Length;
                        for (int x = 4; x < Tamanos; x++)
                        {
                            incrementa = incrementa * 10;
                        }
                        Rs_Cuentas.P_Cuenta_Final = string.Format("{0:000000000000000000}", Convert.ToDouble(Cuenta) + incrementa);
                        Ds_Consulta = Rs_Cuentas.Consultar_Cuentas_Por_Genero();
                        Ds_Consulta.DefaultView.RowFilter = "NIVEL_ID='00005'";
                        if (Ds_Consulta.DefaultView.ToTable().Rows.Count > 0)
                        {
                            Session["Dt_Datos_Detalles_5"] = Ds_Consulta.DefaultView.ToTable();
                            Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Cuentas_Nivel_5");
                            Gv_Detalles.DataSource = Ds_Consulta.DefaultView;
                            Gv_Detalles.DataBind();
                        }
                        else
                        {
                            Img.Visible = false;
                        }
                        Session["Contador4"] = Contador + 1;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            //'****************************************************************************************
            //'NOMBRE DE LA FUNCION: Grid_Cuentas_5_RowDataBound
            //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            //'PARAMETROS  : 
            //'CREO        : Sergio Manuel Gallardo
            //'FECHA_CREO  : 16/julio/2011 10:01 am
            //'MODIFICO          :
            //'FECHA_MODIFICO    :
            //'CAUSA_MODIFICACION:
            //'****************************************************************************************
            protected void Grid_Cuentas_5_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                DataTable Ds_Consulta_Modificada = new DataTable();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Cuentas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
                String Cuenta = "";
                int Contador;
                Double Tamanos;
                Double incrementa = 1;
                Image Img = new Image();
                Img = (Image)e.Row.FindControl("Img_Btn_Expandir_5");
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio_5");
                Label Lbl_facturas = new Label();
                Lbl_facturas = (Label)e.Row.FindControl("Lbl_Cuentas_4");
                try
                {
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {
                        Contador = (int)(Session["Contador5"]);
                        Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                        Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                        + (Lbl_facturas.Text + ("\',\'"
                                        + (Img.ClientID + "\')")))));
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles_5"]));
                        Cuenta = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["CUENTA"].ToString());
                        Rs_Cuentas.P_Cuenta_Inicial = Cuenta;
                        Tamanos = Cuenta.Length;
                        for (int x = 5; x < Tamanos; x++)
                        {
                            incrementa = incrementa * 10;
                        }
                        Rs_Cuentas.P_Cuenta_Final = string.Format("{0:000000000000000000}", Convert.ToDouble(Cuenta) + incrementa);
                        Ds_Consulta = Rs_Cuentas.Consultar_Cuentas_Por_Genero();
                        Ds_Consulta.DefaultView.RowFilter = "NIVEL_ID='00006'";
                        if (Ds_Consulta.DefaultView.ToTable().Rows.Count > 0)
                        {
                            Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Cuentas_Nivel_6");
                            Gv_Detalles.DataSource = Ds_Consulta.DefaultView;
                            Gv_Detalles.DataBind();
                        }
                        else
                        {
                            Img.Visible = false;
                        }
                        Session["Contador5"] = Contador + 1;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            #endregion
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
            ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
            ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
            ///                             para mostrar los datos al usuario
            ///CREO       : Yazmin A Delgado Gómez
            ///FECHA_CREO : 12-Octubre-2011
            ///MODIFICO          :
            ///FECHA_MODIFICO    :
            ///CAUSA_MODIFICACIÓN:
            ///******************************************************************************
            private void Abrir_Ventana(String Nombre_Archivo)
            {
                String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
                try
                {
                    Pagina = Pagina + Nombre_Archivo;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
                }
            }
        #endregion
    #endregion
    protected void Btn_Reporte_Cuentas_Contables_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Consulta_Cuentas_Contables(); //Consulta los cheques emitidos por fecha
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Btn_Nuevo_Click
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    /// PARÁMETROS:
    /// USUARIO CREO:       Sergio Manuel Gallardo Andrade
    /// FECHA CREO:         24/Mayo/2012
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
          Mostrar_Reporte(); //Consulta el libro de mayor de la cuenta seleccionada
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }
}
