﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Autoriza_Solicitud_Deudores.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Autoriza_Ejercido.Negocio;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Deudores.Negocios;

public partial class paginas_Contabilidad_Frm_Ope_Con_Recepcion_Gasto_Comprobar : System.Web.UI.Page
{
    #region PAGE LOAD
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    Recepcion_Deudores_Inicio();
                }
            }
            catch(Exception Ex) 
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error["+Ex.Message+"]");
            }
        }
    #endregion

    #region METODOS
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Recepcion_Deudores_Inicio
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo    
        ///FECHA_CREO           : 06/Agosto/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Recepcion_Deudores_Inicio() 
        {
            try
            {
                Limpiar_Controles();
                Llenar_Grid_Solicitudes();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
            }
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
        ///DESCRIPCIÓN          : Metodo para limpiar los controles de la página
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Limpiar_Controles()
        {
            try
            {
                Txt_No_Solicitud_Autorizar.Value = "";
                Txt_Rechazo.Value = "";
                Txt_Comentario.Text = "";
                Txt_Comentario.Text = "";
                Grid_Solicitud_Deudores.DataSource = null;
                Grid_Solicitud_Deudores.DataBind();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al limpiar los controles de la página Error[" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Llenar_Grid_Solicitudes
        /// DESCRIPCION : Llena el grid Solicitudes de pago
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 06/Agosto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Llenar_Grid_Solicitudes()
        {
            try
            {
                Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Autoriza_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
                //Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Autoriza_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
                DataTable Dt_Resultado = new DataTable();
                DataTable Dt_Modificado = new DataTable();
                Rs_Autoriza_Solicitud.P_Estatus = "AUTORIZADO";
                Dt_Resultado = Rs_Autoriza_Solicitud.Consulta_Solicitudes_Autorizadas();
                DataTable Dt_Datos_Detalles = new DataTable();
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                int Contador = 0;
                Session["Contador"] = Contador;
                Grid_Solicitud_Deudores.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
                Grid_Solicitud_Deudores.DataBind();    // Se ligan los datos.;

                if (Dt_Resultado.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Dt_Resultado.Rows)
                    {
                        if (Dt_Modificado.Rows.Count <= 0)
                        {
                            Dt_Modificado.Columns.Add("DEUDOR_ID", typeof(System.String));
                            Dt_Modificado.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                            Dt_Modificado.Columns.Add("NO_DEUDA", typeof(System.String));
                            Dt_Modificado.Columns.Add("NOMBRE", typeof(System.String));
                            Dt_Modificado.Columns.Add("TIPO_MOVIMIENTO", typeof(System.String));
                            Dt_Modificado.Columns.Add("CONCEPTO", typeof(System.String));
                            Dt_Modificado.Columns.Add("IMPORTE", typeof(System.Double));
                            Dt_Modificado.Columns.Add("ESTATUS", typeof(System.String));
                        }
                        DataRow row = Dt_Modificado.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        row["NO_DEUDA"] = Fila["NO_DEUDA"].ToString().Trim();
                        row["NOMBRE"] = Fila["NOMBRE"].ToString().Trim();
                        row["CONCEPTO"] = Fila["CONCEPTO"].ToString().Trim();
                        row["TIPO_MOVIMIENTO"] = Fila["TIPO_MOVIMIENTO"].ToString().Trim();
                        row["Concepto"] = Fila["Concepto"].ToString().Trim();
                        row["TIPO_DEUDOR"] = Fila["TIPO_DEUDOR"].ToString().Trim();
                        row["ESTATUS"] = Fila["ESTATUS"].ToString().Trim();
                        row["IMPORTE"] = Fila["IMPORTE"].ToString().Trim();
                        row["DEUDOR_ID"] = Fila["DEUDOR_ID"].ToString().Trim();
                        Dt_Modificado.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Modificado.AcceptChanges();
                    }
                    Session["Dt_Datos_Detalles"] = Dt_Modificado;

                   // Grid_Solicitud_Deudores.Columns[0].Visible = true;
                    Grid_Solicitud_Deudores.Columns[8].Visible = true;
                    Grid_Solicitud_Deudores.Columns[9].Visible = true;
                    Grid_Solicitud_Deudores.Columns[10].Visible = true;
                    Grid_Solicitud_Deudores.DataSource = Dt_Modificado;   // Se iguala el DataTable con el Grid
                    Grid_Solicitud_Deudores.DataBind();    // Se ligan los datos.;
                   // Grid_Solicitud_Deudores.Columns[0].Visible = false;
                    Grid_Solicitud_Deudores.Columns[8].Visible = false;
                    Grid_Solicitud_Deudores.Columns[9].Visible = false;
                    Grid_Solicitud_Deudores.Columns[10].Visible = false;
                }
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitudes de Pago", "alert('En este momento no se tienen pagos pendientes por autorizar');", true);
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago
        /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
        ///               proporcionados por el usuario
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Cancela_Solicitud(String No_Solicitud, String Comentario, String Empleado_ID, String Nombre_Empleado)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio(); //Variable de conexión hacia la capa de Negocios
            //Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
            DataTable Dt_Detalles = new DataTable();
            DataTable Dt_Datos_Polizas = new DataTable();
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            try
            {    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Deuda = No_Solicitud;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "CANCELADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanza = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Asigno_Banco = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Autorizacion_de_Solicitud(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados              
                //Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);
            }
            catch (Exception ex)
            {
                throw new Exception("Modificar_Solicitud " + ex.Message.ToString(), ex);
            }
        }
    #endregion

    #region EVENTOS
        protected void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Recepcion_Deudores_Inicio();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
            }
        }
        protected void Btn_Comentar_Click(object sender, EventArgs e)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Modificar_Ope_Con_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
            try
            {
                if (Txt_Comentario.Text != "")
                {
                    Cancela_Solicitud(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString());
                    Recepcion_Deudores_Inicio();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);   
                }
                else
                {
                    Recepcion_Deudores_Inicio();
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";

                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
        ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
        ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
        ///                             para mostrar los datos al usuario
        ///CREO       : Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO  : 21-Febrero-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        private void Abrir_Ventana(String Nombre_Archivo)
        {
            String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
            try
            {
                Pagina = Pagina + Nombre_Archivo;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception ex)
            {
                throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
            }
        }
        protected void Btn_Buscar_No_Solicitud_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Consultar_Solicitud =new  Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
           // Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Resultado = new DataTable();
            DataTable Dt_Modificado = new DataTable();
            DataTable Dt_Datos_Detalles = new DataTable();
            Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
            int Contador = 0;
            Session["Contador"] = Contador;
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
                {
                    Rs_Consultar_Solicitud.P_No_Deuda = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
                    Rs_Consultar_Solicitud.P_Estatus = "AUTORIZADO";
                    Dt_Resultado = Rs_Consultar_Solicitud.Consulta_Solicitudes_Autorizadas();
                }
                else
                {
                    Rs_Consultar_Solicitud.P_Estatus = "AUTORIZADO";
                    Dt_Resultado = Rs_Consultar_Solicitud.Consulta_Solicitudes_Autorizadas();
                }
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la busqueda <br>";
                    Txt_Busqueda_No_Solicitud.Focus();
                    Grid_Solicitud_Deudores.DataSource = null;
                    Grid_Solicitud_Deudores.DataBind();
                }
                else
                {
                    foreach (DataRow Fila in Dt_Resultado.Rows)
                    {
                        if (Dt_Modificado.Rows.Count <= 0)
                        {
                            Dt_Modificado.Columns.Add("DEUDOR_ID", typeof(System.String));
                            Dt_Modificado.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                            Dt_Modificado.Columns.Add("NO_DEUDA", typeof(System.String));
                            Dt_Modificado.Columns.Add("NOMBRE", typeof(System.String));
                            Dt_Modificado.Columns.Add("TIPO_MOVIMIENTO", typeof(System.String));
                            Dt_Modificado.Columns.Add("CONCEPTO", typeof(System.String));
                            Dt_Modificado.Columns.Add("IMPORTE", typeof(System.Double));
                            Dt_Modificado.Columns.Add("ESTATUS", typeof(System.String));
                        }
                        DataRow row = Dt_Modificado.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        row["NO_DEUDA"] = Fila["NO_DEUDA"].ToString().Trim();
                        row["NOMBRE"] = Fila["NOMBRE"].ToString().Trim();
                        row["CONCEPTO"] = Fila["CONCEPTO"].ToString().Trim();
                        row["TIPO_MOVIMIENTO"] = Fila["TIPO_MOVIMIENTO"].ToString().Trim();
                        row["Concepto"] = Fila["Concepto"].ToString().Trim();
                        row["TIPO_DEUDOR"] = Fila["TIPO_DEUDOR"].ToString().Trim();
                        row["ESTATUS"] = Fila["ESTATUS"].ToString().Trim();
                        row["IMPORTE"] = Fila["IMPORTE"].ToString().Trim();
                        row["DEUDOR_ID"] = Fila["DEUDOR_ID"].ToString().Trim();
                        Dt_Modificado.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Modificado.AcceptChanges();
                    }
                    Session["Dt_Datos_Detalles"] = Dt_Modificado;
                    //Grid_Solicitud_Deudores.Columns[0].Visible = true;
                    Grid_Solicitud_Deudores.Columns[8].Visible = true;
                    Grid_Solicitud_Deudores.Columns[9].Visible = true;
                    Grid_Solicitud_Deudores.Columns[10].Visible = true;
                    Grid_Solicitud_Deudores.DataSource = Dt_Modificado;   // Se iguala el DataTable con el Grid
                    Grid_Solicitud_Deudores.DataBind();    // Se ligan los datos.;
                    //Grid_Solicitud_Deudores.Columns[0].Visible = false;
                    Grid_Solicitud_Deudores.Columns[8].Visible = false;
                    Grid_Solicitud_Deudores.Columns[9].Visible = false;
                    Grid_Solicitud_Deudores.Columns[10].Visible = false;
                    Txt_Busqueda_No_Solicitud.Text = "";
                }

            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        #region GENERALES
            ///*********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
            ///DESCRIPCIÓN          : Evento del boton de salir
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Diciembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
                protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    if (Btn_Salir.ToolTip == "Inicio")
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                    else
                    {
                        Recepcion_Deudores_Inicio();
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
        #endregion

        #region"Grid"
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Grid_Solicitud_RowDataBound
            /// DESCRIPCION : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 09/enero/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Grid_Solicitud_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DropDownList Cmb = new DropDownList();
                DropDownList Cmb_Tipo_Pago = new DropDownList();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                DataTable Ds_Modificada = new DataTable();
                DataTable Dt_Existencia = new DataTable();
                DataTable Dt_Consulta_Cuenta = new DataTable();
                Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
                Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
                Cls_Cat_Con_Parametros_Negocio Rs_Parametros_Montos= new Cls_Cat_Con_Parametros_Negocio();
                DataTable Dt_Parametros= new DataTable();
                String No_Solicitud = "";
                Decimal Monto = 0;
                int Contador;
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
                Label Lbl_Solicitud = new Label();
                Lbl_Solicitud = (Label)e.Row.FindControl("Lbl_Solicitud");
                try
                {
                    CheckBox chek = (CheckBox)e.Row.FindControl("Chk_Autorizado");
                    CheckBox chek2 = (CheckBox)e.Row.FindControl("Chk_Rechazar");
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Contador = (int)(Session["Contador"]);
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                        No_Solicitud = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["NO_DEUDA"].ToString());
                        Monto = Convert.ToDecimal(Dt_Datos_Detalles.Rows[Contador]["IMPORTE"].ToString());
                        Rs_Solicitudes.P_No_Deuda  = No_Solicitud;
                        Ds_Consulta = Rs_Solicitudes.Consulta_Solicitudes_Autorizadas();
                        foreach (DataRow Fila in Ds_Consulta.Rows)
                        {
                            if (Ds_Modificada.Rows.Count <= 0)
                            {
                                Ds_Modificada.Columns.Add("DEUDOR_ID", typeof(System.String));
                                Ds_Modificada.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                                Ds_Modificada.Columns.Add("NO_DEUDA", typeof(System.String));
                                Ds_Modificada.Columns.Add("NOMBRE", typeof(System.String));
                                Ds_Modificada.Columns.Add("TIPO_MOVIMIENTO", typeof(System.String));
                                Ds_Modificada.Columns.Add("CONCEPTO", typeof(System.String));
                                Ds_Modificada.Columns.Add("IMPORTE", typeof(System.Double));
                                Ds_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                            }
                            DataRow row = Ds_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            //Asigna los valores al nuevo registro creado a la tabla
                            row["NO_DEUDA"] = Fila["NO_DEUDA"].ToString().Trim();
                            row["DEUDOR_ID"] = Fila["DEUDOR_ID"].ToString().Trim();
                            row["TIPO_DEUDOR"] = Fila["TIPO_DEUDOR"].ToString().Trim();
                            row["NOMBRE"] = Fila["NOMBRE"].ToString().Trim();
                            row["CONCEPTO"] = Fila["CONCEPTO"].ToString().Trim();
                            row["TIPO_MOVIMIENTO"] = Fila["TIPO_MOVIMIENTO"].ToString().Trim();
                            row["ESTATUS"] = Fila["ESTATUS"].ToString().Trim();
                            row["IMPORTE"] = Fila["IMPORTE"].ToString().Trim();
                            Ds_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Ds_Modificada.AcceptChanges();
                        }
                        Gv_Detalles = (GridView)e.Row.Cells[11].FindControl("Grid_Datos_Solicitud");
                        Gv_Detalles.DataSource = Ds_Modificada;
                        Gv_Detalles.DataBind();
                        Session["Contador"] = Contador + 1;
                        if (e.Row.Cells[8].Text != "AUTORIZADO")
                        {
                            chek.Enabled = false;
                            chek2.Enabled = false;
                        }
                        else
                        {
                            chek.Enabled = true;
                            chek2.Enabled = true;
                        }
                        //llenar el combon de bancos
                        //DataTable Dt_Bancos = Bancos_Negocio.Consulta_Bancos();
                        Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
                        Cmb = (DropDownList)e.Row.Cells[5].FindControl("Cmb_Banco_Transferencia");
                        Cmb.DataSource = Dt_Existencia;
                        Cmb.DataTextField = "NOMBRE";
                        Cmb.DataValueField = "NOMBRE";
                        Cmb.DataBind();
                        Cmb.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                        //consultar los parametros del monto de caja chica
                        Dt_Parametros= new DataTable();
                        Dt_Parametros=Rs_Parametros_Montos.Consulta_Datos_Parametros_2();
                        if (Dt_Parametros.Rows.Count > 0)
                        {
                            Cmb_Tipo_Pago = (DropDownList)e.Row.Cells[3].FindControl("Cmb_Tipo_Pago");
                            if (Monto > Convert.ToDecimal(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Monto_Caja_Chica].ToString()))
                            {
                                Cmb_Tipo_Pago.SelectedValue = "2";
                                Cmb_Tipo_Pago = (DropDownList)e.Row.Cells[4].FindControl("Cmb_Banco_Transferencia");
                                Cmb_Tipo_Pago.Enabled = true;
                                Cmb_Tipo_Pago = (DropDownList)e.Row.Cells[5].FindControl("Cmb_Cuenta");
                                Cmb_Tipo_Pago.Enabled = true;
                            }
                            else
                            {
                                Cmb_Tipo_Pago.SelectedValue = "1";
                                Cmb_Tipo_Pago = (DropDownList)e.Row.Cells[4].FindControl("Cmb_Banco_Transferencia");
                                Cmb_Tipo_Pago.Enabled = false;
                                Cmb_Tipo_Pago = (DropDownList)e.Row.Cells[5].FindControl("Cmb_Cuenta");
                                Cmb_Tipo_Pago.Enabled = false;
                            }
                        }

                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cmb_Banco_Transferencia_OnSelectedIndexChanged
            ///DESCRIPCIÓN          : Metodo para llenar el combo de cuentas por registro dependiendo la fila que se selecciono
            ///PROPIEDADES          :
            ///CREO                 : Sergio Manuel Gallardo Andrade
            ///FECHA_CREO           : 12/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Cmb_Banco_Transferencia_OnSelectedIndexChanged(object sender, EventArgs e)
            {

                Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
                DataTable Dt_Existencia = new DataTable();
                try
                {
                    //locate the row in which the dropdown value has been changed
                    GridViewRow gr = (GridViewRow)((DataControlFieldCell)((DropDownList)sender).Parent).Parent;
                    //find the control in that
                    LinkButton ss = (LinkButton)gr.FindControl("Btn_Solicitud");
                    DropDownList d1 = (DropDownList)gr.FindControl("Cmb_Banco_Transferencia");
                    string selectedvalue = d1.SelectedValue;
                    DropDownList d2 = (DropDownList)gr.FindControl("Cmb_Cuenta");
                    Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = d1.SelectedItem.Text.ToString();
                    Dt_Existencia = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico(d2, Dt_Existencia, "CUENTA", "BANCO_ID");
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cmb_Tipo_Pago_OnSelectedIndexChanged
            ///DESCRIPCIÓN          : Metodo para llenar el combo de cuentas por registro dependiendo la fila que se selecciono
            ///PROPIEDADES          :
            ///CREO                 : Sergio Manuel Gallardo Andrade
            ///FECHA_CREO           : 23/Agosto/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Cmb_Tipo_Pago_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
                DataTable Dt_Existencia = new DataTable();
                try
                {
                    //locate the row in which the dropdown value has been changed
                    GridViewRow gr = (GridViewRow)((DataControlFieldCell)((DropDownList)sender).Parent).Parent;
                    //find the control in that
                    LinkButton ss = (LinkButton)gr.FindControl("Btn_Solicitud");
                    DropDownList Cmb_Tipo_Pago = (DropDownList)gr.FindControl("Cmb_Tipo_Pago");
                    DropDownList Cmb_Cuenta = (DropDownList)gr.FindControl("Cmb_Cuenta");
                    DropDownList Cmb_Banco_Transferencia = (DropDownList)gr.FindControl("Cmb_Banco_Transferencia");
                    if (Cmb_Tipo_Pago.SelectedValue == "1")
                    {
                        Cmb_Banco_Transferencia.SelectedIndex = -1;
                        Cmb_Cuenta.Items.Clear();
                        Cmb_Cuenta.Enabled = false;
                        Cmb_Banco_Transferencia.Enabled = false;
                    }
                    else
                    {
                        Cmb_Cuenta.Enabled = true;
                        Cmb_Banco_Transferencia.Enabled = true;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Autorizar_Solicitud
            ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado 
            ///PROPIEDADES          :
            ///CREO                 : Sergio Manuel Gallardo Andrade
            ///FECHA_CREO           : 02/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Autorizar_Solicitud(object sender, EventArgs e)
            {
                Int32 Indice = 0;
                String Estatus;
                String No_Solicitud;
                String Forma_Pago;
                String Banco = "";
                String Cuenta_Banco = "";
                Boolean Autorizado;
                String Vale = "";
                DataTable Dt_solicitud = new DataTable();
                DataTable Dt_Consulta_Estatus = new DataTable();
                Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
                //Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
                try
                {
                    foreach (GridViewRow Renglon_Grid in Grid_Solicitud_Deudores.Rows)
                    {
                        Indice++;
                        Grid_Solicitud_Deudores.SelectedIndex = Indice;
                        Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                        if (Autorizado)
                        {
                            No_Solicitud = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).CssClass;
                            Forma_Pago = ((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Tipo_Pago")).SelectedItem.Text.ToString();
                            if (Forma_Pago != "Vale")
                            {
                                if (!String.IsNullOrEmpty(((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Banco_Transferencia")).Text))
                                {
                                    Banco = ((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Banco_Transferencia")).Text;
                                }
                                if (!String.IsNullOrEmpty(((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Cuenta")).SelectedValue))
                                {
                                    Cuenta_Banco = ((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Cuenta")).SelectedValue;
                                }
                                if (Banco != "" && Cuenta_Banco != "")
                                {
                                    Rs_Solicitud.P_No_Deuda = No_Solicitud;
                                    Dt_solicitud = Rs_Solicitud.Consulta_Solicitudes_Autorizadas();
                                    if (Dt_solicitud.Rows.Count > 0)
                                    {
                                        Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                                        if (Estatus == "AUTORIZADO")
                                        {
                                            Rs_Solicitud.P_No_Deuda = No_Solicitud;
                                            Rs_Solicitud.P_Estatus = "PORPAGAR";
                                            Rs_Solicitud.P_Forma_Pago = Forma_Pago;
                                            Rs_Solicitud.P_Cuenta_Banco_Pago = Cuenta_Banco;
                                            Rs_Solicitud.P_Comentario_Finanza = "";
                                            Rs_Solicitud.P_Usuario_Asigno_Banco = Cls_Sessiones.Nombre_Empleado.ToString();
                                            Rs_Solicitud.Autorizacion_de_Solicitud();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Vale=Alta_Poliza_Caja_Chica(No_Solicitud);
                                //Imprimir la solcitud de pago
                                Imprimir(Vale);
                            }
                        }
                    }
                    ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "window.setTimeout('window.location.reload(true);',1);", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Alta_Poliza_Caja_Chica
            /// DESCRIPCION : Creacion De la poliza de Caja Chica
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 26/Agosto/2013
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private String Alta_Poliza_Caja_Chica(String No_Solicitud)
            {
                Cls_Ope_Con_Deudores_Negocio Rs_Deudor= new Cls_Ope_Con_Deudores_Negocio();
                Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
                Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
                Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
                DataTable Dt_Consulta = new DataTable();
                DataTable Dt_Consulta_Deudor = new DataTable();
                String Vale = "";
                String Resultado = "";
                String Bancos_Otros = "";
                String Cuentas_Por_Pagar = "";
                DataTable Dt_Parametros = new DataTable();
                DataTable Dt_Banco_Cuenta_Contable = new DataTable();
                DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
                try
                {
                    ///Se consulta el monto de la deuda
                    Rs_Deudor.P_Cmmd=Cmmd;
                    Rs_Deudor.P_No_Deuda=No_Solicitud;
                    Dt_Consulta_Deudor=Rs_Deudor.Consulta_Deudores();
                    //consultar LOS PARAMETROS
                    Rs_Parametros.P_Cmmd = Cmmd;
                    Dt_Parametros = Rs_Parametros.Consulta_Datos_Parametros();
                    if (Dt_Parametros.Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Banco_Otros_Deudor].ToString()))
                        {
                            Bancos_Otros = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Banco_Otros_Deudor].ToString();
                        }
                        if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Por_Pagar_Deudor].ToString()))
                        {
                            Cuentas_Por_Pagar = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Por_Pagar_Deudor].ToString();
                        }
                    }
                    //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));


                    //Agrega el abono del registro de la póliza
                    DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuentas_Por_Pagar;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Dt_Consulta_Deudor.Rows[0][OPE_CON_DEUDORES.Campo_Importe].ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                    row["BENEFICIARIO_ID"] = Dt_Consulta_Deudor.Rows[0][OPE_CON_DEUDORES.Campo_Deudor_ID].ToString();
                    row["TIPO_BENEFICIARIO"] = "EMPLEADO";
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();

                    //Agrega el cargo del registro de la póliza
                    row = Dt_Partidas_Polizas.NewRow();
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Bancos_Otros;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Dt_Consulta_Deudor.Rows[0][OPE_CON_DEUDORES.Campo_Importe].ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                    row["BENEFICIARIO_ID"] = Dt_Consulta_Deudor.Rows[0][OPE_CON_DEUDORES.Campo_Deudor_ID].ToString();
                    row["TIPO_BENEFICIARIO"] = "EMPLEADO";
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();

                    Rs_Solicitudes.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                    Rs_Solicitudes.P_No_Deuda = No_Solicitud;
                    Rs_Solicitudes.P_Comentario = "";
                    Rs_Solicitudes.P_Importe = Dt_Consulta_Deudor.Rows[0][OPE_CON_DEUDORES.Campo_Importe].ToString();
                    Rs_Solicitudes.P_No_Partidas = "2";
                    Rs_Solicitudes.P_Estatus = "GENERADO";
                    Rs_Solicitudes.P_Fecha_Pago = String.Format("{0:dd/MM/yy}", DateTime.Now).ToString();
                    Rs_Solicitudes.P_Cmmd = Cmmd;
                    Rs_Solicitudes.P_Referencia = "No_Deuda"+Dt_Consulta_Deudor.Rows[0][OPE_CON_DEUDORES.Campo_No_Deuda].ToString();
                    Rs_Solicitudes.P_Usuario_Autorizo = Cls_Sessiones.Nombre_Empleado;
                    Vale = Rs_Solicitudes.Alta_Vale();
                    Resultado = Vale.Substring(11, 2);
                    if (Resultado == "SI")
                    {
                        Trans.Commit();
                    }
                    else
                    {
                        Trans.Rollback();
                        Lbl_Mensaje_Error.Text = "Error:";
                        Lbl_Mensaje_Error.Text = "No hay suficiencia  para realizar el vale ";
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Visible = true;
                    }
                    return Vale;
                }
                catch (SqlException Ex)
                {
                        Trans.Rollback();
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                        Trans.Rollback();
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                        Trans.Rollback();
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                        Cn.Close();
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Imprimir
            ///DESCRIPCIÓN: Imprime la solicitud
            ///PROPIEDADES:     
            ///CREO: Sergio Manuel Gallardo
            ///FECHA_CREO: 06/Enero/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Imprimir(String Vale)
            {
                DataSet Ds_Reporte = null;
                DataTable Dt_Pagos = null;
                try
                {
                    Cls_Ope_Con_Deudores_Negocio Rs_Datos_Vale = new Cls_Ope_Con_Deudores_Negocio();
                    Ds_Reporte = new DataSet();
                    Rs_Datos_Vale.P_Vale = Vale.Substring(0,10);
                    Dt_Pagos = Rs_Datos_Vale.Consultar_Datos_Vale();
                    if (Dt_Pagos.Rows.Count > 0)
                    {
                        Dt_Pagos.TableName = "Dt_Vales";
                        Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                        //Se llama al método que ejecuta la operación de generar el reporte.
                        Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Vale.rpt", "VALE_"+Vale + "", ".pdf");
                    }
                }
                //}
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Text = "Error:";
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                    Img_Error.Visible = true;
                }

            }
            /// *************************************************************************************
            /// NOMBRE:             Generar_Reporte
            /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
            ///              
            /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
            ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
            ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
            ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
            /// USUARIO CREO:       Juan Alberto Hernández Negrete.
            /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
            /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
            /// FECHA MODIFICO:     16/Mayo/2011
            /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
            ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
            /// *************************************************************************************
            public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
            {
                ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
                String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
                try
                {
                    Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
                    Reporte.Load(Ruta);

                    if (Ds_Reporte_Crystal is DataSet)
                    {
                        if (Ds_Reporte_Crystal.Tables.Count > 0)
                        {
                            Reporte.SetDataSource(Ds_Reporte_Crystal);
                            Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                            //Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                            Mostrar_Archivo(Nombre_Reporte_Generar + Formato);
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
                }
            }

            /// *************************************************************************************
            /// NOMBRE:             Exportar_Reporte_PDF
            /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
            ///                     especificada.
            /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
            ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
            /// USUARIO CREO:       Juan Alberto Hernández Negrete.
            /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA MODIFICACIÓN:
            /// *************************************************************************************
            public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
            {
                ExportOptions Opciones_Exportacion = new ExportOptions();
                DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

                try
                {
                    if (Reporte is ReportDocument)
                    {
                        Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                        Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                        Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                        Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                        Reporte.Export(Opciones_Exportacion);
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
                }
            }

            private void Mostrar_Archivo(String URL)
            {
                string Pagina = "../../Reporte/";
                try
                {
                    Pagina = Pagina + URL;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "B1",
                        "window.open('" + Pagina + "', 'Solicitud Pago','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "B", "window.location='Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL + "';", true);
                    //Response.Redirect("Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL, false);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al mostrar el archivo de dispersion generado generado. Error: [" + Ex.Message + "]");
                }
            }

        #endregion

    #endregion

    
}
