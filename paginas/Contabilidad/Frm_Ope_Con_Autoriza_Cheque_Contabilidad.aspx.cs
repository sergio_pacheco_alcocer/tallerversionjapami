﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Ope_Con_Autorizar_Cheques.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Autoriza_Cheque_Contabilidad : System.Web.UI.Page
{
     #region PAGE LOAD
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Page_Load
    ///DESCRIPCIÓN          : Inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 26/Diciembre/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                //Establecer la fechas inicial y final
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);
                _DateTime = _DateTime.AddMonths(-1);
                Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                //
                Autorizacion_Cheques_Inicio();
                Acciones();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
        }
    }
    #endregion

    #region METODOS

    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Autorizacion_Cheques_Inicio
    ///DESCRIPCIÓN          : Inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Diciembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Autorizacion_Cheques_Inicio()
    {
        try
        {
            Limpiar_Controles();
            Llenar_Combo_Tipos_Beneficiarios();
            Llenar_Grid_Cheques_Pago();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de autprización de cheques Error[" + Ex.Message + "]");
        }
    }

    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
    ///DESCRIPCIÓN          : Metodo para limpiar los controles de la página
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Diciembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            Hf_No_Cheque_Autorizar.Value = "";
            Hf_Rechazo.Value = "";
            Txt_Comentario.Text = "";
            Grid_Solicitud_Pagos.DataSource = null;
            Grid_Solicitud_Pagos.DataBind();
            Txt_Beneficiario.Text = "";
            Txt_Folio_Busqueda.Text = "";
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al limpiar los controles de la página Error[" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Grid_Cheques_Pago
    /// DESCRIPCION : Llena el grid Solicitudes de pago
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llenar_Grid_Cheques_Pago()
    {
        try
        {
            Cls_Ope_Con_Estatus_Cheques_Negocio Autorizar_Cheque_Negocio = new Cls_Ope_Con_Estatus_Cheques_Negocio();
            DataTable Dt_Cheques = new DataTable();
            Autorizar_Cheque_Negocio.P_Estatus_Entrada = "ENTRADA_CONTABILIDAD"; //Filtro para que  busque los cheques que estan en estatus caja
            //Verificar si tiene algo el filtro de No_Cheque
            if (!String.IsNullOrEmpty(Txt_Folio_Busqueda.Text))
            {
                Autorizar_Cheque_Negocio.P_Folio_Cheque = Txt_Folio_Busqueda.Text.ToString().Trim();
            }
            //Verificar si tiene datos el filtro Beneficario
            if (!String.IsNullOrEmpty(Txt_Beneficiario.Text))
            {
                Autorizar_Cheque_Negocio.P_Beneficiario = Txt_Beneficiario.Text.ToString().Trim();
            }
            //Obtener los fitlros de fecha
            Autorizar_Cheque_Negocio.P_Fecha_Emision_Inicio = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Inicial.Text);
            Autorizar_Cheque_Negocio.P_Fecha_Emision_Fin= String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Final.Text);
            //Verificar si se selecciono una opcion del filtro Tipo_Beneficiario
            if (Cmb_Tipo_Beneficiario.SelectedIndex > 0)
            {
                Autorizar_Cheque_Negocio.P_Tipo_Beneficiario = Cmb_Tipo_Beneficiario.SelectedValue;
            }
            Dt_Cheques = Autorizar_Cheque_Negocio.Consultar_Lista_De_Cheques();
            Grid_Solicitud_Pagos.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
            Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;
            if (Dt_Cheques.Rows.Count > 0)
            {
                Grid_Solicitud_Pagos.Columns[0].Visible = true;
                Grid_Solicitud_Pagos.DataSource = Dt_Cheques;   // Se iguala el DataTable con el Grid
                Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;
                Grid_Solicitud_Pagos.Columns[0].Visible = false;
            }
            //else
            //{
            //   // ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitudes de Pago", "alert('En este momento no se tienen pagos pendientes por autorizar');", true);
            //}
        }
        catch (Exception ex)
        {
            throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
        }
    }

    // ****************************************************************************************
    //'NOMBRE DE LA FUNCION:Accion
    //'DESCRIPCION : realiza la modificacion la preautorización del pago o el rechazo de la solicitud de pago
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 07/Noviembre/2011 12:12 pm
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Acciones()
    {
        String Accion = String.Empty;
        String No_Cheque = String.Empty;
        String Comentario = String.Empty;
        DataTable Dt_Consulta_Estatus = new DataTable();
        if (Request.QueryString["Accion"] != null)
        {
            Accion = HttpUtility.UrlDecode(Request.QueryString["Accion"].ToString());
            if (Request.QueryString["id"] != null)
            {
                No_Cheque = HttpUtility.UrlDecode(Request.QueryString["id"].ToString());
            }
            if (Request.QueryString["x"] != null)
            {
                Comentario = HttpUtility.UrlDecode(Request.QueryString["x"].ToString());
            }
            //Response.Clear()
            switch (Accion)
            {
                case "Autorizar_Cheques":
                    Cls_Ope_Con_Estatus_Cheques_Negocio Cheques_Negocio = new Cls_Ope_Con_Estatus_Cheques_Negocio();
                    Cheques_Negocio.P_No_Cheque_ID = No_Cheque;
                    Cheques_Negocio.P_Estatus_Salida = "CONTABILIDAD";
                    String Mensaje = Cheques_Negocio.Autorizar_Cheques();
                    
                    break;
                case "Rechazar_Solicitud":
                    Cancelar_Cheque_Pago(No_Cheque, Comentario);
                    break;
            }
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cancelar_Cheque_Pago
    /// DESCRIPCION : Modifica los datos del cheque para actualizarlo en estatus cancelado
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 26/Diciembre/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cancelar_Cheque_Pago(String No_Cheque, String Comentario)
    {
        Cls_Ope_Con_Estatus_Cheques_Negocio Autorizar_Cheque_Negocio = new Cls_Ope_Con_Estatus_Cheques_Negocio();
        try
        {
            Autorizar_Cheque_Negocio.P_No_Cheque_ID = No_Cheque;
            Autorizar_Cheque_Negocio.P_Comentarios = Comentario;
            Autorizar_Cheque_Negocio.P_Estatus_Salida = "CANCELADO";

            String Mensaje = Autorizar_Cheque_Negocio.Rechazar_Cheque();
            
        }
        catch (Exception ex)
        {
            throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Buscar_Beneficiario
    ///DESCRIPCIÓN: Busca al beneficiario de a cuerdo al tipo seleccionadoo en el combo Tipo_Beneficiario
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 21/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Buscar_Beneficiario()
    {
        Cls_Ope_Con_Estatus_Cheques_Negocio Autorizar_Cheque_Negocio = new Cls_Ope_Con_Estatus_Cheques_Negocio();
        DataTable Dt_Beneficiarios = null;
        try
        {
            Autorizar_Cheque_Negocio.P_Beneficiario = Txt_Beneficiario.Text.ToString().Trim(); //Criterio de busqueda para buscar el beneficiario
            if (Cmb_Tipo_Beneficiario.SelectedIndex > 0)
                Autorizar_Cheque_Negocio.P_Tipo_Beneficiario = Cmb_Tipo_Beneficiario.SelectedValue.Trim();//Criterio de busqueda para buscar el beneficiario

            Dt_Beneficiarios = Autorizar_Cheque_Negocio.Consultar_Beneficiarios(); //Consultamos los beneficiarios que coinciden con la descripcion introducida en txt_Beneficiario
            Cmb_Beneficiario.Items.Clear();
            if (Dt_Beneficiarios != null && Dt_Beneficiarios.Rows.Count > 0)
            {
                //Llenamos el combo de beneficiarios
                Cmb_Beneficiario.DataSource = Dt_Beneficiarios;
                Cmb_Beneficiario.DataTextField = Dt_Beneficiarios.Columns[0].ToString();
                Cmb_Beneficiario.DataValueField = Dt_Beneficiarios.Columns[0].ToString();
                Cmb_Beneficiario.DataBind();
                Cmb_Beneficiario.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
                Cmb_Beneficiario.SelectedIndex = 1;
                Txt_Beneficiario.Text = Cmb_Beneficiario.SelectedItem.Text;
            }
            else
            {
                Cmb_Beneficiario.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
                Cmb_Beneficiario.SelectedIndex = 0;
            }
        }
        catch (Exception Ex)
        {
           throw new Exception("Error al cargar los beneficiarios: " + Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Tipos_Beneficiarios
    ///DESCRIPCIÓN: Lllena el combo tipos_beneficiarios
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 13/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_Tipos_Beneficiarios()
    {
        Cls_Ope_Con_Estatus_Cheques_Negocio Seguimieto_Cheques_Negocio = new Cls_Ope_Con_Estatus_Cheques_Negocio();
        DataTable Dt_Tipos_Beneficiarios = new DataTable();
        try
        {
            Dt_Tipos_Beneficiarios = Seguimieto_Cheques_Negocio.Consultar_Tipos_Beneficiario();
            if (Dt_Tipos_Beneficiarios != null && Dt_Tipos_Beneficiarios.Rows.Count > 0)
            {
                Cmb_Beneficiario.Items.Clear();
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Tipo_Beneficiario, Dt_Tipos_Beneficiarios);
            }
            else
            {
                Cmb_Beneficiario.Items.Clear();
                Cmb_Beneficiario.Items.Add("<SELECCIONAR>");
                Cmb_Beneficiario.Items[0].Value = "0";
                Cmb_Beneficiario.Items[0].Selected = true;
            }
            //Combo Beneficiarios
            Cmb_Beneficiario.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
            Cmb_Beneficiario.SelectedIndex = 0;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar la lista de tipos de beneficiarios : " + Ex.Message);
        }
    }
    #endregion

    #region EVENTOS
    protected void Btn_Cancelar_Click(object sender, EventArgs e)
    {
        try
        {
            Autorizacion_Cheques_Inicio();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
        }
    }

    protected void Btn_Comentar_Click(object sender, EventArgs e)
    {
        Cls_Ope_Con_Estatus_Cheques_Negocio Autorizar_Cheques_Negocio = new Cls_Ope_Con_Estatus_Cheques_Negocio(); //Variable de conexión hacia la capa de Negocios
        try
        {
            if (!String.IsNullOrEmpty(Txt_Comentario.Text.ToString()))
            {
                Cancelar_Cheque_Pago(Hf_No_Cheque_Autorizar.Value, Txt_Comentario.Text);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Autorización de cheques", "alert('La cancelación del cheque de fue exitosa');", true);
                Autorizacion_Cheques_Inicio();
            }
            else
            {
                Autorizacion_Cheques_Inicio();
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";

            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #region eventos
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
    ///DESCRIPCIÓN          : Evento del boton de salir
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Diciembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Inicio")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Autorizacion_Cheques_Inicio();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Txt_Beneficiario_TextChanged
    ///DESCRIPCIÓN          : Evento de la caja de texto Beneficiario
    ///PROPIEDADES          :
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 21/Diciembre/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Txt_Beneficiario_TextChanged(object sender, EventArgs e)
    {
        try 
        {
            if (!String.IsNullOrEmpty(Txt_Beneficiario.Text.ToString()))
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Text = "";

                Buscar_Beneficiario();
            }
            else
            {
                Cmb_Beneficiario.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
                Cmb_Beneficiario.SelectedIndex = 0;
            }
            
        }
        catch(Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Beneficiario_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo Beneficiario
    ///PROPIEDADES          :
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 21/Diciembre/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Cmb_Beneficiario_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Beneficiario.SelectedIndex > 0)
        {
            Txt_Beneficiario.Text = Cmb_Beneficiario.SelectedItem.ToString();
        }
    }
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_No_Cheque_Click
    ///DESCRIPCIÓN          : Evento del Boton_Buscar
    ///PROPIEDADES          :
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 21/Diciembre/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Buscar_No_Cheque_Click(object sender, ImageClickEventArgs e)
    {
        try 
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Llenar_Grid_Cheques_Pago();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
    #region"Grid"
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Solicitud_Pagos_RowDataBound
    /// DESCRIPCION : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 09/enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Solicitud_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            CheckBox chek = (CheckBox)e.Row.FindControl("Chk_Autorizado");
            CheckBox chek2 = (CheckBox)e.Row.FindControl("Chk_Rechazado");
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[5].Text != "ENTRADA_CONTABILIDAD")
                {
                    chek.Enabled = false;
                    chek2.Enabled = false;
                }
                else
                {
                    chek.Enabled = true;
                    chek2.Enabled = true;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #endregion

    #endregion
}

