﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Tipo_Polizas.Negocios;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Polizas.Negocios;

public partial class paginas_contabilidad_Frm_Ope_Con_Reporte_Polizas : System.Web.UI.Page
{
    #region (Load/Init)
        protected void Page_Load(object sender, EventArgs e)
        {
            //Refresca la session del usuario lagueado al sistema.
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            //Valida que exista algun usuario logueado al sistema.
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
                Txt_Fecha_Inicio.Text=String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                Txt_Fecha_Final.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                Btn_Reporte_Pdf.Visible= false;
            }
        }
    #endregion
    #region (Metodos)
        #region (Métodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
            ///               diferentes operaciones
            /// PARAMETROS  : 
            /// CREO        : José Antonio López Hernández
            /// FECHA_CREO  : 09-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                try
                {
                    Limpia_Controles();     //Limpia los controles del forma
                    Consulta_Tipo_Poliza(); //Consulta todas los Tipos de Polizas que fueron dadas de alta en la BD
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString());
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION : Limpia los controles que se encuentran en la forma
            /// PARAMETROS  : 
            /// CREO        : José Antonio López Hernández
            /// FECHA_CREO  : 09-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Controles()
            {
                try
                {
                    Txt_Fecha_Final.Text = "";
                    Txt_Fecha_Inicio.Text = "";
                    //Txt_No_Poliza_Inicio.Text = "";
                    //Txt_No_Poliza_Termino.Text = "";
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
                }
            }
        #endregion
        #region (Control Acceso Pagina)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: IsNumeric
            /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
            /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
            /// CREO        : José Antonio López Hernández
            /// FECHA_CREO  : 29/Noviembre/2010
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Es_Numero(String Cadena)
            {
                Boolean Resultado = true;
                Char[] Array = Cadena.ToCharArray();
                try
                {
                    for (int index = 0; index < Array.Length; index++)
                    {
                        if (!Char.IsDigit(Array[index])) return false;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
                }
                return Resultado;
            }
        #endregion
        #region (Método Consulta)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Tipo_Poliza
            /// DESCRIPCION : Consulta los Tipos de Poliza que estan dadas de alta en la BD
            /// PARAMETROS  : 
            /// CREO        : José Antonio López Hernández
            /// FECHA_CREO  : 09-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
                ///*******************************************************************************
            private void Consulta_Tipo_Poliza()
            {
                Cls_Cat_Con_Tipo_Polizas_Negocio Rs_Consulta_Cat_Con_Tipo_Polizas = new Cls_Cat_Con_Tipo_Polizas_Negocio(); //Variable de conexión hacia la capa de Negocios
                DataTable Dt_Tipo_Poliza; //Variable que obtendra los datos de la consulta 

                try
                {
                    Rs_Consulta_Cat_Con_Tipo_Polizas.P_Descripcion = "";
                    Dt_Tipo_Poliza = Rs_Consulta_Cat_Con_Tipo_Polizas.Consulta_Datos_Tipo_Poliza(); //Consulta los datos generales de los Tipos de Poliza dados de alta en la BD
                    Session["Consulta_Tipo_Poliza"] = Dt_Tipo_Poliza;
                    Llena_Check_Tipo_Poliza(); //Agrega los tipos de Poliza obtenidas de la consulta anterior
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Tipo_Poliza " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Llena_Check_Tipo_Poliza
            /// DESCRIPCION : Llena el grid con los Tipos de Poliza que se encuentran en la 
            ///               base de datos
            /// PARAMETROS  : 
            /// CREO        : José Antonio López Hernández
            /// FECHA_CREO  : 11/Julio/2011 13:18
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Llena_Check_Tipo_Poliza()
            {
                DataTable Dt_Tipo_Poliza; //Variable que obtendra los datos de la consulta 
                try
                {
                    Chk_Tipos_Poliza.DataBind();
                    Dt_Tipo_Poliza = (DataTable)Session["Consulta_Tipo_Poliza"];
                    Chk_Tipos_Poliza.DataSource = Dt_Tipo_Poliza;
                    Chk_Tipos_Poliza.DataTextField = "Descripcion";
                    Chk_Tipos_Poliza.DataValueField = "Tipo_Poliza_ID";
                    Chk_Tipos_Poliza.DataBind();
                }
                catch (Exception ex)
                {
                    throw new Exception("Llena_Check_Tipo_Poliza " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
            ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
            ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
            ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
            ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 01/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Nombre_PDF)
            {
                ReportDocument Reporte = new ReportDocument();
                String File_Path = Server.MapPath("../Rpt/Contabilidad/" + Nombre_Reporte);
                Reporte.Load(File_Path);
                Ds_Reporte = Data_Set_Consulta_DB;
                Reporte.SetDataSource(Ds_Reporte);
                ExportOptions Export_Options = new ExportOptions();
                DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
                Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
                Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
                Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
                Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Export_Options);
                String Ruta = "../../Reporte/" + Nombre_PDF;
                //Mostrar_Reporte(Nombre_PDF, "PDF");
            }
            /////*******************************************************************************
            /////NOMBRE:              Mostrar_Reporte
            /////DESCRIPCIÓN:         Muestra el reporte en pantalla.
            /////PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
            /////                     Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
            /////USUARIO CREO:        Juan Alberto Hernández Negrete.
            /////FECHA CREO:          3/Mayo/2011 18:20 p.m.
            /////USUARIO MODIFICO:    Salvador Hernández Ramírez
            /////FECHA MODIFICO:      16-Mayo-2011
            /////CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
            /////*******************************************************************************
            //protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
            //{
            //    String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

            //    try
            //    {
            //        if (Formato == "PDF")
            //        {
            //            Pagina = Pagina + Nombre_Reporte_Generar;
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            //            "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            //        }
            //        else if (Formato == "Excel")
            //        {
            //            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            //        }
            //    }
            //    catch (Exception Ex)
            //    {
            //        throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            //    }
            //}
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Formato_Fecha
            ///DESCRIPCIÓN: Metodo que cambia el mes dic a dec para que oracle lo acepte
            ///PARAMETROS:  1.- String Fecha, es la fecha a la cual se le cambiara el formato 
            ///                     en caso de que cumpla la condicion del if
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 2/Septiembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            public String Formato_Fecha(String Fecha)
            {
                String Fecha_Valida = Fecha;
                //Se le aplica un split a la fecha 
                String[] aux = Fecha.Split('/');
                //Se modifica el es a solo mayusculas para que oracle acepte el formato. 
                switch (aux[1])
                {
                    case "dic":
                        aux[1] = "DEC";
                        break;
                }
                //Concatenamos la fecha, y se cambia el orden a DD-MMM-YYYY para que sea una fecha valida para oracle
                Fecha_Valida = aux[0] + "-" + aux[1] + "-" + aux[2];
                return Fecha_Valida;
            }// fin de Formato_Fecha
        #endregion
        #region (Validaciones)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Validar_Datos_Operacion
            /// DESCRIPCION : Validar datos requeridos para realizar la operación.
            /// CREO        : Juan Alberto Hernandez Negrete
            /// FECHA_CREO  : 18/Mayo/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Validar_Datos()
            {
                Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";

                try
                {
                    //if (Txt_No_Poliza_Inicio.Text != "" || Txt_No_Poliza_Termino.Text != "")
                    //{
                    //    if (Txt_No_Poliza_Inicio.Text == "")
                    //    {
                    //        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Es necesario proporcionar el limite inicial del rango de números de póliza. <br>";
                    //        Datos_Validos = false;
                    //    }
                    //    else
                    //    {
                    //        if (Txt_No_Poliza_Termino.Text == "")
                    //        {
                    //            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Es necesario proporcionar el limite final del rango de números de póliza. <br>";
                    //            Datos_Validos = false;
                    //        }
                    //    }
                    //}

                    if (Txt_Fecha_Inicio.Text == "" || Txt_Fecha_Final.Text == "")
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Es necesario seleccionar el periodo de fechas para generar el reporte. <br>";
                        Datos_Validos = false;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al validar los datos para el reporte. Error: [" + Ex.Message + "]");
                }

                return Datos_Validos;
            }
        #endregion
    #endregion
    #region (Eventos)
            protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            String Mi_SQL = ""; //Arma el query de consulta de las polizas por tipo
            Boolean Selecciono_Tipos_Poliza = false; //Anexa el filtro de los tipos de poliza
            Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Ope_Con_Poliza_Detalles = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocioss

            try
            {
                if (Validar_Datos())
                {
                    //Creamos el objeto del dataset perteneciente al reporte
                    Ds_Rpt_Con_Tipos_Polizas Ds_Obj_Rpt_Tipos_Polizas = new Ds_Rpt_Con_Tipos_Polizas();                    

                    Mi_SQL = " AND (";
                    for (int Cont_Tipos = 0; Cont_Tipos < Chk_Tipos_Poliza.Items.Count; Cont_Tipos++)
                    {
                        if (Chk_Tipos_Poliza.Items[Cont_Tipos].Selected == true)
                        {
                            Mi_SQL += Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '" + Chk_Tipos_Poliza.Items[Cont_Tipos].Value + "' OR ";
                            Selecciono_Tipos_Poliza = true;
                        }
                    }
                    //Si hubo tipos seleccionados quita el ultimo OR y agrega el parentesis del cierre
                    if (Selecciono_Tipos_Poliza == true)
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 3);
                        Mi_SQL += ")";
                    }
                    //Si no se selecciono ningun tipo se quita el primer parentesis y el AND
                    else
                    {
                        Mi_SQL = "";
                    }

                    if (!String.IsNullOrEmpty(Mi_SQL)) Rs_Consulta_Ope_Con_Poliza_Detalles.P_Tipo_Polizas = Mi_SQL;
                    if (!String.IsNullOrEmpty(Txt_Fecha_Inicio.Text)) Rs_Consulta_Ope_Con_Poliza_Detalles.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text));
                    if (!String.IsNullOrEmpty(Txt_Fecha_Final.Text)) Rs_Consulta_Ope_Con_Poliza_Detalles.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text));
                    if (!String.IsNullOrEmpty(Txt_Concepto.Text)) Rs_Consulta_Ope_Con_Poliza_Detalles.P_Concepto = Txt_Concepto.Text.Trim().Replace("'","");
                    DataSet Ds_Rpt_Tipos_Polizas_Datos = Rs_Consulta_Ope_Con_Poliza_Detalles.Consulta_Tipo_Poliza();

                    //Ds_Rpt_Tipos_Polizas_Datos.Tables[0].TableName = "Ope_Polizas";
                    Llenar_Grid_Polizas(Ds_Rpt_Tipos_Polizas_Datos.Tables[0]);
                    //Generar_Reporte(Ds_Rpt_Tipos_Polizas_Datos, Ds_Obj_Rpt_Tipos_Polizas, "Rpt_Con_Tipos_Polizas.rpt", "Rpt_Con_Tipos_Polizas.pdf");
                }
                else
                {
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Visible = true;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Ex.Message;
            }
        }
            //'****************************************************************************************
            //'NOMBRE DE LA FUNCION: Grid_Polizas_RowDataBound
            //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            //'PARAMETROS  : 
            //'CREO        : Sergio Manuel Gallardo
            //'FECHA_CREO  : 16/julio/2011 10:01 am
            //'MODIFICO          :
            //'FECHA_MODIFICO    :
            //'CAUSA_MODIFICACION:
            //'****************************************************************************************
            protected void Grid_Polizas_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Polizas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
                String No_Poliza = "";
                String Mes_Anio = "";
                String Tipo_Poliza = "";
                int Contador;
                Image Img = new Image();
                Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
                Label Lbl_facturas = new Label();
                Lbl_facturas = (Label)e.Row.FindControl("Lbl_Poliza");
                try
                {
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {
                        Contador = (int)(Session["Contador"]);
                        Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                        Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                        + (Lbl_facturas.Text + ("\',\'"
                                        + (Img.ClientID + "\')")))));
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Polizas"]));
                        No_Poliza = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["P_No_Poliza"].ToString());
                        Tipo_Poliza = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["Tipo_Poliza_ID"].ToString());
                        Mes_Anio = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["Mes_Ano"].ToString());
                        Rs_Polizas.P_Mes_Anio = Mes_Anio;
                        Rs_Polizas.P_Tipo_Polizas = Tipo_Poliza;
                        Rs_Polizas.P_No_Poliza = No_Poliza;
                        Rs_Polizas.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                        Rs_Polizas.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));
                        Ds_Consulta = Rs_Polizas.Consulta_Detalles_Poliza();
                        Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Movimientos");
                        Gv_Detalles.DataSource = Ds_Consulta;
                        Gv_Detalles.DataBind();
                        Session["Contador"] = Contador + 1;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_Analitico_Click
            ///DESCRIPCIÓN: Realizara los metodos requeridos para el reporte
            ///PARAMETROS: 
            ///CREO:        Hugo Enrique Ramírez Aguilera
            ///FECHA_CREO:  21/Febrero/2012
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Btn_Reporte_PDF_Click(object sender, ImageClickEventArgs e)
            {
                DataTable Dt_Consulta = new DataTable();
                try
                {

                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Polizas = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
                    DataTable Dt_Tipo_Reporte = new DataTable(); //Variable a conter los valores a pasar al reporte
                    Ds_Rpt_Con_Tipos_Polizas Ds_Reporte = new Ds_Rpt_Con_Tipos_Polizas();
                    ReportDocument Reporte = new ReportDocument();
                    String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
                    String Nombre_Archivo = "Polizas" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
                    String Mi_SQL = ""; //Arma el query de consulta de las polizas por tipo
                    Boolean Selecciono_Tipos_Poliza = false; //Anexa el filtro de los tipos de poliza
                    Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Ope_Con_Poliza_Detalles = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocioss
                    if (Validar_Datos())
                    {
                        //Creamos el objeto del dataset perteneciente al reporte
                        Ds_Rpt_Con_Tipos_Polizas Ds_Obj_Rpt_Tipos_Polizas = new Ds_Rpt_Con_Tipos_Polizas();

                        Mi_SQL = " AND (";
                        for (int Cont_Tipos = 0; Cont_Tipos < Chk_Tipos_Poliza.Items.Count; Cont_Tipos++)
                        {
                            if (Chk_Tipos_Poliza.Items[Cont_Tipos].Selected == true)
                            {
                                Mi_SQL += Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '" + Chk_Tipos_Poliza.Items[Cont_Tipos].Value + "' OR ";
                                Selecciono_Tipos_Poliza = true;
                            }
                        }
                        //Si hubo tipos seleccionados quita el ultimo OR y agrega el parentesis del cierre
                        if (Selecciono_Tipos_Poliza == true)
                        {
                            Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 3);
                            Mi_SQL += ")";
                        }
                        //Si no se selecciono ningun tipo se quita el primer parentesis y el AND
                        else
                        {
                            Mi_SQL = "";
                        }

                        if (!String.IsNullOrEmpty(Mi_SQL)) Rs_Consulta_Ope_Con_Poliza_Detalles.P_Tipo_Polizas = Mi_SQL;
                        if (!String.IsNullOrEmpty(Txt_Fecha_Inicio.Text)) Rs_Consulta_Ope_Con_Poliza_Detalles.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text));
                        if (!String.IsNullOrEmpty(Txt_Fecha_Final.Text)) Rs_Consulta_Ope_Con_Poliza_Detalles.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text));
                        DataSet Ds_Rpt_Tipos_Polizas_Datos = Rs_Consulta_Ope_Con_Poliza_Detalles.Consulta_Tipo_Poliza();
                        Dt_Consulta = Ds_Rpt_Tipos_Polizas_Datos.Tables[0];
                        Dt_Consulta.TableName = "Ope_Con_Polizas";
                        Ds_Reporte.Clear();
                        Ds_Reporte.Tables.Clear();
                        Ds_Reporte.Tables.Add(Dt_Consulta.Copy());
                        Ds_Reporte.Tables.Add(Dt_Tipo_Reporte.Copy());
                        Reporte.Load(Ruta_Archivo + "Rpt_Con_Tipos_Polizas.rpt");
                        Reporte.SetDataSource(Ds_Reporte);
                        DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();
                        Nombre_Archivo += ".pdf";
                        Ruta_Archivo = @Server.MapPath("../../Reporte/");
                        m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;
                        ExportOptions Opciones_Exportacion = new ExportOptions();
                        Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                        Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                        Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                        Reporte.Export(Opciones_Exportacion);

                        Abrir_Ventana(Nombre_Archivo);

                    }
                }
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Ex.Message;
                    throw new Exception(Ex.Message, Ex);
                }
            }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO  : 21-Febrero-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    private void Abrir_Ventana(String Nombre_Archivo)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
        try
        {
            Pagina = Pagina + Nombre_Archivo;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
        }
    }
            //*******************************************************************************
            // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Reservas
            // DESCRIPCIÓN: Llena el grid principal de requisiciones
            // RETORNA: 
            // CREO: Gustavo Angeles Cruz
            // FECHA_CREO: Diciembre/2010 
            // MODIFICOHabili

            // FECHA_MODIFICO:
            // CAUSA_MODIFICACIÓN:
            //********************************************************************************/
            public void Llenar_Grid_Polizas(DataTable Dt_Polizas)
            {
                DataTable Dt_Datos_Detalles = new DataTable();
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Polizas"]));
                int Contador = 0;
                Session["Contador"] = Contador;
                if (Dt_Polizas.Rows.Count > 0)
                {
                    Btn_Reporte_Pdf.Visible = true;
                }
                else
                {
                    Btn_Reporte_Pdf.Visible = false;

                }
                Session["Dt_Datos_Polizas"] = Dt_Polizas;
                    Grid_Polizas.DataSource = Dt_Polizas;
                    Grid_Polizas.DataBind();
            }
            protected void Btn_Poliza_Click(object sender, EventArgs e)
            {
                String fecha = ((LinkButton)sender).CommandArgument;
                String Tipo_poliza = ((LinkButton)sender).CssClass;
                String No_poliza = ((LinkButton)sender).Text;
                Imprimir(No_poliza, Tipo_poliza, fecha);
            }
            protected void Imprimir(String NO_POLIZA, String TIPO_POLIZA, String FECHA)
            {
                DataSet Ds_Reporte = null;
                DataTable Dt_Modificado = new DataTable();
                String Mes = "";
                DataTable Dt_Pagos = new DataTable();
                String Ano = "";
                try
                {
                       // Fecha_Poliza = Convert.ToDateTime(FECHA);
                        Mes = FECHA.Substring(0, 2);
                        Ano = FECHA.Substring(2, 2);
                    Cls_Ope_Con_Polizas_Negocio Poliza = new Cls_Ope_Con_Polizas_Negocio();
                    Ds_Reporte = new DataSet();
                    Poliza.P_No_Poliza = NO_POLIZA;
                    Poliza.P_Tipo_Poliza_ID = TIPO_POLIZA;
                    Poliza.P_Mes_Ano = Mes + Ano;
                    Dt_Pagos = Poliza.Consulta_Detalle_Poliza();
                    if (Dt_Pagos.Rows.Count > 0)
                    {
                        if (Dt_Modificado.Rows.Count <= 0 && Dt_Modificado.Columns.Count <= 0)
                        {
                            Dt_Modificado.Columns.Add("NO_POLIZA", typeof(System.String));
                            Dt_Modificado.Columns.Add("MES_ANO", typeof(System.String));
                            Dt_Modificado.Columns.Add("TIPO_POLIZA_ID", typeof(System.String));
                            Dt_Modificado.Columns.Add("NO_PARTIDAS", typeof(System.String));
                            Dt_Modificado.Columns.Add("CONCEPTO", typeof(System.String));
                            Dt_Modificado.Columns.Add("FECHA_POLIZA", typeof(System.String));
                            Dt_Modificado.Columns.Add("TOTAL_HABER", typeof(System.Double));
                            Dt_Modificado.Columns.Add("TOTAL_DEBE", typeof(System.Double));
                            Dt_Modificado.Columns.Add("REFERENCIA", typeof(System.String));
                            Dt_Modificado.Columns.Add("PARTIDA", typeof(System.Double));
                            Dt_Modificado.Columns.Add("CUENTA_CONTABLE_ID", typeof(System.String));
                            Dt_Modificado.Columns.Add("CONCEPTO_PARTIDA", typeof(System.String));
                            Dt_Modificado.Columns.Add("DEBE", typeof(System.Double));
                            Dt_Modificado.Columns.Add("HABER", typeof(System.Double));
                            Dt_Modificado.Columns.Add("CUENTA", typeof(System.String));
                            Dt_Modificado.Columns.Add("TIPO_POLIZA", typeof(System.String));
                            Dt_Modificado.Columns.Add("DESCRIPCION", typeof(System.String));
                            Dt_Modificado.Columns.Add("CODIGO_PROGRAMATICO", typeof(System.String));
                            Dt_Modificado.Columns.Add("BENEFICIARIO", typeof(System.String));
                        }
                        foreach (DataRow Renglon in Dt_Pagos.Rows)
                        {
                            DataRow Filas = Dt_Modificado.NewRow(); //Crea un nuevo registro a la tabla
                            Filas["NO_POLIZA"] = Renglon["NO_POLIZA"].ToString();
                            Filas["MES_ANO"] = Renglon["MES_ANO"].ToString();
                            Filas["TIPO_POLIZA_ID"] = Renglon["TIPO_POLIZA_ID"].ToString();
                            Filas["NO_PARTIDAS"] = Renglon["NO_PARTIDAS"].ToString();
                            Filas["CONCEPTO"] = Renglon["CONCEPTO"].ToString();
                            Filas["FECHA_POLIZA"] = Renglon["FECHA_POLIZA"].ToString();
                            Filas["TOTAL_HABER"] = Renglon["TOTAL_HABER"].ToString();
                            Filas["TOTAL_DEBE"] = Renglon["TOTAL_DEBE"].ToString();
                            Filas["REFERENCIA"] = Renglon["REFERENCIA"].ToString();
                            Filas["PARTIDA"] = Renglon["PARTIDA"].ToString();
                            Filas["CUENTA_CONTABLE_ID"] = Renglon["CUENTA_CONTABLE_ID"].ToString();
                            Filas["CONCEPTO_PARTIDA"] = Renglon["CONCEPTO_PARTIDA"].ToString();
                            Filas["DEBE"] = Renglon["DEBE"].ToString();
                            Filas["HABER"] = Renglon["HABER"].ToString();
                            Filas["CUENTA"] = Renglon["CUENTA"].ToString();
                            Filas["TIPO_POLIZA"] = Renglon["TIPO_POLIZA"].ToString();
                            Filas["DESCRIPCION"] = Renglon["DESCRIPCION"].ToString();
                            Filas["CODIGO_PROGRAMATICO"] = Renglon["CODIGO_PROGRAMATICO"].ToString();
                            Filas["BENEFICIARIO"] = Renglon["BENEFICIARIO_PAGO"].ToString();
                            Dt_Modificado.Rows.Add(Filas); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificado.AcceptChanges();
                        }


                        Dt_Modificado.TableName = "Dt_Datos_Poliza";
                        Ds_Reporte.Tables.Add(Dt_Modificado.Copy());
                        //Se llama al método que ejecuta la operación de generar el reporte.
                        Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Poliza.rpt", "Poliza" + NO_POLIZA, ".pdf");
                    }
                }
                //}
                catch (Exception Ex)
                {
                    //Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                    //Lbl_Mensaje_Error.Visible = true;
                }

            }
            /// *************************************************************************************
            /// NOMBRE:             Generar_Reporte
            /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
            ///              
            /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
            ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
            ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
            ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
            /// USUARIO CREO:       Juan Alberto Hernández Negrete.
            /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
            /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
            /// FECHA MODIFICO:     16/Mayo/2011
            /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
            ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
            /// *************************************************************************************
            public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
            {
                ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
                String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 

                try
                {
                    Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
                    Reporte.Load(Ruta);

                    if (Ds_Reporte_Crystal is DataSet)
                    {
                        if (Ds_Reporte_Crystal.Tables.Count > 0)
                        {
                            Reporte.SetDataSource(Ds_Reporte_Crystal);
                            Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                            Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
                }
            }
            /// *************************************************************************************
            /// NOMBRE:             Exportar_Reporte_PDF
            /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
            ///                     especificada.
            /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
            ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
            /// USUARIO CREO:       Juan Alberto Hernández Negrete.
            /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA MODIFICACIÓN:
            /// *************************************************************************************
            public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
            {
                ExportOptions Opciones_Exportacion = new ExportOptions();
                DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

                try
                {
                    if (Reporte is ReportDocument)
                    {
                        Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                        Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                        Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                        Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                        Reporte.Export(Opciones_Exportacion);
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
                }
            }
            /// *************************************************************************************
            /// NOMBRE:              Mostrar_Reporte
            /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
            /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
            ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
            /// USUARIO CREO:        Juan Alberto Hernández Negrete.
            /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
            /// USUARIO MODIFICO:    Salvador Hernández Ramírez
            /// FECHA MODIFICO:      16-Mayo-2011
            /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
            /// *************************************************************************************
            protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
            {
                String Pagina = "../../Reporte/"; //"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

                try
                {
                    Pagina = Pagina + Nombre_Reporte_Generar + Formato;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                        "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
                }
            }
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
    #endregion
}