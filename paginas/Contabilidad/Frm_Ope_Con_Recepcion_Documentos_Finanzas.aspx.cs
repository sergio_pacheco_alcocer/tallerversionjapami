﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Autoriza_Ejercido.Negocio;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Cat_Contratos.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;
using System.Data.SqlClient;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Solicitud_Pagos_Ingresos.Datos;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Parametros_Almacen_Cuentas.Negocio;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;

public partial class paginas_Contabilidad_Frm_Ope_Con_Recepcion_Documentos_Finanzas : System.Web.UI.Page
{
    #region PAGE LOAD
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    Recepcion_Documentos_Inicio();
                    Acciones();
                    Txt_Lector_Codigo_Barras.Focus();
                    Llenar_Combo_Banco();
                }
            }
            catch(Exception Ex) 
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error["+Ex.Message+"]");
            }
        }
    #endregion

    #region METODOS
        //*******************************************************************************
        // NOMBRE DE LA FUNCIÓN: Llenar_Combos_Generales()
        // DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
        // RETORNA: 
        // CREO: Sergio Manuel Gallardo Andrade
        // FECHA_CREO: 17/Noviembre/2011 
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        public void Llenar_Combo_Banco()
        {
            Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
            //DataTable Dt_Bancos = Bancos_Negocio.Consulta_Bancos();
            DataTable Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Banco_Lector, Dt_Existencia, "NOMBRE", "NOMBRE");
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Recepcion_Documentos_Inicio
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Recepcion_Documentos_Inicio() 
        {
            try
            {
                Limpiar_Controles();
                Llenar_Grid_Solicitudes_Pago();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cmb_Banco_Lector_OnSelectedIndexChanged
        ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
        ///PARAMETROS: 
        ///CREO:       Sergio Manuel Gallardo Andrade
        ///FECHA_CREO:  04/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Cmb_Banco_Lector_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //Declaracion de variables
            Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
            DataTable Dt_Consulta = new DataTable();
            int Cont_Elementos = 0; //variable para el contador
            DropDownList Cmb_Banco_Transferencia_src; //Combo para la modificacion de los combos del grid de los bancos
            DropDownList Cmb_Cuenta_src; //Combos para la modificacion de los combos del grid de las cuentas del banco seleccionado
            Cls_Ope_Con_Cheques_Bancos_Negocio Cheques_Bancos_Negocio = new Cls_Ope_Con_Cheques_Bancos_Negocio(); //variable para la capa de negocio
            DataTable Dt_Cuentas_Contables = new DataTable(); //tabla para las cuentas contables del banco

            try
            {
                Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco_Lector.SelectedValue;
                Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Cuenta_Lector, Dt_Consulta, "CUENTA", "BANCO_ID");

                //Consultar las cuentas del banco
                Cheques_Bancos_Negocio.P_Nombre_Banco = Cmb_Banco_Lector.SelectedItem.Value;

                //Verificar si  no es el elemento 0
                if (Cmb_Banco_Lector.SelectedIndex > 0)
                {
                    //Consultar las cuentas contables del banco
                    Cheques_Bancos_Negocio.P_Nombre_Banco = Cmb_Banco_Lector.SelectedItem.Text;
                    Dt_Cuentas_Contables = Cheques_Bancos_Negocio.Consultar_Cuenta_Bancos_con_Descripcion();
                }

                //Ciclo para el barrido del grid
                for (Cont_Elementos = 0; Cont_Elementos < Grid_Solicitud_Pagos.Rows.Count; Cont_Elementos++)
                {
                    //instanciar el combo de los bancos
                    Cmb_Banco_Transferencia_src = ((DropDownList) Grid_Solicitud_Pagos.Rows[Cont_Elementos].Cells[5].FindControl("Cmb_Banco_Transferencia"));

                    //seleccionar el elementos
                    Cmb_Banco_Transferencia_src.SelectedIndex = Cmb_Banco_Lector.SelectedIndex;

                    //instanciar el combo de las cuentas
                    Cmb_Cuenta_src = ((DropDownList)Grid_Solicitud_Pagos.Rows[Cont_Elementos].Cells[6].FindControl("Cmb_Cuenta"));

                    //verificar si el indice no es cero
                    if (Cmb_Banco_Lector.SelectedIndex > 0)
                    {
                        //Llenar el combo de la cuentas
                        Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Cuenta_src, Dt_Cuentas_Contables, "CUENTA", "BANCO_ID");
                    }
                    else
                    {
                        Cmb_Cuenta_src.Items.Clear();
                    }                                        
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
        ///DESCRIPCIÓN          : Metodo para limpiar los controles de la página
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Limpiar_Controles()
        {
            try
            {
                Cmb_Banco_Lector.SelectedIndex = -1;
                Cmb_Cuenta_Lector.SelectedIndex = -1;
                Cmb_Forma_Pago_Lector.SelectedIndex = -1;
                Txt_No_Solicitud_Autorizar.Value = "";
                Txt_Rechazo.Value = "";
                Txt_Comentario.Text = "";
                //Txt_Documentos.Text = "";
                Txt_Comentario.Text = "";
                Grid_Solicitud_Pagos.DataSource = null;
                Grid_Solicitud_Pagos.DataBind();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al limpiar los controles de la página Error[" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Llenar_Grid_Solicitudes_Pago
        /// DESCRIPCION : Llena el grid Solicitudes de pago
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Llenar_Grid_Solicitudes_Pago()
        {
            try
            {
                Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Autoriza_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
                DataTable Dt_Resultado = new DataTable();
                DataTable Dt_Modificado = new DataTable();
                Rs_Autoriza_Solicitud.P_Estatus = "PRE-EJERCIDO";
                Dt_Resultado = Rs_Autoriza_Solicitud.Consulta_Solicitudes();
                DataTable Dt_Datos_Detalles = new DataTable();
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                int Contador = 0;
                Session["Contador"] = Contador;
                Grid_Solicitud_Pagos.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
                Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;

                if (Dt_Resultado.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Dt_Resultado.Rows)
                    {
                        if (Dt_Modificado.Rows.Count <= 0)
                        {
                            Dt_Modificado.Columns.Add("No_Solicitud_Pago", typeof(System.String));
                            Dt_Modificado.Columns.Add("No_Reserva", typeof(System.String));
                            Dt_Modificado.Columns.Add("Tipo_Solicitud_Pago_ID", typeof(System.String));
                            Dt_Modificado.Columns.Add("Tipo_Pago", typeof(System.String));
                            Dt_Modificado.Columns.Add("Concepto", typeof(System.String));
                            Dt_Modificado.Columns.Add("Beneficiario", typeof(System.String));
                            Dt_Modificado.Columns.Add("Estatus", typeof(System.String));
                            Dt_Modificado.Columns.Add("Monto", typeof(System.Double));
                        }
                        DataRow row = Dt_Modificado.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        row["No_Solicitud_Pago"] = Fila["No_Solicitud_Pago"].ToString().Trim();
                        row["No_Reserva"] = Fila["No_Reserva"].ToString().Trim();
                        row["Tipo_Solicitud_Pago_ID"] = Fila["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                        row["Tipo_Pago"] = Fila["Tipo_Pago_Finanzas"].ToString().Trim();
                        row["Concepto"] = Fila["Concepto"].ToString().Trim();
                        if (!String.IsNullOrEmpty(Fila["Proveedor"].ToString().Trim()))
                        {
                            row["Beneficiario"] = Fila["Proveedor"].ToString().Trim();
                        }
                        else
                        {
                            row["Beneficiario"] = Fila["Empleado"].ToString().Trim();
                        }
                        row["Estatus"] = Fila["Estatus"].ToString().Trim();
                        row["Monto"] = Fila["Monto"].ToString().Trim();
                        Dt_Modificado.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Modificado.AcceptChanges();
                    }
                    Session["Dt_Datos_Detalles"] = Dt_Modificado;
                    Grid_Solicitud_Pagos.Columns[2].Visible = true;
                    Grid_Solicitud_Pagos.Columns[9].Visible = true;
                    Grid_Solicitud_Pagos.DataSource = Dt_Modificado;   // Se iguala el DataTable con el Grid
                    Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;
                    Grid_Solicitud_Pagos.Columns[2].Visible = false;
                    Grid_Solicitud_Pagos.Columns[9].Visible = false;
                }
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitudes de Pago", "alert('En este momento no se tienen pagos pendientes por autorizar');", true);
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
            }
        }
        // ****************************************************************************************
        //'NOMBRE DE LA FUNCION:Accion
        //'DESCRIPCION : realiza la modificacion la preautorización del pago o el rechazo de la solicitud de pago
        //'PARAMETROS  : 
        //'CREO        : Sergio Manuel Gallardo
        //'FECHA_CREO  : 07/Noviembre/2011 12:12 pm
        //'MODIFICO          :
        //'FECHA_MODIFICO    :
        //'CAUSA_MODIFICACION:
        //'****************************************************************************************
        protected void Acciones()
        {
            String Accion = String.Empty;
            String No_Solicitud = String.Empty;
            String Comentario = String.Empty;
            String Documento = String.Empty;
            String Estatus;
            String Forma_Pago;
            String Cuenta_Banco = "";
            DataTable Dt_solicitud= new DataTable();
            DataTable Dt_Consulta_Estatus = new DataTable();
            Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
            if (Request.QueryString["Accion"] != null)
            {
                Accion = HttpUtility.UrlDecode(Request.QueryString["Accion"].ToString());
                if (Request.QueryString["id"] != null)
                {
                    No_Solicitud = HttpUtility.UrlDecode(Request.QueryString["id"].ToString());
                }
                if (Request.QueryString["x"] != null)
                {
                    Comentario = HttpUtility.UrlDecode(Request.QueryString["x"].ToString());
                }
                //Response.Clear()
                switch (Accion)
                {
                    case "Codigo_Barras":
                        Forma_Pago = Comentario.Substring(0,Comentario.IndexOf('-'));
                        Cuenta_Banco = Comentario.Substring(Comentario.IndexOf('-')+1);
                            if (No_Solicitud.Length < 10)
                            {
                                Rs_Solicitud_Pago.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                            }
                            else
                            {
                                Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                            }
                            Dt_solicitud = Rs_Solicitud_Pago.Consultar_Solicitud_Pago();
                            if (Dt_solicitud.Rows.Count > 0)
                            {
                                Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                                if (Estatus == "PRE-EJERCIDO")
                                {
                                    Rs_Solicitud_Pago.P_Estatus = "EJERCIDO";
                                    Rs_Solicitud_Pago.P_Forma_Pago = Forma_Pago;
                                    Rs_Solicitud_Pago.P_Cuenta_Banco_Pago = Cuenta_Banco;
                                    Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                    Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                    Rs_Solicitud_Pago.P_Usuario_Recibio_Pagado = Cls_Sessiones.Nombre_Empleado.ToString();
                                    Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                }
                            }
                        break;
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago
        /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
        ///               proporcionados por el usuario
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private String Cancela_Solicitud_Pago(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand  P_Cmmd)
        {
            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
            Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
            DataTable Dt_Cuentas_Iva = new DataTable();
            DataTable Dt_Cuentas = new DataTable();
            DataTable Dt_C_Proveedor = new DataTable();
            DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Honorarios = new DataTable();
            DataTable Dt_Arrendamientos = new DataTable();
            DataTable Dt_Hon_Asimilables = new DataTable();
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
            DataRow Registro_Honorarios;
            Double Monto_total = 0;
            Decimal Iva_Por_Partida = 0;
            DataRow Registro_Arrendamiento;
            DataRow Registro_Hon_Asimila;
            Decimal Total_ISR_Honorarios = 0;
            Decimal Total_Cedular_Honorarios = 0;
            Decimal Total_ISR_Arrendamientos = 0;
            Decimal Total_Cedular_Arrendamientos = 0;
            Decimal Total_Hono_asimi = 0;
            String Cuenta_ISR = "0";
            String Cuenta_Cedular = "0";
            String CTA_IVA_PENDIENTE = "";
            String Beneficiario_ID = "";
            String Tipo_Beneficiario = "";
            Int32 partida = 0;
            Decimal Total_Iva_por_Acreditar = 0;
            String Resultado = ""; 
            String Tipo_Comprobacion = "";            
            DataRow row;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (P_Cmmd != null)
            {
                Cmmd =P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            //int Partida = 0;
            try
            {
                //Agregar las partidas de los impuestos de isr y de retencion de iva si tiene honorarios la solicitud de pago
                //se crean las columnas de los datatable de honorarios y de arrendamientos
                if (Dt_Honorarios.Rows.Count == 0)
                {
                    Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                    Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                    Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Decimal));
                    Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Decimal));
                }
                if (Dt_Arrendamientos.Rows.Count == 0)
                {
                    Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                    Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                    Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Decimal));
                    Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Decimal));
                }
                if (Dt_Hon_Asimilables.Rows.Count == 0)
                {
                    Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                    Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                    Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Decimal));
                    Dt_Hon_Asimilables.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Decimal));
                }
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                if (Dt_Datos_Solicitud.Rows.Count > 0)
                {
                    foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                    {
                        if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                        {
                            Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                        }
                    }
                    //se recorre el dataset de los detalles para dividir los detalles por tipo de operacion 
                    foreach (DataRow fila in Dt_Datos_Solicitud.Rows)
                    {
                        if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "HONORARIOS")
                        {
                            Registro_Honorarios = Dt_Honorarios.NewRow();
                            Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                            Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                            Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                            Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                            Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                            Dt_Honorarios.Rows.Add(Registro_Honorarios); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Honorarios.AcceptChanges();
                        }
                        if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "ARRENDAMIENTO")
                        {
                            Registro_Arrendamiento = Dt_Arrendamientos.NewRow();
                            Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                            Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                            Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                            Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                            Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                            Dt_Arrendamientos.Rows.Add(Registro_Arrendamiento); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Arrendamientos.AcceptChanges();
                        }
                        if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "HON. ASIMILABLE")
                        {
                            Registro_Hon_Asimila = Dt_Hon_Asimilables.NewRow();
                            Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                            Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                            Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                            Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                            Registro_Hon_Asimila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                            Dt_Hon_Asimilables.Rows.Add(Registro_Hon_Asimila); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Hon_Asimilables.AcceptChanges();
                        }
                    }
                    Total_ISR_Honorarios = 0;
                    Total_Cedular_Honorarios = 0;
                    if (Dt_Honorarios.Rows.Count > 0)
                    {
                        foreach (DataRow fila in Dt_Honorarios.Rows)
                        {
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                            {
                                Total_ISR_Honorarios = Total_ISR_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                            }
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                            {
                                Total_Cedular_Honorarios = Total_Cedular_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                            }

                        }
                    }
                    Total_ISR_Arrendamientos = 0;
                    Total_Cedular_Arrendamientos = 0;
                    if (Dt_Arrendamientos.Rows.Count > 0)
                    {
                        foreach (DataRow fila in Dt_Arrendamientos.Rows)
                        {
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                            {
                                Total_ISR_Arrendamientos = Total_ISR_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                            }
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                            {
                                Total_Cedular_Arrendamientos = Total_Cedular_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                            }
                        }
                    }
                    Total_Hono_asimi = 0;
                    if (Dt_Hon_Asimilables.Rows.Count > 0)
                    {
                        foreach (DataRow fila in Dt_Hon_Asimilables.Rows)
                        {
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                            {
                                Total_Hono_asimi = Total_Hono_asimi + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                            }
                        }
                    }
                }
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                String Tipo_Poliza_ID = "";
                foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
                {
                    if (!String.IsNullOrEmpty(Registro["Monto_Partida"].ToString()))
                    {
                        if (!String.IsNullOrEmpty(Registro["IVA_MONTO"].ToString()))
                        {
                            Iva_Por_Partida = Convert.ToDecimal(Registro["IVA_MONTO"].ToString());
                        }
                        else
                        {
                            Iva_Por_Partida = 0;
                        }
                        partida = partida + 1;
                        Txt_Monto_Solicitud.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                        if (!String.IsNullOrEmpty(Registro["Empleado_Cuenta"].ToString()))
                        {
                            Txt_Cuenta_Contable_ID_Empleado.Value = Registro["Empleado_Cuenta"].ToString();
                            Beneficiario_ID = Registro[Cat_Empleados.Campo_Empleado_ID].ToString();
                            Tipo_Beneficiario = "EMPLEADO";
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Acreedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "A")
                            {
                                Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Acreedor"].ToString(); 
                                Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                Tipo_Beneficiario = "PROVEEDOR";
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Contratista"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "C")
                                {
                                    Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Contratista"].ToString();
                                    Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                    Tipo_Beneficiario = "PROVEEDOR";
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Deudor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "D")
                                    {
                                        Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Deudor"].ToString();
                                        Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                        Tipo_Beneficiario = "PROVEEDOR";
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Judicial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "J")
                                        {
                                            Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Judicial"].ToString();
                                            Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                            Tipo_Beneficiario = "PROVEEDOR";
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Nomina"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "N")
                                            {
                                                Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Nomina"].ToString();
                                                Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                                Tipo_Beneficiario = "PROVEEDOR";
                                            }
                                            else
                                            {
                                                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Predial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "Z")
                                                {
                                                    Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Predial"].ToString();
                                                    Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                                    Tipo_Beneficiario = "PROVEEDOR";
                                                }
                                                else
                                                {
                                                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Proveedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                                                    {
                                                        Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Proveedor"].ToString();
                                                        Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                                        Tipo_Beneficiario = "PROVEEDOR";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (!String.IsNullOrEmpty(Registro["Tipo_Solicitud_Pago_ID"].ToString()))
                        {
                            Tipo_Poliza_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                            Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                            Rs_Consulta.P_Cmmd = Cmmd;
                            Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                        }
                        //Txt_Monto_Solicitud.Value = Registro["Monto"].ToString();
                        //Txt_Concepto_Solicitud.Value = Registro["Concepto"].ToString();
                        Txt_Cuenta_Contable_reserva.Value = Registro["Cuenta_Contable_Reserva"].ToString();
                        Txt_No_Reserva.Value = Registro["NO_RESERVA"].ToString();
                        if(Tipo_Comprobacion =="NO")
                        {
                            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                            if (Dt_Partidas_Polizas.Rows.Count == 0)
                            {
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
                                Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                            }
                            row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_reserva.Value;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Registro["Monto_Partida"].ToString()) + Convert.ToDecimal(Registro["Monto_ISR"].ToString()) + Convert.ToDecimal(Registro["Monto_Cedular"].ToString()) - Iva_Por_Partida;
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                            Monto_total = Monto_total + (Convert.ToDouble(Registro["Monto_Partida"].ToString()) + Convert.ToDouble(Registro["Monto_ISR"].ToString()) + Convert.ToDouble(Registro["Monto_Cedular"].ToString()) - Convert.ToDouble(Iva_Por_Partida));
                        }
                    }
                }
                if (Tipo_Comprobacion == "NO")
                {
                    //se consultan los detalles de la solicitud de pago
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                    Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                    if (Dt_Datos_Solicitud.Rows.Count > 0)
                    {
                        Total_ISR_Honorarios = 0;
                        Total_Cedular_Honorarios = 0;
                        if (Dt_Honorarios.Rows.Count > 0)
                        {
                            foreach (DataRow fila in Dt_Honorarios.Rows)
                            {
                                if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                                {
                                    Total_ISR_Honorarios = Total_ISR_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                                }
                                if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                                {
                                    Total_Cedular_Honorarios = Total_Cedular_Honorarios + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                                }
                            }
                        }
                        Total_ISR_Arrendamientos = 0;
                        Total_Cedular_Arrendamientos = 0;
                        if (Dt_Arrendamientos.Rows.Count > 0)
                        {
                            foreach (DataRow fila in Dt_Arrendamientos.Rows)
                            {
                                if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                                {
                                    Total_ISR_Arrendamientos = Total_ISR_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                                }
                                if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                                {
                                    Total_Cedular_Arrendamientos = Total_Cedular_Arrendamientos + Convert.ToDecimal(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                                }
                            }
                        }
                        row = Dt_Partidas_Polizas.NewRow();
                        partida = partida + 1;
                        //Agrega el abono del registro de la póliza
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                        if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Proveedor.Value.ToString()))
                        {
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Proveedor.Value;
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Empleado.Value.ToString()))
                            {
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Empleado.Value;
                            }
                        }
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Txt_Monto_Solicitud.Value.ToString()) - (Total_Cedular_Arrendamientos + Total_Cedular_Honorarios + Total_ISR_Arrendamientos + Total_ISR_Honorarios);
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                        row["Beneficiario_ID"] = Beneficiario_ID;
                        row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                        Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Partidas_Polizas.AcceptChanges();
                        // se recorre el datatable de honorarios para realizar la sumatoria de los impuestos en las variables correspondientes 
                        //y asi obtener el total de los impuestos de cada tipo
                        Rs_Parametros.P_Cmmd = Cmmd;
                        Dt_Cuentas = Rs_Parametros.Consulta_Datos_Parametros();
                        if (Dt_Honorarios.Rows.Count > 0)
                        {
                            if (Total_ISR_Honorarios > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                                if (Dt_Cuentas.Rows.Count > 0){
                                    Cuenta_ISR = Dt_Cuentas.Rows[0]["CTA_ISR"].ToString().Trim();
                                    }
                                if (!String.IsNullOrEmpty(Cuenta_ISR))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_ISR_Honorarios;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                    row["Beneficiario_ID"] = Beneficiario_ID;
                                    row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Total_Cedular_Honorarios > 0)
                            {
                                // CUENTA IMPUESTO CEDULAR 1% HONORARIOS RET consultamos el id que tiene la cuenta en el sistema
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Cedular = Dt_Cuentas.Rows[0]["CTA_Cedular"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_Cedular))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cedular;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Cedular_Honorarios;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                    row["Beneficiario_ID"] = Beneficiario_ID;
                                    row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                        }
                        // se recorre el datatable de honorarios para realizar la sumatoria de los impuestos en las variables correspondientes 
                        //y asi obtener el total de los impuestos de cada tipo
                        if (Dt_Arrendamientos.Rows.Count > 0)
                        {
                            Cuenta_Cedular = "";
                            Cuenta_ISR = "";
                            if (Total_ISR_Arrendamientos > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                               if (Dt_Cuentas.Rows.Count > 0)
                               {
                                    Cuenta_ISR = Dt_Cuentas.Rows[0]["ISR_Arrendamiento"].ToString().Trim();
                                }
                               if (!String.IsNullOrEmpty(Cuenta_ISR))
                               {
                                   partida = partida + 1;
                                   row = Dt_Partidas_Polizas.NewRow();
                                   //Agrega el cargo del registro de la póliza
                                   row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                   row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                                   row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                   row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_ISR_Arrendamientos;
                                   row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                   row["Beneficiario_ID"] = Beneficiario_ID;
                                   row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                   Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                   Dt_Partidas_Polizas.AcceptChanges();
                               }
                            }
                            if (Total_Cedular_Arrendamientos > 0)
                            {
                                // CUENTA IMPUESTO CEDULAR 1% HONORARIOS RET consultamos el id que tiene la cuenta en el sistema
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Cedular = Dt_Cuentas.Rows[0]["Cedular_Arrendamiento"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_Cedular))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cedular;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Cedular_Arrendamientos;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                    row["Beneficiario_ID"] = Beneficiario_ID;
                                    row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                        }
                        if (Dt_Hon_Asimilables.Rows.Count > 0)
                        {
                            if (Total_Hono_asimi > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_ISR = Dt_Cuentas.Rows[0]["Honorarios_Asimilables"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_ISR))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Hono_asimi;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                    row["Beneficiario_ID"] = Beneficiario_ID;
                                    row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                        }
                        //Se agrega la partida de iva pendiente de acreditar
                        Rs_Parametros_Iva.P_Cmmd = Cmmd;
                        Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            if (Total_Iva_por_Acreditar > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                                if (Dt_Cuentas_Iva.Rows.Count > 0)
                                {
                                    CTA_IVA_PENDIENTE = Dt_Cuentas_Iva.Rows[0]["CTA_IVA_PENDIENTE"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(CTA_IVA_PENDIENTE))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_PENDIENTE;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                                    row["Beneficiario_ID"] = Beneficiario_ID;
                                    row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                        }
                    }

                    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(partida);
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PRE-DOCUMENTADO";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Contabi = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Contabilidad = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Ejercido = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Contabilidad = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_recepcion_Documentos = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Jefe = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Ejercido = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva =Convert.ToDouble(Txt_No_Reserva.Value);
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Monto_total + Convert.ToDouble(Total_Iva_por_Acreditar);// Convert.ToDouble(Txt_Monto_Solicitud.Value);
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Resultado=Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
                }
                else
                {
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago_Sin_Poliza();
                    Resultado = "SI";
                }
                return Resultado;
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }

            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago_Servicios_Generales
        /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
        ///               proporcionados por el usuario
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Septiembre/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private String Cancela_Solicitud_Pago_Servicios_Generales(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand P_Cmmd)
        {
            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
            Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
            DataTable Dt_Cuentas_Iva = new DataTable();
            DataTable Dt_Cuentas = new DataTable();
            DataTable Dt_C_Proveedor = new DataTable();
            DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Honorarios = new DataTable();
            DataTable Dt_Arrendamientos = new DataTable();
            DataTable Dt_Hon_Asimilables = new DataTable();
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
            Double Monto_total = 0;
            String CTA_IVA_PENDIENTE = "";
            String Beneficiario_ID = "";
            String Tipo_Beneficiario = "";
            Int32 partida = 0;
            Decimal Total_Iva_por_Acreditar = 0;
            String Resultado = "";
            String Tipo_Comprobacion = "";
            DataRow row;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            //int Partida = 0;
            try
            {
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                if (Dt_Datos_Solicitud.Rows.Count > 0)
                {
                    foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                    {
                        if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                        {
                            Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                        }
                    }
                }
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago_Servicios_Generales();
                String Tipo_Poliza_ID = "";
                foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
                {
                    if (!String.IsNullOrEmpty(Registro["Monto_Partida"].ToString()))
                    {
                        partida = partida + 1;
                        Txt_Monto_Solicitud.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                        if (!String.IsNullOrEmpty(Registro["Empleado_Cuenta"].ToString()))
                        {
                            Txt_Cuenta_Contable_ID_Empleado.Value = Registro["Empleado_Cuenta"].ToString();
                            Beneficiario_ID = Registro[Cat_Empleados.Campo_Empleado_ID].ToString();
                            Tipo_Beneficiario = "EMPLEADO";
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Proveedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                            {
                                Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Proveedor"].ToString();
                                Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                Tipo_Beneficiario = "PROVEEDOR";
                            }
                        }
                        if (!String.IsNullOrEmpty(Registro["Tipo_Solicitud_Pago_ID"].ToString()))
                        {
                            Tipo_Poliza_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                            Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                            Rs_Consulta.P_Cmmd = Cmmd;
                            Tipo_Comprobacion = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                        }
                        Txt_Cuenta_Contable_reserva.Value = Registro["Cuenta_Contable_Reserva"].ToString();
                        Txt_No_Reserva.Value = Registro["NO_RESERVA"].ToString();
                        if (Tipo_Comprobacion == "NO")
                        {
                            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                            if (Dt_Partidas_Polizas.Rows.Count == 0)
                            {
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
                                Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                            }
                            row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_reserva.Value;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Registro["Monto_Partida"].ToString());
                            row["Beneficiario_ID"] = Beneficiario_ID;
                            row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                            Monto_total = Monto_total + (Convert.ToDouble(Registro["Monto_Partida"].ToString()));
                        }
                    }
                }
                if (Tipo_Comprobacion == "NO")
                {
                    //se consultan los detalles de la solicitud de pago
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                    Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                    if (Dt_Datos_Solicitud.Rows.Count > 0)
                    {
                        row = Dt_Partidas_Polizas.NewRow();
                        partida = partida + 1;
                        //Agrega el abono del registro de la póliza
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                        if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Proveedor.Value.ToString()))
                        {
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Proveedor.Value;
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Empleado.Value.ToString()))
                            {
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Empleado.Value;
                            }
                        }
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Txt_Monto_Solicitud.Value.ToString());
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                        row["Beneficiario_ID"] = Beneficiario_ID;
                        row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                        Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Partidas_Polizas.AcceptChanges();
                        //Se agrega la partida de iva pendiente de acreditar
                        Rs_Parametros_Iva.P_Cmmd = Cmmd;
                        Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            if (Total_Iva_por_Acreditar > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                                if (Dt_Cuentas_Iva.Rows.Count > 0)
                                {
                                    CTA_IVA_PENDIENTE = Dt_Cuentas_Iva.Rows[0]["CTA_IVA_PENDIENTE"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(CTA_IVA_PENDIENTE))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_PENDIENTE;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "RECHAZO-" + No_Solicitud_Pago;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                                    row["Beneficiario_ID"] = Beneficiario_ID;
                                    row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                        }
                    }
                    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(partida);
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PRE-DOCUMENTADO";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Servicios_Generales = "SI";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Contabi = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Contabilidad = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Ejercido = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Contabilidad = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_recepcion_Documentos = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Jefe = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Ejercido = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva.Value);
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Monto_total + Convert.ToDouble(Total_Iva_por_Acreditar);// Convert.ToDouble(Txt_Monto_Solicitud.Value);
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Resultado = Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
                }
                else
                {
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago_Sin_Poliza();
                    Resultado = "SI";
                }
                return Resultado;
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }

            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago_Finiquito
        /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
        ///               proporcionados por el usuario
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Agosto/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private String Cancela_Solicitud_Pago_Finiquito(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand P_Cmmd)
        {
            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
            Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
            DataTable Dt_Cuentas_Iva = new DataTable();
            DataTable Dt_Cuentas = new DataTable();
            DataTable Dt_C_Proveedor = new DataTable();
            DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Honorarios = new DataTable();
            DataTable Dt_Arrendamientos = new DataTable();
            DataTable Dt_Hon_Asimilables = new DataTable();
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
            String Beneficiario_ID = "";
            String Tipo_Beneficiario = "";
            Int32 partida = 0;
            String Resultado = "";
            String Concepto_Solicitud = "";
            DataRow row; 
            DataTable Dt_Deducciones = new DataTable();
            DataTable Dt_Datos_Deduccones = new DataTable();
            DataRow Registro_Deduccion;
            DataTable Dt_Cuenta_Empleado = new DataTable();
            DataTable Dt_Deducciones_Persepciones = new DataTable();
            Decimal Total_Debe = 0;
            Decimal Total_Monto = 0;
            Decimal Total_Haber = 0; 
            Decimal Total_Partidas = 0;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            //int Partida = 0;
            try
            {
                if (Dt_Deducciones.Rows.Count == 0)
                {
                    Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Partida_ID, typeof(System.String));
                    Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Fte_Financiamiento_ID, typeof(System.String));
                    Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID, typeof(System.String));
                    Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Area_Funcional_ID, typeof(System.String));
                    Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Debe, typeof(System.Double));
                    Dt_Deducciones.Columns.Add(Ope_Con_Solicitud_Finiquito_Det.Campo_Haber, typeof(System.Double));
                }
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                if (Dt_Datos_Polizas.Rows.Count > 0)
                {
                    //Se consultan las deducciones y persepciones de la solicitud de pago
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                    Dt_Datos_Deduccones = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud_Finiquito();
                    if (Dt_Datos_Deduccones.Rows.Count > 0)
                    {
                        foreach (DataRow Fila_Deduccion in Dt_Datos_Deduccones.Rows)
                        {
                            Registro_Deduccion = Dt_Deducciones.NewRow();
                            Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Partida_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Partida_ID].ToString().Trim();
                            Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Fte_Financiamiento_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Fte_Financiamiento_ID].ToString().Trim();
                            Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Proyecto_Programa_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Proyecto_Programa_ID].ToString().Trim();
                            Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID].ToString().Trim();
                            Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID].ToString().Trim();
                            Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Area_Funcional_ID] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Area_Funcional_ID].ToString().Trim();
                            Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString().Trim();
                            Registro_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber] = Fila_Deduccion[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString().Trim();
                            Dt_Deducciones.Rows.Add(Registro_Deduccion); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Deducciones.AcceptChanges();
                        }
                    }
                }
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                if (Dt_Datos_Polizas.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Dt_Datos_Polizas.Rows[0]["Monto"].ToString()))
                    {
                        Total_Partidas = Convert.ToDecimal(Dt_Datos_Polizas.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                        Txt_Monto_Solicitud.Value = Dt_Datos_Polizas.Rows[0]["Monto"].ToString();
                        Dt_Cuenta_Empleado = Rs_Parametros_Iva.Obtener_Parametros_Poliza();//se optiene la cuenta del empleado
                        if (Dt_Cuenta_Empleado.Rows.Count > 0)
                        {
                            Txt_Cuenta_Contable_ID_Empleado.Value = Dt_Cuenta_Empleado.Rows[0][Cat_Nom_Parametros_Poliza.Campo_Cuenta_Servicios_Profecionales_Pagar_A_CP].ToString();
                        }
                        else
                        {
                            Txt_Cuenta_Contable_ID_Empleado.Value = "";
                        }
                        Beneficiario_ID = Dt_Datos_Polizas.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString();
                        Tipo_Beneficiario = "EMPLEADO";
                        Concepto_Solicitud = Dt_Datos_Polizas.Rows[0]["Concepto"].ToString();
                        Txt_No_Reserva.Value = Dt_Datos_Polizas.Rows[0]["NO_RESERVA"].ToString();
                    }
                }
                if (Txt_Cuenta_Contable_ID_Empleado.Value != "")
                {
                    //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                    if (Dt_Partidas_Polizas.Rows.Count == 0)
                    {
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
                        Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                    }
                    ///Se consultan los detalles de las deducciones y persepciones del finiquito
                    Dt_Deducciones_Persepciones = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consultar_Datos_Solicitud_Finiquito();
                    if (Dt_Deducciones_Persepciones.Rows.Count > 0)
                    {
                        foreach (DataRow Fila_D_P in Dt_Deducciones_Persepciones.Rows)
                        {
                            if (Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString()) != 0)
                            {
                                partida = partida + 1;
                                Total_Debe = Total_Debe + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString());
                                row = Dt_Partidas_Polizas.NewRow();
                                //Agrega el cargo del registro de la póliza
                                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID].ToString();
                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString());
                                row["Beneficiario_ID"] = Beneficiario_ID;
                                row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Partidas_Polizas.AcceptChanges();
                            }
                            else
                            {
                                partida = partida + 1;
                                Total_Haber = Total_Haber + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString());
                                row = Dt_Partidas_Polizas.NewRow();
                                //Agrega el cargo del registro de la póliza
                                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Cuenta_Contable_ID].ToString();
                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString());
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                row["Beneficiario_ID"] = Beneficiario_ID;
                                row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Partidas_Polizas.AcceptChanges();
                            }
                        }
                    }
                    Total_Monto = Total_Debe - Total_Haber;
                    row = Dt_Partidas_Polizas.NewRow();
                    partida = partida + 1;
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Empleado.Value.ToString()))
                    {
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Empleado.Value;
                    }
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = //Txt_Concepto_Solicitud.Value;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Monto;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row["Beneficiario_ID"] = Beneficiario_ID;
                    row["Tipo_Beneficiario"] = Tipo_Beneficiario;
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();

                    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(partida);
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PRE-DOCUMENTADO";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto_Poliza = "RECHAZO-" + No_Solicitud_Pago;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Finanzas = Comentario;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Contabi = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Ejercido = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Contabilidad = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio_Ejercido = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Contabilidad = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_recepcion_Documentos = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Jefe = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Recibio_Ejercido = "";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva.Value);
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Convert.ToDouble(Txt_Monto_Solicitud.Value);
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Resultado = Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Finiquito(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
                }
                else
                {
                    Resultado = "NO,PE";
                }
                return Resultado;
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }

            }
        }
    #endregion

    #region EVENTOS
        protected void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Recepcion_Documentos_Inicio();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Solicitud_Click
        ///DESCRIPCIÓN: manda llamar el metodo de la impresion de la caratula 
        ///PARÁMETROS :
        ///CREO       : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO  : 21-mayo-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        protected void Btn_Solicitud_Click(object sender, EventArgs e)
        {
            String No_Solicitud = ((LinkButton)sender).Text;
            Imprimir(No_Solicitud);
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Comentar_Click
        ///DESCRIPCIÓN: Se cancela la solicitud de pago 
        ///PARÁMETROS :
        ///CREO       : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO  : 21-mayo-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        protected void Btn_Comentar_Click(object sender, EventArgs e)
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Tipo_Solicitud = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
            Cls_Con_Cat_Contratos_Negocios Rs_Contratos = new Cls_Con_Cat_Contratos_Negocios();
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
            DataTable Dt_Datos_Polizas = new DataTable();
            DataTable Dt_Datos_Contratos = new DataTable();
            DataTable Dt_Tipo_Solicitud = new DataTable();
            DataTable Dt_solicitud = new DataTable();
            String Servicios_Generales = "";
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            ReportDocument Reporte = new ReportDocument();
            String Tipo_Solicitud = "";
            String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
            String Nombre_Archivo = "Rechazado";// +Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
            String Usuario = "";
            String No_Reserva = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            String Resultado = "";
            // crear transaccion para crear el convenio 
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Cn;
            Cmmd.Transaction = Trans;
            try
            {
                DataRow Row;
                DataTable Dt_Reporte = new DataTable();
                if (Txt_Comentario.Text != "")
                {
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud_Autorizar.Value;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Dt_solicitud = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                    //revisar si la solicitud es de servicios masivos generales 
                    if (!String.IsNullOrEmpty(Dt_solicitud.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString().Trim()))
                    {
                        Servicios_Generales = "SI";
                    }
                    else
                    {
                        Servicios_Generales = "NO";
                    }
                    DataTable Dt_Tipo = new DataTable();
                    Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                    Rs_Consulta.P_Cmmd = Cmmd;
                    Dt_Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos();
                    if (Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().ToUpper() != "FINIQUITO" || Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().Substring(0, 4).ToUpper() != "FINI")
                    {
                        if (Servicios_Generales == "NO")
                        {
                            Resultado = Cancela_Solicitud_Pago(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(), Cmmd);
                           // Respuesta = Autoriza_Contabilidad_Solicitud_pago(No_Solicitud, Cmmd);//, Comentario, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString());
                        }
                        else
                        {
                            Resultado = Cancela_Solicitud_Pago_Servicios_Generales(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(), Cmmd);
                           // Respuesta = Autoriza_Contabilidad_Solicitud_Pagos_Generales(No_Solicitud, Cmmd);//Manda llamar la autorizacion para la creacion de la poliza masiva 
                        }
                    }
                    else
                    {
                        Resultado = Cancela_Solicitud_Pago_Finiquito(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(), Cmmd);
                    }
                        if (Resultado == "SI")
                        {
                            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud_Autorizar.Value;
                            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                            Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                            Trans.Commit();
                            //  se crea la tabla para el reporte de la cancelacion
                            Dt_Reporte.Columns.Add("NO_SOLICITUD", typeof(System.String));
                            Dt_Reporte.Columns.Add("NO_RESERVA", typeof(System.String));
                            Dt_Reporte.Columns.Add("PROVEEDOR", typeof(System.String));
                            Dt_Reporte.Columns.Add("MONTO", typeof(System.Double));
                            Dt_Reporte.Columns.Add("FECHA_CREO", typeof(System.DateTime));
                            Dt_Reporte.Columns.Add("FECHA_RECHAZO", typeof(System.DateTime));
                            Dt_Reporte.Columns.Add("TIPO_SOLICITUD_PAGO_ID", typeof(System.String));
                            Dt_Reporte.Columns.Add("CONCEPTO_SOLICITUD", typeof(System.String));
                            Dt_Reporte.Columns.Add("COMENTARIO", typeof(System.String));
                            Dt_Reporte.Columns.Add("USUARIO_CREO", typeof(System.String));
                            Dt_Reporte.Columns.Add("USUARIO_RECHAZO", typeof(System.String));
                            Dt_Reporte.TableName = "Dt_Cancelacion";


                            foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
                            {
                                Row = Dt_Reporte.NewRow();

                                Row["NO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString());
                                Row["NO_RESERVA"] = (Registro["Reserva"].ToString());
                                if (!String.IsNullOrEmpty(Registro["Proveedor"].ToString()))
                                {
                                    Row["PROVEEDOR"] = (Registro["Empleado"].ToString());
                                }
                                else
                                {
                                    Row["PROVEEDOR"] = (Registro["Proveedor"].ToString());
                                } 
                                Row["MONTO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                                Row["FECHA_CREO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo].ToString());
                                Row["FECHA_RECHAZO"] = "" + DateTime.Now;

                                //  para el tipo de solicitud
                                Rs_Tipo_Solicitud.P_Tipo_Solicitud = (Registro[Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString());
                                Dt_Tipo_Solicitud = Rs_Tipo_Solicitud.Consulta_Tipo_Solicitud();

                                foreach (DataRow Tipo in Dt_Tipo_Solicitud.Rows)
                                {
                                    Tipo_Solicitud = (Tipo[Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion].ToString());
                                }
                                Row["TIPO_SOLICITUD_PAGO_ID"] = Tipo_Solicitud;
                                Row["CONCEPTO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString());
                                Row["COMENTARIO"] = Txt_Comentario.Text;
                                Usuario = (Registro[Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo].ToString());
                                Usuario = Usuario.Replace(".", "");
                                Row["USUARIO_CREO"] = Usuario;
                                Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                                Usuario = Usuario.Replace(".", "");
                                Row["USUARIO_RECHAZO"] = Usuario;
                                Dt_Reporte.Rows.Add(Row);
                                Dt_Reporte.AcceptChanges();
                            }
                            Ds_Reporte.Clear();
                            Ds_Reporte.Tables.Clear();
                            Ds_Reporte.Tables.Add(Dt_Reporte.Copy());
                            Reporte.Load(Ruta_Archivo + "Rpt_Con_Cancelacion_Recepcion.rpt");
                            Reporte.SetDataSource(Ds_Reporte);
                            DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                            Nombre_Archivo += ".pdf";
                            Ruta_Archivo = @Server.MapPath("../../Reporte/");
                            m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                            ExportOptions Opciones_Exportacion = new ExportOptions();
                            Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                            Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                            Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                            Reporte.Export(Opciones_Exportacion);
                            Abrir_Ventana(Nombre_Archivo);
                           // ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);   
                            ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "window.setTimeout('window.location.reload(true);',1);", true);
                        }
                        else
                        {
                            Trans.Rollback();
                            Recepcion_Documentos_Inicio();
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "No Hay suficiencia presupuestal por lo quie no se puede realizar la cancelacion";
                        }
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);   
                }
                else
                {
                    Recepcion_Documentos_Inicio();
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";

                }
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                Img_Error.Visible = true;
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                Img_Error.Visible = true;
            }
            finally
            {
                Cn.Close();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
        ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
        ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
        ///                             para mostrar los datos al usuario
        ///CREO       : Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO  : 21-Febrero-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        private void Abrir_Ventana(String Nombre_Archivo)
        {
            String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
            try
            {
                Pagina = Pagina + Nombre_Archivo;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception ex)
            {
                throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
            }
        }
        protected void Btn_Buscar_No_Solicitud_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Resultado = new DataTable();
            DataTable Dt_Modificado = new DataTable();
            DataTable Dt_Datos_Detalles = new DataTable();
            Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
            int Contador = 0;
            Session["Contador"] = Contador;
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
                {
                    Rs_Consultar_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
                }
                else { 
                    Rs_Consultar_Solicitud_Pagos.P_Estatus="PRE-EJERCIDO";
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes();
                }
                
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la busqueda <br>";
                    Txt_Busqueda_No_Solicitud.Focus();
                }
                else
                {
                    foreach (DataRow Fila in Dt_Resultado.Rows)
                    {
                        if (Dt_Modificado.Rows.Count <= 0)
                        {
                            Dt_Modificado.Columns.Add("No_Solicitud_Pago", typeof(System.String));
                            Dt_Modificado.Columns.Add("No_Reserva", typeof(System.String));
                            Dt_Modificado.Columns.Add("Tipo_Solicitud_Pago_ID", typeof(System.String));
                            Dt_Modificado.Columns.Add("Tipo_Pago", typeof(System.String));
                            Dt_Modificado.Columns.Add("Concepto", typeof(System.String));
                            Dt_Modificado.Columns.Add("Beneficiario", typeof(System.String));
                            Dt_Modificado.Columns.Add("Estatus", typeof(System.String));
                            Dt_Modificado.Columns.Add("Monto", typeof(System.Double));
                        }
                        DataRow row = Dt_Modificado.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        row["No_Solicitud_Pago"] = Fila["No_Solicitud_Pago"].ToString().Trim();
                        row["No_Reserva"] = Fila["No_Reserva"].ToString().Trim();
                        row["Tipo_Solicitud_Pago_ID"] = Fila["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                        row["Tipo_Pago"] = Fila["Tipo_Pago_Finanzas"].ToString().Trim();
                        row["Concepto"] = Fila["Concepto"].ToString().Trim();
                        if (!String.IsNullOrEmpty(Fila["Proveedor"].ToString().Trim()))
                        {
                            row["Beneficiario"] = Fila["Proveedor"].ToString().Trim();
                        }
                        else
                        {
                            row["Beneficiario"] = Fila["Empleado"].ToString().Trim();
                        }
                        row["Estatus"] = Fila["Estatus"].ToString().Trim();
                        row["Monto"] = Fila["Monto"].ToString().Trim();
                        Dt_Modificado.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Modificado.AcceptChanges();
                    }
                    Session["Dt_Datos_Detalles"] = Dt_Modificado;
                    Grid_Solicitud_Pagos.Columns[2].Visible = true;
                    Grid_Solicitud_Pagos.Columns[9].Visible = true;
                    Grid_Solicitud_Pagos.DataSource = Dt_Modificado;
                    Grid_Solicitud_Pagos.DataBind();
                    Grid_Solicitud_Pagos.Columns[2].Visible = false;
                    Grid_Solicitud_Pagos.Columns[9].Visible = false;
                    Txt_Busqueda_No_Solicitud.Text = "";
                }

            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        protected void Txt_Buscar_No_Solicitud_TextChanged(object sender, EventArgs e)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Resultado = new DataTable();
            DataTable Dt_Modificado = new DataTable();
            DataTable Dt_Datos_Detalles = new DataTable();
            Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
            int Contador = 0;
            Session["Contador"] = Contador;
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
                {
                    Rs_Consultar_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
                }
                else
                {
                    Rs_Consultar_Solicitud_Pagos.P_Estatus = "PRE-EJERCIDO";
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes();
                }

                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la busqueda <br>";
                    Txt_Busqueda_No_Solicitud.Focus();
                }
                else
                {
                    foreach (DataRow Fila in Dt_Resultado.Rows)
                    {
                        if (Dt_Modificado.Rows.Count <= 0)
                        {
                            Dt_Modificado.Columns.Add("No_Solicitud_Pago", typeof(System.String));
                            Dt_Modificado.Columns.Add("No_Reserva", typeof(System.String));
                            Dt_Modificado.Columns.Add("Tipo_Solicitud_Pago_ID", typeof(System.String));
                            Dt_Modificado.Columns.Add("Tipo_Pago", typeof(System.String));
                            Dt_Modificado.Columns.Add("Concepto", typeof(System.String));
                            Dt_Modificado.Columns.Add("Beneficiario", typeof(System.String));
                            Dt_Modificado.Columns.Add("Estatus", typeof(System.String));
                            Dt_Modificado.Columns.Add("Monto", typeof(System.Double));
                        }
                        DataRow row = Dt_Modificado.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        row["No_Solicitud_Pago"] = Fila["No_Solicitud_Pago"].ToString().Trim();
                        row["No_Reserva"] = Fila["No_Reserva"].ToString().Trim();
                        row["Tipo_Solicitud_Pago_ID"] = Fila["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                        row["Tipo_Pago"] = Fila["Tipo_Pago_Finanzas"].ToString().Trim();
                        row["Concepto"] = Fila["Concepto"].ToString().Trim();
                        if (!String.IsNullOrEmpty(Fila["Proveedor"].ToString().Trim()))
                        {
                            row["Beneficiario"] = Fila["Proveedor"].ToString().Trim();
                        }
                        else
                        {
                            row["Beneficiario"] = Fila["Empleado"].ToString().Trim();
                        }
                        row["Estatus"] = Fila["Estatus"].ToString().Trim();
                        row["Monto"] = Fila["Monto"].ToString().Trim();
                        Dt_Modificado.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Modificado.AcceptChanges();
                    }
                    Session["Dt_Datos_Detalles"] = Dt_Modificado;
                    Grid_Solicitud_Pagos.Columns[2].Visible = true;
                    Grid_Solicitud_Pagos.Columns[9].Visible = true;
                    Grid_Solicitud_Pagos.DataSource = Dt_Modificado;
                    Grid_Solicitud_Pagos.DataBind();
                    Grid_Solicitud_Pagos.Columns[2].Visible = false;
                    Grid_Solicitud_Pagos.Columns[9].Visible = false;
                    Txt_Busqueda_No_Solicitud.Text = "";
                }

            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        protected void Cmb_Forma_Pago_Lector_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Declaracion de variables
            int Cont_Elementos = 0; //variable para el contador
            DropDownList Cmb_Tipo_Pago_src; //Combo para el tipo de pago

            try
            {
                //Ciclo para el barrido de los elementos del grid
                for (Cont_Elementos = 0; Cont_Elementos < Grid_Solicitud_Pagos.Rows.Count; Cont_Elementos++)
                {
                    //instanciar el combo
                    Cmb_Tipo_Pago_src = ((DropDownList) Grid_Solicitud_Pagos.Rows[Cont_Elementos].Cells[4].FindControl("Cmb_Tipo_Pago"));

                    //Asignar el valor del combo
                    Cmb_Tipo_Pago_src.SelectedIndex = Cmb_Forma_Pago_Lector.SelectedIndex;
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message;
            }
        }
        protected void Cmb_Cuenta_Lector_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Declaracion de variables
            int Cont_Elementos = 0; //variable para el contador
            DropDownList Cmb_Cuenta_src; //variable para el combo de la cuenta

            try
            {
                //Ciclo para el barrido del grid
                for (Cont_Elementos = 0; Cont_Elementos < Grid_Solicitud_Pagos.Rows.Count; Cont_Elementos++)
                {
                    //Instanciar el combo
                    Cmb_Cuenta_src = ((DropDownList)Grid_Solicitud_Pagos.Rows[Cont_Elementos].Cells[6].FindControl("Cmb_Cuenta"));

                    //Asignar el valor del combo
                    Cmb_Cuenta_src.SelectedIndex = Cmb_Cuenta_Lector.SelectedIndex;
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message;
            }
        }
        #region GENERALES
            ///*********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
            ///DESCRIPCIÓN          : Evento del boton de salir
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Diciembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
                protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    if (Btn_Salir.ToolTip == "Inicio")
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                    else
                    {
                        Recepcion_Documentos_Inicio();
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
        #endregion

        #region"Grid"

                protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
                {
                    HyperLink Hyp_Lnk_Ruta;
                    try
                    {
                        if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                        {
                            Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                            if (!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) && e.Row.Cells[3].Text.Trim() != "&nbsp;")
                            {
                                Hyp_Lnk_Ruta.NavigateUrl = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[3].Text.Trim();
                                Hyp_Lnk_Ruta.Enabled = true;
                            }
                            else
                            {
                                Hyp_Lnk_Ruta.NavigateUrl = "";
                                Hyp_Lnk_Ruta.Enabled = false;
                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception(Ex.Message);
                    }
                }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Solicitud_Pagos_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Diciembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Grid_Solicitud_Pagos_SelectedIndexChanged(object sender, EventArgs e)
            {
                Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
                DataTable Dt_Detalles = new DataTable();
                DataTable Dt_Documentos = new DataTable();

                try
                {
                    if (Grid_Solicitud_Pagos.SelectedIndex > (-1))
                    {
                        Solicitud_Negocio.P_No_Solicitud_Pago = Grid_Solicitud_Pagos.SelectedRow.Cells[2].Text.Trim();
                        Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
                        Dt_Documentos = Solicitud_Negocio.Consulta_Documentos();

                        if (Dt_Detalles != null)
                        {
                            if (Dt_Detalles.Rows.Count > 0)
                            {
                                Txt_No_Pago_Det.Text = Dt_Detalles.Rows[0]["NO_SOLICITUD_PAGO"].ToString().Trim();
                                Txt_No_Reserva_Det.Text = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString().Trim();
                                Txt_Concepto_Reserva_Det.Text = Dt_Detalles.Rows[0]["CONCEPTO_RESERVA"].ToString().Trim();
                                Txt_Beneficiario_Det.Text = Dt_Detalles.Rows[0]["BENEFICIARIO"].ToString().Trim();
                                Txt_Fecha_Solicitud_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_SOLICITUD"]);
                                Txt_Monto_Solicitud_Det.Text = String.Format("{0:c}", Dt_Detalles.Rows[0]["MONTO"]);
                                Txt_Fecha_Autoriza_Director_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZO_RECHAZO_JEFE"]);
                                Txt_No_poliza_Det.Text = Dt_Detalles.Rows[0]["NO_POLIZA"].ToString().Trim();
                                Txt_Fecha_Recepcion_Doc_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECEPCION_DOCUMENTOS"]);
                                Txt_Recibio_Documentacion_Fisica.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_DOC_FISICA"].ToString().Trim();
                                Txt_Fecha_Recibio_Documentacion_Fisica.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECEPCION_DOCUMENTOS"]);
                                Txt_Aut_Documentacion_Fisica.Text = Dt_Detalles.Rows[0]["USUARIO_AUTORIZO_DOCUMENTOS"].ToString().Trim();
                                Txt_Fecha_Recepcion_Doc_Contabilidad.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECIBIO_CONTABILIDAD"]);
                                Txt_Recibio_Documentacion_Contabilidad.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_CONTABILIDAD"].ToString().Trim();
                                Txt_Aut_Contabilidad.Text = Dt_Detalles.Rows[0]["USUARIO_AUTORIZO_CONTABILIDAD"].ToString().Trim();
                                Txt_Fecha_Aut_Contabilidad.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZO_RECHAZO_CONTABI"]);
                                Txt_Recibio_Documentacion_Ejercido.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_EJERCIDO"].ToString().Trim();
                                Txt_Fecha_Recepcion_Doc_Ejercido.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECIBIO_EJERCIDO"]);
                                Txt_Aut_Ejercido.Text = Dt_Detalles.Rows[0]["USUARIO_AUTORIZO_EJERCIDO"].ToString().Trim();
                                Txt_Fecha_Aut_Ejercido.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZA_RECHAZA_EJD"]);
                                Grid_Solicitud_Pagos.SelectedIndex = -1;
                                if (String.IsNullOrEmpty(Txt_No_poliza_Det.Text))
                                {
                                    Tr_Poliza.Style.Add("Display", "none");
                                }
                                else
                                {
                                    Tr_Poliza.Style.Add("Display", "block");
                                }

                                Grid_Documentos.Columns[3].Visible = true;
                                Grid_Documentos.Columns[4].Visible = true;
                                Grid_Documentos.DataSource = Dt_Documentos;
                                Grid_Documentos.DataBind();
                                Grid_Documentos.Columns[3].Visible = false;
                                Grid_Documentos.Columns[4].Visible = false;
                                Mpe_Detalles.Show();
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
                }
            } 
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Grid_Solicitud_Pagos_RowDataBound
            /// DESCRIPCION : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 09/enero/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Grid_Solicitud_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DropDownList Cmb = new DropDownList();
                DropDownList Cmb2 = new DropDownList();
                DropDownList Cmb3 = new DropDownList();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                DataTable Ds_Modificada = new DataTable();
                DataTable Dt_Existencia = new DataTable();
                DataTable Dt_Consulta_Cuenta = new DataTable();
                DataTable DT_Contratos= new DataTable();
                Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
                Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
                Cls_Con_Cat_Contratos_Negocios Rs_Contratos = new Cls_Con_Cat_Contratos_Negocios();
                Cls_Cat_Nom_Bancos_Negocio Rs_Bancos= new Cls_Cat_Nom_Bancos_Negocio();
                Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
                DataTable Dt_Banco= new DataTable();
                DataTable Dt_Existencia_Cuenta = new DataTable();
                String No_Solicitud = "";
                int Contador;
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
                Label Lbl_Solicitud = new Label();
                Lbl_Solicitud = (Label)e.Row.FindControl("Lbl_Solicitud_Datos");
                try
                {
                    CheckBox chek = (CheckBox)e.Row.FindControl("Chk_Autorizado");
                    CheckBox chek2 = (CheckBox)e.Row.FindControl("Chk_Rechazar");
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Contador = (int)(Session["Contador"]);
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                        No_Solicitud = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["NO_SOLICITUD_PAGO"].ToString());
                        Rs_Solicitudes.P_No_Solicitud_Pago = No_Solicitud;
                        Ds_Consulta = Rs_Solicitudes.Consulta_Solicitudes_SinAutorizar();
                        foreach (DataRow Fila in Ds_Consulta.Rows)
                        {
                            if (Ds_Modificada.Rows.Count <= 0)
                            {
                                Ds_Modificada.Columns.Add("No_Solicitud_Pago", typeof(System.String));
                                Ds_Modificada.Columns.Add("No_Reserva", typeof(System.String));
                                Ds_Modificada.Columns.Add("Tipo_Solicitud_Pago_ID", typeof(System.String));
                                Ds_Modificada.Columns.Add("Tipo_Pago", typeof(System.String));
                                Ds_Modificada.Columns.Add("Concepto", typeof(System.String));
                                Ds_Modificada.Columns.Add("Beneficiario", typeof(System.String));
                                Ds_Modificada.Columns.Add("Estatus", typeof(System.String));
                                Ds_Modificada.Columns.Add("Monto", typeof(System.Double));
                            }
                            DataRow row = Ds_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            //Asigna los valores al nuevo registro creado a la tabla
                            row["No_Solicitud_Pago"] = Fila["No_Solicitud_Pago"].ToString().Trim();
                            row["No_Reserva"] = Fila["No_Reserva"].ToString().Trim();
                            row["Tipo_Solicitud_Pago_ID"] = Fila["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                            row["Tipo_Pago"] = Fila["Tipo_Pago_Finanzas"].ToString().Trim();
                            row["Concepto"] = Fila["Concepto"].ToString().Trim();
                            if (!String.IsNullOrEmpty(Fila["Proveedor"].ToString().Trim()))
                            {
                                row["Beneficiario"] = Fila["Proveedor"].ToString().Trim();
                            }
                            else
                            {
                                row["Beneficiario"] = Fila["Empleado"].ToString().Trim();
                            }
                            row["Estatus"] = Fila["Estatus"].ToString().Trim();
                            row["Monto"] = Fila["Monto"].ToString().Trim();
                            Ds_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Ds_Modificada.AcceptChanges();
                        }
                        Gv_Detalles = (GridView)e.Row.Cells[9].FindControl("Grid_Datos_Solicitud");
                        Gv_Detalles.Columns[1].Visible = true;
                        Gv_Detalles.DataSource = Ds_Modificada;
                        Gv_Detalles.DataBind();
                        Gv_Detalles.Columns[1].Visible = false;
                        Session["Contador"] = Contador + 1;
                        if (e.Row.Cells[9].Text != "PRE-EJERCIDO")
                        {
                            chek.Enabled = false;
                            chek2.Enabled = false;
                        }
                        else
                        {
                            chek.Enabled = true;
                            chek2.Enabled = true;
                        }
                        //llenar el combon de bancos
                        //DataTable Dt_Bancos = Bancos_Negocio.Consulta_Bancos();
                        Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
                        Cmb = (DropDownList)e.Row.Cells[5].FindControl("Cmb_Banco_Transferencia");
                        Cmb.DataSource = Dt_Existencia;
                        Cmb.DataTextField = "NOMBRE";
                        Cmb.DataValueField = "NOMBRE";
                        Cmb.DataBind();
                        Cmb.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                        Rs_Contratos.P_No_Reserva = Ds_Consulta.Rows[0][Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString();
                        DT_Contratos=Rs_Contratos.Consultar_Contratos();
                        if(DT_Contratos.Rows.Count>0){
                            Cmb2 = (DropDownList)e.Row.Cells[4].FindControl("Cmb_Tipo_Pago");
                            if(DT_Contratos.Rows[0][Cat_Con_Contratos.Campo_Forma_Pago].ToString()=="CHEQUE"){
                                Cmb2.SelectedValue="2";
                            }else{
                                // if(DT_Contratos.Rows[0][Cat_Con_Contratos.Campo_Forma_Pago].ToString()=="TRANSFERENCIA"){
                                     Cmb2.SelectedValue="1";
                                //}
                            }
                            Rs_Bancos.P_Banco_ID=DT_Contratos.Rows[0][Cat_Con_Contratos.Campo_Cuenta_Banco_Contratos_ID].ToString();
                            Dt_Banco = Rs_Bancos.Consulta_Bancos();
                            if (Dt_Banco.Rows.Count > 0)
                            {
                                Cmb3 = (DropDownList)e.Row.Cells[4].FindControl("Cmb_Cuenta");
                                Cmb.SelectedValue = Dt_Banco.Rows[0][Cat_Nom_Bancos.Campo_Nombre].ToString();
                                Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb.SelectedItem.Text.ToString();
                                Dt_Existencia_Cuenta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                                Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb3, Dt_Existencia_Cuenta, "CUENTA", "BANCO_ID");
                                Cmb3.SelectedValue = DT_Contratos.Rows[0][Cat_Con_Contratos.Campo_Cuenta_Banco_Contratos_ID].ToString();
                            }
                        }

                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
           ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Txt_Lector_Codigo_Barras_TextChanged
            ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado 
            ///PROPIEDADES          :
            ///CREO                 : Sergio Manuel Gallardo Andrade
            ///FECHA_CREO           : 02/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Txt_Lector_Codigo_Barras_TextChanged(object sender, EventArgs e)
            {
                String Accion = String.Empty;
                String No_Solicitud = String.Empty;
                String Comentario = String.Empty;
                String Documento = String.Empty;
                String Estatus;
                String Forma_Pago="";
                String Banco="";
                String Cuenta_Banco = "";
                DataTable Dt_solicitud = new DataTable();
                DataTable Dt_Consulta_Estatus = new DataTable();
                Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
                try
                    {
                    if (!String.IsNullOrEmpty(Txt_Lector_Codigo_Barras.Text))
                    {
                        No_Solicitud = Txt_Lector_Codigo_Barras.Text;//((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).CssClass;
                        if (No_Solicitud.Length < 10)
                        {
                            No_Solicitud = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                        }
                        Forma_Pago = Cmb_Forma_Pago_Lector.SelectedItem.Text; //((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Tipo_Pago")).SelectedItem.Text.ToString();
                        if (Cmb_Banco_Lector.SelectedIndex > 0)
                        {
                            Banco = Cmb_Banco_Lector.SelectedItem.Text;//((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Banco_Transferencia")).Text;
                        }
                        if (Cmb_Cuenta_Lector.SelectedIndex > 0)
                        {
                            Cuenta_Banco = Cmb_Cuenta_Lector.SelectedValue;
                        }
                       if (Banco != "" && Cuenta_Banco != "")
                        {
                            Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                            Dt_solicitud = Rs_Solicitud_Pago.Consultar_Solicitud_Pago();
                            if (Dt_solicitud.Rows.Count > 0)
                            {
                                Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                                if (Estatus == "PRE-EJERCIDO")
                                {
                                    SqlConnection Cn = new SqlConnection();
                                    SqlCommand Cmmd = new SqlCommand();
                                    SqlTransaction Trans = null;

                                    // crear transaccion para crear el convenio 
                                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                                    Cn.Open();
                                    Trans = Cn.BeginTransaction();
                                    Cmmd.Connection = Cn;
                                    Cmmd.Transaction = Trans;
                                    try
                                    {
                                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                        Rs_Solicitud_Pago.P_Estatus = "EJERCIDO";
                                        Rs_Solicitud_Pago.P_Forma_Pago = Forma_Pago;
                                        Rs_Solicitud_Pago.P_Cuenta_Banco_Pago = Cuenta_Banco;
                                        Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                        Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                        Rs_Solicitud_Pago.P_Usuario_Recibio_Pagado = Cls_Sessiones.Nombre_Empleado.ToString();
                                        Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                        Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                        Trans.Commit();
                                        ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "window.setTimeout('window.location.reload(true);',1);", true);
                                    }
                                    catch (SqlException Ex)
                                    {
                                        Trans.Rollback();
                                        Lbl_Mensaje_Error.Text = "Error:";
                                        Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                        Img_Error.Visible = true;
                                        Lbl_Mensaje_Error.Visible = true;
                                    }
                                    catch (Exception Ex)
                                    {
                                        Lbl_Mensaje_Error.Text = "Error:";
                                        Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                        Img_Error.Visible = true;
                                        Lbl_Mensaje_Error.Visible = true;
                                    }
                                    finally
                                    {
                                        Cn.Close();
                                    }

                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
                }
            }
    
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cmb_Banco_Transferencia_OnSelectedIndexChanged
            ///DESCRIPCIÓN          : Metodo para llenar el combo de cuentas por registro dependiendo la fila que se selecciono
            ///PROPIEDADES          :
            ///CREO                 : Sergio Manuel Gallardo Andrade
            ///FECHA_CREO           : 12/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Cmb_Banco_Transferencia_OnSelectedIndexChanged(object sender, EventArgs e)
            {

                Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
                DataTable Dt_Existencia = new DataTable();
                try
                {
                    //locate the row in which the dropdown value has been changed
                    GridViewRow gr =((GridViewRow)((DropDownList)sender).Parent.NamingContainer);
                    //find the control in that
                    LinkButton ss = (LinkButton)gr.FindControl("Btn_Solicitud");
                    DropDownList d1 = (DropDownList)gr.FindControl("Cmb_Banco_Transferencia");
                    string selectedvalue = d1.SelectedValue;

                    //using selectedvalue execute a query like 
                    //select * from product where brand_id=selectevalue
                    //get the result in datatable dt

                    //located the second dropdown(dd2)
                    DropDownList d2 = (DropDownList)gr.FindControl("Cmb_Cuenta");
                    Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = d1.SelectedItem.Text.ToString();
                    Dt_Existencia = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico(d2, Dt_Existencia, "CUENTA", "BANCO_ID");
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Autorizar_Solicitud
            ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado 
            ///PROPIEDADES          :
            ///CREO                 : Sergio Manuel Gallardo Andrade
            ///FECHA_CREO           : 02/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Autorizar_Solicitud(object sender, EventArgs e)
            {
                Int32 Indice = 0;
                String Estatus;
                String No_Solicitud;
                String Forma_Pago;
                String Banco="";
                String Cuenta_Banco="";
                Boolean Autorizado;
                DataTable Dt_solicitud = new DataTable();
                DataTable Dt_Consulta_Estatus = new DataTable();
                Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
                bool Mostrar_Error = false; //variable que indica que hay un error

                try
                {
                    foreach (GridViewRow Renglon_Grid in Grid_Solicitud_Pagos.Rows)
                    {
                        Indice++;
                        Grid_Solicitud_Pagos.SelectedIndex = Indice;
                        Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                        if (Autorizado)
                        {
                            No_Solicitud = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).CssClass;

                            //Verificar si esta seleccionado el tipo de pago
                            if (Cmb_Forma_Pago_Lector.SelectedIndex > 0)
                            {
                                Forma_Pago = Cmb_Forma_Pago_Lector.SelectedItem.Value;
                            }
                            else
                            {
                                Forma_Pago = ((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Tipo_Pago")).SelectedItem.Text.ToString();
                            }

                            //verificar si esta seleccionado un banco y su cuienta contable
                            if (Cmb_Banco_Lector.SelectedIndex > 0 && Cmb_Cuenta_Lector.SelectedIndex > 0)
                            {
                                Banco = Cmb_Banco_Lector.SelectedItem.Text;
                                Cuenta_Banco = Cmb_Cuenta_Lector.SelectedItem.Value;
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Banco_Transferencia")).Text))
                                {
                                    Banco = ((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Banco_Transferencia")).Text;
                                }
                                if (!String.IsNullOrEmpty(((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Cuenta")).SelectedValue))
                                {
                                    Cuenta_Banco = ((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Cuenta")).SelectedValue;
                                }
                            }

                            if (Banco != "" && Cuenta_Banco != "")
                            {
                                Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                Dt_solicitud = Rs_Solicitud_Pago.Consultar_Solicitud_Pago();
                                if (Dt_solicitud.Rows.Count > 0)
                                {
                                    Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                                    if (Estatus == "PRE-EJERCIDO")
                                    {
                                        SqlConnection Cn = new SqlConnection();
                                        SqlCommand Cmmd = new SqlCommand();
                                        SqlTransaction Trans = null;

                                        // crear transaccion para crear el convenio 
                                        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                                        Cn.Open();
                                        Trans = Cn.BeginTransaction();
                                        Cmmd.Connection = Cn;
                                        Cmmd.Transaction = Trans;
                                        try
                                        {
                                            Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                            Rs_Solicitud_Pago.P_Estatus = "EJERCIDO";
                                            Rs_Solicitud_Pago.P_Forma_Pago = Forma_Pago;
                                            Rs_Solicitud_Pago.P_Cuenta_Banco_Pago = Cuenta_Banco;
                                            Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                            Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                            Rs_Solicitud_Pago.P_Usuario_Recibio_Pagado = Cls_Sessiones.Nombre_Empleado.ToString();
                                            Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                            Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                            Trans.Commit();
                                        }
                                        catch (SqlException Ex)
                                        {
                                            Trans.Rollback();
                                            Lbl_Mensaje_Error.Text = "Error:";
                                            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                            Img_Error.Visible = true;
                                            Lbl_Mensaje_Error.Visible = true;
                                        }
                                        catch (Exception Ex)
                                        {
                                            Lbl_Mensaje_Error.Text = "Error:";
                                            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                            Img_Error.Visible = true;
                                            Lbl_Mensaje_Error.Visible = true;
                                        }
                                        finally
                                        {
                                            Cn.Close();
                                        }

                                    }
                                }
                            }
                            else
                            {
                                Lbl_Mensaje_Error.Text = "Favor de seleccionar el tipo de pago, el banco y la cuenta para hacer las actualizaciones.";
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                                Mostrar_Error = true;
                                break;
                            }
                        }
                    }

                    //Verificar si hubo un mensaje de error
                    if (Mostrar_Error == false)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "window.setTimeout('window.location.reload(true);',1);", true);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
                }
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Rechazar_Solicitud
            ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado 
            ///PROPIEDADES          :
            ///CREO                 : Sergio Manuel Gallardo Andrade
            ///FECHA_CREO           : 12/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Rechazar_Solicitud(object sender, EventArgs e)
            {
                Int32 Indice = 0;
                String Estatus;
                String No_Solicitud;
                String Forma_Pago;
                String Banco = "";
                String Cuenta_Banco = "";
                Boolean Autorizado;
                DataTable Dt_solicitud = new DataTable();
                DataTable Dt_Consulta_Estatus = new DataTable();
                Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
                try
                {
                    foreach (GridViewRow Renglon_Grid in Grid_Solicitud_Pagos.Rows)
                    {
                        Indice++;
                        Grid_Solicitud_Pagos.SelectedIndex = Indice;
                        Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                        if (Autorizado)
                        {
                            No_Solicitud = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).CssClass;
                            Forma_Pago = ((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Tipo_Pago")).SelectedItem.Text.ToString();
                            if (!String.IsNullOrEmpty(((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Banco_Transferencia")).Text))
                            {
                                Banco = ((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Banco_Transferencia")).Text;
                            }
                            if (!String.IsNullOrEmpty(((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Cuenta")).SelectedValue))
                            {
                                Cuenta_Banco = ((System.Web.UI.WebControls.DropDownList)Renglon_Grid.FindControl("Cmb_Cuenta")).SelectedValue;
                            }
                            if (Banco != "" && Cuenta_Banco != "")
                            {
                                Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                Dt_solicitud = Rs_Solicitud_Pago.Consultar_Solicitud_Pago();
                                if (Dt_solicitud.Rows.Count > 0)
                                {
                                    Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                                    if (Estatus == "PRE-EJERCIDO")
                                    {
                                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                        Rs_Solicitud_Pago.P_Estatus = "PRE-DOCUMENTADO";
                                        Rs_Solicitud_Pago.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                        Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                        Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                    }
                                }
                            }
                        }
                    }
                    ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "window.setTimeout('window.location.reload(true);',1);", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
                }
            }
        #endregion

    #endregion
    #region generar caratula
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Imprimir
            ///DESCRIPCIÓN: Imprime la solicitud
            ///PROPIEDADES:     
            ///CREO: Sergio Manuel Gallardo
            ///FECHA_CREO: 06/Enero/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Imprimir(String Numero_Solicitud)
            {
                DataSet Ds_Reporte = null;
                DataTable Dt_Pagos = null;
                try
                {
                    Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
                    Ds_Reporte = new DataSet();
                    Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
                    Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
                    if (Dt_Pagos.Rows.Count > 0)
                    {
                        Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                        Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                        //Se llama al método que ejecuta la operación de generar el reporte.
                        Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
                    }
                }
                //}
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                    Lbl_Mensaje_Error.Visible = true;
                }

            }
            /// *************************************************************************************
            /// NOMBRE:             Generar_Reporte
            /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
            ///              
            /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
            ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
            ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
            ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
            /// USUARIO CREO:       Juan Alberto Hernández Negrete.
            /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
            /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
            /// FECHA MODIFICO:     16/Mayo/2011
            /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
            ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
            /// *************************************************************************************
            public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
            {
                ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
                String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
                try
                {
                    Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
                    Reporte.Load(Ruta);

                    if (Ds_Reporte_Crystal is DataSet)
                    {
                        if (Ds_Reporte_Crystal.Tables.Count > 0)
                        {
                            Reporte.SetDataSource(Ds_Reporte_Crystal);
                            Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                            Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
                }
            }
            /// *************************************************************************************
            /// NOMBRE:             Exportar_Reporte_PDF
            /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
            ///                     especificada.
            /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
            ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
            /// USUARIO CREO:       Juan Alberto Hernández Negrete.
            /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA MODIFICACIÓN:
            /// *************************************************************************************
            public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
            {
                ExportOptions Opciones_Exportacion = new ExportOptions();
                DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

                try
                {
                    if (Reporte is ReportDocument)
                    {
                        Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                        Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                        Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                        Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                        Reporte.Export(Opciones_Exportacion);
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
                }
            }
            /// *************************************************************************************
            /// NOMBRE:              Mostrar_Reporte
            /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
            /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
            /// USUARIO CREO:        Juan Alberto Hernández Negrete.
            /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
            /// USUARIO MODIFICO:    Salvador Hernández Ramírez
            /// FECHA MODIFICO:      16-Mayo-2011
            /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
            /// *************************************************************************************
            protected void Mostrar_Reporte(String Nombre_Reporte)
            {
                String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

                try
                {
                    Pagina = Pagina + Nombre_Reporte;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                        "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
                }
            }
            #endregion
}
