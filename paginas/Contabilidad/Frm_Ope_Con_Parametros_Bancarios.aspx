﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Parametros_Bancarios.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Parametros_Bancarios" Title="Comisiones Bancos"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript" language="javascript">
        function calculo() {
            if (document.getElementById("<%=Txt_Comision.ClientID%>").value < 0) {
                document.getElementById("<%=Txt_Iva_Comision.ClientID%>").value = document.getElementById("<%=Txt_IVA_Parametro.ClientID%>").value;  //"0";
            } else {
            document.getElementById("<%=Txt_Iva_Comision.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Comision.ClientID%>").value * (document.getElementById("<%=Txt_IVA_Parametro.ClientID%>").value / 100)) * 100) / 100;
            }
        }
        function calculo2() {
            if (document.getElementById("<%=Txt_Comision_Spay.ClientID%>").value < 0) {
                document.getElementById("<%=Txt_IVA_Comision_Spay.ClientID%>").value = "0";
            } else {
            document.getElementById("<%=Txt_IVA_Comision_Spay.ClientID%>").value = Math.round((document.getElementById("<%=Txt_Comision_Spay.ClientID%>").value * (document.getElementById("<%=Txt_IVA_Parametro.ClientID%>").value / 100)) * 100) / 100;
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
   <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager>
       <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
             <ContentTemplate>
               <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div  class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                
            <div id="Div_General" runat="server"  style="background-color:#ffffff; width:100%; height:100%;"> <%--Fin del div General--%>
                    <table  width="98%" border="0" cellspacing="0" class="estilo_fuente" frame="border" >
                        <tr align="center">
                            <td class="label_titulo" colspan="2">Par&aacute;metros Bancarios
                            </td>
                       </tr>
                        <tr> <!--Bloque del mensaje de error-->
                            <td colspan="2">
                                <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                            </td>      
                        </tr>
                        
                        <tr class="barra_busqueda" align="right">
                            <td align="left">
                               <asp:ImageButton ID="Btn_Modificar" runat="server"  ToolTip="Modificar" CssClass="Img_Button" TabIndex="2"
                                    ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" />     
                                <asp:ImageButton ID="Btn_Salir" runat="server" 
                                    CssClass="Img_Button" 
                                    ToolTip="Salir"
                                    ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                    onclick="Btn_Salir_Click"/>
                             </td>
                             <td style="width:50%">B&uacute;squeda   
                                <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="100" TabIndex="5"  ToolTip = "Buscar por Nombre" Width="180px"/>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                        WatermarkText="<Ingrese nombre del banco>" TargetControlID="Txt_Busqueda" />
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" 
                                        runat="server" TargetControlID="Txt_Busqueda" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. "/>
                                 <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6"
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Consultar"
                                        onclick="Btn_Buscar_Click" />
                            </td>
                        </tr>
                    </table>
                
                    <div id="Div_Detalles" runat="server" style="overflow:auto;height:400px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:none">
                        <asp:UpdatePanel ID="Upnl_Detalle" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>             
                                <asp:Panel ID="Pnl_Detalle" runat="server" GroupingText="Datos Generales" Width="98%" BackColor="white">
                                    <table class="estilo_fuente" width="98%">
                                         <tr >
                                            <td style="width:15%"></td>
                                            <td style="width:25%"></td>
                                            <td style="width:15%" ></td>
                                            <td style="width:25%" ></td>
                                            <td style="width:20%" ></td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%" >
                                                <asp:Label ID="Lbl_Nombre_Banco" runat="server" Text="Nombre banco"></asp:Label>
                                            </td>
                                           <td>
                                                    <asp:TextBox ID="Txt_Nombre_Banco" runat="server" Width="75%" ReadOnly="true" ></asp:TextBox>
                                           </td>
                                           <td style="width:15%" >
                                                <asp:Label ID="Lbl_Clabe" runat="server" Text="Clabe Bancaria"></asp:Label>
                                            </td>
                                           <td>
                                                    <asp:TextBox ID="Txt_Clabe" runat="server" Width="75%" MaxLength="18"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="Filtered_Txt_Clabe" 
                                                    runat="server" TargetControlID="Txt_Clabe" FilterType="Numbers, Custom" 
                                                    ValidChars="0123456789"/>
                                           </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%" >
                                                <asp:Label ID="Lbl_No_Cuenta" runat="server" Text="No. Cuenta"></asp:Label>
                                            </td>
                                           <td>
                                                    <asp:TextBox ID="Txt_No_Cuenta" runat="server" Width="75%" MaxLength="20" ReadOnly="true"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Cuenta" 
                                                    runat="server" TargetControlID="Txt_No_Cuenta" FilterType="Numbers, Custom" 
                                                    ValidChars="0123456789"/>
                                           </td>
                                           <td>
                                           </td>
                                           <td>
                                           </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                            
                        <asp:UpdatePanel ID="Upnl_Generales_Poliza" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>             
                                <asp:Panel ID="Pnl_Datos_Comision" runat="server" GroupingText="Comision Por Transferencia" Width="98%" BackColor="white">
                                    <table width="100%" class="estilo_fuente">
                                        <tr>
                                            <td style="width:15%">
                                                <asp:Label ID="Lbl_Comision" runat="server" Text="Comisión"></asp:Label>
                                            </td>
                                            <td style="width:25%">
                                                <asp:TextBox ID="Txt_Comision" runat="server" Width="75%" MaxLength="20" TabIndex="1" OnKeyUp="javascript:calculo();">
                                                </asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="Txt_Comision_FilteredTextBoxExtender1" 
                                                    runat="server" TargetControlID="Txt_Comision" FilterType="Numbers, Custom" 
                                                    ValidChars="0123456789."/>
                                            </td>
                                            <td style="width:15%">
                                                <asp:Label ID="Lbl_Iva_Comision" runat="server" Text="IVA Comisión"></asp:Label>
                                            </td>
                                            <td style="width:25%">
                                                 <asp:TextBox ID="Txt_Iva_Comision" runat="server" Width="75%" MaxLength="20" TabIndex="2"></asp:TextBox>
                                                  <cc1:FilteredTextBoxExtender ID="Txt_Iva_Comision_FilteredTextBoxExtender1" 
                                                    runat="server" TargetControlID="Txt_Iva_Comision" FilterType="Numbers,Custom" 
                                                    ValidChars="0123456789."/>  
                                            </td>
                                            <td style="width:20%" >
                                                <asp:HiddenField ID="Txt_IVA_Parametro" runat="server"/>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="Pnl_Comision_Spay" runat="server" GroupingText="Comision Por S.P.E.I." Width="98%" BackColor="white">
                                    <table width="100%" class="estilo_fuente">
                                        <tr>
                                            <td style="width:15%">
                                                <asp:Label ID="Lbl_Comision_Spay" runat="server" Text="Comisión"></asp:Label>
                                            </td>
                                            <td style="width:25%">
                                                <asp:TextBox ID="Txt_Comision_Spay" runat="server" Width="75%" MaxLength="20" TabIndex="1" OnKeyUp="javascript:calculo2();">
                                                </asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comision_Spay" 
                                                    runat="server" TargetControlID="Txt_Comision_Spay" FilterType="Numbers, Custom" 
                                                    ValidChars="0123456789."/>
                                            </td>
                                            <td style="width:15%">
                                                <asp:Label ID="Lbl_IVA_Comision_Spay" runat="server" Text="IVA Comisión"></asp:Label>
                                            </td>
                                            <td style="width:25%">
                                                 <asp:TextBox ID="Txt_IVA_Comision_Spay" runat="server" Width="75%" MaxLength="20" TabIndex="2"></asp:TextBox>
                                                  <cc1:FilteredTextBoxExtender ID="FTE_Txt_IVA_Comision_Spay" 
                                                    runat="server" TargetControlID="Txt_IVA_Comision_Spay" FilterType="Numbers,Custom" 
                                                    ValidChars="0123456789."/>  
                                            </td>
                                            <td style="width:20%" >
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>        
                        </asp:UpdatePanel>
                    </div>
                    
                    
                    <table class="estilo_fuente" width="98%" >
                    
                         <%--<tr>
                            <td colspan="3" >
                                <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">--%>
                                <tr >  
                                    <td>
                                        <div id="Div_Grid" runat="server" style="overflow:auto;height:200px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:block">
                        
                                            <asp:GridView ID="Grid_Bancos" runat="server" Width="100%"
                                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                                 OnSelectedIndexChanged="Grid_Bancos_SelectedIndexChanged" >
                                                <Columns>   
                                                     <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                        <ItemStyle Width="10%" />
                                                    </asp:ButtonField>
                                                       <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" ItemStyle-Font-Size="Small">
                                                         <HeaderStyle HorizontalAlign="Left" Width="30%"   Font-Size="X-Small"/>
                                                         <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                       </asp:BoundField>
                                                        <asp:BoundField DataField="BANCO_CLABE" HeaderText="Clabe" ItemStyle-Font-Size="Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="20%" Font-Size="X-Small"/>
                                                           <ItemStyle HorizontalAlign="Center" Width="20%" />
                                                       </asp:BoundField>
                                                       <asp:BoundField DataField="COMISION_TRANSFERENCIA" HeaderText="Comisión Transferencia" ItemStyle-Font-Size="Small">
                                                         <HeaderStyle HorizontalAlign="Center" Width="20%" Font-Size="X-Small"/>
                                                         <ItemStyle HorizontalAlign="Center" Width="20%" />
                                                       </asp:BoundField>
                                                       <asp:BoundField DataField="IVA_COMISION" HeaderText="IVA Comisión" ItemStyle-Font-Size="Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="20%" Font-Size="X-Small"/>
                                                           <ItemStyle HorizontalAlign="Center" Width="20%" />
                                                       </asp:BoundField>
                                                       <asp:BoundField DataField="NO_CUENTA" />
                                                       <asp:BoundField DataField="COMISION_SPAY" />
                                                       <asp:BoundField DataField="IVA_SPAY" />
                                                </Columns>
                                                <SelectedRowStyle CssClass="GridSelected" />
                                                <PagerStyle CssClass="GridHeader" />
                                                <HeaderStyle CssClass="tblHead" />
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                            </asp:GridView> 
                                        </div>                         
                                    </td>  
                                </tr>                                
                        </table>
                </div>
            </ContentTemplate>
      </asp:UpdatePanel>
</asp:Content>