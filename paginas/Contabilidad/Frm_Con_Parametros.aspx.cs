﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Tipo_Polizas.Negocios;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;

public partial class paginas_Contabilidad_Frm_Con_Parametros : System.Web.UI.Page
{
    #region (Page_Load)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Page_Load
    /// DESCRIPCION : Carga la configuración inicial de los controles de la página.
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 15/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
                DataTable Dt_Cuentas = new DataTable();
                Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas = new Cls_Cat_Con_Cuentas_Contables_Negocio();
                Rs_Cuentas.P_Afectable = "SI";
                Dt_Cuentas = Rs_Cuentas.Consulta_Cuentas_Contables_Concatena();
                Cargar_Combos(Cmb_CTA_CAP, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_DIVO, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_EST_Y_PROY, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_ICIC, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_LAB_CONTROL_C, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_OBS, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_RAPCE, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_SEFUPU, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_Cedular, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_Cedular_Arren, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_Retencion_Iva, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_ISR, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_Honorarios_Asimilables, Dt_Cuentas);
                Cargar_Combos(Cmb_CTA_ISR_Arren, Dt_Cuentas);
                Cargar_Combos(Cmb_Devengado_Ing, Dt_Cuentas);
                Cargar_Combos(Cmb_Obras_Proceso, Dt_Cuentas);
                Cargar_Combos(Cmb_Cuenta_Baja_Activo, Dt_Cuentas);
                Cargar_Combos(Cmb_Cuenta_Comision_Bancaria, Dt_Cuentas);
                Cargar_Combos(Cmb_Cuenta_Bancos_Otros,Dt_Cuentas);
                Cargar_Combos(Cmb_Cuenta_por_Pagar, Dt_Cuentas);
                Cargar_Combos(Cmb_Deudor_Caja_Chica, Dt_Cuentas);
                Dt_Cuentas = new DataTable();
                Cls_Cat_Con_Tipo_Polizas_Negocio Rs_Tipo_Poliza = new Cls_Cat_Con_Tipo_Polizas_Negocio();
                Dt_Cuentas=Rs_Tipo_Poliza.Consulta_Tipos_Poliza();
                Cargar_Tipos_Poliza(Cmb_Tipo_Poliza_Baja, Dt_Cuentas);
                Dt_Cuentas = new DataTable();
                Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Tipo_Solicitud = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
                Rs_Tipo_Solicitud.P_Tipo_Comprobacion = "SI";
                Dt_Cuentas = Rs_Tipo_Solicitud.Consulta_Tipo_Solicitud_Pagos_Combo();
                Cargar_Tipo_Solicitudes(Cmb_Tipo_Revolvente,Dt_Cuentas);
                Cargar_Tipo_Solicitudes(Cmb_Tipo_Caja_Chica, Dt_Cuentas);
                Dt_Cuentas = new DataTable();
                Cls_Cat_Empleados_Negocios Rs_Empleados = new Cls_Cat_Empleados_Negocios();
                Dt_Cuentas = Rs_Empleados.Consulta_Empleados();
                Cargar_Combos_Usuarios(Cmb_Usuario_Autoriza_Gastos, Dt_Cuentas);
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Metodos)
    #region (Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 15/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        Cls_Cat_Con_Parametros_Negocio Rs_Consulta_Cat_Con_Parametros = new Cls_Cat_Con_Parametros_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Parametros; //Variable que obtendra los datos de la consulta 
        try
        {
            Limpia_Controles();             //Limpia los controles del forma
            Dt_Parametros = Rs_Consulta_Cat_Con_Parametros.Consulta_Datos_Parametros();//Consulta los datos generales de las Cuentas Contables dados de alta en la BD
            if (Dt_Parametros != null && Dt_Parametros.Rows.Count > 0)
            {
                Habilitar_Controles("Existe"); //Habilita solo los controles de Modificar debido a que ya existe un registro.
                Grid_Parametros.DataSource = Dt_Parametros;
                Grid_Parametros.DataBind();
                Session["Dt_Parametros"] = Dt_Parametros;

                Llenar_Combo_Fte_Financiamiento(String.Empty, "Municipal");
                Llenar_Combo_Fte_Financiamiento(String.Empty, "Ramo33");
                Llenar_Combo_Programa(String.Empty, "Municipal");
                Llenar_Combo_Programa(String.Empty, "Ramo33");
            }
            //else
            //{
            //    Session.Remove("Dt_Parametros");
            //    Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar                      
            //}
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 15/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Txt_Autoriza_Inv_Publicas.Text = "";
            Txt_Autoriza_Inv_Publicas_2.Text = "";
            Txt_ISR.Text = "";
            Txt_IVA.Text = "";
            Txt_Retencion_Iva.Text = "";
            Txt_Cedular.Text = "";
            Txt_Cap.Text = "";
            Txt_ICIC.Text = "";
            Txt_EST_PROY.Text = "";
            Txt_DIVO.Text = "";
            Txt_Iva_Inverciones.Text = "";
            Txt_Lab_Control_C.Text = "";
            Txt_OBS.Text = "";
            Txt_RAPCE.Text = "";
            Txt_SEFUPU.Text = "";
            Cmb_CTA_CAP.SelectedIndex = -1;
            Cmb_CTA_Cedular.SelectedIndex = -1;
            Cmb_CTA_DIVO.SelectedIndex = -1;
            Cmb_CTA_EST_Y_PROY.SelectedIndex = -1;
            Cmb_CTA_ICIC.SelectedIndex = -1;
            Cmb_CTA_ISR.SelectedIndex = -1;
            Cmb_CTA_LAB_CONTROL_C.SelectedIndex = -1;
            Cmb_CTA_OBS.SelectedIndex = -1;
            Cmb_CTA_RAPCE.SelectedIndex = -1;
            Cmb_CTA_Retencion_Iva.SelectedIndex = -1;
            Cmb_CTA_SEFUPU.SelectedIndex = -1;
            Txt_Buscar_Fte_Municipal.Text = String.Empty;
            Txt_Buscar_Fte_Ramo33.Text = String.Empty;
            Txt_Buscar_Programa_Municipal.Text = String.Empty;
            Txt_Buscar_Programa_Ramo33.Text = String.Empty;
            Txt_Devengado_Ing.Text = String.Empty;
            Txt_Obras_Proceso.Text = String.Empty;
            Cmb_Fte_Municipal.SelectedIndex = -1;
            Cmb_Fte_Ramo33.SelectedIndex = -1;
            Cmb_Programa_Municipal.SelectedIndex = -1;
            Cmb_Programa_Ramo33.SelectedIndex = -1;
            Cmb_Devengado_Ing.SelectedIndex = -1;
            Cmb_Obras_Proceso.SelectedIndex = -1;
            Cmb_CTA_Cedular_Arren.SelectedIndex = -1;
            Cmb_CTA_ISR_Arren.SelectedIndex = -1;
            Txt_Busqueda_Cedular_Arren.Text = String.Empty;
            Txt_Busqueda_ISR_Arren.Text = String.Empty;
            Cmb_CTA_Honorarios_Asimilables.SelectedIndex = -1;
            Txt_Busqueda_Honorarios_Asimilables.Text = String.Empty;
            Txt_Honorarios_Asimilables.Text = String.Empty;
            Txt_ISR_Arren.Text = String.Empty;
            Txt_Cedular_Arren.Text = String.Empty;
            Cmb_Cuenta_Baja_Activo.SelectedIndex = -1;
            Txt_Cuenta_Baja_Activo.Text = String.Empty;
            Cmb_Tipo_Poliza_Baja.SelectedIndex = -1;

            Txt_Busqueda_ISR.Text = "";
            Txt_Busqueda_ISR_Arren.Text = "";
            Txt_Busqueda.Text = "";
            Txt_Busqueda_Cedular.Text = "";
            Txt_Busqueda_Cedular_Arren.Text = "";
            Txt_Busqueda_Honorarios_Asimilables.Text = "";
            Txt_Cuenta_Baja_Activo.Text = "";
            Txt_Cuenta_Comision_Bancaria.Text = "";
            Cmb_Cuenta_Comision_Bancaria.SelectedIndex =-1;

            Cmb_Cuenta_por_Pagar.SelectedIndex = -1;
            Cmb_Cuenta_Bancos_Otros.SelectedIndex = -1;
            Txt_Cuenta_Bancos_Otros.Text = "";
            Txt_Cuenta_por_Pagar.Text = "";
            Cmb_Tipo_Revolvente.SelectedIndex = -1;
            Cmb_Tipo_Caja_Chica.SelectedIndex = -1;
            Txt_Monto_caja_Chica.Text = "";
            Txt_Deudor_Caja_Chica.Text = "";
            Cmb_Deudor_Caja_Chica.SelectedIndex = -1;
            Cmb_Usuario_Autoriza_Gastos.SelectedIndex = -1;
            Txt_Usuario_Autoriza_Gastos.Text = "";
            Txt_Dias_Comprobacion.Text = "";
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///                para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                           si es una alta, modificacion
    ///                           
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 15/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.Visible = false;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Configuracion_Acceso("Frm_Con_Parametros.aspx");
                    break;

                case "Existe":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    break;
            }
            Txt_Autoriza_Inv_Publicas.Enabled = Habilitado;
            Txt_Autoriza_Inv_Publicas_2.Enabled = Habilitado;
            Txt_ISR.Enabled = Habilitado;
            Txt_IVA.Enabled = Habilitado;
            Txt_Retencion_Iva.Enabled = Habilitado;
            Txt_Cedular.Enabled = Habilitado;
            Txt_Cap.Enabled = Habilitado;
            Txt_DIVO.Enabled = Habilitado;
            Txt_EST_PROY.Enabled = Habilitado;
            Txt_ICIC.Enabled = Habilitado;
            Txt_Iva_Inverciones.Enabled = Habilitado;
            Txt_Lab_Control_C.Enabled = Habilitado;
            Txt_OBS.Enabled = Habilitado;
            Txt_RAPCE.Enabled = Habilitado;
            Txt_SEFUPU.Enabled = Habilitado;
            Txt_Busqueda.Enabled = Habilitado;
            Txt_Busqueda_CAP.Enabled = Habilitado;
            Txt_Busqueda_Cedular.Enabled = Habilitado;
            Txt_Busqueda_DIVO.Enabled = Habilitado;
            Txt_Busqueda_ICIC.Enabled = Habilitado;
            Txt_Busqueda_ISR.Enabled = Habilitado;
            Txt_Busqueda_LAB.Enabled = Habilitado;
            Txt_Busqueda_OBS.Enabled = Habilitado;
            Txt_busqueda_PROY.Enabled = Habilitado;
            Txt_Busqueda_RAPCE.Enabled = Habilitado;
            Txt_Busqueda_SEFUPU.Enabled = Habilitado;
            Txt_Cuenta_Comision_Bancaria.Enabled = Habilitado;
            Btn_SEFUPU.Enabled = Habilitado;
            Btn_Busqueda_Cap.Enabled = Habilitado;
            Btn_Busqueda_Cedular.Enabled = Habilitado;
            Btn_Busqueda_DIVO.Enabled = Habilitado;
            Btn_Busqueda_ICIC.Enabled = Habilitado;
            Btn_Busqueda_ISR.Enabled = Habilitado;
            Btn_Busqueda_LAB.Enabled = Habilitado;
            Btn_Busqueda_OBS.Enabled = Habilitado;
            Btn_Busqueda_PROY.Enabled = Habilitado;
            Btn_Busqueda_RAPCE.Enabled = Habilitado;
            Btn_Buscar.Enabled = Habilitado;
            Btn_Buscar_Cuenta_Comision_Bancaria.Enabled = Habilitado;
            Cmb_CTA_CAP.Enabled = Habilitado;
            Cmb_CTA_DIVO.Enabled = Habilitado;
            Cmb_CTA_EST_Y_PROY.Enabled = Habilitado;
            Cmb_CTA_ICIC.Enabled = Habilitado;
            Cmb_CTA_LAB_CONTROL_C.Enabled = Habilitado;
            Cmb_CTA_OBS.Enabled = Habilitado;
            Cmb_CTA_RAPCE.Enabled = Habilitado;
            Cmb_CTA_SEFUPU.Enabled = Habilitado;
            Cmb_CTA_Retencion_Iva.Enabled = Habilitado;
            Cmb_CTA_ISR.Enabled = Habilitado;
            Cmb_CTA_Cedular.Enabled = Habilitado;
            Cmb_Cuenta_Comision_Bancaria.Enabled = Habilitado;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            Txt_Buscar_Fte_Municipal.Enabled = Habilitado;
            Txt_Buscar_Fte_Ramo33.Enabled = Habilitado;
            Txt_Buscar_Programa_Municipal.Enabled = Habilitado;
            Txt_Buscar_Programa_Ramo33.Enabled = Habilitado;
            Txt_Devengado_Ing.Enabled = Habilitado;
            Txt_Obras_Proceso.Enabled = Habilitado;
            Cmb_Fte_Municipal.Enabled = Habilitado;
            Cmb_Fte_Ramo33.Enabled = Habilitado;
            Cmb_Programa_Municipal.Enabled = Habilitado;
            Cmb_Programa_Ramo33.Enabled = Habilitado;
            Cmb_Devengado_Ing.Enabled = Habilitado;
            Cmb_Obras_Proceso.Enabled = Habilitado;
            Cmb_CTA_Cedular_Arren.Enabled = Habilitado;
            Cmb_CTA_ISR_Arren.Enabled = Habilitado;
            Txt_Busqueda_Cedular_Arren.Enabled = Habilitado;
            Txt_Busqueda_ISR_Arren.Enabled = Habilitado;
            Btn_Busqueda_Cedular_Arren.Enabled = Habilitado;
            Btn_Busqueda_ISR_Arren.Enabled = Habilitado;
            Cmb_CTA_Honorarios_Asimilables.Enabled = Habilitado;
            Txt_Busqueda_Honorarios_Asimilables.Enabled = Habilitado;
            Btn_Busqueda_Honorarios_Asimilables.Enabled = Habilitado;
            Txt_Honorarios_Asimilables.Enabled = Habilitado;
            Txt_ISR_Arren.Enabled = Habilitado;
            Txt_Cedular_Arren.Enabled = Habilitado;
            Txt_Cuenta_Baja_Activo.Enabled = Habilitado;
            Cmb_Cuenta_Baja_Activo.Enabled = Habilitado;
            Btn_Buscar_Cuenta_Baja_Activo.Enabled = Habilitado;
            Cmb_Tipo_Poliza_Baja.Enabled = Habilitado;
            Cmb_Cuenta_Bancos_Otros.Enabled = Habilitado;
            Cmb_Cuenta_por_Pagar.Enabled = Habilitado;
            Txt_Cuenta_por_Pagar.Enabled = Habilitado;
            Txt_Cuenta_Bancos_Otros.Enabled = Habilitado;
            Btn_Buscar_Cuenta_Bancos_Otros.Enabled = Habilitado;
            Btn_Buscar_Cuenta_por_Pagar.Enabled = Habilitado;
            Cmb_Tipo_Revolvente.Enabled = Habilitado;
            Cmb_Tipo_Caja_Chica.Enabled = Habilitado;
            Txt_Monto_caja_Chica.Enabled = Habilitado;
            Txt_Deudor_Caja_Chica.Enabled = Habilitado;
            Cmb_Deudor_Caja_Chica.Enabled = Habilitado;
            Cmb_Usuario_Autoriza_Gastos.Enabled = Habilitado;
            Txt_Dias_Comprobacion.Enabled = Habilitado;
            Txt_Usuario_Autoriza_Gastos.Enabled = Habilitado;
            Btn_Buscar_Usuario_Autoriza_Gastos.Enabled = Habilitado;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    ///*******************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 15/Septiembre/2011
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    ///*******************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 15/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region (Metodos Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos_Parametros
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 15/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos_Parametros()
    {
        String Espacios_Blanco;
        Cls_Cat_Con_Parametros_Negocio Empleado_Existe = new Cls_Cat_Con_Parametros_Negocio();
        DataTable Dt_Empleado = new DataTable();
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br />";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

        if (!String.IsNullOrEmpty(Txt_Autoriza_Inv_Publicas.Text.Trim()))
        {
            Empleado_Existe.P_No_Empleado = Txt_Autoriza_Inv_Publicas_2.Text.ToString();
            if (!String.IsNullOrEmpty(Empleado_Existe.P_No_Empleado))
            {
                Dt_Empleado = Empleado_Existe.Consulta_Empleados();
                if (Dt_Empleado.Rows.Count <= 0)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Segundo Usuario que ingresaste no existe. <br />";
                    Datos_Validos = false;
                }
            }

        }
        if (String.IsNullOrEmpty(Txt_Cedular.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El impuesto Cedular es un campo requerido. <br />";
            Datos_Validos = false;
        }
        if (String.IsNullOrEmpty(Txt_ISR.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El impuesto ISR  es un campo requerido. <br />";
            Datos_Validos = false;
        }
        if (String.IsNullOrEmpty(Txt_IVA.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El IVA es un campo requerido. <br />";
            Datos_Validos = false;
        }
        if (String.IsNullOrEmpty(Txt_Retencion_Iva.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + La Retención de IVA  es un campo requerido. <br />";
            Datos_Validos = false;
        }
        if (String.IsNullOrEmpty(Txt_Cedular_Arren.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + La Retencion Cedular de Arrendamiento es un campo requerido. <br />";
            Datos_Validos = false;
        }
        if (String.IsNullOrEmpty(Txt_ISR_Arren.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + La Retención de ISR de Arrendamiento  es un campo requerido. <br />";
            Datos_Validos = false;
        }
        if (String.IsNullOrEmpty(Txt_Honorarios_Asimilables.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Impuesto de Honorarios Asimilables es un campo requerido. <br />";
            Datos_Validos = false;
        }
        return Datos_Validos;
    }
    #endregion

    #region (Metodos Operacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Parametros
    /// DESCRIPCION : Da de Alta el Parametro con los datos proporcionados por 
    ///               el usuario
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 15/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Parametros()
    {
        Cls_Cat_Con_Parametros_Negocio Rs_Alta_Cat_Con_Parametros = new Cls_Cat_Con_Parametros_Negocio();  //Variable de conexión hacia la capa de Negocios

        try
        {
            if (!String.IsNullOrEmpty(Txt_Autoriza_Inv_Publicas.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_Usuario_Autoriza_Inv = Txt_Autoriza_Inv_Publicas.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Autoriza_Inv_Publicas_2.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_Usuario_Autoriza_Inv_2 = Txt_Autoriza_Inv_Publicas_2.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_ISR.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_ISR = Txt_ISR.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Cedular.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_Cedular = Txt_Cedular.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_IVA.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_IVA = Txt_IVA.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Retencion_Iva.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_Retencion_IVA = Txt_Retencion_Iva.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Cap.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_CAP = Txt_Cap.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_ICIC.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_ICIC = Txt_ICIC.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_RAPCE.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_RAPCE = Txt_RAPCE.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_DIVO.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_DIVO = Txt_DIVO.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_SEFUPU.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_SEFUPU = Txt_SEFUPU.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_OBS.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_OBS = Txt_OBS.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_EST_PROY.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_Est_Y_Proy = Txt_EST_PROY.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Lab_Control_C.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_Lab_Control_C = Txt_Lab_Control_C.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Iva_Inverciones.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_Iva_Inversiones = Txt_Iva_Inverciones.Text.Trim();
            }
            if (Cmb_CTA_CAP.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_CAP = Cmb_CTA_CAP.SelectedItem.Value;
            }
            if (Cmb_CTA_ICIC.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_ICIC = Cmb_CTA_ICIC.SelectedItem.Value;
            }
            if (Cmb_CTA_RAPCE.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_RAPCE = Cmb_CTA_RAPCE.SelectedItem.Value;
            }
            if (Cmb_CTA_DIVO.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_DIVO = Cmb_CTA_DIVO.SelectedItem.Value;
            }
            if (Cmb_CTA_SEFUPU.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_SEFUPU = Cmb_CTA_SEFUPU.SelectedItem.Value;
            }
            if (Cmb_CTA_OBS.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_OBS = Cmb_CTA_OBS.SelectedItem.Value;
            }
            if (Cmb_CTA_EST_Y_PROY.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_Est_Y_Proy = Cmb_CTA_EST_Y_PROY.SelectedItem.Value;
            }
            if (Cmb_CTA_LAB_CONTROL_C.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_Lab_Control_C = Cmb_CTA_LAB_CONTROL_C.SelectedItem.Value;
            }
            if (Cmb_CTA_ISR.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_ISR = Cmb_CTA_ISR.SelectedItem.Value;
            }
            if (Cmb_CTA_Cedular.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_Cedular = Cmb_CTA_Cedular.SelectedItem.Value;
            }
            if (Cmb_CTA_Retencion_Iva.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_Retencion_IVA = Cmb_CTA_Retencion_Iva.SelectedItem.Value;
            }

            if (Cmb_Fte_Municipal.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_Fte_Municipal = Cmb_Fte_Municipal.SelectedItem.Value.Trim();
            }
            else 
            {
                Rs_Alta_Cat_Con_Parametros.P_Fte_Municipal = String.Empty;
            }

            if (Cmb_Fte_Ramo33.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_Fte_Ramo33 = Cmb_Fte_Ramo33.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_Fte_Ramo33 = String.Empty;
            }

            if (Cmb_Programa_Municipal.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_Programa_Municipal = Cmb_Programa_Municipal.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_Programa_Municipal = String.Empty;
            }

            if (Cmb_Programa_Ramo33.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_Programa_Ramo33 = Cmb_Programa_Ramo33.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_Programa_Ramo33 = String.Empty;
            }
            if (Cmb_Devengado_Ing.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_Devengado_Ing = Cmb_Devengado_Ing.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_Devengado_Ing = String.Empty;
            }
            if (Cmb_Obras_Proceso.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_Obras_Proceso = Cmb_Obras_Proceso.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_Obras_Proceso = String.Empty;
            }
            if (Cmb_CTA_Cedular_Arren.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_Cedular_Arrendamiento = Cmb_CTA_Cedular_Arren.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_Cedular_Arrendamiento = String.Empty;
            }
            if (Cmb_CTA_Honorarios_Asimilables.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_Honorarios_Arrendamiento = Cmb_CTA_Honorarios_Asimilables.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_Honorarios_Arrendamiento = String.Empty;
            }
            if (Cmb_CTA_ISR_Arren.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_ISR_Arrendamiento = Cmb_CTA_ISR_Arren.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_ISR_Arrendamiento = String.Empty;
            }
            if (!String.IsNullOrEmpty(Txt_ISR_Arren.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_ISR_Arrendamiento_Monto = Txt_ISR_Arren.Text.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_ISR_Arrendamiento_Monto = "0";
            }
            if (!String.IsNullOrEmpty(Txt_Honorarios_Asimilables.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_Honorarios_Arrendamiento_Monto = Txt_Honorarios_Asimilables.Text.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_Honorarios_Arrendamiento_Monto = "0";
            }
            if (!String.IsNullOrEmpty(Txt_Cedular_Arren.Text))
            {
                Rs_Alta_Cat_Con_Parametros.P_Cedular_Arrendamiento_Monto = Txt_Cedular_Arren.Text.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_Cedular_Arrendamiento_Monto = "0";
            }
            if (Cmb_Cuenta_Baja_Activo.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_Baja_Activo = Cmb_Cuenta_Baja_Activo.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_Baja_Activo = String.Empty;
            }
            if (Cmb_Tipo_Poliza_Baja.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_Tipo_Poliza_Baja_ID = Cmb_Tipo_Poliza_Baja.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_Tipo_Poliza_Baja_ID = String.Empty;
            }

            //Verificar si esta la cuenta contable de la comision
            if (Cmb_Cuenta_Comision_Bancaria.SelectedIndex > 0)
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_COMISION_BANCARIA = Cmb_Cuenta_Comision_Bancaria.SelectedItem.Value;
            }
            else
            {
                Rs_Alta_Cat_Con_Parametros.P_CTA_COMISION_BANCARIA = string.Empty;
            }
            Rs_Alta_Cat_Con_Parametros.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            Rs_Alta_Cat_Con_Parametros.Alta_Parametros(); //Da de alto los datos del Parametro en la BD
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Parametros", "alert('El Alta de Parametros fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_Parametros " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Parametros
    /// DESCRIPCION : Modifica los datos del Parametro
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 21/Septiembre/2011
    /// MODIFICO          : Jorge L. Gonzalez Reyes
    /// FECHA_MODIFICO    : 15/Agosto/2012
    /// CAUSA_MODIFICACION: Se agregaron mas campos
    ///*******************************************************************************
    private void Modificar_Parametros()
    {
        Cls_Cat_Con_Parametros_Negocio Rs_Modificar_Cat_Con_Parametros = new Cls_Cat_Con_Parametros_Negocio(); //Variable de conexion a la capa de negocios
        Cls_Cat_Con_Parametros_Negocio Empleado_Existe = new Cls_Cat_Con_Parametros_Negocio();
        DataTable Dt_Empleado = new DataTable();
        Double Numero;
        try
        {
            if (!String.IsNullOrEmpty(Txt_Autoriza_Inv_Publicas.Text.Trim()))
            {
                Empleado_Existe.P_No_Empleado = Txt_Autoriza_Inv_Publicas.Text.ToString();
                Dt_Empleado = Empleado_Existe.Consulta_Empleados();
                if (Dt_Empleado.Rows.Count > 0)
                {
                    Rs_Modificar_Cat_Con_Parametros.P_Usuario_Autoriza_Inv = Dt_Empleado.Rows[0]["EMPLEADO_ID"].ToString();
                }

            }
            if (!String.IsNullOrEmpty(Txt_Autoriza_Inv_Publicas_2.Text.Trim()))
            {
                Empleado_Existe.P_No_Empleado = Txt_Autoriza_Inv_Publicas_2.Text.ToString();
                Dt_Empleado = Empleado_Existe.Consulta_Empleados();
                if (Dt_Empleado.Rows.Count > 0)
                {
                    Rs_Modificar_Cat_Con_Parametros.P_Usuario_Autoriza_Inv_2 = Dt_Empleado.Rows[0]["EMPLEADO_ID"].ToString();
                }

            }
            if (!String.IsNullOrEmpty(Txt_ISR.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_ISR = Txt_ISR.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Cedular.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_Cedular = Txt_Cedular.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_IVA.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_IVA = Txt_IVA.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Retencion_Iva.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_Retencion_IVA = Txt_Retencion_Iva.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Cap.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_CAP = Txt_Cap.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_ICIC.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_ICIC = Txt_ICIC.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_RAPCE.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_RAPCE = Txt_RAPCE.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_DIVO.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_DIVO = Txt_DIVO.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_SEFUPU.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_SEFUPU = Txt_SEFUPU.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_OBS.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_OBS = Txt_OBS.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_EST_PROY.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_Est_Y_Proy = Txt_EST_PROY.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Lab_Control_C.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_Lab_Control_C = Txt_Lab_Control_C.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Iva_Inverciones.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_Iva_Inversiones = Txt_Iva_Inverciones.Text.Trim();
            }

            if (Cmb_CTA_CAP.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_CAP = Cmb_CTA_CAP.SelectedItem.Value;
            }
            if (Cmb_CTA_ICIC.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_ICIC = Cmb_CTA_ICIC.SelectedItem.Value;
            }
            if (Cmb_CTA_RAPCE.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_RAPCE = Cmb_CTA_RAPCE.SelectedItem.Value;
            }
            if (Cmb_CTA_DIVO.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_DIVO = Cmb_CTA_DIVO.SelectedItem.Value;
            }
            if (Cmb_CTA_SEFUPU.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_SEFUPU = Cmb_CTA_SEFUPU.SelectedItem.Value;
            }
            if (Cmb_CTA_OBS.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_OBS = Cmb_CTA_OBS.SelectedItem.Value;
            }
            if (Cmb_CTA_EST_Y_PROY.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_Est_Y_Proy = Cmb_CTA_EST_Y_PROY.SelectedItem.Value;
            }
            if (Cmb_CTA_LAB_CONTROL_C.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_Lab_Control_C = Cmb_CTA_LAB_CONTROL_C.SelectedItem.Value;
            }

            if (Cmb_CTA_ISR.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_ISR = Cmb_CTA_ISR.SelectedItem.Value;
            }
            if (Cmb_CTA_Cedular.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_Cedular = Cmb_CTA_Cedular.SelectedItem.Value;
            }
            if (Cmb_CTA_Retencion_Iva.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_Retencion_IVA = Cmb_CTA_Retencion_Iva.SelectedItem.Value;
            }

            if (Cmb_Fte_Municipal.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Fte_Municipal = Cmb_Fte_Municipal.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Fte_Municipal = String.Empty;
            }

            if (Cmb_Fte_Ramo33.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Fte_Ramo33 = Cmb_Fte_Ramo33.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Fte_Ramo33 = String.Empty;
            }

            if (Cmb_Programa_Municipal.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Programa_Municipal = Cmb_Programa_Municipal.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Programa_Municipal = String.Empty;
            }

            if (Cmb_Programa_Ramo33.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Programa_Ramo33 = Cmb_Programa_Ramo33.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Programa_Ramo33 = String.Empty;
            }

            if (Cmb_Devengado_Ing.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Devengado_Ing = Cmb_Devengado_Ing.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Devengado_Ing = String.Empty;
            }

            if (Cmb_Obras_Proceso.SelectedIndex>=1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Obras_Proceso = Cmb_Obras_Proceso.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Obras_Proceso = String.Empty;
            }

            if (Cmb_CTA_Honorarios_Asimilables.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Honorarios_Arrendamiento = Cmb_CTA_Honorarios_Asimilables.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Honorarios_Arrendamiento = String.Empty;
            }

            if (Cmb_CTA_ISR_Arren.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_ISR_Arrendamiento = Cmb_CTA_ISR_Arren.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_ISR_Arrendamiento = String.Empty;
            }

            if (Cmb_CTA_Cedular_Arren.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Cedular_Arrendamiento = Cmb_CTA_Cedular_Arren.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Cedular_Arrendamiento = String.Empty;
            }
            if (!String.IsNullOrEmpty(Txt_Honorarios_Asimilables.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_Honorarios_Arrendamiento_Monto = Txt_Honorarios_Asimilables.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Cedular_Arren.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_Cedular_Arrendamiento_Monto = Txt_Cedular_Arren.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_ISR_Arren.Text.Trim()))
            {
                Rs_Modificar_Cat_Con_Parametros.P_ISR_Arrendamiento_Monto = Txt_ISR_Arren.Text.Trim();
            }
            if (Cmb_Cuenta_Baja_Activo.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_Baja_Activo = Cmb_Cuenta_Baja_Activo.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_Baja_Activo = String.Empty;
            }
            if (Cmb_Tipo_Poliza_Baja.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Tipo_Poliza_Baja_ID = Cmb_Tipo_Poliza_Baja.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Tipo_Poliza_Baja_ID = String.Empty;
            }

            if (Cmb_Cuenta_Comision_Bancaria.SelectedIndex > 0)
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_COMISION_BANCARIA = Cmb_Cuenta_Comision_Bancaria.SelectedItem.Value;
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_CTA_COMISION_BANCARIA = String.Empty;
            }
            if (Cmb_Tipo_Revolvente.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Tipo_Solicitud_Revolvente = Cmb_Tipo_Revolvente.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Tipo_Solicitud_Revolvente = String.Empty;
            }
            if (Cmb_Tipo_Caja_Chica.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Tipo_Caja_Chica = Cmb_Tipo_Caja_Chica.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Tipo_Caja_Chica = String.Empty;
            }
            if (Cmb_Cuenta_por_Pagar.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Cuenta_Por_Pagar_Deudor = Cmb_Cuenta_por_Pagar.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Cuenta_Por_Pagar_Deudor = String.Empty;
            }
            if (Cmb_Cuenta_Bancos_Otros.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Cuenta_Banco_Otros = Cmb_Cuenta_Bancos_Otros.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Cuenta_Banco_Otros = String.Empty;
            } 
            if (Cmb_Deudor_Caja_Chica.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Deudor_Caja_Chica = Cmb_Deudor_Caja_Chica.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Deudor_Caja_Chica = String.Empty;
            }
            if (!string.IsNullOrEmpty(Txt_Monto_caja_Chica.Text))
            {
                Boolean Numero_Real =Double.TryParse(Txt_Monto_caja_Chica.Text, out Numero);
                if (Numero_Real)
                {
                    Rs_Modificar_Cat_Con_Parametros.P_Monto_Caja_Chica = Txt_Monto_caja_Chica.Text;
                }
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Monto_Caja_Chica = "0";
            }
            if (Cmb_Usuario_Autoriza_Gastos.SelectedIndex >= 1)
            {
                Rs_Modificar_Cat_Con_Parametros.P_Usuario_Autoriza_Gastos = Cmb_Usuario_Autoriza_Gastos.SelectedItem.Value.Trim();
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Deudor_Caja_Chica = "";
            }
            if (!string.IsNullOrEmpty(Txt_Dias_Comprobacion.Text))
            {
                Boolean Numero_Real = Double.TryParse(Txt_Dias_Comprobacion.Text, out Numero);
                if (Numero_Real)
                {
                    Rs_Modificar_Cat_Con_Parametros.P_Dias_Comprobacion = Txt_Dias_Comprobacion.Text;
                }
            }
            else
            {
                Rs_Modificar_Cat_Con_Parametros.P_Dias_Comprobacion = "0";
            }
            Rs_Modificar_Cat_Con_Parametros.P_Usuario_Modifico = Cls_Sessiones.No_Empleado;
            Rs_Modificar_Cat_Con_Parametros.Modificar_Parametros_Autorizacion();//Modifica el registro del parametro de la mascara de la cuenta contable con los datos proporcionados por el usuario
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Parametros", "alert('La Modificación de Parámetros fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Modificar_Parametros " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region (Metodos Consulta)
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combos
    ///DESCRIPCIÓN: Metodo usado para cargar la informacion de los combos
    ///PARAMETROS: 
    ///CREO: Jorge L. Gonzalez REyes
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Combos(DropDownList Cmb_Cuentas, DataTable Dt_Cuentas)
    {
        try
        {
            Cmb_Cuentas.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
            Cmb_Cuentas.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
            Cmb_Cuentas.DataSource = Dt_Cuentas;
            Cmb_Cuentas.DataBind();
            Cmb_Cuentas.Items.Insert(0, new ListItem("<- SELECCIONE ->", "0"));

        }
        catch (Exception ex)
        {
            throw new Exception("Cargar combo Parametros " + ex.Message.ToString(), ex);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combos_Usuarios
    ///DESCRIPCIÓN: Metodo usado para cargar la informacion de los combos
    ///PARAMETROS: 
    ///CREO: Jorge L. Gonzalez REyes
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Combos_Usuarios(DropDownList Cmb_Cuentas, DataTable Dt_Cuentas)
    {
        try
        {
            Cmb_Cuentas.DataTextField = "EMPLEADO";
            Cmb_Cuentas.DataValueField = Cat_Empleados.Campo_Empleado_ID;
            Cmb_Cuentas.DataSource = Dt_Cuentas;
            Cmb_Cuentas.DataBind();
            Cmb_Cuentas.Items.Insert(0, new ListItem("<- SELECCIONE ->", "0"));

        }
        catch (Exception ex)
        {
            throw new Exception("Cargar combo Parametros " + ex.Message.ToString(), ex);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Tipos_Poliza
    ///DESCRIPCIÓN: Metodo usado para cargar la informacion de los combos
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 06/marzo/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Tipos_Poliza(DropDownList Cmb_Cuentas, DataTable Dt_Cuentas)
    {
        try
        {
            Cmb_Cuentas.DataTextField = Cat_Con_Tipo_Polizas.Campo_Descripcion;
            Cmb_Cuentas.DataValueField = Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID ;
            Cmb_Cuentas.DataSource = Dt_Cuentas;
            Cmb_Cuentas.DataBind();
            Cmb_Cuentas.Items.Insert(0, new ListItem("<- SELECCIONE ->", "0"));

        }
        catch (Exception ex)
        {
            throw new Exception("Cargar combo Parametros " + ex.Message.ToString(), ex);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Tipo_Solicitudes
    ///DESCRIPCIÓN: Metodo usado para cargar la informacion de los combos
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 06/Agosto/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Tipo_Solicitudes(DropDownList Cmb_Cuentas, DataTable Dt_Cuentas)
    {
        try
        {
            Cmb_Cuentas.DataTextField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion;
            Cmb_Cuentas.DataValueField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
            Cmb_Cuentas.DataSource = Dt_Cuentas;
            Cmb_Cuentas.DataBind();
            Cmb_Cuentas.Items.Insert(0, new ListItem("<- SELECCIONE ->", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("Cargar combo Parametros " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #endregion

    #region (Eventos)
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Limpia_Controles();           //Limpia los controles de la forma para poder introducir nuevos datos
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
            }
            else
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                //Valida si todos los campos requeridos estan llenos si es así da de alta los datos en la base de datos
                if (Validar_Datos_Parametros())
                {
                    Alta_Parametros(); //Da de alta el Tipo de Poliza con los datos que proporciono el usuario
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                if (Grid_Parametros.SelectedIndex >= 0)
                {
                    Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Es necesario seleccinar el registro para modificarlo";
                }
            }
            else
            {
                Modificar_Parametros();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_Retencion_Iva.SelectedIndex = Cmb_CTA_Retencion_Iva.Items.IndexOf(Cmb_CTA_Retencion_Iva.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_ISR_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_ISR_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_ISR.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_ISR.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_ISR.SelectedIndex = Cmb_CTA_ISR.Items.IndexOf(Cmb_CTA_ISR.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_ISR_Arren_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_ISR_Arren_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_ISR_Arren.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_ISR_Arren.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_ISR_Arren.SelectedIndex = Cmb_CTA_ISR_Arren.Items.IndexOf(Cmb_CTA_ISR_Arren.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Cedular_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Cedular_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_Cedular.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_Cedular.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_Cedular.SelectedIndex = Cmb_CTA_Cedular.Items.IndexOf(Cmb_CTA_Cedular.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Cedular_Arren_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Cedular_Arren_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_Cedular_Arren.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_Cedular_Arren.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_Cedular_Arren.SelectedIndex = Cmb_CTA_Cedular_Arren.Items.IndexOf(Cmb_CTA_Cedular_Arren.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Honorarios_Asimilables_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Honorarios_Asimilables_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_Honorarios_Asimilables.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_Honorarios_Asimilables.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_Honorarios_Asimilables.SelectedIndex = Cmb_CTA_Honorarios_Asimilables.Items.IndexOf(Cmb_CTA_Honorarios_Asimilables.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Cap_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Cap_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_CAP.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_CAP.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_CAP.SelectedIndex = Cmb_CTA_CAP.Items.IndexOf(Cmb_CTA_CAP.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_ICIC_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_ICIC_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_ICIC.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_ICIC.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_ICIC.SelectedIndex = Cmb_CTA_ICIC.Items.IndexOf(Cmb_CTA_ICIC.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_RAPCE_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_RAPCE_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_RAPCE.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_RAPCE.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_RAPCE.SelectedIndex = Cmb_CTA_RAPCE.Items.IndexOf(Cmb_CTA_RAPCE.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_DIVO_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_DIVO_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_DIVO.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_DIVO.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_DIVO.SelectedIndex = Cmb_CTA_DIVO.Items.IndexOf(Cmb_CTA_DIVO.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_SEFUPU_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_SEFUPU_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_SEFUPU.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_SEFUPU.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_SEFUPU.SelectedIndex = Cmb_CTA_SEFUPU.Items.IndexOf(Cmb_CTA_SEFUPU.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_OBS_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_OBS_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_OBS.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_OBS.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_OBS.SelectedIndex = Cmb_CTA_OBS.Items.IndexOf(Cmb_CTA_OBS.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_PROY_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_PROY_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_busqueda_PROY.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_busqueda_PROY.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_EST_Y_PROY.SelectedIndex = Cmb_CTA_EST_Y_PROY.Items.IndexOf(Cmb_CTA_EST_Y_PROY.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_LAB_Click
    ///DESCRIPCIÓN: Metodo para buscar una cuenta con filtro
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_LAB_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda_LAB.Text))
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuenta_con = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable DT_Busqueda;

            Cuenta_con.P_Descripcion = Txt_Busqueda_LAB.Text.ToUpper();
            DT_Busqueda = Cuenta_con.Consulta_Busqueda_Cuenta();
            if (DT_Busqueda.Rows.Count > 0)
            {
                Cmb_CTA_LAB_CONTROL_C.SelectedIndex = Cmb_CTA_LAB_CONTROL_C.Items.IndexOf(Cmb_CTA_LAB_CONTROL_C.Items.FindByValue(DT_Busqueda.Rows[0][0].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron cuentas que coincidan con la busqueda.";
            }
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Boton para salir de la forma
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 14/Agosto/2012 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                Session.Remove("Dt_Parametros");
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Grid)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Parametros_SelectedIndexChanged
    /// DESCRIPCION : Consulta los datos del Parametro seleccionado por el usuario
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 21/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Parametros_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable Dt_Parametros = new DataTable(); //Variable que obtendra los datos de la consulta 

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Limpia_Controles(); //Limpia los controles del la forma para poder agregar los valores del registro seleccionado

            Dt_Parametros = (DataTable)Session["Dt_Parametros"];
            if (Dt_Parametros != null && Dt_Parametros.Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv].ToString()))
                    Txt_Autoriza_Inv_Publicas.Text = Dt_Parametros.Rows[0]["usuario1"].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2].ToString()))
                    Txt_Autoriza_Inv_Publicas_2.Text = Dt_Parametros.Rows[0]["usuario2"].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_IVA].ToString()))
                    Txt_IVA.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_IVA].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_ISR].ToString()))
                    Txt_ISR.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_ISR].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cedular].ToString()))
                    Txt_Cedular.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cedular].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Retencion_IVA].ToString()))
                    Txt_Retencion_Iva.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Retencion_IVA].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Iva_Inversiones].ToString()))
                    Txt_Iva_Inverciones.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Iva_Inversiones].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CAP].ToString()))
                    Txt_Cap.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CAP].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_ICIC].ToString()))
                    Txt_ICIC.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_ICIC].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_RAPCE].ToString()))
                    Txt_RAPCE.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_RAPCE].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_DIVO].ToString()))
                    Txt_DIVO.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_DIVO].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_SEFUPU].ToString()))
                    Txt_SEFUPU.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_SEFUPU].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_OBS].ToString()))
                    Txt_OBS.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_OBS].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Est_Y_Proy].ToString()))
                    Txt_EST_PROY.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Est_Y_Proy].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Lab_Control_C].ToString()))
                    Txt_Lab_Control_C.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Lab_Control_C].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_CAP].ToString()))
                    Cmb_CTA_CAP.SelectedIndex = Cmb_CTA_CAP.Items.IndexOf(Cmb_CTA_CAP.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_CAP].ToString()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_ICIC].ToString()))
                    Cmb_CTA_ICIC.SelectedIndex = Cmb_CTA_ICIC.Items.IndexOf(Cmb_CTA_ICIC.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_ICIC].ToString()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_RAPCE].ToString()))
                    Cmb_CTA_RAPCE.SelectedIndex = Cmb_CTA_RAPCE.Items.IndexOf(Cmb_CTA_RAPCE.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_RAPCE].ToString()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_DIVO].ToString()))
                    Cmb_CTA_DIVO.SelectedIndex = Cmb_CTA_DIVO.Items.IndexOf(Cmb_CTA_DIVO.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_DIVO].ToString()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_SEFUPU].ToString()))
                    Cmb_CTA_SEFUPU.SelectedIndex = Cmb_CTA_SEFUPU.Items.IndexOf(Cmb_CTA_SEFUPU.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_SEFUPU].ToString()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_OBS].ToString()))
                    Cmb_CTA_OBS.SelectedIndex = Cmb_CTA_OBS.Items.IndexOf(Cmb_CTA_OBS.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_OBS].ToString()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Est_Y_Proy].ToString()))
                    Cmb_CTA_EST_Y_PROY.SelectedIndex = Cmb_CTA_EST_Y_PROY.Items.IndexOf(Cmb_CTA_EST_Y_PROY.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Est_Y_Proy].ToString()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Lab_Control_C].ToString()))
                    Cmb_CTA_LAB_CONTROL_C.SelectedIndex = Cmb_CTA_LAB_CONTROL_C.Items.IndexOf(Cmb_CTA_LAB_CONTROL_C.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Lab_Control_C].ToString()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_ISR].ToString()))
                    Cmb_CTA_ISR.SelectedIndex = Cmb_CTA_ISR.Items.IndexOf(Cmb_CTA_ISR.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_ISR].ToString()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Cedular].ToString()))
                    Cmb_CTA_Cedular.SelectedIndex = Cmb_CTA_Cedular.Items.IndexOf(Cmb_CTA_Cedular.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Cedular].ToString()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Retencion_IVA].ToString()))
                    Cmb_CTA_Retencion_Iva.SelectedIndex = Cmb_CTA_Retencion_Iva.Items.IndexOf(Cmb_CTA_Retencion_Iva.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Retencion_IVA].ToString()));

                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Fte_Financiamiento_ID_Ingresos].ToString().Trim()))
                    Cmb_Fte_Municipal.SelectedIndex = Cmb_Fte_Municipal.Items.IndexOf(Cmb_Fte_Municipal.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Fte_Financiamiento_ID_Ingresos].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Programa_ID_Ingresos].ToString().Trim()))
                    Cmb_Programa_Municipal.SelectedIndex = Cmb_Programa_Municipal.Items.IndexOf(Cmb_Programa_Municipal.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Programa_ID_Ingresos].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Fte_Financiamiento_ID_Ramo33].ToString().Trim()))
                    Cmb_Fte_Ramo33.SelectedIndex = Cmb_Fte_Ramo33.Items.IndexOf(Cmb_Fte_Ramo33.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Fte_Financiamiento_ID_Ramo33].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Programa_ID_Ramo33].ToString().Trim()))
                    Cmb_Programa_Ramo33.SelectedIndex = Cmb_Programa_Ramo33.Items.IndexOf(Cmb_Programa_Ramo33.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Programa_ID_Ramo33].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Devengado_Ing].ToString().Trim()))
                    Cmb_Devengado_Ing.SelectedIndex = Cmb_Devengado_Ing.Items.IndexOf(Cmb_Devengado_Ing.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Devengado_Ing].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Obras_Proceso].ToString().Trim()))
                    Cmb_Obras_Proceso.SelectedIndex = Cmb_Obras_Proceso.Items.IndexOf(Cmb_Obras_Proceso.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Obras_Proceso].ToString().Trim()));
                
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Honorarios_Asimilables].ToString().Trim()))
                    Cmb_CTA_Honorarios_Asimilables.SelectedIndex = Cmb_CTA_Honorarios_Asimilables.Items.IndexOf(Cmb_CTA_Honorarios_Asimilables.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Honorarios_Asimilables].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_ISR_Arrendamiento].ToString().Trim()))
                    Cmb_CTA_ISR_Arren.SelectedIndex = Cmb_CTA_ISR_Arren.Items.IndexOf(Cmb_CTA_ISR_Arren.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_ISR_Arrendamiento].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cedular_Arrendamiento].ToString().Trim()))
                    Cmb_CTA_Cedular_Arren.SelectedIndex = Cmb_CTA_Cedular_Arren.Items.IndexOf(Cmb_CTA_Cedular_Arren.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cedular_Arrendamiento].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_ISR_Arrendamiento_Monto].ToString()))
                    Txt_ISR_Arren.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_ISR_Arrendamiento_Monto].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Honorarios_Asimilables_Monto].ToString()))
                    Txt_Honorarios_Asimilables.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Honorarios_Asimilables_Monto].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cedular_Arrendamiento_Monto].ToString()))
                    Txt_Cedular_Arren.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cedular_Arrendamiento_Monto].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Baja_Activo_ID].ToString()))
                    Cmb_Cuenta_Baja_Activo.SelectedIndex = Cmb_Cuenta_Baja_Activo.Items.IndexOf(Cmb_Cuenta_Baja_Activo.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Baja_Activo_ID].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Poliza_Baja_ID].ToString()))
                    Cmb_Tipo_Poliza_Baja.SelectedIndex = Cmb_Tipo_Poliza_Baja.Items.IndexOf(Cmb_Tipo_Poliza_Baja.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Poliza_Baja_ID].ToString().Trim()));
                if (string.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Comision_Bancaria].ToString().Trim()) == false)
                {
                    Cmb_Cuenta_Comision_Bancaria.SelectedIndex = Cmb_Cuenta_Comision_Bancaria.Items.IndexOf(Cmb_Cuenta_Comision_Bancaria.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_CTA_Comision_Bancaria].ToString().Trim()));
                }
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Banco_Otros_Deudor].ToString()))
                    Cmb_Cuenta_Bancos_Otros.SelectedIndex = Cmb_Cuenta_Bancos_Otros.Items.IndexOf(Cmb_Cuenta_Bancos_Otros.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Banco_Otros_Deudor].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Por_Pagar_Deudor].ToString()))
                    Cmb_Cuenta_por_Pagar.SelectedIndex = Cmb_Cuenta_por_Pagar.Items.IndexOf(Cmb_Cuenta_por_Pagar.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Por_Pagar_Deudor].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Solicitud_Fondo_Revolvente].ToString()))
                    Cmb_Tipo_Revolvente.SelectedIndex = Cmb_Tipo_Revolvente.Items.IndexOf(Cmb_Tipo_Revolvente.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Solicitud_Fondo_Revolvente].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Caja_Chica].ToString()))
                    Cmb_Tipo_Caja_Chica.SelectedIndex = Cmb_Tipo_Caja_Chica.Items.IndexOf(Cmb_Tipo_Caja_Chica.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Caja_Chica].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Monto_Caja_Chica].ToString()))
                    Txt_Monto_caja_Chica.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Monto_Caja_Chica].ToString();
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Deudor_Caja_Chica].ToString()))
                    Cmb_Deudor_Caja_Chica.SelectedIndex = Cmb_Deudor_Caja_Chica.Items.IndexOf(Cmb_Deudor_Caja_Chica.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Deudor_Caja_Chica].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Gastos].ToString()))
                    Cmb_Usuario_Autoriza_Gastos.SelectedIndex = Cmb_Usuario_Autoriza_Gastos.Items.IndexOf(Cmb_Usuario_Autoriza_Gastos.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Gastos].ToString().Trim()));
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Dias_Comprobacion].ToString()))
                    Txt_Dias_Comprobacion.Text = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Dias_Comprobacion].ToString();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region(Datos Ingresos)

        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fte_Financiamiento
        ///DESCRIPCIÓN          : metodo para llenar el combo de fuentes de financiamiento
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 03/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        private void Llenar_Combo_Fte_Financiamiento(String Busqueda, String Tipo)
        {
            Cls_Cat_Con_Parametros_Negocio Negocio = new Cls_Cat_Con_Parametros_Negocio();
            DataTable Dt_Fte_Financiamiento = new DataTable();

            try
            {

                if (!String.IsNullOrEmpty(Busqueda.Trim()))
                {
                    Negocio.P_Busqueda = Busqueda.Trim();
                }
                else 
                {
                    Negocio.P_Busqueda = String.Empty;
                }

                if (Tipo.Trim().Equals("Municipal"))
                {
                    Negocio.P_Tipo_Fte = "RECURSO PROPIO";
                    Dt_Fte_Financiamiento = Negocio.Consulta_Fte_Financiamiento();
                    Cmb_Fte_Municipal.Items.Clear();

                    if (Dt_Fte_Financiamiento != null)
                    {
                        if (Dt_Fte_Financiamiento.Rows.Count > 0)
                        {
                            Cmb_Fte_Municipal.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                            Cmb_Fte_Municipal.DataTextField = "CLAVE_NOMBRE";
                            Cmb_Fte_Municipal.DataSource = Dt_Fte_Financiamiento;
                            Cmb_Fte_Municipal.DataBind();
                        }
                    }

                    Cmb_Fte_Municipal.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
                }
                else 
                {
                    Negocio.P_Tipo_Fte = "RAMO33";
                    Dt_Fte_Financiamiento = Negocio.Consulta_Fte_Financiamiento();
                    Cmb_Fte_Ramo33.Items.Clear();

                    if (Dt_Fte_Financiamiento != null)
                    {
                        if (Dt_Fte_Financiamiento.Rows.Count > 0)
                        {
                            Cmb_Fte_Ramo33.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                            Cmb_Fte_Ramo33.DataTextField = "CLAVE_NOMBRE";
                            Cmb_Fte_Ramo33.DataSource = Dt_Fte_Financiamiento;
                            Cmb_Fte_Ramo33.DataBind();
                        }
                    }
                    Cmb_Fte_Ramo33.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Fte_Financiamiento ERROR[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programa
        ///DESCRIPCIÓN          : metodo para llenar el combo de los programas
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 03/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        private void Llenar_Combo_Programa(String Busqueda, String Tipo)
        {
            Cls_Cat_Con_Parametros_Negocio Negocio = new Cls_Cat_Con_Parametros_Negocio();
            DataTable Dt_Programas = new DataTable();

            try
            {
                if (!String.IsNullOrEmpty(Busqueda.Trim()))
                {
                    Negocio.P_Busqueda = Busqueda.Trim();
                }
                else
                {
                    Negocio.P_Busqueda = String.Empty;
                }

                Dt_Programas = Negocio.Consulta_Programas();

                if (Tipo.Trim().Equals("Municipal"))
                {
                    Cmb_Programa_Municipal.Items.Clear();

                    if (Dt_Programas != null)
                    {
                        if (Dt_Programas.Rows.Count > 0)
                        {
                            Cmb_Programa_Municipal.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                            Cmb_Programa_Municipal.DataTextField = "CLAVE_NOMBRE";
                            Cmb_Programa_Municipal.DataSource = Dt_Programas;
                            Cmb_Programa_Municipal.DataBind();
                        }
                    }

                    Cmb_Programa_Municipal.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
                }
                else 
                {
                    Cmb_Programa_Ramo33.Items.Clear();

                    if (Dt_Programas != null)
                    {
                        if (Dt_Programas.Rows.Count > 0)
                        {
                            Cmb_Programa_Ramo33.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                            Cmb_Programa_Ramo33.DataTextField = "CLAVE_NOMBRE";
                            Cmb_Programa_Ramo33.DataSource = Dt_Programas;
                            Cmb_Programa_Ramo33.DataBind();
                        }
                    }

                    Cmb_Programa_Ramo33.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Programas ERROR[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Cuentas
        ///DESCRIPCIÓN          : metodo para llenar el combo de las cuentas
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 05/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        private void Llenar_Combo_Cuentas(String Busqueda)
        {
            Cls_Cat_Con_Cuentas_Contables_Negocio Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable Dt_Cuentas = new DataTable();

            try
            {
                Negocio.P_Afectable = "SI";
                if (!String.IsNullOrEmpty(Busqueda))
                {
                    Negocio.P_Descripcion = Busqueda.Trim().ToUpper();
                }
                else
                {
                    Negocio.P_Descripcion = String.Empty;
                }
                Dt_Cuentas = Negocio.Consulta_Cuentas_Contables_Concatena();
                    Cmb_Devengado_Ing.Items.Clear();
                    if (Dt_Cuentas != null)
                    {
                        if (Dt_Cuentas.Rows.Count > 0)
                        {
                            Cmb_Devengado_Ing.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                            Cmb_Devengado_Ing.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                            Cmb_Devengado_Ing.DataSource = Dt_Cuentas;
                            Cmb_Devengado_Ing.DataBind();
                        }
                    }
                    Cmb_Devengado_Ing.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Programas ERROR[" + ex.Message + "]");
            }
        }
    
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Cuentas_Obras
        ///DESCRIPCIÓN          : metodo para llenar el combo de las cuentas
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 12/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        private void Llenar_Combo_Cuentas_Obras(String Busqueda)
        {
            Cls_Cat_Con_Cuentas_Contables_Negocio Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable Dt_Cuentas = new DataTable();

            try
            {
                Negocio.P_Afectable = "SI";
                if (!String.IsNullOrEmpty(Busqueda))
                {
                    Negocio.P_Descripcion = Busqueda.Trim().ToUpper();
                }
                else
                {
                    Negocio.P_Descripcion = String.Empty;
                }
                Dt_Cuentas = Negocio.Consulta_Cuentas_Contables_Concatena();
                Cmb_Obras_Proceso.Items.Clear();
                    if (Dt_Cuentas != null)
                    {
                        if (Dt_Cuentas.Rows.Count > 0)
                        {
                            Cmb_Obras_Proceso.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                            Cmb_Obras_Proceso.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                            Cmb_Obras_Proceso.DataSource = Dt_Cuentas;
                            Cmb_Obras_Proceso.DataBind();
                        }
                    }
                    Cmb_Obras_Proceso.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Programas ERROR[" + ex.Message + "]");
            }
        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : LLenar_Combo_Usuarios
        ///DESCRIPCIÓN          : metodo para llenar el combo de las cuentas
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 09/Septiembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        private void LLenar_Combo_Usuarios(String Busqueda)
        {
            Cls_Cat_Empleados_Negocios Negocio = new Cls_Cat_Empleados_Negocios();
            DataTable Dt_Empleados = new DataTable();

            try
            {
                if (!String.IsNullOrEmpty(Busqueda))
                {
                    Negocio.P_Nombre = Busqueda.Trim().ToUpper();
                }
                else
                {
                    Negocio.P_Nombre = String.Empty;
                }
                Dt_Empleados = Negocio.Consulta_Empleados();
                Cmb_Usuario_Autoriza_Gastos.Items.Clear();
                if (Dt_Empleados != null)
                    {
                        Dt_Empleados.DefaultView.RowFilter = "ESTATUS='ACTIVO'";
                        if (Dt_Empleados.DefaultView.ToTable().Rows.Count > 0)
                        {
                            Cmb_Usuario_Autoriza_Gastos.DataValueField = Cat_Empleados.Campo_Empleado_ID;
                            Cmb_Usuario_Autoriza_Gastos.DataTextField = "EMPLEADO";
                            Cmb_Usuario_Autoriza_Gastos.DataSource = Dt_Empleados;
                            Cmb_Usuario_Autoriza_Gastos.DataBind();
                        }
                    }
                Cmb_Usuario_Autoriza_Gastos.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Usuarios ERROR[" + ex.Message + "]");
            }
        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Cuentas_Baja_Activo
        ///DESCRIPCIÓN          : metodo para llenar el combo de las cuentas
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 12/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        private void Llenar_Combo_Cuentas_Baja_Activo(String Busqueda)
        {
            Cls_Cat_Con_Cuentas_Contables_Negocio Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable Dt_Cuentas = new DataTable();

            try
            {
                Negocio.P_Afectable = "SI";
                if (!String.IsNullOrEmpty(Busqueda))
                {
                    Negocio.P_Descripcion = Busqueda.Trim().ToUpper();
                }
                else
                {
                    Negocio.P_Descripcion = String.Empty;
                }
                Dt_Cuentas = Negocio.Consulta_Cuentas_Contables_Concatena();
                Cmb_Cuenta_Baja_Activo.Items.Clear();
                if (Dt_Cuentas != null)
                {
                    if (Dt_Cuentas.Rows.Count > 0)
                    {
                        Cmb_Cuenta_Baja_Activo.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                        Cmb_Cuenta_Baja_Activo.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                        Cmb_Cuenta_Baja_Activo.DataSource = Dt_Cuentas;
                        Cmb_Cuenta_Baja_Activo.DataBind();
                    }
                }
                Cmb_Cuenta_Baja_Activo.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Programas ERROR[" + ex.Message + "]");
            }
        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llena_Combo_Cuenta_Bancaria_Comision
        ///DESCRIPCIÓN          : metodo para llenar el combo de las cuentas
        ///PARAMETROS           : 
        ///CREO                 : Noe Mosqueda Valadez
        ///FECHA_CREO           : 12/Junio/2013 11:44
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        private void Llena_Combo_Cuenta_Bancaria_Comision(string Busqueda)
        {
            Cls_Cat_Con_Cuentas_Contables_Negocio Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable Dt_Cuentas = new DataTable();

            try
            {
                Negocio.P_Afectable = "SI";
                if (!String.IsNullOrEmpty(Busqueda))
                {
                    Negocio.P_Descripcion = Busqueda.Trim().ToUpper();
                }
                else
                {
                    Negocio.P_Descripcion = String.Empty;
                }
                Dt_Cuentas = Negocio.Consulta_Cuentas_Contables_Concatena();
                Cmb_Cuenta_Comision_Bancaria.Items.Clear();
                if (Dt_Cuentas != null)
                {
                    if (Dt_Cuentas.Rows.Count > 0)
                    {
                        Cmb_Cuenta_Comision_Bancaria.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                        Cmb_Cuenta_Comision_Bancaria.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                        Cmb_Cuenta_Comision_Bancaria.DataSource = Dt_Cuentas;
                        Cmb_Cuenta_Comision_Bancaria.DataBind();
                    }
                }
                Cmb_Cuenta_Comision_Bancaria.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Programas ERROR[" + ex.Message + "]");
            }

        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llena_Combo_Cuenta_Bancos_Otros
        ///DESCRIPCIÓN          : metodo para llenar el combo de las cuentas
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 22/Agosto/2013 11:44
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        private void Llena_Combo_Cuenta_Bancos_Otros(string Busqueda)
        {
            Cls_Cat_Con_Cuentas_Contables_Negocio Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable Dt_Cuentas = new DataTable();

            try
            {
                Negocio.P_Afectable = "SI";
                if (!String.IsNullOrEmpty(Busqueda))
                {
                    Negocio.P_Descripcion = Busqueda.Trim().ToUpper();
                }
                else
                {
                    Negocio.P_Descripcion = String.Empty;
                }
                Dt_Cuentas = Negocio.Consulta_Cuentas_Contables_Concatena();
                Cmb_Cuenta_por_Pagar.Items.Clear();
                if (Dt_Cuentas != null)
                {
                    if (Dt_Cuentas.Rows.Count > 0)
                    {

                        Cmb_Cuenta_por_Pagar.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                        Cmb_Cuenta_por_Pagar.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                        Cmb_Cuenta_por_Pagar.DataSource = Dt_Cuentas;
                        Cmb_Cuenta_por_Pagar.DataBind();
                    }
                }
                Cmb_Cuenta_por_Pagar.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Programas ERROR[" + ex.Message + "]");
            }

        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llena_Combo_Cuenta_Deudor
        ///DESCRIPCIÓN          : metodo para llenar el combo de las cuentas
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 22/Agosto/2013 11:44
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        private void Llena_Combo_Cuenta_Deudor(string Busqueda)
        {
            Cls_Cat_Con_Cuentas_Contables_Negocio Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable Dt_Cuentas = new DataTable();

            try
            {
                Negocio.P_Afectable = "SI";
                if (!String.IsNullOrEmpty(Busqueda))
                {
                    Negocio.P_Descripcion = Busqueda.Trim().ToUpper();
                }
                else
                {
                    Negocio.P_Descripcion = String.Empty;
                }
                Dt_Cuentas = Negocio.Consulta_Cuentas_Contables_Concatena();
                Cmb_Deudor_Caja_Chica.Items.Clear();
                if (Dt_Cuentas != null)
                {
                    if (Dt_Cuentas.Rows.Count > 0)
                    {
                        Cmb_Deudor_Caja_Chica.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                        Cmb_Deudor_Caja_Chica.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                        Cmb_Deudor_Caja_Chica.DataSource = Dt_Cuentas;
                        Cmb_Deudor_Caja_Chica.DataBind();
                    }
                }
                Cmb_Deudor_Caja_Chica.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Programas ERROR[" + ex.Message + "]");
            }

        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llena_Combo_Cuenta_por_Pagar
        ///DESCRIPCIÓN          : metodo para llenar el combo de las cuentas
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 22/Agosto/2013 11:44
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        private void Llena_Combo_Cuenta_por_Pagar(string Busqueda)
        {
            Cls_Cat_Con_Cuentas_Contables_Negocio Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable Dt_Cuentas = new DataTable();

            try
            {
                Negocio.P_Afectable = "SI";
                if (!String.IsNullOrEmpty(Busqueda))
                {
                    Negocio.P_Descripcion = Busqueda.Trim().ToUpper();
                }
                else
                {
                    Negocio.P_Descripcion = String.Empty;
                }
                Dt_Cuentas = Negocio.Consulta_Cuentas_Contables_Concatena();
                Cmb_Cuenta_por_Pagar.Items.Clear();
                if (Dt_Cuentas != null)
                {
                    if (Dt_Cuentas.Rows.Count > 0)
                    {
                        Cmb_Cuenta_por_Pagar.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                        Cmb_Cuenta_por_Pagar.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                        Cmb_Cuenta_por_Pagar.DataSource = Dt_Cuentas;
                        Cmb_Cuenta_por_Pagar.DataBind();
                    }
                }
                Cmb_Cuenta_por_Pagar.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Programas ERROR[" + ex.Message + "]");
            }

        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Buscar_Fte_Municipal_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de fuentes de financiamiento municipal
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 03/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Buscar_Fte_Municipal_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Combo_Fte_Financiamiento(Txt_Buscar_Fte_Municipal.Text.Trim(), "Municipal");
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de fuentes. Error[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Buscar_Fte_Ramo33_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de fuentes de financiamiento ramo33
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 03/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Buscar_Fte_Ramo33_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Combo_Fte_Financiamiento(Txt_Buscar_Fte_Ramo33.Text.Trim(), "Ramo33");
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de fuentes. Error[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Buscar_Programa_Municipal_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de los programas municipales
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 03/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Buscar_Programa_Municipal_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Combo_Programa(Txt_Buscar_Programa_Municipal.Text.Trim(), "Municipal");
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Buscar_Programa_Ramo33_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de los programas ramo33
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 03/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Buscar_Programa_Ramo33_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Combo_Programa(Txt_Buscar_Programa_Ramo33.Text.Trim(), "Ramo33");
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]");
            }
        }
    ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Buscar_Programa_Ramo33_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de los programas ramo33
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 03/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Usuario_Autoriza_Gastos_TextChanged(object sender, EventArgs e)
        {
            try
            {
                LLenar_Combo_Usuarios(Txt_Usuario_Autoriza_Gastos.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]");
            }
        }
    
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Devengado_Ing_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de los programas ramo33
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo
        ///FECHA_CREO           : 05/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Devengado_Ing_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Combo_Cuentas(Txt_Devengado_Ing.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]");
            }
        }
        
    #endregion
    #region
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Obras_Proceso_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de los programas ramo33
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 12/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Obras_Proceso_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Combo_Cuentas_Obras(Txt_Obras_Proceso.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]");
            }
        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Cuenta_Baja_Activo_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de Cuenta de Baja del Activo
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 26/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Cuenta_Baja_Activo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Combo_Cuentas_Baja_Activo(Txt_Cuenta_Baja_Activo.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]");
            }
        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Cuenta_Comision_Bancaria_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de Cuenta 
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 26/Agosto/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Cuenta_Comision_Bancaria_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llena_Combo_Cuenta_Bancaria_Comision(Txt_Cuenta_Comision_Bancaria.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]");
            }
        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Cuenta_Bancos_Otros_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de Cuenta 
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 26/Agosto/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Cuenta_Bancos_Otros_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llena_Combo_Cuenta_Bancos_Otros(Txt_Cuenta_Bancos_Otros.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]");
            }
        }
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Cuenta_por_Pagar_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de Cuenta 
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 26/Agosto/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Cuenta_por_Pagar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llena_Combo_Cuenta_Bancos_Otros(Txt_Cuenta_por_Pagar.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]");
            }
        }

    
        ///*****************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Deudor_Caja_Chica_TextChanged
        ///DESCRIPCIÓN          : evento de la caja de texto de busqueda de Cuenta 
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 26/Agosto/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************
        protected void Txt_Deudor_Caja_Chica_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llena_Combo_Cuenta_Deudor(Txt_Deudor_Caja_Chica.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]");
            }
        }

    #endregion

}
