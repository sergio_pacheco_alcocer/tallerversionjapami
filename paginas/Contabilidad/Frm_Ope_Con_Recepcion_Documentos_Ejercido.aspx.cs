﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Autoriza_Ejercido.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Recepcion_Documentos_Ejercido : System.Web.UI.Page
{
    #region PAGE LOAD
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    Recepcion_Documentos_Inicio();
                    Acciones();
                    Txt_Lector_Codigo_Barras.Focus();
                }
            }
            catch(Exception Ex) 
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error["+Ex.Message+"]");
            }
        }
    #endregion

    #region METODOS
     
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Recepcion_Documentos_Inicio
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Recepcion_Documentos_Inicio() 
        {
            try
            {
                Limpiar_Controles();
                Llenar_Grid_Solicitudes_Pago();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
            }
        }
        
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
        ///DESCRIPCIÓN          : Metodo para limpiar los controles de la página
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Limpiar_Controles()
        {
            try
            {
                Hf_No_Solicitud_Autorizar.Value = "";
                Hf_Rechazo.Value = "";
                Txt_Documentos.Text = "";
                Txt_Comentario.Text = "";
                Grid_Solicitud_Pagos.DataSource = null;
                Grid_Solicitud_Pagos.DataBind();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al limpiar los controles de la página Error[" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Llenar_Grid_Solicitudes_Pago
        /// DESCRIPCION : Llena el grid Solicitudes de pago
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Llenar_Grid_Solicitudes_Pago()
        {
            String No_Autoriza_Ramo33="";
            String No_Autoriza_Ramo33_2="";
            try
            {
                Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
                DataTable Dt_Usuario = new DataTable();
                Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
                if (Dt_Usuario.Rows.Count > 0)
                {
                    No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                    No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                }
                Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Autoriza_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
                DataTable Dt_Resultado = new DataTable();
                Rs_Autoriza_Solicitud.P_Estatus = "PRE-PORPAGAR";
                if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                {
                    Rs_Autoriza_Solicitud.P_Tipo_Recurso = "RAMO 33";
                }
                else
                {
                    Rs_Autoriza_Solicitud.P_Tipo_Recurso = "RECURSO ASIGNADO";
                }
                Dt_Resultado = Rs_Autoriza_Solicitud.Consulta_Solicitudes();
                
                Grid_Solicitud_Pagos.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
                Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;

                if (Dt_Resultado.Rows.Count > 0)
                {
                    Grid_Solicitud_Pagos.Columns[3].Visible = true;
                    Grid_Solicitud_Pagos.DataSource = Dt_Resultado;   // Se iguala el DataTable con el Grid
                    Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;
                    Grid_Solicitud_Pagos.Columns[3].Visible = false;
                }
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitudes de Pago", "alert('En este momento no se tienen pagos pendientes por autorizar');", true);
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
            }
        }

        // ****************************************************************************************
        //'NOMBRE DE LA FUNCION:Accion
        //'DESCRIPCION : realiza la modificacion la preautorización del pago o el rechazo de la solicitud de pago
        //'PARAMETROS  : 
        //'CREO        : Sergio Manuel Gallardo
        //'FECHA_CREO  : 07/Noviembre/2011 12:12 pm
        //'MODIFICO          :
        //'FECHA_MODIFICO    :
        //'CAUSA_MODIFICACION:
        //'****************************************************************************************
        protected void Acciones()
        {
            String Accion = String.Empty;
            String No_Solicitud = String.Empty;
            String Comentario = String.Empty;
            String Documento = String.Empty;
            String Estatus;
            DataTable Dt_solicitud= new DataTable();
            DataTable Dt_Consulta_Estatus = new DataTable();
            String No_Autoriza_Ramo33 = "";
            String No_Autoriza_Ramo33_2 = "";
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();    //Objeto de acceso a los metodos.
            if (Request.QueryString["Accion"] != null)
            {
                Accion = HttpUtility.UrlDecode(Request.QueryString["Accion"].ToString());
                if (Request.QueryString["id"] != null)
                {
                    No_Solicitud = HttpUtility.UrlDecode(Request.QueryString["id"].ToString());
                }
                if (Request.QueryString["x"] != null)
                {
                    Comentario = HttpUtility.UrlDecode(Request.QueryString["x"].ToString());
                }
                //Response.Clear()
                switch (Accion)
                {
                    case "Autorizar_Solicitud":
                        Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
                        DataTable Dt_Usuario = new DataTable();
                        Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
                        if (Dt_Usuario.Rows.Count > 0)
                        {
                            No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                            No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                        }
                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                        if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                        {
                            Rs_Solicitud_Pago.P_Tipo_Recurso = "RAMO 33";
                        }
                        else
                        {
                            Rs_Solicitud_Pago.P_Tipo_Recurso = "RECURSO ASIGNADO";
                        }
                        Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                        if (Dt_Consulta_Estatus.Rows.Count > 0)
                        {
                            Estatus = Dt_Consulta_Estatus.Rows[0]["ESTATUS"].ToString().Trim();
                            if (Estatus == "PRE-PORPAGAR")
                            {
                                Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();
                                Rs_Solicitud.P_No_Solicitud_Pago = No_Solicitud;
                                Rs_Solicitud.P_Estatus = "PORPAGAR";
                                Rs_Solicitud.P_Comentario = Comentario;
                                Rs_Solicitud.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                Rs_Solicitud.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                Rs_Solicitud.P_Usuario_Recibio_Ejercido = Cls_Sessiones.Nombre_Empleado.ToString();
                                Rs_Solicitud.Cambiar_Estatus_Solicitud_Pago();
                                
                            }
                        }
                        break;
                    case "Rechazar_Solicitud":
                        Cancela_Solicitud_Pago(No_Solicitud, Comentario, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString());
                        break;
                    case "Codigo_Barras":
                        if (No_Solicitud.Length < 10)
                        {
                            Rs_Solicitud_Pago.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                        }
                        else
                        {
                            Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                        }
                        Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario_2 = new Cls_Cat_Con_Parametros_Negocio();
                        DataTable Dt_Usuario_2 = new DataTable();
                        Dt_Usuario_2 = Rs_Validar_Usuario_2.Consulta_Datos_Parametros();
                        if (Dt_Usuario_2.Rows.Count > 0)
                        {
                            No_Autoriza_Ramo33 = Dt_Usuario_2.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                            No_Autoriza_Ramo33_2 = Dt_Usuario_2.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                        }
                        if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                        {
                            Rs_Solicitud_Pago.P_Tipo_Recurso = "RAMO 33";
                        }
                        else
                        {
                            Rs_Solicitud_Pago.P_Tipo_Recurso = "RECURSO ASIGNADO";
                        }
                        Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                        //Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                        //Dt_solicitud = Rs_Solicitud_Pago.Consultar_Solicitud_Pago();
                        if (Dt_Consulta_Estatus.Rows.Count > 0)
                        {
                            Estatus = Dt_Consulta_Estatus.Rows[0]["ESTATUS"].ToString().Trim();
                            if (Estatus == "PRE-PORPAGAR")
                            {
                                Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();
                                if (No_Solicitud.Length < 10)
                                {
                                    Rs_Solicitud.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                                }
                                else
                                {
                                    Rs_Solicitud.P_No_Solicitud_Pago = No_Solicitud;
                                }
                                Rs_Solicitud.P_Estatus = "PORPAGAR";
                                Rs_Solicitud.P_Comentario = Comentario;
                                Rs_Solicitud.P_Empleado_ID_Ejercido = Cls_Sessiones.Empleado_ID.ToString();
                                Rs_Solicitud.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                Rs_Solicitud.P_Usuario_Recibio_Ejercido = Cls_Sessiones.Nombre_Empleado.ToString();
                                Rs_Solicitud.Cambiar_Estatus_Solicitud_Pago();
                            }
                        }
                        break;
                }
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago
        /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
        ///               proporcionados por el usuario
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Cancela_Solicitud_Pago(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado)
        {
         Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
            DataTable Dt_Detalles = new DataTable();
            String Monto;
            String Reserva;
            Monto = "0";
            Reserva = "0";
            try
            {    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Solicitud_Negocio.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
                if (Dt_Detalles != null)
                {
                    if (Dt_Detalles.Rows.Count > 0)

                    {
                        Monto =Dt_Detalles.Rows[0]["MONTO"].ToString();
                        Reserva = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString();
                    }
                }
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Convert.ToDouble(Monto);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "CANCELADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto ="CANCELACION-" + No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Recepcion = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva_Anterior = Convert.ToDouble(Reserva);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_recepcion_Documentos = String.Format("{0:dd/MM/yyyy}",DateTime.Now);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Modificar_Solicitud_Pago_Sin_Poliza(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
               // Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                
            }
            catch (Exception ex)
            {
                throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
            }
        }
    #endregion

    #region EVENTOS
        protected void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Recepcion_Documentos_Inicio();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
            }
        }

        protected void Btn_Comentar_Click(object sender, EventArgs e)
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Tipo_Solicitud = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
            DataTable Dt_Datos_Polizas = new DataTable();
            DataTable Dt_Tipo_Solicitud = new DataTable();
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            ReportDocument Reporte = new ReportDocument();
            String Tipo_Solicitud = "";
            String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
            String Nombre_Archivo = "Rechazado";// +Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
            String Usuario = "";
            try
            {
                DataRow Row;
                DataTable Dt_Reporte = new DataTable();
                if (Txt_Comentario.Text != "")
                {
                    Cancela_Solicitud_Pago(Hf_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString());
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Hf_No_Solicitud_Autorizar.Value;
                    Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();

                    //  se crea la tabla para el reporte de la cancelacion
                    Dt_Reporte.Columns.Add("NO_SOLICITUD", typeof(System.String));
                    Dt_Reporte.Columns.Add("NO_RESERVA", typeof(System.String));
                    Dt_Reporte.Columns.Add("PROVEEDOR", typeof(System.String));
                    Dt_Reporte.Columns.Add("MONTO", typeof(System.Double));
                    Dt_Reporte.Columns.Add("FECHA_CREO", typeof(System.DateTime));
                    Dt_Reporte.Columns.Add("FECHA_RECHAZO", typeof(System.DateTime));
                    Dt_Reporte.Columns.Add("TIPO_SOLICITUD_PAGO_ID", typeof(System.String));
                    Dt_Reporte.Columns.Add("CONCEPTO_SOLICITUD", typeof(System.String));
                    Dt_Reporte.Columns.Add("COMENTARIO", typeof(System.String));
                    Dt_Reporte.Columns.Add("USUARIO_CREO", typeof(System.String));
                    Dt_Reporte.Columns.Add("USUARIO_RECHAZO", typeof(System.String));
                    Dt_Reporte.TableName = "Dt_Cancelacion";


                    foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
                    {
                        Row = Dt_Reporte.NewRow();

                        Row["NO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString());
                        Row["NO_RESERVA"] = (Registro["Reserva"].ToString());
                        Row["PROVEEDOR"] = (Registro["Proveedor"].ToString());
                        Row["MONTO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                        Row["FECHA_CREO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo].ToString());
                        Row["FECHA_RECHAZO"] = "" + DateTime.Now;

                        //  para el tipo de solicitud
                        Rs_Tipo_Solicitud.P_Tipo_Solicitud = (Registro[Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString());
                        Dt_Tipo_Solicitud = Rs_Tipo_Solicitud.Consulta_Tipo_Solicitud();

                        foreach (DataRow Tipo in Dt_Tipo_Solicitud.Rows)
                        {
                            Tipo_Solicitud = (Tipo[Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion].ToString());
                        }
                        Row["TIPO_SOLICITUD_PAGO_ID"] = Tipo_Solicitud;
                        Row["CONCEPTO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString());
                        Row["COMENTARIO"] = Txt_Comentario.Text;
                        Usuario = (Registro[Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo].ToString());
                        Usuario = Usuario.Replace(".", "");
                        Row["USUARIO_CREO"] = Usuario;
                        Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                        Usuario = Usuario.Replace(".", "");
                        Row["USUARIO_RECHAZO"] = Usuario;
                        Dt_Reporte.Rows.Add(Row);
                        Dt_Reporte.AcceptChanges();
                    }
                    Ds_Reporte.Clear();
                    Ds_Reporte.Tables.Clear();
                    Ds_Reporte.Tables.Add(Dt_Reporte.Copy());
                    Reporte.Load(Ruta_Archivo + "Rpt_Con_Cancelacion_Recepcion.rpt");
                    Reporte.SetDataSource(Ds_Reporte);
                    DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                    Nombre_Archivo += ".pdf";
                    Ruta_Archivo = @Server.MapPath("../../Reporte/");
                    m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                    ExportOptions Opciones_Exportacion = new ExportOptions();
                    Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);

                    Abrir_Ventana(Nombre_Archivo);
                    Recepcion_Documentos_Inicio();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);   
                }
                else
                {
                    Recepcion_Documentos_Inicio();
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";

                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
        ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
        ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
        ///                             para mostrar los datos al usuario
        ///CREO       : Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO  : 21-Febrero-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        private void Abrir_Ventana(String Nombre_Archivo)
        {
            String Pagina = "../../Reporte/";//"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
            try
            {
                Pagina = Pagina + Nombre_Archivo;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception ex)
            {
                throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
            }
        }

        protected void Btn_Buscar_No_Solicitud_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Resultado = new DataTable();
            String No_Autoriza_Ramo33 = "";
            String No_Autoriza_Ramo33_2 = "";
            try
            {
                Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
                DataTable Dt_Usuario = new DataTable();
                Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
                if (Dt_Usuario.Rows.Count > 0)
                {
                    No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                    No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                }
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
                {
                    Rs_Consultar_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
                    if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                    }
                    else
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    }
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
                }
                else { 
                    Rs_Consultar_Solicitud_Pagos.P_Estatus="PRE-PORPAGAR";
                    if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                    }
                    else
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    }
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes();
                }
                
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la b&uacute;squeda <br />";
                    Txt_Busqueda_No_Solicitud.Focus();
                }
                else
                {
                    Grid_Solicitud_Pagos.Columns[3].Visible = true;
                    Grid_Solicitud_Pagos.DataSource = Dt_Resultado;
                    Grid_Solicitud_Pagos.DataBind();
                    Grid_Solicitud_Pagos.Columns[3].Visible = false;
                    Txt_Busqueda_No_Solicitud.Text = "";
                }

            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        protected void Txt_Buscar_No_Solicitud_TextChanged(object sender, EventArgs e)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Resultado = new DataTable();
            String No_Autoriza_Ramo33 = "";
            String No_Autoriza_Ramo33_2 = "";
            try
            {
                Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
                DataTable Dt_Usuario = new DataTable();
                Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
                if (Dt_Usuario.Rows.Count > 0)
                {
                    No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                    No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                }
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
                {
                    Rs_Consultar_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
                    if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                    }
                    else
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    }
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
                }
                else
                {
                    Rs_Consultar_Solicitud_Pagos.P_Estatus = "PRE-PORPAGAR";
                    if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                    }
                    else
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    }
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes();
                }

                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la b&uacute;squeda <br />";
                    Txt_Busqueda_No_Solicitud.Focus();
                }
                else
                {
                    Grid_Solicitud_Pagos.Columns[3].Visible = true;
                    Grid_Solicitud_Pagos.DataSource = Dt_Resultado;
                    Grid_Solicitud_Pagos.DataBind();
                    Grid_Solicitud_Pagos.Columns[3].Visible = false;
                    Txt_Busqueda_No_Solicitud.Text = "";
                }

            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }

        #region GENERALES
            ///*********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
            ///DESCRIPCIÓN          : Evento del boton de salir
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Diciembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
                protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    if (Btn_Salir.ToolTip == "Inicio")
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                    else
                    {
                        Recepcion_Documentos_Inicio();
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
        #endregion

        #region"Grid"

                protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
                {
                    HyperLink Hyp_Lnk_Ruta;
                    try
                    {
                        if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                        {
                            Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                            if (!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) && e.Row.Cells[3].Text.Trim() != "&nbsp;")
                            {
                                Hyp_Lnk_Ruta.NavigateUrl = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[3].Text.Trim();
                                Hyp_Lnk_Ruta.Enabled = true;
                            }
                            else
                            {
                                Hyp_Lnk_Ruta.NavigateUrl = "";
                                Hyp_Lnk_Ruta.Enabled = false;
                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception(Ex.Message);
                    }
                }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Solicitud_Pagos_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Diciembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Grid_Solicitud_Pagos_SelectedIndexChanged(object sender, EventArgs e)
            {
                Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
                DataTable Dt_Detalles = new DataTable();
                DataTable Dt_Documentos = new DataTable();

                try
                {
                    if (Grid_Solicitud_Pagos.SelectedIndex > (-1))
                    {
                        Solicitud_Negocio.P_No_Solicitud_Pago = Grid_Solicitud_Pagos.SelectedRow.Cells[1].Text.Trim();
                        Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
                        Dt_Documentos = Solicitud_Negocio.Consulta_Documentos();

                        if (Dt_Detalles != null)
                        {
                            if (Dt_Detalles.Rows.Count > 0)
                            {
                                Txt_No_Pago_Det.Text = Dt_Detalles.Rows[0]["NO_SOLICITUD_PAGO"].ToString().Trim();
                                Txt_No_Reserva_Det.Text = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString().Trim();
                                Txt_Concepto_Reserva_Det.Text = Dt_Detalles.Rows[0]["CONCEPTO_RESERVA"].ToString().Trim();
                                Txt_Beneficiario_Det.Text = Dt_Detalles.Rows[0]["BENEFICIARIO"].ToString().Trim();
                                Txt_Fecha_Solicitud_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_SOLICITUD"]);
                                Txt_Monto_Solicitud_Det.Text = String.Format("{0:c}", Dt_Detalles.Rows[0]["MONTO"]);
                                Txt_Fecha_Autoriza_Director_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZO_RECHAZO_JEFE"]);
                                Txt_No_poliza_Det.Text = Dt_Detalles.Rows[0]["NO_POLIZA"].ToString().Trim();
                                Txt_Fecha_Recepcion_Doc_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECEPCION_DOCUMENTOS"]);
                                Txt_Recibio_Documentacion_Fisica.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_DOC_FISICA"].ToString().Trim();
                                Txt_Fecha_Recibio_Documentacion_Fisica.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECEPCION_DOCUMENTOS"]);
                                Txt_Aut_Documentacion_Fisica.Text = Dt_Detalles.Rows[0]["USUARIO_AUTORIZO_DOCUMENTOS"].ToString().Trim();
                                Txt_Fecha_Recepcion_Doc_Contabilidad.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECIBIO_CONTABILIDAD"]);
                                Txt_Recibio_Documentacion_Contabilidad.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_CONTABILIDAD"].ToString().Trim();
                                Txt_Aut_Contabilidad.Text = Dt_Detalles.Rows[0]["USUARIO_AUTORIZO_CONTABILIDAD"].ToString().Trim();
                                Txt_Fecha_Aut_Contabilidad.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZO_RECHAZO_CONTABI"]);
                                Grid_Solicitud_Pagos.SelectedIndex = -1;
                                if (String.IsNullOrEmpty(Txt_No_poliza_Det.Text))
                                {
                                    Tr_Poliza.Style.Add("Display", "none");
                                }
                                else
                                {
                                    Tr_Poliza.Style.Add("Display", "block");
                                }

                                Grid_Documentos.Columns[3].Visible = true;
                                Grid_Documentos.Columns[4].Visible = true;
                                Grid_Documentos.DataSource = Dt_Documentos;
                                Grid_Documentos.DataBind();
                                Grid_Documentos.Columns[3].Visible = false;
                                Grid_Documentos.Columns[4].Visible = false;
                                Mpe_Detalles.Show();
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
                }
            } ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Grid_Solicitud_Pagos_RowDataBound
            /// DESCRIPCION : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 09/enero/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Grid_Solicitud_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                try
                {
                    CheckBox chek = (CheckBox)e.Row.FindControl("Chk_Autorizado");
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        if (e.Row.Cells[7].Text != "PRE-PORPAGAR")
                        {
                            chek.Enabled = false;
                        }
                        else
                        {
                            chek.Enabled = true;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
        #endregion

    #endregion


    
}
