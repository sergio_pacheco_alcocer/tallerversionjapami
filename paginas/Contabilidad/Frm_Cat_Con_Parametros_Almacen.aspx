﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" CodeFile="Frm_Cat_Con_Parametros_Almacen.aspx.cs" Inherits="paginas_Contabilidad_Frm_Cat_Con_Parametros_Almacen" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
        <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
        </asp:UpdateProgress>
        <%--Div de Contenido --%>
        <div id="Div_Contenido" style="width:97%;height:100%;">
        <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
            <tr>
                <td colspan ="4" class="label_titulo">Parametros Polizas Almacen</td>
            </tr>
            <%--Fila de div de Mensaje de Error --%>
            <tr>
                <td colspan ="4">
                    <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                    <table style="width:100%;">
                        <tr>
                            <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                            Width="24px" Height="24px"/>
                            </td>            
                            <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                            </td>
                        </tr> 
                    </table>                   
                    </div>
                </td>
            </tr>
            <%--Fila de Busqueda y Botones Generales --%>
            <tr class="barra_busqueda">
                <td style="width:20%;" colspan="4">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:ImageButton ID="Btn_Modificar" runat="server" CssClass="Img_Button" 
                        ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                        ToolTip="Modificar" onclick="Btn_Modificar_Click"/>
                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                            onclick="Btn_Salir_Click"/>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div id="Div_General" style="width:100%;height:100%;" runat="server">
                        <table width="100%">
                            <tr>
                                <td style="width:20%">
                                    Cuenta Almacen General</td>
                                <td style="width:38%">                                
                                    <asp:TextBox ID="Txt_Cuenta_A_General" runat="server" Width="100%" 
                                        ontextchanged="Txt_Cuenta_A_General_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                </td>
                                <td style="width:40%">
                                    <asp:DropDownList ID="Cmb_Cuenta_A_General" runat="server" Width="100%" 
                                        onselectedindexchanged="Cmb_Cuenta_A_General_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>                            
                            <tr>
                                <td>
                                    Cuenta Almacen Papeleria</td>
                                 <td>
                                     <asp:TextBox ID="Txt_Cuenta_A_Papeleria" runat="server" Width="100%" 
                                         ontextchanged="Txt_Cuenta_A_Papeleria_TextChanged" AutoPostBack="true"></asp:TextBox>
                                </td>
                                 <td>
                                     <asp:DropDownList ID="Cmb_Cuenta_A_Papeleria" runat="server" Width="100%" 
                                         onselectedindexchanged="Cmb_Cuenta_A_Papeleria_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cuenta IVA Acreditable</td>
                                 <td>
                                    <asp:TextBox ID="Txt_Cuenta_IVA" runat="server" Width="100%" 
                                         ontextchanged="Txt_Cuenta_IVA_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                 </td>
                                 <td>
                                     <asp:DropDownList ID="Cmb_Cuenta_IVA" runat="server" Width="100%" 
                                         onselectedindexchanged="Cmb_Cuenta_IVA_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cuenta IVA Pendiente de Acreditar</td>
                                 <td>
                                    <asp:TextBox ID="Txt_Cuenta_IVA_Pendiente" runat="server" Width="100%" 
                                          AutoPostBack="true" ></asp:TextBox>
                                 </td>
                                 <td>
                                     <asp:DropDownList ID="Cmb_Cuenta_IVA_Pendiente" runat="server" Width="100%" 
                                      AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tipo Poliza Egresos</td>
                                 <td colspan="2">
                                     <asp:DropDownList ID="Cmb_Poliza_Egresos" runat="server" Width="80%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tipo Poliza Diario</td>
                                <td colspan="2">
                                     <asp:DropDownList ID="Cmb_Poliza_Diario" runat="server" Width="80%">
                                    </asp:DropDownList>
                                </td>                   
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>    
            
